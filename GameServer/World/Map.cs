﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Map.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Items;
using GameServer.World.MapStruct;

#endregion

namespace GameServer.World
{
    public sealed class Map : Floor
    {
        public const uint DEFAULT_LIGHT_RGB = 0xFFFFFF;

        public const int REGION_NONE = 0,
            REGION_CITY = 1,
            REGION_WEATHER = 2,
            REGION_STATUARY = 3,
            REGION_DESC = 4,
            REGION_GOBALDESC = 5,
            REGION_DANCE = 6, // data0: idLeaderRegion, data1: idMusic, 
            REGION_PK_PROTECTED = 7,
            REGION_FLAG_BASE = 8;

        public static readonly sbyte[] WalkXCoords = {0, -1, -1, -1, 0, 1, 1, 1};
        public static readonly sbyte[] WalkYCoords = {1, 1, 0, -1, -1, -1, 0, 1};

        public static readonly sbyte[] DeltaWalkXCoords =
            {0, -2, -2, -2, 0, 2, 2, 2, 1, 0, -2, 0, 1, 0, 2, 0, 0, -2, 0, -1, 0, 2, 0, 1, 0};

        public static readonly sbyte[] DeltaWalkYCoords =
            {2, 2, 0, -2, -2, -2, 0, 2, 2, 0, -1, 0, -2, 0, 1, 0, 0, 1, 0, -2, 0, -1, 0, 2, 0};

        private MapEntity m_dbMap;
        private DynamicMapEntity m_dbDynaMap;
        private KeyValuePair<uint, Point> m_kRebirthPoint;
        public ConcurrentDictionary<uint, Character> PlayerSet = new ConcurrentDictionary<uint, Character>();
        public ConcurrentDictionary<uint, ScreenObject> RoleSet = new ConcurrentDictionary<uint, ScreenObject>();

        public ConcurrentDictionary<uint, IPassway> Portals = new ConcurrentDictionary<uint, IPassway>();
        public ConcurrentDictionary<uint, RegionEntity> Regions = new ConcurrentDictionary<uint, RegionEntity>();

        public Map(MapEntity dbMap)
            : base(dbMap.Path)
        {
            m_dbMap = dbMap;

            m_kRebirthPoint = new KeyValuePair<uint, Point>(m_dbMap.RebornMap ?? 1002,
                new Point((int) (m_dbMap.PortalX ?? 430), (int) (m_dbMap.PortalY ?? 388)));

            Weather = new Weather(this);
        }

        public Map(DynamicMapEntity dbMap)
            : base(dbMap.FileName)
        {
            m_dbDynaMap = dbMap;

            m_kRebirthPoint = new KeyValuePair<uint, Point>(m_dbDynaMap.RebornMapid,
                new Point((int) m_dbDynaMap.Portal0X, (int) m_dbDynaMap.Portal0Y));

            Weather = new Weather(this);
        }

        #region Identification

        public uint Identity => m_dbMap?.Identity ?? m_dbDynaMap.Identity;

        public uint MapDoc => m_dbMap?.MapDoc ?? m_dbDynaMap.MapDoc;

        public uint Type => m_dbMap?.Type ?? m_dbDynaMap.Type;

        public string Name => m_dbMap?.Name ?? m_dbDynaMap.Name;

        public uint RebornMapId => m_kRebirthPoint.Key;

        public Point RebornMapPoint => m_kRebirthPoint.Value;

        #endregion

        #region Dimensions

        public int Width => Boundaries.Width;
        public int Height => Boundaries.Height;

        #endregion

        #region Weather

        public Weather Weather;

        public void SendWeather()
        {
            foreach (var player in PlayerSet.Values)
                Weather.SendWeather(player);
        }

        #endregion

        #region Light

        public uint Light;

        #endregion

        #region Map Checks

        /// <summary>
        /// Checks if the map is a pk field. Wont add pk points.
        /// </summary>
        public bool IsPkField()
        {
            return (Type & (uint) MapTypeFlags.PkField) != 0;
        }

        /// <summary>
        /// Disable teleporting by skills or scrolls.
        /// </summary>
        public bool IsChgMapDisable()
        {
            return (Type & (uint) MapTypeFlags.ChangeMapDisable) != 0;
        }

        /// <summary>
        /// Disable recording the map position into the database.
        /// </summary>
        public bool IsRecordDisable()
        {
            return (Type & (uint) MapTypeFlags.RecordDisable) != 0;
        }

        /// <summary>
        /// Disable team creation into the map.
        /// </summary>
        public bool IsTeamDisable()
        {
            return (Type & (uint) MapTypeFlags.TeamDisable) != 0;
        }

        /// <summary>
        /// Disable use of pk on the map.
        /// </summary>
        public bool IsPkDisable()
        {
            return (Type & (uint) MapTypeFlags.PkDisable) != 0;
        }

        /// <summary>
        /// Disable teleporting by actions.
        /// </summary>
        public bool IsTeleportDisable()
        {
            return (Type & (uint) MapTypeFlags.TeleportDisable) != 0;
        }

        /// <summary>
        /// Checks if the map is a syndicate map
        /// </summary>
        /// <returns></returns>
        public bool IsSynMap()
        {
            return (Type & (uint) MapTypeFlags.GuildMap) != 0;
        }

        /// <summary>
        /// Checks if the map is a prision
        /// </summary>
        public bool IsPrisionMap()
        {
            return (Type & (uint) MapTypeFlags.PrisonMap) != 0;
        }

        /// <summary>
        /// If the map enable the fly skill.
        /// </summary>
        public bool IsWingDisable()
        {
            return (Type & (uint) MapTypeFlags.WingDisable) != 0;
        }

        /// <summary>
        /// Check if the map is in war.
        /// </summary>
        public bool IsWarTime()
        {
            return (Flag & 1) != 0;
        }

        /// <summary>
        /// Check if the map is the training ground. [1039]
        /// </summary>
        public bool IsTrainingMap()
        {
            return Identity == 1039;
        }

        /// <summary>
        /// Check if its the family (clan) map.
        /// </summary>
        public bool IsFamilyMap()
        {
            return (Type & (uint) MapTypeFlags.Family) != 0;
        }

        /// <summary>
        /// If the map enables booth to be built.
        /// </summary>
        public bool IsBoothEnable()
        {
            return (Type & (uint) MapTypeFlags.BoothEnable) != 0;
        }

        public bool IsDeadIsland()
        {
            return (Type & (uint) MapTypeFlags.DeadIsland) != 0;
        }

        public bool IsPkGameMap()
        {
            return (Type & (uint) MapTypeFlags.PkGame) != 0;
        }

        public bool IsMineField()
        {
            return (Type & (uint) MapTypeFlags.MineField) != 0;
        }

        public bool IsSkillMap()
        {
            return (Type & (uint) MapTypeFlags.SkillMap) != 0;
        }

        public bool IsLineSkillMap()
        {
            return (Type & (ulong) MapTypeFlags.LineSkillOnly) != 0;
        }

        public bool IsDynamicMap()
        {
            return Identity > 999999;
        }

        #endregion

        #region Reborn Map

        public bool GetRebornMap(ref uint mapId, ref Point pposTarget)
        {
            Map targetMap = ServerKernel.Maps.Values.FirstOrDefault(x => x.Identity == RebornMapId);
            if (targetMap == null)
            {
                Program.WriteLog($"ERROR: Could not find reborn map [{RebornMapId}] to map [{Identity}]",
                    LogType.WARNING);
                return false;
            }

            var posNew = new Point(targetMap.RebornMapPoint.X, targetMap.RebornMapPoint.Y);
            if (posNew.X == 0 || posNew.Y == 0)
            {
                posNew.X = 430;
                posNew.Y = 378;
                mapId = 1002;
                return false;
            }

            mapId = RebornMapId != 0 ? RebornMapId : Identity;
            pposTarget = posNew;
            return true;
        }

        #endregion

        #region Teleport

        public bool GetPassageMap(ref uint idMap, ref Point posTarget, uint idx)
        {
            IPassway portal = Portals.Values.FirstOrDefault(x => x.PasswayIndex == idx);
            if (portal == null)
            {
                Program.WriteLog($"Invalid portal at [{Identity}] idx {idx}");
                return false;
            }

            idMap = portal.PortalMap;
            posTarget.X = (int) portal.PortalX;
            posTarget.Y = (int) portal.PortaLy;
            return true;
        }
        public ushort PortalX
        {
            get
            {
                if (IsDynamicMap())
                    return (ushort)m_dbDynaMap.Portal0X;
                return (ushort) (m_dbMap?.PortalX ?? 0);
            }
            set { if (IsDynamicMap()) m_dbDynaMap.Portal0X = value; }
        }

        public ushort PortalY
        {
            get
            {
                if (IsDynamicMap())
                    return (ushort)m_dbDynaMap.Portal0Y;
                return (ushort)(m_dbMap?.PortalY ?? 0);
            }
            set { if (IsDynamicMap()) m_dbDynaMap.Portal0Y = value; }
        }


        #endregion

        #region Loading

        public override bool Load()
        {
            if (base.Load())
            {
                Loaded = true;
                return true;
            }

            return false;
        }

        #endregion

        #region Indexes

        public int Pos2Index(int x, int y, int cx, int cy)
        {
            return x + y * cx;
        }

        public int Index2X(int idx, int cx, int cy)
        {
            return idx % cy;
        }

        public int Index2Y(int idx, int cx, int cy)
        {
            return idx / cy;
        }

        #endregion

        #region Movement

        public bool IsAltOver(Point point, int nAlt)
        {
            if (!IsValidPoint(point))
                return false;

            if (this[point.X, point.Y].Elevation > nAlt)
                return true;

            return false;
        }

        public bool IsValidPoint(Point pos)
        {
            return IsValidPoint(pos.X, pos.Y);
        }

        public bool IsValidPoint(int x, int y)
        {
            return x >= 0 && x < Width && y >= 0 && y < Height;
        }

        public bool IsStandEnable(ushort nPosX, ushort nPosY)
        {
            if (nPosX > 0 && nPosX < Width && nPosY > 0 && nPosY < Height)
                return this[nPosX, nPosY].Access > TileType.Terrain;
            return false;
        }

        public bool IsSuperPosition(int x, int y)
        {
            return PlayerSet.Values.Any(a => a.MapX == x && a.MapY == y) || RoleSet.Values.Any(a => a.MapX == x && a.MapY == y && !(a is MapItem) && (a as Role)?.IsAlive == true);
        }

        public bool IsMoveEnable(int x, int y, int nDir, int nSizeAdd, int nClimbCap)
        {
            if (nSizeAdd > 4)
                nSizeAdd = 4;

            ushort newX = (ushort) (x + WalkXCoords[nDir]);
            ushort newY = (ushort) (y + WalkYCoords[nDir]);

            if (!IsValidPoint(newX, newY))
                return false;

            int nElevation = 0;
            int nOldElevation = this[x, y].Elevation;
            int nNewElevation = this[newX, newY].Elevation;
            if (nOldElevation >= nNewElevation)
                nElevation = nOldElevation - nNewElevation;
            else
                nElevation = nNewElevation - nOldElevation;

            if (nClimbCap > 0 && nElevation > nClimbCap)
                return false;

            if (nSizeAdd > 0 && nSizeAdd <= 2)
            {
                int nMoreDir = nDir % 2 > 0 ? 1 : 2;
                for (int i = -1 * nMoreDir; i <= nMoreDir; i++)
                {
                    int nDir2 = (nDir + i + 8) % 8;
                    int nNewX2 = newX + WalkXCoords[nDir2];
                    int nNewY2 = newY + WalkYCoords[nDir2];
                    if (!IsValidPoint(nNewX2, nNewY2))
                        return false;
                }
            }
            else if (nSizeAdd > 2)
            {
                int nRange = (nSizeAdd + 1) / 2;
                for (ushort i = (ushort) (newX - nRange); i <= newX + nRange; i++)
                {
                    for (ushort j = (ushort) (newY - nRange); j <= newY + nRange; j++)
                    {
                        if (GetDistance(i, j, (ushort) x, (ushort) y) > nRange)
                        {
                            if (!IsValidPoint(i, j))
                                return false;
                        }
                    }
                }
            }

            if (this[newX, newY].Access < TileType.Monster)
                return false;

            return true;
        }

        public double GetDistance(ushort x, ushort y, ushort nx, ushort ny)
        {
            return Calculations.GetDistance(x, y, nx, ny);
        }

        public bool IsInScreen(ScreenObject obj1, ScreenObject obj2)
        {
            return Calculations.InScreen(obj1.MapX, obj1.MapY, obj2.MapX, obj2.MapY);
        }

        public bool IsInScreen(Point p, ScreenObject obj2)
        {
            return Calculations.InScreen((ushort) p.X, (ushort) p.Y, obj2.MapX, obj2.MapY);
        }

        public bool SampleElevation(int distance, ushort startX, ushort startY, int deltaX, int deltaY, short elevation)
        {
            // Initialize variables and sample the area between the start and final position.
            List<DynamicNpc> mapThings = CollectMapThing(distance, new Point(startX, startY)).Where(x => x is DynamicNpc npc && npc.Type == 26).Cast<DynamicNpc>().ToList();
            int violations = 0;
            for (int index = 1; index <= distance; index++)
            {
                int x = startX + ((int)(((double)(index * deltaX)) / distance));
                int y = startY + ((int)(((double)(index * deltaY)) / distance));
                if (!Calculations.WithinElevation(this[x, y].Elevation, elevation) && this[startX, startY].Elevation < this[x, y].Elevation)
                    if (++violations > 2)
                    {
                        return false;
                    }

                if (mapThings?.Count > 0)
                {
                    foreach (var npc in mapThings)
                    {
                        switch (npc.Lookface)
                        {
                            case 241: // left gate
                            case 251: // left gate
                                for (int i = 1; i < 7; i++)
                                {
                                    if (x == npc.MapX + i && y == npc.MapY)
                                    {
                                        return false;
                                    }
                                }
                                break;
                            case 277: // right gate
                                for (int i = 1; i < 7; i++)
                                {
                                    if (x == npc.MapX && y == npc.MapY + i)
                                    {
                                        return false;
                                    }
                                }
                                break;
                        }
                    }
                }
            }
            return true;
        }

        #endregion

        #region Flags

        public byte Flag { get; set; }
        public uint OwnerIdentity
        {
            get => m_dbDynaMap?.OwnerId ?? m_dbMap?.OwnerId ?? 0u;
            set
            {
                if (IsDynamicMap())
                    m_dbDynaMap.OwnerId = value;
                else m_dbMap.OwnerId = value;
                Save();
            }
        }

        public void SetStatus(byte value, bool flag)
        {
            ulong oldFlag = Flag;
            if (flag)
                Flag |= value;
            else
                Flag &= (byte) ~value;

            if (Flag != oldFlag)
                SendToMap(new MsgMapInfo(Identity, MapDoc, Flag));
        }

        #endregion

        #region Regions

        public bool QueryRegion(int regionType, ushort x, ushort y)
        {
            return Regions.Values
                .Where(re => x > re.BoundX && x < re.BoundX + re.BoundCX && y > re.BoundY && y < re.BoundY + re.BoundCY)
                .Any(region => region.Type == regionType);
        }

        #endregion

        #region Item Positions

        public bool IsLayItemEnable(int x, int y)
        {
            return this[x, y].Access > TileType.Terrain &&
                   RoleSet.Values.FirstOrDefault(a => a.MapX == x && a.MapY == y) == null;
        }

        public List<MapItem> CollectMapItem(ref Point pPos, int nRange)
        {
            var list = new List<MapItem>();
            foreach (var item in RoleSet.Values.Where(x => x is MapItem).Cast<MapItem>())
                if (Calculations.GetDistance((ushort) pPos.X, (ushort) pPos.Y, item.MapX, item.MapY) <= nRange)
                {
                    list.Add(item);
                }

            return list;
        }

        public bool FindDropItemCell(int nRange, ref Point pPos)
        {
            int nSize = nRange * 2 +
                        1; // set the size, remember, if value is 2, it's 2 for each side, that's why we multiply
            int nBufSize = nSize ^ 2;

            if (IsLayItemEnable(pPos.X, pPos.Y))
            {
                return true;
            }

            for (int i = 0; i < 8; i++)
            {
                int newX = pPos.X + ScreenObject.DeltaX[i];
                int newY = pPos.Y + ScreenObject.DeltaY[i];
                if (IsLayItemEnable(newX, newY))
                {
                    pPos.X = newX;
                    pPos.Y = newY;
                    return true;
                }
            }

            //for (int i = 0; i < 24; i++)
            //{
            //    int newX = pPos.X + Handlers.DELTA_WALK_X_COORDS[i];
            //    int newY = pPos.Y + Handlers.DELTA_WALK_Y_COORDS[i];
            //    if (IsLayItemEnable(newX, newY))
            //    {
            //        pPos.X = newX;
            //        pPos.Y = newY;
            //        return true;
            //    }
            //}

            int nIndex = Calculations.Random.Next(nBufSize);
            var posTest = new Point();
            int nLeft = pPos.X - nRange;
            int nTop = pPos.Y - nRange;
            posTest.X = nLeft + Index2X(nIndex, nSize, nSize);
            posTest.Y = nTop + Index2Y(nIndex, nSize, nSize);
            if (IsLayItemEnable(posTest.X, posTest.Y))
            {
                pPos = posTest;
                return true;
            }

            if (nRange < 2)
                return false;

            var setItem = CollectMapItem(ref pPos, nRange);

            int nMinRange = nRange + 1;
            bool ret = false;
            var posFree = new Point();
            for (int i = Math.Max(pPos.X - nRange, 0); i <= pPos.X + nRange && i < Width; i++)
            {
                for (int j = Math.Max(pPos.Y - nRange, 0); j <= pPos.Y + nRange && j < Height; j++)
                {
                    int idx = Pos2Index(i - (pPos.X - nRange), j - (pPos.Y - nRange), nSize, nSize);

                    if (idx >= 0 && idx < nBufSize)
                        if (setItem.FirstOrDefault(x =>
                                Pos2Index(x.MapX - i + nRange, x.MapY - j + nRange, nRange, nRange) == idx) != null)
                            continue;

                    if (IsLayItemEnable(pPos.X, pPos.Y))
                    {
                        double nDistance =
                            Calculations.GetDistance((ushort) i, (ushort) j, (ushort) pPos.X, (ushort) pPos.Y);
                        if (nDistance < nMinRange)
                        {
                            nMinRange = (int) nDistance;
                            posFree.X = i;
                            posFree.Y = j;
                            ret = true;
                        }
                    }
                }
            }

            if (ret)
            {
                pPos = posFree;
                return true;
            }

            return true;
        }

        #endregion

        #region Role Management

        public bool EnterRoom(ScreenObject obj)
        {
            if (!Loaded) Load();
            if (obj is Character user) // the screen system will handle the user entering screen
                return PlayerSet.TryAdd(user.Identity, user);

            // objects must be spawned
            foreach (var player in PlayerSet.Values)
            {
                if (Calculations.InScreen(player.MapX, player.MapY, obj.MapX, obj.MapY))
                {
                    player.Screen.Add(obj);
                    obj.SendSpawnTo(player);
                }
            }

            return RoleSet.TryAdd(obj.Identity, obj);
        }

        public bool LeaveRoom(uint idObj)
        {
            Character user = null;
            ScreenObject role = null;
            if (!PlayerSet.TryRemove(idObj, out user) && !RoleSet.TryRemove(idObj, out role))
                return false;

            if (user != null)
            {
                user.Screen.RemoveFromObservers();
                return true;
            }

            if (role != null)
            {
                foreach (var plr in PlayerSet.Values)
                    plr.Screen.Delete(role.Identity);
                return true;
            }

            return false;
        }

        public ScreenObject QueryRole(int x, int y)
        {
            Role result = PlayerSet.Values.FirstOrDefault(a => a.MapX == x && a.MapY == y);
            return result ?? RoleSet.Values.FirstOrDefault(a => a.MapX == x && a.MapY == y);
        }

        public ScreenObject QueryRole(uint idRole)
        {
            if (idRole > IdentityRange.PLAYER_ID_FIRST && idRole < IdentityRange.PLAYER_ID_LAST)
                return PlayerSet.TryGetValue(idRole, out Character user) ? user : null;
            return RoleSet.TryGetValue(idRole, out var role) ? role : null;
        }

        public List<Role> CollectMapThing(int nRange, Point pPos)
        {
            var pTemp = RoleSet.Values.OfType<Role>().Where(obj => Calculations.GetDistance((ushort)pPos.X, (ushort)pPos.Y, obj.MapX, obj.MapY) <= nRange).ToList();
            pTemp.AddRange(from obja in PlayerSet.Values select obja into obj where Calculations.GetDistance((ushort)pPos.X, (ushort)pPos.Y, obj.MapX, obj.MapY) <= nRange select obj as Role);
            return pTemp;
        }

        public List<Character> CollectMapUser(int nRange, Point pPos)
        {
            return PlayerSet.Values.Where(x => Calculations.GetDistance((ushort) pPos.X, (ushort) pPos.Y, x.MapX, x.MapY) <= nRange).ToList();
        }

        #endregion

        #region Network

        public void SendMapInfo(Character user)
        {
            if (Weather.GetType() != Weather.WeatherType.WeatherNone)
                Weather.SendWeather(user);
            else
                Weather.SendNoWeather(user);

            if (Light != 0)
                SendToMap(new MsgAction(1, Light, 0, 0, GeneralActionType.MapArgb));

            user?.Send(new MsgMapInfo(Identity, MapDoc, Type));
        }

        #endregion

        #region Communication

        public void SendMessageToMap(string msg, ChatTone tone)
        {
            foreach (var plr in PlayerSet.Values)
                plr.Send(new MsgTalk(msg, tone));
        }

        public void SendToMap(byte[] msg)
        {
            foreach (var plr in PlayerSet
                .Values)
                plr.Send(msg);
        }

        public void SendToRegion(byte[] msg, int x, int y, int distance = Calculations.SCREEN_DISTANCE)
        {
            foreach (var plr in PlayerSet.Values)
            {
                if (plr.GetDistance(x, y) <= distance)
                    plr.Send(msg);
            }
        }

        #endregion

        #region Database

        public static string FindMapPath(uint idDoc)
        {
            return ServerKernel.MapPathData.TryGetValue(idDoc, out string value) ? value : string.Empty;
        }

        public bool Save()
        {
            return IsDynamicMap() ? new DynamicMapRepository().Save(m_dbDynaMap) : new MapRepository().Save(m_dbMap);
        }

        public bool Delete()
        {
            return IsDynamicMap() && new DynamicMapRepository().Delete(m_dbDynaMap);
        }

        #endregion

        public void OnTimer()
        {

        }
    }
}