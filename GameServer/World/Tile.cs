﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Tile.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Runtime.InteropServices;

#endregion

namespace GameServer.World
{
    /// <summary>
    /// This structure encapsulates a tile from the floor's coordinate grid. It contains the tile access information
    /// and the elevation of the tile. The map's coordinate grid is composed of these tiles. The tile structure
    /// is not optimized by C#, and thus takes up 48 bits of memory (or 6 bytes).
    /// </summary>
    [StructLayout(LayoutKind.Sequential, Pack = 2, Size = 12)]
    public struct Tile
    {
        public TileType Access; // The access type for processing the tile.
        public short Elevation; // The elevation of the tile on the map.
        public int Index;
        public ushort X;
        public ushort Y;

        /// <summary>
        /// This structure encapsulates a tile from the floor's coordinate grid. It contains the tile access information
        /// and the elevation of the tile. The map's coordinate grid is composed of these tiles. The tile structure
        /// is not optimized by C#, and thus takes up 24 bits of memory (or 3 bytes).
        /// </summary>
        /// <param name="type">The access type for processing the tile.</param>
        /// <param name="elevation">The elevation of the tile on the map.</param>
        /// <param name="index">The index of a portal.</param>
        public Tile(TileType type, short elevation, ushort x, ushort y, int index = -1)
        {
            Access = type;
            Elevation = elevation;
            if (type == TileType.Portal)
                Index = index;
            else
                Index = -1;

            X = x;
            Y = y;
        }
    }

    /// <summary> This enumeration type defines the access types for tiles. </summary>
    public enum TileType : byte
    {
        Terrain,
        Npc,
        Monster,
        Portal,
        Item,
        MarketSpot,
        Available
    }

    /// <summary> This enumeration type defines the types of scenery files used by the client. </summary>
    public enum SceneryType
    {
        SceneryObject = 1,
        DdsCover = 4,
        Effect = 10,
        Sound = 15
    }
}