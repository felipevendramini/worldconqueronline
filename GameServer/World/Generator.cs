﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Generator.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Concurrent;
using System.Drawing;
using System.Linq;
using FtwCore.Common;
using FtwCore.Database.Entities;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.World
{
    public class Generator
    {
        private static uint m_idGenerator = 2000000;

        private const int _MAX_PER_GEN = 50;
        private const int _MIN_TIME_BETWEEN_GEN = 10;

        private GeneratorEntity m_dbGen;
        private MonstertypeEntity m_dbMonster;
        private Point m_pCenter;
        private Map m_pMap;
        private TimeOut m_pTimer;
        private Random m_pRandom = new Random();
        private Monster m_pDemo;

        private bool m_bIsDynamic;

        public ConcurrentDictionary<uint, Monster> Collection;

        public Generator(GeneratorEntity dbGen)
        {
            m_dbGen = dbGen;
        }

        public Generator(uint idMap, uint idMonster, ushort usX, ushort usY, ushort usCx, ushort usCy)
        {
            m_dbGen = new GeneratorEntity
            {
                Mapid = idMap,
                BoundX = usX,
                BoundY = usY,
                BoundCx = usCx,
                BoundCy = usCy,
                Npctype = idMonster,
                MaxNpc = 0,
                MaxPerGen = 0,
                Id = m_idGenerator++
            };

            if (!ServerKernel.Maps.TryGetValue(m_dbGen.Mapid, out m_pMap))
            {
                ServerKernel.Log.SaveLog($"Could not load map ({m_dbGen.Mapid}) for generator ({m_dbGen.Id})");
                return;
            }

            if (!ServerKernel.Monsters.TryGetValue(m_dbGen.Npctype, out m_dbMonster))
            {
                ServerKernel.Log.SaveLog($"Could not load monster ({m_dbGen.Npctype}) for generator ({m_dbGen.Id})");
                return;
            }

            m_pCenter = new Point(m_dbGen.BoundX + m_dbGen.BoundCx / 2, m_dbGen.BoundY + m_dbGen.BoundCy / 2);
            m_bIsDynamic = true;
            Collection = new ConcurrentDictionary<uint, Monster>();
            FirstGeneration();
        }

        public uint Identity => m_dbGen.Id;

        public uint RoleType => m_dbGen.Npctype;

        public int RestSeconds => m_dbGen.RestSecs;

        public uint MapIdentity => m_dbGen.Mapid;

        public string MonsterName => m_dbMonster.Name;

        public bool Create()
        {
            if (!ServerKernel.Maps.TryGetValue(m_dbGen.Mapid, out m_pMap))
            {
                Program.WriteLog($"Could not load map ({m_dbGen.Mapid}) for generator ({m_dbGen.Id})");
                return false;
            }

            if (!ServerKernel.Monsters.TryGetValue(m_dbGen.Npctype, out m_dbMonster))
            {
                Program.WriteLog($"Could not load monster ({m_dbGen.Npctype}) for generator ({m_dbGen.Id})");
                return false;
            }

            if (!m_pMap.Loaded)
                m_pMap.Load();

            m_dbGen.RestSecs = Math.Max(_MIN_TIME_BETWEEN_GEN, m_dbGen.RestSecs);
            m_pCenter = new Point(m_dbGen.BoundX + m_dbGen.BoundCx / 2, m_dbGen.BoundY + m_dbGen.BoundCy / 2);

            m_pTimer = new TimeOut(m_dbGen.RestSecs);
            Collection = new ConcurrentDictionary<uint, Monster>();
            return true;
        }

        private bool Monster(out Monster mob)
        {
            mob = default(Monster);
            Point pos = new Point(-1, -1);
            try
            {
                for (int i = 0; i < 10; i++)
                {
                    if (!NewPoint(out pos))
                    {
                        if (i >= 10)
                            return false;
                    }
                    else break;
                }

                if (pos == default(Point) || pos.X == -1 || pos.Y == -1 || pos.X == 0 || pos.Y == 0)
                    return false;

                ushort x = (ushort)pos.X, y = (ushort)pos.Y;

                mob = new Monster(m_dbMonster, (uint) ServerKernel.RoleManager.MonsterIdentity.GetNextIdentity, this);
                return mob.Initialize(MapIdentity, x, y);
            }
            catch
            {
                return false;
            }
        }

        private bool NewPoint(out Point pPos)
        {
            pPos = default;
            try
            {
                ushort x = (ushort) Math.Max(m_dbGen.BoundX, m_pRandom.Next(m_dbGen.BoundX, m_dbGen.BoundX + m_dbGen.BoundCx));
                ushort y = (ushort) Math.Max(m_dbGen.BoundY, m_pRandom.Next(m_dbGen.BoundY, m_dbGen.BoundY + m_dbGen.BoundCy));

                if (!m_pMap.IsStandEnable(x, y) && !m_pMap.IsSuperPosition(x, y) && !m_pDemo.IsGuard())
                    return false;

                pPos = new Point(x, y);
                return true;
            }
            catch
            {
                pPos = default;
                return false;
            }
        }

        public void FirstGeneration()
        {
            m_pTimer = new TimeOut(m_dbGen.RestSecs);
            m_pTimer.Startup(m_dbGen.RestSecs);
            m_pDemo = new Monster(m_dbMonster, 0, this);

            for (int i = 0; i < m_dbGen.MaxPerGen; i++)
            {
                if (Monster(out var pRole))
                {
                    if (Collection.Count >= m_dbGen.MaxPerGen || !Collection.TryAdd(pRole.Identity, pRole))
                        return;
                    ServerKernel.RoleManager.AddRole(pRole);
                }
            }
        }

        public void OnTimer()
        {
            try
            {
                foreach (var mob in Collection.Values.Where(x => !x.IsAlive || x.DisappearNow))
                {
                    if (mob.IsDisappear() || mob.DisappearNow)
                    {
                        ServerKernel.RoleManager.RemoveRole(mob.Identity);
                        Collection.TryRemove(mob.Identity, out _);
                    }
                }

                if (m_pTimer.ToNextTime(m_dbGen.RestSecs))
                {
                    if (Collection.Count < m_dbGen.MaxPerGen) // spawn new mobs
                    {
                        int gen = m_dbGen.MaxPerGen - Collection.Count;
                        if (gen > _MAX_PER_GEN)
                            gen = _MAX_PER_GEN;

                        for (int i = 0; i < gen; i++)
                        {
                            if (!Monster(out var pRole))
                                continue;

                            if (!Collection.TryAdd(pRole.Identity, pRole) || !ServerKernel.RoleManager.AddRole(pRole))
                            {
                                Program.WriteLog($"Could not add monster {pRole.Identity} {pRole.Name} {pRole.MapX},{pRole.MapY}:{pRole.MapIdentity}");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
            }
        }

        public Point GetCenter()
        {
            return m_pCenter;
        }

        public bool IsTooFar(ushort x, ushort y, int nRange)
        {
            return !(x >= m_dbGen.BoundX - nRange
                     && x < m_dbGen.BoundX + m_dbGen.BoundCx + nRange
                     && y >= m_dbGen.BoundY - nRange
                     && y < m_dbGen.BoundY + m_dbGen.BoundCy + nRange);
        }

        public bool IsInRegion(int x, int y)
        {
            return x >= m_dbGen.BoundX && x < m_dbGen.BoundX + m_dbGen.BoundCx
                                       && y >= m_dbGen.BoundY && y < m_dbGen.BoundY + m_dbGen.BoundCy;
        }

        public int GetWidth()
        {
            return m_dbGen.BoundCx;
        }

        public int GetHeight()
        {
            return m_dbGen.BoundCy;
        }

        public int GetPosX()
        {
            return m_dbGen.BoundX;
        }

        public int GetPosY()
        {
            return m_dbGen.BoundY;
        }
    }
}