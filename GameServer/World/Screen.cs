﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Screen.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.World
{
    /// <summary>
    /// This class encapsulates the client's screen system. It handles screen objects that the player can currently
    /// see in the client window as they enter, move, and leave the screen. It controls the distribution of packets
    /// to the other players in the screen and adding new objects as the character (the actor) moves. 
    /// </summary>
    public sealed class Screen
    {
        // Ownership variable declaration
        private Character m_pOwner;

        private ConcurrentDictionary<uint, ScreenObject> m_pPlayers = new ConcurrentDictionary<uint, ScreenObject>();
        private ConcurrentDictionary<uint, ScreenObject> m_pRoles = new ConcurrentDictionary<uint, ScreenObject>();

        /// <summary>
        /// This class encapsulates the client's screen system. It handles screen objects that the player can currently
        /// see in the client window as they enter, move, and leave the screen. It controls the distribution of packets
        /// to the other players in the screen and adding new objects as the character (the actor) moves. 
        /// </summary>
        /// <param name="pOwner">The owner of the screen.</param>
        public Screen(Character pOwner)
        {
            m_pOwner = pOwner;
        }

        /// <summary>
        /// This method adds the screen object specified in the parameter arguments to the owner's screen. If the 
        /// object already exists in the screen, it will not be added and this method will return false. If the
        /// screen object is being added, and the object is of type character, then the owner will be added to the
        /// observer's screen as well. 
        /// </summary>
        /// <param name="pObj">The screen object being added to the owner's screen.</param>
        public bool Add(ScreenObject pObj)
        {
            if (pObj is Character character)
                return m_pPlayers.TryAdd(character.Identity, character);
            return m_pRoles.TryAdd(pObj.Identity, pObj);
        }

        /// <summary>
        /// This method checks if the screen object specified in the parameter arguments is inserted into the owner
        /// screen.
        /// </summary>
        /// <param name="dwIdentity">The unique identifier of the object.</param>
        public bool Contains(uint dwIdentity)
        {
            return m_pPlayers.ContainsKey(dwIdentity) || m_pRoles.ContainsKey(dwIdentity);
        }

        /// <summary>
        /// This method deletes a screen object from the owner's screen. It uses the entity removal subtype from
        /// the general action packet to forcefully remove the entity from the owner's screen. It returns false if
        /// the character was never in the owner's screen to begin with.
        /// </summary>
        /// <param name="dwIdentity">The identity of the screen object.</param>
        public bool Delete(uint dwIdentity)
        {
            if (m_pPlayers.TryRemove(dwIdentity, out _)
                || m_pRoles.TryRemove(dwIdentity, out _))
            {
                m_pOwner.Send(new MsgAction(dwIdentity, 0, 0, GeneralActionType.RemoveEntity));
                return true;
            }

            return false;
        }

        /// <summary>
        /// This method removes a screen object from the owner's screen without using force. It will not remove the
        /// spawn. This method is used for characters who are actively removing themselves out of the screen.
        /// </summary>
        /// <param name="dwIdentity">The identity of the actor.</param>
        public bool Remove(uint dwIdentity)
        {
            if (m_pPlayers.TryRemove(dwIdentity, out _)
                || m_pRoles.TryRemove(dwIdentity, out _))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// This method removes the owner from all observers. It makes use of the delete method (general action 
        /// subtype packet) to forcefully remove the owner from each screen within the owner's screen distance.
        /// </summary>
        public void RemoveFromObservers()
        {
            foreach (var obs in m_pPlayers.Values)
            {
                var pPlayer = obs as Character;
                if (pPlayer == null) continue;
                pPlayer.Screen.Delete(m_pOwner.Identity);
                Remove(obs.Identity);
            }

            foreach (var obs in m_pOwner.Map.PlayerSet.Values.Where(x => m_pOwner.GetDistance(x.MapX, x.MapY) <= 21))
            {
                obs.Send(new MsgAction(m_pOwner.Identity, 0, 0, GeneralActionType.RemoveEntity));
            }
        }

        /// <summary>
        /// This method removes the owner from all observers. It makes use of the delete method (general action 
        /// subtype packet) to forcefully remove the owner from each screen within the owner's screen distance.
        /// It then respawns the character in the observers' screens.
        /// </summary>
        public void RefreshSpawnForObservers()
        {
            foreach (var observer in m_pPlayers.Values.Where(x => x is Character).Cast<Character>())
            {
                observer.Screen.Delete(m_pOwner.Identity);
                Remove(observer.Identity);
                Add(observer);
                m_pOwner.SendSpawnTo(observer);
            }
        }

        public void LoadSurroundings()
        {
            #region Spawn Players

            RemoveFromObservers();
            m_pRoles.Clear();

            foreach (var pPlayer in m_pOwner.Map.PlayerSet.Values.Where(x => x.Identity != m_pOwner.Identity))
            {
                if (Calculations.InScreen(pPlayer.MapX, pPlayer.MapY, m_pOwner.MapX, m_pOwner.MapY))
                    if (Add(pPlayer) && pPlayer.Screen.Add(m_pOwner))
                        m_pOwner.ExchangeSpawnPackets(pPlayer);
            }

            #endregion

            #region Spawn Objects (NPCs, Items)

            foreach (var pObj in m_pOwner.Map.RoleSet.Values)
            {
                if (Calculations.InScreen(pObj.MapX, pObj.MapY, m_pOwner.MapX, m_pOwner.MapY) && Add(pObj))
                {
                    pObj.SendSpawnTo(m_pOwner);
                }
            }

            #endregion
        }

        public void SendMovement(byte[] pMsg)
        {
            if (m_pOwner.IsArenaWitness)
                return;

            // For each possible observer on the map
            foreach (var pPlayer in m_pOwner.Map.PlayerSet.Values.Where(x => x.Identity != m_pOwner.Identity))
            {
                // If the character is in screen, make sure it's in the owner's screen
                if (Calculations.InScreen(pPlayer.MapX, pPlayer.MapY, m_pOwner.MapX, m_pOwner.MapY))
                {
                    // Check if the user is already added to the owner's screen
                    if (Add(pPlayer) && pPlayer.Screen.Add(m_pOwner))
                        m_pOwner.ExchangeSpawnPackets(pPlayer);

                    pPlayer.Send(pMsg);
                }
                // Else, remove the entity from the screens and send the last packet
                else
                {
                    if (Contains(pPlayer.Identity))
                    {
                        Remove(pPlayer.Identity);
                        pPlayer.Screen.Remove(m_pOwner.Identity);

                        if (pPlayer.FetchTradeRequest(m_pOwner.Identity))
                            pPlayer.ClearTradeRequest();
                        if (m_pOwner.FetchTradeRequest(pPlayer.Identity))
                            m_pOwner.ClearTradeRequest();
                        if (m_pOwner.Trade != null && m_pOwner.Trade.IsTrading(m_pOwner, pPlayer))
                            m_pOwner.Trade.CloseWindow();

                        pPlayer.Send(pMsg);
                    }
                    else if (m_pOwner.GetDistance(pPlayer.MapX, pPlayer.MapY) <= 21)
                    {
                        pPlayer.Send(pMsg);
                    }
                }
            }

            foreach (var obj in m_pOwner.Map.RoleSet.Values)
            {
                // If the character is in screen, make sure it's in the owner's screen
                if (Calculations.InScreen(obj.MapX, obj.MapY, m_pOwner.MapX, m_pOwner.MapY))
                {
                    // Check if the user is already added to the owner's screen
                    if (Add(obj))
                        obj.SendSpawnTo(m_pOwner);
                }
                // Else, remove the entity from the screens and send the last packet
                else // if (Contains(obj.Identity))
                {
                    if (Contains(obj.Identity))
                    {
                        Remove(obj.Identity);
                    }
                }
            }
        }

        /// <summary>
        /// This method clears the owner's screen trackers. It does not remove the objects from the screen. The 
        /// delete method is required to delete screen objects.
        /// </summary>
        public void Clear()
        {
            m_pPlayers = new ConcurrentDictionary<uint, ScreenObject>();
            m_pRoles = new ConcurrentDictionary<uint, ScreenObject>();
        }

        public void Send(string szMsg, bool bSelf, ChatTone pTone = ChatTone.TopLeft)
        {
            var pMsg = new MsgTalk(szMsg, pTone);
            if (bSelf) m_pOwner.Send(pMsg);
            foreach (var pRole in m_pPlayers.Values.Cast<Character>()) pRole.Send(pMsg);
        }

        public void Send(byte[] pMsg, bool bSelf)
        {
            if (m_pOwner != null && bSelf) m_pOwner.Send(pMsg);
            foreach (var role in m_pPlayers.Values.Cast<Character>()) role.Send(pMsg);
        }

        public void SendDeadMessage(byte[] pMsg)
        {
            foreach (var player in m_pPlayers.Values.Cast<Character>())
            {
                if (player.ProfessionSort >= 10 || player.ProfessionSort == 6 || !player.IsAlive)
                    player.Send(pMsg);
            }
        }

        public List<Role> GetAroundRoles
        {
            get
            {
                List<Role> roles = new List<Role>();
                foreach (var plr in m_pPlayers.Values)
                    roles.Add(plr as Role);
                foreach (var ai in m_pRoles.Values)
                    roles.Add(ai as Role);
                return roles;
            }
        }

        public List<ScreenObject> GetAroundScreenObjects
        {
            get
            {
                List<ScreenObject> roles = new List<ScreenObject>();
                foreach (var plr in m_pPlayers.Values)
                    roles.Add(plr);
                foreach (var ai in m_pRoles.Values)
                    roles.Add(ai);
                return roles;
            }
        }
    }
}