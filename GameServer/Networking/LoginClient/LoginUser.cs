﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - LoginUser.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Net.Sockets;
using FtwCore.Common.Enums;
using FtwCore.Interfaces;
using FtwCore.Networking.Sockets;

#endregion

namespace GameServer.Networking.LoginClient
{
    public sealed class LoginUser : Passport
    {
        private AsynchronousClientSocket m_socket;

        public LoginUser(AsynchronousClientSocket server, Socket socket, ICipher cipher)
            : base(server, socket, cipher)
        {
            m_socket = server;
        }

        public new void Send(byte[] msg)
        {
            ServerKernel.Analytics.IncrementSentPackets();
            ServerKernel.Analytics.IncrementSentBytes(msg.Length);
            base.Send(msg);
        }

        public InterServerState ConnectionState { get; set; }
    }
}