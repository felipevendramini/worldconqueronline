﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Login Server.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Net.Sockets;
using FtwCore.Common;
using FtwCore.Networking.Packets;
using FtwCore.Networking.Sockets;
using GameServer.Networking.GameServer;

#endregion

namespace GameServer.Networking.LoginClient
{
    public sealed class LoginSocket : AsynchronousClientSocket
    {
        PacketProcessor<PacketHandlerType, PacketType, Action<LoginUser, byte[]>> m_pProcessor;

        public LoginSocket()
            : base("", AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
        {
            OnClientConnect = Connect;
            OnClientReceive = Receive;
            OnClientDisconnect = Disconnect;

            m_pProcessor =
                new PacketProcessor<PacketHandlerType, PacketType, Action<LoginUser, byte[]>>(
                    new LoginPacketHandlers());
        }

        /// <summary>
        /// This method is invoked when the client has been approved of connecting to the server. The client should
        /// be constructed in this method, and cipher algorithms should be initialized. If any packets need to be
        /// sent in the connection state, they should be sent here.
        /// </summary>
        /// <param name="pState">Represents the status of an asynchronous operation.</param>
        public void Connect(AsynchronousState pState)
        {
            var pServer = new LoginUser(this, pState.Socket, null);
            pState.Client = pServer;
        }

        /// <summary>
        /// This method is invoked when the client has data ready to be processed by the server. The server will
        /// switch between the packet type to find an appropriate function for processing the packet. If the
        /// packet was not found, the packet will be outputted to the console as an error.
        /// </summary>
        /// <param name="pState">Represents the status of an asynchronous operation.</param>
        public void Receive(AsynchronousState pState)
        {
            var pServer = pState.Client as LoginUser;
            if (pServer?.Packet != null)
            {
                ServerKernel.Analytics.IncrementRecvPackets();
                ServerKernel.Analytics.IncrementRecvBytes(pServer.Packet.Length);

                var type = (PacketType) BitConverter.ToUInt16(pServer.Packet, 2);
                Action<LoginUser, byte[]> action = m_pProcessor[type];

                // Process the client's packet:
                if (action != null) action(pServer, pServer.Packet);
                else GamePacketHandler.Report(pServer.Packet);
            }
        }

        /// <summary>
        /// This method is invoked when the client is disconnecting from the server. It disconnects the client
        /// from the map server and disposes of game structures. Upon disconnecting from the map server, the
        /// character will be removed from the map and screen actions will be terminated. Then, character
        /// data will be disposed of.
        /// </summary>
        /// <param name="pState">Represents the status of an asynchronous operation.</param>
        public void Disconnect(object pState)
        {
            var pObj = pState as LoginUser;
            if (pObj == null)
            {
                // something went wrong
                return;
            }

            Program.WriteLog("Disconnected from the account server...", LogType.WARNING);
            ServerKernel.LoginServer = null;
        }
    }
}