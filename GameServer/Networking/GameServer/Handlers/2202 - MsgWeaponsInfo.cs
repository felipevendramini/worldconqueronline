﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 2202 - MsgWeaponsInfo.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Linq;
using FtwCore.Common.Enums;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgWeaponsInfo(Character pUser, MsgWeaponsInfo pMsg)
        {
            if (pUser.Syndicate == null)
                return;

            TotemPoleType pType = (TotemPoleType) pMsg.ArsenalType;
            if (pType == TotemPoleType.TOTEM_NONE)
                pType = TotemPoleType.TOTEM_HEADGEAR;

            if (!pUser.Syndicate.Arsenal.Poles.TryGetValue(pType, out var pPole))
                return;

            uint beginAt = pMsg.BeginAt - 1;
            uint length = (uint) pPole.Items.Count;

            length = Math.Min(length, 8);
            int nAmount = 0;
            foreach (var item in pPole.Items.Values.OrderByDescending(x => x.Donation()))
            {
                if (nAmount < beginAt)
                {
                    nAmount++;
                    continue;
                }

                if (nAmount > beginAt + length)
                    break;
                pMsg.AppendItem(item.Identity, (uint) nAmount, item.PlayerName, item.Itemtype,
                    (byte) (item.Itemtype % 10),
                    item.Item.Addition, (byte) item.Item.SocketOne, (byte) item.Item.SocketTwo,
                    (uint) item.Item.BattlePower,
                    item.Donation());
                nAmount++;
            }

            pMsg.EndAt = length + pMsg.BeginAt - 1;
            pMsg.Donation = (uint) pPole.Donation;
            pMsg.Enchantment = 0; // todo
            pMsg.EnchantmentExpire = 0; // todo
            pMsg.SharedBattlePower = pPole.BattlePower;
            pMsg.TotalInscribed = (uint) pPole.Items.Count;
            pUser.Send(pMsg);
        }
    }
}