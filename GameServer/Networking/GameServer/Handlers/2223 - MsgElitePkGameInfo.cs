﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 2223 - MsgElitePkGameInfo.cs
// Last Edit: 2020/01/16 21:49
// Created: 2020/01/16 21:48
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgElitePkGameInfo(Character user, MsgElitePKGameRankInfo msg)
        {
            switch (msg.Type)
            {
                case 0:
                {
                    user.Send(msg);
                    break;
                }
                default:
                {

                    break;
                }
            }
        }
    }
}