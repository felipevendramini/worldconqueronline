﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 2110 - MsgSuperFlag.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Items;
using GameServer.World;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleSuperFlag(Character pUser, MsgSuperFlag pMsg)
        {
            switch (pMsg.Action)
            {
                case 1: // save location
                {
                    if (pUser.Map.IsDynamicMap())
                        return;
                    if (pUser.Map.IsRecordDisable())
                        return;
                    if (pUser.Map.IsPkField())
                        return;
                    if (pUser.Map.IsBoothEnable())
                        return;
                    if (pUser.Map.IsFamilyMap() || pUser.Map.IsSynMap())
                        return;
                    if (pUser.Map.IsPrisionMap() || pUser.Map.IsMineField())
                        return;
                    if (pUser.Map.Identity == 2055 || pUser.Map.Identity == 2056)
                        return;
                    Item item;
                    if ((item = pUser.UserPackage[pMsg.ItemIdentity]) != null)
                    {
                        if (pMsg.CarryIdentity < item.CarryCount)
                        {
                            item.UpdateLocation((int) pMsg.CarryIdentity);
                        }
                        else
                        {
                            if (item.CarryCount >= Carry.MAX_RECORDS)
                                return;
                            item.SaveLocation();
                        }
                    }

                    break;
                }

                case 2: // change name
                {
                    if (pMsg.Name.Length <= 0 || !CheckName(pMsg.Name))
                        return;
                    string name = pMsg.Name.Length > 32 ? pMsg.Name.Substring(0, 32) : pMsg.Name;
                    Item item;
                    if ((item = pUser.UserPackage[pMsg.ItemIdentity]) != null)
                    {
                        item.CarrySetName(name, (int) pMsg.CarryIdentity);
                    }

                    break;
                }

                case 3: // goto location
                {
                    if (pUser.Map.IsPrisionMap() || pUser.Map.IsChgMapDisable())
                        return;
                    Item item;
                    if ((item = pUser.UserPackage[pMsg.ItemIdentity]) != null)
                    {
                        CarryEntity carry;
                        if (!item.GetTeleport(pMsg.CarryIdentity, out carry))
                            return;
                        Map map;
                        if (!ServerKernel.Maps.TryGetValue(carry.RecordMapId, out map))
                            return;

                        if (map.IsDynamicMap() || pUser.Map.IsMineField())
                            return;
                        if (map.IsRecordDisable())
                            return;
                        if (map.IsPkField())
                            return;
                        if (map.IsBoothEnable())
                            return;
                        if (map.IsFamilyMap() || map.IsSynMap())
                            return;
                        if (map.IsPrisionMap())
                            return;
                        if (item.Durability <= 0)
                            return;
                        item.Durability -= 1;
                        item.SendCarry();
                        item.CreateMsgItemInfo(ItemMode.Update);
                        pUser.FlyMap(carry.RecordMapId, carry.RecordMapX, carry.RecordMapY);
                    }

                    break;
                }

                case 4: // refill
                {
                    Item item;
                    if ((item = pUser.UserPackage[pMsg.ItemIdentity]) != null)
                    {
                        int nCost = Math.Max(1, item.MaxDurability - item.Durability) / 2;
                        if (!pUser.SpendEmoney(nCost, EmoneySourceType.Carry, pUser))
                            return;
                        item.Durability = item.MaxDurability;
                        item.SendCarry();
                    }

                    break;
                }
            }
        }
    }
}