﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 2101 - MsgFactionRankInfo.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.Linq;
using FtwCore.Common.Enums;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Groups.Syndicates;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        private static SyndicateRank[] m_pShowDuty =
        {
            SyndicateRank.Manager,
            SyndicateRank.Supervisor,
            SyndicateRank.SilverSupervisor,
            SyndicateRank.CpSupervisor,
            SyndicateRank.PkSupervisor,
            SyndicateRank.GuideSupervisor,
            SyndicateRank.ArsenalSupervisor,
            SyndicateRank.RoseSupervisor,
            SyndicateRank.LilySupervisor,
            SyndicateRank.OrchidSupervisor,
            SyndicateRank.TulipSupervisor,
            SyndicateRank.Steward,
            SyndicateRank.DeputySteward,
            SyndicateRank.TulipAgent,
            SyndicateRank.OrchidAgent,
            SyndicateRank.CpAgent,
            SyndicateRank.ArsenalAgent,
            SyndicateRank.SilverAgent,
            SyndicateRank.GuideAgent,
            SyndicateRank.PkAgent,
            SyndicateRank.RoseAgent,
            SyndicateRank.LilyAgent,
            SyndicateRank.Agent,
            SyndicateRank.TulipFollower,
            SyndicateRank.OrchidFollower,
            SyndicateRank.CpFollower,
            SyndicateRank.ArsenalFollower,
            SyndicateRank.SilverFollower,
            SyndicateRank.GuideFollower,
            SyndicateRank.PkFollower,
            SyndicateRank.RoseFollower,
            SyndicateRank.LilyFollower,
            SyndicateRank.Follower,
            SyndicateRank.SeniorMember
        };

        public static void HandleMsgFactionRankInfo(Character pUser, MsgFactionRankInfo pMsg)
        {
            if (pUser.Syndicate == null || pUser.SyndicateMember == null)
                return;

            Syndicate pSyn = pUser.Syndicate;

            List<SyndicateMember> memberList = new List<SyndicateMember>(10);
            switch (pMsg.Subtype)
            {
                case 0: // silver
                {
                    memberList = pSyn.Members.Values.OrderByDescending(x => x.SilverDonation).ToList();
                    break;
                }

                case 1: // Emoney
                {
                    memberList = pSyn.Members.Values.OrderByDescending(x => x.EmoneyDonation).ToList();
                    break;
                }

                case 2: // guide
                {
                    memberList = pSyn.Members.Values.OrderByDescending(x => x.GuideDonation).ToList();
                    break;
                }

                case 3: // Pk
                {
                    memberList = pSyn.Members.Values.OrderByDescending(x => x.PkDonation).ToList();
                    break;
                }

                case 4: // Arsenal
                {
                    memberList = pSyn.Members.Values.OrderByDescending(x => x.ArsenalDonation).ToList();
                    break;
                }

                case 5: // Rose
                {
                    memberList = pSyn.Members.Values.OrderByDescending(x => x.RedRoseDonation).ToList();
                    break;
                }

                case 6: // orchid
                {
                    memberList = pSyn.Members.Values.OrderByDescending(x => x.OrchidDonation).ToList();
                    break;
                }

                case 7: // lily
                {
                    memberList = pSyn.Members.Values.OrderByDescending(x => x.WhiteRoseDonation).ToList();
                    break;
                }

                case 8: // tulip
                {
                    memberList = pSyn.Members.Values.OrderByDescending(x => x.TulipDonation).ToList();
                    break;
                }

                case 9: // total
                {
                    memberList = pSyn.Members.Values.OrderByDescending(x => x.TotalDonation).ToList();
                    break;
                }
                //case 10: // total donation
                //    {

                //        break;
                //    }
                default:
                    Program.WriteLog($"MsgFactionRankInfo::Subtype {pMsg.Subtype} not handled");
                    break;
            }
            
            uint count = 0;
            foreach (var member in memberList)
                //for (int i = 0; i < memberList.Count; i++)
            {
                //SyndicateMember member = memberList[i];
                pMsg.AddMember(member.Identity, member.Position, count, (int) member.SilverDonation,
                    member.EmoneyDonation, member.PkDonation,
                    member.GuideDonation, member.ArsenalDonation, member.RedRoseDonation, member.WhiteRoseDonation,
                    member.OrchidDonation,
                    member.TulipDonation, member.TotalDonation, member.Name);
                if (count++ > 10)
                    break;
            }

            //pMsg.Subtype = 4;
            pMsg.Count = (ushort) count;
            pMsg.MaxCount = (uint) Math.Min(pSyn.Members.Count, 30);
            pUser.Send(pMsg);
        }
    }
}