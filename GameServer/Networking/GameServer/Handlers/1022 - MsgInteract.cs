﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1022 - MsgInteract.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using FtwCore;
using FtwCore.Common.Enums;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgInteract(Character user, MsgInteract msg)
        {
            if (user?.BattleSystem == null)
                return;

            user.BattleSystem.DestroyAutoAttack();
            user.BattleSystem.ResetBattle();

            if (user.Magics.QueryMagic() != null)
                user.Magics.AbortMagic(false);

            if (!user.IsAlive)
                return;

            Role target = user.BattleSystem.FindRole(msg.TargetIdentity);
            if (target == null
                && msg.Action != InteractionType.ACT_ITR_MAGIC_ATTACK
                && msg.Action != InteractionType.ACT_ITR_SHOOT
                && msg.Action != InteractionType.ACT_ITR_COUNTER_KILL_SWITCH
                && msg.Action != InteractionType.ACT_ITR_PRESENT_EMONEY)
            {
                user.Send(new MsgAction(msg.TargetIdentity, 0, 0, 0, GeneralActionType.RemoveEntity));
                return;
            }

            if (msg.TargetIdentity == 1)
                target = user;

            if (msg.Action != InteractionType.ACT_ITR_PRESENT_EMONEY && target != null &&
                (target.MapIdentity != user.MapIdentity
                 || target.GetDistance(user.MapX, user.MapY) > 18))
            {
                user.Send(new MsgAction(msg.TargetIdentity, 0, 0, 0, GeneralActionType.RemoveEntity));
                return;
            }

            switch (msg.Action)
            {
                case InteractionType.ACT_ITR_ATTACK:
                case InteractionType.ACT_ITR_SHOOT:
                {
                    user.BattleSystem.CreateBattle(msg.TargetIdentity);
                    user.BattleSystem.SetAutoAttack();
                    user.SetAttackTarget(target);
                    break;
                }

                case InteractionType.ACT_ITR_COURT:
                {
                    if (msg.TargetIdentity == user.Identity)
                        return;

                    Character targetUser = ServerKernel.UserManager.GetUser(msg.TargetIdentity);
                    if (targetUser == null)
                    {
                        user.SendSysMessage(Language.StrMarriageNotApply);
                        return;
                    }

                    if (targetUser.Mate != Language.StrNone)
                    {
                        user.SendSysMessage(Language.StrTargetAlreadyMarried);
                        return;
                    }

                    if (user.Mate != Language.StrNone)
                    {
                        user.SendSysMessage(Language.StrYouAlreadyMarried);
                        return;
                    }

                    if (targetUser.Gender == user.Gender)
                    {
                        user.SendSysMessage(Language.StrNowAllowedSameGenderMarriage);
                        return;
                    }

                    targetUser.SetMarryRequest(user.Identity);
                    targetUser.Send(msg);
                    break;
                }

                case InteractionType.ACT_ITR_MARRY:
                {
                    if (msg.TargetIdentity == user.Identity)
                        return;

                    Character targetUser = ServerKernel.UserManager.GetUser(msg.TargetIdentity);
                    if (targetUser == null)
                    {
                        user.SendSysMessage(Language.StrMarriageNotApply);
                        return;
                    }

                    if (targetUser.Mate != Language.StrNone)
                    {
                        user.SendSysMessage(Language.StrTargetAlreadyMarried);
                        return;
                    }

                    if (user.Mate != Language.StrNone)
                    {
                        user.SendSysMessage(Language.StrYouAlreadyMarried);
                        return;
                    }

                    if (targetUser.Gender == user.Gender)
                    {
                        user.SendSysMessage(Language.StrNowAllowedSameGenderMarriage);
                        return;
                    }

                    if (!user.FetchMarryRequest(targetUser.Identity))
                    {
                        user.SendSysMessage(Language.StrNowAllowedSameGenderMarriage);
                        return;
                    }

                    user.Mate = targetUser.Name;
                    targetUser.Mate = user.Name;

                    MsgName nameMsg = new MsgName
                    {
                        Identity = user.Identity,
                        Action = StringAction.Mate
                    };
                    nameMsg.Append(user.Mate);
                    user.Send(nameMsg);
                    nameMsg = new MsgName
                    {
                        Identity = targetUser.Identity,
                        Action = StringAction.Mate
                    };
                    nameMsg.Append(targetUser.Mate);
                    targetUser.Send(nameMsg);

                    ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrMarry, targetUser.Name, user.Name),
                        ChatTone.Center);
                    break;
                }

                case InteractionType.ACT_ITR_MAGIC_ATTACK:
                {
                    #region TemporaryDecryption

                    ushort skillId = Convert.ToUInt16(((long) msg[28] & 0xFF) | (((long) msg[29] & 0xFF) << 8));
                    skillId ^= 0x915d;
                    skillId ^= (ushort) user.Identity;
                    skillId = (ushort) ((skillId << 0x3) | (skillId >> 0xd));
                    skillId -= 0xeb42;

                    uint Target = ((uint) msg[16] & 0xFF) | (((uint) msg[17] & 0xFF) << 8) |
                                  (((uint) msg[18] & 0xFF) << 16) | (((uint) msg[19] & 0xFF) << 24);
                    Target =
                        ((((Target & 0xffffe000) >> 13) | ((Target & 0x1fff) << 19)) ^ 0x5F2D2463 ^ user.Identity) -
                        0x746F4AE6;

                    msg.TargetIdentity = Target;

                    ushort TargetX = 0;
                    ushort TargetY = 0;
                    long xx = (msg[20] & 0xFF) | ((msg[21] & 0xFF) << 8);
                    long yy = (msg[22] & 0xFF) | ((msg[23] & 0xFF) << 8);
                    xx = xx ^ (user.Identity & 0xffff) ^ 0x2ed6;
                    xx = ((xx << 1) | ((xx & 0x8000) >> 15)) & 0xffff;
                    xx |= 0xffff0000;
                    xx -= 0xffff22ee;
                    yy = yy ^ (user.Identity & 0xffff) ^ 0xb99b;
                    yy = ((yy << 5) | ((yy & 0xF800) >> 11)) & 0xffff;
                    yy |= 0xffff0000;
                    yy -= 0xffff8922;
                    TargetX = Convert.ToUInt16(xx);
                    TargetY = Convert.ToUInt16(yy);
                    msg.MagicType = skillId;
                    msg.CellX = TargetX;
                    msg.CellY = TargetY;

                    #endregion

                    if (user.IsAlive)
                        user.ProcessMagicAttack(msg.MagicType, msg.TargetIdentity, msg.CellX, msg.CellY);
                    break;
                }

                case InteractionType.ACT_ITR_PRESENT_EMONEY:
                {
                    user.AwardEmoney((int) user.CoinMoney, EmoneySourceType.Credit, user);
                    msg.Amount = user.CoinMoney;
                    user.CoinMoney = 0;
                    user.Send(msg);
                    break;
                }

                case InteractionType.ACT_ITR_COUNTER_KILL_SWITCH:
                {
#if DEBUG
                    //if (user.ProfessionSort != 5)
                    //return;
#else
                    if (user.ProfessionSort != 5 || !user.IsPureClass())
                        return;
#endif

                    if (!user.IsAlive || user.IsWing)
                        return;

                    msg.TargetIdentity = msg.EntityIdentity = user.Identity;
                    msg.CellX = user.MapX;
                    msg.CellY = user.MapY;
                    //if (user.Scapegoat)
                    //{
                    //    user.Scapegoat = false;
                    //}
                    //else
                    //{
                    //    user.Scapegoat = true;
                    //}
                    user.Scapegoat = !user.Scapegoat;

                    msg.Data = (uint) (user.Scapegoat ? 1 : 0);
                    user.Send(msg);
                    break;
                }
            }
        }
    }
}