﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 2211 - MsgArenicWitness.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/11/20 17:30
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgArenicWitness(Character user, MsgArenicWitness msg)
        {
            switch (msg.Action)
            {
                case MsgArenicWitness.RequestView:
                {
                    if (!user.IsAlive)
                        return;

                    if (user.Map.IsSkillMap() || user.Map.IsLineSkillMap() || user.Map.IsTrainingMap() ||
                        user.Map.IsPrisionMap())
                        return;

                    ServerKernel.ArenaQualifier.WatchMatch(user, msg.UserIdentity);
                    break;
                }
                case MsgArenicWitness.QuitView:
                {
                    ServerKernel.ArenaQualifier.QuitWatch(user);
                    break;
                }
                case MsgArenicWitness.Fighters:
                {
                    ServerKernel.ArenaQualifier.WitnessVote(user, msg.UserIdentity);
                    break;
                }
            }
        }
    }
}