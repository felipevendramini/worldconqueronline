﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 2205 - MsgQualifyingInteractive.cs
// Last Edit: 2020/01/17 23:08
// Created: 2019/10/22 01:05
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Qualifiers.User;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgQualifyingInteractive(Character user, MsgQualifyingInteractive msg)
        {
            switch (msg.Type)
            {
                case ArenaType.ARENA_ICON_ON:
                {
                    if (ServerKernel.ArenaQualifier.Inscribe(user))
                    {
                        user.SendArenaScreen();
                    }
                    else
                    {
                        ServerKernel.ArenaQualifier.Uninscribe(user);
                        user.SendArenaScreen();
                    }

                    break;
                }

                case ArenaType.ARENA_ICON_OFF:
                {
                    ServerKernel.ArenaQualifier.Uninscribe(user);
                    user.SendArenaScreen();
                    break;
                }

                case ArenaType.ACCEPT_DIALOG:
                {
                    PlayerQualifierMatch match = ServerKernel.ArenaQualifier.FindMatch(user.Identity);
                    if (match == null)
                    {
                        if (ServerKernel.ArenaQualifier.IsUserQueued(user.Identity))
                            ServerKernel.ArenaQualifier.Uninscribe(user);

                        user.SendArenaScreen();
                        return;
                    }

                    if (msg.Option == 1)
                    {
                        match.Accept(user.Identity);
                        if (match.IsReadyToStart())
                            match.EnterMatch();
                        user.SendArenaScreen();
                    }
                    else if (msg.Option == 2)
                    {
                        match.QuitMatch(user.Identity, true);
                        user.SendArenaScreen();
                    }

                    break;
                }

                case ArenaType.OPPONENT_GAVE_UP:
                {
                    PlayerQualifierMatch match = ServerKernel.ArenaQualifier.FindMatch(user.Identity);
                    if (match == null)
                        return;

                    if (user.Identity == match.User0.Identity)
                    {
                        match.Points2 = uint.MaxValue;
                    }
                    else
                    {
                        match.Points1 = uint.MaxValue;
                    }

                    match.EndMatch();
                    user.SendArenaScreen();
                    break;
                }

                case ArenaType.BUY_ARENA_POINTS:
                {
                    if (user.PlayerQualifier.Points >= 1500)
                        return;

                    if (!user.SpendMoney(ArenaQualifierManager.PRICE_PER_1500_POINTS, true))
                        return;

                    user.PlayerQualifier.Points += 1500;
                    user.SendArenaScreen();
                    break;
                }

                case ArenaType.END_MATCH_JOIN:
                {
                    if (ServerKernel.ArenaQualifier.Uninscribe(user))
                    {
                        ServerKernel.ArenaQualifier.Inscribe(user);
                        user.SendArenaScreen();
                    }

                    break;
                }

                default:
                    Program.WriteLog($"Not handled 2205:{msg.Type}");
                    break;
            }
        }
    }
}