﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 2076 - MsgQuench.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Drawing;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Items;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        private const uint _REFINERY_DEFAULT_TIME = 7; // in days
        private const uint _ARTIFACT_DEFAULT_TIME = 7; // in days

        public static void HandleMsgQuench(Character user, MsgQuench msg)
        {
            Item source = user.UserPackage[msg.TargetIdentity];
            Item target = null;

            switch (msg.Mode)
            {
                case PurificationMode.Purify:
                {
                    target = user.UserPackage[msg.ItemIdentity] ?? user.UserPackage.GetEquipById(msg.ItemIdentity);
                    if (target == null)
                        return;

                    uint type = target.Type;
                    uint refineryType = source.Type;
                    if (!ServerKernel.Refinery.TryGetValue(refineryType, out var refinery))
                    {
                        user.SendSysMessage(Language.StrQuenchIncompatibleRefinery);
                        ServerKernel.Log.GmLog("Missing Refinery",
                            $"Non existent refinery [{refineryType}] requested by the client ");
                        return;
                    }

                    switch (refinery.Itemtype.ToString().Length)
                    {
                        // Position
                        case 1:
                        {
                            if (Calculations.GetItemPosition(type) == (ItemPosition) refinery.Itemtype
                                || Calculations.GetItemPosition(type) == ItemPosition.RightHand && refinery.Itemtype == 5 && target.GetSort() == ItemSort.ItemsortWeaponDoubleHand)
                                break;
                            user.SendSysMessage(Language.StrQuenchIncompatibleRefinery);
                            return;
                        }

                        // Type
                        case 2:
                        {
                            if (type / 10000 == refinery.Itemtype)
                                break;
                            user.SendSysMessage(Language.StrQuenchIncompatibleRefinery);
                            return;
                        }

                        // Specific item
                        case 3:
                        {
                            if (type / 1000 == refinery.Itemtype)
                                break;
                            // Ring/HeavyRing Exception :)
                            if (refinery.Itemtype == 150 && type / 1000 == 151)
                                break;
                            user.SendSysMessage(Language.StrQuenchIncompatibleRefinery);
                            return;
                        }

                        default:
                            return;
                    }

                    if (!user.UserPackage.RemoveFromInventory(source.Identity, RemovalType.Delete))
                        return;


                    target.RefineryExpire = (uint) (UnixTimestamp.Now() + 60 * 60 * 24 * _REFINERY_DEFAULT_TIME);
                    target.RefineryLevel = (byte) refinery.Level;
                    target.RefineryStart = (uint) UnixTimestamp.Now();
                    target.RefineryType = refinery.Id;
                    //user.Send(msg);
                    target.Save();
                    user.Send(target.CreateMsgItemInfo(ItemMode.Update));
                    target.LoadArtifactAndRefinery();
                    target.SendPurification();

                    user.SendSysMessage(
                        string.Format(Language.StrQuenchRefinerySuccess, PrepareMessage(refinery.Type, refinery.Power),
                            target.Itemtype.Name), Color.White);

                    ServerKernel.Log.GmLog("refinery",
                        $"User[{user.Identity}] Refined[{source.Type}]Identity[{source.Identity}] Into[{target.Identity}]");
                    break;
                }

                case PurificationMode.ItemArtifact:
                {
                    target = user.UserPackage[msg.ItemIdentity];

                    if (target == null)
                        return;

                    // Ok, be careful with this
                    if (source.Type < 800000
                        || source.Type >= 899999)
                    {
                        user.SendSysMessage("This item cannot be refined.");
                        return;
                    }

                    if (target.Itemtype.ReqLevel < source.Itemtype.ReqLevel)
                    {
                        user.SendSysMessage($"Your item need to be at least level {source.Itemtype.ReqLevel} to be refined with this artifact.");
                        return;
                    }

                    if (source.Itemtype.RequireWeaponType > 0)
                    {
                        switch (source.Itemtype.RequireWeaponType.ToString().Length)
                        {
                            case 1: // sort
                            {
                                if (source.Itemtype.RequireWeaponType != (int) target.GetSort())
                                    return;
                                break;
                            }

                            case 3: // specific type
                            {
                                if (source.Itemtype.RequireWeaponType != target.GetItemSubtype())
                                    return;
                                break;
                            }
                        }
                    }
                    else
                    {
                        switch (source.GetItemSubtype())
                        {
                            case 800: // weapons
                            {
                                switch (source.Type % 1000 / 100)
                                {
                                    case 0:
                                    case 1: // single hand
                                        if (target.GetSort() != ItemSort.ItemsortWeaponSingleHand
                                            && target.GetSort() != ItemSort.ItemsortWeaponProfBased)
                                            return;
                                        break;
                                    case 2:
                                    case 3: // double hand
                                        if (target.GetItemSubtype() == 500
                                            || target.GetSort() != ItemSort.ItemsortWeaponDoubleHand)
                                            return;
                                        break;
                                    case 4: // shield
                                        if (target.GetSort() != ItemSort.ItemsortWeaponShield)
                                            return;
                                        break;
                                    case 5: // backsword
                                        if (target.GetItemSubtype() != 421)
                                            return;
                                        break;
                                    case 6: // bow
                                        if (target.GetItemSubtype() != 500)
                                            return;
                                        break;
                                    case 7: // beads
                                        if (target.GetItemSubtype() != 610)
                                            return;
                                        break;
                                }

                                break;
                            }

                            case 820: // headwear
                            {
                                if (Calculations.GetItemPosition(target.Type) != ItemPosition.Headwear)
                                    return;
                                break;
                            }

                            case 821: // neck
                            {
                                if (Calculations.GetItemPosition(target.Type) != ItemPosition.Necklace)
                                    return;
                                break;
                            }

                            case 822: // armor
                            {
                                if (Calculations.GetItemPosition(target.Type) != ItemPosition.Armor)
                                    return;
                                break;
                            }

                            case 823: // ring
                            {
                                if (Calculations.GetItemPosition(target.Type) != ItemPosition.Ring)
                                    return;
                                break;
                            }

                            case 824: // boots
                            {
                                if (Calculations.GetItemPosition(target.Type) != ItemPosition.Boots)
                                    return;
                                break;
                            }
                        }
                    }

                    //user.Send(msg);
                    if (!user.UserPackage.SpendMeteors((int) source.Itemtype.MeteorAmount))
                    {
                        user.SendSysMessage(string.Format(Language.StrQuenchArtifactMeteorAmount,
                            source.Itemtype.MeteorAmount));
                        return;
                    }

                    if (!user.UserPackage.RemoveFromInventory(source.Identity, RemovalType.Delete))
                    {
                        user.SendSysMessage(Language.StrQuenchArtifactNoArtifact);
                        ServerKernel.Log.GmLog("artifact_error",
                            $"User[{user.Identity}] LostMeteors[{source.Itemtype.MeteorAmount}] ArtifactAdded[false]");
                        return;
                    }

                    uint extraTime = 0;
                    if (user.Owner.VipLevel < 4)
                        extraTime += user.Owner.VipLevel;
                    else
                        extraTime += 7;

                    target.ArtifactType = source.Type;
                    target.ArtifactStart = (uint) UnixTimestamp.Now();
                    target.ArtifactExpire = target.ArtifactStart + (_ARTIFACT_DEFAULT_TIME + extraTime) * UnixTimestamp.TIME_SECONDS_DAY;
                    target.Save();

                    target.LoadArtifactAndRefinery();
                    target.SendPurification();

                    //Acredito eu que voce só precisaria enviar este pacote com esse tipo: DragonSoul:5
                    target.SendArtifact(PurificationType.DragonSoul);

                    user.Send(target.CreateMsgItemInfo(ItemMode.Update));

                    user.Send(msg);
                    ServerKernel.Log.GmLog("artifact",
                        $"User[{user.Identity}] Refined[{source.Type}]Identity[{source.Identity}] Into[{target.Identity}]");
                    break;
                }
            }
        }

        private static string PrepareMessage(uint type, uint value)
        {
            switch (type)
            {
                case 1:
                    return value + "% M-Defense";
                case 2:
                    return value + "% Critical-Strike";
                case 3:
                    return value + "% Skill Critical-Strike";
                case 4:
                    return value + "% Immunity";
                case 5:
                    return value + "% Breakthrough";
                case 6:
                    return value + "% Counteraction";
                case 7:
                    return value + "% Detoxication";
                case 8:
                    return value + "% Block";
                case 9:
                    return value + "% Penetration";
                case 10:
                    return value + "Intensification";
                case 11:
                    return value + "% Fire Resist";
                case 12:
                    return value + "% Water Resist";
                case 13:
                    return value + "% Wood Resist";
                case 14:
                    return value + "% Metal Resist";
                case 15:
                    return value + "% Earth Resist";
                default:
                    return "Error";
            }
        }
    }
}