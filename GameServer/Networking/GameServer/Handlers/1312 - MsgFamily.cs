﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1312 - MsgFamily.cs
// Last Edit: 2019/11/24 20:07
// Created: 2019/11/24 18:50
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Linq;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Networking.Packets;
using GameServer.Structures;
using GameServer.Structures.Entities;
using GameServer.Structures.Groups.Families;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgFamily(Character user, MsgFamily msg)
        {
            // Add Enemy and Add Ally send the family name
            switch (msg.Type)
            {
                #region Information

                case FamilyType.Info:
                {
                    if (user.Family == null)
                        return;

                    user.Family.SendFamily(user);
                    break;
                }

                #endregion

                #region Members List

                case FamilyType.Members:
                {
                    if (user.Family == null)
                        return;
                    user.Family.SendMembers(user);
                    break;
                }

                #endregion

                #region Announcement

                case FamilyType.Announce:
                {
                    if (user.Family == null)
                        return;
                    msg.Identity = user.FamilyIdentity;
                    msg.AddString(user.Family.Announcement);
                    user.Send(msg);
                    break;
                }

                #endregion

                #region Set Announcement

                case FamilyType.SetAnnouncement:
                {
                    if (user.Family == null)
                        return;

                    if (user.FamilyPosition != FamilyRank.ClanLeader)
                        return;

                    string szAnnounce = msg.Announcement;
                    if (szAnnounce.Length > 127)
                        szAnnounce = szAnnounce.Substring(0, 127);

                    user.Family.Announcement = szAnnounce;
                    user.Send(msg);
                    break;
                }

                #endregion

                #region My Clan

                case FamilyType.MyClan:
                {
                    if (user.Family == null) return;

                    user.Family.SendFamily(user);
                    user.Family.SendOccupation(user);
                    break;
                }

                #endregion

                #region Dedicate

                case FamilyType.Dedicate:
                {
                    if (user.Family == null)
                        return;

                    if (!user.SpendMoney((int) msg.Identity, true))
                        return;

                    user.Family.MoneyFunds += msg.Identity;
                    user.FamilyMember.Donation += msg.Identity;
                    user.Family.SendFamily(user);
                    break;
                }

                #endregion

                #region Invite

                case FamilyType.Recruit:
                {
                    if (user.Family == null)
                        return;

                    if (user.Family.IsFull)
                        return;

                    Character pTarget = ServerKernel.UserManager.GetUser(msg.Identity);
                    if (pTarget == null)
                        return;

                    if (pTarget.Family != null)
                        return;

                    msg.Identity = user.Family.Identity;
                    msg.AddString(user.FamilyName);
                    msg.AddString(user.Name);
                    pTarget.Send(msg);

                    user.SetFamilyRecruitRequest(pTarget.Identity);
                    break;
                }

                #endregion

                #region Accept Invite

                case FamilyType.AcceptRecruit:
                {
                    if (user.Family != null) return;

                    Family pFamily;
                    if (!ServerKernel.Families.TryGetValue(msg.Identity, out pFamily))
                        return;

                    if (pFamily.IsFull)
                        return;

                    Character pLeader = ServerKernel.UserManager.GetUser(pFamily.LeaderIdentity);
                    if (pLeader == null)
                        return;

                    if (pLeader.FamilyPosition != FamilyRank.ClanLeader)
                        return;

                    if (!pLeader.FetchFamilyRecruitRequest(user.Identity))
                        return;

                    pFamily.AppendMember(pLeader, user);
                    pLeader.ClearFamilyRecruitRequest();
                    break;
                }

                #endregion

                #region Join

                case FamilyType.Join:
                {
                    if (user.Family != null)
                        return;

                    Character pTarget = ServerKernel.UserManager.GetUser(msg.Identity);
                    if (pTarget == null)
                        return;

                    if (pTarget.Family == null || pTarget.Family.IsFull)
                        return;

                    msg.Identity = user.Identity;
                    msg.AddString(user.Name);
                    pTarget.Send(msg);

                    user.SetFamilyJoinRequest(pTarget.Identity);
                    break;
                }

                #endregion

                #region Accept Join

                case FamilyType.AcceptJoinRequest:
                {
                    if (user.Family == null || user.FamilyPosition != FamilyRank.ClanLeader)
                        return;

                    Character pInvited = ServerKernel.UserManager.GetUser(msg.Identity);
                    if (pInvited == null)
                        return;
                    if (pInvited.Family != null)
                        return;

                    if (!pInvited.FetchFamilyJoinRequest(user.Identity))
                        return;

                    user.Family.AppendMember(user, pInvited);
                    pInvited.ClearFamilyJoinRequest();
                    break;
                }

                #endregion

                #region Quit

                case FamilyType.Quit:
                {
                    if (user.Family == null)
                        return;
                    if (user.FamilyPosition == FamilyRank.ClanLeader)
                        return;
                    if (user.FamilyPosition == FamilyRank.Spouse)
                        return;
                    user.Family.KickoutMember(user.FamilyMember);
                    break;
                }

                #endregion

                #region Abdicate

                case FamilyType.TransferLeader:
                {
                    if (user.Family == null)
                        return;
                    if (user.FamilyPosition != FamilyRank.ClanLeader)
                        return;

                    Character target = ServerKernel.UserManager.GetUser(msg.Name);
                    if (target == null || target.FamilyIdentity != user.FamilyIdentity || target.FamilyPosition == FamilyRank.Spouse)
                        return;

                    user.Family.Abdicate(user, target);
                    break;
                }

                #endregion

                #region Kickout

                case FamilyType.Kickout:
                {
                    if (user.Family == null || user.FamilyPosition != FamilyRank.ClanLeader)
                        return;
                    FamilyMember pTarget = null;
                    foreach (var client in user.Family.Members.Values)
                    {
                        if (client.Name == msg.Name)
                        {
                            pTarget = client;
                            break;
                        }
                    }

                    if (pTarget == null || pTarget.Position == FamilyRank.ClanLeader || pTarget.Position == FamilyRank.Spouse)
                        return;

                    user.Family.KickoutMember(pTarget);
                    user.Family.SendFamily(user);
                    user.Family.SendMembers(user);
                    break;
                }

                #endregion

                #region Ally

                case FamilyType.AddAlly:
                {
                    if (user.Family == null)
                        return;
                    if (user.FamilyPosition != FamilyRank.ClanLeader)
                        return;
                    if (user.Family.Allies.Count >= 5)
                        return;

                    Character pTargetLeader = ServerKernel.UserManager.GetUser(msg.Identity);
                    if (pTargetLeader == null)
                        return;

                    if (pTargetLeader.FamilyPosition != FamilyRank.ClanLeader)
                        return;

                    Family pTarget = pTargetLeader.Family;
                    if (pTarget == null)
                        return;

                    RequestBox pAlly = new RequestBox
                    {
                        OwnerIdentity = user.FamilyIdentity,
                        OwnerName = user.FamilyName,
                        ObjectIdentity = pTarget.Identity,
                        ObjectName = pTarget.Name,
                        Type = RequestBoxType.FamilyAlly,
                        Message =
                            $"{user.Name} Leader of the Clan {user.FamilyName} wants to be your ally. Do you accept?"
                    };
                    user.RequestBox = pTargetLeader.RequestBox = pAlly;
                    pAlly.Send(pTargetLeader);
                    break;
                }

                #endregion

                #region Delete Ally

                case FamilyType.DeleteAlly:
                {
                    if (user.Family == null)
                        return;
                    if (user.FamilyPosition != FamilyRank.ClanLeader)
                        return;

                    Family pTarget = ServerKernel.Families.Values.FirstOrDefault(x => x.Name == msg.Name);
                    if (pTarget == null)
                        return;
                    user.Family.RemoveAlly(pTarget);
                    user.Family.SendRelation(user);
                    break;
                }

                #endregion

                #region Enemy

                case FamilyType.AddEnemy:
                {
                    if (user.Family == null)
                        return;
                    if (user.FamilyPosition != FamilyRank.ClanLeader)
                        return;
                    if (user.Family.Enemies.Count >= 5)
                        return;
                    Family pTarget = ServerKernel.Families.Values.FirstOrDefault(x => x.Name == msg.Name);
                    if (pTarget == null)
                        return;
                    user.Family.EnemyFamily(pTarget);
                    user.Family.SendRelation(user);
                    break;
                }

                #endregion

                #region Delete Enemy

                case FamilyType.DeleteEnemy:
                {
                    if (user.Family == null)
                        return;
                    if (user.FamilyPosition != FamilyRank.ClanLeader)
                        return;

                    Family pTarget = ServerKernel.Families.Values.FirstOrDefault(x => x.Name == msg.Name);
                    if (pTarget == null)
                        return;
                    user.Family.RemoveEnemy(pTarget);
                    user.Family.SendRelation(user);
                    break;
                }

                #endregion

                default:
                {
                    ServerKernel.Log.SaveLog($"MsgFamily:Type{msg.Type} not handled", LogType.WARNING);
                    GamePacketHandler.Report(msg);
                    break;
                }
            }
        }
    }
}