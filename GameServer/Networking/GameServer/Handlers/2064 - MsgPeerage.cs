﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 2064 - MsgPeerage.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Drawing;
using System.Linq;
using FtwCore;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Networking.Packets;
using GameServer.Structures;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgPeerage(Character pRole, MsgPeerage pMsg)
        {
            switch (pMsg.Action)
            {
                #region List

                case NobilityAction.LIST:
                {
                    const int maxPages = 5;
                    int page = pMsg.Data;
                    const int maxPerPage = 10;
                    int value = 0;
                    int amount = 0;

                    int minValue = page * maxPerPage;
                    int maxValue = (page + 1) * maxPerPage;

                    MsgPeerage nPacket = new MsgPeerage(32 + 48) {Action = NobilityAction.LIST};
                    ushort maxCount = (ushort) Math.Ceiling((double) ServerKernel.Nobility.Count / 10);
                    nPacket.DataShort = (ushort) (maxCount > maxPages ? maxPages : maxCount);

                    foreach (DynaRankRecordsEntity dynaRank in ServerKernel.Nobility.Values.OrderByDescending(x =>
                        x.Value))
                    {
                        if (value >= maxPages * maxPerPage)
                            break;

                        if (value < minValue)
                        {
                            value++;
                            continue;
                        }

                        if (value >= maxValue)
                            break;

                        uint lookface = 0u;
                        Character pPlayer = ServerKernel.UserManager.GetUser(dynaRank.UserIdentity);
                        if (pPlayer != null)
                        {
                            lookface = pPlayer.Lookface;
                        }

                        NobilityLevel rank = GetNobilityLevel(value, dynaRank.Value);
                        nPacket.WriteNobilityData(dynaRank.UserIdentity, lookface, dynaRank.Username,
                            dynaRank.Value, rank, value);
                        value++;
                        amount++;
                    }

                    nPacket.DataHighLow = (ushort) amount;

                    nPacket.WriteByte((byte) amount, 28);
                    pRole.Send(nPacket);
                    break;
                }

                #endregion

                #region Query Remaining Silver

                case NobilityAction.QUERY_REMAINING_SILVER:
                {
                    var nPacket = new MsgPeerage
                    {
                        Action = NobilityAction.QUERY_REMAINING_SILVER
                    };
                    for (int i = 1; i < 13; i += 2)
                    {
                        if (i == 11) i++;
                        nPacket.AddMinimum((byte) i,
                            (ulong) GetRemainingSilver((NobilityLevel) i, pRole.NobilityDonation));
                    }

                    pRole.Send(nPacket);
                    break;
                }

                #endregion

                #region Donate

                case NobilityAction.DONATE:
                {
                    if (pRole.Level < 70)
                    {
                        pRole.SendSysMessage(Language.StrPeerageDonateErrBelowLevel);
                        return;
                    }

                    long donation = pMsg.DataLong;

                    if (donation < Nobility.MINIMUM_DONATION)
                    {
                        pRole.SendSysMessage(Language.StrPeerageDonateErrBelowUnderline);
                        return;
                    }

                    // maximum donation apply only for gold. CP's will be divided by 50000, so amount is increased
                    if (donation > Nobility.MAXIMUM_DONATION && pMsg.Data2 == 0)
                    {
                        pRole.SendSysMessage(Language.StrPeerageDonateAboveAmount);
                        return;
                    }

                    NobilityLevel oldRank = pRole.NobilityRank;
                    int oldPos = pRole.Nobility.Position;
                    //bool bSynHasKing = oldRank != NobilityLevel.KING && pRole.Syndicate?.HasKingMember() == true;

                    // donate cps
                    if (pMsg.DonationType >= 1)
                    {
                        pRole.SendSysMessage(Language.StrPeerageEmpireOnlyGold);
                        return;
                        //// if we don't accept any, error
                        //if (!ServerKernel.NobilityBoundCPDonation && !ServerKernel.NobilityCPDonation)
                        //{
                        //    pRole.SendSysMessage(Language.StrPeerageDonateErrNoEmoney);
                        //    return;
                        //}

                        //int cps = (int) (donation / 50000);
                        //if (ServerKernel.NobilityBoundCPDonation) // if we can donate bound cps, let's try to do it
                        //{
                        //    if (!pRole.SpendBoundEmoney(cps) && !ServerKernel.NobilityCPDonation)
                        //    {
                        //        pRole.SendSysMessage(Language.StrPeerageDonateErrNoBEmoney);
                        //        return;
                        //    }
                        //}

                        //if (ServerKernel.NobilityCPDonation && !pRole.SpendEmoney(cps, EmoneySourceType.Event,
                        //        EmoneyOperationType.Nobility, pRole))
                        //{
                        //    pRole.SendSysMessage(Language.StrPeerageDonateErrNoEmoney);
                        //    return;
                        //}

                        //pRole.Nobility.Donate(donation);
                    }
                    else if (pMsg.DonationType == 0) // donate silvers
                    {
                        if (!pRole.SpendMoney((int) donation))
                        {
                            pRole.SendSysMessage(Language.StrPeerageDonateErrNoEnoughMoney);
                            return;
                        }

                        ServerKernel.Empire.AwardMoney((int) donation);
                        pRole.Nobility.Donate(donation);
                    }

                    if (!ServerKernel.Nobility.ContainsKey(pRole.Identity))
                    {
                        ServerKernel.Nobility.TryAdd(pRole.Identity, pRole.Nobility.Database);
                        pRole.Nobility.Update();
                    }

                    if (pRole.NobilityRank > NobilityLevel.EARL && oldPos != pRole.Nobility.Position)
                    {
                        ServerKernel.UserManager.UpdateNobilityForAllUsers();

                        if (pRole.Syndicate != null && pRole.Syndicate.KingCount() > 1)
                        {
                            if (pRole.NobilityRank == NobilityLevel.KING &&
                                pRole.SyndicatePosition < SyndicateRank.GuildLeader)
                            {
                                pRole.Syndicate.QuitSyndicate(pRole, true);
                                pRole.SendMessage(Language.StrSynLeaveByKing, Color.White, ChatTone.Talk);
                            }
                            else
                            {
                                pRole.Syndicate.KickKingMember();
                            }
                        }
                    }
                    else
                        pRole.Nobility.SendNobilityIcon();

                    if (pRole.NobilityRank > oldRank)
                    {
                        switch (pRole.NobilityRank)
                        {
                            case NobilityLevel.KING:
                                if (pRole.Gender == 1)
                                    ServerKernel.UserManager.SendToAllUser(
                                        string.Format(Language.StrPeeragePromptKing, pRole.Name), ChatTone.Center);
                                else
                                    ServerKernel.UserManager.SendToAllUser(
                                        string.Format(Language.StrPeeragePromptQueen, pRole.Name), ChatTone.Center);
                                break;
                            case NobilityLevel.PRINCE:
                                if (pRole.Gender == 1)
                                    ServerKernel.UserManager.SendToAllUser(
                                        string.Format(Language.StrPeeragePromptPrince, pRole.Name), ChatTone.Center);
                                else
                                    ServerKernel.UserManager.SendToAllUser(
                                        string.Format(Language.StrPeeragePromptPrincess, pRole.Name), ChatTone.Center);
                                break;
                            case NobilityLevel.DUKE:
                                if (pRole.Gender == 1)
                                    ServerKernel.UserManager.SendToAllUser(
                                        string.Format(Language.StrPeeragePromptDuke, pRole.Name), ChatTone.Center);
                                else
                                    ServerKernel.UserManager.SendToAllUser(
                                        string.Format(Language.StrPeeragePromptDuchess, pRole.Name), ChatTone.Center);
                                break;
                            case NobilityLevel.EARL:
                                if (pRole.Gender == 1)
                                    ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrPeeragePromptEarl,
                                        pRole.Name));
                                else
                                    ServerKernel.UserManager.SendToAllUser(
                                        string.Format(Language.StrPeeragePromptCountess, pRole.Name));
                                break;
                            case NobilityLevel.BARON:
                                if (pRole.Gender == 1)
                                    ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrPeeragePromptBaron,
                                        pRole.Name));
                                else
                                    ServerKernel.UserManager.SendToAllUser(
                                        string.Format(Language.StrPeeragePromptBaroness, pRole.Name));
                                break;
                            case NobilityLevel.KNIGHT:
                                if (pRole.Gender == 1)
                                    ServerKernel.UserManager.SendToAllUser(
                                        string.Format(Language.StrPeeragePromptKnight, pRole.Name));
                                else
                                    ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrPeeragePromptLady,
                                        pRole.Name));
                                break;
                        }
                    }

                    break;
                }

                #endregion
            }
        }

        public static long GetRemainingSilver(NobilityLevel level, long donation)
        {
            switch (level)
            {
                case NobilityLevel.KING:
                    return GetDonation(3) + 1 - donation;
                case NobilityLevel.PRINCE:
                    return GetDonation(15) + 1 - donation;
                case NobilityLevel.DUKE:
                    return GetDonation(50) + 1 - donation;
                case NobilityLevel.EARL:
                    return 200000000 - donation;
                case NobilityLevel.BARON:
                    return 100000000 - donation;
                case NobilityLevel.KNIGHT:
                    return 30000000 - donation;
                default:
                    return 0;
            }
        }

        public static NobilityLevel GetRanking(uint idUser)
        {
            if (ServerKernel.Nobility.Values.FirstOrDefault(x => x.UserIdentity == idUser) == null)
                return NobilityLevel.SERF;

            uint i = 1;
            DynaRankRecordsEntity record = null;
            foreach (var rank in ServerKernel.Nobility.Values.OrderByDescending(x => x.Value))
            {
                if (rank.UserIdentity == idUser)
                {
                    record = rank;
                    break;
                }

                i++;
            }

            return GetNobilityLevel(i, record?.Value ?? 0);
        }

        public static long GetDonation(int position)
        {
            int ranking = 1;
            foreach (DynaRankRecordsEntity dynaRank in ServerKernel.Nobility.Values.OrderByDescending(x => x.Value))
            {
                if (position == ranking)
                    return dynaRank.Value;
                ranking++;
            }

            return 0;
        }

        public static NobilityLevel GetNobilityLevel(long position, long donation)
        {
            if (position < 0 || position >= 50)
            {
                if (donation >= 200000000)
                    return NobilityLevel.EARL;
                if (donation >= 100000000)
                    return NobilityLevel.BARON;
                if (donation >= 30000000)
                    return NobilityLevel.KNIGHT;
                return NobilityLevel.SERF;
            }

            if (position <= 2)
                return NobilityLevel.KING;
            if (position < 15)
                return NobilityLevel.PRINCE;
            if (position < 50)
                return NobilityLevel.DUKE;
            return NobilityLevel.SERF;
        }
    }
}