﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1045 - MsgMailOperation.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Networking.Packets;
using GameServer.Structures;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgMailOperation(Character user, MsgMailOperation msg)
        {
            switch (msg.Mode)
            {
                case MsgMailOperation.Type.OPEN:
                {
                    user.MailBox.ReadMail(msg.MailIdentity);
                    break;
                }

                case MsgMailOperation.Type.DELETE:
                {
                    MailMessage mail = user.MailBox.GetMail(msg.MailIdentity);
                    if (mail == null)
                        return;

                    mail.MarkAsDeleted();
                    user.Send(msg);

                    user.MailBox.Refresh();
                    break;
                }

                case MsgMailOperation.Type.REMOVE_MONEY:
                {
                    MailMessage mail = user.MailBox.GetMail(msg.MailIdentity);
                    if (mail == null || !mail.IsClaimable)
                        return;
                    if (!mail.HasMoneyToClaim)
                        return;

                    mail.ClaimMoney();
                    user.Send(msg);
                    break;
                }

                case MsgMailOperation.Type.REMOVE_E_MONEY:
                {
                    MailMessage mail = user.MailBox.GetMail(msg.MailIdentity);
                    if (mail == null || !mail.IsClaimable)
                        return;

                    if (!mail.HasEMoneyToClaim)
                        return;

                    mail.ClaimEmoney();
                    user.Send(msg);
                    break;
                }

                case MsgMailOperation.Type.REMOVE_ATTACHMENT:
                {
                    MailMessage mail = user.MailBox.GetMail(msg.MailIdentity);
                    if (mail == null || !mail.IsClaimable)
                        return;

                    mail.ClaimItem();

                    user.Send(msg);
                    break;
                }

                default:
                    Program.WriteLog($"1045:{msg.Mode} not handled");
                    break;
            }
        }
    }
}