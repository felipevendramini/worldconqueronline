﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 2036 - MsgDataArray.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.Linq;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Items;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgDataArray(Character user, MsgDataArray msg)
        {
            Item target = user.UserPackage[msg.MainIdentity] ?? user.UserPackage.GetEquipById(msg.MainIdentity); //, source = user.UserPackage[msg.MinorIdentity];

            if (target == null)
                return;

            // Sort check
            uint oldPlus = target.Addition;

            int nTotal = 0;
            List<Item> usable = new List<Item>();

            switch (msg.Mode)
            {
                case DataArrayMode.ComposeItem:
                {
                    if (target.IsMount() || target.Addition >= 12)
                    {
                        user.SendSysMessage(Language.StrComposeItemMaxComposition);
                        return;
                    }

                    foreach (var minor in msg.Minors)
                    {
                        Item source = user.UserPackage[minor];
                        if (source == null)
                            continue;

                        if (source.Type < SpecialItem.TYPE_STONE1 || source.Type > SpecialItem.TYPE_STONE8)
                        {
                            if (source.IsWeaponOneHand())
                            {
                                if (!target.IsWeaponOneHand() && !target.IsWeaponProBased())
                                    continue;
                            }
                            else if (source.IsWeaponTwoHand())
                            {
                                if (source.IsBow() && !target.IsBow())
                                    continue;
                                if (!target.IsWeaponTwoHand())
                                    continue;
                            }

                            if (target.GetSort() != source.GetSort())
                                continue;

                            if (source.Addition == 0 || source.Addition > 8)
                                continue;
                        }

                        target.AdditionProgress += StonePlus(source.Addition, false);
                        while (target.AdditionProgress >= GetAddLevelExp(target.Addition, false) &&
                               target.Addition < 12)
                        {
                            if (target.Addition < 12)
                            {
                                target.AdditionProgress -= GetAddLevelExp(target.Addition, false);
                                target.Addition++;
                            }
                            else
                            {
                                target.AdditionProgress = 0;
                                break;
                            }
                        }

                        user.UserPackage.RemoveFromInventory(source, RemovalType.Delete);
                    }

                    break;
                }

                case DataArrayMode.ComposeSteedOriginal:
                {
                    if (!target.IsMount())
                    {
                        return;
                    }

                    foreach (var minor in msg.Minors)
                    {
                        Item source = user.UserPackage[minor];
                        if (source == null || !source.IsMount())
                            continue;

                        target.AdditionProgress += StonePlus(source.Addition, true);
                        while (target.AdditionProgress >= GetAddLevelExp(target.Addition, true) &&
                               target.Addition < 12)
                        {
                            if (target.Addition < 12)
                            {
                                target.AdditionProgress -= GetAddLevelExp(target.Addition, true);
                                target.Addition++;
                            }
                        }

                        user.UserPackage.RemoveFromInventory(source, RemovalType.Delete);
                    }

                    break;
                }

                case DataArrayMode.ComposeSteedNew:
                {
                    foreach (var minor in msg.Minors)
                    {
                        Item source = user.UserPackage[minor];
                        if (source == null || !source.IsMount())
                            continue;

                        target.AdditionProgress += StonePlus(source.Addition, true);
                        while (target.AdditionProgress >= GetAddLevelExp(target.Addition, true) &&
                               target.Addition < 12)
                        {
                            if (target.Addition < 12)
                            {
                                target.AdditionProgress -= GetAddLevelExp(target.Addition, true);
                                target.Addition++;
                            }
                        }

                        int color1 = (int) target.SocketProgress;
                        int color2 = (int) source.SocketProgress;
                        int B1 = color1 & 0xFF;
                        int B2 = color2 & 0xFF;
                        int G1 = (color1 >> 8) & 0xFF;
                        int G2 = (color2 >> 8) & 0xFF;
                        int R1 = (color1 >> 16) & 0xFF;
                        int R2 = (color2 >> 16) & 0xFF;
                        int newB = (int) Math.Floor(0.9 * B1) + (int) Math.Floor(0.1 * B2);
                        int newG = (int) Math.Floor(0.9 * G1) + (int) Math.Floor(0.1 * G2);
                        int newR = (int) Math.Floor(0.9 * R1) + (int) Math.Floor(0.1 * R2);
                        target.SocketProgress = (uint) (newB | (newG << 8) | (newR << 16));

                        user.UserPackage.RemoveFromInventory(source, RemovalType.Delete);
                    }

                    break;
                }

                case DataArrayMode.QuickCompose:
                case DataArrayMode.QuickComposeMount:
                {
                    if (target.AdditionProgress == 0 || target.Addition >= 12)
                        return;

                    float percent = (float) (target.AdditionProgress / (double) GetAddLevelExp(target.Addition, false) * 100);
                    if (Calculations.ChanceCalc(percent))
                    {
                        target.Addition++;
                    }

                    target.AdditionProgress = 0;
                    break;
                }

                case DataArrayMode.UpgradeItemLevel:
                {
                    if (!IsItemUpgradable(target))
                        return;

                    var nextItem = target.NextItemLevel();
                    if (nextItem == null)
                    {
                        user.Send(new MsgItem
                        {
                            Identity = target.Identity,
                            Param1 = 0,
                            Action = ItemAction.Uplev
                        });
                        user.SendSysMessage(Language.StrItemErrMaxLevel);
                        user.Send(target.CreateMsgItemInfo(ItemMode.Update));
                        return;
                    }

                    if (IsMeteorLevelUpgrade(target))
                    {
                        foreach (var idMinor in msg.Minors)
                        {
                            if (idMinor == 0)
                                return;

                            Item minor = user.UserPackage[idMinor];
                            if (minor == null)
                                continue;

                            switch (minor.Type)
                            {
                                case SpecialItem.TYPE_METEOR:
                                case SpecialItem.TYPE_METEORTEAR:
                                    nTotal++;
                                    break;
                                case SpecialItem.TYPE_METEOR_SCROLL:
                                    nTotal += 10;
                                    break;
                                default:
                                    continue;
                            }

                            usable.Add(minor);
                        }

                        int reqMeteors = (int) target.GetUpgradeGemAmount();
                        if (reqMeteors > nTotal)
                        {
                            foreach (var usage in usable)
                                user.UserPackage.RemoveFromInventory(usage, RemovalType.Delete);

                            float chance = nTotal / (float) reqMeteors * 100;
                            if (!Calculations.ChanceCalc(chance))
                            {
                                user.Send(new MsgItem
                                {
                                    Identity = target.Identity,
                                    Param1 = 0,
                                    Action = ItemAction.Uplev
                                });
                                target.Durability /= 2;
                                user.Send(target.CreateMsgItemInfo(ItemMode.Update));
                                return;
                            }

                            target.ChangeType(nextItem.Type);
                            user.SendEffect("improveSuc", false);
                            user.Send(new MsgItem
                            {
                                Identity = target.Identity,
                                Param1 = 1,
                                Action = ItemAction.Uplev
                            });
                        }
                        else
                        {
                            int nUsage = 0;
                            bool bound = false;
                            foreach (var item in usable.OrderByDescending(x => x.Type))
                            {
                                switch (item.Type)
                                {
                                    case SpecialItem.TYPE_METEOR:
                                    case SpecialItem.TYPE_METEORTEAR:
                                        nUsage++;
                                        break;
                                    case SpecialItem.TYPE_METEOR_SCROLL:
                                        nUsage += 10;
                                        break;
                                    default:
                                        continue;
                                }

                                bound |= item.IsBound;

                                user.UserPackage.RemoveFromInventory(item, RemovalType.Delete);

                                if (nUsage >= reqMeteors)
                                    break;
                            }

                            int nReturn = nUsage - reqMeteors;
                            if (nReturn > 0)
                            {
                                for (int i = 0; i < nReturn; i++)
                                    user.UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_METEOR, bound));
                            }

                            target.ChangeType(nextItem.Type);
                            user.SendEffect("improveSuc", false);
                            user.Send(new MsgItem
                            {
                                Identity = target.Identity,
                                Param1 = 1,
                                Action = ItemAction.Uplev
                            });
                        }
                    }
                    else
                    {
                        if (msg.Minors.Count <= 1)
                        {
                            user.Send(new MsgItem
                            {
                                Identity = target.Identity,
                                Param1 = 0,
                                Action = ItemAction.Uplev
                            });
                            user.Send(target.CreateMsgItemInfo(ItemMode.Update));
                            user.SendSysMessage(Language.StrItemErrNoDragonBall);
                            return;
                        }

                        Item minor = null;

                        for (int i = 1; i < msg.Minors.Count; i++)
                        {
                            if ((minor = user.UserPackage[msg.Minors[i]]) != null)
                                break;
                        }

                        if (minor == null)
                        {
                            user.Send(new MsgItem
                            {
                                Identity = target.Identity,
                                Param1 = 0,
                                Action = ItemAction.Uplev
                            });
                            user.Send(target.CreateMsgItemInfo(ItemMode.Update));
                            user.SendSysMessage(Language.StrItemErrNoDragonBall);
                            return;
                        }

                        user.UserPackage.RemoveFromInventory(minor, RemovalType.Delete);

                        target.ChangeType(nextItem.Type);
                        user.SendEffect("improveSuc", false);
                        user.Send(new MsgItem
                        {
                            Identity = target.Identity,
                            Param1 = 1,
                            Action = ItemAction.Uplev
                        });
                        user.Send(target.CreateMsgItemInfo(ItemMode.Update));
                    }

                    break;
                }

                case DataArrayMode.UpgradeItemQuality:
                {
                    uint idNewType = 0;
                    double nChance = 0.00;
                    target.GetUpEpQualityInfo(out nChance, out idNewType);
                    var nextItem = ServerKernel.Itemtype.Values.FirstOrDefault(x => x.Type == idNewType);

                    if (nextItem == null)
                    {
                        user.Send(new MsgItem
                        {
                            Identity = target.Identity,
                            Param1 = 0,
                            Action = ItemAction.Improve
                        });
                        user.SendSysMessage(Language.StrItemCannotImprove);
                        user.Send(target.CreateMsgItemInfo(ItemMode.Update));
                        return;
                    }

                    foreach (var idMinor in msg.Minors)
                    {
                        if (idMinor == 0)
                            return;

                        Item minor = user.UserPackage[idMinor];
                        if (minor == null)
                            continue;

                        switch (minor.Type)
                        {
                            case SpecialItem.TYPE_DRAGONBALL:
                                nTotal++;
                                break;
                            case SpecialItem.TYPE_DRAGONBALL_SCROLL:
                                nTotal += 10;
                                break;
                            default:
                                continue;
                        }

                        usable.Add(minor);
                    }

                    int reqDragonBalls = (int) target.GetUpQualityGemAmount();
                    if (reqDragonBalls > nTotal)
                    {
                        foreach (var usage in usable)
                            user.UserPackage.RemoveFromInventory(usage, RemovalType.Delete);

                        float chance = nTotal / (float) reqDragonBalls * 100;
                        if (!Calculations.ChanceCalc(chance))
                        {
                            user.Send(target.CreateMsgItemInfo(ItemMode.Update));
                            user.Send(new MsgItem
                            {
                                Identity = target.Identity,
                                Param1 = 0,
                                Action = ItemAction.Improve
                            });
                            return;
                        }

                        target.ChangeType(nextItem.Type);
                        user.SendEffect("improveSuc", false);
                        user.Send(new MsgItem
                        {
                            Identity = target.Identity,
                            Param1 = 1,
                            Action = ItemAction.Improve
                        });
                    }
                    else
                    {
                        int nUsage = 0;
                        bool bound = false;
                        foreach (var item in usable.OrderByDescending(x => x.Type))
                        {
                            switch (item.Type)
                            {
                                case SpecialItem.TYPE_DRAGONBALL:
                                    nUsage++;
                                    break;
                                case SpecialItem.TYPE_DRAGONBALL_SCROLL:
                                    nUsage += 10;
                                    break;
                                default:
                                    continue;
                            }

                            bound |= item.IsBound;

                            user.UserPackage.RemoveFromInventory(item, RemovalType.Delete);

                            if (nUsage >= reqDragonBalls)
                                break;
                        }

                        int nReturn = nUsage - reqDragonBalls;
                        if (nReturn > 0)
                        {
                            for (int i = 0; i < nReturn; i++)
                                user.UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_DRAGONBALL, bound));
                        }

                        target.ChangeType(nextItem.Type);
                        user.SendEffect("improveSuc", false);
                        user.Send(new MsgItem
                        {
                            Identity = target.Identity,
                            Param1 = 1,
                            Action = ItemAction.Improve
                        });
                    }

                    break;
                }

                default:
                    return;
            }

            if (oldPlus < target.Addition && target.Addition >= 6)
                ServerKernel.UserManager.SendToAllUser(user.Gender == 1
                    ? string.Format(Language.StrComposeOverpowerMale, user.Name,
                        target.Itemtype.Name, target.Addition)
                    : string.Format(Language.StrComposeOverpowerFemale, user.Name,
                        target.Itemtype.Name, target.Addition));

            target.Save();
            user.Send(target.CreateMsgItemInfo(ItemMode.Update));
        }

        private static bool IsMeteorLevelUpgrade(Item item)
        {
            switch (Calculations.GetItemPosition(item.Type))
            {
                case ItemPosition.Headwear:
                    return item.GetLevel() < 9;
                case ItemPosition.Necklace:
                    return item.GetLevel() < 22;
                case ItemPosition.Ring:
                    if (item.IsBangle())
                        return item.GetLevel() < 22;
                    return item.GetLevel() < 23;
                case ItemPosition.RightHand:
                    if (item.IsBow())
                        return item.GetLevel() < 21;
                    return item.GetLevel() < 22;
                case ItemPosition.LeftHand:
                    if (item.IsShield())
                        return item.GetLevel() < 10;
                    if (item.IsPistol())
                        return item.GetLevel() < 22;
                    return false;
                case ItemPosition.Armor:
                    return item.GetLevel() < 9;
                case ItemPosition.Boots:
                    return item.GetLevel() < 22;
            }

            return false;
        }

        private static bool IsItemUpgradable(Item item)
        {
            switch (Calculations.GetItemPosition(item.Type))
            {
                case ItemPosition.Headwear:
                    return item.GetLevel() < 30;
                case ItemPosition.Necklace:
                    return item.GetLevel() < 26;
                case ItemPosition.Ring:
                    if (item.IsBangle())
                        return item.GetLevel() < 27;
                    return item.GetLevel() < 26;
                case ItemPosition.RightHand:
                    if (item.IsBow())
                        return item.GetLevel() < 42;
                    return item.GetLevel() < 43;
                case ItemPosition.LeftHand:
                    if (item.IsShield())
                        return item.GetLevel() < 30;
                    if (item.IsPistol())
                        return item.GetLevel() < 42;
                    return false;
                case ItemPosition.Armor:
                    return item.GetLevel() < 30;
                case ItemPosition.Boots:
                    return item.GetLevel() < 24;
            }

            return false;
        }

        private static ushort StonePlus(uint plus, bool steed)
        {
            switch (plus)
            {
                case 0:
                    if (steed) return 1;
                    return 0;
                case 1: return 10;
                case 2: return 40;
                case 3: return 120;
                case 4: return 360;
                case 5: return 1080;
                case 6: return 3240;
                case 7: return 9720;
                case 8: return 29160;
                default: return 0;
            }
        }

        private static ushort GetAddLevelExp(uint plus, bool steed)
        {
            switch (plus)
            {
                case 0: return 20;
                case 1: return 20;
                case 2:
                    if (steed) return 90;
                    return 80;
                case 3: return 240;
                case 4: return 720;
                case 5: return 2160;
                case 6: return 6480;
                case 7: return 19440;
                case 8: return 58320;
                case 9: return 2700;
                case 10: return 5500;
                case 11: return 9000;
                default: return 0;
            }
        }
    }
}