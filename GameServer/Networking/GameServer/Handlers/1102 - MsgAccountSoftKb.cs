﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1102 - MsgAccountSoftKb.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore;
using FtwCore.Networking.Packets;
using GameServer.Structures;
using GameServer.Structures.Entities;
using GameServer.Structures.Items;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgAccountSoftKb(Character user, MsgAccountSoftKb msg)
        {
            uint dwWarehouse = Warehouse.GetWarehousePosition(msg.Identity);
            if (!user.Warehouses.ContainsKey(dwWarehouse))
            {
                //user.Send("Invalid warehouse.");
                return;
            }

            if (!user.IsUnlocked())
            {
                Msg2ndPsw pw2nd = new Msg2ndPsw
                {
                    Request = PasswordRequestType.CorrectPassword,
                    Password = 0x1
                };
                user.Send(pw2nd);
                return;
            }

            Warehouse wh = user.Warehouses[dwWarehouse];
            switch (msg.Action)
            {
                #region View Warehouse

                case WarehouseMode.WH_VIEW:
                    wh.SendAll();
                    break;

                #endregion

                #region Warehouse add item

                case WarehouseMode.WH_ADDITEM:
                    if (!wh.IsFull())
                    {
                        uint idItem = msg.Identifier;
                        Item item = user.UserPackage[idItem];
                        if (item == null)
                            return; // item doesnt exist
                        if (!user.UserPackage.RemoveFromInventory(item.Identity, RemovalType.RemoveAndDisappear)) return;
                        wh.Add(item);
                    }
                    else
                    {
                        user.SendSysMessage(Language.StrWarehouseIsFull);
                    }

                    break;

                #endregion

                #region Warehouse remove item

                case WarehouseMode.WH_REMITEM:
                    uint idItem0 = msg.Identifier;
                    Item _item = wh[idItem0];
                    if (_item != null)
                    {
                        if (!wh.Remove(_item.Identity))
                            return;
                        user.Send(msg);
                    }

                    break;

                #endregion
            }
        }
    }
}