﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 2533 - MsgTrainingVitality.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Drawing;
using FtwCore;
using FtwCore.Common.Enums;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Managers;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgTrainingVitality(Character user, MsgTrainingVitality msg)
        {
            switch (msg.Type)
            {
                case ChiRequestType.TrainingtypeUnlock:
                    if (user.Level < 110 && user.Metempsychosis == 0)
                    {
                        user.SendSysMessage(Language.StrNotEnoughLevel, Color.Red);
                        return;
                    }

                    ChiPowerType mode = (ChiPowerType) msg.Mode;
                    if (!user.ChiSystem.IsLocked(mode))
                        return;
                    switch (mode)
                    {
                        case ChiPowerType.Dragon:
                            // no requirements to meet
                            break;
                        case ChiPowerType.Phoenix:
                            if (user.ChiSystem.IsLocked(ChiPowerType.Dragon))
                                return;
                            if (user.ChiSystem.GetTotalPoints(ChiPowerType.Dragon) < 300)
                                return;
                            break;
                        case ChiPowerType.Tiger:
                            if (user.Level < 110 || user.Metempsychosis < 2)
                                return;
                            if (user.ChiSystem.IsLocked(ChiPowerType.Phoenix))
                                return;
                            if (user.ChiSystem.GetTotalPoints(ChiPowerType.Phoenix) < 300)
                                return;
                            break;
                        case ChiPowerType.Turtle:
                            if (user.Level < 110 || user.Metempsychosis < 2)
                                return;
                            if (user.ChiSystem.IsLocked(ChiPowerType.Tiger))
                                return;
                            if (user.ChiSystem.GetTotalPoints(ChiPowerType.Tiger) < 300)
                                return;
                            break;
                    }

                    if (user.SpendChiPoints(300))
                    {
                        user.ChiSystem.OpenChi(mode);
                    }
                    break;
                case ChiRequestType.TrainingtypeQueryinfo:
                    if (msg.UserIdentity == user.Identity)
                        user.ChiSystem.SendInfo();
                    else
                    {
                        Character target = ServerKernel.UserManager.GetUser(msg.UserIdentity);
                        if (target == null)
                            return;
                        target.ChiSystem.SendInfo(user);
                    }
                    break;
                case ChiRequestType.TrainingtypeStudy:
                    if (msg.Param >= TrainingVitalityManager.CHISAVE_ALL)
                        return;
                    int nValue = 50;

                    if ((msg.Param & TrainingVitalityManager.CHISAVE_VALUE1) != 0)
                        nValue += 50;
                    if ((msg.Param & TrainingVitalityManager.CHISAVE_VALUE2) != 0)
                        nValue += 50;
                    if ((msg.Param & TrainingVitalityManager.CHISAVE_VALUE3) != 0)
                        nValue += 50;
                    if ((msg.Param & TrainingVitalityManager.CHISAVE_VALUE4) != 0)
                        nValue += 50;

                    if (user.SpendChiPoints(nValue))
                        user.ChiSystem.GenerateValue((ChiPowerType) msg.Mode, (int) msg.Param);
                    break;
            }
        }
    }
}