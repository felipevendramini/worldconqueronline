﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1313 - MsgFamilyOccupy.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/11/24 18:50
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgFamilyOccupy(Character user, MsgFamilyOccupy msg)
        {
            switch (msg.Type)
            {
                case FamilyPromptType.RequestNpc:
                {
                    switch (msg.RequestNpc)
                    {
                        case 1005:
                        {
                            msg.DailyPrize = 722458;
                            msg.WeeklyPrize = 722454;
                            break;
                        }
                        case 1006:
                        {
                            msg.DailyPrize = 722458;
                            msg.WeeklyPrize = 722454;
                            break;
                        }
                        case 1007:
                        {
                            msg.DailyPrize = 722478;
                            msg.WeeklyPrize = 722474;
                            break;
                        }
                        case 1008:
                        {
                            msg.DailyPrize = 722478;
                            msg.WeeklyPrize = 722474;
                            break;
                        }
                        case 1009:
                        {
                            msg.DailyPrize = 722473;
                            msg.WeeklyPrize = 722469;
                            break;
                        }
                        case 1010:
                        {
                            msg.DailyPrize = 722473;
                            msg.WeeklyPrize = 722469;
                            break;
                        }
                        case 1011:
                        {
                            msg.DailyPrize = 722463;
                            msg.WeeklyPrize = 722459;
                            break;
                        }
                        case 1012:
                        {
                            msg.DailyPrize = 722463;
                            msg.WeeklyPrize = 722459;
                            break;
                        }
                        case 1013:
                        {
                            msg.DailyPrize = 722468;
                            msg.WeeklyPrize = 722464;
                            break;
                        }
                        case 1014:
                        {
                            msg.DailyPrize = 722468;
                            msg.WeeklyPrize = 722464;
                            break;
                        }
                    }

                    user.Send(msg);
                    break;
                }
                default:
                    ServerKernel.Log.SaveLog("Family Prompt Packet " + msg.Type);
                    break;
            }
        }
    }
}