﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1070 - MsgHangUp.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common.Enums;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgHangUp(Character user, MsgHangUp msg)
        {
            switch (msg.Mode)
            {
                case HangUpMode.Show:
                {
                    msg.Param = 391;
                    user.Send(msg);
                    return;
                }

                case HangUpMode.Start:
                {
                    user.Send(msg);
                    user.AttachStatus(user, FlagInt.AUTO_HUNTING, 0, int.MaxValue, 0, 0);
                    return;
                }

                case HangUpMode.Interface:
                {
                    msg.Experience = user.HangUpExperience;
                    user.Send(msg);
                    return;
                }

                case HangUpMode.End:
                {
                    user.Send(msg);
                    user.HangUpExperience = 0;
                    user.DetachStatus(FlagInt.AUTO_HUNTING);
                    return;
                }
            }
        }
    }
}