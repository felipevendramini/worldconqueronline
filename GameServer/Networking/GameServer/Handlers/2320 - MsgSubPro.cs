﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 2320 - MsgSubPro.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgSubPro(Character user, MsgSubPro msg)
        {
            if (user.SubClass == null)
                return;
            switch (msg.Action)
            {
                case SubClassActions.Switch:
                {
                    user.SubClass.Active(msg.Subclass);
                    break;
                }
                case SubClassActions.RequestUplev:
                {
                    user.SubClass.Uplev(msg.Subclass);
                    break;
                }
                case SubClassActions.MartialPromoted:
                {
                    user.SubClass.Promote(msg.Subclass);
                    break;
                }
                case SubClassActions.Info:
                {
                    user.SubClass.SendAll();
                    break;
                }
                case SubClassActions.LearnRemote:
                {
                    user.SubClass.Learn(msg.Subclass);
                    break;
                }

                case (SubClassActions) 10:
                {
                    user.SubClass.Promote(msg.Subclass);
                    break;
                }
                default:
                    Program.WriteLog($"Not handled [2320:{msg.Action}]", LogType.DEBUG);
                    break;
            }
        }
    }
}