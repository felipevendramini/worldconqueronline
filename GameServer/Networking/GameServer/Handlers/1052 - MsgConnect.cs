﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1052 - MsgConnect.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using FtwCore.Security;
using GameServer.Structures.Entities;
using GameServer.Structures.Login;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgConnect(User user, MsgConnect msg)
        {
            if (user.Identity == 0)
            {
                if (ServerKernel.LoginServer == null)
                {
                    user.Disconnect();
                    return;
                }

                // Decrypt important client data from the Account Server:
                var pCipher = new PhoenixTransferCipher(ServerKernel.TransferKey, ServerKernel.TransferSalt,
                    user.IpAddress);
                var pDecrypted = pCipher.Decrypt(new[] {msg.Identity, msg.Authentication});

                // let's check if the player has really requested the login into the account server
                LoginRequest
                    pRequest; // = ServerKernel.LoginQueue.Values.FirstOrDefault(x => x.IpAddress == user.IpAddress);
                if (ServerKernel.LoginQueue.TryRemove(pDecrypted[0], out pRequest))
                    //if (pRequest != null) // not the right way, but it's what we got now
                {
                    // user found
                    if (!pRequest.IsValid(pDecrypted[0], pDecrypted[1], user.IpAddress))
                    {
                        // something is not right, disconnect.
                        user.Disconnect();
                        return;
                    }
                }
                else
                {
                    ServerKernel.UserManager.RemoveFromCreationQueue(user);
                    ServerKernel.UserManager.LogoutUser(user.Character);
                    // user did not request a login token on the account server
                    user.Disconnect();
                    return;
                }

                user.AccountIdentity = msg.Identity = pRequest.AccountIdentity;
                user.Language = msg.Language;
                user.LastLogin = pRequest.LastLogin;
                user.VipLevel = pRequest.VipLevel;
                user.Authority = pRequest.Authority;
                user.LastIpAddress = pRequest.LastIpAddress;

                InitializeCharacter(user, msg);
            }
            else
            {
                InitializeCharacter(user, msg, true);
            }
        }

        private static void InitializeCharacter(User user, MsgConnect msg, bool bCreate = false)
        {
            AccountEntity account = new AccountRepository().SearchByIdentity(user.AccountIdentity);
            CharacterEntity character = new CharacterRepository().SearchByAccount(user.AccountIdentity);
            if (account == null)
            {
                user.Send(new MsgConnectEx(RejectionType.InvalidAccount));
                user.Disconnect();
                return;
            }

            if (character == null)
            {
                if (!ServerKernel.UserManager.AddToCreationQueue(user))
                    ServerKernel.UserManager.RemoveFromCreationQueue(user);

                user.Send(ServerMessages.Login.NewRole);
                return;
            }

            Character gameUser = ServerKernel.UserManager.CreateUser(character, user);
            if (gameUser == null)
                return;

            if (ServerKernel.UserManager.LoginUser(gameUser))
            {
                ServerKernel.LoginServer.Send(new MsgLoginUserInfo
                {
                    AccountIdentity = gameUser.Owner.AccountIdentity,
                    UserIdentity = gameUser.Identity,
                    UserName = gameUser.Name,
                    IpAddress = user.IpAddress,
                    MacAddress = gameUser.Owner.MacAddress,
                    Mode = LoginUserInfoMode.Connect
                });
            }
        }
    }
}