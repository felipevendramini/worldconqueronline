﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1130 - MsgTitle.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using FtwCore.Common;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void LoadTitles(Character pUser)
        {
            TitleRepository tempRepo = new TitleRepository();
            IList<TitleEntity> titles = tempRepo.GetUserTitle(pUser.Identity);
            if (titles != null)
            {
                int nNow = UnixTimestamp.Now();
                foreach (TitleEntity title in titles)
                {
                    if (title.Timestamp < nNow)
                    {
                        tempRepo.Delete(title);
                        continue;
                    }

                    pUser.Titles.TryAdd((UserTitle) title.Title, title);
                }
            }

            if (pUser.Titles.Count > 0)
            {
                MsgTitle pTitle = new MsgTitle
                {
                    Identity = pUser.Identity,
                    Action = TitleAction.QueryTitle
                };
                foreach (var title in pUser.Titles.Values)
                    pTitle.Append(title.Title);
                pUser.Send(pTitle);
            }

            if (pUser.Titles.ContainsKey(pUser.Title))
            {
                MsgTitle pTitle = new MsgTitle
                {
                    Identity = pUser.Identity,
                    Action = TitleAction.AddTitle,
                    SelectedTitle = pUser.Title
                };
                pUser.Send(pTitle);
            }
            else
            {
                pUser.Title = 0;
            }
        }

        public static void HandleMsgTitle(Character pUser, MsgTitle pMsg)
        {
            switch (pMsg.Action)
            {
                #region Query Title

                case TitleAction.QueryTitle:
                {
                    if (pUser.Titles.Count > 0)
                    {
                        foreach (var title in pUser.Titles.Values)
                            pMsg.Append(title.Title);
                        pUser.Send(pMsg);
                    }

                    break;
                }

                #endregion

                #region Select Title

                case TitleAction.SelectTitle:
                {
                    if (pMsg.SelectedTitle == UserTitle.None)
                    {
                        pUser.Title = UserTitle.None;
                        pUser.Send(pMsg);
                    }
                    else if (pUser.Titles.ContainsKey(pMsg.SelectedTitle))
                    {
                        pUser.Title = pMsg.SelectedTitle;
                        pUser.Send(pMsg);
                    }
                    else
                    {
                        return;
                    }

                    pUser.Screen.RefreshSpawnForObservers();
                    break;
                }

                #endregion
            }
        }
    }
}