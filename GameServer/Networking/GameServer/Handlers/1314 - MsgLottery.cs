﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1314 - MsgLottery.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore;
using FtwCore.Common;
using FtwCore.Networking.Packets;
using GameServer.Structures;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgLottery(MsgLottery msg, Character user)
        {
            /**
             * Statistics for Lottery
             * idEvent: 22
             *
             * idType: 0
             *  How many times the character has entered the lottery.
             * idType: 1
             *  How many times the user has tried before Accept.
             * idType: 2
             *  If the user has accepted the last prize.
             *
             * Rules:
             * If idType(2) == 0 and the user is trying to open another box then the user will receive the latest prize. The same if disconnecting.
             *
             */
            switch (msg.Request)
            {
                case LotteryRequest.Accept:
                {
                    user.AcceptLotteryPrize();
                    break;
                }

                case LotteryRequest.AddJade:
                {
                    user.LotteryTryAgain();
                    break;
                }

                case LotteryRequest.Continue:
                {
                    if (user.Statistics.GetValue(22) >= Lottery.MAX_ATTEMPTS)
                    {
                        return;
                    }

                    if (!user.UserPackage.SpendItem(SpecialItem.SMALL_LOTTERY_TICKET, 3))
                    {
                        user.SendSysMessage(Language.StrEmbedNoRequiredItem);
                        return;
                    }

                    Lottery.GenerateItem(user, true);
                    break;
                }
            }
        }
    }
}