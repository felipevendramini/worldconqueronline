﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - 1151 - MsgRank.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Flower;
using GameServer.Structures.Managers;

#endregion

namespace GameServer.Networking.GameServer
{
    public static partial class Handlers
    {
        public static void HandleMsgRank(Character user, MsgRank msg)
        {
            switch (msg.Mode)
            {
                // request ranking
                case 1:
                {
                    switch (msg.Type)
                    {
                        case RankType.Flower:
                        {
                            if (user.Gender != 2)
                                return;

                            FlowerObject obj = ServerKernel.FlowerRanking.FetchUser(user.Identity);
                            if (obj == null)
                            {
                                ServerKernel.FlowerRanking.AddFlowers(FlowerType.RED_ROSE, 0, user.Identity);
                                obj = ServerKernel.FlowerRanking.FetchUser(user.Identity);
                                if (obj == null)
                                    return;
                            }

                            int red = obj.RedRoseRanking;
                            int white = obj.WhiteRoseRanking;
                            int orchid = obj.OrchidsRanking;
                            int tulip = obj.TulipsRanking;

                            FlowerObject[] ranking = null;
                            switch (msg.FlowerIdentity)
                            {
                                case MsgRank.RED_ROSE_TYPE:
                                    if (red >= 0 && red < 100)
                                    {
                                        ranking = ServerKernel.FlowerRanking.RedRosesRanking();
                                        break;
                                    }

                                    return;
                                case MsgRank.WHITE_ROSE_TYPE:
                                    if (white >= 0 && white < 100)
                                    {
                                        ranking = ServerKernel.FlowerRanking.WhiteRosesRanking();
                                        break;
                                    }

                                    return;
                                case MsgRank.ORCHIDS_TYPE:
                                    if (orchid >= 0 && orchid < 100)
                                    {
                                        ranking = ServerKernel.FlowerRanking.OrchidsRanking();
                                        break;
                                    }

                                    return;
                                case MsgRank.TULIPS_TYPE:
                                    if (tulip >= 0 && tulip < 100)
                                    {
                                        ranking = ServerKernel.FlowerRanking.TulipsRanking();
                                        break;
                                    }

                                    return;
                            }

                            if (ranking == null)
                                return;

                            int index = msg.PageNumber * 10;
                            int count = 0;
                            if (index >= ranking.Length)
                                return;

                            MsgRank msgRank = new MsgRank
                            {
                                Mode = 1
                            };
                            for (; index < ranking.Length && count < 10; index++, count++)
                            {
                                if (ranking[index] == null)
                                    break;
                                switch (msg.FlowerIdentity)
                                {
                                    case MsgRank.RED_ROSE_TYPE:
                                        msgRank.AddToRanking((uint) (index + 1), ranking[index].RedRoses,
                                            ranking[index].PlayerIdentity, ranking[index].PlayerName);
                                        break;
                                    case MsgRank.WHITE_ROSE_TYPE:
                                        msgRank.AddToRanking((uint) (index + 1), ranking[index].WhiteRoses,
                                            ranking[index].PlayerIdentity, ranking[index].PlayerName);
                                        break;
                                    case MsgRank.ORCHIDS_TYPE:
                                        msgRank.AddToRanking((uint) (index + 1), ranking[index].Orchids,
                                            ranking[index].PlayerIdentity, ranking[index].PlayerName);
                                        break;
                                    case MsgRank.TULIPS_TYPE:
                                        msgRank.AddToRanking((uint) (index + 1), ranking[index].Tulips,
                                            ranking[index].PlayerIdentity, ranking[index].PlayerName);
                                        break;
                                }
                            }

                            user.Send(msgRank);
                            return;
                        }

                        case RankType.ChiDragon:
                        case RankType.ChiPhoenix:
                        case RankType.ChiTiger:
                        case RankType.ChiTurtle:
                        {
                            List<TrainingVitalityStruct> rank = null;
                            uint usrPos = uint.MaxValue;
                            switch (msg.Type)
                            {
                                case RankType.ChiDragon:
                                    rank = TrainingVitalityManager.GetDragonTop50();
                                    usrPos = TrainingVitalityManager.GetUserDragonRanking(user.Identity);
                                    break;
                                case RankType.ChiPhoenix:
                                    rank = TrainingVitalityManager.GetPhoenixTop50();
                                    usrPos = TrainingVitalityManager.GetUserPhoenixRanking(user.Identity);
                                    break;
                                case RankType.ChiTiger:
                                    rank = TrainingVitalityManager.GetTigerTop50();
                                    usrPos = TrainingVitalityManager.GetUserTigerRanking(user.Identity);
                                    break;
                                case RankType.ChiTurtle:
                                    rank = TrainingVitalityManager.GetTurtleTop50();
                                    usrPos = TrainingVitalityManager.GetUserTurtleRanking(user.Identity);
                                    break;
                            }

                            if (rank == null)
                                return;

                            int index = msg.PageNumber * 10;
                            int count = 0;
                            if (index >= rank.Count)
                                return;

                            msg.Unknown = (ushort) rank.Count;

                            if (usrPos < 50)
                            {
                                msg.AddToRanking(usrPos + 1, rank[(int) usrPos].GetTotalPoints(), user.Identity,
                                    user.Name);
                            }

                            for (; index < rank.Count && count < 10; index++, count++)
                            {
                                if (rank[index].Equals(default(TrainingVitalityStruct)))
                                    break;

                                //if (rank[index].UserIdentity == user.Identity)
                                //    continue;

                                msg.AddToRanking((uint) (index + 1), rank[index].GetTotalPoints(),
                                    rank[index].UserIdentity, rank[index].PlayerName);
                            }

                            user.Send(msg);
                            return;
                        }
                    }

                    break;
                }

                case 2:
                {
                    if (user.Gender != 2)
                        return;

                    FlowerObject obj = ServerKernel.FlowerRanking.FetchUser(user.Identity);
                    if (obj == null)
                    {
                        ServerKernel.FlowerRanking.AddFlowers(FlowerType.RED_ROSE, 0, user.Identity);
                        obj = ServerKernel.FlowerRanking.FetchUser(user.Identity);
                        if (obj == null)
                            return;
                    }

                    MsgFlower pFlower = new MsgFlower
                    {
                        Mode = 2,
                        RedRoses = obj.RedRoses,
                        RedRosesToday = obj.RedRosesToday,
                        WhiteRoses = obj.WhiteRoses,
                        WhiteRosesToday = msg.WhiteRosesToday,
                        Orchids = obj.Orchids,
                        OrchidsToday = obj.OrchidsToday,
                        Tulips = obj.Tulips,
                        TulipsToday = obj.TulipsToday
                    };
                    user.Send(pFlower);

                    int red = obj.RedRoseRanking;
                    int white = obj.WhiteRoseRanking;
                    int orchid = obj.OrchidsRanking;
                    int tulip = obj.TulipsRanking;

                    if (red >= 0 && red < 100)
                    {
                        if (red < 3)
                        {
                            user.FlowerCharm = 30010002;
                        }
                        else if (red < 10)
                        {
                            user.FlowerCharm = 30040002;
                        }
                        else if (red < 25)
                        {
                            user.FlowerCharm = 30040002;
                        }
                        else
                        {
                            user.FlowerCharm = 30110002;
                        }

                        MsgRank pRank = new MsgRank();
                        pRank.SetCharm(1, user.Name, obj.RedRoses, user.Identity, user.FlowerCharm);
                        user.Send(pRank);
                    }

                    if (white >= 0 && white < 100)
                    {
                        if (white < 3)
                        {
                            user.FlowerCharm = 30010102;
                        }
                        else if (white < 10)
                        {
                            user.FlowerCharm = 30020102;
                        }
                        else if (white < 25)
                        {
                            user.FlowerCharm = 30040102;
                        }
                        else
                        {
                            user.FlowerCharm = 30110102;
                        }

                        MsgRank pRank = new MsgRank();
                        pRank.SetCharm(1, user.Name, obj.WhiteRoses, user.Identity, user.FlowerCharm);
                        user.Send(pRank);
                    }

                    if (orchid >= 0 && orchid < 100)
                    {
                        if (orchid < 3)
                        {
                            user.FlowerCharm = 30010202;
                        }
                        else if (orchid < 10)
                        {
                            user.FlowerCharm = 30020202;
                        }
                        else if (orchid < 25)
                        {
                            user.FlowerCharm = 30040202;
                        }
                        else
                        {
                            user.FlowerCharm = 30110202;
                        }

                        MsgRank pRank = new MsgRank();
                        pRank.SetCharm(1, user.Name, obj.Orchids, user.Identity, user.FlowerCharm);
                        user.Send(pRank);
                    }

                    if (tulip >= 0 && tulip < 100)
                    {
                        if (tulip < 3)
                        {
                            user.FlowerCharm = 30010302;
                        }
                        else if (tulip < 10)
                        {
                            user.FlowerCharm = 30020302;
                        }
                        else if (tulip < 25)
                        {
                            user.FlowerCharm = 30040302;
                        }
                        else
                        {
                            user.FlowerCharm = 30110302;
                        }

                        MsgRank pRank = new MsgRank();
                        pRank.SetCharm(1, user.Name, obj.Tulips, user.Identity, user.FlowerCharm);
                        user.Send(pRank);
                    }

                    msg = new MsgRank {Mode = 5};
                    user.Send(msg);
                    break;
                }

                default:
                    Program.WriteLog($"MsgRank.Mode({msg.Mode}) is not handled");
                    break;
            }
        }
    }
}