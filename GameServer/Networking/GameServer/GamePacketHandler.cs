﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - GamePacketHandler.cs
// Last Edit: 2020/01/16 22:14
// Created: 2019/12/07 18:33
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using FtwCore.Common;
using FtwCore.Networking.Packets;

#endregion

namespace GameServer.Networking.GameServer
{
    public class GamePacketHandler
    {
        #region 1001 - MsgRegister

        [PacketHandlerType(PacketType.MsgRegister)]
        public void ProcessRegister(User pClient, byte[] pBuffer)
        {
            if (pClient.Character != null)
                return;
            Handlers.HandleMsgRegister(pClient, new MsgRegister(pBuffer));
        }

        #endregion

        #region 1004 - MsgTalk

        [PacketHandlerType(PacketType.MsgTalk)]
        public void ProcessTalk(User user, byte[] buffer)
        {
            Handlers.HandleMsgTalk(user.Character, new MsgTalk(buffer));
        }

        #endregion

        #region 1009 - MsgItem

        [PacketHandlerType(PacketType.MsgItem)]
        public void ProcessItem(User user, byte[] buffer)
        {
            Handlers.HandleMsgItem(user.Character, new MsgItem(buffer));
        }

        #endregion

        #region 1015 - MsgName

        [PacketHandlerType(PacketType.MsgName)]
        public void ProcessName(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;
            Handlers.HandleMsgString(user.Character, new MsgName(buffer));
        }

        #endregion

        #region 1019 - MsgFriend

        [PacketHandlerType(PacketType.MsgFriend)]
        public void ProcessFriend(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;
            Handlers.HandleMsgFriend(user.Character, new MsgFriend(buffer));
        }

        #endregion

        #region 1022 - MsgInteract

        [PacketHandlerType(PacketType.MsgInteract)]
        public void ProcessInteract(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;
            Handlers.HandleMsgInteract(user.Character, new MsgInteract(buffer));
        }

        #endregion

        #region 1023 - MsgTeam

        [PacketHandlerType(PacketType.MsgTeam)]
        public void ProcessTeam(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;
            Handlers.HandleMsgTeam(user.Character, new MsgTeam(buffer));
        }

        #endregion

        #region 1024 - MsgAllot

        [PacketHandlerType(PacketType.MsgAllot)]
        public void ProcessAllot(User user, byte[] buffer)
        {
            if (user.Character == null)
                return;

            Handlers.HandleMsgAllot(user.Character, new MsgAllot(buffer));
        }

        #endregion

        #region 1027 - MsgGemEmbed

        [PacketHandlerType(PacketType.MsgGemEmbed)]
        public void ProcessGemEmbed(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;
            Handlers.HandleMsgGemEmbed(user.Character, new MsgGemEmbed(buffer));
        }

        #endregion

        #region 1033 - MsgData

        [PacketHandlerType(PacketType.MsgData)]
        public void ProcessData(User user, byte[] buffer)
        {
            // todo handle data bot??
        }

        #endregion

        #region 1037 - MsgPing

        [PacketHandlerType(PacketType.MsgPing)]
        public void ProcessPing(User user, byte[] buffer)
        {
            // todo handle Ping??? usage??
        }

        #endregion

        #region 1038 - MsgSolidify

        [PacketHandlerType(PacketType.MsgSolidify)]
        public void ProcessSolidify(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;
            Handlers.HandleMsgSolidify(user.Character, new MsgSolidify(buffer));
        }

        #endregion

        #region 1040 - MsgPlayerAttribInfo

        [PacketHandlerType(PacketType.MsgPlayerAttribInfo)]
        public void ProcessPlayerAttribInfo(User user, byte[] buffer)
        {
            if (user.Character == null)
                return;
            Handlers.HandleMsgPlayerAttribInfo(user.Character, new MsgPlayerAttribInfo(buffer));
        }

        #endregion

        #region 1045 - MsgMailOperation

        [PacketHandlerType(PacketType.MsgMailOperation)]
        public void ProcessMailOperation(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;

            Handlers.HandleMsgMailOperation(user.Character, new MsgMailOperation(buffer));
        }

        #endregion

        #region 1046 - MsgMailList

        [PacketHandlerType(PacketType.MsgMailList)]
        public void ProcessMailList(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;
            Handlers.HandleMsgMailList(user.Character, new MsgMailList(buffer));
        }

        #endregion

        #region 1052 - MsgConnect

        [PacketHandlerType(PacketType.MsgConnect)]
        public void ProcessConnect(User pClient, byte[] pBuffer)
        {
            if (pClient.Character != null)
                return;
            Handlers.HandleMsgConnect(pClient, new MsgConnect(pBuffer));
        }

        #endregion

        #region 1056 - MsgTrade

        [PacketHandlerType(PacketType.MsgTrade)]
        public void ProcessTrade(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;
            Handlers.HandleMsgTrade(user.Character, new MsgTrade(buffer));
        }

        #endregion

        #region 1058 - MsgSynpOffer

        [PacketHandlerType(PacketType.MsgSynpOffer)]
        public void ProcessSynpOffer(User user, byte[] buffer)
        {
            if (user?.Character?.Syndicate == null)
                return;

            user.Character.SyndicateMember.SendCharacterInformation();
        }

        #endregion

        #region 1063 - MsgSelfSynMemAwardRank

        [PacketHandlerType(PacketType.MsgSelfSynMemAwardRank)]
        public void ProcessSelfSynMemAwardRank(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;

            Handlers.HandleMsgSelfSynMemAwardRank(user.Character, new MsgSelfSynMemAwardRank(buffer));
        }

        #endregion

        #region 1066 - MsgMeteSpecial

        [PacketHandlerType(PacketType.MsgMeteSpecial)]
        public void ProcessMeteSpecial(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;
            Handlers.HandleMsgMeteSpecial(user.Character, new MsgMeteSpecial(buffer));
        }

        #endregion

        #region 1070 - MsgHangUp

        [PacketHandlerType(PacketType.MsgHangUp)]
        public void ProcessHangUp(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;

            Handlers.HandleMsgHangUp(user.Character, new MsgHangUp(buffer));
        }

        #endregion

        #region 1101 - MsgMapItem

        [PacketHandlerType(PacketType.MsgMapItem)]
        public void ProcessMapItem(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;
            Handlers.HandleMsgMapItem(user.Character, new MsgMapItem(buffer));
        }

        #endregion

        #region 1102 - MsgAccountSoftKb

        [PacketHandlerType(PacketType.MsgAccountSoftKb)]
        public void ProcessAccountSoftKb(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;

            Handlers.HandleMsgAccountSoftKb(user.Character, new MsgAccountSoftKb(buffer));
        }

        #endregion

        #region 1107 - MsgSyndicate

        [PacketHandlerType(PacketType.MsgSyndicate)]
        public void ProcessSyndicate(User user, byte[] buffer)
        {
            Handlers.HandleMsgSyndicate(user.Character, new MsgSyndicate(buffer));
        }

        #endregion

        #region 1130 - MsgTitle

        [PacketHandlerType(PacketType.MsgTitle)]
        public void ProcessTitle(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;

            Handlers.HandleMsgTitle(user.Character, new MsgTitle(buffer));
        }

        #endregion

        #region 1134 - MsgTaskStatus

        [PacketHandlerType(PacketType.MsgTaskStatus)]
        public void ProcessTaskStatus(User user, byte[] buffer)
        {
        }

        #endregion

        #region 1136 - MsgAchievement

        [PacketHandlerType(PacketType.MsgAchievement)]
        public void ProcessAchievement(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;

            Handlers.HandleMsgAchievement(user.Character, new MsgAchievement(buffer));
        }

        #endregion

        #region 1150 - MsgFlower

        [PacketHandlerType(PacketType.MsgFlower)]
        public void ProcessFlower(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;
            Handlers.HandleMsgFlower(user.Character, new MsgFlower(buffer));
        }

        #endregion

        #region 1151 - MsgRank

        [PacketHandlerType(PacketType.MsgRank)]
        public void ProcessRank(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;
            Handlers.HandleMsgRank(user.Character, new MsgRank(buffer));
        }

        #endregion

        #region 1312 - MsgFamily

        [PacketHandlerType(PacketType.MsgFamily)]
        public void ProcessFamily(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;
            Handlers.HandleMsgFamily(user.Character, new MsgFamily(buffer));
        }

        #endregion

        #region 1313 - MsgFamilyOccupy

        [PacketHandlerType(PacketType.MsgFamilyOccupy)]
        public void ProcessFamilyOccupy(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;
            Handlers.HandleMsgFamilyOccupy(user.Character, new MsgFamilyOccupy(buffer));
        }

        #endregion

        #region 1314 - MsgLottery

        [PacketHandlerType(PacketType.MsgLottery)]
        public void ProcessLottery(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;

            Handlers.HandleMsgLottery(new MsgLottery(buffer), user.Character);
        }

        #endregion

        #region 2030 - MsgNpc

        [PacketHandlerType(PacketType.MsgNpcInfo)]
        public void ProcessNpcInfo(User user, byte[] buffer)
        {
            if (user.Character == null)
                return;
            Handlers.HandleMsgNpcInfo(user.Character, new MsgNpcInfo(buffer));
        }

        #endregion

        #region 2031 - MsgNpc

        [PacketHandlerType(PacketType.MsgNpc)]
        public void ProcessNpc(User user, byte[] buffer)
        {
            if (user.Character == null)
                return;
            Handlers.HandleMsgTaskDialog(user.Character, new MsgTaskDialog(buffer));
        }

        #endregion

        #region 2032 - MsgTaskDialog

        [PacketHandlerType(PacketType.MsgTaskDialog)]
        public void ProcessTaskDialog(User user, byte[] buffer)
        {
            if (user.Character == null)
                return;
            Handlers.HandleMsgTaskDialog(user.Character, new MsgTaskDialog(buffer));
        }

        #endregion

        #region 2036 - MsgDataArray

        [PacketHandlerType(PacketType.MsgDataArray)]
        public void ProcessDataArray(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;
            Handlers.HandleMsgDataArray(user.Character, new MsgDataArray(buffer));
        }

        #endregion

        #region 2046 - MsgTradeBuddy

        [PacketHandlerType(PacketType.MsgTradeBuddy)]
        public void ProcessTradeBuddy(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;

            Handlers.HandleMsgTradeBuddy(user.Character, new MsgTradeBuddy(buffer));
        }

        #endregion

        #region 2048 - MsgEquipLock

        [PacketHandlerType(PacketType.MsgEquipLock)]
        public void ProcessEquipLock(User user, byte[] buffer)
        {
            Handlers.HandleMsgEquipLock(user.Character, new MsgEquipLock(buffer));
        }

        #endregion

        #region 2050 - MsgPigeon

        [PacketHandlerType(PacketType.MsgPigeon)]
        public void ProcessPigeon(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;
            Handlers.HandleMsgPigeon(user.Character, new MsgPigeon(buffer));
        }

        #endregion

        #region 2064 - MsgPeerage

        [PacketHandlerType(PacketType.MsgPeerage)]
        public void ProcessPeerage(User client, byte[] buffer)
        {
            if (client.Character == null)
                return;

            Handlers.HandleMsgPeerage(client.Character, new MsgPeerage(buffer));
        }

        #endregion

        #region 2068 - MsgQuiz

        [PacketHandlerType(PacketType.MsgQuiz)]
        public void ProcessQuiz(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;

            Handlers.HandleMsgQuiz(user.Character, new MsgQuiz(buffer));
        }

        #endregion

        #region 2076 - MsgQuench

        [PacketHandlerType(PacketType.MsgQuench)]
        public void ProcessQuench(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;

            Handlers.HandleMsgQuench(user.Character, new MsgQuench(buffer));
        }

        #endregion

        #region 2080 - MsgChangeName

        [PacketHandlerType(PacketType.MsgChangeName)]
        public void ProcessChangeName(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;

            Handlers.HandleMsgChangeName(new MsgChangeName(buffer), user.Character);
        }

        #endregion

        #region 2101 - MsgFactionRankInfo

        [PacketHandlerType(PacketType.MsgFactionRankInfo)]
        public void ProcessFactionRankInfo(User user, byte[] buffer)
        {
            if (user?.Character?.Syndicate == null)
                return;
            Handlers.HandleMsgFactionRankInfo(user.Character, new MsgFactionRankInfo(buffer));
        }

        #endregion

        #region 2102 - MsgSynMemberList

        [PacketHandlerType(PacketType.MsgSynMemberList)]
        public void ProcessSynMemberInfo(User user, byte[] buffer)
        {
            if (user?.Character?.Syndicate == null)
                return;
            Handlers.HandleMsgSynMemberList(user.Character, new MsgSynMemberList(buffer));
        }

        #endregion

        #region 2110 - MsgSuperFlag

        [PacketHandlerType(PacketType.MsgSuperFlag)]
        public void ProcessSuperFlag(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;
            Handlers.HandleSuperFlag(user.Character, new MsgSuperFlag(buffer));
        }

        #endregion

        #region 2202 - MsgWeaponsInfo

        [PacketHandlerType(PacketType.MsgWeaponsInfo)]
        public void ProcessWeaponsInfo(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;
            Handlers.HandleMsgWeaponsInfo(user.Character, new MsgWeaponsInfo(buffer));
        }

        #endregion

        #region 2203 - MsgTotemPole

        [PacketHandlerType(PacketType.MsgTotemPole)]
        public void ProcessTotemPole(User user, byte[] buffer)
        {
            if (user?.Character?.Syndicate == null)
                return;

            Handlers.HandleMsgTotemPole(user.Character, new MsgTotemPole(buffer));
        }

        #endregion

        #region 2205 - MsgQualifyingInteractive

        [PacketHandlerType(PacketType.MsgQualifyingInteractive)]
        public void ProcessQualifyingInteractive(User user, byte[] buffer)
        {
            Handlers.HandleMsgQualifyingInteractive(user.Character, new MsgQualifyingInteractive(buffer));
        }

        #endregion

        #region 2206 - MsgQualifyingFightersList

        [PacketHandlerType(PacketType.MsgQualifyingFightersList)]
        public void ProcessQualifyingFightersList(User user, byte[] buffer)
        {
            Handlers.HandleMsgQualifyingFightersList(user.Character, new MsgQualifyingFightersList(buffer));
        }

        #endregion

        #region 2207 - MsgQualifyingRank

        [PacketHandlerType(PacketType.MsgQualifyingRank)]
        public void ProcessQualifyingRank(User user, byte[] buffer)
        {
            Handlers.HandleMsgQualifyingRank(user.Character, new MsgQualifyingRank(buffer));
        }

        #endregion

        #region 2208 - MsgQualifyingSeasonRankList

        [PacketHandlerType(PacketType.MsgQualifyingSeasonRankList)]
        public void ProcessQualifyingSeasonRankList(User user, byte[] buffer)
        {
            Handlers.HandleMsgQualifyingSeasonRankList(user.Character, new MsgQualifyingSeasonRankList(buffer));
        }

        #endregion

        #region 2209 - MsgQualifyingDetailInfo

        [PacketHandlerType(PacketType.MsgQualifyingDetailInfo)]
        public void ProcessQualifyingDetailInfo(User user, byte[] buffer)
        {
            Handlers.HandleMsgQualifyingSeasonRankList(user.Character, new MsgQualifyingSeasonRankList(buffer));
        }

        #endregion

        #region 2211 - MsgArenicWitness

        [PacketHandlerType(PacketType.MsgArenicWitness)]
        public void ProcessArenicWitness(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;

            Handlers.HandleMsgArenicWitness(user.Character, new MsgArenicWitness(buffer));
        }

        #endregion

        #region 2219 - MsgPKEliteMatchInfo

        [PacketHandlerType(PacketType.MsgPkEliteMatchInfo)]
        public void ProcessPkEliteMatchInfo(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;

            Handlers.HandleMsgPkEliteMatchInfo(user.Character, new MsgPKEliteMatchInfo(buffer));
        }

        #endregion

        #region 2220 - MsgPKStatistic

        [PacketHandlerType(PacketType.MsgPkStatistic)]
        public void ProcessPkStatistic(User user, byte[] buffer)
        {
            if (user?.Character?.PkExploit == null)
                return;

            Handlers.HandleMsgPKStatistic(user.Character, new MsgPkStatistic(buffer));
        }

        #endregion

        #region 2223 - MsgElitePkGameInfo

        [PacketHandlerType(PacketType.MsgElitePkGameRankInfo)]
        public void ProcessElitePkGameRankInfo(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;

            Handlers.HandleMsgElitePkGameInfo(user.Character, new MsgElitePKGameRankInfo(buffer));
        }

        #endregion

        #region 2224 - MsgWarFlag

        [PacketHandlerType(PacketType.MsgWarFlag)]
        public void ProcessWarFlag(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;
            Handlers.HandleMsgWarFlag(user.Character, new MsgWarFlag(buffer));
        }

        #endregion

        #region 2225 - MsgSynRecuitAdvertising

        [PacketHandlerType(PacketType.MsgSynRecuitAdvertising)]
        public void ProcessRecruitAdvertising(User user, byte[] buffer)
        {
            if (user.Character == null)
                return;
            Handlers.HandleMsgSynRecruitAdvertising(user.Character, new MsgSynRecuitAdvertising(buffer));
        }

        #endregion

        #region 2226 - MsgSynRecruitAdvertisingList

        [PacketHandlerType(PacketType.MsgSynRecruitAdvertisingList)]
        public void ProcessSynRecruitAdvertisingList(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;

            MsgSynRecruitAdvertisingList msg = new MsgSynRecruitAdvertisingList(buffer);
            ServerKernel.SyndicateRecruitment.SendSyndicates(msg.StartIndex, msg, user.Character);
        }

        #endregion

        #region 2227 - MsgSynRecruitAdvertisingOpt

        [PacketHandlerType(PacketType.MsgSynRecruitAdvertisingOpt)]
        public void ProcessSynRecruitAdvertisingOpt(User user, byte[] buffer)
        {
            if (user.Character == null)
                return;
            Handlers.HandleMsgSynAdvertisingOpt(user.Character, new MsgSynRecruitAdvertisingOpt(buffer));
        }

        #endregion

        #region 2242 - MsgTeamArenaFightingTeamList

        [PacketHandlerType(PacketType.MsgTeamArenaFightingTeamList)]
        public void ProcessTeamArenaFightingTeamList(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;

            Handlers.HandleMsgTeamArenaFightingTeamList(user.Character, new MsgTeamArenaFightingTeamList(buffer));
        }

        #endregion

        #region 2244 - MsgTeamArenaYTop10List

        [PacketHandlerType(PacketType.MsgTeamArenaYTop10List)]
        public void ProcessTeamArenaYTop10List(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;

            Handlers.HandleMsgTeamArenaYTop10List(user.Character, new MsgTeamArenaYTop10List(buffer));
        }

        #endregion

        #region 2245 - MsgTeamArenaHeroData

        [PacketHandlerType(PacketType.MsgTeamArenaHeroData)]
        public void ProcessTeamArenaHeroData(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;

            Handlers.HandleMsgTeamArenaHeroData(user.Character, new MsgTeamArenaHeroData(buffer));
        }

        #endregion

        #region 2261 - Msg2ndPw

        [PacketHandlerType(PacketType.Msg2ndPsw)]
        public void Process2ndPw(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;

            Handlers.HandleMsg2ndPw(user.Character, new Msg2ndPsw(buffer));
        }

        #endregion

        #region 2320 - MsgSubPro

        [PacketHandlerType(PacketType.MsgSubPro)]
        public void ProcessSubPro(User user, byte[] buffer)
        {
            if (user.Character == null)
                return;
            Handlers.HandleMsgSubPro(user.Character, new MsgSubPro(buffer));
        }

        #endregion

        #region 2420 - MsgNationality

        [PacketHandlerType(PacketType.MsgNationatlity)]
        public void ProcessNationality(User user, byte[] buffer)
        {
            if (user.Character == null)
                return;
            Handlers.HandleMsgNationality(user.Character, new MsgNationality(buffer));
        }

        #endregion

        #region 2533 - MsgTrainingVitality

        [PacketHandlerType(PacketType.MsgTrainingVitality)]
        public void ProcessTrainingVitality(User user, byte[] buffer)
        {
            if (user.Character == null)
                return;
            Handlers.HandleMsgTrainingVitality(user.Character, new MsgTrainingVitality(buffer));
        }

        #endregion

        #region 2700 - MsgOwnKongfuBase

        [PacketHandlerType(PacketType.MsgOwnKongfuBase)]
        public void ProcessKongfuBase(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;

            Handlers.HandleMsgOwnKongfuBase(user.Character, new MsgOwnKongfuBase(buffer));
        }

        #endregion

        #region 2702 - MsgOwnKongfuImproveFeedback

        [PacketHandlerType(PacketType.MsgOwnKongfuImproveFeedback)]
        public void ProcessOwnKongfuImproveFeedback(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;
            Handlers.HandleMsgOwnKongfuImproveFeedback(user.Character, new MsgOwnKongfuImproveFeedback(buffer));
        }

        #endregion

        #region 2703 - MsgOwnKongRank

        [PacketHandlerType(PacketType.MsgOwnKongRank)]
        public void ProcessOwnKongRank(User user, byte[] buffer)
        {
            if (user?.Character == null)
                return;

            Handlers.HandleOwnKongRank(user.Character, new MsgOwnKongRank(buffer));
        }

        #endregion

        #region 10005 - MsgWalk

        [PacketHandlerType(PacketType.MsgWalk)]
        public void ProcessWalk(User client, byte[] buffer)
        {
            Handlers.HandleMsgWalk(client.Character, new MsgWalk(buffer));
        }

        #endregion

        #region 10010 - MsgAction

        [PacketHandlerType(PacketType.MsgAction)]
        public void ProcessAction(User pClient, byte[] pBuffer)
        {
            if (pClient.Character == null)
                return;

            Handlers.HandleMsgAction(pClient.Character, new MsgAction(pBuffer));
        }

        #endregion

        /// <summary>
        ///     This function reports a missing packet handler to the console. It writes the length and type of the
        ///     packet, then a packet dump to the console.
        /// </summary>
        /// <param name="packet">The packet buffer being reported.</param>
        public static void Report(byte[] packet)
        {
            ushort length = BitConverter.ToUInt16(packet, 0);
            ushort identity = BitConverter.ToUInt16(packet, 2);

            // Print the packet and the packet header:
            Program.WriteLog($"Missing Packet Handler: {identity} (Length: {length})",
                LogType.DEBUG);
            ServerKernel.Log.WriteToFile(PacketDump.Hex(packet), "missing_packet");
        }
    }
}