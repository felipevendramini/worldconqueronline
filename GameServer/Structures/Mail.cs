﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Mail.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region

#endregion

#region References

using System;
using System.Collections.Generic;
using System.Linq;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Items;
using Newtonsoft.Json;

#endregion

namespace GameServer.Structures
{
    public sealed class UserMailBox
    {
        public const int MAX_PERPAGE = 7;

        private Character m_pUser;
        private List<MailMessage> m_lEmails = new List<MailMessage>();

        public UserMailBox(Character user)
        {
            m_pUser = user;
        }

        public void Init()
        {
            IList<MailEntity> emails = new MailRepository().GetByUser(m_pUser.Identity);
            foreach (var mail in emails)
            {
                MailMessage msg = new MailMessage(m_pUser, mail);
                if (msg.Deleted)
                    continue;
                m_lEmails.Add(msg);
            }

            Notify();
        }

        public void Notify()
        {
            if (m_lEmails.Any(x => !x.IsRead || x.IsClaimable))
                m_pUser.Send(new MsgMailNotify {Notify = (MailNotification) 3});
        }

        public void ReadMail(uint idMail)
        {
            MailMessage mail = m_lEmails.FirstOrDefault(x => x.Identity == idMail);
            if (mail == null)
                return;

            m_pUser.Send(new MsgMailContent(mail.Content)
            {
                MailIdentity = mail.Identity
            });
            mail.MarkAsRead();
        }

        public void SendList(int nPage = 0)
        {
            MsgMailList msg = new MsgMailList();
            msg.Page = (uint) nPage;
            msg.MaxPages = (uint) Math.Ceiling(m_lEmails.Count / 7f);
            for (int i = nPage * MAX_PERPAGE; i % MAX_PERPAGE < MAX_PERPAGE && i < m_lEmails.Count; i++)
            {
                MailMessage mail = m_lEmails[i];
                msg.AppendEmail(mail.Identity, mail.SenderName, mail.Title, mail.Money, mail.ConquerPoints, mail.RemainingTime, 1, mail.Itemtype);
            }
            m_pUser.Send(msg);
        }

        public MailMessage GetMail(uint id)
        {
            return m_lEmails.FirstOrDefault(x => x.Identity == id);
        }

        public void Refresh()
        {
            m_lEmails.Clear();
            Init();
        }

        public static bool SendEmail(uint idTarget, string szName, string szTitle, string szContent, int duration = 0, uint nMoney = 0, uint nEmoney = 0, int nAttachType = 0,
            uint idItemtype = 0)
        {
            MailEntity entity = new MailEntity
            {
                TargetIdentity = idTarget,
                TargetName = szName,
                Money = nMoney,
                Emoney = nEmoney,
                ExpireTime = duration == 0 ? (DateTime?) null : DateTime.Now.AddSeconds(duration),
                Content = szContent,
                SendTime = DateTime.Now,
                SenderIdentity = 0,
                SenderName = "SYSTEM",
                Title = szTitle
            };
            if (!new MailRepository().Save(entity))
                return false;

            if (idItemtype == 0)
                return true;

            MailAttachmentEntity attach = new MailAttachmentEntity
            {
                ItemIdentity = (uint)nAttachType,
                Itemtype = idItemtype,
                MailIdentity = entity.Identity
            };
            new MailAttachmentRepository().Save(attach);

            ServerKernel.UserManager.GetUser(idTarget)?.MailBox.Refresh();

            return true;
        }
    }

    public sealed class MailMessage
    {
        public const int READ = 0x1;
        public const int DELETED = 0x2;
        public const int MONEY_CLAIMED = 0x4;
        public const int EMONEY_CLAIMED = 0x8;

        private Character m_pOwner;
        private Character m_pSender;
        private string m_szMessage;
        private MailEntity m_dbMail;

        public MailAttachment Attachment = null;

        public MailMessage(Character target, MailEntity mail = null)
        {
            m_pOwner = target;
            m_dbMail = mail ?? new MailEntity();

            Attachment = new MailAttachment(new MailAttachmentRepository().GetByMail(Identity));
            m_szMessage = mail?.Content ?? "";
        }

        public MailMessage(Character sender, Character target, MailEntity mail = null)
        {
            m_pOwner = target;
            m_pSender = sender;
            m_dbMail = mail ?? new MailEntity();

            m_szMessage = mail?.Content ?? "";
        }

        public uint Identity => m_dbMail?.Identity ?? 0;
        public uint SenderIdentity => m_dbMail?.SenderIdentity ?? 0;
        public string SenderName => m_dbMail?.SenderName ?? "";
        public uint TargetIdentity => m_dbMail?.TargetIdentity ?? 0;
        public string TargetName => m_dbMail?.TargetName ?? "";

        public uint Money => HasMoneyToClaim ? (m_dbMail?.Money ?? 0) : 0;

        public uint ConquerPoints => HasEMoneyToClaim ? m_dbMail?.Emoney ?? 0 : 0;

        public uint Itemtype
        {
            get
            {
                if (((MailAttachmentEntity)Attachment) == null || !Attachment.IsClaimable)
                    return 0;
                return ((MailAttachmentEntity)Attachment).Itemtype;
            }
        }

        public uint RemainingTime
        {
            get
            {
                if (m_dbMail.ExpireTime != null)
                {
                    return (uint) (DateTime.Now - m_dbMail.ExpireTime.Value).TotalSeconds;
                }
                return int.MaxValue;
            }
        }

        public string Title
        {
            get => m_dbMail?.Title ?? "";
            set => m_dbMail.Title = value;
        }

        public string Content
        {
            get => m_szMessage;
            set => m_szMessage = value.Substring(0, 512);
        }

        public int Flag
        {
            get => m_dbMail?.Flag ?? 0;
            set
            {
                if (m_dbMail == null)
                    return;
                m_dbMail.Flag = value;
            }
        }

        public bool IsRead => (Flag & READ) != 0;

        public bool IsClaimable => Attachment?.IsClaimable == true || HasMoneyToClaim || HasEMoneyToClaim;

        public void Send()
        {
            if (m_dbMail == null)
            {
                m_dbMail = new MailEntity
                {
                    SenderIdentity = m_pSender?.Identity ?? 0,
                    SenderName = m_pSender?.Name ?? "SYSTEM",
                    TargetIdentity = m_pOwner.Identity,
                    TargetName = m_pOwner.Name,
                    Content = Content,
                    SendTime = DateTime.Now
                };
            }

            if (!new MailRepository().Save(m_dbMail))
            {
                Program.WriteLog($"Could not save email: {JsonConvert.SerializeObject(this)}");
            }

            if (Attachment != null)
            {
                MailAttachmentRepository repo = new MailAttachmentRepository();
                ((MailAttachmentEntity)Attachment).MailIdentity = m_dbMail.Identity;
                repo.Save(Attachment);
            }
        }

        public bool ClaimMoney()
        {
            if (Money == 0 || !HasMoneyToClaim)
                return false;

            m_pOwner.AwardMoney((int) Money);

            MarkMoneyClaimed();
            return true;
        }

        public bool ClaimEmoney()
        {
            if (ConquerPoints == 0 || !HasEMoneyToClaim)
                return false;

            m_pOwner.AwardEmoney((int)ConquerPoints, EmoneySourceType.Mail, null);

            MarkEmoneyClaimed();
            return true;
        }

        public bool ClaimItem()
        {
            if (Attachment == null)
                return false;

            if (!Attachment.IsClaimable)
                return false; // already claimed

            if (m_pOwner == null)
                return false; // target doesn't exist

            ItemEntity dbItem;
            MailAttachmentEntity attachment = Attachment;
            if (attachment.ItemIdentity != 0) // existing item
            {
                dbItem = Database.ItemRepository.FetchByIdentity(attachment.ItemIdentity);
                if (dbItem == null)
                    return false; // item doesn't exist.
            }
            else
            {
                dbItem = Item.CreateEntity(attachment.Itemtype);
                if (dbItem == null)
                    return false; // no item to claim
            }

            if (m_pOwner.UserPackage.IsPackFull(attachment.Itemtype, Math.Max((ushort) 1, dbItem.StackAmount)))
                return false; // pack full

            if (m_pOwner.UserPackage.AwardItem(dbItem) != null)
            {
                Attachment.ClaimTime = DateTime.Now;
                Attachment.Flag |= MailAttachment.CLAIMED; // if the item has been successfully awarded, then we mark as claimed
                Attachment.Save();
            }
            return true;
        }

        public bool HasMoneyToClaim => (m_dbMail.Flag & MONEY_CLAIMED) == 0 && m_dbMail.Money > 0;

        public bool HasEMoneyToClaim => (m_dbMail.Flag & EMONEY_CLAIMED) == 0 && m_dbMail.Emoney > 0;
        public bool Deleted => (m_dbMail.Flag & DELETED) != 0;

        public bool MarkMoneyClaimed()
        {
            try
            {
                m_dbMail.Flag |= MONEY_CLAIMED;
                return new MailRepository().Save(m_dbMail);
            }
            catch
            {
                return false;
            }
        }

        public bool MarkEmoneyClaimed()
        {
            try
            {
                m_dbMail.Flag |= EMONEY_CLAIMED;
                return new MailRepository().Save(m_dbMail);
            }
            catch
            {
                return false;
            }
        }

        public bool MarkAsRead()
        {
            try
            {
                m_dbMail.Flag |= READ;
                m_dbMail.ReadTime = DateTime.Now;
                return new MailRepository().Save(m_dbMail);
            }
            catch
            {
                return false;
            }
        }

        public bool MarkAsDeleted()
        {
            try
            {
                if (IsClaimable)
                    return false;
                m_dbMail.Flag |= DELETED;
                return new MailRepository().Save(m_dbMail);
            }
            catch
            {
                return false;
            }
        }
    }

    public sealed class MailAttachment
    {
        public const int CLAIMED = 0x1;

        private MailAttachmentEntity m_dbEntity;

        public MailAttachment(MailAttachmentEntity entity)
        {
            m_dbEntity = entity;
        }

        public bool IsClaimable => m_dbEntity != null && (m_dbEntity.Flag & CLAIMED) == 0;

        public DateTime? ClaimTime
        {
            get => m_dbEntity.ClaimTime;
            set => m_dbEntity.ClaimTime = value;
        }
        public int Flag
        {
            get => m_dbEntity.Flag;
            set => m_dbEntity.Flag = value;
        }

        public bool Save()
        {
            return new MailAttachmentRepository().Save(this);
        }

        public bool Delete()
        {
            return new MailAttachmentRepository().Delete(this);
        }

        public static implicit operator MailAttachmentEntity(MailAttachment attach)
        {
            return attach.m_dbEntity;
        }
    }

    public enum MailAttachmentType
    {
        Itemtype,
        Item,
        Money,
        ConquerPoints
    }
}