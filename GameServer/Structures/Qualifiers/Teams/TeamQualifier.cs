﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - TeamQualifier.cs
// Last Edit: 2020/01/17 19:44
// Created: 2020/01/16 16:02
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using FtwCore.Common;
using FtwCore.Database.Entities;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Managers;
using GameServer.Structures.Qualifiers.User;
using GameServer.World;

#endregion

namespace GameServer.Structures.Qualifiers.Teams
{
    public sealed class TeamArenaQualifierManager
    {
        public const int MIN_LEVEL = 40;
        public const int ARENA_CHECK_INTERVAL_MS = 750;

        private static readonly uint[] m_dwStartPoints =
        {
            1500, // over 70
            2200, // over 90
            2700, // over 100
            3200, // over 110
            4000 // over 120
        };

        /// <summary>
        ///     Users in the Queue awaiting for a match.
        /// </summary>
        private readonly ConcurrentDictionary<uint, Team> m_dicObjects = new ConcurrentDictionary<uint, Team>();

        /// <summary>
        ///     Users registered in the Arena Qualifier.
        /// </summary>
        private readonly ConcurrentDictionary<uint, PlayerQualifierUser> m_dicRanking =
            new ConcurrentDictionary<uint, PlayerQualifierUser>();
        
        private readonly TimeOutMS m_tArenaCheck = new TimeOutMS(ARENA_CHECK_INTERVAL_MS);

        /// <summary>
        ///     Matches being played.
        /// </summary>
        private readonly ConcurrentDictionary<uint, TeamQualifierMatch> m_tMatches =
            new ConcurrentDictionary<uint, TeamQualifierMatch>();

        private Map m_pBaseMap;

        public QualifierState Status { get; private set; } = QualifierState.Disabled;

        public bool Create()
        {
            if (!ServerKernel.Maps.TryGetValue(900000, out m_pBaseMap))
            {
                Program.WriteLog("COULD NOT START ARENA QUALIFIER MAP EXCEPTION", LogType.ERROR);
                return false;
            }

            IList<ArenaEntity> allArena = Database.Arena.FetchAll(PlayerQualifierUser.RANK_TYPE_TEAM);
            if (allArena != null)
            {
                foreach (var arena in allArena)
                {
                    if (!m_dicRanking.TryAdd(arena.PlayerIdentity, new PlayerQualifierUser(arena)))
                        Program.WriteLog($"Duplicate key for user {arena.PlayerIdentity}:{arena.Name}",
                            LogType.WARNING);
                }
            }

            Status = QualifierState.Ready;
            return true;
        }

        public static uint GetInitialPoints(byte level)
        {
            if (level < MIN_LEVEL)
                return 0;
            if (level < 90)
                return m_dwStartPoints[0];
            if (level < 100)
                return m_dwStartPoints[1];
            if (level < 110)
                return m_dwStartPoints[2];
            if (level < 120)
                return m_dwStartPoints[3];
            return m_dwStartPoints[4];
        }

        public bool GenerateFirstData(Character user)
        {
            if (m_dicRanking.TryGetValue(user.Identity, out _)) // existing user
                return false;

            PlayerQualifierUser player = new PlayerQualifierUser(user, PlayerQualifierUser.RANK_TYPE_TEAM);
            return m_dicRanking.TryAdd(user.Identity, player);
        }

        public bool ContainsUser(uint identity)
        {
            return m_dicRanking.ContainsKey(identity);
        }

        public PlayerQualifierUser GetUser(uint identity)
        {
            return m_dicRanking.TryGetValue(identity, out var value) ? value : null;
        }

        public bool IsUserInsideMatch(uint identity)
        {
            return m_tMatches.Values.Any(x =>
                x.Team0.Members.ContainsKey(identity) || x.Team1.Members.ContainsKey(identity));
        }

        public TeamQualifierMatch FindMatch(uint identity)
        {
            return m_tMatches.Values.FirstOrDefault(x =>
                x.Team0.Members.ContainsKey(identity) || x.Team1.Members.ContainsKey(identity));
        }

        public bool IsUserQueued(uint identity)
        {
            return m_dicObjects.ContainsKey(identity) || m_dicObjects.Values.Any(x => x.Members.ContainsKey(identity));
        }

        public bool Inscribe(Character user)
        {
            if (Status == QualifierState.Disabled)
                return false;

            if (!ContainsUser(user.Identity) && !GenerateFirstData(user))
                return false;

            /**
             * Only the team leader can apply.
             */
            if (user.Team?.Leader.Identity != user.Identity)
                return false;

            Team team = user.Team;
            if (IsUserQueued(user.Identity) || team.Members.Values.Any(x => IsUserQueued(x.Identity) || IsUserInsideMatch(x.Identity)))
                return false;

            if (!team.CanJoinTeamQualifier(true))
                return false;

            team.EnterTeamQualifier();
            return m_dicObjects.TryAdd(user.Identity, user.Team);
        }

        public bool Uninscribe(Character user, UninscribeReason reason)
        {
            if (Status == QualifierState.Disabled)
                return false;

            if (!ContainsUser(user.Identity) && !GenerateFirstData(user))
                return false;

            /**
             * Only the team leader can apply.
             */
            if (user.Team?.Leader?.Identity != user.Identity && reason != UninscribeReason.Disconnect)
                return false;

            TeamQualifierMatch match = FindMatch(user.Identity);
            if (user.Team?.Leader?.Identity == user.Identity)
            {
                /**
                 * We will only uninscribe if the match has not started yet.
                 */
                if (match != null && !match.IsReadyToStart())
                {
                    if (match.Team0.Leader.Identity == user.Identity)
                    {
                        match.ForceWinner(match.Team1.Leader.Identity, !match.IsRunning());
                    }
                    else if (match.Team1.Leader.Identity == user.Identity)
                    {
                        match.ForceWinner(match.Team0.Leader.Identity, !match.IsRunning());
                    }
                    else // ???
                    {
                        return false;
                    }
                }
                else if (match != null)
                {
                    return false;
                }
                user.Team?.ExitTeamQualifier(ArenaWaitStatus.NOT_SIGNED_UP);
            }
            else if (reason == UninscribeReason.Disconnect)
            {
                // player may be disconnecting
                match?.Lose(user, true);
                user.TeamPlayerQualifier.Status = ArenaWaitStatus.NOT_SIGNED_UP;
                return true;
            }
            else
            {
                return false;
            }

            return m_dicObjects.TryRemove(user.Identity, out _);
        }

        public void OnTimer()
        {
            if (!m_tArenaCheck.ToNextTime(ARENA_CHECK_INTERVAL_MS))
                return;
        }

        public int GetUserPosition(uint idUser)
        {
            if (!ContainsUser(idUser))
                return 0;
            if (GetUser(idUser).GamesToday <= 0)
                return 0;

            int pos = 1;
            foreach (var user in m_dicRanking.Values.Where(x => x.GamesToday > 0)
                .OrderByDescending(x => x.Points)
                .ThenByDescending(x => x.Wins)
                .ThenBy(x => x.Loss))
            {
                if (user.Identity == idUser)
                    return pos;
                pos++;
            }

            return pos > 1000 ? 0 : pos;
        }

        public void CreateMatch(Team team)
        {
            if (!team.IsWaitingForOpponent())
                return;

            Team target = FindTarget(team);
            if (target == null)
                return;

            if (!team.CanJoinTeamQualifier(false))
            {
                Uninscribe(team.Leader, UninscribeReason.UserRequest);
                return;
            }

            if (!target.CanJoinTeamQualifier(false))
                return;

            m_dicObjects.TryRemove(team.Leader.Identity, out _);
            m_dicObjects.TryRemove(target.Leader.Identity, out _);

            TeamQualifierMatch match = new TeamQualifierMatch(PrepareMap());
            if (!match.Create(team, target))
            {
                Uninscribe(team.Leader, UninscribeReason.UserRequest);
                Uninscribe(target.Leader, UninscribeReason.UserRequest);
                return;
            }

            team.ExitTeamQualifier(ArenaWaitStatus.WAITING_INACTIVE);
            target.ExitTeamQualifier(ArenaWaitStatus.WAITING_INACTIVE);

            m_tMatches.TryAdd(match.MapIdentity, match);
        }

        private Map PrepareMap()
        {
            DynamicMapEntity dynaMap = new DynamicMapEntity
            {
                Identity = (uint) ArenaQualifierManager.MapIdentity.GetNextIdentity,
                MapDoc = m_pBaseMap.MapDoc,
                Name = "ArenaQualifier",
                Type = 7,
                FileName = "newarena.DMap"
            };
            Map temp = new Map(dynaMap);
            temp.Load();
            return temp;
        }

        public Team FindTarget(Team team)
        {
            List<Team> possibleTargets = new List<Team>();
            foreach (var targetTeam in m_dicObjects.Values
                .Where(x => x.Leader.Identity != team.Leader.Identity 
                            && x.IsWaitingForOpponent()
                            && x.IsMatchEnable(team.GetTeamQualifierGrade()))
                .OrderByDescending(x => x.GetTeamQualifierPoints()))
            {
                possibleTargets.Add(targetTeam);
            }
            return possibleTargets[ThreadSafeRandom.RandGet(0, possibleTargets.Count) % possibleTargets.Count];
        }

        public MsgTeamArenaYTop10List GetTop10(MsgTeamArenaYTop10List msg)
        {
            int count = 0;
            foreach (var latest in m_dicRanking.Values.Where(x => x.LastRanking > 0).OrderBy(x => x.LastRanking))
            {
                if (count++ >= 10)
                    break;
                msg.Append(latest.Name, latest.Lookface, latest.Level, (uint)latest.Profession,
                    latest.LastPoints, latest.LastRanking, latest.LastWins, latest.LastLoss);
            }
            return msg;
        }

        public enum UninscribeReason
        {
            UserRequest,
            Disconnect,
            ExitingTeam
        }
    }
}