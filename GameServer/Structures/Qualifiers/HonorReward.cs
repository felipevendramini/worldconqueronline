﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - HonorReward.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/22 20:58
// ////////////////////////////////////////////////////////////////////////////////////

namespace GameServer.Structures.Qualifiers
{
    public struct HonorReward
    {
        public int Position;
        public int ArenaDailyReward;
        public int ArenaWeeklyReward;
        public int TeamHonorReward;
    }
}