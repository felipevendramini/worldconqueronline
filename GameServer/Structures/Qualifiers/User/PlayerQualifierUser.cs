﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - PlayerQualifierUser.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/18 19:40
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Structures.Qualifiers.User
{
    public sealed class PlayerQualifierUser
    {
        public const byte RANK_TYPE_PLAYER = 0;
        public const byte RANK_TYPE_FAMILY = 1;
        public const byte RANK_TYPE_TEAM = 2;
        public const int DEFAULT_BAN_TIME = UnixTimestamp.TIME_SECONDS_DAY * 7;

        private readonly ArenaEntity m_dbEntity;

        /// <summary>
        /// Used to create new entities.
        /// </summary>
        public PlayerQualifierUser(Character user, byte type = RANK_TYPE_PLAYER)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            m_dbEntity = new ArenaEntity
            {
                Level = user.Level,
                Lookface = user.Lookface,
                Name = user.Name,
                Points = ArenaQualifierManager.GetInitialPoints(user.Level),
                PlayerIdentity = user.Identity,
                Profession = (ushort) user.Profession,
                RankType = type
            };

            Save();
        }

        /// <summary>
        /// Loads an existing entity.
        /// </summary>
        public PlayerQualifierUser(ArenaEntity entity)
        {
            m_dbEntity = entity ?? throw new ArgumentNullException(nameof(entity));
        }

        public Character User => ServerKernel.UserManager.GetUser(Identity);
        public uint Identity => m_dbEntity.PlayerIdentity;
        public string Name => User?.Name ?? m_dbEntity.Name;
        public byte Level => User?.Level ?? m_dbEntity.Level;
        public ProfessionType Profession => User?.Profession ?? (ProfessionType) m_dbEntity.Profession;
        public uint SyndicateIdentity => User?.SyndicateIdentity ?? 0;
        public string SyndicateName => User?.SyndicateName ?? Language.StrNone;

        public PkModeType OldPkMode { get; set; }

        public uint Points
        {
            get => m_dbEntity.Points;
            set => m_dbEntity.Points = value;
        }

        public uint Wins
        {
            get => m_dbEntity.WinsToday;
            set => m_dbEntity.WinsToday = value;
        }

        public uint LastWins => m_dbEntity.LastWin;

        public uint TotalWins
        {
            get => m_dbEntity.WinsTotal;
            set => m_dbEntity.WinsTotal = value;
        }

        public uint Loss
        {
            get => m_dbEntity.LossToday;
            set => m_dbEntity.LossToday = value;
        }

        public uint LastLoss => m_dbEntity.LastLoss;

        public uint TotalLoss
        {
            get => m_dbEntity.LossTotal;
            set => m_dbEntity.LossTotal = value;
        }

        public uint GamesToday => Wins + Loss;

        public bool ChangeHonorPoints(int amount)
        {
            if (User == null) // user is offline
            {
                CharacterEntity entity = Database.CharacterRepository.SearchByIdentity(Identity);
                if (entity == null)
                    return false;

                if (amount > 0)
                {
                    entity.HonorPoints += (uint) amount;
                }
                else if (amount < 0 && amount*-1 <= entity.HonorPoints)
                {
                    entity.HonorPoints -= (uint) (amount * -1);
                }
                else
                {
                    return false;
                }
                return Database.CharacterRepository.Save(entity);
            }
            return User.ChangeHonorPoints(amount);
        }

        public uint HonorPoints => User?.HonorPoints ?? 0u;

        public uint TotalHonorPoints
        {
            get => m_dbEntity.TotalHonorPoints;
            set => m_dbEntity.TotalHonorPoints = value;
        }

        public bool IsBanned => m_dbEntity.LockReleaseTime != 0 && m_dbEntity.LockReleaseTime > UnixTimestamp.Now();

        public int BanRemainingHours => (int) (UnixTimestamp.ToDateTime(m_dbEntity.LockReleaseTime) - DateTime.Now).TotalHours;

        public void SetBan(int time = DEFAULT_BAN_TIME)
        {
            m_dbEntity.LockReleaseTime = (uint) time;
            Save();
        }

        public ArenaWaitStatus Status { get; set; }

        public DateTime JoinTime { get; set; }

        public int Ranking => m_dbEntity.RankType == RANK_TYPE_TEAM ? ServerKernel.TeamQualifier.GetUserPosition(Identity) : ServerKernel.ArenaQualifier.GetUserPosition(Identity);
        public uint Lookface => m_dbEntity.Lookface;
        public uint LastRanking => m_dbEntity.LastRank;
        public uint LastPoints => m_dbEntity.LastPoints;

        public int Grade
        {
            get
            {
                if (Points >= 4000)
                    return 5;
                if (Points >= 3300 && Points < 4000)
                    return 4;
                if (Points >= 2800 && Points < 3300)
                    return 3;
                if (Points >= 2200 && Points < 2800)
                    return 2;
                if (Points >= 1500 && Points < 2200)
                    return 1;
                return 0;
            }
        }

        public bool IsMatchEnable(int targetGrade)
        {
            int nDelta = Grade - targetGrade;
            if (nDelta < 0)
                nDelta *= -1;
            return nDelta < 2;
        }

        public void Synchro()
        {
            if (User == null)
                return;

            Character user = User;

            m_dbEntity.Name = user.Name;
            m_dbEntity.Level = user.Level;
            m_dbEntity.Profession = (ushort) user.Profession;
            m_dbEntity.Lookface = user.Lookface;
            Save();
        }

        public void SetLastRank(uint i)
        {
            m_dbEntity.LastRank = i;
            m_dbEntity.LastPoints = Points;
            m_dbEntity.LastWin = Wins;
            m_dbEntity.LastLoss = Loss;
        }

        public void Reset()
        {
            Synchro();

            m_dbEntity.Points = ArenaQualifierManager.GetInitialPoints(Level);
            Wins = 0;
            Loss = 0;
        }

        public void Send(byte[] msg)
        {
            User?.Send(msg);
        }

        public bool Save()
        {
            return Database.Arena.Save(m_dbEntity);
        }

        /// <summary>Determines whether the specified object is equal to the current object.</summary>
        /// <param name="obj">The object to compare with the current object. </param>
        /// <returns>true if the specified object  is equal to the current object; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is Character))
                return false;
            Character user = obj as Character;
            if (user.Identity != Identity) return false;
            if (user.Name != Name) return false;
            if (user.Level != Level) return false;
            if (user.Profession != Profession) return false;
            return true;
        }

        private bool Equals(PlayerQualifierUser other)
        {
            return Equals(m_dbEntity, other.m_dbEntity);
        }

        public override int GetHashCode()
        {
            return (m_dbEntity != null ? m_dbEntity.GetHashCode() : 0);
        }

        public static bool operator ==(PlayerQualifierUser left, PlayerQualifierUser right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(PlayerQualifierUser left, PlayerQualifierUser right)
        {
            return !Equals(left, right);
        }
    }
}