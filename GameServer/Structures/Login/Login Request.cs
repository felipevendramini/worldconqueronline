﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Login Request.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common;

#endregion

namespace GameServer.Structures.Login
{
    public sealed class LoginRequest
    {
        public const int REQUEST_TIMEOUT = 60;

        private readonly TimeOut m_pTimeOut = new TimeOut(REQUEST_TIMEOUT);

        public LoginRequest(uint dwUsrId, uint dwHash, uint dwTime, string szAddress, byte pLevel, byte pType,
            string szMac)
        {
            AccountIdentity = dwUsrId;
            Hash = dwHash;
            RequestTime = dwTime;
            IpAddress = szAddress;
            VipLevel = pLevel;
            Authority = pType;
            MacAddress = szMac;

            m_pTimeOut.Update();
        }

        public uint AccountIdentity;
        public uint Hash;
        public uint RequestTime;
        public uint LastLogin;
        public string IpAddress;
        public string LastIpAddress;
        public byte VipLevel;
        public byte Authority;
        public string MacAddress;

        public bool IsValid(uint dwUsrId, uint dwHash, string szIp)
        {
            return !m_pTimeOut.IsTimeOut() && AccountIdentity == dwUsrId && Hash == dwHash && IpAddress == szIp;
        }

        public bool IsExpired()
        {
            return m_pTimeOut.IsTimeOut();
        }
    }
}