﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Trade Partner.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Structures
{
    public class TradePartner
    {
        private Character m_pOwner;
        private BusinessEntity m_dbObj;
        private MsgTradeBuddy m_pPacket;
        private uint m_idIdentity;
        private string m_szName;
        private uint m_dwAddDate;

        public TradePartner(Character pOwner)
        {
            m_pOwner = pOwner;
            m_pPacket = new MsgTradeBuddy();
        }

        public TradePartner(Character pOwner, BusinessEntity BusinessEntity)
        {
            m_pOwner = pOwner;
            m_dbObj = BusinessEntity;
            m_idIdentity = BusinessEntity.Business;
            m_szName = BusinessEntity.Name;
            m_dwAddDate = BusinessEntity.Date;
            m_pPacket = new MsgTradeBuddy
            {
                Identity = m_idIdentity,
                Name = m_szName,
                HoursLeft = HoursLeft,
                Online = TargetOnline
            };
        }

        public bool Create(uint idTarget)
        {
            Character target = ServerKernel.UserManager.GetUser(idTarget);
            if (target == null)
            {
                //m_pOwner.Send("The target is not online.");
                return false;
            }

            m_idIdentity = target.Identity;
            m_szName = target.Name;
            m_dwAddDate = (uint) UnixTimestamp.Now();

            new BusinessRepository().Save(new BusinessEntity
            {
                Userid = m_pOwner.Identity,
                Business = m_idIdentity,
                Date = m_dwAddDate,
                Name = m_szName // target name
            });

            m_pOwner.TradePartners.TryAdd(idTarget, this);

            m_pPacket = new MsgTradeBuddy
            {
                Identity = m_idIdentity,
                Name = m_szName,
                HoursLeft = (int) ((UnixTimestamp.Now() + UnixTimestamp.TIME_SECONDS_DAY * 3 - m_dwAddDate) / 60 / 60),
                Type = TradePartnerType.ADD_PARTNER,
                Online = TargetOnline
            };
            m_pOwner.Send(m_pPacket);
            return true;
        }

        public bool Delete()
        {
            return new BusinessRepository().Delete(m_dbObj);
        }

        public uint OwnerIdentity => m_pOwner.Identity;

        public uint TargetIdentity => m_idIdentity;

        public string Name
        {
            get => m_szName;
            set => m_szName = value;
        }

        public int HoursLeft => !IsActive
            ? (int) ((m_dwAddDate + UnixTimestamp.TIME_SECONDS_DAY * 3 - UnixTimestamp.Now()) / 60 / 60)
            : 0;

        public Character Owner => m_pOwner;

        public bool TargetOnline => ServerKernel.UserManager.GetUser(m_idIdentity) != null;

        public bool IsActive => UnixTimestamp.Now() > m_dwAddDate + UnixTimestamp.TIME_SECONDS_DAY * 3;

        public MsgTradeBuddy ToArray(TradePartnerType type)
        {
            m_pPacket.Type = type;
            return m_pPacket;
        }
    }
}