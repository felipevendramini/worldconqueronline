﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Magic.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Linq;
using FtwCore.Common;
using FtwCore.Database.Entities;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Structures
{
    public sealed class Magic
    {
        private Role m_pOwner;

        private MagicEntity m_dbMagic;
        private MagictypeEntity m_dbMagictype;

        private TimeOutMS m_tDelay;

        private byte m_pMaxLevel;

        public Magic(Role owner)
        {
            m_pOwner = owner;
        }

        public bool Create(uint idMgc)
        {
            return Create(idMgc, 0);
        }

        public bool Create(uint idMgc, ushort level)
        {
            m_dbMagictype = ServerKernel.Magictype.Values.FirstOrDefault(x => x.Type == idMgc && x.Level == level);
            if (m_dbMagictype == null)
            {
                ServerKernel.Log.GmLog("magic_fail",$"Skill not existent for creation (type:{idMgc}, level:{0}, player: {m_pOwner.Identity})");
                return false;
            }

            if (m_pOwner.Magics.CheckType((ushort)idMgc))
                return false;

            m_dbMagic = new MagicEntity
            {
                OwnerId = m_pOwner.Identity,
                Type = (ushort)idMgc,
                Level = level
            };

            GetMaxLevel();
            if (m_pOwner is Character)
            {
                Save();
                SendSkill();
            }
            SetDelay();
            return true;
        }

        public bool Create(MagicEntity pMgc)
        {
            m_dbMagictype = ServerKernel.Magictype.Values.FirstOrDefault(x => x.Type == pMgc.Type && x.Level == pMgc.Level);
            if (m_dbMagictype == null)
            {
                ServerKernel.Log.GmLog("magic_fail", $"Skill not existent (type:{pMgc.Type}, level:{pMgc.Level}, player: {m_pOwner.Identity})");
                return false;
            }

            m_dbMagic = pMgc;
            GetMaxLevel();
            SetDelay();
            return true;
        }

        private void GetMaxLevel()
        {
            m_pMaxLevel = (byte) (ServerKernel.Magictype.Values.OrderByDescending(x => x.Level).FirstOrDefault(x => x.Type == Type)?.Level ?? 0);
        }

        public uint Identity => m_dbMagic.Id;

        public string Name => m_dbMagictype.Name;

        public ushort Type => m_dbMagic.Type;

        public ushort Level
        {
            get => m_dbMagic.Level;
            set
            {
                m_dbMagic.Level = Math.Min(m_pMaxLevel, value);
                m_dbMagictype = ServerKernel.Magictype.Values.FirstOrDefault(x => x.Type == m_dbMagic.Type && x.Level == m_dbMagic.Level);
                Save();
            }
        }

        public uint Experience
        {
            get => m_dbMagic.Experience;
            set
            {
                m_dbMagic.Experience = value;
                Save();
                SendSkill();
            }
        }

        public byte OldLevel
        {
            get => (byte) m_dbMagic.OldLevel;
            set
            {
                m_dbMagic.OldLevel = value;
                Save();
            }
        }

        public ushort MaxLevel => m_pMaxLevel;
        public byte Sort => (byte) m_dbMagictype.Sort;
        public byte AutoActive => m_dbMagictype.AutoActive;
        public byte Crime => m_dbMagictype.Crime;
        public byte Ground => m_dbMagictype.Ground;
        public byte Multi => m_dbMagictype.Multi;
        public byte Target => (byte) m_dbMagictype.Target;
        public uint UseMana => m_dbMagictype.UseMp;
        public int Power => m_dbMagictype.Power;
        public uint IntoneSpeed => m_dbMagictype.IntoneSpeed;
        public uint Percent => m_dbMagictype.Percent;
        public uint StepSeconds => m_dbMagictype.StepSecs;
        public uint Range => m_dbMagictype.Range;
        public uint Distance => m_dbMagictype.Distance;
        public long Status => m_dbMagictype.Status;
        public uint NeedProf => m_dbMagictype.NeedProf;
        public int NeedExp => m_dbMagictype.NeedExp;
        public uint NeedLevel => m_dbMagictype.NeedLevel;
        public byte UseXp => m_dbMagictype.UseXp;
        public uint WeaponSubtype => m_dbMagictype.WeaponSubtype;
        public byte WeaponSubtypeNum => m_dbMagictype.WeaponSubtypeNum;
        public uint ActiveTimes => m_dbMagictype.ActiveTimes;
        public uint FloorAttr => m_dbMagictype.FloorAttr;
        public byte AutoLearn => m_dbMagictype.AutoLearn;
        public byte DropWeapon => m_dbMagictype.DropWeapon;
        public uint UseStamina => m_dbMagictype.UseEp;
        public byte WeaponHit => m_dbMagictype.WeaponHit;
        public uint UseItem => m_dbMagictype.UseItem;
        public uint NextMagic => m_dbMagictype.NextMagic;
        public int DelayMs => (int) (m_dbMagictype.DelayMs > 0 ? m_dbMagictype.DelayMs : m_dbMagictype.Timeout);
        public uint UseItemNum => m_dbMagictype.UseItemNum;
        public byte ElementType => m_dbMagictype.ElementType;
        public uint ElementPower => m_dbMagictype.ElementPower;
        public uint DashRange => m_dbMagictype.MaximumDashRange;
        public uint CpsCost => (uint) (m_dbMagictype.EmoneyPrice/22.22d);

        public int GetElementPower(Role pTarget)
        {
            int nDmg = 0;
            switch ((FtwCore.Common.Enums.ElementType) ElementType)
            {
                case FtwCore.Common.Enums.ElementType.Water:
                {
                    nDmg = (int)(Power * (1 - (pTarget.WaterResistance / 100f)));
                    break;
                }
                case FtwCore.Common.Enums.ElementType.Fire:
                {
                    nDmg = (int)(Power * (1 - (pTarget.FireResistance / 100f)));
                    break;
                }
                case FtwCore.Common.Enums.ElementType.Wood:
                {
                    nDmg = (int)(Power * (1 - (pTarget.WoodResistance / 100f)));
                    break;
                }
                case FtwCore.Common.Enums.ElementType.Earth:
                {
                    nDmg = (int)(Power * (1 - (pTarget.EarthResistance / 100f)));
                    break;
                }
                case FtwCore.Common.Enums.ElementType.Metal:
                {
                    nDmg = (int)(Power * (1 - (pTarget.MetalResistance / 100f)));
                    break;
                }
            }
            return nDmg;
        }

        public bool Delay()
        {
            if (m_tDelay == null)
                m_tDelay = new TimeOutMS(DelayMs);
            return m_tDelay.ToNextTime(DelayMs);
        }

        public void SetDelay()
        {
            if (m_tDelay == null)
                m_tDelay = new TimeOutMS(DelayMs);
            m_tDelay.Startup(DelayMs);
        }

        public void ClearDelay()
        {
            m_tDelay.Clear();
            m_tDelay.SetInterval(DelayMs);
        }

        public bool IsReady()
        {
            if (m_tDelay == null)
                return true;
            return m_tDelay.IsTimeOut(Math.Max(300, DelayMs));
        }

        public int GetLockSecs()
        {
            return (int) (StepSeconds / 1000 + 1);
        }

        public void SendSkill(SkillAction action = SkillAction.AddNew)
        {
            if (m_pOwner is Character user)
                user.Send(new MsgMagicInfo(Experience, Level, Type) { Action = action });
        }

        public bool Save()
        {
            return Database.MagicRepository.Save(m_dbMagic);
        }

        public bool Unlearn()
        {
            (m_pOwner as Character)?.Send(new MsgAction(Identity, 0, 0, 0, GeneralActionType.DropMagic));
            m_dbMagic.Unlearn = 1;
            return Database.MagicRepository.Save(m_dbMagic);
        }

        public bool Delete()
        {
            return Database.MagicRepository.Delete(m_dbMagic);
        }
    }
}