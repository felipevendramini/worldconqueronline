﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Nobility.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Linq;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Structures
{
    public sealed class Nobility
    {
        public const int MINIMUM_DONATION = 3000000;
        public const int MAXIMUM_DONATION = int.MaxValue;

        private readonly DynaRankRecordsEntity m_dbRank;
        private readonly Character m_pOwner;
        private int m_nRanking;
        private NobilityLevel m_pLevel;

        public Nobility(Character pOwner)
        {
            m_pOwner = pOwner;

            if (!ServerKernel.Nobility.TryGetValue(pOwner.Identity, out m_dbRank))
            {
                m_dbRank = new DynaRankRecordsEntity
                {
                    ObjectId = 0,
                    ObjectName = "",
                    RankType = (uint) DynamicRankingType.Nobility,
                    UserIdentity = pOwner.Identity,
                    Username = pOwner.Name,
                    Value = pOwner.NobilityDonation
                };

                m_pLevel = NobilityLevel.SERF;
                m_nRanking = 0;
            }
            else
            {
                Update();
            }

            SendNobilityIcon();
        }

        public long Donation => m_pOwner.NobilityDonation;

        public NobilityLevel Level => m_pLevel;

        public DynaRankRecordsEntity Database => m_dbRank;

        private int GetRanking()
        {
            int nRanking = 0;
            if (ServerKernel.Nobility.Values.OrderByDescending(x => x.Value)
                .TakeWhile(obj => nRanking++ < 60)
                .Any(obj => obj.UserIdentity == m_pOwner.Identity))
            {
                m_nRanking = nRanking;
                m_pLevel = GetNobility;
                return m_nRanking;
            }
            m_pLevel = GetNobility;
            return m_nRanking = -1;
        }

        public NobilityLevel GetNobility
        {
            get
            {
                if (m_nRanking >= 0)
                {
                    if (m_nRanking <= 3)
                        return NobilityLevel.KING;
                    if (m_nRanking <= 15)
                        return NobilityLevel.PRINCE;
                    if (m_nRanking <= 50)
                        return NobilityLevel.DUKE;
                }

                if (Donation >= 200000000)
                    return NobilityLevel.EARL;
                if (Donation >= 100000000)
                    return NobilityLevel.BARON;
                if (Donation >= 30000000)
                    return NobilityLevel.KNIGHT;
                return NobilityLevel.SERF;
            }
        }

        public int Position => m_nRanking;

        public int Donate(long nAmount)
        {
            if (nAmount < MINIMUM_DONATION || nAmount > MAXIMUM_DONATION) return GetRanking();

            if ((ulong) nAmount + (ulong) Donation > long.MaxValue)
            {
                m_dbRank.Value = long.MaxValue;
            }
            else
            {
                m_dbRank.Value = m_pOwner.NobilityDonation += nAmount;
            }
            Save();

            m_pOwner.NobilityDonation = m_dbRank.Value;
            return GetRanking();
        }

        public void Update()
        {
            GetRanking();
        }

        public int BattlePower => (int) (ServerKernel.NobilityEnabled ? m_pLevel : 0);

        public void SendNobilityIcon()
        {
            Update();

            string nobilityInfoString = $"{m_dbRank.UserIdentity} {m_dbRank.Value} {(byte) Level:d} {m_nRanking - 1}";
            var nPacket = new MsgPeerage(132 + nobilityInfoString.Length);
            nPacket.WriteByte(1, 120);
            nPacket.WriteStringWithLength(nobilityInfoString, 121);
            nPacket.Action = NobilityAction.INFO;
            nPacket.DataLow = m_dbRank.UserIdentity;
            m_pOwner.Send(nPacket);

            m_pOwner.NobilityRank = Level;
            m_pOwner.Screen.RefreshSpawnForObservers();
        }

        public bool Save()
        {
            return Donation > 2999999 && new DynamicRankingRecordsRepository().Save(m_dbRank);
        }
    }
}