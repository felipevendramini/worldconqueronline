﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Warehouse.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Items;

#endregion

namespace GameServer.Structures
{
    public class Warehouse
    {
        private const int _WAREHOUSE_LIIMIT_I = 60;
        public Dictionary<uint, Item> Items;
        private ushort m_dwIdentity;
        private Character m_pOwner;

        public Warehouse(Character pOwner, ushort pos)
        {
            m_pOwner = pOwner;
            Items = new Dictionary<uint, Item>();
            m_dwIdentity = pos;
        }

        public ushort Identity => m_dwIdentity;

        public Item this[uint idItem]
        {
            get
            {
                try
                {
                    return Items.TryGetValue(idItem, out var value) ? value : null;
                }
                catch
                {
                    return null;
                }
            }
        }

        public bool Add(Item item, bool loading = false)
        {
            if (IsFull()) return false;
            if (Items.ContainsKey(item.Identity)) return false;
            if (!loading)
            {
                item.Position = (ItemPosition) Identity;
                item.Save();
                SendSingle(item);
            }

            Items.Add(item.Identity, item);
            return true;
        }

        public bool Remove(uint idItem)
        {
            if (!Items.ContainsKey(idItem))
                return false;

            return m_pOwner.UserPackage.AddItem(Items[idItem]) && Items.Remove(idItem);
        }

        public bool Delete(uint idItem)
        {
            if (!Items.ContainsKey(idItem))
                return false;
            Items[idItem].Delete();
            Items.Remove(idItem);
            return true;
        }

        public void SendSingle(Item item)
        {
            int now = UnixTimestamp.Now();
            uint remain = 0;
            if (now < item.RemainingTime)
                remain = (uint) (item.RemainingTime - now);
            var pMsg = new MsgAccountSoftKb
            {
                Action = WarehouseMode.WH_ADDITEM,
                ItemsCount = 1,
                ItemIdentity = item.Identity,
                Itemtype = item.Type,
                Bless = item.ReduceDamage,
                Bound = item.IsBound,
                Color = (byte) item.Color,
                Effect = (byte) item.Effect,
                Enchant = item.Enchantment,
                Identity = GetWarehouseIdentity(Identity),
                SocketOne = (byte) item.SocketOne,
                SocketTwo = (byte) item.SocketTwo,
                Type = 10,
                Suspicious = false,
                Plus = item.Addition,
                RemainingTime = remain,
                Locked = item.IsLocked,
                StackAmount = item.StackAmount,
                SocketProgress = item.SocketProgress,
                AddLevelExp = item.AdditionProgress
            };
            m_pOwner.Send(pMsg);
            item.TryUnlock();
            item.SendPurification(m_pOwner);
        }

        public void SendAll()
        {
            foreach (var item in Items.Values)
            {
                SendSingle(item);
            }
        }

        public int RemainingSpace()
        {
            return _WAREHOUSE_LIIMIT_I - Items.Count;
        }

        public bool IsFull()
        {
            return Items.Count >= _WAREHOUSE_LIIMIT_I;
        }

        public static byte GetWarehousePosition(uint itemPos)
        {
            switch (itemPos)
            {
                case 8:
                    return 230;
                case 44:
                    return 231;
                case 4101:
                    return 232;
                case 10011:
                    return 233;
                case 10012:
                    return 234;
                case 10027:
                    return 235;
                case 10028:
                    return 236;
            }

            return 0;
        }

        public static uint GetWarehouseIdentity(uint itemPos)
        {
            switch (itemPos)
            {
                case 230:
                    return 8;
                case 231:
                    return 44;
                case 232:
                    return 4101;
                case 233:
                    return 10011;
                case 234:
                    return 10012;
                case 235:
                    return 10027;
                case 236:
                    return 10028;
            }

            return 0;
        }
    }
}