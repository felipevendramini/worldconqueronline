﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Daily Update.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Structures
{
    public static class DailyUpdate
    {
#if !DEBUG
        public const int DAILY_CHI_AMOUNT = 500;
#else
        public const int DAILY_CHI_AMOUNT = 4000;
        public const int DAILY_STUDY_POINTS = 2000;
#endif

        public static void Do()
        {
            DailyUpdateEntity latest = new DailyUpdateRepository().GetLatest();
            if (latest != null && uint.Parse(latest.Timestamp.ToString("yyyyMMdd")) >=
                uint.Parse(DateTime.Now.ToString("yyyyMMdd")))
                return;

            uint online = 0, offline = 0;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            IList<CharacterEntity> allPlayers = Database.CharacterRepository.FetchAll();
            foreach (var player in allPlayers)
            {
                if (player.LastUpdate >= uint.Parse(DateTime.Now.ToString("yyyyMMdd")))
                    continue;

                Character user = ServerKernel.UserManager.GetUser(player.Identity);
                if (user == null)
                {
                    try
                    {
                        user = new Character(player.Identity, null, player);
                    }
                    catch (Exception ex)
                    {
                        Program.WriteLog($"Failed to create user obj for daily reward. UserID: {player.Identity}",
                            LogType.ERROR);
                        Program.WriteLog(ex.ToString(), LogType.EXCEPTION, false);
                        continue;
                    }

                    offline++;
                }
                else
                {
                    user.SendMessage("Your daily missions has been reseted.", Color.White, ChatTone.Talk);
                    online++;
                }

//#if DEBUG
//                try
//                {
//                    user.AwardEmoney(21500, EmoneySourceType.GmCommand, null);
//                }
//                catch (Exception ex)
//                {
//                    Program.WriteLog($"Failed to give daily debug reward. UserID: {user.Identity}", LogType.ERROR);
//                    Program.WriteLog(ex.ToString(), LogType.EXCEPTION, false);
//                }
//#endif

                try
                {
                    user.AwardChiPoints(DAILY_CHI_AMOUNT);
                }
                catch (Exception ex)
                {
                    Program.WriteLog($"Failed to give daily Chi reward. UserID: {user.Identity}", LogType.ERROR);
                    Program.WriteLog(ex.ToString(), LogType.EXCEPTION, false);
                }

#if DEBUG
                try
                {
                    user.AwardStudyPoints(DAILY_STUDY_POINTS);
                }
                catch (Exception ex)
                {
                    Program.WriteLog($"Failed to give daily Study Beta reward. UserID: {user.Identity}", LogType.ERROR);
                    Program.WriteLog(ex.ToString(), LogType.EXCEPTION, false);
                }
#endif

                try
                {
                    if (user.PlayerQualifier != null
                        && user.PlayerQualifier.Ranking > 0 && user.PlayerQualifier.Ranking < 1000
                        && (user.PlayerQualifier.Wins > 0 || user.PlayerQualifier.Loss > 0))
                    {
                        user.ChangeHonorPoints(ServerKernel.HonorRewards[user.PlayerQualifier.Ranking - 1]
                            .ArenaDailyReward);
                    }
                }
                catch (Exception ex)
                {
                    Program.WriteLog($"Failed to do arena dailyupdates. UserID: {user.Identity}", LogType.ERROR);
                    Program.WriteLog(ex.ToString(), LogType.EXCEPTION, false);
                }

                try
                {
                    user.SetDailyUpdate();
                }
                catch (Exception ex)
                {
                    Program.WriteLog($"Failed to save dailyupdates. UserID: {user.Identity}", LogType.ERROR);
                    Program.WriteLog(ex.ToString(), LogType.EXCEPTION, false);
                }
            }

            ServerKernel.ArenaQualifier.Reset();

            sw.Stop();
            new DailyUpdateRepository().Save(new DailyUpdateEntity
            {
                ElapsedMilliseconds = (uint) sw.ElapsedMilliseconds,
                OfflinePlayers = offline,
                OnlinePlayers = online,
                ServerVersion = ServerKernel.Version,
                Timestamp = DateTime.Now
            });
        }
    }
}