﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Achievements.cs
// Last Edit: 2020/01/14 19:35
// Created: 2020/01/14 16:01
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Concurrent;
using System.Linq;
using FtwCore;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Structures
{
    public class UserAchievements
    {
        private readonly ConcurrentDictionary<int, AchievementsEntity> m_dicAchievements =
            new ConcurrentDictionary<int, AchievementsEntity>();

        private readonly Character m_pUser;

        public UserAchievements(Character user)
        {
            m_pUser = user;
            LoadUserInfo();
        }

        public void LoadUserInfo()
        {
            var allAchievements = new AchievementsRepository().FetchByUser(m_pUser.Identity);
            foreach (var obj in allAchievements)
            {
                m_dicAchievements.TryAdd(IdToFlag(obj.Achievement), obj);
            }
        }


        private int IdToFlag(int id)
        {
            return (id / 100 % 100 - 1) * 32 + (id % 100 - 1);
        }

        public int FlagToId(int flag)
        {
            return 10100 + 100 * (byte) (flag / 32) + (byte) (flag % 32) + 1;
        }

        public void NewAchievement(int flag)
        {
            if (m_dicAchievements.ContainsKey(IdToFlag(flag)))
                return;

            if (!Process(flag))
                return;

            AchievementsEntity entity = new AchievementsEntity
            {
                UserIdentity = m_pUser.Identity,
                Achievement = flag,
                EarnDate = DateTime.Now
            };

            new AchievementsRepository().Save(entity);

            m_dicAchievements.TryAdd(IdToFlag(flag), entity);

            m_pUser.Send(new MsgAchievement
            {
                Identity = m_pUser.Identity,
                Action = AchievementRequest.SendQueue,
                Flag = flag
            });

            string message = string.Format(Language.StrAchievementReceive, m_pUser.Name, flag);
            if (m_pUser.Syndicate == null)
                m_pUser.SendMessage(message, ChatTone.Talk);
            else m_pUser.Syndicate.Send(message);
        }

        /// <summary>
        ///     Function to check if the user has the requirements to confirm the achievement and to give rewards if enabled.
        /// </summary>
        /// <returns></returns>
        public bool Process(int flag)
        {
            if (!Enum.IsDefined(typeof(AchievementType), flag))
                return false;

            switch ((AchievementType) flag)
            {
                case AchievementType.Millionaire:
                    if (m_pUser.Silver < 3000000)
                        return false;
                    break;
                case AchievementType.Chasingthewind:
                    if (m_pUser.UserPackage[ItemPosition.Steed] == null)
                        return false;
                    break;
                case AchievementType.Asfastaslightning:
                    if (m_pUser.UserPackage[ItemPosition.Steed]?.Addition != 12)
                        return false;
                    break;
                case AchievementType.Youarenotalone:
                    if (m_pUser.Friends.Count == 0)
                        return false;
                    break;
                case AchievementType.Anotherclient:
                    if (m_pUser.TradePartners.Count == 0)
                        return false;
                    break;
                case AchievementType.Whatabeautifulflower:
                    if (m_pUser.RedRoses == 0
                        && m_pUser.WhiteRoses == 0
                        && m_pUser.Orchids == 0
                        && m_pUser.Tulips == 0)
                        return false;
                    break;
                case AchievementType.Tietheknot:
                    if (m_pUser.Mate == Language.StrNone)
                        return false;
                    break;

                /**
                 * Syndicate
                 */
                case AchievementType.Thisisyourhome:
                    if (m_pUser.Syndicate == null)
                        return false;
                    break;
                case AchievementType.Imtheking:
                    if (m_pUser.SyndicatePosition != SyndicateRank.GuildLeader)
                        return false;
                    break;
                case AchievementType.ItsanHonor:
                    if (m_pUser.SyndicatePosition != SyndicateRank.HonoraryDeputyLeader
                        && m_pUser.SyndicatePosition != SyndicateRank.HonoraryManager
                        && m_pUser.SyndicatePosition != SyndicateRank.HonorarySupervisor
                        && m_pUser.SyndicatePosition != SyndicateRank.HonorarySteward)
                        return false;
                    break;
                case AchievementType.No2intheguild:
                    if (m_pUser.SyndicatePosition != SyndicateRank.LeaderSpouse
                        && m_pUser.SyndicatePosition != SyndicateRank.DeputyLeader
                        && m_pUser.SyndicatePosition != SyndicateRank.HonoraryDeputyLeader)
                        return false;
                    break;
                case AchievementType.Respectable:
                    if (m_pUser.SyndicatePosition != SyndicateRank.Manager
                        && m_pUser.SyndicatePosition != SyndicateRank.HonoraryManager)
                        return false;
                    break;
                case AchievementType.Reverential:
                    if (m_pUser.SyndicatePosition < SyndicateRank.HonorarySupervisor
                        || m_pUser.SyndicatePosition > SyndicateRank.TulipSupervisor)
                        return false;
                    break;
                case AchievementType.Feelsgood:
                    if (m_pUser.SyndicatePosition != SyndicateRank.Steward
                        && m_pUser.SyndicatePosition != SyndicateRank.HonorarySteward)
                        return false;
                    break;

                /**
                 * Family
                 */
                case AchievementType.Weareafamily:
                    if (m_pUser.Family == null)
                        return false;
                    break;
                case AchievementType.Level130:
                    if (m_pUser.Level < 130)
                        return false;
                    break;
                case AchievementType.Level140:
                    if (m_pUser.Level < 140)
                        return false;
                    break;
                case AchievementType.Fighter:
                    if (m_pUser.BattlePower < 100)
                        return false;
                    break;
                case AchievementType.Seniorfighter:
                    if (m_pUser.BattlePower < 200)
                        return false;
                    break;
                case AchievementType.Exellentfighter:
                    if (m_pUser.BattlePower < 300)
                        return false;
                    break;
                case AchievementType.Reborn:
                    if (m_pUser.Metempsychosis < 1)
                        return false;
                    break;
                case AchievementType.Newlife:
                    if (m_pUser.Metempsychosis < 2)
                        return false;
                    break;
                case AchievementType.Thanksalot:
                    if (m_pUser.VirtuePoints < 5000)
                        return false;
                    break;
                case AchievementType.YouareaNoble:
                    if (m_pUser.NobilityRank == NobilityLevel.SERF)
                        return false;
                    break;

                /**
                 * Proficiency
                 */
                case AchievementType.Boxingmaster:
                    if (m_pUser.WeaponSkill.Skills[0]?.Level < 12)
                        return false;
                    break;
                case AchievementType.Swordmaster:
                    if (m_pUser.WeaponSkill.Skills[420]?.Level < 12)
                        return false;
                    break;
                case AchievementType.Blademaster:
                    if (m_pUser.WeaponSkill.Skills[410]?.Level < 12)
                        return false;
                    break;
                case AchievementType.Hammermaster:
                    if (m_pUser.WeaponSkill.Skills[460]?.Level < 12)
                        return false;
                    break;
                case AchievementType.LongHammermaster:
                    if (m_pUser.WeaponSkill.Skills[540]?.Level < 12)
                        return false;
                    break;
                case AchievementType.Halbertmaster:
                    if (m_pUser.WeaponSkill.Skills[580]?.Level < 12)
                        return false;
                    break;
                case AchievementType.Spearmaster:
                    if (m_pUser.WeaponSkill.Skills[560]?.Level < 12)
                        return false;
                    break;
                case AchievementType.Daggermaster:
                    if (m_pUser.WeaponSkill.Skills[490]?.Level < 12)
                        return false;
                    break;
                case AchievementType.Wandmaster:
                    if (m_pUser.WeaponSkill.Skills[561]?.Level < 12)
                        return false;
                    break;
                case AchievementType.Glaivemaster:
                    if (m_pUser.WeaponSkill.Skills[510]?.Level < 12)
                        return false;
                    break;
                case AchievementType.Poleaxemaster:
                    if (m_pUser.WeaponSkill.Skills[530]?.Level < 12)
                        return false;
                    break;
                case AchievementType.Hookmaster:
                    if (m_pUser.WeaponSkill.Skills[430]?.Level < 12)
                        return false;
                    break;
                case AchievementType.Axemaster:
                    if (m_pUser.WeaponSkill.Skills[450]?.Level < 12)
                        return false;
                    break;
                case AchievementType.Clubmaster:
                    if (m_pUser.WeaponSkill.Skills[480]?.Level < 12)
                        return false;
                    break;
                case AchievementType.Sceptermaster:
                    if (m_pUser.WeaponSkill.Skills[481]?.Level < 12)
                        return false;
                    break;
                case AchievementType.Whipmaster:
                    if (m_pUser.WeaponSkill.Skills[440]?.Level < 12)
                        return false;
                    break;
                case AchievementType.Bowmaster:
                    if (m_pUser.WeaponSkill.Skills[500]?.Level < 12)
                        return false;
                    break;
                case AchievementType.Beadsmaster:
                    if (m_pUser.WeaponSkill.Skills[610]?.Level < 12)
                        return false;
                    break;
                case AchievementType.NinjaKatanamaster:
                    if (m_pUser.WeaponSkill.Skills[601]?.Level < 12)
                        return false;
                    break;
                case AchievementType.Backswordmaster:
                    if (m_pUser.WeaponSkill.Skills[421]?.Level < 12)
                        return false;
                    break;
                case AchievementType.Shieldmaster:
                    if (m_pUser.WeaponSkill.Skills[900]?.Level < 12)
                        return false;
                    break;
                case AchievementType.Fightlikeaman:
                    if (m_pUser.WeaponSkill.Skills[611]?.Level < 12)
                        return false;
                    break;
                case AchievementType.BangBangBang:
                    if (m_pUser.WeaponSkill.Skills[612]?.Level < 12)
                        return false;
                    break;
                case AchievementType.Scythemaster:
                    if (m_pUser.WeaponSkill.Skills[511]?.Level < 12)
                        return false;
                    break;
                case AchievementType.LegendaryKnife:
                    if (m_pUser.WeaponSkill.Skills[613]?.Level < 12)
                        return false;
                    break;
                case AchievementType.YouhaveaSuperitem:
                    if (m_pUser.UserPackage.GetEquipment().All(x => x.GetQuality() != 9))
                        return false;
                    break;
                case AchievementType.FullUniqueequipment:
                    if (m_pUser.IsFullQuality < 1)
                        return false;
                    break;
                case AchievementType.FullEliteequipment:
                    if (m_pUser.IsFullQuality < 2)
                        return false;
                    break;
                case AchievementType.FullSuperequipment:
                    if (m_pUser.IsFullQuality != 3)
                        return false;
                    break;
            }

            return true;
        }

        public void SendAll(Character target = null)
        {
            MsgAchievement msg = new MsgAchievement
            {
                Identity = target?.Identity ?? m_pUser.Identity,
                Action = AchievementRequest.BigHash
            };
            int value = 0;
            for (AchievementType i = Enum.GetValues(typeof(AchievementType)).Cast<AchievementType>().Min();
                i <= Enum.GetValues(typeof(AchievementType)).Cast<AchievementType>().Max();
                i++)
            {
                if ((int) i % 32 == 0)
                {
                    msg.Append(value);
                    value = 0;
                }

                if (m_dicAchievements.ContainsKey(IdToFlag((int) i)))
                    value |= 1 << ((int) i % 32);
            }

            if (target == null) m_pUser.Send(msg);
            else target.Send(msg);
        }
    }
}