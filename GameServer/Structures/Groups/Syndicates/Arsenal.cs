﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Arsenal.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Networking.GameServer;
using GameServer.Structures.Entities;
using GameServer.Structures.Items;

#endregion

namespace GameServer.Structures.Groups.Syndicates
{
    public sealed class Arsenal
    {
        public Syndicate Owner;
        public ConcurrentDictionary<TotemPoleType, TotemPole> Poles;
        public byte BattlePower;

        private List<SyndicateTotemEnhanceEntity> m_lEnhancement;

        public Arsenal(Syndicate owner)
        {
            Owner = owner;
            Poles = new ConcurrentDictionary<TotemPoleType, TotemPole>();

            m_lEnhancement = new SyndicateTotemEnhanceRepository().GetFromSyndicate(owner.Identity).ToList();
        }

        /// <summary>
        /// Used to add items to the totem pole on the server startup.
        /// </summary>
        /// <param name="item">The item that will be inserted into the arsenal.</param>
        /// <param name="totem">The totem object.</param>
        /// <returns>If the item has been inscribed successfully.</returns>
        public bool AddItem(Item item, Totem totem)
        {
            TotemPoleType type = GetArsenalType(item);
            if (type == TotemPoleType.TOTEM_NONE)
                return false;

            if (!Poles.ContainsKey(type))
                return false; // doesn't contain the required type

            if (Poles[type].Locked)
                return false;

            if (item.GetQuality() < 8 || item.IsArrowSort())
                return false;

            if (Poles[type].Items.ContainsKey(item.Identity))
                return false; // that item is already inscribed?

            if (!Poles[type].Items.TryAdd(item.Identity, totem))
                return false;

            item.IsInscribed = true;
            item.Save();
            return true;
        }

        public bool InscribeItem(Item pItem, Character pUser)
        {
            TotemPoleType pType = GetArsenalType(pItem);

            if (pType == TotemPoleType.TOTEM_NONE) // invalid item
                return false;

            if (!Poles.ContainsKey(pType)) // arsenal probably closed
                return false;

            if (pUser.UserPackage[pItem.Identity] == null) // item should be in the user inventory
                return false;

            if (pItem.GetQuality() < 8 || pItem.IsArrowSort())
                return false;

            if (Poles[pType].Items.ContainsKey(pItem.Identity)) // already inscribed
                return false;

            int nTotal = Poles[pType].Items.Values.Count(x => x.PlayerIdentity == pUser.Identity);
            if (nTotal >= MaxPerType(pUser)) // inscribed max items
                return false;

            var totem = new Totem(pItem, pUser);
            if (!Poles[pType].Items.TryAdd(pItem.Identity, totem))
                return false;

            uint dwOldBp = BattlePower;

            pItem.IsInscribed = true;
            pUser.Send(pItem.CreateMsgItemInfo(ItemMode.Update));
            totem.Save();
            UpdatePoles();
            pUser.SyndicateMember.ArsenalDonation += totem.Donation();

            if (dwOldBp != BattlePower)
                SendBattlePower();

            SendArsenal(pUser);
            return true;
        }

        public bool UninscribeItem(uint idItem, Character pUser)
        {
            ItemEntity dbItem = Database.ItemRepository.FetchByIdentity(idItem);
            if (dbItem == null)
                return false;

            TotemPoleType pType = GetArsenalType(Calculations.GetItemPosition(dbItem.Type));
            if (pType == TotemPoleType.TOTEM_NONE)
                return false;

            Totem pPole;
            if (!Poles.ContainsKey(pType) || !Poles[pType].Items.TryGetValue(idItem, out pPole))
                return false;

            return UninscribeItem(pPole.Item, pUser);
        }

        public bool UninscribeItem(Item pItem, Character pUser)
        {
            TotemPoleType pType = GetArsenalType(pItem);
            if (pType == TotemPoleType.TOTEM_NONE) // shit happens
                return false;

            if (!Poles.ContainsKey(pType))
                return false;

            if (!Poles[pType].Items.ContainsKey(pItem.Identity))
                return false;

            uint dwOldBp = BattlePower;

            Totem pTotem;
            if (!Poles[pType].Items.TryRemove(pItem.Identity, out pTotem))
                return false;

            pTotem.Delete();
            pItem.IsInscribed = false;
            pUser.Send(pItem.CreateMsgItemInfo(ItemMode.Update));
            UpdatePoles();

            if (dwOldBp != BattlePower)
                SendBattlePower();

            SendArsenal(pUser);
            return true;
        }

        public void RemoveAllFromUser(uint idUser)
        {
            uint oldBp = BattlePower;
            foreach (var pole in Poles.Values)
            {
                List<uint> remove = new List<uint>();
                foreach (var totem in pole.Items.Values.Where(x => x.PlayerIdentity == idUser))
                {
                    totem.Item.IsInscribed = false;
                    totem.Remove();
                    totem.Delete();
                    remove.Add(totem.Identity);
                }
                Totem trash;
                for (int i = 0; i < remove.Count; i++)
                    pole.Items.TryRemove(remove[i], out trash);
                trash = null;
            }

            Character pUser = ServerKernel.UserManager.GetUser(idUser);
            if (pUser != null)
            {
                foreach (var item in pUser.UserPackage.GetAll())
                {
                    item.IsInscribed = false;
                    pUser.Send(item.CreateMsgItemInfo(ItemMode.Update));
                    item.Save();
                }
                // todo inventory
                // todo handle user online and offline
            }
            else
            {
                IList<ItemEntity> allItems = Database.ItemRepository.FetchAll(idUser);
                foreach (var item in allItems)
                {
                    item.Inscribed = 0;
                }
            }

            //Database.

            UpdatePoles();
            if (oldBp != BattlePower)
                SendBattlePower();
        }

        private bool OpenArsenal(TotemPoleType pType)
        {
            Owner.LastOpenTotem = DateTime.Now;

            switch (pType)
            {
                case TotemPoleType.TOTEM_HEADGEAR:
                    Owner.TotemHeadwear = true;
                    break;
                case TotemPoleType.TOTEM_NECKLACE:
                    Owner.TotemNecklace = true;
                    break;
                case TotemPoleType.TOTEM_RING:
                    Owner.TotemRing = true;
                    break;
                case TotemPoleType.TOTEM_WEAPON:
                    Owner.TotemWeapon = true;
                    break;
                case TotemPoleType.TOTEM_ARMOR:
                    Owner.TotemArmor = true;
                    break;
                case TotemPoleType.TOTEM_BOOTS:
                    Owner.TotemBoots = true;
                    break;
                case TotemPoleType.TOTEM_FAN:
                    Owner.TotemHeavenFan = true;
                    break;
                case TotemPoleType.TOTEM_TOWER:
                    Owner.TotemStarTower = true;
                    break;
            }

            return Poles.TryAdd(pType, new TotemPole(pType) { Locked = false });
        }

        public bool UnlockArsenal(TotemPoleType pType, Character pUser)
        {
            if (pUser.SyndicateMember == null || pUser.Syndicate == null || pUser.Syndicate != Owner
                ||
                (pUser.SyndicateMember.Position != SyndicateRank.GuildLeader &&
                 pUser.SyndicateMember.Position != SyndicateRank.LeaderSpouse &&
                 pUser.SyndicateMember.Position != SyndicateRank.DeputyLeader &&
                 pUser.SyndicateMember.Position != SyndicateRank.HonoraryDeputyLeader))
            {
                pUser.SendSysMessage(Language.StrSynTotemPoleUnlockErrPermission, Color.Red);
                return false;
            }

            if (pType == TotemPoleType.TOTEM_NONE)
            {
                pUser.SendSysMessage(Language.StrSynTotemPoleUnlockErrInvalid, Color.Red);
                return false;
            }

            if (Poles.ContainsKey(pType))
            {
                pUser.SendSysMessage(Language.StrSynTotemPoleUnlockErrAlreadyOpen, Color.Red);
                return false;
            }

            uint dwPrice = UnlockValue();

            if (Owner.SilverDonation < dwPrice)
            {
                pUser.SendSysMessage(Language.StrSynTotemPoleUnlockErrNotEnoughFunds, Color.Red);
                return false;
            }

            if (DateTime.Now <= Owner.LastOpenTotem)
            {
                pUser.SendSysMessage(Language.StrSynTotemPoleUnlockErrOpenedToday, Color.Red);
                return false;
            }

            if (!Owner.ChangeFunds((int)UnlockValue() * -1))
                return false;

            OpenArsenal(pType);
            SendArsenal(pUser);
            pUser.SyndicateMember.SendSyndicate();
            return true;
        }

        public void SendBattlePower()
        {
            foreach (var member in Owner.Members.Values.Where(x => x.IsOnline))
            {
                member.User.UpdateAttributes();
                member.User.UpdateClient(ClientUpdateType.GuildBattlepower, SharedBattlePower(member.Position, member.User), false);
                member.User.TotemBattlePower = SharedBattlePower(member.Position, member.User);
            }
        }

        public int MaxPerType(Character pUser)
        {
            if (pUser.Metempsychosis == 0)
            {
                if (pUser.Level < 100)
                    return 7;
                return 14;
            }
            if (pUser.Metempsychosis == 1)
                return 21;
            return 30;
        }

        public static TotemPoleType GetArsenalType(Item item)
        {
            return GetArsenalType(Calculations.GetItemPosition(item.Type));
        }

        public static TotemPoleType GetArsenalType(ItemPosition pos)
        {
            switch (pos)
            {
                case ItemPosition.Headwear: return TotemPoleType.TOTEM_HEADGEAR;
                case ItemPosition.Necklace: return TotemPoleType.TOTEM_NECKLACE;
                case ItemPosition.Ring: return TotemPoleType.TOTEM_RING;
                case ItemPosition.LeftHand:
                case ItemPosition.RightHand: return TotemPoleType.TOTEM_WEAPON;
                case ItemPosition.Armor: return TotemPoleType.TOTEM_ARMOR;
                case ItemPosition.Boots: return TotemPoleType.TOTEM_BOOTS;
                case ItemPosition.AttackTalisman: return TotemPoleType.TOTEM_FAN;
                case ItemPosition.DefenceTalisman: return TotemPoleType.TOTEM_TOWER;
                default: return TotemPoleType.TOTEM_NONE;
            }
        }

        public void SendArsenal(Character pUser)
        {
            var pMsg = new MsgTotemPoleInfo();
            UpdatePoles();
            var orderedTotem = Poles.Values.OrderByDescending(x => x.Donation).ToList();
            for (TotemPoleType i = TotemPoleType.TOTEM_HEADGEAR; i < TotemPoleType.TOTEM_NONE; i++)
            {
                TotemPole totem = orderedTotem.FirstOrDefault(x => x.Type == i);
                if (totem == null)
                {
                    pMsg.AddTotemPole(i, 0, 0, 0, false);
                    continue;
                }
                pMsg.AddTotemPole(totem.Type, (uint) (totem.BattlePower - GetEnhancement(totem.Type)), GetEnhancement(totem.Type), (uint)totem.Donation, true);
            }
            pMsg.SharedBattlePower = BattlePower;
            pMsg.BattlePower = SharedBattlePower(pUser.SyndicateMember.Position, pUser);
            pMsg.TotemDonation = pUser.SyndicateMember.ArsenalDonation;

            if (pUser.Syndicate.Arsenal != null)
                pUser.UpdateClient(ClientUpdateType.GuildBattlepower, pUser.Syndicate.Arsenal.SharedBattlePower(pUser));

            pUser.Send(pMsg);
        }

        public void UpdatePoles()
        {
            foreach (var pole in Poles.Values)
            {
                pole.BattlePower = 0;
                pole.Donation = 0;
                foreach (var totem in pole.Items.Values)
                    pole.Donation += totem.Donation();

                if (pole.Donation >= 2000000)
                {
                    pole.BattlePower++;
                    if (pole.Donation >= 4000000)
                    {
                        pole.BattlePower++;
                        if (pole.Donation >= 10000000)
                            pole.BattlePower++;
                    }
                }

                pole.BattlePower = (byte) Math.Min(3, pole.BattlePower + GetEnhancement(pole.Type));
            }

            BattlePower = 0;
            int nCount = 0;
            foreach (var pole in Poles.Values.OrderByDescending(x => x.Donation))
            {
                if (nCount++ >= 5)
                    break;
                BattlePower += pole.BattlePower;
            }
        }

        public uint GetBattlePower()
        {
            UpdatePoles();
            return BattlePower;
        }

        public uint SharedBattlePower(User client) { return SharedBattlePower(client.Character.SyndicatePosition, client.Character); }

        public uint SharedBattlePower(Character client) { return SharedBattlePower(client.SyndicatePosition, client); }

        public uint SharedBattlePower(SyndicateRank rank, Character user)
        {
            uint totalBp = BattlePower;

            /**
             * Users with less than 1 week will get the position rule for battle power.
             * After 1 week on the syndicate it will get 100% bp.
             */
            if (user != null)
            {
                if (user.SyndicateMember.DaysOnSyndicate >= 7)
                    return totalBp;
            }
            else
            {
                Program.WriteLog($"System is asking for shared bp with null user.", LogType.WARNING);
            }

            switch (rank)
            {
                case SyndicateRank.GuildLeader:
                case SyndicateRank.LeaderSpouse:
                    return totalBp;
                case SyndicateRank.DeputyLeader:
                case SyndicateRank.HonoraryDeputyLeader:
                    return (uint)(totalBp * 0.9f);
                case SyndicateRank.Manager:
                case SyndicateRank.HonoraryManager:
                    return (uint)(totalBp * 0.8f);
                case SyndicateRank.Supervisor:
                case SyndicateRank.ArsenalSupervisor:
                case SyndicateRank.CpSupervisor:
                case SyndicateRank.GuideSupervisor:
                case SyndicateRank.HonorarySupervisor:
                case SyndicateRank.LilySupervisor:
                case SyndicateRank.OrchidSupervisor:
                case SyndicateRank.PkSupervisor:
                case SyndicateRank.RoseSupervisor:
                case SyndicateRank.SilverSupervisor:
                case SyndicateRank.TulipSupervisor:
                    return (uint)(totalBp * 0.7f);
                case SyndicateRank.Steward:
                case SyndicateRank.DeputyLeaderSpouse:
                case SyndicateRank.LeaderSpouseAide:
                case SyndicateRank.DeputyLeaderAide:
                    return (uint)(totalBp * 0.5f);
                case SyndicateRank.DeputySteward:
                    return (uint)(totalBp * 0.4f);
                case SyndicateRank.Agent:
                case SyndicateRank.SupervisorSpouse:
                case SyndicateRank.ManagerSpouse:
                case SyndicateRank.SupervisorAide:
                case SyndicateRank.ManagerAide:
                case SyndicateRank.ArsenalAgent:
                case SyndicateRank.CpAgent:
                case SyndicateRank.GuideAgent:
                case SyndicateRank.LilyAgent:
                case SyndicateRank.OrchidAgent:
                case SyndicateRank.PkAgent:
                case SyndicateRank.RoseAgent:
                case SyndicateRank.SilverAgent:
                case SyndicateRank.TulipAgent:
                    return (uint)(totalBp * 0.3f);
                case SyndicateRank.StewardSpouse:
                case SyndicateRank.Follower:
                case SyndicateRank.ArsenalFollower:
                case SyndicateRank.CpFollower:
                case SyndicateRank.GuideFollower:
                case SyndicateRank.LilyFollower:
                case SyndicateRank.OrchidFollower:
                case SyndicateRank.PkFollower:
                case SyndicateRank.RoseFollower:
                case SyndicateRank.SilverFollower:
                case SyndicateRank.TulipFollower:
                    return (uint)(totalBp * 0.2f);
                case SyndicateRank.SeniorMember:
                    return (uint)(totalBp * 0.15f);
                case SyndicateRank.Member:
                    return (uint)(totalBp * 0.1f);
                default:
                    return 0;
            }
        }

        public void Enhance(Character user, TotemPoleType type, byte power)
        {
            SyndicateTotemEnhanceRepository repo = new SyndicateTotemEnhanceRepository();
            if (HasActiveEnhancement(type))
            {
                SyndicateTotemEnhanceEntity oldEnhance = m_lEnhancement.FirstOrDefault(x => x.Totem == (int) type);
                repo.Delete(oldEnhance);
                m_lEnhancement.Remove(oldEnhance);
            }

            SyndicateTotemEnhanceEntity enhance = new SyndicateTotemEnhanceEntity
            {
                UserIdentity = user?.Identity ??0u,
                SyndicateIdentity = Owner.Identity,
                StartTime = DateTime.Now,
                EndTime = DateTime.Now.AddDays(30),
                Enhancement = power,
                Totem = (byte) type
            };
            repo.Save(enhance);
            m_lEnhancement.Add(enhance);
        }

        public bool HasActiveEnhancement(TotemPoleType type)
        {
            return m_lEnhancement.Any(x => x.Totem == (int) type && x.StartTime <= DateTime.Now && x.EndTime > DateTime.Now);
        }

        public byte GetEnhancement(TotemPoleType type)
        {
            return m_lEnhancement
                       .FirstOrDefault(x =>
                           x.Totem == (int) type && x.StartTime <= DateTime.Now && x.EndTime > DateTime.Now)
                       ?.Enhancement ?? 0;
        }

        public uint UnlockValue()
        {
            switch (Poles.Count)
            {
                case 0:
                case 1: return 5000000;
                case 2:
                case 3:
                case 4: return 10000000;
                case 5:
                case 6: return 15000000;
                case 7: return 20000000;
            }
            return 0;
        }
    }
}