﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Totem.cs
// Last Edit: 2019/12/16 16:19
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using GameServer.Structures.Entities;
using GameServer.Structures.Items;

#endregion

namespace GameServer.Structures.Groups.Syndicates
{
    public sealed class Totem
    {
        private Item m_pItem;
        private readonly SyndicateTotemEntity m_pTotem;

        public Totem(Item pItem, Character pOwner)
        {
            m_pTotem = new SyndicateTotemEntity
            {
                Itemid = pItem.Identity,
                Synid = pOwner.SyndicateIdentity,
                Userid = pOwner.Identity,
                Username = pOwner.Name
            };

            if (m_pTotem.Synid <= 0)
                throw new Exception("Synid cannot be zero.");

            m_pItem = pItem;
        }

        public Totem(SyndicateTotemEntity dbTotem, Item pItem)
        {
            m_pItem = pItem;
            m_pTotem = dbTotem;
        }

        public Character Owner => ServerKernel.UserManager.GetUser(m_pTotem.Userid);

        public uint Identity => m_pItem.Identity;

        public string Name => m_pItem.Name;

        public uint PlayerIdentity => m_pTotem.Userid;

        public string PlayerName => m_pTotem.Username;

        public Item Item
        {
            get
            {
                Item item = Owner?.UserPackage.Find(Identity);
                if (item != null)
                    m_pItem = item;
                return m_pItem;
            }
        }

        public uint Itemtype => m_pItem.Type;

        public uint Donation()
        {
            uint result = 0;
            var id = Item?.Type % 10;

            switch (id)
            {
                case 8:
                    result = 1000;
                    break;
                case 9:
                    result = 16660;
                    break;
                default: return 0;
            }

            if (Item?.SocketOne > 0 && Item?.SocketTwo == 0)
                result += 33330;
            if (Item?.SocketOne > 0 && Item?.SocketTwo > 0)
                result += 133330;

            switch (Item?.Addition)
            {
                case 1:
                    result += 90;
                    break;
                case 2:
                    result += 490;
                    break;
                case 3:
                    result += 1350;
                    break;
                case 4:
                    result += 4070;
                    break;
                case 5:
                    result += 12340;
                    break;
                case 6:
                    result += 37030;
                    break;
                case 7:
                    result += 111110;
                    break;
                case 8:
                    result += 333330;
                    break;
                case 9:
                    result += 1000000;
                    break;
                case 10:
                    result += 1033330;
                    break;
                case 11:
                    result += 1101230;
                    break;
                case 12:
                    result += 1212340;
                    break;
            }

            return result;
        }

        public void Remove()
        {
            Item.IsInscribed = false;
            if (Owner != null && Item.Position < ItemPosition.Crop)
                Owner.Send(m_pItem.CreateMsgItemInfo(ItemMode.Update));
        }

        public bool Save()
        {
            return new SyndicateTotemRepository().Save(m_pTotem);
        }

        public bool Delete()
        {
            return new SyndicateTotemRepository().Delete(m_pTotem);
        }
    }
}