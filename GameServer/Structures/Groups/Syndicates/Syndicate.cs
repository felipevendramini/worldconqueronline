﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Syndicate.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Concurrent;
using System.Drawing;
using System.Linq;
using System.Reflection;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Networking.GameServer;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Structures.Groups.Syndicates
{
    public sealed class Syndicate
    {
        #region Private Getters

        private int MaxDeputyLeader => Level < 4 ? 2 : Level < 7 ? 3 : 4;

        private int MaxHonoraryDeputyLeader => Level < 6 ? 1 : 2;

        private int MaxHonoraryManager => Level < 5 ? 1 : Level < 7 ? 2 : Level < 9 ? 4 : 6;

        private int MaxHonorarySupervisor => Level < 5 ? 1 : Level < 7 ? 2 : Level < 9 ? 4 : 6;

        private int MaxHonorarySteward => Level < 3 ? 1 : Level < 5 ? 2 : Level < 7 ? 4 : Level < 9 ? 6 : 8;

        private int MaxAide => Level < 4 ? 2 : Level < 6 ? 4 : 6;

        private int MaxManager
        {
            get
            {
                switch (Level)
                {
                    case 1:
                    case 2: return 1;
                    case 3:
                    case 4: return 2;
                    case 5:
                    case 6: return 4;
                    case 7:
                    case 8: return 6;
                    case 9: return 8;
                    default: return 0;
                }
            }
        }

        private int MaxSupervisor => Level < 4 ? 0 : Level < 8 ? 1 : 2;

        private int MaxSteward
        {
            get
            {
                switch (Level)
                {
                    case 0:
                    case 1: return 0;
                    case 2: return 1;
                    case 3: return 2;
                    case 4: return 3;
                    case 5: return 4;
                    case 6: return 5;
                    case 7: return 6;
                    case 8:
                    case 9: return 8;
                    default: return 0;
                }
            }
        }

        #endregion

        public const int HONORARY_DEPUTY_LEADER_PRICE = -650,
            HONORARY_MANAGER_PRICE = -320,
            HONORARY_SUPERVISOR_PRICE = -250,
            HONORARY_STEWARD_PRICE = -100;

        public ConcurrentDictionary<uint, Syndicate> Allies;
        public ConcurrentDictionary<uint, Syndicate> Enemies;

        public Arsenal Arsenal;

        private SyndicateEntity m_dbSyn;
        private ushort m_usMembers;

        public ConcurrentDictionary<uint, SyndicateMember> Members;

        #region Announcement

        public string Announcement
        {
            get => m_dbSyn.Announce;
            set
            {
                m_dbSyn.Announce = value;
                m_dbSyn.AnnounceDate = uint.Parse(DateTime.Now.ToString("yyyyMMdd"));
                Save();
            }
        }

        #endregion

        #region Contribution Rank

        public uint MinimumDonation(SyndicateRank pos)
        {
            try
            {
                PropertyInfo pInfo = null;
                int nMaxCount = 0;
                switch (pos)
                {
                    case SyndicateRank.Manager:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("TotalDonation");
                        nMaxCount = MaxManager;
                        break;
                    }

                    case SyndicateRank.Supervisor:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("TotalDonation");
                        nMaxCount = MaxSupervisor;
                        break;
                    }

                    case SyndicateRank.ArsenalSupervisor:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("ArsenalDonation");
                        nMaxCount = MaxSupervisor;
                        break;
                    }

                    case SyndicateRank.CpSupervisor:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("EmoneyDonation");
                        nMaxCount = MaxSupervisor;
                        break;
                    }

                    case SyndicateRank.SilverSupervisor:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("SilverDonation");
                        nMaxCount = MaxSupervisor;
                        break;
                    }

                    case SyndicateRank.GuideSupervisor:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("GuideDonation");
                        nMaxCount = MaxSupervisor;
                        break;
                    }

                    case SyndicateRank.PkSupervisor:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("PkDonation");
                        nMaxCount = MaxSupervisor;
                        break;
                    }

                    case SyndicateRank.RoseSupervisor:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("RedRoseDonation");
                        nMaxCount = MaxSupervisor;
                        break;
                    }

                    case SyndicateRank.LilySupervisor:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("WhiteRoseDonation");
                        nMaxCount = MaxSupervisor;
                        break;
                    }

                    case SyndicateRank.OrchidSupervisor:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("OrchidDonation");
                        nMaxCount = MaxSupervisor;
                        break;
                    }

                    case SyndicateRank.TulipSupervisor:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("TulipDonation");
                        nMaxCount = MaxSupervisor;
                        break;
                    }

                    case SyndicateRank.Steward:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("TotalDonation");
                        nMaxCount = MaxSteward;
                        break;
                    }

                    case SyndicateRank.Agent:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("TotalDonation");
                        break;
                    }

                    case SyndicateRank.ArsenalAgent:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("ArsenalDonation");
                        break;
                    }

                    case SyndicateRank.CpAgent:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("EmoneyDonation");
                        break;
                    }

                    case SyndicateRank.SilverAgent:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("SilverDonation");
                        break;
                    }

                    case SyndicateRank.GuideAgent:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("GuideDonation");
                        break;
                    }

                    case SyndicateRank.PkAgent:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("PkDonation");
                        break;
                    }

                    case SyndicateRank.RoseAgent:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("RedRoseDonation");
                        nMaxCount = 1;
                        break;
                    }

                    case SyndicateRank.LilyAgent:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("WhiteRoseDonation");
                        nMaxCount = 1;
                        break;
                    }

                    case SyndicateRank.OrchidAgent:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("OrchidDonation");
                        nMaxCount = 1;
                        break;
                    }

                    case SyndicateRank.TulipAgent:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("TulipDonation");
                        nMaxCount = 1;
                        break;
                    }

                    case SyndicateRank.Follower:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("TotalDonation");
                        break;
                    }

                    case SyndicateRank.ArsenalFollower:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("ArsenalDonation");
                        break;
                    }

                    case SyndicateRank.CpFollower:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("EmoneyDonation");
                        break;
                    }

                    case SyndicateRank.SilverFollower:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("SilverDonation");
                        break;
                    }

                    case SyndicateRank.GuideFollower:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("GuideDonation");
                        break;
                    }

                    case SyndicateRank.PkFollower:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("PkDonation");
                        break;
                    }

                    case SyndicateRank.RoseFollower:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("RedRoseDonation");
                        nMaxCount = 1;
                        break;
                    }

                    case SyndicateRank.LilyFollower:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("WhiteRoseDonation");
                        nMaxCount = 1;
                        break;
                    }

                    case SyndicateRank.OrchidFollower:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("OrchidDonation");
                        nMaxCount = 1;
                        break;
                    }

                    case SyndicateRank.TulipFollower:
                    {
                        pInfo = typeof(SyndicateMember).GetProperty("TulipDonation");
                        nMaxCount = 1;
                        break;
                    }

                    default:
                        return 0;
                }

                if (pInfo != null && pos != SyndicateRank.DeputySteward)
                {
                    int nCount = Members.Values.Count(x => x.Position == pos);
                    return nMaxCount <= 0 || nMaxCount > nCount
                        ? 0
                        : uint.Parse(pInfo.GetValue(Members.Values.OrderBy(x => pInfo).FirstOrDefault()).ToString());
                }

                return (uint) (pos == SyndicateRank.SeniorMember ? 25000 :
                    pos == SyndicateRank.DeputySteward ? 175000 : 0);
            }
            catch (Exception ex)
            {
                // of course will not throw shit
                // i hope
                Program.WriteLog($@"The unexpected happened! Pos:{pos}", LogType.ERROR);
                Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
            }

            return 0;
        }

        #endregion

        #region Create

        public bool Create(string name, Character owner)
        {
            Members = new ConcurrentDictionary<uint, SyndicateMember>(10, 800);
            m_dbSyn = new SyndicateEntity
            {
                Amount = 1,
                Announce = "This is a new guild.",
                DelFlag = 0,
                LeaderIdentity = owner.Identity,
                LeaderName = owner.Name,
                Money = 500000,
                Name = name,
                ReqLevel = 1,
                AnnounceDate = uint.Parse(DateTime.Now.ToString("yyyyMMdd"))
            };
            Save();
            Level = 1;

            Arsenal = new Arsenal(this);
            Allies = new ConcurrentDictionary<uint, Syndicate>();
            Enemies = new ConcurrentDictionary<uint, Syndicate>();
            return true;
        }

        public bool Create(SyndicateEntity syndicate)
        {
            m_dbSyn = syndicate;
            LeaderIdentity = syndicate.LeaderIdentity;
            Members = new ConcurrentDictionary<uint, SyndicateMember>(10, 800);

            Arsenal = new Arsenal(this);
            Allies = new ConcurrentDictionary<uint, Syndicate>();
            Enemies = new ConcurrentDictionary<uint, Syndicate>();

            CheckLeaderSpouse();
            return true;
        }

        #endregion

        #region Identity

        public uint Identity => m_dbSyn.Identity;

        public string Name
        {
            get => m_dbSyn.Name;
            set
            {
                m_dbSyn.Name = value;
                Save();
            }
        }

        public byte Level { get; set; }

        public ushort MemberCount
        {
            get => m_usMembers;
            set
            {
                m_usMembers = value;
                m_dbSyn.Amount = value;
                Save();
            }
        }

        public bool IsDeleted => m_dbSyn.DelFlag != 0;

        #endregion

        #region Leader

        public uint LeaderIdentity
        {
            get => m_dbSyn.LeaderIdentity;
            set
            {
                m_dbSyn.LeaderIdentity = value;
                Save();
            }
        }

        public string LeaderName
        {
            get => m_dbSyn.LeaderName;
            set
            {
                m_dbSyn.LeaderName = value;
                Save();
            }
        }

        public bool LeaderIsOnline => ServerKernel.UserManager.GetUser(LeaderIdentity) != null;

        public Character LeaderRole => ServerKernel.UserManager.GetUser(LeaderIdentity);

        #endregion

        #region Currency

        public ulong SilverDonation
        {
            get => m_dbSyn.Money;
            set
            {
                m_dbSyn.Money = value;
                Save();
            }
        }

        public uint EmoneyDonation
        {
            get => m_dbSyn.EMoney;
            set
            {
                m_dbSyn.EMoney = value;
                Save();
            }
        }

        /// <summary>
        /// Silver reward set for the next event.
        /// </summary>
        public long MoneyPrize
        {
            get => m_dbSyn.MoneyPrize;
            set
            {
                m_dbSyn.MoneyPrize = value;
                Save();
            }
        }

        /// <summary>
        ///  Emoney reward set for the next event.
        /// </summary>
        public uint EmoneyPrize
        {
            get => m_dbSyn.EmoneyPrize;
            set
            {
                m_dbSyn.EmoneyPrize = value;
                Save();
            }
        }

        #region Funds Management

        public bool ChangeFunds(int nAmount)
        {
            if (nAmount < 0)
            {
                if (nAmount + (long) SilverDonation < 0)
                {
                    SilverDonation = (ulong) Math.Max(nAmount + (long) SilverDonation, 0);
                    return false;
                }

                nAmount *= -1;
                SilverDonation -= (ulong) nAmount;
                return Save();
            }

            if (nAmount > 0)
            {
                if ((ulong) nAmount + SilverDonation > long.MaxValue)
                {
                    SilverDonation = long.MaxValue;
                    return Save();
                }

                SilverDonation += (ulong) nAmount;
                return Save();
            }

            return false;
        }

        public bool ChangeEmoneyFunds(int nAmount)
        {
            if (nAmount < 0)
            {
                if (nAmount + EmoneyDonation < 0)
                    return false;
                nAmount *= -1;
                EmoneyDonation -= (uint) nAmount;
                return Save();
            }

            if (nAmount > 0)
            {
                if ((uint) nAmount + EmoneyDonation > int.MaxValue)
                {
                    EmoneyDonation = int.MaxValue;
                    return Save();
                }

                EmoneyDonation += (uint) nAmount;
                return Save();
            }

            return false;
        }

        #endregion

        #endregion

        #region Alliance / Antagonize
        public bool IsHostile(ushort identity) { return Enemies.ContainsKey(identity); }

        public bool AntagonizeSyndicate(string szName)
        {
            Syndicate temp = Enemies.Values.FirstOrDefault(x => x.Name == szName);
            if (temp != null)
                return false;
            var syn = ServerKernel.SyndicateManager.GetSyndicate(szName);
            if (syn == null || syn.IsDeleted) return false;
            return AntagonizeSyndicate(syn);
        }

        public bool AntagonizeSyndicate(ushort identity)
        {
            Syndicate temp = ServerKernel.SyndicateManager.GetSyndicate(identity);
            if (Enemies.ContainsKey(identity) || temp == null || temp.IsDeleted)
                return false;

            return AntagonizeSyndicate(temp);
        }

        public bool AntagonizeSyndicate(Syndicate syn)
        {
            if (Enemies.ContainsKey((ushort)syn.Identity) || Allies.ContainsKey((ushort)syn.Identity)) return false;

            if (Enemies.Count >= 5)
                return false;

            var enemy = new SyndicateEnemyEntity
            {
                Enemyid = syn.Identity,
                Enemyname = syn.Name,
                Synid = (ushort)Identity,
                Synname = Name
            };

            if (Enemies.TryAdd((ushort)syn.Identity, syn) && new SyndicateEnemyRepository().Save(enemy))
            {
                var msg = new MsgSyndicate { Action = SyndicateRequest.SYN_ENEMIED, Param = syn.Identity };
                Send(msg);

                var pMsg = new MsgName
                {
                    Action = StringAction.SetEnemy,
                    Identity = syn.Identity
                };
                pMsg.Append($"{syn.Name} {syn.LeaderName} {syn.Level} {syn.MemberCount}");
                Send(pMsg);

                new SyndicateEnemyRepository().Save(enemy);

                Send(string.Format(Language.StrSynEnemy1, LeaderName, syn.Name));
                syn.Send(string.Format(Language.StrSynEnemy0, LeaderName, Name));
                return true;
            }
            return false;
        }

        public bool RemoveEnemy(uint identity)
        {
            Syndicate temp;
            if (!Enemies.TryRemove(identity, out temp)) return false;
            new SyndicateEnemyRepository().DeleteAntagonize((ushort)identity, (ushort)Identity);

            var msg = new MsgSyndicate
            {
                Action = SyndicateRequest.SYN_NEUTRAL2,
                Param = identity
            };
            Send(msg);

            Send(string.Format(Language.StrSynEnemyRemove0, LeaderName, temp.Name));
            temp.Send(string.Format(Language.StrSynEnemyRemove1, LeaderName, Name));

            return true;
        }

        public bool AllySyndicate(string szName)
        {
            Syndicate temp = Allies.Values.FirstOrDefault(x => x.Name == szName);
            if (temp != null)
                return false;
            var syn = ServerKernel.SyndicateManager.GetSyndicate(szName);
            if (syn == null || syn.IsDeleted) return false;
            return AllySyndicate(syn);
        }

        public bool AllySyndicate(ushort identity)
        {
            Syndicate temp = ServerKernel.SyndicateManager.GetSyndicate(identity);
            if (Allies.ContainsKey(identity) || temp == null || temp.IsDeleted)
                return false;

            return AllySyndicate(temp);
        }

        public bool AllySyndicate(Syndicate syn)
        {
            if (Allies.ContainsKey((ushort)syn.Identity) || Enemies.ContainsKey((ushort)syn.Identity)) return false;

            if (Allies.Count >= MaxAllies())
                return false;

            if (Allies.TryAdd((ushort)syn.Identity, syn) && syn.Allies.TryAdd((ushort)Identity, this))
            {
                new SyndicateAllyRepository().Save(new SyndicateAllyEntity
                {
                    Allyid = (ushort)syn.Identity,
                    Allyname = syn.Name,
                    Synid = (ushort)Identity,
                    Synname = Name
                });
                new SyndicateAllyRepository().Save(new SyndicateAllyEntity
                {
                    Allyid = Identity,
                    Allyname = Name,
                    Synid = syn.Identity,
                    Synname = syn.Name
                });

                var msg = new MsgSyndicate { Action = SyndicateRequest.SYN_ALLIED, Param = syn.Identity };
                Send(msg);

                var pMsg = new MsgName
                {
                    Action = StringAction.SetAlly,
                    Identity = syn.Identity
                };
                pMsg.Append($"{syn.Name} {syn.LeaderName} {syn.Level} {syn.MemberCount}");
                Send(pMsg);

                msg.Param = Identity;
                syn.Send(msg);
                pMsg = new MsgName
                {
                    Action = StringAction.SetAlly,
                    Identity = Identity
                };
                pMsg.Append($"{Name} {LeaderName} {Level} {MemberCount}");
                syn.Send(pMsg);

                return true;
            }
            return false;
        }

        public bool RemoveAlliance(uint identity)
        {
            Syndicate temp;
            if (!Allies.TryRemove(identity, out temp) || temp.Allies.TryRemove(Identity, out _)) return false;
            new SyndicateAllyRepository().DeleteRelationship((ushort)identity, (ushort)Identity);

            var msg = new MsgSyndicate
            {
                Action = SyndicateRequest.SYN_NEUTRAL1,
                Param = identity
            };

            Send(msg);
            Send(string.Format(Language.StrSynAllyRemove0, LeaderName, temp.Name));

            msg.Param = Identity;
            temp.Send(msg);
            temp.Send(string.Format(Language.StrSynAllyRemove1, LeaderName, Name));

            return true;
        }

        public bool IsFriendly(ushort identity) { return Allies.ContainsKey(identity); }
        #endregion

        #region Totem Pole

        public DateTime LastOpenTotem
        {
            get => UnixTimestamp.ToDateTime(m_dbSyn.LastTotem);
            set
            {
                m_dbSyn.LastTotem = uint.Parse(value.ToString("yyyyMMdd"));
                Save();
            }
        }

        public bool TotemHeadwear
        {
            get => (m_dbSyn.TotemPole & (1 << (int) TotemPoleType.TOTEM_HEADGEAR)) != 0;
            set
            {
                if (value)
                {
                    m_dbSyn.TotemPole |= 1 << (int) TotemPoleType.TOTEM_HEADGEAR;
                }
                else
                {
                    m_dbSyn.TotemPole &= ~(1 << (int) TotemPoleType.TOTEM_HEADGEAR);
                }

                Save();
            }
        }

        public bool TotemNecklace
        {
            get => (m_dbSyn.TotemPole & (1 << (int) TotemPoleType.TOTEM_NECKLACE)) != 0;
            set
            {
                if (value)
                {
                    m_dbSyn.TotemPole |= 1 << (int) TotemPoleType.TOTEM_NECKLACE;
                }
                else
                {
                    m_dbSyn.TotemPole &= ~(1 << (int) TotemPoleType.TOTEM_NECKLACE);
                }

                Save();
            }
        }

        public bool TotemRing
        {
            get => (m_dbSyn.TotemPole & (1 << (int) TotemPoleType.TOTEM_RING)) != 0;
            set
            {
                if (value)
                {
                    m_dbSyn.TotemPole |= 1 << (int) TotemPoleType.TOTEM_RING;
                }
                else
                {
                    m_dbSyn.TotemPole &= ~(1 << (int) TotemPoleType.TOTEM_RING);
                }

                Save();
            }
        }

        public bool TotemWeapon
        {
            get => (m_dbSyn.TotemPole & (1 << (int) TotemPoleType.TOTEM_WEAPON)) != 0;
            set
            {
                if (value)
                {
                    m_dbSyn.TotemPole |= 1 << (int) TotemPoleType.TOTEM_WEAPON;
                }
                else
                {
                    m_dbSyn.TotemPole &= ~(1 << (int) TotemPoleType.TOTEM_WEAPON);
                }

                Save();
            }
        }

        public bool TotemArmor
        {
            get => (m_dbSyn.TotemPole & (1 << (int) TotemPoleType.TOTEM_ARMOR)) != 0;
            set
            {
                if (value)
                {
                    m_dbSyn.TotemPole |= 1 << (int) TotemPoleType.TOTEM_ARMOR;
                }
                else
                {
                    m_dbSyn.TotemPole &= ~(1 << (int) TotemPoleType.TOTEM_ARMOR);
                }

                Save();
            }
        }

        public bool TotemBoots
        {
            get => (m_dbSyn.TotemPole & (1 << (int) TotemPoleType.TOTEM_BOOTS)) != 0;
            set
            {
                if (value)
                {
                    m_dbSyn.TotemPole |= 1 << (int) TotemPoleType.TOTEM_BOOTS;
                }
                else
                {
                    m_dbSyn.TotemPole &= ~(1 << (int) TotemPoleType.TOTEM_BOOTS);
                }

                Save();
            }
        }

        public bool TotemHeavenFan
        {
            get => (m_dbSyn.TotemPole & (1 << (int) TotemPoleType.TOTEM_FAN)) != 0;
            set
            {
                if (value)
                {
                    m_dbSyn.TotemPole |= 1 << (int) TotemPoleType.TOTEM_FAN;
                }
                else
                {
                    m_dbSyn.TotemPole &= ~(1 << (int) TotemPoleType.TOTEM_FAN);
                }

                Save();
            }
        }

        public bool TotemStarTower
        {
            get => (m_dbSyn.TotemPole & (1 << (int) TotemPoleType.TOTEM_TOWER)) != 0;
            set
            {
                if (value)
                {
                    m_dbSyn.TotemPole |= 1 << (int) TotemPoleType.TOTEM_TOWER;
                }
                else
                {
                    m_dbSyn.TotemPole &= ~(1 << (int) TotemPoleType.TOTEM_TOWER);
                }

                Save();
            }
        }

        public void AddTotemPower(Character user, TotemPoleType type, byte power)
        {
            if (Arsenal.GetEnhancement(type) >= power)
                return;

            int nFunds;
            switch (power)
            {
                case 1:
                    nFunds = 60000000;
                    break;
                case 2:
                    nFunds = 100000000;
                    break;
                default:
                    return;
            }

            if (SilverDonation < (ulong) nFunds)
                return;

            ChangeFunds(nFunds * -1);
            Arsenal.Enhance(user, type, power);
        }
        #endregion

        #region Requirements



        public byte LevelRequirement
        {
            get => m_dbSyn.ReqLevel;
            set
            {
                m_dbSyn.ReqLevel = value;
                Save();
            }
        }

        public byte MetempsychosisRequirement
        {
            get => m_dbSyn.ReqMetempsychosis;
            set
            {
                m_dbSyn.ReqMetempsychosis = value;
                Save();
            }
        }

        public byte ProfessionRequirement
        {
            get => m_dbSyn.ReqClass;
            set
            {
                m_dbSyn.ReqClass = value;
                Save();
            }
        }

        public bool TrojanEnabled
        {
            get => (m_dbSyn.ReqClass & SyndicateProfessionRequirement.CLASS_TROJAN) == 0;
            set
            {
                if (value)
                {
                    m_dbSyn.ReqClass |= SyndicateProfessionRequirement.CLASS_TROJAN;
                }
                else
                {
                    m_dbSyn.ReqClass =
                        (byte) Calculations.RemoveFlag(m_dbSyn.ReqClass, SyndicateProfessionRequirement.CLASS_TROJAN);
                }

                Save();
            }
        }

        public bool WarriorEnabled
        {
            get => (m_dbSyn.ReqClass & SyndicateProfessionRequirement.CLASS_WARRIOR) == 0;
            set
            {
                if (value)
                {
                    m_dbSyn.ReqClass |= SyndicateProfessionRequirement.CLASS_WARRIOR;
                }
                else
                {
                    m_dbSyn.ReqClass = (byte) Calculations.RemoveFlag(m_dbSyn.ReqClass,
                        SyndicateProfessionRequirement.CLASS_WARRIOR);
                }

                Save();
            }
        }

        public bool ArcherEnabled
        {
            get => (m_dbSyn.ReqClass & SyndicateProfessionRequirement.CLASS_ARCHER) == 0;
            set
            {
                if (value)
                {
                    m_dbSyn.ReqClass |= SyndicateProfessionRequirement.CLASS_ARCHER;
                }
                else
                {
                    m_dbSyn.ReqClass =
                        (byte) Calculations.RemoveFlag(m_dbSyn.ReqClass, SyndicateProfessionRequirement.CLASS_ARCHER);
                }

                Save();
            }
        }

        public bool NinjaEnabled
        {
            get => (m_dbSyn.ReqClass & SyndicateProfessionRequirement.CLASS_NINJA) == 0;
            set
            {
                if (value)
                {
                    m_dbSyn.ReqClass |= SyndicateProfessionRequirement.CLASS_NINJA;
                }
                else
                {
                    m_dbSyn.ReqClass =
                        (byte) Calculations.RemoveFlag(m_dbSyn.ReqClass, SyndicateProfessionRequirement.CLASS_NINJA);
                }

                Save();
            }
        }

        public bool MonkEnabled
        {
            get => (m_dbSyn.ReqClass & SyndicateProfessionRequirement.CLASS_MONK) == 0;
            set
            {
                if (value)
                {
                    m_dbSyn.ReqClass |= SyndicateProfessionRequirement.CLASS_MONK;
                }
                else
                {
                    m_dbSyn.ReqClass =
                        (byte) Calculations.RemoveFlag(m_dbSyn.ReqClass, SyndicateProfessionRequirement.CLASS_MONK);
                }

                Save();
            }
        }

        public bool PirateEnabled
        {
            get => (m_dbSyn.ReqClass & SyndicateProfessionRequirement.CLASS_PIRATE) == 0;
            set
            {
                if (value)
                {
                    m_dbSyn.ReqClass |= SyndicateProfessionRequirement.CLASS_PIRATE;
                }
                else
                {
                    m_dbSyn.ReqClass =
                        (byte) Calculations.RemoveFlag(m_dbSyn.ReqClass, SyndicateProfessionRequirement.CLASS_PIRATE);
                }

                Save();
            }
        }

        public bool TaoistEnabled
        {
            get => (m_dbSyn.ReqClass & SyndicateProfessionRequirement.CLASS_TAOIST) == 0;
            set
            {
                if (value)
                {
                    m_dbSyn.ReqClass |= SyndicateProfessionRequirement.CLASS_TAOIST;
                }
                else
                {
                    m_dbSyn.ReqClass =
                        (byte) Calculations.RemoveFlag(m_dbSyn.ReqClass, SyndicateProfessionRequirement.CLASS_TAOIST);
                }

                Save();
            }
        }

        #endregion

        #region Member Management

        public bool AppendMember(Character pTarget)
        {
            if (pTarget.Syndicate != null)
                return false;

            if (pTarget.NobilityRank == NobilityLevel.KING && HasKingMember())
            {
                pTarget.SendMessage(Language.StrSynAlreadyHasKing, Color.White, ChatTone.Talk);
                return false;
            }

            switch ((int) Math.Floor((int) pTarget.Profession / 10f))
            {
                case 1:
                    if (!TrojanEnabled)
                        return false;
                    break;
                case 2:
                    if (!WarriorEnabled)
                        return false;
                    break;
                case 4:
                    if (!ArcherEnabled)
                        return false;
                    break;
                case 5:
                    if (!NinjaEnabled)
                        return false;
                    break;
                case 6:
                    if (!MonkEnabled)
                        return false;
                    break;
                case 7:
                    if (!PirateEnabled)
                        return false;
                    break;
                case 10:
                case 13:
                case 14:
                    if (!TaoistEnabled)
                        return false;
                    break;
                default:
                    return false;
            }

            if (pTarget.Level < LevelRequirement)
                return false;

            if (pTarget.Metempsychosis < MetempsychosisRequirement)
                return false;

            var newMember = new SyndicateMember(this);
            if (!newMember.Create(pTarget))
            {
                newMember.Delete();
                return false;
            }

            if (!Members.TryAdd(newMember.Identity, newMember) || !ServerKernel.SyndicateManager.AddSyndicateMember(newMember))
            {
                Members.TryRemove(pTarget.Identity, out _);
                newMember.Delete();
                return false;
            }

            MemberCount = (ushort) Members.Count;

            pTarget.SyndicateIdentity = Identity;
            pTarget.SyndicatePosition = newMember.Position;
            pTarget.Syndicate = this;
            pTarget.SyndicateMember = newMember;

            CheckLeaderSpouse();

            newMember.SendSyndicate();
            pTarget.Screen.RefreshSpawnForObservers();
            SendName(pTarget, true);

            Send(string.Format(Language.StrSynRecruitJoined, pTarget.Name));
            return true;
        }

        public bool AppendMember(Character pSender, uint dwTarget, bool bInvite)
        {
            Character pTarget = ServerKernel.UserManager.GetUser(dwTarget);
            if (pTarget == null)
            {
                pSender.SendSysMessage(Language.StrTargetNotOnline, Color.Red);
                return false;
            }

            if (pTarget.SyndicateMember != null)
            {
                pSender.SendSysMessage(Language.StrSynTargetAlreadyInGuild, Color.Red);
                return false;
            }

            if (pTarget.NobilityRank == NobilityLevel.KING && HasKingMember())
            {
                pSender.SendMessage(Language.StrSynAlreadyHasKing, Color.White, ChatTone.Talk);
                return false;
            }

            var pInvite = pSender.SyndicateMember;
            switch (pInvite.Position)
            {
                case SyndicateRank.GuildLeader:
                case SyndicateRank.DeputyLeader:
                case SyndicateRank.HonoraryDeputyLeader:
                case SyndicateRank.LeaderSpouse:
                case SyndicateRank.Manager:
                case SyndicateRank.HonoraryManager:
                    break;
                default:
                    return false;
            }

            switch ((int) Math.Floor((int) pTarget.Profession / 10f))
            {
                case 1:
                    if (!TrojanEnabled)
                        return false;
                    break;
                case 2:
                    if (!WarriorEnabled)
                        return false;
                    break;
                case 4:
                    if (!ArcherEnabled)
                        return false;
                    break;
                case 5:
                    if (!NinjaEnabled)
                        return false;
                    break;
                case 6:
                    if (!MonkEnabled)
                        return false;
                    break;
                case 7:
                    if (!PirateEnabled)
                        return false;
                    break;
                case 10:
                case 13:
                case 14:
                    if (!TaoistEnabled)
                        return false;
                    break;
            }

            if (pTarget.Level < LevelRequirement)
                return false;

            if (pTarget.Metempsychosis < MetempsychosisRequirement)
                return false;

            var newMember = new SyndicateMember(this);
            if (!newMember.Create(pTarget))
            {
#if DEBUG
                pSender.SendSysMessage("An error ocurred. Please, try again. Syn::0001", Color.Red);
#endif
                newMember.Delete();
                return false;
            }

            Send(!bInvite
                ? string.Format(Language.StrSynJoinGuild, pInvite.GetRankName(), pInvite.Name, pTarget.Name)
                : string.Format(Language.StrSynInviteGuild, pInvite.GetRankName(), pInvite.Name, pTarget.Name));

            if (!Members.TryAdd(newMember.Identity, newMember) || !ServerKernel.SyndicateManager.AddSyndicateMember(newMember))
            {
#if DEBUG
                pSender.SendSysMessage("An error ocurred. Please, try again. Syn::0002", Color.Red);
#endif
                newMember.Delete();
                return false;
            }

            MemberCount = (ushort) Members.Count;
            pTarget.SyndicateIdentity = Identity;
            pTarget.SyndicatePosition = newMember.Position;
            pTarget.Syndicate = this;
            pTarget.SyndicateMember = newMember;

            CheckLeaderSpouse();

            newMember.SendSyndicate();
            pTarget.Screen.RefreshSpawnForObservers();
            SendName(pTarget, true);
            return true;
        }

        public void QuitSyndicate(Character pSender, bool free = false)
        {
            SyndicateMember pUser;
            if (!Members.TryGetValue(pSender.Identity, out pUser))
                return;

            if (pUser.Position == SyndicateRank.GuildLeader)
                return;

            if (pUser.SilverDonation < 2 && !free)
            {
                pSender.SendSysMessage("You need to donate at least 20,000 silvers to the guild.");
                return;
            }

            switch (pUser.Position)
            {
                case SyndicateRank.DeputyLeader:
                    DeputyLeaderCount -= 1;
                    break;
                case SyndicateRank.HonoraryDeputyLeader:
                    HonoraryDeputyLeaderCount -= 1;
                    break;
                case SyndicateRank.HonoraryManager:
                    HonoraryManagerCount -= 1;
                    break;
                case SyndicateRank.HonorarySupervisor:
                    HonorarySupervisorCount -= 1;
                    break;
                case SyndicateRank.HonorarySteward:
                    HonoraryStewardCount -= 1;
                    break;
                case SyndicateRank.Aide:
                    AideCount -= 1;
                    break;
                case SyndicateRank.LeaderSpouseAide:
                    LeaderSpouseAideCount -= 1;
                    break;
                case SyndicateRank.DeputyLeaderAide:
                    DeputyLeaderAideCount -= 1;
                    break;
                case SyndicateRank.ManagerAide:
                    ManagerAideCount -= 1;
                    break;
                case SyndicateRank.SupervisorAide:
                    SupervisorAideCount -= 1;
                    break;
            }

            Arsenal.RemoveAllFromUser(pUser.Identity);
            Members.TryRemove(pUser.Identity, out _);
            ServerKernel.SyndicateManager.RemoveSyndicateMember(pUser.Identity);
            pUser.User.SyndicateIdentity = 0;
            pUser.User.SyndicatePosition = SyndicateRank.None;
            pUser.Delete();
            pUser.Send(new MsgSyndicate {Action = SyndicateRequest.SYN_DISBAND, Param = Identity});
            pUser.User.Screen.RefreshSpawnForObservers();
            MemberCount = (ushort) Members.Values.Count;
            Save();

            ServerKernel.CaptureTheFlag.RemoveMember(pUser.Identity);

            ServerKernel.Log.GmLog("Syndicate", $"{pSender.Name} has left the syndicate {Name} [{Identity}]");
            pUser.User.Syndicate = null;
            pUser.User.SyndicateMember = null;
        }

        public void ExpelMember(Character pSender, string szName, bool bKick)
        {
            var pTarget = Members.Values.FirstOrDefault(x => x.Name == szName);
            if (pTarget == null)
            {
                pSender.SendSysMessage(Language.StrTargetNotInRange, Color.Red);
                return;
            }

            if (pSender != null)
            {
                if (pSender.SyndicatePosition < SyndicateRank.LeaderSpouse)
                    return;

                SyndicateRank oldRank = pTarget.Position;
                if (pSender.SyndicatePosition == SyndicateRank.DeputyLeader
                    || pSender.SyndicatePosition == SyndicateRank.LeaderSpouse
                    || pSender.SyndicatePosition == SyndicateRank.HonoraryDeputyLeader)
                    if (pTarget.Position >= SyndicateRank.Supervisor)
                        return; // if DL or Spouse cannot kick position higher than superv
            }

            switch (pTarget.Position)
            {
                case SyndicateRank.GuildLeader:
                    return; // the target is leader?
                case SyndicateRank.DeputyLeader:
                    DeputyLeaderCount -= 1;
                    break;
                case SyndicateRank.HonoraryDeputyLeader:
                    HonoraryDeputyLeaderCount -= 1;
                    break;
                case SyndicateRank.HonoraryManager:
                    HonoraryManagerCount -= 1;
                    break;
                case SyndicateRank.HonorarySupervisor:
                    HonorarySupervisorCount -= 1;
                    break;
                case SyndicateRank.HonorarySteward:
                    HonoraryStewardCount -= 1;
                    break;
                case SyndicateRank.Aide:
                    AideCount -= 1;
                    break;
                case SyndicateRank.LeaderSpouseAide:
                    LeaderSpouseAideCount -= 1;
                    break;
                case SyndicateRank.DeputyLeaderAide:
                    DeputyLeaderAideCount -= 1;
                    break;
                case SyndicateRank.ManagerAide:
                    ManagerAideCount -= 1;
                    break;
                case SyndicateRank.SupervisorAide:
                    SupervisorAideCount -= 1;
                    break;
            }

            if (bKick && pSender != null)
                Send(string.Format(Language.StrKickoutGuild, pSender.SyndicateMember.GetRankName(), pSender.Name,
                    pTarget.Name));

            Arsenal.RemoveAllFromUser(pTarget.Identity);

            bool bKernel = Members.TryRemove(pTarget.Identity, out _);
            if (pTarget.IsOnline)
            {
                pTarget.User.SyndicateIdentity = 0;
                pTarget.User.SyndicatePosition = SyndicateRank.None;
                pTarget.User.Syndicate = null;
                pTarget.Delete();
                pTarget.User.SyndicateMember = null;

                pTarget.Send(new MsgSyndicate {Action = SyndicateRequest.SYN_QUIT, Param = Identity});
                pTarget.User.Screen.RefreshSpawnForObservers();
            }
            else
            {
                pTarget.Delete();
            }

            ServerKernel.SyndicateManager.RemoveSyndicateMember(pTarget.Identity);
            SendMembers(pSender, 0);
            MemberCount = (ushort) Members.Values.Count;
            Save();

            ServerKernel.CaptureTheFlag.RemoveMember(pTarget.Identity);
            ServerKernel.Log.GmLog("Syndicate", $"{pSender?.Name ?? "[SYSTEM]"} has kicked {pTarget.Name} from the syndicate {Name} [{Identity}]");
        }

        public void DisbandSyndicate(Character pSender)
        {
            if (pSender == null ||
                pSender.Syndicate == null ||
                pSender.SyndicateMember == null)
                return; // how the fuck is this possible? asshole

            if (pSender.SyndicatePosition != SyndicateRank.GuildLeader)
            {
                pSender.SendSysMessage(Language.StrNoDisbandLeader, Color.Red);
                return;
            }

            if (Members.Count > 1)
            {
                pSender.SendSysMessage(Language.StrNoDisband, Color.Red);
                return;
            }

            pSender.SyndicateIdentity = 0;
            pSender.SyndicatePosition = SyndicateRank.None;
            pSender.Syndicate = null;
            pSender.SyndicateMember.Delete();
            pSender.SyndicateMember = null;
            pSender.Screen.RefreshSpawnForObservers();
            pSender.Send(new MsgSyndicate {Action = SyndicateRequest.SYN_DISBAND, Param = Identity});
            ServerKernel.SyndicateManager.RemoveSyndicateMember(pSender.Identity);

            Delete();
            MemberCount = 0;
            Save();

            // clear allies
            foreach (var ally in Allies.Values)
            {
                Syndicate trash;
                ally.Allies.TryRemove(Identity, out trash);
                ally.Send(new MsgSyndicate
                {
                    Action = SyndicateRequest.SYN_NEUTRAL1,
                    Param = Identity
                });
            }

            // clear enemies
            foreach (var enemy in Enemies.Values)
            {
                Syndicate trash;
                enemy.Enemies.TryRemove(Identity, out trash);
                enemy.Send(new MsgSyndicate
                {
                    Action = SyndicateRequest.SYN_NEUTRAL2,
                    Param = Identity
                });
            }
            //new SyndicateAllyRepository().ClearAlliesAndEnemies((ushort)Identity);
        }

        public void ProcessPaidPromotion(MsgSyndicate msg, Character sender)
        {
            if (sender == null || sender.SyndicatePosition != SyndicateRank.GuildLeader)
                return;

            var target = Members.Values.FirstOrDefault(x => x.Name == msg.Name);

            if (target?.User == null)
                return; // ?

            switch ((SyndicateRank) msg.Param)
            {
                case SyndicateRank.HonoraryDeputyLeader:
                    if (HonoraryDeputyLeaderCount >= MaxHonoraryDeputyLeader)
                        return;
                    if (!ChangeEmoneyFunds(HONORARY_DEPUTY_LEADER_PRICE))
                    {
                        sender.SendSysMessage(Language.StrNotEnoughFunds, Color.Red);
                        return;
                    }

                    break;
                case SyndicateRank.HonoraryManager:
                    if (HonoraryManagerCount >= MaxHonoraryManager)
                        return;
                    if (!ChangeEmoneyFunds(HONORARY_MANAGER_PRICE))
                    {
                        sender.SendSysMessage(Language.StrNotEnoughFunds, Color.Red);
                        return;
                    }

                    break;
                case SyndicateRank.HonorarySupervisor:
                    if (HonorarySupervisorCount >= MaxHonorarySupervisor)
                        return;
                    if (!ChangeEmoneyFunds(HONORARY_SUPERVISOR_PRICE))
                    {
                        sender.SendSysMessage(Language.StrNotEnoughFunds, Color.Red);
                        return;
                    }

                    break;
                case SyndicateRank.HonorarySteward:
                    if (HonoraryStewardCount >= MaxHonorarySteward)
                        return;
                    if (!ChangeEmoneyFunds(HONORARY_STEWARD_PRICE))
                    {
                        sender.SendSysMessage(Language.StrNotEnoughFunds, Color.Red);
                        return;
                    }

                    break;
                default:
                    return;
            }

            PromoteMember(sender, target, (SyndicateRank) msg.Param);
        }

        public bool PromoteMember(Character sender, SyndicateMember target, SyndicateRank pos)
        {
            if (target.Position == SyndicateRank.GuildLeader)
            {
                return false;
            }

            // for guild leader or discharge use Abdicate or Discharge
            switch (pos)
            {
                case SyndicateRank.GuildLeader:
                {
                    return AbdicateSyndicate(sender.SyndicateMember, target);
                }

                case SyndicateRank.DeputyLeader:
                    if (DeputyLeaderCount >= MaxDeputyLeader)
                        return false; // limit exceed
                    if (sender.SyndicatePosition != SyndicateRank.GuildLeader)
                        return false;
                    break;
                case SyndicateRank.HonoraryDeputyLeader:
                    if (HonoraryDeputyLeaderCount >= MaxHonoraryDeputyLeader)
                        return false; // limit exceed
                    if (sender.SyndicatePosition != SyndicateRank.GuildLeader)
                        return false;
                    break;
                case SyndicateRank.DeputyLeaderAide:
                    if (DeputyLeaderAideCount >= 1)
                        return false; // limit exceed
                    if (sender.SyndicatePosition != SyndicateRank.DeputyLeader)
                        return false;
                    break;
                case SyndicateRank.HonoraryManager:
                    if (HonoraryManagerCount >= MaxHonoraryManager)
                        return false; // limit exceed
                    if (sender.SyndicatePosition != SyndicateRank.GuildLeader)
                        return false;
                    break;
                case SyndicateRank.ManagerAide:
                    if (ManagerAideCount >= 1)
                        return false; // limit exceed
                    if (sender.SyndicatePosition != SyndicateRank.Manager)
                        return false;
                    break;
                case SyndicateRank.HonorarySupervisor:
                    if (HonorarySupervisorCount >= MaxHonorarySupervisor)
                        return false; // limit exceed
                    if (sender.SyndicatePosition != SyndicateRank.GuildLeader)
                        return false;
                    break;
                case SyndicateRank.SupervisorAide:
                    if (SupervisorAideCount >= 1)
                        return false; // limit exceed
                    if (sender.SyndicatePosition != SyndicateRank.Supervisor
                        || sender.SyndicatePosition != SyndicateRank.ArsenalSupervisor
                        || sender.SyndicatePosition != SyndicateRank.CpSupervisor
                        || sender.SyndicatePosition != SyndicateRank.SilverSupervisor
                        || sender.SyndicatePosition != SyndicateRank.GuideSupervisor
                        || sender.SyndicatePosition != SyndicateRank.HonorarySupervisor
                        || sender.SyndicatePosition != SyndicateRank.LilySupervisor
                        || sender.SyndicatePosition != SyndicateRank.OrchidSupervisor
                        || sender.SyndicatePosition != SyndicateRank.RoseSupervisor
                        || sender.SyndicatePosition != SyndicateRank.TulipSupervisor
                        || sender.SyndicatePosition != SyndicateRank.PkSupervisor)
                        return false;
                    break;
                case SyndicateRank.HonorarySteward:
                    if (HonoraryStewardCount >= MaxHonorarySteward)
                        return false; // limit exceed
                    if (sender.SyndicatePosition != SyndicateRank.GuildLeader)
                        return false;
                    break;
                case SyndicateRank.Aide:
                    if (AideCount >= MaxAide)
                        return false; // limit exceed
                    if (sender.SyndicatePosition != SyndicateRank.GuildLeader)
                        return false;
                    break;
                default:
                    return false; // change not enabled
            }

            // gotta change the amount
            switch (target.Position)
            {
                case SyndicateRank.GuildLeader:
                    return false; // you cannot promote the guild leader -.-
                case SyndicateRank.DeputyLeader:
                    DeputyLeaderCount -= 1;
                    break;
                case SyndicateRank.HonoraryDeputyLeader:
                    HonoraryDeputyLeaderCount -= 1;
                    break;
                case SyndicateRank.HonoraryManager:
                    HonoraryManagerCount -= 1;
                    break;
                case SyndicateRank.HonorarySupervisor:
                    HonorarySupervisorCount -= 1;
                    break;
                case SyndicateRank.HonorarySteward:
                    HonoraryStewardCount -= 1;
                    break;
                case SyndicateRank.Aide:
                    AideCount -= 1;
                    break;
                case SyndicateRank.LeaderSpouse:
                    LeaderSpouseAideCount -= 1;
                    break;
                case SyndicateRank.DeputyLeaderAide:
                    DeputyLeaderAideCount -= 1;
                    break;
                case SyndicateRank.ManagerAide:
                    ManagerAideCount -= 1;
                    break;
                case SyndicateRank.SupervisorAide:
                    SupervisorAideCount -= 1;
                    break;
            }

            SyndicateRank oldRank = target.Position;

            target.Position = pos;
            target.SendSyndicate();
            target.User.Screen.RefreshSpawnForObservers();

            if (pos == SyndicateRank.HonoraryDeputyLeader || pos == SyndicateRank.HonoraryManager ||
                pos == SyndicateRank.HonorarySteward || pos == SyndicateRank.HonorarySupervisor)
                target.PositionExpire = (uint) UnixTimestamp.Now() + UnixTimestamp.TIME_SECONDS_DAY * 30;
            else
                target.PositionExpire = 0;

            Send(string.Format(Language.StrSyndicatePromote, sender.SyndicateMember.GetRankName(), sender.Name,
                target.Name, target.GetRankName()));

            target.Save();

            switch (target.Position)
            {
                case SyndicateRank.DeputyLeader:
                    DeputyLeaderCount += 1;
                    break;
                case SyndicateRank.HonoraryDeputyLeader:
                    HonoraryDeputyLeaderCount += 1;
                    break;
                case SyndicateRank.HonoraryManager:
                    HonoraryManagerCount += 1;
                    break;
                case SyndicateRank.HonorarySupervisor:
                    HonorarySupervisorCount += 1;
                    break;
                case SyndicateRank.HonorarySteward:
                    HonoraryStewardCount += 1;
                    break;
                case SyndicateRank.Aide:
                    AideCount += 1;
                    break;
                case SyndicateRank.LeaderSpouse:
                    LeaderSpouseAideCount += 1;
                    break;
                case SyndicateRank.DeputyLeaderAide:
                    DeputyLeaderAideCount += 1;
                    break;
                case SyndicateRank.ManagerAide:
                    ManagerAideCount += 1;
                    break;
                case SyndicateRank.SupervisorAide:
                    SupervisorAideCount += 1;
                    break;
            }

            Arsenal.UpdatePoles();
            target.User.UpdateClient(ClientUpdateType.GuildBattlepower,
                Arsenal.SharedBattlePower(target.Position, target.User));
            return true;
        }

        public bool DischargeMember(User sender, string strName)
        {
            var target = Members.Values.FirstOrDefault(x => x.Name == strName);

            if (sender == null
                || target == null
                || sender.Character == null)
                return false; // ?

            return DischargeMember(sender.Character, target);
        }

        public bool DischargeMember(Character sender, SyndicateMember target)
        {
            // gotta change the amount
            switch (target.Position)
            {
                case SyndicateRank.GuildLeader:
                    return false; // you cannot demote the guild leader -.-
                case SyndicateRank.DeputyLeader:
                    DeputyLeaderCount -= 1;
                    break;
                case SyndicateRank.HonoraryDeputyLeader:
                    HonoraryDeputyLeaderCount -= 1;
                    break;
                case SyndicateRank.HonoraryManager:
                    HonoraryManagerCount -= 1;
                    break;
                case SyndicateRank.HonorarySupervisor:
                    HonorarySupervisorCount -= 1;
                    break;
                case SyndicateRank.HonorarySteward:
                    HonoraryStewardCount -= 1;
                    break;
                case SyndicateRank.Aide:
                    AideCount -= 1;
                    break;
                case SyndicateRank.LeaderSpouse:
                    LeaderSpouseAideCount -= 1;
                    break;
                case SyndicateRank.DeputyLeaderAide:
                    DeputyLeaderAideCount -= 1;
                    break;
                case SyndicateRank.ManagerAide:
                    ManagerAideCount -= 1;
                    break;
                case SyndicateRank.SupervisorAide:
                    SupervisorAideCount -= 1;
                    break;
            }

            var oldPos = target.GetRankName();
            target.Position = SyndicateRank.Member;
            target.PositionExpire = 0;

            CheckLeaderSpouse();

            if (target.IsOnline)
            {
                target.User.Screen.RefreshSpawnForObservers();
                target.SendSyndicate();
            }

            target.Save();

            Send(string.Format(Language.StrSyndicateDischarge, sender.SyndicateMember.GetRankName(), sender.Name,
                target.Name, oldPos));
            SendMembers(sender, 0);
            return true;
        }

        public void CheckLeaderSpouse()
        {
            if (IsDeleted)
                return;

            CharacterEntity pLeader = new CharacterRepository().SearchByIdentity(LeaderIdentity);
            if (pLeader == null || pLeader.Mate.ToLower() == Language.StrNone.ToLower())
                return;

            CharacterEntity pSpouse = new CharacterRepository().SearchByName(pLeader.Mate);
            if (pSpouse == null)
                return;

            if (!Members.TryGetValue(pSpouse.Identity, out SyndicateMember spouse))
                return;

            if (SyndicateMember.IsUserSetPosition(spouse.Position))
                return;

            spouse.Position = SyndicateRank.LeaderSpouse;
            spouse.Save();

            if (spouse.IsOnline)
            {
                spouse.User.Screen.RefreshSpawnForObservers();
                spouse.SendSyndicate();
            }
        }

        public bool IsSpousePosition(SyndicateRank pos)
        {
            switch (pos)
            {
                case SyndicateRank.LeaderSpouse:
                case SyndicateRank.DeputyLeaderSpouse:
                case SyndicateRank.ManagerSpouse:
                case SyndicateRank.SupervisorSpouse:
                    return true;
            }

            return false;
        }

        public bool HasSpousePosition(SyndicateRank pos)
        {
            switch (pos)
            {
                case SyndicateRank.GuildLeader:
                case SyndicateRank.DeputyLeader:
                case SyndicateRank.HonoraryDeputyLeader:
                case SyndicateRank.Manager:
                case SyndicateRank.HonoraryManager:
                case SyndicateRank.Supervisor:
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Used to compare when switching positions automatically.
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <returns>Returns true if p1 is higher than p2.</returns>
        private bool IsHigherPosition(SyndicateRank p1, SyndicateRank p2)
        {
            if (SyndicateMember.IsUserSetPosition(p2))
                return false;
            return p1 > p2;
        }

        public bool AbdicateSyndicate(SyndicateMember sender, string strName)
        {
            var target = Members.Values.FirstOrDefault(x => x.Name == strName);

            if (sender == null || target?.User == null || sender.Position != SyndicateRank.GuildLeader)
                return false; // ?

            return AbdicateSyndicate(sender, target);
        }

        public bool AbdicateSyndicate(SyndicateMember sender, SyndicateMember target)
        {
            try
            {
                if (sender == null || target == null || sender == target || sender.Position != SyndicateRank.GuildLeader)
                    return false;

                // gotta change the amount
                switch (target.Position)
                {
                    case SyndicateRank.GuildLeader:
                        return false; // the target is already leader?
                    case SyndicateRank.DeputyLeader:
                        DeputyLeaderCount -= 1;
                        break;
                    case SyndicateRank.HonoraryDeputyLeader:
                        HonoraryDeputyLeaderCount -= 1;
                        break;
                    case SyndicateRank.HonoraryManager:
                        HonoraryManagerCount -= 1;
                        break;
                    case SyndicateRank.HonorarySupervisor:
                        HonorarySupervisorCount -= 1;
                        break;
                    case SyndicateRank.HonorarySteward:
                        HonoraryStewardCount -= 1;
                        break;
                    case SyndicateRank.Aide:
                        AideCount -= 1;
                        break;
                    case SyndicateRank.LeaderSpouseAide:
                        LeaderSpouseAideCount -= 1;
                        break;
                    case SyndicateRank.DeputyLeaderAide:
                        DeputyLeaderAideCount -= 1;
                        break;
                    case SyndicateRank.ManagerAide:
                        ManagerAideCount -= 1;
                        break;
                    case SyndicateRank.SupervisorAide:
                        SupervisorAideCount -= 1;
                        break;
                }

                LeaderIdentity = target.Identity;
                LeaderName = target.Name;

                target.Position = SyndicateRank.GuildLeader;
                target.PositionExpire = 0;
                target.User.Screen.RefreshSpawnForObservers();
                target.SendSyndicate();

                sender.Position = SyndicateRank.Member;
                sender.PositionExpire = 0;
                sender.User.Screen.RefreshSpawnForObservers();
                sender.SendSyndicate();

                CheckLeaderSpouse();

                Send(string.Format(Language.StrSyndicateAbdicate, sender.Name, target.Name));
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        public bool HasKingMember()
        {
            return Members.Values.Any(member => (NobilityLevel) member.Nobility == NobilityLevel.KING);
        }

        public int KingCount() => Members.Values.Count(x => (NobilityLevel) x.Nobility == NobilityLevel.KING);

        public void KickKingMember(bool sysNotify = false)
        {
            SyndicateMember member = Members.Values.FirstOrDefault(x => (NobilityLevel) x.Nobility == NobilityLevel.KING && x.Position != SyndicateRank.GuildLeader);
            if (member != null)
            {
                if (member.User != null)
                {
                    QuitSyndicate(member.User, true);
                }
                else
                {
                    ExpelMember(null, member.Name, true);
                }
                member.User?.SendMessage(Language.StrSynLeaveByKing, Color.White, ChatTone.Talk);

                if (sysNotify)
                    Program.WriteLog($"King {member.Name} has been kicked from {member.Syndicate.Name}", LogType.DEBUG);
            }
        }

        #endregion

        #region Position Count

        public int DeputyLeaderCount,
            HonoraryDeputyLeaderCount,
            DeputyLeaderAideCount,
            HonoraryManagerCount,
            HonorarySupervisorCount,
            AideCount,
            ManagerAideCount,
            SupervisorAideCount,
            LeaderSpouseAideCount,
            HonoraryStewardCount;

        public ushort MaxAllies()
        {
            return MaxAllies(Level);
        }

        public ushort MaxAllies(ushort level)
        {
            switch (level)
            {
                case 1: return 5;
                case 2: return 7;
                case 3: return 9;
                case 4: return 12;
                default: return 15;
            }
        }

        public ushort MaxEnemies()
        {
            return MaxEnemies(Level);
        }

        public ushort MaxEnemies(ushort level)
        {
            switch (level)
            {
                case 1: return 5;
                case 2: return 7;
                case 3: return 9;
                case 4: return 12;
                default: return 15;
            }
        }

        public uint MaxPositionAmount(SyndicateRank pos)
        {
            switch (Level)
            {
                #region Level 1

                case 1:
                {
                    switch (pos)
                    {
                        case SyndicateRank.Manager:
                        case SyndicateRank.ManagerAide:
                            return 1;
                        case SyndicateRank.Supervisor:
                        case SyndicateRank.ArsenalSupervisor:
                        case SyndicateRank.CpSupervisor:
                        case SyndicateRank.GuideSupervisor:
                        case SyndicateRank.LilySupervisor:
                        case SyndicateRank.OrchidSupervisor:
                        case SyndicateRank.PkSupervisor:
                        case SyndicateRank.SilverSupervisor:
                        case SyndicateRank.TulipSupervisor:
                        case SyndicateRank.Steward:
                            return 0;
                        case SyndicateRank.Agent:
                        case SyndicateRank.ArsenalAgent:
                        case SyndicateRank.CpAgent:
                        case SyndicateRank.GuideAgent:
                        case SyndicateRank.LilyAgent:
                        case SyndicateRank.OrchidAgent:
                        case SyndicateRank.PkAgent:
                        case SyndicateRank.SilverAgent:
                        case SyndicateRank.TulipAgent:
                        case SyndicateRank.Follower:
                        case SyndicateRank.ArsenalFollower:
                        case SyndicateRank.CpFollower:
                        case SyndicateRank.GuideFollower:
                        case SyndicateRank.LilyFollower:
                        case SyndicateRank.OrchidFollower:
                        case SyndicateRank.PkFollower:
                        case SyndicateRank.SilverFollower:
                        case SyndicateRank.TulipFollower:
                            return 1;
                        default:
                            return 0;
                    }
                }

                #endregion

                #region Level 2

                case 2:
                {
                    switch (pos)
                    {
                        case SyndicateRank.Manager:
                        case SyndicateRank.ManagerAide:
                            return 1;
                        case SyndicateRank.Supervisor:
                        case SyndicateRank.ArsenalSupervisor:
                        case SyndicateRank.CpSupervisor:
                        case SyndicateRank.GuideSupervisor:
                        case SyndicateRank.LilySupervisor:
                        case SyndicateRank.OrchidSupervisor:
                        case SyndicateRank.PkSupervisor:
                        case SyndicateRank.SilverSupervisor:
                        case SyndicateRank.TulipSupervisor:
                            return 0;
                        case SyndicateRank.Steward:
                            return 1;
                        case SyndicateRank.Agent:
                        case SyndicateRank.ArsenalAgent:
                        case SyndicateRank.CpAgent:
                        case SyndicateRank.GuideAgent:
                        case SyndicateRank.LilyAgent:
                        case SyndicateRank.OrchidAgent:
                        case SyndicateRank.PkAgent:
                        case SyndicateRank.SilverAgent:
                        case SyndicateRank.TulipAgent:
                        case SyndicateRank.Follower:
                        case SyndicateRank.ArsenalFollower:
                        case SyndicateRank.CpFollower:
                        case SyndicateRank.GuideFollower:
                        case SyndicateRank.LilyFollower:
                        case SyndicateRank.OrchidFollower:
                        case SyndicateRank.PkFollower:
                        case SyndicateRank.SilverFollower:
                        case SyndicateRank.TulipFollower:
                            return 1;
                        default:
                            return 0;
                    }
                }

                #endregion

                #region Level 3

                case 3:
                {
                    switch (pos)
                    {
                        case SyndicateRank.Manager:
                        case SyndicateRank.ManagerAide:
                            return 2;
                        case SyndicateRank.Supervisor:
                        case SyndicateRank.ArsenalSupervisor:
                        case SyndicateRank.CpSupervisor:
                        case SyndicateRank.GuideSupervisor:
                        case SyndicateRank.LilySupervisor:
                        case SyndicateRank.OrchidSupervisor:
                        case SyndicateRank.PkSupervisor:
                        case SyndicateRank.SilverSupervisor:
                        case SyndicateRank.TulipSupervisor:
                            return 0;
                        case SyndicateRank.Steward:
                            return 2;
                        case SyndicateRank.Agent:
                        case SyndicateRank.ArsenalAgent:
                        case SyndicateRank.CpAgent:
                        case SyndicateRank.GuideAgent:
                        case SyndicateRank.LilyAgent:
                        case SyndicateRank.OrchidAgent:
                        case SyndicateRank.PkAgent:
                        case SyndicateRank.SilverAgent:
                        case SyndicateRank.TulipAgent:
                        case SyndicateRank.Follower:
                        case SyndicateRank.ArsenalFollower:
                        case SyndicateRank.CpFollower:
                        case SyndicateRank.GuideFollower:
                        case SyndicateRank.LilyFollower:
                        case SyndicateRank.OrchidFollower:
                        case SyndicateRank.PkFollower:
                        case SyndicateRank.SilverFollower:
                        case SyndicateRank.TulipFollower:
                            return 1;
                        default:
                            return 0;
                    }
                }

                #endregion

                #region Level 4

                case 4:
                {
                    switch (pos)
                    {
                        case SyndicateRank.Manager:
                        case SyndicateRank.ManagerAide:
                            return 2;
                        case SyndicateRank.Supervisor:
                        case SyndicateRank.ArsenalSupervisor:
                        case SyndicateRank.CpSupervisor:
                        case SyndicateRank.GuideSupervisor:
                        case SyndicateRank.LilySupervisor:
                        case SyndicateRank.OrchidSupervisor:
                        case SyndicateRank.PkSupervisor:
                        case SyndicateRank.SilverSupervisor:
                        case SyndicateRank.TulipSupervisor:
                            return 1;
                        case SyndicateRank.Steward:
                            return 3;
                        case SyndicateRank.Agent:
                        case SyndicateRank.ArsenalAgent:
                        case SyndicateRank.CpAgent:
                        case SyndicateRank.GuideAgent:
                        case SyndicateRank.LilyAgent:
                        case SyndicateRank.OrchidAgent:
                        case SyndicateRank.PkAgent:
                        case SyndicateRank.SilverAgent:
                        case SyndicateRank.TulipAgent:
                        case SyndicateRank.Follower:
                        case SyndicateRank.ArsenalFollower:
                        case SyndicateRank.CpFollower:
                        case SyndicateRank.GuideFollower:
                        case SyndicateRank.LilyFollower:
                        case SyndicateRank.OrchidFollower:
                        case SyndicateRank.PkFollower:
                        case SyndicateRank.SilverFollower:
                        case SyndicateRank.TulipFollower:
                            return 1;
                        default:
                            return 0;
                    }
                }

                #endregion

                #region Level 5

                case 5:
                {
                    switch (pos)
                    {
                        case SyndicateRank.Manager:
                        case SyndicateRank.ManagerAide:
                            return 4;
                        case SyndicateRank.Supervisor:
                        case SyndicateRank.ArsenalSupervisor:
                        case SyndicateRank.CpSupervisor:
                        case SyndicateRank.GuideSupervisor:
                        case SyndicateRank.LilySupervisor:
                        case SyndicateRank.OrchidSupervisor:
                        case SyndicateRank.PkSupervisor:
                        case SyndicateRank.SilverSupervisor:
                        case SyndicateRank.TulipSupervisor:
                            return 1;
                        case SyndicateRank.Steward:
                            return 4;
                        case SyndicateRank.Agent:
                        case SyndicateRank.ArsenalAgent:
                        case SyndicateRank.CpAgent:
                        case SyndicateRank.GuideAgent:
                        case SyndicateRank.LilyAgent:
                        case SyndicateRank.OrchidAgent:
                        case SyndicateRank.PkAgent:
                        case SyndicateRank.SilverAgent:
                        case SyndicateRank.TulipAgent:
                        case SyndicateRank.Follower:
                        case SyndicateRank.ArsenalFollower:
                        case SyndicateRank.CpFollower:
                        case SyndicateRank.GuideFollower:
                        case SyndicateRank.LilyFollower:
                        case SyndicateRank.OrchidFollower:
                        case SyndicateRank.PkFollower:
                        case SyndicateRank.SilverFollower:
                        case SyndicateRank.TulipFollower:
                            return 1;
                        default:
                            return 0;
                    }
                }

                #endregion

                #region Level 6

                case 6:
                {
                    switch (pos)
                    {
                        case SyndicateRank.Manager:
                        case SyndicateRank.ManagerAide:
                            return 4;
                        case SyndicateRank.Supervisor:
                        case SyndicateRank.ArsenalSupervisor:
                        case SyndicateRank.CpSupervisor:
                        case SyndicateRank.GuideSupervisor:
                        case SyndicateRank.LilySupervisor:
                        case SyndicateRank.OrchidSupervisor:
                        case SyndicateRank.PkSupervisor:
                        case SyndicateRank.SilverSupervisor:
                        case SyndicateRank.TulipSupervisor:
                            return 1;
                        case SyndicateRank.Steward:
                            return 5;
                        case SyndicateRank.Agent:
                        case SyndicateRank.ArsenalAgent:
                        case SyndicateRank.CpAgent:
                        case SyndicateRank.GuideAgent:
                        case SyndicateRank.LilyAgent:
                        case SyndicateRank.OrchidAgent:
                        case SyndicateRank.PkAgent:
                        case SyndicateRank.SilverAgent:
                        case SyndicateRank.TulipAgent:
                        case SyndicateRank.Follower:
                        case SyndicateRank.ArsenalFollower:
                        case SyndicateRank.CpFollower:
                        case SyndicateRank.GuideFollower:
                        case SyndicateRank.LilyFollower:
                        case SyndicateRank.OrchidFollower:
                        case SyndicateRank.PkFollower:
                        case SyndicateRank.SilverFollower:
                        case SyndicateRank.TulipFollower:
                            return 1;
                        default:
                            return 0;
                    }
                }

                #endregion

                #region Level 7

                case 7:
                {
                    switch (pos)
                    {
                        case SyndicateRank.Manager:
                        case SyndicateRank.ManagerAide:
                            return 6;
                        case SyndicateRank.Supervisor:
                        case SyndicateRank.ArsenalSupervisor:
                        case SyndicateRank.CpSupervisor:
                        case SyndicateRank.GuideSupervisor:
                        case SyndicateRank.LilySupervisor:
                        case SyndicateRank.OrchidSupervisor:
                        case SyndicateRank.PkSupervisor:
                        case SyndicateRank.SilverSupervisor:
                        case SyndicateRank.TulipSupervisor:
                            return 1;
                        case SyndicateRank.Steward:
                            return 6;
                        case SyndicateRank.Agent:
                        case SyndicateRank.ArsenalAgent:
                        case SyndicateRank.CpAgent:
                        case SyndicateRank.GuideAgent:
                        case SyndicateRank.LilyAgent:
                        case SyndicateRank.OrchidAgent:
                        case SyndicateRank.PkAgent:
                        case SyndicateRank.SilverAgent:
                        case SyndicateRank.TulipAgent:
                        case SyndicateRank.Follower:
                        case SyndicateRank.ArsenalFollower:
                        case SyndicateRank.CpFollower:
                        case SyndicateRank.GuideFollower:
                        case SyndicateRank.LilyFollower:
                        case SyndicateRank.OrchidFollower:
                        case SyndicateRank.PkFollower:
                        case SyndicateRank.SilverFollower:
                        case SyndicateRank.TulipFollower:
                            return 1;
                        default:
                            return 0;
                    }
                }

                #endregion

                #region Level 8

                case 8:
                {
                    switch (pos)
                    {
                        case SyndicateRank.Manager:
                        case SyndicateRank.ManagerAide:
                            return 6;
                        case SyndicateRank.Supervisor:
                        case SyndicateRank.ArsenalSupervisor:
                        case SyndicateRank.CpSupervisor:
                        case SyndicateRank.GuideSupervisor:
                        case SyndicateRank.LilySupervisor:
                        case SyndicateRank.OrchidSupervisor:
                        case SyndicateRank.PkSupervisor:
                        case SyndicateRank.SilverSupervisor:
                        case SyndicateRank.TulipSupervisor:
                            return 2;
                        case SyndicateRank.Steward:
                            return 7;
                        case SyndicateRank.Agent:
                        case SyndicateRank.ArsenalAgent:
                        case SyndicateRank.CpAgent:
                        case SyndicateRank.GuideAgent:
                        case SyndicateRank.LilyAgent:
                        case SyndicateRank.OrchidAgent:
                        case SyndicateRank.PkAgent:
                        case SyndicateRank.SilverAgent:
                        case SyndicateRank.TulipAgent:
                        case SyndicateRank.Follower:
                        case SyndicateRank.ArsenalFollower:
                        case SyndicateRank.CpFollower:
                        case SyndicateRank.GuideFollower:
                        case SyndicateRank.LilyFollower:
                        case SyndicateRank.OrchidFollower:
                        case SyndicateRank.PkFollower:
                        case SyndicateRank.SilverFollower:
                        case SyndicateRank.TulipFollower:
                            return 1;
                        default:
                            return 0;
                    }
                }

                #endregion

                #region Level 9

                case 9:
                {
                    switch (pos)
                    {
                        case SyndicateRank.Manager:
                        case SyndicateRank.ManagerAide:
                            return 8;
                        case SyndicateRank.Supervisor:
                        case SyndicateRank.ArsenalSupervisor:
                        case SyndicateRank.CpSupervisor:
                        case SyndicateRank.GuideSupervisor:
                        case SyndicateRank.LilySupervisor:
                        case SyndicateRank.OrchidSupervisor:
                        case SyndicateRank.PkSupervisor:
                        case SyndicateRank.SilverSupervisor:
                        case SyndicateRank.TulipSupervisor:
                            return 2;
                        case SyndicateRank.Steward:
                            return 8;
                        case SyndicateRank.Agent:
                        case SyndicateRank.ArsenalAgent:
                        case SyndicateRank.CpAgent:
                        case SyndicateRank.GuideAgent:
                        case SyndicateRank.LilyAgent:
                        case SyndicateRank.OrchidAgent:
                        case SyndicateRank.PkAgent:
                        case SyndicateRank.SilverAgent:
                        case SyndicateRank.TulipAgent:
                        case SyndicateRank.Follower:
                        case SyndicateRank.ArsenalFollower:
                        case SyndicateRank.CpFollower:
                        case SyndicateRank.GuideFollower:
                        case SyndicateRank.LilyFollower:
                        case SyndicateRank.OrchidFollower:
                        case SyndicateRank.PkFollower:
                        case SyndicateRank.SilverFollower:
                        case SyndicateRank.TulipFollower:
                            return 1;
                        default:
                            return 0;
                    }
                }

                #endregion

                default:
                    return 0;
            }
        }

        #endregion

        #region Message

        public void SetAnnouncement(string szMessage)
        {
            if (szMessage.Length > 127)
                szMessage = szMessage.Substring(0, 127);

            Announcement = szMessage;

            Send(new MsgTalk(Announcement, ChatTone.GuildAnnouncement));
        }

        public void Send(byte[] pBuffer)
        {
            foreach (var plr in Members.Values.Where(x => x.IsOnline))
            {
                plr.Send(pBuffer);
            }
        }

        public void Send(byte[] pBuffer, uint idSender)
        {
            foreach (var plr in Members.Values.Where(x => x.IsOnline && x.Identity != idSender))
            {
                plr.Send(pBuffer);
            }
        }

        public void Send(string szMessage)
        {
            foreach (var plr in Members.Values.Where(x => x.IsOnline))
            {
                plr.Send(new MsgTalk(szMessage, ChatTone.Guild, Color.White));
            }
        }

        public void Send(string szMessage, uint idSender)
        {
            foreach (var plr in Members.Values.Where(x => x.IsOnline && x.Identity != idSender))
            {
                plr.Send(new MsgTalk(szMessage, ChatTone.Guild, Color.White));
            }
        }

        #endregion

        #region Socket

        public MsgName NamePacket
        {
            get
            {
                MsgName pMsg = new MsgName
                {
                    Action = StringAction.Guild,
                    Identity = Identity
                };
                pMsg.Append($"{Name.Replace(' ', '~')} {LeaderName.Replace(' ', '~')} {Level} {MemberCount}");
                return pMsg;
            }
        }

        public void SendName()
        {
            ServerKernel.UserManager.SendToAllUser(NamePacket);
        }

        public void SendName(Character pTarget, bool bTransmit = false)
        {
            if (bTransmit)
            {
                pTarget.Screen.Send(NamePacket, true);
                return;
            }

            pTarget.Send(NamePacket);
        }

        public void SendRelation(Character pTarget)
        {
            foreach (var syn in Allies.Values)
            {
                var allies = new MsgName
                {
                    Identity = syn.Identity,
                    Action = StringAction.SetAlly
                };
                string test = $"{syn.Name} {syn.LeaderName} {syn.Level} {syn.MemberCount}";
                allies.Append(test);
                pTarget.Send(allies);
            }

            foreach (var syn in Enemies.Values)
            {
                var enemies = new MsgName
                {
                    Identity = syn.Identity,
                    Action = StringAction.SetEnemy
                };
                enemies.Append($"{syn.Name} {syn.LeaderName} {syn.Level} {syn.MemberCount}");
                pTarget.Send(enemies);
            }
        }

        public void SendMembers(Character pTarget, uint idx)
        {
            if (pTarget == null)
                return;

            var msg = new MsgSynMemberList
            {
                StartIndex = idx
            };

            uint maxmem = idx + 12;
            uint minmem = idx;
            uint count = 0;

            foreach (var member in Members.Values.OrderByDescending(x => x.IsOnline ? 1 : 0)
                .ThenByDescending(x => x.Position))
            {
                if (count < minmem || count >= maxmem)
                {
                    count++;
                    continue;
                }

                uint expire = 0;
                uint time = (uint) UnixTimestamp.Now();
                if (time < member.PositionExpire)
                    expire = uint.Parse(UnixTimestamp.ToDateTime(member.PositionExpire).ToString("yyyyMMdd"));

                msg.Append(member.Name, member.Lookface, member.Nobility, member.Level, member.Position,
                    expire /*time*/,
                    member.TotalDonation, member.IsOnline, member.Profession, 0);

                count++;
            }

            pTarget.Send(msg);
        }

        #endregion

        #region Database

        public bool Save()
        {
            return m_dbSyn != null && new SyndicateRepository().Save(m_dbSyn);
        }

        public bool Delete()
        {
            if (m_dbSyn == null) return false;
            m_dbSyn.DelFlag = 1;
            return new SyndicateRepository().Save(m_dbSyn);
        }

        #endregion

        #region Syndicate War

        public void AddSynWarScore(DynamicNpc pNpc, uint nScore)
        {
            if (!pNpc.Scores.ContainsKey(Identity))
            {
                var sScore = new NpcScore(Identity, Name);
                sScore.Score += nScore;
                pNpc.Scores.TryAdd(Identity, sScore);
                return;
            }
            pNpc.Scores[Identity].Score += nScore;
        }

        #endregion
    }
}