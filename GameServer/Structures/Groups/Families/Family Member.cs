﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Family Member.cs
// Last Edit: 2019/11/24 19:21
// Created: 2019/11/24 19:18
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Structures.Groups.Families
{
    public sealed class FamilyMember
    {
        private FamilyMemberEntity m_dbObj;
        private readonly Family m_pFamily;

        public FamilyMember(Family pFamily)
        {
            m_pFamily = pFamily;
        }

        public uint Identity => m_dbObj.Identity;

        public string Name { get; set; }

        public byte Level { get; set; }

        public uint Donation
        {
            get => m_dbObj.Money;
            set
            {
                m_dbObj.Money = value;
                Save();
            }
        }

        public FamilyRank Position
        {
            get => (FamilyRank) m_dbObj.Position;
            set
            {
                m_dbObj.Position = (byte) value;
                Save();
            }
        }

        public bool IsOnline => Owner != null;

        public Character Owner => ServerKernel.UserManager.GetUser(Identity);

        public uint JoinDate => m_dbObj.JoinDate;

        public bool Create(Character pUser)
        {
            m_dbObj = new FamilyMemberEntity
            {
                FamilyIdentity = m_pFamily.Identity,
                Identity = pUser.Identity,
                JoinDate = (uint) UnixTimestamp.Now(),
                Position = (byte) FamilyRank.Member
            };
            Save();

            Name = pUser.Name;
            Level = pUser.Level;

            return true;
        }

        public bool Create(CharacterEntity user, FamilyMemberEntity obj)
        {
            m_dbObj = obj;
            Name = user.Name;
            Level = user.Level;
            return true;
        }

        public bool Save()
        {
            return m_dbObj != null && Database.FamilyMember.Save(m_dbObj);
        }

        public bool Delete()
        {
            return m_dbObj != null && Database.FamilyMember.Delete(m_dbObj);
        }
    }
}