﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Team.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Concurrent;
using System.Drawing;
using System.Linq;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Qualifiers.Teams;

#endregion

namespace GameServer.Structures
{
    public class Team
    {
        private Character m_pLeader;
        public ConcurrentDictionary<uint, Character> Members;

        private bool m_bCloseMoney, m_bCloseItem = true, m_bCloseGem = true, m_bForbid, m_bAutoInvite = false;

        public Team(Character leader)
        {
            m_pLeader = leader;
            Members = new ConcurrentDictionary<uint, Character>();
            Members.TryAdd(leader.Identity, leader);
        }

        public bool IsCloseMoney => m_bCloseMoney;

        public bool IsCloseItem => m_bCloseItem;

        public bool IsCloseGem => m_bCloseGem;

        public bool IsForbid => m_bForbid;

        public bool IsAutoInvite => m_bAutoInvite;

        public Character Leader => m_pLeader;

        public void SetForbid(bool bTrue)
        {
            m_bForbid = bTrue;
        }

        public void SetCloseMoney(bool bClose)
        {
            m_bCloseMoney = bClose;
        }

        public void SetCloseItem(bool bClose)
        {
            m_bCloseItem = bClose;
        }

        public void SetCloseGem(bool bClose)
        {
            m_bCloseGem = bClose;
        }

        public void SetAutoInvite(bool bAutoInvite)
        {
            m_bAutoInvite = bAutoInvite;
        }

        public bool Destroy(Character pRole, MsgTeam pMsg)
        {
            if (pRole != m_pLeader)
            {
                pRole.SendSysMessage(Language.StrTeamNoCaptainDismiss);
                return false;
            }

            Send(pMsg);

            foreach (var plr in Members.Values)
                plr.Team = null;
            pRole.DetachStatus(FlagInt.TEAM_LEADER);
            m_pLeader = null;
            Members.Clear();
            return true;
        }

        public bool IsTeamMember(Character pRole)
        {
            return IsTeamMember(pRole.Identity);
        }

        public bool IsTeamMember(uint idRole)
        {
            return Members.ContainsKey(idRole);
        }

        public void LeaderDisconnect()
        {
            m_pLeader.DetachStatus(FlagInt.TEAM_LEADER);
            m_pLeader = Members.Values.OrderByDescending(x => x.BattlePower).FirstOrDefault(x => x.Identity != Leader.Identity);
            SendMember();
        }

        public void Send(string szMsg)
        {
            foreach (var usr in Members.Values)
                usr.Send(new MsgTalk(szMsg, ChatTone.Team, Color.White));
        }

        public void Send(byte[] pMsg)
        {
            foreach (var usr in Members.Values)
                usr.Send(pMsg);
        }

        public void Send(byte[] pMsg, uint idSender)
        {
            foreach (var usr in Members.Values.Where(x => x.Identity != idSender))
                usr.Send(pMsg);
        }

        public void LeaveTeam(Character pRole, MsgTeam pMsg)
        {
            Character trash;
            if (!Members.TryRemove(pRole.Identity, out trash))
                return;

            ServerKernel.TeamQualifier.Uninscribe(pRole, pMsg == null ? TeamArenaQualifierManager.UninscribeReason.Disconnect : TeamArenaQualifierManager.UninscribeReason.ExitingTeam);
            if (pRole.Identity == Leader.Identity)
            {
                LeaderDisconnect();
            }

            if (Members.Count == 0)
            {
                Destroy(pRole, pMsg);
            }

            pRole.Team = null;
            if (pMsg != null && pMsg.Type != TeamActionType.DISMISS)
            {
                Send(pMsg);
            }
            else
            {
                Send(new MsgTeam
                {
                    Type = TeamActionType.KICK,
                    Target = pRole.Identity
                });
            }
            pRole.Send(pMsg);
            CheckAuras();
        }

        public int MembersCount()
        {
            return Members.Count;
        }

        public void KickMember(Character pRole, MsgTeam pMsg)
        {
            if (pRole != m_pLeader)
            {
                pRole.SendSysMessage(Language.StrTeamNotLeaderKick);
                return;
            }

            Character pTarget;
            if (!Members.TryRemove(pMsg.Target, out pTarget))
            {
                pRole.SendSysMessage(Language.StrTeamApplicantNotFound);
                return;
            }

            pTarget.Send(pMsg);
            Send(pMsg);
            pTarget.Team = null;
            CheckAuras();
        }

        /// <summary>
        /// This method will send the request to the leader to accept.
        /// </summary>
        /// <param name="pSender">The user who wants to join the team</param>
        /// <param name="pTarget">The leader of the team</param>
        /// <param name="pMsg">The packet that will be processed</param>
        public void RequestMember(Character pSender, Character pTarget, MsgTeam pMsg)
        {
            if (pTarget != m_pLeader)
            {
                pSender.SendSysMessage(Language.StrTeamNoCapitainClose);
                return;
            }

            if (IsForbid)
            {
                pSender.SendSysMessage(Language.StrTeamForbiddenJoin); // message to player
                return;
            }

            if (Members.Count >= _MAX_MEMBER)
            {
                pSender.SendSysMessage(Language.StrTeamIsFull);
                return;
            }

            pSender.SetTeamJoin(pTarget.Identity);
            pMsg.Target = pSender.Identity;
            pTarget.SendRelation(pSender);
            pTarget.Send(pMsg);
        }

        /// <summary>
        /// This method will be called when the leader is inviting the member.
        /// </summary>
        /// <param name="pSender">The user who is being invited.</param>
        /// <param name="pTarget">The leader of the team</param>
        /// <param name="pMsg">The packet that will be processed</param>
        public void InviteMember(Character pSender, Character pTarget, MsgTeam pMsg)
        {
            if (pTarget != m_pLeader)
            {
                pSender.SendSysMessage(Language.StrTeamNoCapitainAccept);
                return;
            }

            if (IsForbid)
            {
                // pSender.SendSysMessage(Language.STR_FORBIDDEN_JOIN); // message to player
                pTarget.SendSysMessage(Language.StrTeamForbiddenJoin); // message to leader
                return;
            }

            if (Members.Count >= _MAX_MEMBER)
            {
                pSender.SendSysMessage(Language.StrTeamIsFull);
                return;
            }

            pTarget.SetTeamInvite(pSender.Identity);
            pMsg.Target = pTarget.Identity;
            pSender.SendRelation(pTarget);
            pSender.Send(pMsg);
        }

        /// <summary>
        /// This method is called when the leader accept an user request.
        /// </summary>
        public void AcceptMember(Character pLeader, Character pMember, MsgTeam pMsg)
        {
            if (pLeader != m_pLeader)
            {
                pLeader.SendSysMessage(Language.StrTeamNoCapitainAccept);
                return;
            }

            if (IsForbid)
            {
                // pLeader.SendSysMessage(Language.STR_FORBIDDEN_JOIN); // message to player
                pLeader.SendSysMessage(Language.StrTeamIsClosed); // message to leader
                return;
            }

            if (Members.Count >= _MAX_MEMBER)
            {
                pLeader.SendSysMessage(Language.StrTeamIsFull);
                return;
            }

            if (!pMember.FetchTeamJoin(pLeader.Identity))
                return;

            if (Members.TryAdd(pMember.Identity, pMember))
            {
                pMember.Send(pMsg);
                pMember.Team = this;
                MsgTeamMember member = new MsgTeamMember();
                member.AppendMember(pMember.Name, pMember.Identity, pMember.Lookface, (ushort)pMember.MaxLife, (ushort)pMember.Life);
                pMember.Send(member);
                SendMember();
                pMember.SendMessage(string.Format(Language.StrTeamMoney, !m_bCloseMoney ? "ON" : "OFF"), ChatTone.Team);
                pMember.SendMessage(string.Format(Language.StrTeamItem, !m_bCloseItem ? "ON" : "OFF"), ChatTone.Team);
                pMember.SendMessage(string.Format(Language.StrTeamGem, !m_bCloseGem ? "ON" : "OFF"), ChatTone.Team);
                pMember.ClearTeamJoin();

                CheckAuras();
            }
        }

        /// <summary>
        /// This method is called when the user accept a team invitation.
        /// </summary>
        public void MemberAccept(Character pLeader, Character pMember, MsgTeam pMsg)
        {
            if (pLeader != m_pLeader)
            {
                pMember.SendSysMessage(Language.StrTeamNoCapitainClose);
                return;
            }

            if (IsForbid)
            {
                pLeader.SendSysMessage(Language.StrTeamForbiddenJoin); // message to player
                // pLeader.SendSysMessage(Language.STR_TEAM_CLOSED); // message to leader
                return;
            }

            if (Members.Count >= _MAX_MEMBER)
            {
                pMember.SendSysMessage(Language.StrTeamIsFull);
                return;
            }

            if (!pLeader.FetchTeamInvite(pMember.Identity))
                return;

            if (Members.TryAdd(pMember.Identity, pMember))
            {
                pMember.Send(pMsg);
                pMember.Team = this;
                MsgTeamMember member = new MsgTeamMember();
                member.AppendMember(pMember.Name, pMember.Identity, pMember.Lookface, (ushort) pMember.MaxLife, (ushort) pMember.Life);
                pMember.Send(member);
                SendMember();
                pMember.SendMessage(string.Format(Language.StrTeamMoney, !m_bCloseMoney ? "ON" : "OFF"), ChatTone.Team);
                pMember.SendMessage(string.Format(Language.StrTeamItem, !m_bCloseItem ? "ON" : "OFF"), ChatTone.Team);
                pMember.SendMessage(string.Format(Language.StrTeamGem, !m_bCloseGem ? "ON" : "OFF"), ChatTone.Team);
                pLeader.ClearTeamInvite();

                CheckAuras();
            }
        }

        public void SendMember()
        {
            MsgTeamMember msg = new MsgTeamMember
            {
                Action = MsgTeamMember.ADD_MEMBER
            };
            foreach (var usr0 in Members.Values.OrderByDescending(x => x.Team.Leader.Identity == x.Identity ? 1 : 0))
            {
                msg.AppendMember(usr0.Name, usr0.Identity, usr0.Lookface, (ushort) usr0.MaxLife, (ushort) usr0.Life);
            }
            foreach (var usr1 in Members.Values)
            {
                usr1.Send(msg);
                usr1.Send(new MsgAuraGroup
                {
                    Identity = usr1.Identity,
                    LeaderIdentity = Leader.Identity,
                    Count = (uint)Members.Count,
                    Mode = AuraGroupMode.Leader
                });
            }
        }

        public void SendTeam(Character pTarget)
        {
            foreach (var usr in Members.Values)
            {
                pTarget.Send(new MsgTeamMember
                {
                    Entity = usr.Identity,
                    Life = (ushort) usr.Life,
                    MaxLife = (ushort) usr.MaxLife,
                    Mesh = usr.Lookface,
                    Name = usr.Name
                });
            }
        }

        public void SendLeaderPosition(Character pSender)
        {
            if (m_pLeader == null)
                return;
            var pMsg = new MsgAction(m_pLeader.Identity, m_pLeader.MapIdentity, m_pLeader.MapX, m_pLeader.MapY,
                GeneralActionType.TeamMemberPos);
            pSender.Send(pMsg);
        }

        public void SendMemberPosition(Character pSender, MsgAction pMsg)
        {
            Character pTarget;
            if (!Members.TryGetValue(pMsg.Identity, out pTarget))
                return;
            pMsg.X = pTarget.MapX;
            pMsg.Y = pTarget.MapY;
            pMsg.Data = pTarget.MapIdentity;
            pSender.Send(pMsg);
        }

        public void CheckAuras()
        {
            foreach (var member in Members.Values)
                member.ProcessAura();
        }

        public void AwardMemberExp(uint idKiller, Role pTarget, long nExp)
        {
            if (pTarget == null || nExp < 0) return;

            Character pKiller = ServerKernel.UserManager.GetUser(idKiller);
            if (pKiller == null)
                return;

            int nMonsterLev = pTarget.Level;
            foreach (var pUser in Members.Values)
            {
                if (!pUser.IsAlive)
                    continue;

                // map, no self
                if (pUser.Map.Identity != pKiller.Map.Identity || pUser.Identity == pKiller.Identity)
                    continue;
                // no self
                if (!(pUser.IsAlive && pUser.Identity != idKiller))
                    continue;
                // distance
                if (Calculations.GetDistance(pUser.MapX, pUser.MapY, pKiller.MapX, pKiller.MapY) > 32)
                    continue; // out of range

                LevelExperienceEntity exp = ServerKernel.LevelExperience.Values.FirstOrDefault(x => x.Level == pUser.Level);
                if (exp == null)
                    continue;
                long nAddExp = pUser.AdjustExperience(pTarget, nExp, false);
                int nMaxStuExp = (int) exp.Exp;
                nAddExp = Math.Min(nAddExp, nMaxStuExp);

                if (nAddExp > pUser.Level * 360)
                    nAddExp = pUser.Level * 360;
                if (nAddExp <= 0)
                    nAddExp = 1;

                if (pUser.IsMate(pKiller))
                    nAddExp *= 2;

                pUser.AwardBattleExp(nAddExp, false);
                pUser.SendSysMessage(string.Format(Language.StrTeamExperience, nAddExp));
            }
        }

        public int GetTeamQualifierBattlePower()
        {
            return Members.Values.Max(x => x.BattlePower);
        }

        public bool IsMatchEnable(int targetGrade)
        {
            int nDelta = GetTeamQualifierGrade() - targetGrade;
            if (nDelta < 0)
                nDelta *= -1;
            return nDelta < 2;
        }

        public uint GetTeamQualifierPoints()
        {
            return Members.Values.Max(x => x.TeamPlayerQualifier.Points);
        }

        public int GetTeamQualifierGrade()
        {
            return Members.Values.Max(x => x.TeamPlayerQualifier.Grade);
        }

        public bool CanJoinTeamQualifier(bool showError)
        {
            if (Members.Values.Any(x => x.Level < TeamArenaQualifierManager.MIN_LEVEL))
            {
                if (showError)
                    Send(Language.StrTeamQualifierLowLevel);
                return false;
            }

            if (Members.Values.Any(x => x.TeamPlayerQualifier.IsBanned))
            {
                if (showError)
                    Send(Language.StrTeamQualifierBanned);
                return false;
            }

            if (Members.Values.Any(x => x.TeamPlayerQualifier.Points == 0))
            {
                if (showError)
                    Send(Language.StrTeamQualifierNotEnoughPoints);
                return false;
            }

            if (Members.Values
                .Any(x => x.PlayerQualifier.Status != ArenaWaitStatus.NOT_SIGNED_UP || x.TeamPlayerQualifier.Status != ArenaWaitStatus.NOT_SIGNED_UP))
            {
                if (showError)
                    Send(Language.StrTeamQualifierLeaveQualifier);
                return false;
            }

            if (Members.Values.Any(x => x.Map.IsPrisionMap()))
            {
                if (showError)
                    Send(Language.StrTeamQualifierPrisionMap);
                return false;
            }

            if (Members.Values.Any(x => x.Map.IsTrainingMap()))
            {
                if (showError)
                    Send(Language.StrTeamQualifierTrainingMap);
                return false;
            }

            return true;
        }

        public bool IsWaitingForOpponent()
        {
            return Members.Values.FirstOrDefault(x => x.TeamPlayerQualifier.Status != ArenaWaitStatus.WAITING_FOR_OPPONENT) == null;
        }

        public bool EnterTeamQualifier()
        {
            if (!CanJoinTeamQualifier(false))
                return false;

            foreach (Character user in Members.Values)
            {
                user.TeamPlayerQualifier.Status = ArenaWaitStatus.WAITING_FOR_OPPONENT;
                user.TeamPlayerQualifier.JoinTime = DateTime.Now;
            }
            return true;
        }

        public void ExitTeamQualifier(ArenaWaitStatus stts)
        {
            foreach (Character user in Members.Values)
            {
                user.TeamPlayerQualifier.Status = stts;
                user.TeamPlayerQualifier.JoinTime = DateTime.MinValue;
            }
        }
        
        private const int _MAX_MEMBER = 5;
    }
}