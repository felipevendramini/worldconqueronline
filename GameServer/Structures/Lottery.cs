﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Lottery.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.Linq;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Structures
{
    public static class Lottery
    {
        public const int MAX_ATTEMPTS = 10;

        public static ItemEntity GenerateItem(Character user, bool firstDraw)
        {
            byte color = user.LotteryLastColor;
            byte type = user.LotteryLastType;

            user.LotteryTemporaryItem = null;
            user.LotteryLastItemName = "";

            diceAgain:
            byte rank = 8;
            if (Calculations.ChanceCalc(.25f))
                rank = 1;
            else if (Calculations.ChanceCalc(2f))
                rank = 2;
            else if (Calculations.ChanceCalc(4.5f))
                rank = 3;
            else if (Calculations.ChanceCalc(7.5f))
                rank = 4;
            else if (Calculations.ChanceCalc(10f))
                rank = 5;
            else if (Calculations.ChanceCalc(20f))
                rank = 6;
            else if (Calculations.ChanceCalc(40f))
                rank = 7;

            IList<LotteryEntity> lotto = null;
            if (rank > 5)
                lotto = new LotteryRepository().FetchAllRank5();
            else
                lotto = new LotteryRepository().FetchAllByColor(color);

            if (lotto == null)
            {
#if DEBUG
                user.SendSysMessage("ERROR: Could not fetch any lottery item");
#endif
                return null;
            }

            for (int i = lotto.Count - 1; i >= 0; i--)
            {
                if (lotto[i].Rank != rank && rank < 5)
                    lotto.RemoveAt(i);
            }

            if (lotto.Count <= 0)
            {
#if DEBUG
                user.SendSysMessage("ERROR: no lottery item to sort");
#endif
                return null;
            }

            LotteryEntity dbLotto = lotto[ThreadSafeRandom.RandGet(lotto.Count) % lotto.Count];

            if (!Calculations.ChanceCalc(Math.Min((byte) 100, dbLotto.Chance)))
            {
                goto diceAgain;
            }

            ItemtypeEntity itemtype = ServerKernel.Itemtype.Values.FirstOrDefault(x => x.Type == dbLotto.ItemIdentity);
            if (itemtype == null)
            {
                goto diceAgain;
            }

            ItemEntity dbItem = new ItemEntity
            {
                PlayerId = user.Identity,
                Type = itemtype.Type,
                Amount = itemtype.Amount,
                AmountLimit = itemtype.AmountLimit,
                Magic3 = dbLotto.Plus > 0 ? dbLotto.Plus : itemtype.Magic3,
                Gem1 = (byte) (dbLotto.SocketNum > 0 ? 255 : 0),
                Gem2 = (byte) (dbLotto.SocketNum > 1 ? 255 : 0),
                OwnerId = 923,
                Color = 3,
                StackAmount = 1
            };

            if (!user.UserPackage.IsPackSpare(1))
            {
                user.SendSysMessage(Language.StrYourInventoryIsFull);
                return null;
            }

            user.LotteryLastRank = dbLotto.Rank;
            user.LotteryTemporaryItem = dbItem;
            user.LotteryLastItemName = dbLotto.Itemname;

            if (firstDraw)
            {
                user.Send(new MsgLottery
                {
                    Addition = dbItem.Magic3,
                    SocketOne = (SocketGem) dbItem.Gem1,
                    SocketTwo = (SocketGem) dbItem.Gem2,
                    Color = (ItemColor) dbItem.Color,
                    //UsedChances = 1,
                    Itemtype = dbItem.Type,
                    //Chances = 3,
                    Request = LotteryRequest.Show
                });

                user.Statistics.AddOrUpdate(22, 2, 0, true);
                user.Statistics.AddOrUpdate(22, 0, user.Statistics.GetValue(22, 0) + 1, true);
            }
            else
            {
                user.Send(new MsgLottery
                {
                    Addition = dbItem.Magic3,
                    SocketOne = (SocketGem)dbItem.Gem1,
                    SocketTwo = (SocketGem)dbItem.Gem2,
                    Color = (ItemColor)dbItem.Color,
                    UsedChances = (byte) (user.GetStatisticValue(22, 1) + 1 > 2 ? 2 : 1),
                    Itemtype = dbItem.Type,
                    Request = LotteryRequest.Show
                });
            }

            user.Statistics.AddOrUpdate(22, 1, user.Statistics.GetValue(22, 1) + 1, true);
            return dbItem;
        }

        public static void Clear(Character user)
        {
            user.LotteryLastItemName = "";
            //user.LotteryLastColor = 0;
            user.LotteryLastRank = 0;
            //user.LotteryLastType = 0;
            user.LotteryTemporaryItem = null;
        }
    }
}