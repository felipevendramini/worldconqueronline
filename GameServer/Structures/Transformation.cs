﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Transformation.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Structures
{
    public class Transformation
    {
        private MonstertypeEntity m_dbMonster;
        private Role m_pOwner;

        private ushort m_usLife;

        public Transformation(Role pOwner)
        {
            m_pOwner = pOwner;
        }

        public ushort Life
        {
            get => m_usLife;
            set => m_usLife = value;
        }

        public int MaxLife => m_dbMonster.Life;

        public int MinAttack => m_dbMonster.AttackMin;

        public int MaxAttack => m_dbMonster.AttackMax;

        public int Attack => (MinAttack + MaxAttack) / 2;

        public int Defense => m_dbMonster.Defence;

        public uint Defense2 => m_dbMonster.Defence2;

        public uint Dexterity => m_dbMonster.Dexterity;

        public uint Dodge => m_dbMonster.Dodge;

        public int MagicDefense => m_dbMonster.MagicDef;

        public int InterAtkRate
        {
            get
            {
                int nRate = IntervalAtkRate;
                IStatus pStatus = m_pOwner.QueryStatus(FlagInt.CYCLONE);
                if (pStatus != null)
                    nRate = Calculations.CutTrail(0, Calculations.AdjustDataEx(nRate, pStatus.Power, 0));
                return nRate;
            }
        }

        public int IntervalAtkRate => m_dbMonster.AttackSpeed;

        public int MagicHitRate => (int) m_dbMonster.MagicHitrate;

        public int Lookface => m_dbMonster.Lookface;

        public bool IsJumpEnable => (m_dbMonster.AttackUser & MonsterAttackMode.ATKUSER_JUMP) != 0;

        public bool IsMoveEnable => (m_dbMonster.AttackUser & MonsterAttackMode.ATKUSER_FIXED) == 0;

        public bool Create(MonstertypeEntity pTrans)
        {
            if (m_pOwner == null || pTrans == null || pTrans.Life <= 0)
                return false;

            m_dbMonster = pTrans;
            m_usLife = (ushort) Calculations.CutTrail(1, Calculations.MulDiv(m_pOwner.Life, MaxLife, m_pOwner.MaxLife));

            return true;
        }

        public int GetAttackRange(int nTargetSizeAdd)
        {
            return (int) ((m_dbMonster.AttackRange + GetSizeAdd() + nTargetSizeAdd + 1) / 2);
        }

        public uint GetSizeAdd()
        {
            return m_dbMonster.SizeAdd;
        }
    }
}