﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Flower.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Database.Entities;

#endregion

namespace GameServer.Structures.Flower
{
    public class FlowerObject
    {
        private FlowerEntity m_FlowerEntity;

        public FlowerObject(uint idUser, string szName)
        {
            m_FlowerEntity = new FlowerEntity
            {
                PlayerName = szName,
                PlayerIdentity = idUser
            };
        }

        public FlowerObject(FlowerEntity obj)
        {
            m_FlowerEntity = obj;
        }

        public uint PlayerIdentity => m_FlowerEntity.PlayerIdentity;

        public string PlayerName
        {
            get => m_FlowerEntity.PlayerName;
            set => m_FlowerEntity.PlayerName = value;
        }

        public uint RedRoses { get; set; }

        public uint RedRosesToday
        {
            get => m_FlowerEntity.RedRoses;
            set => m_FlowerEntity.RedRoses = value;
        }

        public int RedRoseRanking => ServerKernel.FlowerRanking.RedRosePosition(PlayerIdentity);
        public uint WhiteRoses { get; set; }

        public uint WhiteRosesToday
        {
            get => m_FlowerEntity.WhiteRoses;
            set => m_FlowerEntity.WhiteRoses = value;
        }

        public int WhiteRoseRanking => ServerKernel.FlowerRanking.WhiteRosePosition(PlayerIdentity);
        public uint Orchids { get; set; }

        public uint OrchidsToday
        {
            get => m_FlowerEntity.Orchids;
            set => m_FlowerEntity.Orchids = value;
        }

        public int OrchidsRanking => ServerKernel.FlowerRanking.OrchidsPosition(PlayerIdentity);
        public uint Tulips { get; set; }

        public uint TulipsToday
        {
            get => m_FlowerEntity.Tulips;
            set => m_FlowerEntity.Tulips = value;
        }

        public int TulipsRanking => ServerKernel.FlowerRanking.TulipsPosition(PlayerIdentity);
        public FlowerEntity Database => m_FlowerEntity;
    }
}