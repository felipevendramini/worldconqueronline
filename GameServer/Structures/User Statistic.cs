﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - User Statistic.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Concurrent;
using System.Linq;
using FtwCore.Common;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Structures
{
    public sealed class UserStatistic
    {
        private ConcurrentDictionary<ulong, StatisticEntity> m_dicStc = new ConcurrentDictionary<ulong, StatisticEntity>();
        private Character m_pOwner;

        public UserStatistic(Character user)
        {
            m_pOwner = user;
        }

        public bool Initialize()
        {
            try
            {
                var list = new StatisticRepository().FetchList(m_pOwner.Identity);
                if (list != null)
                {
                    foreach (var st in list)
                    {
                        m_dicStc.TryAdd(st.EventType + (st.DataType << 32), st);
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool AddOrUpdate(uint idEvent, uint idType, uint data, bool bUpdate)
        {
            ulong key = GetKey(idEvent, idType);
            StatisticEntity stc;
            if (m_dicStc.TryGetValue(key, out stc))
            {
                stc.Data = data;
                if (bUpdate)
                    stc.Timestamp = (uint) UnixTimestamp.Now();
            }
            else
            {
                stc = new StatisticEntity
                {
                    Data = data,
                    DataType = idType,
                    EventType = idEvent,
                    PlayerIdentity = m_pOwner.Identity,
                    PlayerName = m_pOwner.Name,
                    Timestamp = (uint) UnixTimestamp.Now()
                };
                m_dicStc.TryAdd(key, stc);
            }

            return Database.Statistics.Save(stc);
        }

        public bool SetTimestamp(uint idEvent, uint idType, uint data)
        {
            StatisticEntity stc = GetStc(idEvent, idType);
            if (stc == null)
            {
                AddOrUpdate(idEvent, idType, 0, true);
                stc = GetStc(idEvent, idType);

                if (stc == null)
                    return false;
            }
            stc.Timestamp = data;
            return Database.Statistics.Save(stc);
        }

        public uint GetValue(uint idEvent, uint idType = 0)
        {
            return m_dicStc.FirstOrDefault(x => x.Key == GetKey(idEvent, idType)).Value?.Data ?? 0u;
        }

        public StatisticEntity GetStc(uint idEvent, uint idType = 0) => m_dicStc.FirstOrDefault(x => x.Key == GetKey(idEvent, idType)).Value;

        public bool HasEvent(uint idEvent, uint idType) => m_dicStc.ContainsKey(GetKey(idEvent, idType));

        private ulong GetKey(uint idEvent, uint idType) => idEvent + (idType << 32);

        public void SaveAll()
        {
            Database.Statistics.Save(m_dicStc.Values.ToArray());
        }
    }
}