﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Flower Ranking.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Linq;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Flower;

#endregion

namespace GameServer.Structures
{
    public class FlowerManager
    {
        public FlowerManager()
        {
            var plrList = new CharacterRepository().FetchAll();
            var rank = new FlowerRepository().FetchAll();
            foreach (var plr in rank)
            {
                var player = plrList.FirstOrDefault(x => x.Identity == plr.PlayerIdentity);
                if (player == null) continue;
                FlowerObject obj = new FlowerObject(plr)
                {
                    PlayerName = plr.PlayerName,
                    RedRoses = player.RedRoses,
                    WhiteRoses = player.WhiteRoses,
                    Orchids = player.Orchids,
                    Tulips = player.Tulips
                };
                ServerKernel.FlowerRankingDict.TryAdd(player.Identity, obj);
            }

            Program.WriteLog("Flower Ranking loaded...");
        }

        public bool AddFlowers(FlowerType flower, uint dwAmount, uint idTarget)
        {
            FlowerObject obj;
            if (!ServerKernel.FlowerRankingDict.TryGetValue(idTarget, out obj))
            {
                Character target;
                if ((target = ServerKernel.UserManager.GetUser(idTarget)) == null)
                    return false;

                obj = new FlowerObject(idTarget, target.Name);
                ServerKernel.FlowerRankingDict.TryAdd(idTarget, obj);
            }

            switch (flower)
            {
                case FlowerType.RED_ROSE:
                {
                    obj.RedRoses += dwAmount;
                    obj.RedRosesToday += dwAmount;
                    break;
                }

                case FlowerType.WHITE_ROSE:
                {
                    obj.WhiteRoses += dwAmount;
                    obj.WhiteRosesToday += dwAmount;
                    break;
                }

                case FlowerType.ORCHID:
                {
                    obj.Orchids += dwAmount;
                    obj.OrchidsToday += dwAmount;
                    break;
                }

                case FlowerType.TULIP:
                {
                    obj.Tulips += dwAmount;
                    obj.TulipsToday += dwAmount;
                    break;
                }
            }

            return new FlowerRepository().Save(obj.Database);
        }

        public FlowerObject FetchUser(uint idUser)
        {
            return ServerKernel.FlowerRankingDict.Values.FirstOrDefault(x => x.PlayerIdentity == idUser);
        }

        public FlowerObject FetchUser(string szName)
        {
            return ServerKernel.FlowerRankingDict.Values.FirstOrDefault(x => x.PlayerName == szName);
        }

        public FlowerObject[] RedRosesRanking()
        {
            FlowerObject[] list = new FlowerObject[100];
            int i = 0;
            foreach (FlowerObject flowerObject in ServerKernel.FlowerRankingDict.Values.Where(x => x.RedRoses > 0)
                .OrderByDescending(x => x.RedRoses))
            {
                list[i++] = flowerObject;
            }

            Array.Resize(ref list, i);
            return list;
        }

        public int RedRosePosition(uint idUser)
        {
            bool found = false;
            int i = -1;
            foreach (var obj in ServerKernel.FlowerRankingDict.Values.Where(x => x.RedRoses > 0)
                .OrderByDescending(x => x.RedRoses))
            {
                i++;
                if (i > 100)
                    break;
                if (obj.PlayerIdentity == idUser)
                {
                    if (obj.RedRoses <= 0)
                        return -1;

                    found = true;
                    break;
                }
            }

            return found ? i : -1;
        }

        public FlowerObject[] WhiteRosesRanking()
        {
            FlowerObject[] list = new FlowerObject[100];
            int i = 0;
            foreach (FlowerObject flowerObject in ServerKernel.FlowerRankingDict.Values.Where(x => x.WhiteRoses > 0)
                .OrderByDescending(x =>
                    x.WhiteRoses))
            {
                list[i++] = flowerObject;
            }

            Array.Resize(ref list, i + 1);
            return list;
        }

        public int WhiteRosePosition(uint idUser)
        {
            bool found = false;
            int i = -1;
            foreach (var obj in
                ServerKernel.FlowerRankingDict.Values.Where(x => x.WhiteRoses > 0).OrderByDescending(x => x.WhiteRoses))
            {
                i++;
                if (i > 100)
                    break;
                if (obj.PlayerIdentity == idUser)
                {
                    if (obj.WhiteRoses <= 0)
                        return -1;

                    found = true;
                    break;
                }
            }

            return found ? i : -1;
        }

        public FlowerObject[] OrchidsRanking()
        {
            FlowerObject[] list = new FlowerObject[100];
            int i = 0;
            foreach (FlowerObject flowerObject in ServerKernel.FlowerRankingDict.Values.Where(x => x.Orchids > 0)
                .OrderByDescending(
                    x => x.Orchids))
            {
                list[i++] = flowerObject;
            }

            Array.Resize(ref list, i + 1);
            return list;
        }

        public int OrchidsPosition(uint idUser)
        {
            bool found = false;
            int i = -1;
            foreach (var obj in
                ServerKernel.FlowerRankingDict.Values.Where(x => x.Orchids > 0).OrderByDescending(x => x.Orchids))
            {
                i++;
                if (i > 100)
                    break;
                if (obj.PlayerIdentity == idUser)
                {
                    if (obj.Orchids <= 0)
                        return -1;

                    found = true;
                    break;
                }
            }

            return found ? i : -1;
        }

        public FlowerObject[] TulipsRanking()
        {
            FlowerObject[] list = new FlowerObject[100];
            int i = 0;
            foreach (FlowerObject flowerObject in ServerKernel.FlowerRankingDict.Values.Where(x => x.Tulips > 0)
                .OrderByDescending(x => x.Tulips)
            )
            {
                list[i++] = flowerObject;
            }

            Array.Resize(ref list, i + 1);
            return list;
        }

        public int TulipsPosition(uint idUser)
        {
            bool found = false;
            int i = -1;
            foreach (var obj in
                ServerKernel.FlowerRankingDict.Values.Where(x => x.Tulips > 0).OrderByDescending(x => x.Tulips))
            {
                i++;
                if (i > 100)
                    break;
                if (obj.PlayerIdentity == idUser)
                {
                    if (obj.Tulips <= 0)
                        return -1;

                    found = true;
                    break;
                }
            }

            return found ? i : -1;
        }
    }
}