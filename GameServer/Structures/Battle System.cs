﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Battle System.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Linq;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Items;

#endregion

namespace GameServer.Structures
{
    enum MagicType
    {
        MagictypeNone = 0,
        MAGICTYPE_NORMAL = 1,
        MagictypeXpskill = 2
    }

    public sealed class BattleSystem
    {
        private Role m_pOwner;
        private TimeOutMS m_tAttack;

        private uint m_idTarget;
        private bool m_bAutoAttack;

        public BattleSystem(Role owner)
        {
            m_pOwner = owner;

            m_tAttack = new TimeOutMS(2000);
        }

        public Role FindRole(uint idRole)
        {
            if (idRole > IdentityRange.PLAYER_ID_FIRST && idRole < IdentityRange.PLAYER_ID_LAST)
                return ServerKernel.UserManager.GetUser(idRole);
            return ServerKernel.RoleManager.GetRole(idRole, out ScreenObject role) ? (Role) role : null;
        }

        public bool CreateBattle(uint idTarget)
        {
            if (idTarget == 0)
                return false;
            m_idTarget = idTarget;
            return true;
        }

        public void ResetBattle()
        {
            m_idTarget = 0;
            m_bAutoAttack = false;
        }

        public bool IsActive()
        {
            return m_idTarget != 0;
        }

        public void SetAutoAttack()
        {
            m_bAutoAttack = true;
        }

        public bool IsAutoAttack()
        {
            return m_bAutoAttack;
        }

        public void DestroyAutoAttack()
        {
            m_bAutoAttack = false;
        }

        public bool IsBattleMaintain()
        {
            if (m_idTarget == 0)
                return false;

            Role target = FindRole(m_idTarget);

            if (target == null)
            {
                if (m_pOwner is Character user)
                    user.Screen.Delete(m_idTarget);
                //ResetBattle();
                return false;
            }

            if (!target.IsAlive)
            {
                return false;
            }

            if (target.MapIdentity != m_pOwner.MapIdentity)
            {
                //ResetBattle();
                return false;
            }

            if (m_pOwner is Character owner && target is Character tgtUsr && (m_pOwner.Map.IsPkDisable() || m_pOwner.Map.IsLineSkillMap()))
            {
                if (owner.PkMode != PkModeType.JiangHu || !tgtUsr.JiangHuActive)
                    return false;
            }

            if (m_pOwner.Map.IsLineSkillMap())
            {
                //ResetBattle();
                return false;
            }

            if (target.IsWing && !m_pOwner.IsWing && !m_pOwner.IsBowman)
            {
                //ResetBattle();
                return false;
            }

            int nDistance = m_pOwner.GetDistance(target.MapX, target.MapY);
            if (nDistance > m_pOwner.GetAttackRange(target.SizeAddition) && m_pOwner.QueryStatus(FlagInt.FATAL_STRIKE) == null)
            {
                //ResetBattle();
                return false;
            }

            if (!target.IsAttackable(m_pOwner))
                return false;

            if (m_pOwner.QueryStatus(FlagInt.DAZED) != null
                || m_pOwner.QueryStatus(FlagInt.HUGE_DAZED) != null
                || m_pOwner.QueryStatus(FlagInt.ICE_BLOCK) != null
                || m_pOwner.QueryStatus(FlagInt.CONFUSED) != null)
            {
                ////ResetBattle();
                return false;
            }

            return true;
        }

        public bool ProcessAttack()
        {
            if (m_pOwner == null || m_idTarget == 0 || !IsBattleMaintain())
            {
                ResetBattle();
                return false;
            }

            Role target = FindRole(m_idTarget);

            if (target == null)
            {
                ResetBattle();
                return false;
            }

            if (m_pOwner.IsImmunity(target))
            {
                ResetBattle();
                return false;
            }

            if (m_pOwner.Magics.QueryMagic() != null)
                m_pOwner.Magics.AbortMagic(true);

            Character userTarget = target as Character;
            Character user = m_pOwner as Character;
            if (userTarget != null)
            {
                if (userTarget.CheckScapegoat(m_pOwner))
                    return true;
            }

            if (user != null && user.AutoSkillAttack(target))
            {
                return true;
            }

            if (IsTargetDodged(m_pOwner, target))
            {
                m_pOwner.SendDamageMsg(target.Identity, 0, InteractionEffect.None);
                return false;
            }

            InteractionEffect effect = InteractionEffect.None;
            int nDamage = m_pOwner.Attack(target, ref effect);
            int nTargetLifeLost = Math.Max(1, nDamage);
            long nExp = Math.Min(Math.Max(0, nTargetLifeLost), target.MaxLife);

            Monster monster = target as Monster;
            if (m_pOwner.QueryStatus(FlagInt.FATAL_STRIKE) != null
                && monster != null)
            {
                if (!monster.IsGuard()
                    && !monster.IsBoss)
                {
                    m_pOwner.JumpPos(target.MapX, target.MapY);
                    MsgAction msg = new MsgAction(m_pOwner.Identity, target.Identity, m_pOwner.MapX,
                        m_pOwner.MapY, GeneralActionType.NinjaStep);
                    if (user != null)
                    {
                        //user.Send(msg);
                        user.Screen.Send(msg, true);
                    }
                    else
                    {
                        m_pOwner.Map.SendToRegion(msg, m_pOwner.MapX, m_pOwner.MapY);
                    }
                }
            }

            nDamage = target.BattleSystem.CheckAzureShield(nDamage);

            m_pOwner.SendDamageMsg(target.Identity, nDamage, effect);

            m_pOwner.ProcessOnAttack();

            if (nDamage == 0)
                return false;

            target.BeAttack(0, m_pOwner, nDamage, true);
            user?.CheckCrime(target);

            DynamicNpc npc = target as DynamicNpc;
            if (npc != null && user != null)
            {
                if (npc.IsAwardScore())
                {
                    user.AwardSynWarScore(npc, nTargetLifeLost);
                }
            }

            if (user != null && ((monster != null && !monster.IsGuard() && !monster.IsBoss && !monster.IsRighteous()) || npc?.IsGoal() == true))
            {
                nExp = user.AdjustExperience(target, nExp, false);
                int nAdditionExp = 0;
                if (!target.IsAlive)
                {
                    nAdditionExp = (int) (target.MaxLife * 0.05f);
                    nExp += nAdditionExp;

                    user.Team?.AwardMemberExp(user.Identity, target, nAdditionExp);
                }

                user.AwardBattleExp(nExp, true);

                if (!target.IsAlive && nAdditionExp > 0
                                    && !m_pOwner.Map.IsTrainingMap())
                    user.SendSysMessage(string.Format(Language.StrKillingExperience, nAdditionExp));

                int nWeaponExp = (int) nExp/2; //(int) (nExp / 10);
                if (user.UserPackage[ItemPosition.RightHand] != null)
                    user.AddWeaponSkillExp((ushort) user.UserPackage[ItemPosition.RightHand].GetItemSubtype(),
                        nWeaponExp);
                if (user.UserPackage[ItemPosition.LeftHand] != null)
                    user.AddWeaponSkillExp((ushort) user.UserPackage[ItemPosition.LeftHand].GetItemSubtype(),
                        nWeaponExp / 2);

                if (m_pOwner.QueryStatus(FlagInt.FATAL_STRIKE) != null)
                    DestroyAutoAttack();

                if (Calculations.ChanceCalc(7f))
                    user.SendGemEffect();
            }

            if (!target.IsAlive)
            {
                uint dwDieWay = 1; // todo m_pOwner.IsSimpleMagicAtk() ? 3 : 1;
                if (nDamage > target.MaxLife / 3)
                    dwDieWay = 2;

                m_pOwner.Kill(target, dwDieWay);
            }

            return true;
        }

        public int CalcPower(int magic, Role attacker, Role target, ref InteractionEffect special, int nAdjustAtk = 0,
            bool bCanDodge = false)
        {
            if (target.Defense2 == 0 || m_pOwner.Map.IsLineSkillMap())
                return 1;

            if (attacker.QueryStatus(FlagInt.AUTO_HUNTING) != null && target is Monster monster && monster.IsBoss)
            {
                return 0;
            }

            int nPower = 0;
            if (magic == (int) MagicType.MagictypeNone)
                nPower += CalcAttackPower(attacker, target, ref special);
            else
            {
                nPower = CalcMagicPower(attacker, target, nAdjustAtk, ref special);
                if (magic == (int) MagicType.MagictypeXpskill)
                    if (target is Character)
                        nPower /= 50;
            }

            return nPower;
        }

        public int CalcAttackPower(Role attacker, Role target, ref InteractionEffect pSpecial)
        {
            int nAttack = 0;

            Character attackerUser = attacker as Character;
            Character targetUser = target as Character;

            if (targetUser != null && targetUser.QueryTransformation?.Lookface == 223)
                return 1;

            if (target.QueryStatus(FlagInt.VORTEX) != null)
                return 1;

            if (Calculations.ChanceCalc(50f))
                nAttack = attacker.MaxAttack -
                          ThreadSafeRandom.RandGet(1, Math.Max(1, attacker.MaxAttack - attacker.MinAttack) / 2 + 1);
            else
                nAttack = attacker.MinAttack +
                          ThreadSafeRandom.RandGet(1, Math.Max(1, attacker.MaxAttack - attacker.MinAttack) / 2 + 1);

            if (attackerUser != null && targetUser != null && (attackerUser.IsBowman || attackerUser.IsAssassin))
                nAttack = (int) (nAttack / 2.5);

            int nDefense = target.AdjustDefense(attacker);
            if (target.QueryStatus(FlagInt.DEFENSIVE_INSTANCE) != null)
            {
                nDefense = Calculations.AdjustData(nDefense, target.QueryStatus(FlagInt.DEFENSIVE_INSTANCE).Power);
            } 
            else if (target.QueryStatus(FlagInt.SHIELD) != null)
            {
                nDefense = Calculations.AdjustData(nDefense, target.QueryStatus(FlagInt.SHIELD).Power);
            }

            int nDamage = (int) (nAttack - nDefense);
            nDamage = (int)(nDamage * (1f - target.ReduceDamage / 100f));
            nDamage = (int)(nDamage * (1f - target.TortoiseGem));

            Monster targetMonster = target as Monster;
            if (attacker.QueryStatus(FlagInt.OBLIVION) != null
                && targetMonster != null && !targetMonster.IsBoss)
            {
                nDamage *= 2;
            }

            if (attacker.QueryStatus(FlagInt.FATAL_STRIKE) != null
                && !target.IsDynaNpc() && targetUser == null)
            {
                nDamage = Calculations.AdjustData(nDamage, attacker.QueryStatus(FlagInt.FATAL_STRIKE).Power);
            }

            if (attacker.QueryStatus(FlagInt.SUPERMAN) != null
                && targetUser == null && !target.IsDynaNpc())
            {
                nDamage = Calculations.AdjustData(nDamage, attacker.QueryStatus(FlagInt.SUPERMAN).Power);
            }

            if (attacker.QueryStatus(FlagInt.STIG) != null)
            {
                nDamage = Calculations.AdjustData(nDamage, attacker.QueryStatus(FlagInt.STIG).Power);
            }

            if (attacker.QueryStatus(FlagInt.INTENSIFY) != null)
            {
                nDamage = Calculations.AdjustData(nDamage, attacker.QueryStatus(FlagInt.INTENSIFY).Power);
            }

            if (attacker.Magics.QueryMagic() != null)
            {
                int power = attacker.Magics.QueryMagic().Power;

                if (attacker.QueryStatus(FlagInt.BLADE_FLURRY) != null
                    && attacker.Magics.QueryMagic().Status == FlagInt.BLADE_FLURRY
                    && target is Character)
                    power = Calculations.ChangeAdjustRate(power, 2);
                    

                nDamage = Calculations.AdjustData(nDamage, power);
            }

            if (nDamage <= 0)
                nDamage = 7;

            if (attacker is Character && target.IsMonster())
            {
                nDamage = CalcDamageUser2Monster(nDamage, nDefense, attacker.Level, target.Level);
                nDamage = target.AdjustWeaponDamage(nDamage);
                nDamage = AdjustMinDamageUser2Monster(nDamage, attacker, target);
            }
            else if (attacker.IsMonster() && target is Character)
            {
                nDamage = CalcDamageMonster2User(nDamage, nDefense, attacker.Level, target.Level);
                nDamage = target.AdjustWeaponDamage(nDamage);
                nDamage = AdjustMinDamageMonster2User(nDamage, attacker, target);
            }
            else
            {
                nDamage = attacker.AdjustWeaponDamage(nDamage);
            }

            if (attacker.IsPlayer() && attacker.BattlePower < target.BattlePower && targetUser != null)
            {
                if (target.Counteraction < attacker.Breakthrough &&
                    Calculations.ChanceCalc((float) (attacker.Breakthrough - target.Counteraction) / 10f))
                {
                    pSpecial |= InteractionEffect.Breakthrough;
                }
                else
                {
                    nDamage /= 2;
                }
            }

            if (attacker.CriticalStrike > target.Immunity &&
                Calculations.ChanceCalc((float) (attacker.CriticalStrike - target.Immunity) / 100f))
            {
                nDamage = (int) (nDamage * 1.5f);
                pSpecial |= InteractionEffect.CriticalStrike;
            }

            if (Calculations.ChanceCalc(target.Block / 100f))
            {
                nDamage = (int) (nDamage * 0.5d);
                pSpecial |= InteractionEffect.Block;
            }

            nDamage += attacker.FinalAttack;
            nDamage -= target.FinalDefense;

            if (target is Monster mob)
                nDamage = (int) Math.Min(mob.MaxLife * 700, nDamage);

            return Math.Max(1, nDamage);
        }

        public int CalcMagicPower(Role attacker, Role target, int nAdjustAtk, ref InteractionEffect pSpecial)
        {
            Character attackerUser = attacker as Character;
            Character targetUser = target as Character;

            if (targetUser != null && targetUser.QueryTransformation?.Lookface == 223)
                return 1;

            if (target.QueryStatus(FlagInt.VORTEX) != null)
                return 1;

            if (attacker is Monster guard && guard.IsGuard())
                return 80000;

            if (target.QueryStatus(FlagInt.MAGIC_DEFENDER) != null &&
                (attacker.BattlePower > target.BattlePower || target.BattlePower - attacker.BattlePower <= 20))
            {
                return 1;
            }

            if (target is Character pTgtUsr && pTgtUsr.Team != null)
            {
                if (pTgtUsr.Team.Members.Values.Any(x =>x.QueryStatus(FlagInt.MAGIC_DEFENDER) != null && x.GetDistance(target.MapX, target.MapY) <= 4))
                {
                    if (attacker.BattlePower <= target.BattlePower || attacker.BattlePower - target.BattlePower <= 20)
                        return 1;
                }
            }

            int nAttack = attacker.MagicAttack;

            int nDefense = target.AdjustMagicDefense(attacker);
            int nDefenseBonus = (int) (target.MagicDefenseBonus-attacker.Penetration/100f);

            nDefenseBonus = Math.Max(85, nDefenseBonus);

            int nDamage = (int) (nAttack - nDefense * (1 + (nDefenseBonus / 100f)));
            nDamage = (int) (nDamage * (1f - target.ReduceDamage/100f));
            nDamage = (int) (nDamage * (1f - target.TortoiseGem));

            if (attacker.Magics.QueryMagic() != null)
            {
                nDamage = Calculations.AdjustData(nDamage, attacker.Magics.QueryMagic().Power);
            }

            if (attacker is Character && target.IsMonster())
            {
                nDamage = CalcDamageUser2Monster(nDamage, nDefense, attacker.Level, target.Level);
                nDamage = target.AdjustMagicDamage(nDamage);
                nDamage = AdjustMinDamageUser2Monster(nDamage, attacker, target);
            }
            else if (attacker.IsMonster() && target is Character)
            {
                nDamage = CalcDamageMonster2User(nDamage, nDefense, attacker.Level, target.Level);
                nDamage = target.AdjustMagicDamage(nDamage);
                nDamage = AdjustMinDamageMonster2User(nDamage, attacker, target);
            }
            else
            {
                nDamage = target.AdjustMagicDamage(nDamage);
            }

            if (targetUser != null && attacker.BattlePower < target.BattlePower)
            {
                if (Calculations.ChanceCalc(target.Counteraction / 10f - attacker.Breakthrough / 10f))
                {
                    //nDamage = (int) (nDamage * 1.15f);
                    pSpecial |= InteractionEffect.Breakthrough;
                }
                else
                {
                    //int levelDiff = target.BattlePower - attacker.BattlePower;
                    //double disccount = Math.Max(50f, 100f-levelDiff)/100d;

                    //nDamage = (int) (nDamage * disccount);
                    nDamage /= 2;
                }
            }

            if (attacker.SkillCriticalStrike > target.Immunity &&
                Calculations.ChanceCalc((float) (attacker.SkillCriticalStrike - target.Immunity) / 100))
            {
                nDamage = (int) (nDamage * 2f);
                pSpecial |= InteractionEffect.CriticalStrike;
            }

            nDamage += attacker.FinalMagicAttack;
            nDamage -= target.FinalMagicDefense;
            
            if (target is Monster mob)
                nDamage = (int) Math.Min(mob.MaxLife * 700, nDamage);

            return Math.Max(1, nDamage);
        }

        public int TestAzureShield(int nDamage)
        {
            if (m_pOwner.QueryStatus(FlagInt.AZURE_SHIELD) != null)
            {
                var status = m_pOwner.QueryStatus(FlagInt.AZURE_SHIELD);
                int nReduceShield = Math.Min(status.Power, nDamage);
                nDamage = Math.Max(0, nDamage - nReduceShield);
            }
            return nDamage;
        }

        public int CheckAzureShield(int nDamage)
        {
            if (m_pOwner.QueryStatus(FlagInt.AZURE_SHIELD) != null)
            {
                var status = m_pOwner.QueryStatus(FlagInt.AZURE_SHIELD);
                int nReduceShield = Math.Min(status.Power, nDamage);
                nDamage = Math.Max(0, nDamage - nReduceShield);
                if (nDamage > 0) // shield is gone
                {
                    m_pOwner.DetachStatus(FlagInt.AZURE_SHIELD);
                }
                else
                {
                    status.ChangeData(status.Power - nReduceShield, status.RemainingTime);
                }

                m_pOwner.Map.SendToRegion(new MsgInteract
                {
                    EntityIdentity = m_pOwner.Identity,
                    TargetIdentity = m_pOwner.Identity,
                    CellX = m_pOwner.MapX,
                    CellY = m_pOwner.MapY,
                    Action = InteractionType.ACT_ITR_AZURE_DMG,
                    Data = (uint)nReduceShield
                }, m_pOwner.MapX, m_pOwner.MapY);
            }

            return nDamage;
        }

        public bool IsTargetDodged(Role attacker, Role attacked)
        {
            if (attacker == null || attacked == null) return true;

            int nDodge = 0;
            if (attacked is Character || attacked is DynamicNpc || attacked is Monster monster && monster.IsBoss)
                nDodge = 40;

            int atkHit = attacker.Accuracy;
            if (attacker.QueryStatus(FlagInt.STAR_OF_ACCURACY) != null)
                atkHit = Calculations.AdjustData(atkHit, attacker.QueryStatus(FlagInt.STAR_OF_ACCURACY).Power);

            int atkdDodge = attacked.Dodge;
            if (attacked.QueryStatus(FlagInt.DODGE) != null)
                atkHit = Calculations.AdjustData(atkHit, attacker.QueryStatus(FlagInt.DODGE).Power);

            int hitRate = Math.Min(100, Math.Max(30, atkHit - nDodge - atkdDodge));

            if (hitRate < 40 && attacker.IsPlayer() && attacked.IsPlayer())
                hitRate = 40;

#if DEBUG
            if (attacker is Character character)
                if (character.IsPm())
                    character.SendSysMessage("HitRate: " + hitRate);

            if (attacked is Character attackedUser)
                if (attackedUser.IsPm())
                    attackedUser.SendSysMessage("Attacker HitRate: " + hitRate);
#endif

            return !Calculations.ChanceCalc(hitRate);
        }

        public int AdjustDrop(int nDrop, int nAtkLev, int nDefLev)
        {
            if (nAtkLev > 120)
                nAtkLev = 120;

            if (nAtkLev - nDefLev > 0)
            {
                int nDeltaLev = nAtkLev - nDefLev;
                if (1 < nAtkLev && nAtkLev <= 19)
                {
                    if (nDeltaLev < 3)
                        ;
                    else if (3 <= nDeltaLev && nDeltaLev < 6)
                        nDrop = nDrop / 5;
                    else
                        nDrop = nDrop / 10;
                }
                else if (19 < nAtkLev && nAtkLev <= 49)
                {
                    if (nDeltaLev < 5)
                        ;
                    else if (5 <= nDeltaLev && nDeltaLev < 10)
                        nDrop = nDrop / 5;
                    else
                        nDrop = nDrop / 10;
                }
                else if (49 < nAtkLev && nAtkLev <= 85)
                {
                    if (nDeltaLev < 4)
                        ;
                    else if (4 <= nDeltaLev && nDeltaLev < 8)
                        nDrop = nDrop / 5;
                    else
                        nDrop = nDrop / 10;
                }
                else if (85 < nAtkLev && nAtkLev <= 112)
                {
                    if (nDeltaLev < 3)
                        ;
                    else if (3 <= nDeltaLev && nDeltaLev < 6)
                        nDrop = nDrop / 5;
                    else
                        nDrop = nDrop / 10;
                }
                else if (112 < nAtkLev && nAtkLev <= 120)
                {
                    if (nDeltaLev < 2)
                        ;
                    else if (2 <= nDeltaLev && nDeltaLev < 4)
                        nDrop = nDrop / 5;
                    else
                        nDrop = nDrop / 10;
                }
                else if (120 < nAtkLev && nAtkLev <= 130)
                {
                    if (nDeltaLev < 2)
                        ;
                    else if (2 <= nDeltaLev && nDeltaLev < 4)
                        nDrop = nDrop / 5;
                    else
                        nDrop = nDrop / 10;
                }
                else if (130 < nAtkLev && nAtkLev <= 140)
                {
                    if (nDeltaLev < 2)
                        ;
                    else if (2 <= nDeltaLev && nDeltaLev < 4)
                        nDrop = nDrop / 5;
                    else
                        nDrop = nDrop / 10;
                }
            }

            return Calculations.CutTrail(0, nDrop);
        }

        public int GetNameType(int nAtkLev, int nDefLev)
        {
            int nDeltaLev = nAtkLev - nDefLev;

            if (nDeltaLev >= 3)
                return NAME_GREEN;
            if (nDeltaLev >= 0)
                return NAME_WHITE;
            if (nDeltaLev >= -5)
                return NAME_RED;
            return NAME_BLACK;
        }

        public int CalcDamageUser2Monster(int nAtk, int nDef, int nAtkLev, int nDefLev)
        {
            int nDamage = nAtk - nDef;

            if (GetNameType(nAtkLev, nDefLev) != NAME_GREEN)
                return Calculations.CutTrail(0, nDamage);

            int nDeltaLev = nAtkLev - nDefLev;
            if (nDeltaLev >= 3
                && nDeltaLev <= 5)
                nAtk = (int) (nAtk * 1.5);
            else if (nDeltaLev > 5
                     && nDeltaLev <= 10)
                nAtk *= 2;
            else if (nDeltaLev > 10
                     && nDeltaLev <= 20)
                nAtk = (int) (nAtk * 2.5);
            else if (nDeltaLev > 20)
                nAtk *= 3;

            return Calculations.CutTrail(0, nAtk - nDef);
        }

        public int CalcDamageMonster2User(int nAtk, int nDef, int nAtkLev, int nDefLev)
        {
            if (nAtkLev > 120)
                nAtkLev = 120;

            int nDamage = nAtk; //(int) (nAtk - nDef*0.6);

            int nNameType = GetNameType(nAtkLev, nDefLev);

            if (nNameType == NAME_RED)
                nDamage = (int) (nAtk * 1.5f - nDef);
            else if (nNameType == NAME_BLACK)
            {
                int nDeltaLev = nDefLev - nAtkLev;
                if (nDeltaLev >= -10 && nDeltaLev <= -5)
                    nAtk *= 2;
                else if (nDeltaLev >= -20 && nDeltaLev < -10)
                    nAtk = (int) (nAtk * 3.5f);
                else if (nDeltaLev < -20)
                    nAtk *= 5;
                //nDamage = nAtk - nDef;
            }

            return Calculations.CutTrail(0, nDamage);
        }

        public int AdjustMinDamageUser2Monster(int nDamage, Role pAtker, Role pTarget)
        {
            int nMinDamage = 1;
            nMinDamage += pAtker.Level / 10;

            if (!(pAtker is Character))
                return Calculations.CutTrail(nMinDamage, nDamage);

            if (pAtker is Character pUser)
            {
                Item pItem = pUser.UserPackage[ItemPosition.RightHand];
                if (pItem != null)
                    nMinDamage += pItem.GetQuality();
            }

            return Calculations.CutTrail(nMinDamage, nDamage);
        }

        public int AdjustMinDamageMonster2User(int nDamage, Role pAtker, Role pTarget)
        {
            int nMinDamage = 7;

            if (nDamage >= nMinDamage
                || pTarget.Level <= 15)
                return nDamage;

            if (!(pTarget is Character pUser))
                return Calculations.CutTrail(nMinDamage, nDamage);

            //if (pTarget is Character pUser)
            {
                foreach (var item in pUser.UserPackage.GetEquipment())
                {
                    switch (item.Position)
                    {
                        case ItemPosition.Necklace:
                        case ItemPosition.Headwear:
                        case ItemPosition.Armor:
                            nMinDamage -= item.GetQuality() / 5;
                            break;
                    }
                }
            }

            nMinDamage = Calculations.CutTrail(1, nMinDamage);
            return Calculations.CutTrail(nMinDamage, nDamage);
        }

        public long AdjustExp(long nDamage, int nAtkLev, int nDefLev)
        {
            if (nAtkLev > 120)
                nAtkLev = 120;

            long nExp = nDamage;

            int nNameType = NAME_WHITE;
            int nDeltaLev = nAtkLev - nDefLev;
            if (nNameType == NAME_GREEN)
            {
                if (nDeltaLev >= 3 && nDeltaLev <= 5)
                    nExp = nExp * 70 / 100;
                else if (nDeltaLev > 5
                         && nDeltaLev <= 10)
                    nExp = nExp * 20 / 100;
                else if (nDeltaLev > 10
                         && nDeltaLev <= 20)
                    nExp = nExp * 10 / 100;
                else if (nDeltaLev > 20)
                    nExp = nExp * 5 / 100;
            }
            else if (nNameType == NAME_RED)
            {
                nExp = (int) (nExp * 1.3f);
            }
            else if (nNameType == NAME_BLACK)
            {
                if (nDeltaLev >= -10
                    && nDeltaLev < -5)
                    nExp = (int) (nExp * 1.5f);
                else if (nDeltaLev >= -20
                         && nDeltaLev < -10)
                    nExp = (int) (nExp * 1.8f);
                else if (nDeltaLev < -20)
                    nExp = (int) (nExp * 2.3f);
            }

            return Calculations.CutTrail(0, nExp);
        }

        public bool NextAttack(int nFightPause)
        {
            return m_tAttack.ToNextTime(nFightPause);
        }

        public void OtherMemberAwardExp(Role pRole, long nBonusExp)
        {
            if (m_pOwner.Map.IsTrainingMap())
                return;

            if (m_pOwner is Character pOwner)
                pOwner.Team?.AwardMemberExp(pOwner.Identity, pRole, nBonusExp);
        }

        public const int NAME_GREEN = 0,
            NAME_WHITE = 1,
            NAME_RED = 2,
            NAME_BLACK = 3;
    }

    public enum MagicTypeHandle
    {
        MagictypeNone = 0,
        MagictypeNormal = 1,
        MagictypeXpskill = 2
    }
}