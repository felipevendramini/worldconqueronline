﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Request Box.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Groups.Families;
using GameServer.Structures.Groups.Syndicates;

#endregion

namespace GameServer.Structures
{
    public interface IRequestBox
    {
        void OnOk(Character user);
        void OnCancel(Character user);
        void Send(Character user);
    }

    public sealed class RequestBox : IRequestBox
    {
        public uint OwnerIdentity { get; set; }

        public string OwnerName { get; set; }

        public uint ObjectIdentity { get; set; }

        public string ObjectName { get; set; }

        public RequestBoxType Type { get; set; }

        public string Message { get; set; }

        public void OnOk(Character pUsrAccept)
        {
            switch (Type)
            {
                case RequestBoxType.SyndicateAlly:
                {
                    Syndicate pAsk = ServerKernel.SyndicateManager.GetSyndicate(OwnerIdentity),
                        pAccept = ServerKernel.SyndicateManager.GetSyndicate(ObjectIdentity);
                    if (pAsk != null
                        && pAccept != null)
                    {
                        pAsk.AllySyndicate(pAccept);
                        pAsk.Send(string.Format(Language.StrSynAlly1, pAsk.LeaderName, pAccept.Name));
                        pAccept.Send(string.Format(Language.StrSynAlly0, pAccept.LeaderName, pAsk.Name));
                    }
                    else
                    {
                        OnCancel(pUsrAccept);
                    }
                    return;
                }
                case RequestBoxType.FamilyAlly:
                {
                    if (ServerKernel.Families.TryGetValue(OwnerIdentity, out Family pAsk)
                        && ServerKernel.Families.TryGetValue(ObjectIdentity, out Family pAccept))
                    {
                        pAsk.AllyFamily(pAccept);
                    }
                    return;
                }
            }
        }

        public void OnCancel(Character pUsrAccept)
        {
            switch (Type)
            {
                case RequestBoxType.SyndicateAlly:
                {
                    Syndicate pCancel = ServerKernel.SyndicateManager.GetSyndicate(ObjectIdentity);
                    if (pCancel != null)
                    {
                        pUsrAccept.SendSysMessage(string.Format(Language.StrSynAllyDeny));
                    }
                    return;
                }
                case RequestBoxType.FamilyAlly:
                {
                    if (!ServerKernel.Families.TryGetValue(ObjectIdentity, out Family familyCancel))
                    {
                        return;
                    }
                    pUsrAccept.SendSysMessage(Language.StrFamilyAllyDeny);
                    return;
                }
            }
        }

        public void Send(Character pTarget)
        {
            pTarget.Send(new MsgTaskDialog(MsgTaskDialog.MESSAGE_BOX, Message));
        }
    }

    public enum RequestBoxType
    {
        SyndicateAlly,
        FamilyAlly,
        AddMentor,
        AddApprentice
    }
}