﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Quiz Show.cs
// Last Edit: 2020/01/14 00:19
// Created: 2019/12/07 18:33
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.Linq;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Structures
{
    public sealed class QuizShow
    {
        public const ushort QUIZ_TIME_PER_QUESTION = 30; // in seconds
        public const ushort QUIZ_MAX_QUESTION = 20;
        public const ushort QUIZ_MAX_EXPERIENCE = 18000;
        public static readonly int[] QuizShowHour = {00, 04, 08, 12, 16, 20};
        public static readonly ushort[] QuizShowAward = {0, 60000, 45000, 30000};
        public static readonly ushort[] QuizShowEmoney = {0, 2150, 1075, 645};
        public static readonly uint[] QuizShowMoney = {0, 1500000, 1000000, 500000};

        private readonly TimeOut m_pNextQuestion = new TimeOut(QUIZ_TIME_PER_QUESTION);
        private readonly Dictionary<uint, QuizEntity> m_quizShow = new Dictionary<uint, QuizEntity>();

        private readonly Dictionary<uint, QuizShowUserObject> m_quizUserInformation =
            new Dictionary<uint, QuizShowUserObject>();

        private readonly List<QuizEntity> m_temporaryQuestions = new List<QuizEntity>(QUIZ_MAX_QUESTION);

        private int m_nActualQuestion;

        private QuizState m_state = QuizState.Stopped;

        public void Start()
        {
            var list = new QuizRepository().FetchAll();
            foreach (var obj in list)
            {
                m_quizShow.Add(obj.Identity, obj);
            }
        }

        public void ReloadQuestions()
        {
            if (m_state > QuizState.Stopped) return;

            m_quizShow.Clear();
            var list = new QuizRepository().FetchAll();
            foreach (var obj in list)
            {
                m_quizShow.Add(obj.Identity, obj);
            }
        }

        /// <summary>
        ///     Used when user logins into the server.
        /// </summary>
        public bool InsertPlayer(Character pRole)
        {
            QuizShowUserObject plrObj;
            if (!m_quizUserInformation.ContainsKey(pRole.Identity))
            {
                plrObj = new QuizShowUserObject
                {
                    Experience = 0,
                    Name = pRole.Name,
                    Points = 0,
                    TimeTaken = 0,
                    UserIdentity = pRole.Identity,
                    LastQuestion = 0
                };
                m_quizUserInformation.Add(pRole.Identity, plrObj);
            }
            else
            {
                plrObj = m_quizUserInformation[pRole.Identity];
            }

            if (m_state == QuizState.Starting)
            {
                // send initial packet
                var pMsg = new MsgQuiz
                {
                    Type = QuizShowType.START_QUIZ,
                    QuestionAmount = QUIZ_MAX_QUESTION,
                    TimePerQuestion = QUIZ_TIME_PER_QUESTION,
                    TimeTillStart = (ushort) (60 - DateTime.Now.Second),
                    FirstPrize = QuizShowAward[1],
                    SecondPrize = QuizShowAward[2],
                    ThirdPrize = QuizShowAward[3]
                };
                pRole.Send(pMsg);
            }
            else if (m_state == QuizState.Running)
            {
                var pMsg = new MsgQuiz
                {
                    Type = QuizShowType.START_QUIZ,
                    QuestionAmount = QUIZ_MAX_QUESTION,
                    TimePerQuestion = QUIZ_TIME_PER_QUESTION,
                    TimeTillStart = (ushort) m_pNextQuestion.GetRemain(),
                    FirstPrize = QuizShowAward[1],
                    SecondPrize = QuizShowAward[2],
                    ThirdPrize = QuizShowAward[3]
                };
                pRole.Send(pMsg);
                pMsg = new MsgQuiz
                {
                    Type = QuizShowType.AFTER_REPLY,
                    CurrentScore = plrObj.Points,
                    TimeTaken = plrObj.TimeTaken,
                    Rank = 19
                };
                pRole.Send(pMsg);
            }

            return true;
        }

        public bool UserAnswer(Character pRole, ushort nQuestion, ushort nReply)
        {
            try
            {
                if (m_state <= QuizState.Stopped) return false;

                QuizShowUserObject plrObj = null;

                if (!m_quizUserInformation.ContainsKey(pRole.Identity))
                {
                    plrObj = new QuizShowUserObject
                    {
                        Experience = 0,
                        Name = pRole.Name,
                        Points = 0,
                        TimeTaken = 0,
                        UserIdentity = pRole.Identity,
                        LastQuestion = nQuestion
                    };
                    m_quizUserInformation.Add(pRole.Identity, plrObj);
                }

                plrObj = m_quizUserInformation[pRole.Identity];

                if (plrObj.LastQuestion == nQuestion)
                    return false; // player already answered

                int expBallAmount = 0;
                var pQuestion = m_temporaryQuestions[nQuestion - 1];
                if (pQuestion.Correct == nReply)
                {
                    expBallAmount = QUIZ_MAX_EXPERIENCE / QUIZ_MAX_QUESTION;
                    pRole.AwardExperience(pRole.CalculateExpBall(expBallAmount));
                    plrObj.Points += (ushort) m_pNextQuestion.GetRemain();
                }
                else
                {
                    expBallAmount = QUIZ_MAX_EXPERIENCE / (QUIZ_MAX_QUESTION * 5);
                    pRole.AwardExperience(pRole.CalculateExpBall(expBallAmount));
                    plrObj.Points += 1;
                }

                plrObj.TimeTaken += (ushort) ((m_pNextQuestion.GetRemain() - QUIZ_TIME_PER_QUESTION) * -1);
                plrObj.Experience += (ushort) expBallAmount;
                plrObj.LastQuestion = nQuestion - 1;

                var pMsg = new MsgQuiz
                {
                    Type = QuizShowType.AFTER_REPLY,
                    CurrentScore = plrObj.Points,
                    TimeTaken = plrObj.TimeTaken,
                    Rank = plrObj.Rank,
                    LastCorrectAnswer = (ushort) (pQuestion.Correct == nReply ? 1 : 2)
                };
                var rank = RankingStrings();
                pMsg.AddString(rank[0].Name, rank[0].Points, rank[0].TimeTaken);
                pMsg.AddString(rank[1].Name, rank[1].Points, rank[1].TimeTaken);
                pMsg.AddString(rank[2].Name, rank[2].Points, rank[2].TimeTaken);
                pRole.Send(pMsg);
                return true;
            }
            catch
            {
                // should not happen
                Program.WriteLog("Could not add reward to user on Quiz Show", LogType.ERROR);
            }

            return false;
        }

        public void Cancel(uint idUser)
        {
            if (m_quizUserInformation.ContainsKey(idUser))
                m_quizUserInformation[idUser].Canceled = true;
        }

        public void OnTimer()
        {
            if (m_quizShow.Count < QUIZ_MAX_QUESTION) return; // no questions, no quiz

            DateTime now = DateTime.Now;

            if (QuizShowHour.Contains((now.Hour + 1) % 24))
            {
                if (now.Minute == 55
                    && now.Second == 0
                    && m_state < QuizState.Starting)
                {
                    ServerKernel.UserManager.SendToAllUser(Language.StrQuizShowAboutToStart);
                }

                // Quiz starting
                if (now.Minute == 59
                    && now.Second <= 1
                    && m_state < QuizState.Starting)
                {
                    if (now.DayOfWeek == DayOfWeek.Sunday
                        && now.Hour + 1 == 22)
                        return;

                    ReloadQuestions();

                    // reset basic variable
                    m_nActualQuestion = 0;
                    m_quizUserInformation.Clear();
                    m_temporaryQuestions.Clear();

                    // start the quiz
                    m_state = QuizState.Starting;
                    // and send the initial packet :)
                    var pMsg = new MsgQuiz
                    {
                        Type = QuizShowType.START_QUIZ,
                        TimeTillStart = (ushort) (60 - now.Second),
                        TimePerQuestion = QUIZ_TIME_PER_QUESTION,
                        QuestionAmount = QUIZ_MAX_QUESTION,
                        FirstPrize = QuizShowEmoney[1],
                        SecondPrize = QuizShowEmoney[2],
                        ThirdPrize = QuizShowEmoney[3]
                    };
                    // send to all players
                    foreach (var plr in ServerKernel.UserManager.Players.Values)
                    {
                        // create the user object that will be held by the server while it's alive
                        var plrObj = new QuizShowUserObject
                        {
                            Experience = 0,
                            Name = plr.Name,
                            Points = 0,
                            TimeTaken = 0,
                            UserIdentity = plr.Identity,
                            Canceled = false
                        };
                        m_quizUserInformation.Add(plr.Identity, plrObj); // save the info
                        plr.Send(pMsg); // send packet to client
                    }

                    // quiz will only happen if there is at least 20 questions
                    if (m_quizShow.Count > QUIZ_MAX_QUESTION)
                    {
                        List<KeyValuePair<uint, QuizEntity>> tempList = new List<KeyValuePair<uint, QuizEntity>>();
                        Random rand = new Random();

                        foreach (var question in m_quizShow.Values)
                        {
                            tempList.Add(new KeyValuePair<uint, QuizEntity>((uint) rand.Next(), question));
                        }

                        int num = 0;
                        foreach (var question in tempList.OrderBy(x => x.Key)
                            .Where(question => num++ < QUIZ_MAX_QUESTION))
                            m_temporaryQuestions.Add(question.Value);
                    }
                    else
                    {
                        if (m_quizShow.Count < QUIZ_MAX_QUESTION)
                        {
                            m_state = QuizState.Stopped;
                            return;
                        }

                        // we have exactly 20 questions :) so ok
                        foreach (var question in m_quizShow.Values)
                            m_temporaryQuestions.Add(question);
                    }

                    // send message to all (supposing they didn't receive the window lol)
                    ServerKernel.UserManager.SendToAllUser(Language.StrQuizShowStart);
                }
            }

            if (QuizShowHour.Contains(now.Hour) && now.Minute <= QUIZ_MAX_QUESTION * QUIZ_TIME_PER_QUESTION / 60)
            {
                // quiz started
                if (m_state == QuizState.Starting
                    && now.Minute == 0)
                {
                    m_state = QuizState.Running;
                    m_pNextQuestion.Startup(QUIZ_TIME_PER_QUESTION);

                    QuizEntity question = m_temporaryQuestions[m_nActualQuestion++];
                    var pMsg = new MsgQuiz
                    {
                        Type = QuizShowType.QUESTION_QUIZ,
                        QuestionNumber = (ushort) m_nActualQuestion,
                        LastCorrectAnswer = 0,
                        ExperienceAwarded = 1,
                        TimeTakenTillNow = 0,
                        CurrentScore = 0
                    };
                    pMsg.AddString(question.Question, question.Answer0, question.Answer1, question.Answer2,
                        question.Answer3);
                    foreach (var plr in ServerKernel.UserManager.Players.Values)
                        plr.Send(pMsg);
                }

                // quiz running
                if (m_state == QuizState.Running && m_pNextQuestion.ToNextTime() &&
                    m_nActualQuestion < QUIZ_MAX_QUESTION)
                {
                    foreach (var usr in m_quizUserInformation.Values)
                    {
                        if (usr.LastQuestion < m_nActualQuestion)
                        {
                            usr.Points += 1;
                            usr.TimeTaken += QUIZ_TIME_PER_QUESTION;
                        }
                    }

                    UpdateRanking();

                    QuizEntity question = m_temporaryQuestions[m_nActualQuestion++];
                    var pMsg = new MsgQuiz
                    {
                        Type = QuizShowType.QUESTION_QUIZ,
                        QuestionNumber = (ushort) m_nActualQuestion,
                        LastCorrectAnswer = m_temporaryQuestions[m_nActualQuestion - 2].Correct
                    };
                    pMsg.AddString(question.Question, question.Answer0, question.Answer1, question.Answer2,
                        question.Answer3);

                    foreach (var plr in ServerKernel.UserManager.Players.Values)
                    {
                        var plrObj = m_quizUserInformation.Values.FirstOrDefault(x => x.UserIdentity == plr.Identity);

                        if (plrObj == null)
                        {
                            plrObj = new QuizShowUserObject
                            {
                                Experience = 0,
                                Name = plr.Name,
                                Points = 0,
                                TimeTaken = 0,
                                UserIdentity = plr.Identity,
                                LastQuestion = 0
                            };
                        }

                        if (plrObj.Canceled)
                            continue;

                        if (plrObj.LastQuestion < m_nActualQuestion - 2) pMsg.LastCorrectAnswer = 0;
                        pMsg.CurrentScore = plrObj.Points;
                        pMsg.ExperienceAwarded = plrObj.Experience;
                        pMsg.TimeTakenTillNow = plrObj.TimeTaken;
                        plrObj.LastQuestion = m_nActualQuestion - 1;
                        plr.Send(pMsg);
                    }

                    if (m_nActualQuestion >= QUIZ_MAX_QUESTION)
                    {
                        m_state = QuizState.Ended;
                    }
                }

                if (m_state == QuizState.Ended
                    && m_pNextQuestion.ToNextTime())
                {
                    foreach (var usr in m_quizUserInformation.Values)
                    {
                        Character pUser = ServerKernel.UserManager.GetUser(usr.UserIdentity);
                        if (usr.LastQuestion < m_nActualQuestion)
                        {
                            usr.Points += 1;
                            usr.TimeTaken += QUIZ_TIME_PER_QUESTION;

                            if (pUser != null)
                            {
                                var pMsg = new MsgQuiz
                                {
                                    Type = QuizShowType.AFTER_REPLY,
                                    CurrentScore = usr.Points,
                                    TimeTaken = usr.TimeTaken,
                                    Rank = usr.Rank
                                };
                                var rank = RankingStrings();
                                pMsg.AddString(rank[0].Name, rank[0].Points, rank[0].TimeTaken);
                                pMsg.AddString(rank[1].Name, rank[1].Points, rank[1].TimeTaken);
                                pMsg.AddString(rank[2].Name, rank[2].Points, rank[2].TimeTaken);
                                pUser.Send(pMsg);
                            }
                        }

                        if (pUser != null)
                        {
                            try
                            {
                                pUser.StudentPoints += usr.Points;
                                int i = 0;
                                foreach (var tmp in m_quizUserInformation.Values.OrderByDescending(x => x.Points))
                                {
                                    if (i++ > 3) break;
                                    if (tmp.UserIdentity == usr.UserIdentity)
                                    {
                                        long amount = pUser.CalculateExpBall(QuizShowAward[i]);
                                        pUser.AwardExperience(amount);

                                        ushort emoney = QuizShowEmoney[i];
                                        uint money = QuizShowMoney[i];
                                        pUser.AwardEmoney(emoney, EmoneySourceType.Quiz, null);
                                        pUser.AwardMoney((int) money);
                                        pUser.SendSysMessage(string.Format(Language.StrQuizShowAnnounceReward, emoney,
                                            i, money));
                                    }
                                }

                                MsgQuiz pMsg = new MsgQuiz
                                {
                                    Type = QuizShowType.FINISH_QUIZ,
                                    Score = usr.Rank,
                                    Rank = usr.TimeTaken,
                                    FirstPrize = usr.Points,
                                    FinalPrize = usr.Experience
                                };
                                QuizShowUserObject[] pList = RankingStrings();
                                pMsg.AddString(pList[0].Name, pList[0].Points, pList[0].TimeTaken);
                                pMsg.AddString(pList[1].Name, pList[1].Points, pList[1].TimeTaken);
                                pMsg.AddString(pList[2].Name, pList[2].Points, pList[2].TimeTaken);
                                pUser.Send(pMsg);
                            }
                            catch
                            {
                            }
                        }
                        else
                        {
                            try
                            {
                                // disconnected? still have prize to claim
                                CharacterEntity dbObj = new CharacterRepository().SearchByIdentity(usr.UserIdentity);
                                if (dbObj == null) continue;

                                Character pTemp = new Character(dbObj.Identity, null, dbObj);
                                pTemp.StudentPoints += usr.Points;

                                int i = 1;
                                foreach (var tmp in m_quizUserInformation.Values.OrderByDescending(x => x.Points))
                                {
                                    if (i++ > 3) break;
                                    if (tmp.UserIdentity == usr.UserIdentity)
                                    {
                                        pTemp.AwardExperience(pTemp.CalculateExpBall(QuizShowAward[tmp.Rank - 1]));
                                    }
                                }

                                pTemp.Save();
                                pTemp = null;
                            }
                            catch
                            {
                            }
                        }
                    }

                    ServerKernel.UserManager.SendToAllUser(Language.StrQuizShowEnded);
                    m_state = QuizState.Stopped;
                }
            }
        }

        public void UpdateRanking()
        {
            ushort rank = 1;
            foreach (var obj in m_quizUserInformation.Values.OrderByDescending(x => x.Points))
                obj.Rank = rank++;
        }

        private QuizShowUserObject[] RankingStrings()
        {
            UpdateRanking();
            QuizShowUserObject[] ret = new QuizShowUserObject[3];
            for (int i = 0; i < 3; i++)
                ret[i] = new QuizShowUserObject();
            int nCount = 0;
            foreach (var usr in m_quizUserInformation.Values.OrderByDescending(x => x.Points))
            {
                if (nCount >= 3) break;
                ret[nCount++] = usr;
            }

            return ret;
        }

        private enum QuizState : byte
        {
            Stopped = 0,
            Starting = 1,
            Running = 2,
            Ended = 3
        }
    }

    public class QuizShowUserObject
    {
        public QuizShowUserObject()
        {
            UserIdentity = 0;
            Name = "None";
            Points = 0;
            Experience = 0;
            TimeTaken = 0;
            LastQuestion = 0;
            Rank = ushort.MaxValue;
        }

        public uint UserIdentity { get; set; }
        public string Name { get; set; }
        public ushort Points { get; set; }
        public ushort Experience { get; set; } // 600 = 1 expball
        public ushort TimeTaken { get; set; } // in seconds
        public int LastQuestion { get; set; }
        public ushort Rank { get; set; }
        public bool Canceled { get; set; }
    }
}