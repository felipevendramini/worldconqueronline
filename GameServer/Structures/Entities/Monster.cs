﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Monster.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Networking.Packets;
using GameServer.Structures.Items;
using GameServer.World;

#endregion

namespace GameServer.Structures.Entities
{
    public sealed class Monster : Role
    {
        public const int MODE_DORMANCY = 0,
            MODE_IDLE = 1,
            MODE_WALK = 2,
            MODE_GOTO = 3,
            MODE_TALK = 4,
            MODE_FOLLOW = 5,
            MODE_ESCAPE = 6,
            MODE_FORWARD = 7,
            MODE_ATTACK = 8,
            WALKMODE_MOVE = 0,
            WALKMODE_RUN = 1,
            WALKMODE_SHIFT = 2,
            WALKMODE_JUMP = 3,
            WALKMODE_RUN_DIR0 = 20,
            WALKMODE_RUN_DIR7 = 27;

        private MonstertypeEntity m_dbMonstertypeEntity;
        private Generator m_pGenerator;
        private MsgPlayer m_pMonster;
        private SpecialDrop m_pDrop;
        private Magic m_pDefaultMagic;

        private List<MonsterMagicEntity> m_dbMonsterMagics = new List<MonsterMagicEntity>();

        private int m_nActMode = MODE_IDLE;
        private uint m_dwLookface;
        private ushort m_usUseMagicType;
        private bool m_bAttackFlag;
        private bool m_bAheadPath;
        private int m_nNextDir = -1;
        private uint m_idActTarget;
        private uint m_idMoveTarget;
        private uint m_idAtkMe;
        private bool m_bAtkFirst;

        private Role m_pRoleTarget;

        private TimeOut m_tDisappear;
        private TimeOut m_tLocked;
        private TimeOutMS m_tStatusCheck;
        private TimeOutMS m_tAttackMs;
        private TimeOutMS m_tAction;
        private TimeOutMS m_tMoveMs;

        public Monster(MonstertypeEntity entity, uint idRole, Generator generator) 
            : base(idRole)
        {
            m_dbMonstertypeEntity = entity;
            m_pGenerator = generator;

            m_pMonster = new MsgPlayer(idRole);
        }

        ~Monster()
        {
            try
            {
                // the identity must go back (:
                /**
                 * This must be a problem if the world contains more than 100k monsters.
                 * Monster Identity now are shared between all maps.
                 * It also makes it easier to find roles around the world, but we have now a limitation.
                 */
                m_pGenerator.Collection.TryRemove(Identity, out _);
                ServerKernel.RoleManager.MonsterIdentity.ReturnIdentity(Identity);
            }
            catch
            {

            }
        }

        public bool Initialize(uint idMap, ushort x, ushort y)
        {
            m_idMap = idMap;
            if (!ServerKernel.Maps.TryGetValue(m_idMap, out m_pMap))
                return false;

            m_pMonster.Name = Name;
            m_pMonster.IsBoss = m_dbMonstertypeEntity.Boss != 0;

            Lookface = m_dbMonstertypeEntity.Lookface;
            Level = (byte) m_dbMonstertypeEntity.Level;
            MapX = x;
            MapY = y;
            Life = (uint) m_dbMonstertypeEntity.Life;

            Magics = new MagicData(this);
            m_pDrop = ServerKernel.SpecialDrop.FirstOrDefault(z => z.MonsterIdentity == m_dbMonstertypeEntity.Id);
            if (m_dbMonstertypeEntity.MagicType > 0)
            {
                m_pDefaultMagic = new Magic(this);
                if (!m_pDefaultMagic.Create(m_dbMonstertypeEntity.MagicType))
                    m_pDefaultMagic = null;
                else
                {
                    Magics.Magics.TryAdd(m_pDefaultMagic.Type, m_pDefaultMagic);
                }
            }

            var mgcList = ServerKernel.MonsterMagics.Where(z => z.OwnerIdentity == m_dbMonstertypeEntity.Id);
            foreach (var mgc in mgcList)
            {
                m_dbMonsterMagics.Add(mgc);
            }

            m_tLocked = new TimeOut(0);
            m_tDisappear = new TimeOut(5);
            m_tStatusCheck = new TimeOutMS(1000);
            m_tAction = new TimeOutMS(0);
            m_tMoveMs = new TimeOutMS(m_dbMonstertypeEntity.MoveSpeed);
            m_tAttackMs = new TimeOutMS(m_dbMonstertypeEntity.AttackSpeed);
            return true;
        }

        #region Properties

        public override int MinAttack => m_dbMonstertypeEntity.AttackMin;
        public override int MaxAttack => m_dbMonstertypeEntity.AttackMax;
        public override int Defense => m_dbMonstertypeEntity.Defence;
        public override int Defense2 => (int) m_dbMonstertypeEntity.Defence2;
        public override int MagicDefense => m_dbMonstertypeEntity.MagicDef;
        public override int MagicAttack => m_dbMonstertypeEntity.AttackMax;


        public override string Name => m_dbMonstertypeEntity?.Name ?? "";

        public override uint Lookface
        {
            get => m_dwLookface;
            set => m_dwLookface = m_pMonster.Mesh = value;
        }

        public override uint Life
        {
            get => m_pMonster.Life;
            set => m_pMonster.Life = value;
        }

        public override uint MaxLife => (uint) m_dbMonstertypeEntity.Life;

        public override byte Level
        {
            get => m_pMonster.MonsterLevel;
            set => m_pMonster.MonsterLevel = value;
        }

        public override bool IsMonster()
        {
            return true;
        }

        public int ViewRange => m_dbMonstertypeEntity.ViewRange;

        public override ulong Flag1
        {
            get => base.Flag1;
            set
            {
                base.Flag1 = value;
                m_pMonster.Flag1 = value;
            }
        }

        public override ulong Flag2
        {
            get => base.Flag2;
            set
            {
                base.Flag2 = value;
                m_pMonster.Flag2 = value;
            }
        }

        public override uint Flag3
        {
            get => base.Flag3;
            set
            {
                base.Flag3 = value;
                m_pMonster.Flag3 = value;
            }
        }

        public bool DisappearNow = false;

        #region Position

        public override ushort MapX
        {
            get => m_usMapX;
            set => m_usMapX = m_pMonster.MapX = value;
        }

        public override ushort MapY
        {
            get => m_usMapY;
            set => m_usMapY = m_pMonster.MapY = value;
        }

        public bool IsBoss
        {
            get => m_pMonster.IsBoss;
            set => m_pMonster.IsBoss = value;
        }

        public uint AttackUser => m_dbMonstertypeEntity?.AttackUser ?? 0u;

        public uint Type => m_dbMonstertypeEntity?.Id ?? 0u;

        #endregion

        #endregion

        #region Map

        public override void EnterMap()
        {
            if (Map != null)
            {
                Map.EnterRoom(this);
                Map.SendToRegion(new MsgAction(Identity, 0, MapX, MapY,
                    GeneralActionType.SpawnEffect)
                {
                    Direction = FacingDirection.West
                }, MapX, MapY);
            }
        }

        public override void LeaveMap()
        {
            Map?.LeaveRoom(Identity);
        }

        #endregion

        #region Battle

        public override bool BeAttack(int magic, Role attacker, int nPower, bool bReflectEnable)
        {
            if (!IsAlive)
                return false;

            AddAttrib(ClientUpdateType.Hitpoints, nPower * -1);

            if (!IsAlive && !m_tDisappear.IsActive())
            {
                BeKill(attacker);
                return true;
            }

            if (m_tDisappear.IsActive())
                return false;

            return true;
        }

        public override void BeKill(Role attacker)
        {
            // todo pet handle

            if (m_tDisappear.IsActive())
                return;

            DetachAllStatus();
            AttachStatus(attacker, FlagInt.DEAD, 0, 20, 0, 0);
            AttachStatus(attacker, FlagInt.GHOST, 0, 20, 0, 0);
            AttachStatus(attacker, FlagInt.FADE, 0, 20, 0, 0);

            Character pActionUser = attacker as Character;

            uint dieType = pActionUser?.KoCount * 65541 ?? 1;
            var msg = new MsgInteract
            {
                Action = InteractionType.ACT_ITR_KILL,
                EntityIdentity = attacker?.Identity ?? 0,
                TargetIdentity = Identity,
                CellX = MapX,
                CellY = MapY,
                MagicType = 0,
                MagicLevel = 0,
                Data = dieType
            };

            Map.SendToRegion(msg, MapX, MapY);

            m_tDisappear.Startup(5);

            Life = 0;

            uint idMapItemOwner = 0;
            if (pActionUser != null)
                idMapItemOwner = pActionUser.Identity;

            if (attacker?.BattleSystem.IsActive() == true)
                attacker.BattleSystem.ResetBattle();

            if (IsDieAction())
                GameAction.ProcessAction(m_dbMonstertypeEntity.Action, pActionUser, this, null, null);

            if (m_pDrop != null)
            {
                for (int i = 0; i < m_pDrop.DropNum; i++)
                {
                    bool bExe = false;
                    foreach (var dropRule in m_pDrop.Actions.OrderBy(x => x.Value))
                    {
                        if (Calculations.ChanceCalc(dropRule.Value / 100f))
                        {
                            GameAction.ProcessAction(dropRule.Key, pActionUser, this, null, null);
                            bExe = true;
                            break;
                        }
                    }
                    if (!bExe && m_pDrop.DefaultAction > 0)
                        GameAction.ProcessAction(m_pDrop.DefaultAction, pActionUser, this, null, null);
                }
            }

            if (IsGuard() || IsBoss)
                return;

            int nChanceAdjust = 25;
            if (pActionUser != null && GetNameType(pActionUser.Level, Level) == BattleSystem.NAME_GREEN)
                nChanceAdjust = 7;

            if (Calculations.ChanceCalc(nChanceAdjust))
            {
                int dwMoneyMin = (int)(m_dbMonstertypeEntity.DropMoney * 0.85f);
                int dwMoneyMax = (int)(m_dbMonstertypeEntity.DropMoney * 1.15f);
                uint dwMoney = (uint)(dwMoneyMin + Calculations.Random.Next(dwMoneyMax - dwMoneyMin) + 1);

                int nHeapNum = 1 + Calculations.Random.Next(3);
                uint dwMoneyAve = (uint)(dwMoney / nHeapNum);

                for (int i = 0; i < nHeapNum; i++)
                {
                    uint dwMoneyTmp = (uint)Calculations.MulDiv((int)dwMoneyAve, 90 + Calculations.Random.Next(21), 100);

#if !DEBUG
                    if ((pActionUser?.QueryStatus(FlagInt.AUTO_HUNTING) != null && pActionUser.VipLevel > 1) || pActionUser?.VipLevel > 3)
                        pActionUser.AwardMoney((int)dwMoneyTmp);
                    else
                        DropMoney(dwMoneyTmp, idMapItemOwner);
#else
                    if (pActionUser?.QueryStatus(FlagInt.AUTO_HUNTING) != null)
                    {
                        pActionUser.AwardMoney((int)dwMoneyTmp*50);
                    }
                    else if (pActionUser != null)
                    {
                        pActionUser.AwardMoney((int)dwMoneyTmp*100);
                    }
                    else
                    {
                        DropMoney(dwMoneyTmp, idMapItemOwner);
                    }
#endif
                }
            }

            int nDropNum = 0;
            int nAtkLev = Level;
            if (pActionUser != null)
                nAtkLev = pActionUser.Level;

            int nRate = Calculations.Random.Next(1000);

            float dropRate = .15f;
            if (Calculations.ChanceCalc(dropRate))
            {
                if (pActionUser?.QueryStatus(FlagInt.AUTO_HUNTING) == null)
                {
                    if (pActionUser != null)
                        ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrDragonBallDrop, pActionUser.Name));
                    DropItem(SpecialItem.TYPE_DRAGONBALL, idMapItemOwner, 0, 0, 0, 0);
                }
            }

            pActionUser?.AwardEmoney(10, EmoneySourceType.Drop, this);

            if (Calculations.ChanceCalc(0.015f)) // Fire of hell
                DropItem(1060101, idMapItemOwner, 0, 0, 0, 0);

            if (Calculations.ChanceCalc(0.025f)) // BombScroll
                DropItem(1060100, idMapItemOwner, 0, 0, 0, 0);

            if (Calculations.ChanceCalc(0.5f))
                DropItem(SpecialItem.TYPE_METEOR, idMapItemOwner, 0, 0, 0, 0); // meteor

            if (Calculations.ChanceCalc(.04f))
                DropItem(SpecialItem.OBLIVION_DEW, idMapItemOwner, 0, 0, 0, 0);

            if (Calculations.ChanceCalc(.04f))
                DropItem(720598, idMapItemOwner, 0, 0, 0, 0);
            
            if (!IsPkKiller() && !IsGuard() && !IsEvilKiller() && !IsDynaMonster() && !IsDynaNpc())
            {
                if (Calculations.ChanceCalc(.1f))
                {
                    uint dGem = m_pNormalGem[ThreadSafeRandom.RandGet(m_pNormalGem.Length) % m_pNormalGem.Length];
                    DropItem(dGem, idMapItemOwner, 0, 0, 0, 0); // normal gems
                }
            }

            if ((m_dbMonstertypeEntity.Id == 15 || m_dbMonstertypeEntity.Id == 74) && Calculations.ChanceCalc(2f))
            {
                DropItem(1080001, idMapItemOwner, 0, 0, 0, 0); // emerald
            }

            int nChance = BattleSystem.AdjustDrop(100, nAtkLev, Level);
            if (nRate < Math.Min(1000, nChance))
            {
                nDropNum = 1 + Calculations.Random.Next(1, 3); // drop 10-16 items
            }
            else
            {
                nChance += BattleSystem.AdjustDrop(500, nAtkLev, Level);
                if (nRate < Math.Min(1000, nChance))
                    nDropNum = 1; // drop 1 item
            }

            for (int i = 0; i < nDropNum; i++)
            {
                uint idItemtype = GetDropItem();

                ItemtypeEntity itemtype;
                if (!ServerKernel.Itemtype.TryGetValue(idItemtype, out itemtype))
                    continue;

                byte nDmg = 0;
                byte nPlus = 0;

                if (Calculations.ChanceCalc(.5f))
                {
                    // bless
                    nDmg = (byte)(Calculations.ChanceCalc(10) ? 5 : 3);
                }
                if (Calculations.ChanceCalc(.3f))
                {
                    // plus
                    nPlus = 1;
                }

                if (!DropItem(itemtype, idMapItemOwner, nPlus, nDmg, 0, (short)Calculations.Random.Next(-200, 300)))
                    break;
            }
        }

        private readonly uint[] m_pNormalGem = { 700001, 700011, 700021, 700031, 700041, 700051, 700061, 700071, 700101, 7000121 };

        public bool DropItem(uint idItemtype, uint idOwner, byte nMagic3, byte nBless, byte nEnchant,
            short sExtraDura)
        {
            return ServerKernel.Itemtype.TryGetValue(idItemtype, out var db) && DropItem(db, idOwner, nMagic3, nBless, nEnchant, sExtraDura);
        }

        public bool DropItem(ItemtypeEntity idItemtype, uint idOwner, byte nMagic3, byte nBless, byte nEnchant, short sExtraDura)
        {
            if (idItemtype == null) return true;

            var pos = new Point(MapX, MapY);
            if (Map.FindDropItemCell(4, ref pos))
            {
                var pMapItem = new MapItem((uint)ServerKernel.RoleManager.FloorIdentity.GetNextIdentity);
                if (pMapItem.Create(Map, pos, idItemtype, idOwner, nMagic3, nBless, sExtraDura))
                {
                    return true;
                }
            }
            return false;
        }

        public bool DropMoney(uint dwMoney, uint idOwner)
        {
            if (dwMoney <= 0)
                return false;

            if (Level >= 80 && dwMoney < 10)
                return true;

            var pos = new Point(MapX, MapY);
            if (Map.FindDropItemCell(2, ref pos))
            {
                var pMapItem = new MapItem((uint) ServerKernel.RoleManager.FloorIdentity.GetNextIdentity);
                if (pMapItem.CreateMoney(Map, pos, dwMoney, idOwner))
                {
                    return true;
                }
            }

            return false;
        }

        private readonly int[] m_dropHeadgear = { 111000, 112000, 113000, 114000, 143000, 118000, 123000, 141000, 142000, 117000 };
        private readonly int[] m_dropNecklace = { 120000, 121000 };
        private readonly int[] m_dropArmor = { 130000, 131000, 133000, 134000, 135000, 136000, 139000 };
        private readonly int[] m_dropRing = { 150000, 151000, 152000 };
        private readonly int[] m_dropWeapon =
        {
            410000, 420000, 421000, 430000, 440000, 450000, 460000, 480000, 481000,
            490000, 500000, 510000, 530000, 540000, 560000, 561000, 580000, 601000, 
            610000, 611000, 612000, 613000
        };

        public uint GetDropItem()
        {
            /*
             * 0 = armet
             * 1 = necklace
             * 2 = armor
             * 3 = ring
             * 4 = weapon
             * 5 = shield
             * 6 = shoes
             * 7 = hp
             * 8 = mp
             */
            var possibleDrops = new List<int>();
            if (m_dbMonstertypeEntity.DropArmet != 0)
                possibleDrops.Add(0);
            if (m_dbMonstertypeEntity.DropNecklace != 0)
                possibleDrops.Add(1);
            if (m_dbMonstertypeEntity.DropArmor != 0)
                possibleDrops.Add(2);
            if (m_dbMonstertypeEntity.DropRing != 0)
                possibleDrops.Add(3);
            if (m_dbMonstertypeEntity.DropWeapon != 0)
                possibleDrops.Add(4);
            if (m_dbMonstertypeEntity.DropShield != 0)
                possibleDrops.Add(5);
            if (m_dbMonstertypeEntity.DropShoes != 0)
                possibleDrops.Add(6);
            if (m_dbMonstertypeEntity.DropHp != 0)
                possibleDrops.Add(7);
            if (m_dbMonstertypeEntity.DropMp != 0)
                possibleDrops.Add(8);

            if (possibleDrops.Count <= 0)
                return 0;

            int type = possibleDrops[Calculations.Random.Next(possibleDrops.Count) % possibleDrops.Count];
            uint dwItemId = 0;

            switch (type)
            {
                case 0:
                    dwItemId += (uint)m_dropHeadgear[Calculations.Random.Next(m_dropHeadgear.Length - 1)];
                    dwItemId += (uint)(m_dbMonstertypeEntity.DropArmet * 10);
                    break;
                case 1:
                    dwItemId += (uint)m_dropNecklace[Calculations.Random.Next(m_dropNecklace.Length - 1)];
                    dwItemId += (uint)(m_dbMonstertypeEntity.DropNecklace * 10);
                    break;
                case 2:
                    dwItemId += (uint)m_dropArmor[Calculations.Random.Next(m_dropArmor.Length - 1)];
                    dwItemId += (uint)(m_dbMonstertypeEntity.DropArmor * 10);
                    break;
                case 3:
                    dwItemId += (uint)m_dropRing[Calculations.Random.Next(m_dropRing.Length - 1)];
                    dwItemId += (uint)(m_dbMonstertypeEntity.DropRing * 10);
                    break;
                case 4:
                    dwItemId += (uint)m_dropWeapon[Calculations.Random.Next(m_dropWeapon.Length - 1)];
                    dwItemId += (uint)(m_dbMonstertypeEntity.DropWeapon * 10);
                    break;
                case 5:
                    dwItemId += 900000;
                    dwItemId += (uint)(m_dbMonstertypeEntity.DropShield * 10);
                    break;
                case 6:
                    dwItemId += 160000;
                    dwItemId += (uint)(m_dbMonstertypeEntity.DropShoes * 10);
                    break;
                case 7:
                    return m_dbMonstertypeEntity.DropHp;
                case 8:
                    return m_dbMonstertypeEntity.DropMp;
                default:
                    return 0;
            }

            uint nNewLev = (uint)(((dwItemId % 100) / 10) + Calculations.Random.Next(-2, 2));

            switch (type)
            {
                case 0:
                case 2:
                case 5:
                    if (nNewLev > 0 && nNewLev <= 10)
                        dwItemId += (nNewLev) * 10;
                    break;
                case 1:
                case 3:
                case 6:
                    if (nNewLev > 0 && nNewLev <= 24)
                        dwItemId += (nNewLev) * 10;
                    break;
                case 4:
                    if (nNewLev > 0 && nNewLev <= 33)
                        dwItemId += (nNewLev) * 10;
                    break;
            }

            if (Calculations.ChanceCalc(0.01f)) dwItemId += 9; // super
            else if (Calculations.ChanceCalc(0.05f)) dwItemId += 8; // elite
            else if (Calculations.ChanceCalc(0.1f)) dwItemId += 7; // unique
            else if (Calculations.ChanceCalc(0.2f)) dwItemId += 6; // refined
            else if (Calculations.ChanceCalc(0.1f) && type == 4) dwItemId += 0; // fixed
            else dwItemId += (uint)Calculations.Random.Next(3, 5); // normal

            return dwItemId;
        }

        public bool IsDieAction()
        {
            return m_dbMonstertypeEntity.Action != 0;
        }

#endregion

#region Battle Checks

        public override bool IsEvil() { return (AttackUser & (4 | 8192)) == 0; }

        public bool IsDisappear() => m_tDisappear.IsTimeOut(10);

        public bool IsOpposedSyn(uint pRoleId)
        {
            if (!IsSynPet()) return false;
            return false;
        }

        public bool IsSynPet()
        {
            return false;
        }

        public bool IsLockUser()
        {
            return (AttackUser & 256) != 0;
        }

        public bool IsRighteous()
        {
            return (AttackUser & 4) != 0;
        }

        public override bool IsGuard()
        {
            return (AttackUser & 8) != 0;
        }

        public bool IsPkKiller()
        {
            return (AttackUser & 16) != 0;
        }

        public bool IsWalkEnable()
        {
            return (AttackUser & 64) == 0;
        }

        public bool IsJumpEnable()
        {
            return (AttackUser & 32) != 0;
        }

        public bool IsFastBack()
        {
            return (AttackUser & 128) != 0;
        }

        public bool IsLockOne()
        {
            return (AttackUser & 512) != 0;
        }

        public bool IsAddLife()
        {
            return (AttackUser & 1024) != 0;
        }

        public bool IsEvilKiller()
        {
            return (AttackUser & 2048) != 0;
        }

        public bool IsDormancyEnable()
        {
            return (AttackUser & 256) == 0;
        }

        public bool IsDynaMonster()
        {
            return false;
        }

        public int GetNameType(int nAtkLev, int nDefLev)
        {
            int nDeltaLev = nAtkLev - nDefLev;

            if (nDeltaLev >= 3)
                return BattleSystem.NAME_GREEN;
            if (nDeltaLev >= 0)
                return BattleSystem.NAME_WHITE;
            if (nDeltaLev >= -5)
                return BattleSystem.NAME_RED;
            return BattleSystem.NAME_BLACK;
        }

        public bool IsMoveEnable()
        {
            return (!m_tLocked.IsActive() || m_tLocked.IsTimeOut()) && (IsWalkEnable() || IsJumpEnable());
        }

#endregion

#region AI

        public override int GetAttackRange(int nTargetSizeAdd)
        {
            return m_dbMonstertypeEntity.AttackRange + nTargetSizeAdd;
        }

        public bool IsCloseAttack()
        {
            return !IsBowman || m_pDefaultMagic != null || m_dbMonsterMagics.Count > 0;
        }

        public bool IsAttackEnable(Role pTarget)
        {
            if (pTarget.IsWing && !IsWing && IsCloseAttack())
                return false;
            return true;
        }

        public bool FindNewTarget()
        {
            if (Map == null) return false;

            // lock target
            if (IsLockUser() || IsLockOne())
            {
                if (CheckTarget())
                {
                    if (IsLockOne())
                        return true;
                }
                else
                {
                    if (IsLockUser())
                        return false;
                }
            }

            uint idOldTarget = m_idActTarget;
            m_idActTarget = 0;
            m_idMoveTarget = 0;
            Character pTargetUser = null;
            int nDistance = m_dbMonstertypeEntity.ViewRange;

            List<Role> poss = null;

            if (IsRighteous())
            {
                poss = Map.CollectMapThing(ViewRange, new Point(MapX, MapY));
            }
            else
            {
                poss = Map.CollectMapUser(ViewRange, new Point(MapX, MapY)).Cast<Role>().ToList();
            }

            if (poss == null || poss.Count <= 0)
                return false;

            foreach (var role in poss)
            {
                if (role is Character pRoleUser)
                {
                    if (pRoleUser.IsAlive && pRoleUser.Identity != Identity && pRoleUser.QueryStatus(FlagInt.INVISIBLE) == null)
                    {
                        if ((IsGuard() && pRoleUser.IsCrime())
                            || (IsPkKiller() && pRoleUser.IsPker())
                            || (IsEvilKiller() && pRoleUser.IsVirtuous())
                            || (IsEvil() && !(IsPkKiller() || IsEvilKiller()))
                            || (IsSynPet() && IsOpposedSyn(pRoleUser.Identity)))
                        {
                            if (!IsAttackEnable(pRoleUser)) continue;
                            int nDist = GetDistance(pRoleUser.MapX, pRoleUser.MapY);

                            if (nDist <= nDistance)
                            {
                                nDistance = nDist;

                                m_idActTarget = pRoleUser.Identity;
                                m_idMoveTarget = pRoleUser.Identity;
                                m_pRoleTarget = pRoleUser;

                                pTargetUser = pRoleUser;
                            }
                        }
                    }
                }
                else if (role is Monster pNpc)
                {
                    if (pNpc.IsAlive && pNpc.Identity != Identity
                    && (IsEvil() && pNpc.IsRighteous()
                        || IsRighteous() && pNpc.IsEvil()))
                    {
                        if (pNpc.IsWing && !IsWing) continue;

                        int nDist = GetDistance(pNpc.MapX, pNpc.MapY);
                        if (nDist <= nDistance)
                        {
                            nDistance = nDist;
                            m_idActTarget = pNpc.Identity;
                            m_idMoveTarget = m_idActTarget;
                            m_pRoleTarget = pNpc;
                        }
                    }
                }
            }

            if (m_idActTarget != 0)
            {
                if (pTargetUser != null && idOldTarget != pTargetUser.Identity)
                {
                    if (IsGuard() && pTargetUser.IsCrime())
                    {
                        // send guard message
                    }
                    else if (IsPkKiller() && pTargetUser.IsPker() && m_nActMode == MODE_IDLE)
                    {
                        // send guard message
                    }
                }

                FindPath();
                return m_idActTarget != 0;
            }

            m_nNextDir = -1;
            return false;
        }

        public void SetPos(int nPosX, int nPosY)
        {
            MapX = (ushort)nPosX;
            MapY = (ushort)nPosY;
        }

        public bool FindPath(ushort x, ushort y)
        {
            if (x == MapX && y == MapY)
                return false;

            int nDir = Calculations.GetDirection(MapX, MapY, x, y);

            Point pos = new Point(MapX, MapY);

            for (int i = 0; i < 8; i++)
            {
                nDir += i;
                if (TestPath(nDir))
                {
                    m_nNextDir = nDir;
                    break;
                }
            }

            return m_nNextDir >= 0;
        }

        public bool FindPath(int nScapeSteps = 0)
        {
            if (Map == null || m_idMoveTarget == 0) return false;

            m_bAheadPath = (nScapeSteps == 0);
            ClearPath();

            var pRole = Map.QueryRole(m_idMoveTarget) as Role;
            if (pRole == null || !pRole.IsAlive || GetDistance(pRole.MapX, pRole.MapY) > ViewRange)
            {
                m_idActTarget = 0;
                m_idMoveTarget = m_idActTarget;
                ClearPath();
                return m_nNextDir < 0;
            }

            if (!FindPath(pRole.MapX, pRole.MapY))
                return false;

            if (m_nNextDir >= 0)
            {
                int nDir = m_nNextDir % 8;
                if (!Map.IsMoveEnable(MapX, MapY, nDir, 0, 0) && m_pMap[m_usMapX, m_usMapY].Access > TileType.Monster)
                {
                    DetectPath(nDir);
                    return m_nNextDir >= 0;
                }

            }

            return m_nNextDir >= 0;
        }

        public bool CheckMagicAttack()
        {
            m_usUseMagicType = 0;

            if (m_pRoleTarget == null)
                return false;

            if (Magics.Magics.Count > 0)
            {
                foreach (var magic in m_dbMonsterMagics.OrderBy(x => x.Chance))
                {
                    if (Magics.CheckType(magic.MagicIdentity) && Calculations.ChanceCalc(magic.Chance / 100f))
                    {
                        m_usUseMagicType = magic.MagicIdentity;
                        return true;
                    }
                }
            }
            return false;
        }

        public bool ProcessAttack() // felipe apr/10/2015
        {
            if (m_pRoleTarget != null)
            {
                if (m_pRoleTarget.IsAlive
                    && (CheckMagicAttack()
                    || GetDistance(m_pRoleTarget.MapX, m_pRoleTarget.MapY) <= GetAttackRange(m_pRoleTarget.SizeAddition)))
                {
                    if (m_tAction.ToNextTime())
                    {
                        if (Map.IsSuperPosition(MapX, MapY))
                        {
                            m_bAheadPath = false;
                            DetectPath(-1);
                            m_bAheadPath = true;
                            if (m_nNextDir >= 0) PathMove(WALKMODE_SHIFT);
                        }

                        if (m_bAttackFlag)
                            m_bAttackFlag = false;
                        else
                        {
                            ChangeMode(MODE_FORWARD);
                            return true;
                        }
                    }
                    return true;
                }
            }

            // no target
            ChangeMode(MODE_IDLE);
            return true;
        }

        public bool ProcessForward()
        {
            // test attack enable
            //IRole pRole = m_pRoleTarget ?? Map.QueryRole(m_idMoveTarget) as IRole;
            if (m_pRoleTarget != null)
            {
                int nDistance = GetDistance(m_pRoleTarget.MapX, m_pRoleTarget.MapY);
                if (CheckMagicAttack() || m_pRoleTarget.IsAlive && nDistance <= GetAttackRange(m_pRoleTarget.SizeAddition))
                {
                    if (!IsGuard() && !IsMoveEnable() && !m_bAheadPath && m_nNextDir >= 0)
                    {
                        if (PathMove(WALKMODE_RUN)) return true;
                    }

                    ChangeMode(MODE_ATTACK);
                    return true;
                }
            }

            // process forward
            if (IsGuard() || IsPkKiller() || IsFastBack() && m_pGenerator.IsTooFar(MapX, MapY, 48))
            {
                Point pos = m_pGenerator.GetCenter();
                m_idActTarget = 0;
                m_idMoveTarget = m_idActTarget;
                m_pRoleTarget = null;
                FarJump(pos.X, pos.Y, (int)Direction);
                ClearPath();
                ChangeMode(MODE_IDLE);
                return true;
            }

            // guard jump
            if ((IsGuard() || IsPkKiller() || IsEvilKiller()) && m_pRoleTarget != null &&
                GetDistance(m_pRoleTarget.MapX, m_pRoleTarget.MapY) >= 6)
            {
                JumpPos(m_pRoleTarget.MapX, m_pRoleTarget.MapY, (int)Direction);
                return true;
            }

            // forward
            if (m_nNextDir < 0)
            {
                if (FindNewTarget())
                {
                    if (m_nNextDir < 0)
                    {
                        if (IsJumpEnable())
                        {
                            JumpBlock(m_pRoleTarget.MapX, m_pRoleTarget.MapY, (int)Direction);
                            return true;
                        }
                        ChangeMode(MODE_IDLE);
                        return true;
                    }
                    return false;
                }
                ChangeMode(MODE_IDLE);
                return true;
            }

            //if (m_idMoveTarget != 0)
            if (m_pRoleTarget != null)
            {
                if (m_pRoleTarget != null && GetDistance(m_pRoleTarget.MapX, m_pRoleTarget.MapY) <= ViewRange)
                {
                    FindPath();
                    m_tAttackMs.Update();
                    PathMove(WALKMODE_RUN);
                }
                else
                {
                    //m_idMoveTarget = 0;
                    m_pRoleTarget = null;
                }
            }
            else
            {
                //PathMove(WALKMODE_MOVE);
                ChangeMode(MODE_IDLE);
            }
            return true;
        }

        /// <summary>
        /// This process the monster when it's stand still and doesn't have a target. This method will lookup
        /// through the screen and will look out for targets.
        /// </summary>
        /// <returns></returns>
        public bool ProcessIdle()
        {
            // change to other mode
            m_idAtkMe = 0;
            m_bAtkFirst = false;
            if (FindNewTarget()) // find a new target to attack
            {
                if (m_pRoleTarget == null) return false;

                int nDistance = GetDistance(m_pRoleTarget.MapX, m_pRoleTarget.MapY);
                if (nDistance <= GetAttackRange(m_pRoleTarget.SizeAddition))
                {
                    ChangeMode(MODE_ATTACK);
                    return false;
                }
                if (IsMoveEnable())
                {
                    if (m_nNextDir < 0)
                    {
                        if (!IsEscapeEnable() || Calculations.ChanceCalc(80f)) return true;

                        ChangeMode(MODE_ESCAPE);
                        return false;
                    }
                    ChangeMode(MODE_FORWARD);
                    return false;
                }
            }

            if (IsGuard() || IsPkKiller() || IsEvilKiller())
            {
                if (m_nNextDir < 0)
                {
                    PathMove(WALKMODE_MOVE);
                    return true;
                }

                if (m_tAction.ToNextTime())
                    return true;
            }

            if (!IsMoveEnable())
                return true;

            if (m_pGenerator.IsInRegion(MapX, MapY))
            {
                if (IsGuard() || IsPkKiller() || IsEvilKiller())
                {
                    if (Calculations.Random.Next(75) == 5
                        && m_pGenerator.GetWidth() > 1 || m_pGenerator.GetHeight() > 1)
                    {
                        int x = Calculations.Random.Next(m_pGenerator.GetWidth() + m_pGenerator.GetPosX());
                        int y = Calculations.Random.Next(m_pGenerator.GetHeight() + m_pGenerator.GetPosY());

                        if (FindPath((ushort)x, (ushort)y))
                            PathMove(WALKMODE_MOVE);
                    }
                }
                else
                {
                    if (Calculations.Random.Next(50) == 0)
                    {
                        int nDir = Calculations.Random.Next(0, 7);
                        if (TestPath(nDir))
                            PathMove(WALKMODE_MOVE);

                    }
                }
            }
            else // out of range, move back
            {
                if ((IsGuard() || IsPkKiller() || IsFastBack()) || Calculations.Random.Next(5) == 0)
                {
                    if (!m_pGenerator.IsInRegion(MapX, MapY))
                    {
                        Point pos = m_pGenerator.GetCenter();
                        if (FindPath((ushort)pos.X, (ushort)pos.Y))
                            PathMove(WALKMODE_MOVE);
                        else if (/*IsGuard() || IsPkKiller() || */IsJumpEnable())
                            JumpBlock(pos.X, pos.Y, (int)Direction);
                    }
                }
            }

            return true;
        }

        public bool ProcessEscape()
        {
            if (!IsEscapeEnable())
            {
                ChangeMode(MODE_IDLE);
                return true;
            }

            //IRole pRole = m_pRoleTarget ?? Map.QueryRole(m_idActTarget) as IRole;
            if ((IsGuard() || IsPkKiller()) && m_pRoleTarget != null)
            {
                JumpPos(m_pRoleTarget.MapX, m_pRoleTarget.MapY, (int)Direction);
                ChangeMode(MODE_FORWARD);
                return true;
            }

            if (m_nNextDir < 0)
                FindPath(ViewRange * 2);

            if (m_pRoleTarget == null)
            {
                ChangeMode(MODE_IDLE);
                return true;
            }

            if (m_pRoleTarget != null && m_nNextDir < 0)
            {
                ChangeMode(MODE_FORWARD);
                return true;
            }
            PathMove(WALKMODE_RUN);
            return true;
        }

        public bool IsEscapeEnable()
        {
            return true;
        }

        public bool ChangeMode(int nNewMode)
        {
            switch (nNewMode)
            {
                case MODE_DORMANCY:
                    Life = (uint)m_dbMonstertypeEntity.Life;
                    break;
                case MODE_ATTACK:
                    Attack();
                    break;
            }
            if (nNewMode != MODE_FORWARD)
                ClearPath();
            m_nActMode = nNewMode;
            return true;
        }

        public bool CheckTarget()
        {
            if (m_pRoleTarget == null || !m_pRoleTarget.IsAlive)
            {
                m_idActTarget = 0;
                m_idMoveTarget = 0;
                return false;
            }

            if (m_pRoleTarget.IsWing && !IsWing && !IsCloseAttack())
                return false;

            int nDistance = ViewRange;
            int nAtkDistance = Calculations.CutOverflow(nDistance, GetAttackRange(0));
            int nDist = GetDistance(m_pRoleTarget.MapX, m_pRoleTarget.MapY);

            if (!(nDist <= nDistance) || (nDist <= nAtkDistance && GetAttackRange(0) > 1))
            {
                m_idActTarget = 0;
                m_idMoveTarget = 0;
                return false;
            }

            return true;
        }

        public void Attack()
        {
            if (m_pRoleTarget == null || !m_tAttackMs.ToNextTime())
                return;

            if (m_dbMonstertypeEntity.MagicType <= 0)
            {
                BattleSystem.CreateBattle(m_pRoleTarget.Identity);
                BattleSystem.ProcessAttack();
            }
            else
            {
                if (m_usUseMagicType > 0)
                {
                    ProcessMagicAttack(m_usUseMagicType, m_pRoleTarget.Identity, m_pRoleTarget.MapX, m_pRoleTarget.MapY);
                }
                else if (m_pDefaultMagic != null)
                    ProcessMagicAttack((ushort)m_dbMonstertypeEntity.MagicType, m_pRoleTarget.Identity, m_pRoleTarget.MapX, m_pRoleTarget.MapY);
            }

            if (m_idAtkMe == m_idActTarget)
                m_bAtkFirst = true;

            m_tAttackMs.Update();
        }

        public override int Attack(Role pTarget, ref InteractionEffect special)//, ref List<InteractionEffect> pEffects)
        {
            if (pTarget == null || pTarget.Identity == Identity)
                return 0;

            int nTempAdjust = 0;
            int nDamage = BattleSystem.CalcPower(0, this, pTarget, ref special, nTempAdjust, true);
            return nDamage;
        }

#endregion

#region Movement

        public bool PathMove(int nMode)
        {
            if (nMode == 0)
            {
                if (!m_tMoveMs.ToNextTime(m_dbMonstertypeEntity.MoveSpeed))
                    return true;
            }
            else
            {
                if (!m_tMoveMs.ToNextTime((int)(m_dbMonstertypeEntity.RunSpeed)))
                    return true;
            }

            int nDir = (int)(m_nNextDir % 8);

            if (MoveFoward(nDir, nMode))
                return true;

            if (DetectPath(nDir) && m_nNextDir >= 0)
            {
                return MoveFoward(nDir, nMode);
            }

            if (IsJumpEnable())
            {
                var pos = m_pGenerator.GetCenter();
                return JumpBlock(pos.X, pos.Y, (int)Direction);
            }

            return false;
        }

        private void ClearPath()
        {
            m_nNextDir = -1;
        }

        private bool DetectPath(int nNoDir)
        {
            ClearPath();

            var posTarget = new Point();
            if (m_pRoleTarget != null)
            {
                posTarget.X = m_pRoleTarget.MapX;
                posTarget.Y = m_pRoleTarget.MapY;
            }
            else if (m_idMoveTarget != 0)
            {
                if (Map.QueryRole(m_idMoveTarget) is Role pRole)
                {
                    posTarget.X = pRole.MapX;
                    posTarget.Y = pRole.MapY;
                }
            }
            else
            {
                posTarget = m_pGenerator.GetCenter();
            }

            int nDistOld = GetDistance((ushort)posTarget.X, (ushort)posTarget.X);
            int nBestDist = nDistOld;
            int nBestDir = -1; // -1: invalid dir
            int nFirstDir = Calculations.Random.Next(8);

            for (int i = nFirstDir; i < 8; i++)
            {
                int nDir = nFirstDir % 8;
                if (nDir != nNoDir)
                {
                    if (Map != null && Map.IsMoveEnable(MapX, MapY, nDir, 0, 0))
                    {
                        ushort nNewX = (ushort)(MapX + DeltaX[nDir]);
                        ushort nNewY = (ushort)(MapY + DeltaY[nDir]);
                        int nDist = GetDistance(nNewX, nNewY);
                        if ((nBestDist - nDist) * (m_bAheadPath ? 1 : -1) > 0)
                        {
                            nBestDist = nDist;
                            nBestDir = nDir;
                        }
                    }
                }
            }

            if (nBestDir != -1)
            {
                m_nNextDir = nBestDir;
                return true;
            }

            return false;
        }

        public bool TestPath(int nDir)
        {
            if (nDir < 0 || nDir > 7) return false;

            if (Map != null && Map.IsMoveEnable(MapX, MapY, nDir, 0, 0))
            {
                m_nNextDir = nDir;
                return true;
            }

            return false;
        }

        public bool MoveFoward(int nDir, int nMode = 0)
        {
            if (nDir < 0) return false;

            Direction = (FacingDirection)(nDir % 8);

            ushort nNewPosX = (ushort)(MapX + DeltaX[nDir]);
            ushort nNewPosY = (ushort)(MapY + DeltaY[nDir]);

            if (!Map.IsValidPoint(nNewPosX, nNewPosY))
                return false;

            ScreenObject obj = Map.RoleSet.Values.FirstOrDefault(x => x.MapX == nNewPosX && x.MapY == nNewPosY);
            if (Map.IsMoveEnable(MapX, MapY, nDir, 0, 0) && obj == null)
            {
                Map.SendToRegion(new MsgWalk((uint)nDir, Identity, (MovementType)nMode, MapIdentity), MapX, MapY);
                MapX = nNewPosX;
                MapY = nNewPosY;
                return true;
            }

            return false;
        }

        public bool JumpBlock(int x, int y, int nDir)
        {
            int nSteps = (int)GetDistance((ushort)x, (ushort)y);

            if (nSteps <= 0) return false;

            for (int i = 1; i < nSteps; i++)
            {
                Point pos = new Point()
                {
                    X = MapX + (x - MapX) * i / nSteps,
                    Y = MapY + (y - MapY) * i / nSteps
                };

                if (Map.IsStandEnable((ushort)pos.X, (ushort)pos.Y))
                {
                    JumpPos(pos.X, pos.Y, nDir);
                    return true;
                }
            }

            if (Map.IsStandEnable((ushort)x, (ushort)y))
            {
                JumpPos(x, y, nDir);
                return true;
            }

            return false;
        }

        public int GetDistance(ushort x, ushort y)
        {
            return (int)Calculations.GetDistance(MapX, MapY, x, y);
        }

        public void JumpPos(int x, int y, int nDir)
        {
            if (!Map.IsValidPoint(x, y))
                return;

            Map.SendToRegion(new MsgAction(Identity, (ushort)x, (ushort)y, GeneralActionType.Jump), MapX, MapY);

            ClearPath();
            MapX = (ushort)x;
            MapY = (ushort)y;
            Direction = (FacingDirection)(nDir % 8);
        }

        public bool FarJump(int x, int y, int nDir)
        {
            int nSteps = GetDistance(x, y);

            if (nSteps <= 0) return true;

            if (Map.IsStandEnable((ushort)x, (ushort)y))
            {
                JumpPos(x, y, nDir);
                return true;
            }

            for (int i = 1; i < nSteps; i++)
            {
                var pos = new Point
                {
                    X = x + (MapX - x) * i / nSteps,
                    Y = y + (MapY - y) * i / nSteps
                };
                if (Map.IsStandEnable((ushort)pos.X, (ushort)pos.Y))
                {
                    JumpPos(pos.X, pos.Y, nDir);
                    return true;
                }
            }

            return false;
        }

#endregion

        public void LeaveGenerator()
        {
            DisappearNow = true;
            Life = 0;
        }

        public override void OnTimer()
        {
            if (!IsAlive)
                return;

            if (m_tStatusCheck.ToNextTime())
            {
                Status.CheckDeadMark();

                if (Status.Status.Count <= 0)
                    return;

                foreach (var stts in Status.Status.Values)
                {
                    stts.OnTimer();
                    if (!stts.IsValid && stts.Identity != FlagInt.GHOST && stts.Identity != FlagInt.DEAD)
                    {
                        Status.DelObj(stts.Identity);
                    }
                }
            }

            try
            {
                if (!IsAlive || (m_tLocked.IsActive() && m_tLocked.IsTimeOut()))
                    return;
                switch (m_nActMode)
                {
                    case MODE_ESCAPE:
                        ProcessEscape();
                        return;
                    case MODE_ATTACK:
                        ProcessAttack();
                        return;
                    case MODE_FORWARD:
                        ProcessForward();
                        return;
                    case MODE_IDLE:
                        ProcessIdle();
                        return;
                }
            }
            catch (Exception ex)
            {
                ServerKernel.Log.SaveLog(ex.ToString(), LogType.EXCEPTION);
            }
        }

        public override void SendSpawnTo(Character user)
        {
            if (!IsAlive)
                return;
            user.Send(m_pMonster);
        }
    }
}