﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Dynamic Npc.cs
// Last Edit: 2020/01/16 00:41
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Concurrent;
using System.Globalization;
using System.Linq;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures.Groups.Syndicates;

#endregion

namespace GameServer.Structures.Entities
{
    public class DynamicNpc : BaseNpc
    {
        private readonly MsgNpcInfoEx m_pPacket;
        private readonly DynamicNpcEntity m_dbNpc;

        private readonly TimeOut m_tCheckEvent = new TimeOut(1);
        private readonly TimeOut m_tSaveTime = new TimeOut(60);
        private readonly TimeOut m_tUpdateRank = new TimeOut(10);
        private readonly TimeOutMS m_tDeathTime = new TimeOutMS(5000);
        private readonly TimeOut m_tStatusCheck = new TimeOut(1);

        public ConcurrentDictionary<uint, NpcScore> Scores = new ConcurrentDictionary<uint, NpcScore>();

        private uint m_dwLife;

        public DynamicNpc(DynamicNpcEntity npc)
            : base(npc.Id)
        {
            m_dbNpc = npc;
            m_idMap = npc.Mapid;
            m_usMapX = npc.Cellx;
            m_usMapY = npc.Celly;
            m_pPacket = new MsgNpcInfoEx
            {
                Flag = npc.Type,
                Identity = npc.Id,
                Lookface = npc.Lookface,
                Life = npc.Life,
                MaxLife = npc.Maxlife,
                MapX = npc.Cellx,
                MapY = npc.Celly,
                Type = npc.Type
            };
        }

        public override string Name
        {
            get => m_pPacket.Name;
            set => m_pPacket.Name = value;
        }

        public override uint OwnerIdentity
        {
            get => m_dbNpc.Ownerid;
            set
            {
                m_dbNpc.Ownerid = value;
                Save();
            }
        }

        public override uint OwnerType
        {
            get => m_dbNpc.Ownertype;
            set
            {
                m_dbNpc.Ownertype = value;
                Save();
            }
        }

        public override uint Life
        {
            get => m_dwLife;
            set
            {
                m_dwLife = value;
                m_pPacket.Life = value;
                m_dbNpc.Life = value;
            }
        }

        public override uint MaxLife => m_dbNpc?.Maxlife ?? 0;

        public override ushort Type => m_dbNpc.Type;

        public override uint Lookface
        {
            get => m_dbNpc.Lookface;
            set
            {
                m_dbNpc.Lookface = (ushort) value;
                m_pPacket.Lookface = (ushort) value;
                Save();
            }
        }

        public bool Initialize()
        {
            if (m_dbNpc == null)
                return false;

            if (IsUserNpc() && OwnerIdentity > 0) // player
            {
                Character user = ServerKernel.UserManager.GetUser(OwnerIdentity);
                if (user != null)
                {
                    Name = user.Name;
                }
                else
                {
                    CharacterEntity dbUser = new CharacterRepository().SearchByIdentity(OwnerIdentity);
                    if (dbUser != null)
                        Name = dbUser.Name;
                }
            }

            if (IsSynNpc() && OwnerIdentity > 0 && (IsSynFlag() || Type == 26)) // syndicate
            {
                Syndicate syndicate = ServerKernel.SyndicateManager.GetSyndicate(OwnerIdentity);
                if (syndicate != null)
                {
                    Name = syndicate.Name;
                }
            }

            Life = MaxLife;

            m_idMap = m_dbNpc.Mapid;
            MapX = m_dbNpc.Cellx;
            MapY = m_dbNpc.Celly;
            return true;
        }

        public void SetMaxLife(uint value)
        {
            if (Life < value) Life = value;
            m_dbNpc.Maxlife = value;
            m_pPacket.MaxLife = value;
        }

        #region Packet

        public override void SendSpawnTo(Character user)
        {
            user.Send(m_pPacket);
        }

        #endregion

        public bool SetOwnerIdentity(uint idOwner)
        {
            if (idOwner == 0)
            {
                OwnerIdentity = idOwner;
                Name = "";
                return true;
            }

            if (IsUserNpc())
            {
                uint idSet = 0;
                string name = string.Empty;
                Character user = ServerKernel.UserManager.GetUser(idOwner);
                if (user == null)
                {
                    CharacterEntity dbUser = Database.CharacterRepository.SearchByIdentity(idOwner);
                    if (dbUser != null)
                        name = dbUser.Name;
                }
                else
                {
                    name = user.Name;
                }

                OwnerIdentity = idSet;
                Name = name;
            }
            else if (IsSynNpc())
            {
                Syndicate owner = ServerKernel.SyndicateManager.GetSyndicate(idOwner);
                if (owner == null)
                    return false;

                OwnerIdentity = owner.Identity;
                if (IsSynFlag())
                {
                    Name = owner.Name;
                    SendCtfRanking(null);
                }
            }
            else
            {
                OwnerIdentity = idOwner;
            }

            Save();

            Map.SendToRegion(m_pPacket, MapX, MapY);
            return true;
        }

        public override bool DelNpc()
        {
            SetAttrib(ClientUpdateType.Hitpoints, 0);
            m_tDeathTime.Update();

            if (IsSynFlag())
            {
                Map.SetStatus(1, false);
                // todo
            }
            else if (!IsGoal())
            {
                new DynamicNpcRepository().Delete(m_dbNpc);
            }

            ServerKernel.RoleManager.RemoveRole(Identity);
            return true;
        }

        public override void OnTimer()
        {
            if (m_tCheckEvent.ToNextTime())
                CheckFightTime();

            if (m_tUpdateRank.ToNextTime())
                SendOwnerRanking();

            if (m_tSaveTime.ToNextTime())
                Save();

            if (m_tStatusCheck.ToNextTime())
            {
                Status.CheckDeadMark();
            }
        }

        #region Dynamic Npc

        public bool IsGoal()
        {
            return Type == 21 || Type == 22;
        }

        public bool IsAwardScore()
        {
            return m_dbNpc.Type == SYNFLAG_NPC || m_dbNpc.Type == ROLE_CTFBASE_NPC;
        }

        #endregion

        #region Task and Data

        public override int Data0
        {
            get => m_dbNpc.Data0;
            set
            {
                m_dbNpc.Data0 = value;
                Save();
            }
        }

        public override int Data1
        {
            get => m_dbNpc.Data1;
            set
            {
                m_dbNpc.Data1 = value;
                Save();
            }
        }

        public override int Data2
        {
            get => m_dbNpc.Data2;
            set
            {
                m_dbNpc.Data2 = value;
                Save();
            }
        }

        public override int Data3
        {
            get => m_dbNpc.Data3;
            set
            {
                m_dbNpc.Data3 = value;
                Save();
            }
        }

        public override uint Task0 => m_dbNpc.Task0;
        public override uint Task1 => m_dbNpc.Task1;
        public override uint Task2 => m_dbNpc.Task2;
        public override uint Task3 => m_dbNpc.Task3;
        public override uint Task4 => m_dbNpc.Task4;
        public override uint Task5 => m_dbNpc.Task5;
        public override uint Task6 => m_dbNpc.Task6;
        public override uint Task7 => m_dbNpc.Task7;

        #endregion

        #region Database

        public bool Save()
        {
            return new DynamicNpcRepository().Save(m_dbNpc);
        }

        public bool Delete()
        {
            LeaveMap();
            return new DynamicNpcRepository().Delete(m_dbNpc);
        }

        #endregion

        #region Map

        public override ushort MapX
        {
            get => m_usMapX;
            set => m_dbNpc.Cellx = m_usMapX = m_pPacket.MapX = value;
        }

        public override ushort MapY
        {
            get => m_usMapY;
            set => m_dbNpc.Celly = m_usMapY = m_pPacket.MapY = value;
        }

        public override void EnterMap()
        {
            if (ServerKernel.Maps.TryGetValue(MapIdentity, out m_pMap))
            {
                Map.EnterRoom(this);
            }
        }

        public override void LeaveMap()
        {
            Map?.LeaveRoom(Identity);
        }

        #endregion

        #region War

        public void CheckFightTime()
        {
            if (!IsSynFlag())
                return;

            if (Data1 == 0 || Data2 == 0)
                return;

            string strNow = "";
            DateTime now = DateTime.Now;
            strNow += ((int) now.DayOfWeek == 0 ? 7 : (int) now.DayOfWeek).ToString(CultureInfo.InvariantCulture);
            strNow += now.Hour.ToString("00");
            strNow += now.Minute.ToString("00");
            strNow += now.Second.ToString("00");

            int now0 = int.Parse(strNow);
            if (now0 < Data1 || now0 >= Data2)
            {
                if (Map.IsWarTime())
                    OnFightEnd();
                return;
            }

            if (!Map.IsWarTime())
            {
                Map.SetStatus(1, true);

                Map.SendMessageToMap(Language.StrSynWarStart, ChatTone.System);
            }
        }

        public void OnFightEnd()
        {
            Map.SetStatus(1, false);
            Map.SendMessageToMap(Language.StrSynWarEnded, ChatTone.System);

            if (IsSynFlag())
            {
                Syndicate syn = ServerKernel.SyndicateManager.GetSyndicate(OwnerIdentity);
                if (syn != null)
                {
                    new SyndicateWarRepository().Save(new SyndicateWarEntity
                    {
                        Date = DateTime.Now,
                        LeaderIdentity = syn.LeaderIdentity,
                        LeaderName = syn.LeaderName,
                        SyndicateIdentity = syn.Identity,
                        SyndicateName = syn.Name
                    });
                }
            }

            foreach (var usr in Map.PlayerSet.Values.Where(x => x.BattleSystem != null))
            {
                usr.SetAttackTarget(null);
                usr.BattleSystem.DestroyAutoAttack();
            }
        }

        #endregion

        #region Battle

        public void SendOwnerRanking()
        {
            int i = 0;

            if (!IsAwardScore() || !IsAttackable(null) || IsCtfFlag())
                return;

            foreach (var client in ServerKernel.Maps[MapIdentity].PlayerSet.Values)
            {
                if (IsAttackable(null))
                {
                    var nChannel = ChatTone.EventRanking;
                    if (IsSynFlag())
                    {
                        client.SendMessage("Guild War Scores", nChannel);
                        nChannel = ChatTone.EventRankingNext;
                        client.SendMessage("------------------------", nChannel);
                    }

                    foreach (var syn in Scores.Values.OrderByDescending(x => x.Score))
                        if (i++ < 5)
                        {
                            client.SendMessage($"Nº{i}: {syn.Name} - {syn.Score}", nChannel);
                            nChannel = ChatTone.EventRankingNext;
                        }
                }

                i = 0;
            }
        }

        public void SendCtfRanking(Character target)
        {
            if (!IsAwardScore() || !IsAttackable(null))
                return;

            MsgWarFlag msg = new MsgWarFlag
            {
                Type = WarFlagType.WarFlagBaseRank
            };
            uint i = 0;
            foreach (var score in Scores.Values.OrderByDescending(x => x.Score))
            {
                if (i++ >= 5)
                    break;
                msg.Append(i, (uint) score.Score, score.Name);
            }

            if (target == null)
            {
                foreach (var player in m_pMap.PlayerSet.Values.Where(x =>
                    Calculations.GetDistance(MapX, MapY, x.MapX, x.MapY) < 10))
                {
                    player.Send(msg);
                }
            }
            else
            {
                if (Calculations.GetDistance(MapX, MapY, target.MapX, target.MapY) < 10)
                {
                    target.Send(msg);
                }
            }
        }

        public bool IsDieAction()
        {
            return m_dbNpc.Linkid != 0;
        }

        public override bool BeAttack(int bMagic, Role pRole, int nPower, bool bReflectEnable)
        {
            AddAttrib(ClientUpdateType.Hitpoints, nPower * -1);
            if (IsDynaNpc())
            {
                if (IsSynNpc())
                {
                    if (pRole is Character pUser && pUser.SyndicateIdentity > 0)
                    {
                        if (Map.IsWarTime() && OwnerIdentity != pUser.SyndicateIdentity)
                        {
                            pUser.SyndicateMember.IncreaseMoney((uint) Math.Max(0, nPower));
                        }
                    }
                }
            }

            if (!IsAlive)
                BeKill(pRole);

            return true;
        }

        public override void BeKill(Role pRole)
        {
            if (IsDieAction())
            {
                GameAction.ProcessAction(m_dbNpc.Linkid, pRole as Character, this, null, null);
            }
        }

        public override bool IsAttackable(Role pTarget)
        {
            if (!IsSynFlag() && Type != 21 && Type != 22 && Type != 26 && Type != 17)
                return false;

            if (Data1 != 0 && Data2 != 0)
            {
                var strNow = "";
                var now = DateTime.Now;
                strNow += ((int) now.DayOfWeek == 0 ? 7 : (int) now.DayOfWeek).ToString(CultureInfo.InvariantCulture);
                strNow += now.Hour.ToString("00");
                strNow += now.Minute.ToString("00");
                strNow += now.Second.ToString("00");

                var now0 = int.Parse(strNow);
                if (IsCtfFlag())
                {
                    if (!ServerKernel.CaptureTheFlag.IsRunning)
                        return false;
                }
                else
                {
                    if (Data1 >= 1000000)
                    {
                        if ((now0 < Data1 || now0 >= Data2) && IsSynFlag())
                            return false;
                    }
                    else
                    {
                        int nowHour = now0 % 1000000;
                        if (nowHour < Data1 || nowHour >= Data2 && IsSynFlag())
                            return false;
                    }
                }

                if (!IsSynFlag() && Type != 21) // not syn flag neither gate
                    return false;

                if (IsSynFlag() && pTarget is Character pRole)
                {
                    if (pRole.Syndicate != null && pRole.Syndicate.Identity == OwnerIdentity) return false;
                }

                if (!IsAlive && m_pMap.Identity == 7600)
                    return false;
            }

            Character pRoleUser = pTarget as Character;
            if (pRoleUser != null && pRoleUser.SyndicateIdentity > 0)
                if (OwnerType == 2 && pRoleUser.SyndicateIdentity == OwnerIdentity)
                    return false;

            if (!IsDynaNpc() && MaxLife <= 0)
                return false;

            return IsActive();
        }

        public bool IsActive()
        {
            return !m_tDeathTime.IsActive();
        }

        public int GetMaxFixMoney()
        {
            return (int) Calculations.CutRange(Calculations.MulDiv(MaxLife - 1, 1, 1) + 1, 0, MaxLife);
        }

        public int GetLostFixMoney()
        {
            int nLostLifeTmp = (int) (MaxLife - Life);
            return (int) Calculations.CutRange(Calculations.MulDiv(nLostLifeTmp - 1, 1, 1) + 1, 0, MaxLife);
        }

        #endregion
    }

    public class NpcScore
    {
        public NpcScore(uint identity, string name)
        {
            Identity = (ushort) identity;
            Name = name;
        }

        public ushort Identity { get; }

        public string Name { get; }

        public ulong Score { get; set; }
    }
}