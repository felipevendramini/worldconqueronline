﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Npc.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.World;

#endregion

namespace GameServer.Structures.Entities
{
    public class Npc : BaseNpc
    {
        private NpcEntity m_dbNpc;
        private MsgNpcInfo m_packet;

        public Npc(uint idObj, NpcEntity npc)
            : base(idObj)
        {
            m_dbNpc = npc;

            m_idMap = npc.Mapid;
            m_usMapX = npc.Cellx;
            m_usMapY = npc.Celly;

            m_packet = new MsgNpcInfo
            {
                Identity = npc.Id,
                MapX = npc.Cellx,
                MapY = npc.Celly,
                Kind = npc.Type,
                Sort = npc.Sort,
                Lookface = npc.Lookface
            };
        }

        public override ushort Type => m_dbNpc.Type;

        public override string Name
        {
            get => m_packet.Name;
            set => m_packet.Name = value.Substring(0, value.Length > 15 ? 15 : value.Length);
        }

        public override ushort MapX
        {
            get => m_usMapX;
            set
            {
                m_usMapX = value;
                m_dbNpc.Cellx = value;
                m_packet.MapX = value;
                Save();
            }
        }

        public override ushort MapY
        {
            get => m_usMapY;
            set
            {
                m_usMapY = value;
                m_dbNpc.Celly = value;
                m_packet.MapY = value;
                Save();
            }
        }

        public override void SendSpawnTo(Character user)
        {
            //ServerKernel.Log.WriteToFile(PacketDump.Hex(m_packet), "MsgNpcInfo-Sent");
            user.Send(m_packet);
        }

        #region Map

        public override void EnterMap()
        {
            if (ServerKernel.Maps.TryGetValue(m_idMap, out Map map))
            {
                m_pMap = map;
                Map.EnterRoom(this);
            }
        }

        public override void LeaveMap()
        {
            Map?.LeaveRoom(Identity);
        }

        #endregion

        #region Npc Task and Data

        public override uint OwnerIdentity
        {
            get => m_dbNpc.Ownerid;
            set
            {
                m_dbNpc.Ownerid = value;
                Save();
            }
        }

        public override uint Task0 => m_dbNpc.Task0;

        public override uint Task1 => m_dbNpc.Task1;

        public override uint Task2 => m_dbNpc.Task2;

        public override uint Task3 => m_dbNpc.Task3;

        public override uint Task4 => m_dbNpc.Task4;

        public override uint Task5 => m_dbNpc.Task5;

        public override uint Task6 => m_dbNpc.Task6;

        public override uint Task7 => m_dbNpc.Task7;

        public override int Data0
        {
            get => m_dbNpc.Data0;
            set
            {
                m_dbNpc.Data0 = value;
                Save();
            }
        }

        public override int Data1
        {
            get => m_dbNpc.Data1;
            set
            {
                m_dbNpc.Data1 = value;
                Save();
            }
        }

        public override int Data2
        {
            get => m_dbNpc.Data2;
            set
            {
                m_dbNpc.Data2 = value;
                Save();
            }
        }

        public override int Data3
        {
            get => m_dbNpc.Data3;
            set
            {
                m_dbNpc.Data3 = value;
                Save();
            }
        }

        #endregion

        public override bool DelNpc()
        {
            LeaveMap();
            return new NpcRepository().Delete(m_dbNpc);
        }

        #region Database

        public bool Save()
        {
            return new NpcRepository().Save(m_dbNpc);
        }

        public bool Delete()
        {
            return new NpcRepository().Delete(m_dbNpc);
        }

        #endregion
    }
}