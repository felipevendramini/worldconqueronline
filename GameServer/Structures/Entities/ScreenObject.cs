﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - ScreenObject.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common;
using GameServer.World;

#endregion

namespace GameServer.Structures.Entities
{
    public abstract class ScreenObject
    {
        public static readonly int[] DeltaX = { 0, -1, -1, -1, 0, 1, 1, 1, 0 };
        public static readonly int[] DeltaY = { 1, 1, 0, -1, -1, -1, 0, 1, 0 };
        public static readonly sbyte[] DeltaRideXCoords = { 0, -2, -2, -2, 0, 2, 2, 2, 1, 0, -2, 0, 1, 0, 2, 0, 0, -2, 0, -1, 0, 2, 0, 1, 0 };
        public static readonly sbyte[] DeltaRideYCoords = { 2, 2, 0, -2, -2, -2, 0, 2, 2, 0, -1, 0, -2, 0, 1, 0, 0, 1, 0, -2, 0, -1, 0, 2, 0 };
        public const int MAX_DIRSIZE = 8;

        protected uint m_idObj = 0u;

        protected Map m_pMap;
        protected uint m_idMap = 0u;
        protected ushort m_usMapX = 0;
        protected ushort m_usMapY = 0;

        protected ScreenObject(uint idObj)
        {
            m_idObj = idObj;
        }

        public virtual uint Identity => m_idObj;
        public virtual string Name { get; set; }
        public virtual uint MapIdentity => m_idMap;
        public Map Map => m_pMap;

        public virtual ushort MapX
        {
            get => m_usMapX;
            set => m_usMapX = value;
        }

        public virtual ushort MapY
        {
            get => m_usMapY;
            set => m_usMapY = value;
        }

        public virtual void EnterMap()
        {
            if (ServerKernel.Maps.TryGetValue(MapIdentity, out m_pMap))
            {
                Map.EnterRoom(this);
            }
        }

        public virtual void LeaveMap()
        {
            Map?.LeaveRoom(Identity);
        }

        public int GetDistance(int x, int y)
        {
            return (int) Calculations.GetDistance(MapX, MapY, (ushort) x, (ushort) y);
        }

        public ScreenObject FindAroundRole(uint idObj) { return null; }
        public virtual void OnTimer() {  }
        public virtual void SendSpawnTo(Character role) {  }

        public virtual int Send(byte[] buffer)
        {
            return 0;
        }

        public virtual void ExchangeSpawnPackets(Character role) { SendSpawnTo(role); }
    }
}