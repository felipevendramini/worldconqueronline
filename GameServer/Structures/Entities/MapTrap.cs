﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - MapTrap.cs
// Last Edit: 2019/12/07 22:49
// Created: 2019/12/07 22:22
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures.Actions;
using GameServer.World;

#endregion

namespace GameServer.Structures.Entities
{
    public sealed class MapTrap : Role
    {
        private readonly TrapEntity m_dbObj;
        private readonly TimeOut m_tFight;
        private readonly TimeOut m_tLifePeriod;
        private bool m_bDie;
        private uint m_idAction;
        private int m_nAtkMode = 5;
        private int m_nAtkSpeed;
        private ushort m_nDexterity = 0;
        private int m_nMagicHitRate;
        private int m_nMaxAttack;
        private int m_nMinAttack;
        private uint m_nSize;
        private int m_nSort;
        private int m_nTimes;
        private int m_nTimesC;
        private BattleSystem m_pBattleSystem;
        private GameAction m_pGameAction;
        private byte m_pLevel = 1;
        private Map m_pMap;
        private Character m_pUser;

        private ushort m_usMgcType;

        public MapTrap(TrapEntity pTrap)
            : base(pTrap.Id)
        {
            m_dbObj = pTrap;

            m_tLifePeriod = new TimeOut();
            m_tLifePeriod.Clear();
            m_tFight = new TimeOut();
            m_tFight.Clear();
        }

        public uint Type { get; private set; }

        public bool Init()
        {
            if (m_dbObj == null)
                return false;

            TrapTypeEntity type = new TrapTypeRepository().GetById(m_dbObj.Type);
            if (type == null)
            {
                ServerKernel.Log.SaveLog($"ALERT: could not init map trap {m_dbObj.Id}, unexistent type");
                return false;
            }

            m_idAction = type.ActionId;
            m_nTimes = type.ActiveTimes;
            m_nTimesC = type.ActiveTimes;
            m_pLevel = type.Level;
            m_nMinAttack = type.AttackMin;
            m_nMaxAttack = type.AttackMax;
            m_nAtkMode = type.AtkMode;
            m_nAtkSpeed = type.AttackSpeed;
            m_usMgcType = type.MagicType;
            m_nMagicHitRate = type.MagicHitrate;
            m_nSize = type.Sort;
            Type = m_dbObj.Type;

            m_pBattleSystem = new BattleSystem(this);
            m_pGameAction = new GameAction(this);

            m_tFight.SetInterval(m_nAtkSpeed);

            if (!ServerKernel.Maps.TryGetValue(m_dbObj.MapId, out m_pMap))
                return false;

            m_idMap = m_dbObj.MapId;

            EnterMap();
            return true;
        }

        public bool IsDeleted()
        {
            return m_bDie;
        }

        public void SendShow(Character pUser)
        {
            if (IsDeleted())
                return;

            MsgMapItem pMsg = new MsgMapItem(Identity, Lookface, MapX, MapY, DropType.SynchroTrap);
            Map.SendToMap(pMsg);
        }

        public bool DelTrap()
        {
            if (!IsDeleted())
                return false;

            MsgMapItem pMsg = new MsgMapItem(Identity, Lookface, MapX, MapY, DropType.DropTrap);
            Map.SendToMap(pMsg);

            m_bDie = true;
            return true;
        }

        public bool IsTrapSort()
        {
            return m_nSort < 10;
        }

        public bool IsAutoSort()
        {
            return !IsTrapSort();
        }

        public bool IsInRange(Role target)
        {
            return Calculations.GetDistance(MapX, MapY, target.MapX, target.MapY) <= m_nSize;
        }

        public void TrapAttack(Role pTarget)
        {
            if (!IsTrapSort())
                return;

            if (!m_tFight.ToNextTick(m_nAtkSpeed))
                return;

            if (m_nTimes > 0)
                --m_nTimes;

            if (!pTarget.IsAttackable(this))
                return;
            if (m_pUser != null && m_pUser.IsImmunity(pTarget))
                return;

            if (!((m_nAtkMode & 1) > 0 && pTarget is Character || (m_nAtkMode & 2) > 0 && pTarget is Monster))
            {
                return;
            }

            if (m_pUser != null && !pTarget.IsEvil())
                m_pUser.SetCrimeStatus(30);

            if ((m_nAtkMode & 4) == 0)
            {
                m_pBattleSystem.CreateBattle(pTarget.Identity);
                m_pBattleSystem.ProcessAttack();
            }

            if (m_idAction > 0)
            {
                if ((m_nAtkMode & 1) > 0)
                {
                    m_pGameAction.ProcessAction(m_idAction, pTarget as Character, this, null, null);
                }
                else if ((m_nAtkMode & 2) > 0)
                {
                    m_pGameAction.ProcessAction(m_idAction, null, pTarget as Monster, null, null);
                }
            }

            if (m_nTimesC > 0 && m_nTimes <= 0)
                DelTrap();
        }

        public override void OnTimer()
        {
            if (!IsAlive)
                return;

            if (m_tLifePeriod.IsActive())
            {
                if (m_tLifePeriod.ToNextTime())
                {
                    DelTrap();
                    m_tLifePeriod.Clear();
                }
            }

            if (IsAutoSort())
            {
                if (m_tFight.ToNextTick(m_nAtkSpeed))
                {
                    if (m_nTimes > 0)
                        m_nTimes--;

                    // magic atk??? don't exist in cq idk why i'm handling this
                    // just in case somebody create some shit like this, lets just make it here
                    // so the trap disappear over time

                    if (m_nTimesC > 0 && m_nTimes <= 0)
                    {
                        DelTrap();
                    }
                }
            }
        }

        #region ScreenObject

        public override uint MapIdentity => m_dbObj.MapId;

        public override ushort MapX
        {
            get => m_dbObj.PosX;
            set => m_dbObj.PosX = value;
        }

        public override ushort MapY
        {
            get => m_dbObj.PosY;
            set => m_dbObj.PosY = value;
        }

        public override void SendSpawnTo(Character pRole)
        {
            // synchro
            MsgMapItem pMsg = new MsgMapItem(Identity, Lookface, MapX, MapY, DropType.SynchroTrap);
            pRole.Send(pMsg);
        }

        #endregion

        #region Role

        public override int Attack(Role target, ref InteractionEffect effect)
        {
            if (target == null)
                return 0;

            int nTempAdjust = 0;
            InteractionEffect special = InteractionEffect.None;
            int nDamage = m_pBattleSystem.CalcPower(1, this, target, ref special, nTempAdjust,
                true);

            int nLoseLife = (int) Calculations.CutOverflow(nDamage, target.Life);
            target.BeAttack(1, this, nLoseLife, true);

            return nDamage;
        }

        public void ClrAttackTarget()
        {
            SetAttackTarget(null);
        }

        public override bool IsAttackable(Role pTarget)
        {
            if (pTarget == null)
                return false;

            if (!IsAlive)
                return false;

            if (!pTarget.IsAlive)
                return false;

            if (pTarget.Identity == Identity)
                return false;

            if (!pTarget.IsAttackable(this))
                return false;

            if (!(Map != null && pTarget.Map != null && pTarget.MapIdentity == MapIdentity))
                return false;

            return true;
        }

        public override void Kill(Role pTarget, uint dwDieWay)
        {
            if (pTarget == null)
                return;

            if (m_pUser != null)
            {
                m_pUser.Kill(pTarget, dwDieWay);
                return;
            }

            MsgInteract pMsg = new MsgInteract
            {
                Action = InteractionType.ACT_ITR_KILL,
                EntityIdentity = Identity,
                TargetIdentity = pTarget.Identity,
                CellX = pTarget.MapX,
                CellY = pTarget.MapY,
                Data = dwDieWay
            };
            m_pMap.SendToRegion(pMsg, pTarget.MapX, pTarget.MapY);

            pTarget.BeKill(this);
        }

        #endregion

        #region Map

        public override void EnterMap()
        {
            if (ServerKernel.Maps.TryGetValue(m_idMap, out Map map))
            {
                m_pMap = map;
                m_pMap.EnterRoom(this);
            }
        }

        public override void LeaveMap()
        {
            Map.LeaveRoom(Identity);
        }

        #endregion
    }
}