﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Role.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Drawing;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Networking.Packets;
using GameServer.Structures.Actions;

#endregion

namespace GameServer.Structures.Entities
{
    public abstract class Role : ScreenObject
    {
        public const int DEATH_DISAPPEAR_MS = 5000;

        protected StatusSet m_pStatus; // should not use tho

        protected TimeOutMS m_tDead = new TimeOutMS(DEATH_DISAPPEAR_MS);
        protected TimeOut m_tDazed = new TimeOut(500);

        public BattleSystem BattleSystem;
        public GameAction GameAction;
        public MagicData Magics;

        protected ulong m_ulFlag1;
        protected ulong m_ulFlag2;
        protected uint m_ulFlag3;

        protected Role(uint idRole)
            : base(idRole)
        {
            MaxLife = 0;
            MaxMana = 0;

            BattleSystem = new BattleSystem(this);
            GameAction = new GameAction(this);
        }

        #region Attributes

        public virtual uint Lookface { get; set; }
        public virtual byte Level { get; set; }
        public virtual byte Metempsychosis { get; set; }

        public virtual uint Life { get; set; }
        public virtual uint MaxLife { get; }
        public virtual uint Mana { get; set; }
        public virtual uint MaxMana { get; }

        public virtual byte Stamina { get; set; }
        public virtual byte MaxStamina { get; set; }

        public virtual double DragonGem { get; } = 0;
        public virtual double PhoenixGem { get; } = 0;
        public virtual double MoonGem { get; } = 0;
        public virtual double KylinGem { get; } = 0;
        public virtual double TortoiseGem { get; } = 0;
        public virtual double RainbowGem { get; } = 0;
        public virtual double VioletGem { get; } = 0;
        public virtual double FuryGem { get; } = 0;

        public virtual int Defense2 => (Level >= 70 || Metempsychosis >= 1) ? 7000 : 10000;

        #region Battle Attributes

        public virtual int MinAttack => 0;
        public virtual int MaxAttack => 0;
        public virtual int MagicAttack => 0;
        public virtual int Defense => 0;
        public virtual int MagicDefense => 0;
        public virtual float ReduceDamage => 0f;
        public virtual int Accuracy => 0;
        public virtual int AttackSpeed => 0;
        public virtual int Dodge => 0;
        public virtual int MagicDefenseBonus => 0;
        public virtual int FinalAttack => 0;
        public virtual int FinalDefense => 0;
        public virtual int FinalMagicAttack => 0;
        public virtual int FinalMagicDefense => 0;
        public virtual int CriticalStrike => 0;
        public virtual int SkillCriticalStrike => 0;
        public virtual int Immunity => 0;
        public virtual int Penetration => 0;
        public virtual int Breakthrough => 0;
        public virtual int Counteraction => 0;
        public virtual int Block => 0;
        public virtual int Detoxication => 0;
        public virtual int FireResistance => 0;
        public virtual int WaterResistance => 0;
        public virtual int WoodResistance => 0;
        public virtual int EarthResistance => 0;
        public virtual int MetalResistance => 0;

        public virtual ulong Flag1
        {
            get => m_ulFlag1;
            set
            {
                m_ulFlag1 = value;
                UpdateClient(ClientUpdateType.StatusFlag, Flag1, Flag2, Flag3);
            }
        }
        public virtual ulong Flag2
        {
            get => m_ulFlag2;
            set
            {
                m_ulFlag2 = value;
                UpdateClient(ClientUpdateType.StatusFlag, Flag1, Flag2, Flag3);
            }
        }
        public virtual uint Flag3
        {
            get => m_ulFlag3;
            set
            {
                m_ulFlag3 = value;
                UpdateClient(ClientUpdateType.StatusFlag, Flag1, Flag2, Flag3);
            }
        }

        #endregion

        public virtual int BattlePower { get; } = 1;

        public virtual EntityAction Action { get; set; }
        public virtual FacingDirection Direction { get; set; }

        #endregion

        #region Attributes Function

        public virtual void SendEffect(string effectName, bool screen)
        {
            var msg = new MsgName
            {
                Identity = Identity,
                Action = StringAction.RoleEffect
            };
            msg.Append(effectName);
            Map.SendToRegion(msg, MapX, MapY);
        }

        /// <summary>
        /// Send a update packet containing a byte value.
        /// </summary>
        public virtual void UpdateClient(ClientUpdateType type, byte value, bool broadcast = true)
        {
            var updatePacket = new MsgUserAttrib { Identity = Identity };
            updatePacket.Append(type, value);
            Map.SendToRegion(updatePacket, MapX, MapY);
        }

        /// <summary>
        /// Send a update packet containing a uint value.
        /// </summary>
        public virtual void UpdateClient(ClientUpdateType type, uint value, bool broadcast = false)
        {
            var updatePacket = new MsgUserAttrib { Identity = Identity };
            updatePacket.Append(type, value);
            Map.SendToRegion(updatePacket, MapX, MapY);
        }

        /// <summary>
        /// Send a update packet containing a ushort value.
        /// </summary>
        public virtual void UpdateClient(ClientUpdateType type, ushort value, bool broadcast = false)
        {
            var updatePacket = new MsgUserAttrib { Identity = Identity };
            updatePacket.Append(type, value);
            Map.SendToRegion(updatePacket, MapX, MapY);
        }

        /// <summary>
        /// Send a update packet containing a ulong value.
        /// </summary>
        public virtual void UpdateClient(ClientUpdateType type, ulong value, bool broadcast = false)
        {
            var updatePacket = new MsgUserAttrib { Identity = Identity };
            updatePacket.Append(type, value);
            Map.SendToRegion(updatePacket, MapX, MapY);
        }

        public virtual void UpdateClient(ClientUpdateType type, ulong value1, ulong value2, bool broadcast = false)
        {
            var updatePacket = new MsgUserAttrib { Identity = Identity };
            updatePacket.Append(type, value1, value2);
            Map.SendToRegion(updatePacket, MapX, MapY);
        }

        public virtual void UpdateClient(ClientUpdateType type, ulong value1, ulong value2, ulong value3, bool broadcast = false)
        {
            var updatePacket = new MsgUserAttrib { Identity = Identity };
            updatePacket.Append(type, value1, value2, value3);
            Map.SendToRegion(updatePacket, MapX, MapY);
        }

        public virtual void UpdateClient(ClientUpdateType type, uint value1, uint value2, uint value3, uint value4, bool broadcast = false)
        {
            var updatePacket = new MsgUserAttrib { Identity = Identity };
            updatePacket.Append(type, value1, value2, value3, value4);
            Map.SendToRegion(updatePacket, MapX, MapY);
        }

        public virtual void UpdateAzureShield(int nTime, int nPower, byte level)
        {
            var updatePacket = new MsgUserAttrib { Identity = Identity };
            updatePacket.Append(ClientUpdateType.AzureShield, 93u, (uint)nTime, (uint)nPower, level);
            Map.SendToRegion(updatePacket, MapX, MapY);
        }

        public virtual bool AddAttrib(ClientUpdateType type, long value)
        {
            switch (type)
            {
                case ClientUpdateType.Hitpoints:
                    {
                        if (value > 0)
                        {
                            Life = (uint)Math.Min(MaxLife, Life + value);
                        }
                        else if (value < 0)
                        {
                            Life = (uint)Math.Max(0, Life + value);
                        }
                        return true;
                    }
                case ClientUpdateType.Mana:
                    {
                        if (value > 0)
                        {
                            Mana = (uint)Math.Min(MaxMana, Mana + value);
                        }
                        else if (value < 0)
                        {
                            Mana = (uint)Math.Max(0, Mana + value);
                        }
                        return true;
                    }

                case ClientUpdateType.Stamina:
                    {
                        if (value > 0)
                        {
                            Stamina = (byte)Math.Min(MaxStamina, Stamina + value);
                        }
                        else if (value < 0)
                        {
                            Stamina = (byte)Math.Max(0, Stamina + value);
                        }

                        return true;
                    }
            }

            return false;
        }

        public virtual bool SetAttrib(ClientUpdateType type, long value)
        {
            switch (type)
            {
                case ClientUpdateType.Hitpoints:
                {
                    Life = (uint) Math.Min(value, MaxLife);
                    UpdateClient(type, (uint) value, true);
                    return true;
                }
                case ClientUpdateType.Mesh:
                    Lookface = (uint) value;
                    UpdateClient(type, (uint) value, true);
                    return true;
            }

            return true;
        }

        public virtual bool SetAttrib(string type, long value)
        {
            switch (type)
            {
                case "data0":
                    if (this is BaseNpc npc0)
                    {
                        npc0.Data0 = (int)value;
                        return true;
                    }
                    break;
                case "data1":
                    if (this is BaseNpc npc1)
                    {
                        npc1.Data1 = (int)value;
                        return true;
                    }
                    break;
                case "data2":
                    if (this is BaseNpc npc2)
                    {
                        npc2.Data2 = (int)value;
                        return true;
                    }
                    break;
                case "data3":
                    if (this is BaseNpc npc3)
                    {
                        npc3.Data3 = (int) value;
                        return true;
                    }
                    break;
            }

            return false;
        }

        public virtual int GetExpGemEffect()
        {
            return (int) Math.Ceiling(RainbowGem * 100);
        }

        public virtual int GetAtkGemEffect()
        {
            return (int)Math.Ceiling(DragonGem * 100);
        }

        public virtual int GetMAtkGemEffect()
        {
            return (int)Math.Ceiling(PhoenixGem * 100);
        }

        public virtual int GetSkillGemEffect()
        {
            return (int)Math.Ceiling(MoonGem * 100);
        }

        public virtual int GetTortoiseGemEffect()
        {
            return (int)Math.Ceiling(TortoiseGem * 100);
        }

        #endregion

        #region Role Checks

        public bool IsAlive => Life > 0;

        public virtual bool IsWing => QueryStatus(FlagInt.FLY) != null;

        public virtual bool IsBowman => false;

        public virtual int SizeAddition => 1;

        public virtual bool CheckCrime(Role pRole)
        {
            return false;
        }

        #endregion

        #region Status

        public virtual StatusSet Status => m_pStatus ?? (m_pStatus = new StatusSet(this));

        public virtual bool DetachWellStatus()
        {
            for (int i = 1; i < 128; i++)
            {
                if (Status[i] != null)
                    if (IsWellStatus(i))
                        DetachStatus(i);
            }
            return true;
        }

        public virtual bool DetachBadlyStatus()
        {
            for (int i = 1; i < 128; i++)
            {
                if (Status[i] != null)
                    if (IsBadlyStatus(i))
                        DetachStatus(i);
            }
            return true;
        }

        public virtual bool DetachAllStatus()
        {
            DetachBadlyStatus();
            DetachWellStatus();
            return true;
        }

        public virtual bool IsWellStatus(int stts)
        {
            switch (stts)
            {
                case FlagInt.RIDING:
                case FlagInt.FULL_INVIS:
                case FlagInt.LUCKY_DIFFUSE:
                case FlagInt.STIG:
                case FlagInt.SHIELD:
                case FlagInt.STAR_OF_ACCURACY:
                case FlagInt.START_XP:
                case FlagInt.INVISIBLE:
                case FlagInt.SUPERMAN:
                case FlagInt.PARTIALLY_INVISIBLE:
                case FlagInt.LUCKY_ABSORB:
                case FlagInt.VORTEX:
                case FlagInt.POISON_STAR:
                case FlagInt.FLY:
                case FlagInt.FATAL_STRIKE:
                case FlagInt.AZURE_SHIELD:
                case FlagInt.SUPER_SHIELD_HALO:
                case FlagInt.CARYING_FLAG:
                case FlagInt.EARTH_AURA:
                case FlagInt.FEND_AURA:
                case FlagInt.FIRE_AURA:
                case FlagInt.METAL_AURA:
                case FlagInt.TYRANT_AURA:
                case FlagInt.WATER_AURA:
                case FlagInt.WOOD_AURA:
                case FlagInt.OBLIVION:
                case FlagInt.CTF_FLAG:
                    return true;
            }
            return false;
        }

        public virtual bool IsBadlyStatus(int stts)
        {
            switch (stts)
            {
                case FlagInt.POISONED:
                case FlagInt.CONFUSED:
                case FlagInt.ICE_BLOCK:
                case FlagInt.HUGE_DAZED:
                case FlagInt.DAZED:
                case FlagInt.SHACKLED:
                case FlagInt.TOXIC_FOG:
                    return true;
            }
            return false;
        }

        public virtual bool IsWellStatus0(ulong nStatus)
        {
            switch ((Effect0)nStatus)
            {
                case Effect0.FULL_INVIS:
                case Effect0.LUCKY_TIME:
                case Effect0.STIG:
                case Effect0.SHIELD:
                case Effect0.STAR_OF_ACCURACY:
                case Effect0.START_XP:
                case Effect0.INVISIBLE:
                case Effect0.SUPERMAN:
                case Effect0.PARTIALLY_INVISIBLE:
                case Effect0.PRAY:
                    return true;
            }
            return false;
        }

        public virtual bool IsBadlyStatus0(ulong nStatus)
        {
            switch ((Effect0)nStatus)
            {
                case Effect0.POISONED:
                case Effect0.CONFUSED:
                case Effect0.ICE_BLOCK:
                case Effect0.HUGE_DAZED:
                case Effect0.DAZED:
                    return true;
            }
            return false;
        }

        public virtual bool AppendStatus(StatusInfoStruct pInfo)
        {
            if (pInfo.Times > 0)
            {
                var pStatus = new StatusMore();
                if (pStatus.Create(this, pInfo.Status, pInfo.Power, pInfo.Seconds, pInfo.Times))
                    Status.AddObj(pStatus);
            }
            else
            {
                var pStatus = new StatusOnce();
                if (pStatus.Create(this, pInfo.Status, pInfo.Power, pInfo.Seconds, pInfo.Times))
                    Status.AddObj(pStatus);
            }
            return true;
        }

        public virtual bool AttachStatus(Role pSender, int nStatus, int nPower, int nSecs, int nTimes, byte pLevel)
        {
            if (Map == null)
                return false;

            if (nStatus == FlagInt.BLUE_NAME && (Map.IsPkField() || Map.IsSynMap() || Map.IsDeadIsland()))
                return false;

            if (nStatus == FlagInt.DAZED)
                m_tDazed.Startup(500);

            switch (nStatus)
            {
                case FlagInt.DAZED:
                case FlagInt.HUGE_DAZED:
                case FlagInt.ICE_BLOCK:
                case FlagInt.CONFUSED:
                    {
                        BattleSystem.ResetBattle();
                        break;
                    }
            }

            IStatus pStatus = QueryStatus(nStatus);
            if (pStatus != null)
            {
                bool bChangeData = false;
                if (pStatus.Power == nPower)
                    bChangeData = true;
                else
                {
                    int nMinPower = Math.Min(nPower, pStatus.Power);
                    int nMaxPower = Math.Max(nPower, pStatus.Power);

                    if (nPower <= 30000)
                        bChangeData = true;
                    else
                    {
                        if (nMinPower >= 30100 || nMinPower > 0 && nMaxPower < 30000)
                        {
                            if (nPower > pStatus.Power)
                                bChangeData = true;
                        }
                        else if (nMaxPower < 0 || nMinPower > 30000 && nMaxPower < 30100)
                        {
                            if (nPower < pStatus.Power)
                                bChangeData = true;
                        }
                    }
                }

                if (bChangeData)
                {
                    pStatus.ChangeData(nPower, nSecs, nTimes, pSender.Identity);
                }
                return true;
            }
            else
            {
                if (nTimes > 1)
                {
                    var pNewStatus = new StatusMore();
                    if (pNewStatus.Create(this, nStatus, nPower, nSecs, nTimes, pSender.Identity, pLevel))
                    {
                        Status.AddObj(pNewStatus);
                        return true;
                    }
                }
                else
                {
                    var pNewStatus = new StatusOnce();
                    if (pNewStatus.Create(this, nStatus, nPower, nSecs, 0, pSender.Identity, pLevel))
                    {
                        Status.AddObj(pNewStatus);
                        return true;
                    }
                }
            }
            return false;
        }

        public virtual bool DetachStatus(int nType)
        {
            StatusSet pStatusSet = Status;
            if (pStatusSet == null)
                return false;

            return pStatusSet.DelObj(nType);
        }

        public virtual bool DetachStatus(Effect0 nType)
        {
            StatusSet pStatusSet = Status;
            if (pStatusSet == null)
                return false;

            if (nType == Effect0.DAZED)
                m_tDazed.Clear();

            return pStatusSet.DelObj(nType);
        }

        public virtual bool DetachStatus(Effect1 nType)
        {
            StatusSet pStatusSet = Status;
            if (pStatusSet == null)
                return false;

            return pStatusSet.DelObj(nType);
        }

        public virtual bool DetachStatus(ulong nType, bool b64)
        {
            StatusSet pStatusSet = Status;
            if (pStatusSet == null)
                return false;

            if (nType == FlagInt.DAZED)
                m_tDazed.Clear();

            return pStatusSet.DelObj(StatusSet.InvertFlag(nType, b64));
        }

        public virtual IStatus QueryStatus(Effect1 nType)
        {
            return Status?.GetObj((ulong)nType + 64);
        }

        public virtual IStatus QueryStatus(int nType)
        {
            return Status?.GetObjByIndex(nType);
        }

        public virtual IStatus QueryStatus(Effect0 nType)
        {
            return Status?.GetObj((ulong)nType);
        }

        #endregion

        #region Object type

        public virtual bool IsPlayer()
        {
            return false;
        }

        public virtual bool IsNpc()
        {
            return false;
        }

        public virtual bool IsDynaNpc()
        {
            return false;
        }

        public virtual bool IsMonster()
        {
            return false;
        }

        public virtual bool IsGuard()
        {
            return false;
        }

        #endregion

        #region Movement

        public bool SynchroPosition(int nPosX, int nPosY, int nMaxDislocation = 8)
        {
            if (nMaxDislocation <= 0 || (nPosX == 0 && nPosY == 0))	// ignore in this condition
                return true;

            int nDislocation = GetDistance((ushort)nPosX, (ushort)nPosY);
            if (nDislocation >= nMaxDislocation)
                return false;

            if (nDislocation <= 0)
                return true;

            if (!Map.IsValidPoint(nPosX, nPosY))
                return false;

            Kickback((ushort)nPosX, (ushort)nPosY);
            return true;
        }

        public void Kickback(ushort x, ushort y)
        {
            MapX = x;
            MapY = y;
            Send(new MsgAction(Identity, 0, x, y, GeneralActionType.NewCoordinates));
        }

        public virtual void ProcessOnMove(MovementType mode)
        {
            Action = EntityAction.Stand;

            if (QueryStatus(FlagInt.LUCKY_DIFFUSE) != null)
                DetachStatus(FlagInt.LUCKY_DIFFUSE);
            if (QueryStatus(FlagInt.LUCKY_ABSORB) != null)
                DetachStatus(FlagInt.LUCKY_ABSORB);
            if (QueryStatus(FlagInt.MAGIC_DEFENDER) != null)
                DetachStatus(FlagInt.MAGIC_DEFENDER);
            if (QueryStatus(FlagInt.INTENSIFY) != null)
                DetachStatus(FlagInt.INTENSIFY);

            if (BattleSystem.IsActive())
                BattleSystem.ResetBattle();

            if (Magics.QueryMagic() != null)
            {
                Magics.SetMagicState(MagicData.MAGICSTATE_NONE);
                Magics.AbortMagic(true);
            }
        }

        public virtual void ProcessAfterMove()
        {
        }

        public virtual void ProcessOnAttack()
        {
            Action = EntityAction.Stand;

            if (QueryStatus(FlagInt.LUCKY_DIFFUSE) != null)
                DetachStatus(FlagInt.LUCKY_DIFFUSE);
            if (QueryStatus(FlagInt.LUCKY_ABSORB) != null)
                DetachStatus(FlagInt.LUCKY_ABSORB);
            if (QueryStatus(FlagInt.MAGIC_DEFENDER) != null)
                DetachStatus(FlagInt.MAGIC_DEFENDER);
            if (QueryStatus(FlagInt.INTENSIFY) != null)
                DetachStatus(FlagInt.INTENSIFY);
        }

        public void TransPos(ushort x, ushort y)
        {
            if (x == MapX && y == MapY)
                return;

            if (!Map.IsValidPoint(x, y))
                return;

            if (!Map.IsStandEnable(x, y)
                && IsPlayer())
            {
                Character user = (Character) this;
                user.SendSysMessage(Language.StrInvalidCoordinate, Color.Red);
                user.Kickback();
                return;
            }
        }

        public void JumpPos(int nPosX, int nPosY)
        {
            if (nPosX == MapX && nPosY == MapY)
                return;

            if (!Map.IsValidPoint(nPosX, nPosY))
                return;

            Character user = null;
            if (IsPlayer())
            {
                user = (Character) this;
                int deltaX = Math.Abs(nPosX - MapX);
                int deltaY = Math.Abs(nPosY - MapY);

                if (deltaX > Calculations.SCREEN_DISTANCE
                    || deltaY > Calculations.SCREEN_DISTANCE)
                    return;

                if (!Map.IsStandEnable((ushort) nPosX, (ushort) nPosY))
                {
                    user.SendSysMessage(Language.StrInvalidCoordinate, Color.Red);
                    user.Kickback();
                    return;
                }
            }

            // todo user change region
            Direction = (FacingDirection) Calculations.GetDirectionSector(MapX, MapY, (ushort) nPosX, (ushort) nPosY);
            MapX = (ushort) nPosX;
            MapY = (ushort) nPosY;
            ProcessAfterMove();
        }

        public bool MoveToward(FacingDirection dir, bool bSync)
        {
            int nDirX = DeltaX[(int) dir];
            int nDirY = DeltaY[(int) dir];
            int nNewPosX = MapX + nDirX;
            int nNewPosY = MapY + nDirY;

            if (!Map.IsValidPoint(nNewPosX, nNewPosY))
                return false;

            Character user = null;
            if (!Map.IsStandEnable((ushort) nNewPosX, (ushort) nNewPosY)
                && IsPlayer())
            {
                user = (Character) this;
                user.SendSysMessage(Language.StrInvalidCoordinate, Color.Red);
                user.Kickback();
                return false;
            }
            // todo change user region

            Direction = (FacingDirection) Calculations.GetDirectionSector(MapX, MapY, (ushort) nNewPosX,
                (ushort) nNewPosY);
            MapX = (ushort) nNewPosX;
            MapY = (ushort) nNewPosY;
            ProcessAfterMove();
            return true;
        }

        public bool MoveTowardRide(byte dir, bool bSync)
        {
            dir %= 24;
            int nDirX = DeltaRideXCoords[dir];
            int nDirY = DeltaRideYCoords[dir];
            int nNewPosX = MapX + nDirX;
            int nNewPosY = MapY + nDirY;
            int nDeltaX = nNewPosX - MapX;
            int nDeltaY = nNewPosY - MapY;

            if (!Map.IsValidPoint(nNewPosX, nNewPosY) || !Map.SampleElevation(
                    (int) Calculations.GetDistance(MapX, MapY, (ushort) nNewPosX, (ushort) nNewPosY),
                    MapX, MapY, nDeltaX, nDeltaY, Map[MapX, MapY].Elevation) && IsPlayer())
            {
                var user = (Character)this;
                user.SendSysMessage(Language.StrInvalidCoordinate, Color.Red);
                user.Kickback();
                return false;
            }

            if (!Map.IsStandEnable((ushort)nNewPosX, (ushort)nNewPosY)
                && IsPlayer())
            {
                var user = (Character)this;
                user.SendSysMessage(Language.StrInvalidCoordinate, Color.Red);
                user.Kickback();
                return false;
            }

            Direction = (FacingDirection)Calculations.GetDirectionSector(MapX, MapY, (ushort)nNewPosX,
                (ushort)nNewPosY);
            MapX = (ushort)nNewPosX;
            MapY = (ushort)nNewPosY;
            ProcessAfterMove();
            return true;
        }

        #endregion

        #region Battle

        public virtual bool SpendEquipItem(uint useItem, uint useItemNum, bool bSynchro)
        {
            return true;
        }

        public virtual bool DecEquipmentDurability(bool bAttack, int hitByMagic, ushort useItemNum)
        {
            return true;
        }

        public virtual bool ProcessMagicAttack(ushort usMagicType, uint idTarget, ushort x, ushort y,
            byte ucAutoActive = 0)
        {
            return Magics.MagicAttack(usMagicType, idTarget, x, y, ucAutoActive);
        }

        public virtual bool CheckWeaponSubType(uint useItem, uint useItemNum = 0)
        {
            return true;
        }

        public virtual int CalculateFightRate()
        {
            return 0;
        }

        public virtual bool SetAttackTarget(Role role)
        {
            return true;
        }

        public virtual int AdjustDefense(Role attacker)
        {
            return Defense;
        }

        public virtual int AdjustMagicDefense(Role attacker)
        {
            return MagicDefense;
        }

        public virtual int AdjustWeaponDamage(int nDamage)
        {
            return Calculations.MulDiv(nDamage, Defense2, Calculations.DEFAULT_DEFENCE2);
        }

        public int AdjustMagicDamage(int nDamage)
        {
            return Calculations.MulDiv(nDamage, Defense2, Calculations.DEFAULT_DEFENCE2);
        }

        /// <summary>
        /// Get the attack range to cast melee attacks.
        /// </summary>
        /// <returns>The range in blocks.</returns>
        public virtual int GetAttackRange(int nTargetSizeAdd)
        {
            return nTargetSizeAdd + 1;
        }

        /// <summary>
        /// If this entity is able to be attacked.
        /// </summary>
        /// <param name="attacker">The one who is attacking.</param>
        public virtual bool IsAttackable(Role attacker)
        {
            if (!IsAlive)
                return false;

            if (this is Character user)
            {
                if (!user.LoginComplete || (attacker is Character tgtUsr && Map.IsPkDisable() && (!tgtUsr.JiangHuActive || !user.JiangHuActive)) || !user.HasSpawnFinished)
                    return false;

                if (ServerKernel.ArenaQualifier.IsUserInsideMatch(user.Identity) &&
                    !ServerKernel.ArenaQualifier.FindMatch(user.Identity).IsAttackEnable())
                    return false;
            }

            return true;
        }

        /// <summary>
        /// If the target has immunity to this entity attack.
        /// </summary>
        /// <param name="target">The one who is defending.</param>
        /// <returns>False the target entity can be attacked.</returns>
        public virtual bool IsImmunity(Role target)
        {
            if (target == null)
                return true;

            if (target.Identity == Identity)
                return true;
            return false;
        }

        /// <summary>
        /// If the target is flashing for PKing or black named.
        /// </summary>
        /// <returns></returns>
        public virtual bool IsEvil()
        {
            return QueryStatus(FlagInt.BLUE_NAME) != null || QueryStatus(FlagInt.BLACK_NAME) != null;
        }

        /// <summary>
        /// Send the damage message to the map.
        /// </summary>
        public virtual void SendDamageMsg(uint idTarget, int nDamage, InteractionEffect effect)
        {
            MsgInteract msg = new MsgInteract
            {
                EntityIdentity = Identity,
                TargetIdentity = idTarget,
                CellX = MapX,
                CellY = MapY,
                Data = (uint) nDamage,
                ActivationType = effect,
                ActivationValue = (uint) nDamage
            };

            if (IsBowman)
            {
                msg.Action = InteractionType.ACT_ITR_SHOOT;
            }
            else if (QueryStatus(FlagInt.FATAL_STRIKE) != null)
            {
                msg.Action = InteractionType.ACT_ITR_FATAL_STRIKE;
            }
            else
            {
                msg.Action = InteractionType.ACT_ITR_ATTACK;
            }

            Map?.SendToRegion(msg, MapX, MapY);
        }

        public virtual int Attack(Role target, ref InteractionEffect effect)
        {
            return 0;
        }

        public virtual bool BeAttack(int magic, Role attacker, int nPower, bool bReflectEnable)
        {
            return false;
        }

        public virtual void BeKill(Role attacker)
        {
            
        }

        public virtual void Kill(Role target, uint dwDieWay)
        {

        }

        #endregion

        #region Removable Roles Only (Traps, Monsters, Dinamic Roles)



        #endregion
    }
}