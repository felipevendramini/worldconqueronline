﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Character.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/30 20:16
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using FtwCore.Networking.Sockets;
using GameServer.Networking.GameServer;
using GameServer.Structures.Actions;
using GameServer.Structures.Groups.Families;
using GameServer.Structures.Groups.Syndicates;
using GameServer.Structures.Items;
using GameServer.Structures.Managers;
using GameServer.Structures.Qualifiers.User;
using GameServer.World;
using ArenaRankType = FtwCore.Networking.Packets.ArenaRankType;

#endregion

namespace GameServer.Structures.Entities
{
    public sealed class Character : Role
    {
        #region Constants

        public const int EXPBALL_AMOUNT = 600;

        public const int MAX_USERNAME_LENGTH = 16;
        public const int MAX_UPLEVEL = 140;
        public const int MAX_METEMPSYCHOSIS = 2;
        public const int SYNCHRO_SECS = 5;
        public const int MAX_MENUTASKSIZE = 8;
        public const int XP_SECS = 3;
        public const string MATE_NONE = "None";
        public const int ADD_ENERGY_STAND = 7;
        public const int CHGMAP_LOCK_SECS = 10;
        public const int MASTER_WEAPONSKILLLEVEL = 12;
        public const int MAX_WEAPONSKILLLEVEL = 20;

        public const int SYNWAR_NOMONEY_DAMAGETIMES = 10; // °ïÅÉÃ»Ç®Ê±Ôö¼ÓµÄÉËº¦±¶Êý
        public const int PICK_MORE_MONEY = 1000; // ¼ñÇ®¶àÊ±£¬·¢ACTIONÏûÏ¢

        public const int ADD_ENERGY_STAND_SECS = 1; // Ã¿¶àÉÙÃëÔö¼ÓÒ»´Î
        public const int DEL_ENERGY_PELT_SECS = 3; // ¼²ÐÐ×´Ì¬Ã¿3Ãë¼õÒ»´Î
        public const int DEL_ENERGY_PELT = 2; // Ã¿´Î¿Û2µã
        public const int KEEP_STAND_MS = 1500; // Õ¾ÎÈÐèÒªµÄÊ±¼ä£¬Õ¾ÎÈºóÄÜµ²×¡Íæ¼Ò¡£
        public const int CRUSH_ENERGY_NEED = 100; // ¼·¿ªÐèÒª¶àÉÙµã
        public const int CRUSH_ENERGY_EXPEND = 100; // ¼·¿ªÏûºÄ¶àÉÙµã
        public const int SYNWAR_PROFFER_PERCENT = 1; // ´òÖù×ÓµÃ¾­ÑéÖµµÄ°Ù·Ö±È
        public const int SYNWAR_MONEY_PERCENT = 2; // ´òÖù×ÓµÃÇ®µÄ°Ù·Ö±È
        public const int MIN_SUPERMAP_KILLS = 25; // ÎÞË«ÁÐ±íµÍÏÞ
        public const int NEWBIE_LEVEL = 30; // ÐÂÊÖµÄµÈ¼¶
        public const int VETERAN_DIFF_LEVEL = 20; // ÀÏÊÖµÄµÈ¼¶²î
        public const int MIN_TUTOR_LEVEL = 50; // µ¼Ê¦×îµÍµÈ¼¶
        public const int HIGHEST_WATER_WIZARD_PROF = 135; // Ë®ÕæÈË
        public const int MOUNT_ADD_INTIMACY_SECS = 3600; // 
        public const int INTIMACY_DEAD_DEC = 20; // 
        public const int SLOWHEALLIFE_MS = 1000; // ³ÔÒ©²¹ÑªÂýÂý²¹ÉÏÈ¥µÄºÀÃëÊý¡£
        public const int AUTOHEALLIFE_TIME = 10; // Ã¿¸ô10Ãë×Ô¶¯²¹Ñª1´Î¡£
        public const int AUTOHEALLIFE_EACHPERIOD = 6; // Ã¿´Î²¹Ñª6¡£
        public const int AUTOHEAL_MAXLIFE_TIME = 60; // Ã¿¸ô60Ãë×Ô¶¯»Ö¸´maxlife
        public const int AUTOHEAL_MAXLIFE_FLESH_WOUND = 16; // ÇáÉË×´Ì¬ÏÂÃ¿´Î»Ö¸´maxlifeµÄÇ§·Ö±È
        public const int AUTOHEAL_MAXLIFE_GBH = 4; // ÖØÉË×´Ì¬ÏÂÃ¿´Î»Ö¸´maxlifeµÄÇ§·Ö±È
        public const int TICK_SECS = 10;
        public const int MAX_PKLIMIT = 10000; // PKÖµµÄ×î´ó×îÐ¡ÏÞÖÆ(·À»Ø¾í)  //change huang 2004.1.11
        public const int PILEMONEY_CHANGE = 5000; // ´óÁ¿ÏÖ½ð±ä¶¯(ÐèÒª¼´Ê±´æÅÌ)
        public const int ADDITIONALPOINT_NUM = 3; // Éý1¼¶¼Ó¶àÉÙÊôÐÔµã
        public const int PK_DEC_TIME = 180; // Ã¿¼ä¸ô60Ãë½µµÍpkÖµ     //change huang 2004.1.11   
        public const int PKVALUE_DEC_ONCE = 1; // °´Ê±¼ä¼õÉÙPKÖµ¡£        
        public const int PKVALUE_DEC_ONCE_IN_PRISON = 3; // °´Ê±¼ä¼õÉÙPKÖµ¡ª¡ªÔÚ¼àÓüÖÐµÄÇé¿ö
        public const int PKVALUE_DEC_PERDEATH = 10; // ±»PKËÀÒ»´Î¼õÉÙµÄPKÖµ
        public const int TIME_TEAMPRC = 5; // ¶ÓÎéÐÅÏ¢´¦ÀíµÄÊ±¼ä
        public const int DURABILITY_DEC_TIME = 12; // Ã¿12Ãë½µµÍÒ»´Î°´Ê±¼äÏûºÄµÄ×°±¸ÄÍ¾Ã¶È
        public const int USER_ATTACK_SPEED = 1500; // Íæ¼ÒÍ½ÊÖ¹¥»÷ÆµÂÊ
        public const int POISONDAMAGE_INTERVAL = 2; // ÖÐ¶¾Ã¿2ÃëÉËÑªÒ»´Î
        public const int WARGHOST_CHECK_INTERVAL = 10; // Ã¿¸ô10Ãë¼ì²éÒ»´ÎÕ½»êµÈ¼¶(ÎäÆ÷¾­Ñé)
        public const int AUTO_REBORN_SECS = 30; // ¸´»î±¦Ê¯30Ãëºó×Ô¶¯Ê¹ÓÃ
        public const int INC_POTENTIAL_SECS = 6 * 60; // Ã¿¸ô6·ÖÖÓÔö¼ÓÒ»´ÎÇ±ÄÜ
        public const int INC_POTENTIAL_NUM = 1; // Ã¿´ÎÔö¼Ó1µãÇ±ÄÜ
        public const int ADD_POTENTIAL_RELATIONSHIP = 6; // ¹ØÏµÖµÖ®ºÍÃ¿´ïµ½6Ôö¼Ó1µãÇ±Á¦Öµ

        public const uint MAX_MONEY_LIMIT = 4000000000u;

        public const ulong KEEP_EFFECT_NOT_VIRTUOUS = (ulong) (Effect0.BLUE_NAME | Effect0.RED_NAME | Effect0.BLACK_NAME);

        #endregion

        private bool m_bDeleted;

        private CharacterEntity m_dbUser;
        private User m_pOwner;
        private MsgPlayer m_packet;

        private Screen m_pScreen;

        private ushort m_usBody;
        private ushort m_usTransformation;
        private ushort m_usAvatar;
        private uint m_dwLookface;

        private ushort m_usMaxLife;
        private ushort m_usMaxMana;
        private int m_minAttack;
        private int m_minAttackEx;
        private int m_maxAttack;
        private int m_maxAttackEx;
        private int m_magicAttack;
        private int m_magicAttackEx;
        private int m_defense;
        private int m_magicDefense;
        private int m_dexterity;
        private int m_attackSpeed;
        private int m_dodge;
        private int m_magicDefenseBonus;
        private int m_finalAttack;
        private int m_finalDefense;
        private int m_finalMagicAttack;
        private int m_finalMagicDefense;
        private int m_criticalStrike;
        private int m_skillCriticalStrike;
        private int m_immunity;
        private int m_penetration;
        private int m_breakthrough;
        private int m_counteraction;
        private int m_block;
        private int m_detoxication;
        private int m_fireResistance;
        private int m_waterResistance;
        private int m_woodResistance;
        private int m_earthResistance;
        private int m_metalResistance;
        private float m_blessing;
        private uint m_dwVigor;
        private uint m_dwMaxVigor;

        private long m_lAccumulateExp;

        private int m_nFightPause = 1500;

        private double m_dragonGem;
        private double m_phoenixGem;
        private double m_furyGem;
        private double m_rainbowGem;
        private double m_kylinGem;
        private double m_violetGem;
        private double m_moonGem;
        private double m_tortoiseGem;

        // requests
        private uint m_dwFriendRequest;
        private uint m_dwMarryRequest;
        private uint m_dwTradeRequest;
        private uint m_dwTeamJoin;
        private uint m_dwTeamInvite;
        private uint m_dwSynJoin;
        private uint m_dwSynInvite;
        private uint m_dwSynAlly;
        private uint m_dwTradeBuddy;
        private uint m_dwGuideRequest;
        private uint m_dwStudentRequest;
        private uint m_dwFamilyRequest;
        private uint m_dwJoinFamilyRequest;

        private bool m_bIsAway;

        private byte m_pStamina;
        private byte m_pXpPoints;
        private byte m_pBlessPoints;

        private int m_nMineCount;
        private int m_nKoCount;

        private TimeOut m_tCheckLuckyScreen = new TimeOut(2);
        private TimeOut m_tSynchro = new TimeOut(SYNCHRO_SECS);
        private TimeOut m_tXp = new TimeOut(XP_SECS);
        private TimeOut m_tTransformation = new TimeOut(0);
        private TimeOut m_tMine = new TimeOut(0);
        private TimeOutMS m_tStamina = new TimeOutMS(1200);
        private TimeOut m_lifeRecover = new TimeOut(5);
        private TimeOut m_tTeamPos = new TimeOut(3);
        private TimeOut m_tPkDecrease = new TimeOut(360);
        private TimeOut m_tHeavenBlessing = new TimeOut(60);
        private TimeOut m_timePacket = new TimeOut(30);
        private TimeOut m_tRespawn = new TimeOut(CHGMAP_LOCK_SECS);
        private TimeOut m_tDoGhost = new TimeOut(3);
        private TimeOut m_tRevive = new TimeOut(20);
        private TimeOutMS m_tStatusCheck = new TimeOutMS(500);
        private TimeOutMS m_tLastVigor = new TimeOutMS(1500);
        private TimeOut m_tItemCheck = new TimeOut(1);
#if DEBUG
        private TimeOut m_tBetaReward = new TimeOut(3600);
#endif

        private UserPackage m_userPackage;
        private RefineBreath m_chiSystem;
        private SubClass m_subClass;
        private GameAction m_gameAction;
        private Transformation m_pTransformation;
        private WeaponSkill m_pWeaponSkill;
        private UserMailBox m_pMailBox;
        private JiangHu m_pJiangHu;
        private UserAchievements m_pAchievement;

        private Syndicate m_pSyndicate;
        private SyndicateMember m_pSyndicateMember;

        private Family m_pFamily;
        private FamilyMember m_pFamilyMember;

        private List<uint> m_setTaskId = new List<uint>(8);

        public IRequestBox RequestBox;

        public ConcurrentDictionary<uint, TradePartner> TradePartners = new ConcurrentDictionary<uint, TradePartner>();
        public ConcurrentDictionary<uint, Relationship> Friends = new ConcurrentDictionary<uint, Relationship>();
        public ConcurrentDictionary<uint, Relationship> Enemies = new ConcurrentDictionary<uint, Relationship>();

        public Trade Trade = null;
        public PlayerBooth Booth = null;
        public PkExploit PkExploit = null;
        public PlayerQualifierUser PlayerQualifier => ServerKernel.ArenaQualifier.GetUser(Identity);
        public PlayerQualifierUser TeamPlayerQualifier => ServerKernel.TeamQualifier.GetUser(Identity);

        public Character(uint idRole, User pOwner, CharacterEntity entity)
            : base(idRole)
        {
            m_pOwner = pOwner;
            m_dbUser = entity;

            m_packet = new MsgPlayer(idRole);
        }

        #region Attributes

        #region Ownership

        public User Owner => m_pOwner;

        #endregion

        #region Identification

        public override uint Identity => m_dbUser?.Identity ?? 0u;

        public override string Name
        {
            get => m_dbUser?.Name ?? "Invalid";
            set
            {
                if (m_dbUser == null) return;
                m_dbUser.Name = value.Substring(0, Math.Min(MAX_USERNAME_LENGTH, value.Length));
                m_packet.SetNames(Name, Mate, FamilyName);
                Screen.RefreshSpawnForObservers();
                Save();
            }
        }

        public string Mate
        {
            get => m_dbUser?.Mate ?? "Invalid";
            set
            {
                if (m_dbUser == null)
                    return;
                m_dbUser.Mate = value.Substring(0, Math.Min(MAX_USERNAME_LENGTH, value.Length));
                m_packet.SetNames(Name, Mate, FamilyName);
                Save();
            }
        }

        #endregion

        #region Appearence

        public override EntityAction Action
        {
            get => m_packet.Action;
            set => m_packet.Action = value;
        }

        public override FacingDirection Direction
        {
            get => m_packet.Direction;
            set => m_packet.Direction = value;
        }

        public byte Gender => (byte) (Lookface % 10000 / 1000);

        public ushort Hair
        {
            get => m_dbUser?.Hair ?? 0;
            set
            {
                if (m_dbUser == null)
                    return;
                m_dbUser.Hair = value;
                m_packet.Hairstyle = value;
                UpdateClient(ClientUpdateType.HairStyle, value, true);
                Save();
            }
        }

        public override uint Lookface
        {
            get => m_dwLookface;
            set
            {
                if (m_dbUser == null) return;
                m_dwLookface = value;
                m_dbUser.Lookface = value % 10000000;
                m_packet.Mesh = value;
                UpdateClient(ClientUpdateType.Mesh, value, true);
                Save();
            }
        }

        public ushort Avatar
        {
            get => m_usAvatar;
            set
            {
                m_usAvatar = value;
                Lookface = (uint) (m_usTransformation * 10000000 + value * 10000 + m_usBody);
            }
        }

        public ushort Transformation
        {
            get => m_usTransformation;
            set
            {
                m_usTransformation = value;
                Lookface = (uint) ((uint) value * 10000000 + Avatar * 10000 + Body);
            }
        }

        public ushort Body
        {
            get => m_usBody;
            set
            {
                m_usBody = value;
                Lookface = (uint) (Transformation * 10000000 + Avatar * 10000 + value);
            }
        }

        #endregion

        #region Life and Mana

        public override uint Life
        {
            get => m_dbUser?.Life ?? 0;
            set
            {
                if (m_dbUser == null)
                    return;

                m_dbUser.Life = (ushort) Math.Min(ushort.MaxValue, Math.Min(MaxLife, value));
                UpdateClient(ClientUpdateType.Hitpoints, value);
                //Save();
            }
        }

        public override uint MaxLife => m_usMaxLife;

        public override uint Mana
        {
            get => m_dbUser?.Mana ?? 0;
            set
            {
                if (m_dbUser == null)
                    return;

                m_dbUser.Mana = (ushort) Math.Min(ushort.MaxValue, Math.Min(MaxMana, value));
                UpdateClient(ClientUpdateType.Mana, value);
            }
        }

        public override uint MaxMana => m_usMaxMana;

        #endregion

        #region Basic Attributes

        public ushort Force
        {
            get => m_dbUser?.Strength ?? 0;
            set
            {
                if (m_dbUser == null)
                    return;
                m_dbUser.Strength = value;
                UpdateClient(ClientUpdateType.Strength, value);
                Save();
            }
        }

        public ushort Agility
        {
            get => m_dbUser?.Agility ?? 0;
            set
            {
                if (m_dbUser == null)
                    return;
                m_dbUser.Agility = value;
                UpdateClient(ClientUpdateType.Agility, value);
                Save();
            }
        }

        public ushort Vitality
        {
            get => m_dbUser?.Vitality ?? 0;
            set
            {
                if (m_dbUser == null) return;
                m_dbUser.Vitality = value;
                UpdateClient(ClientUpdateType.Vitality, value);
                Save();
            }
        }

        public ushort Spirit
        {
            get => m_dbUser?.Spirit ?? 0;
            set
            {
                if (m_dbUser == null) return;
                m_dbUser.Spirit = value;
                UpdateClient(ClientUpdateType.Spirit, value);
                Save();
            }
        }

        public ushort AdditionalPoints
        {
            get => m_dbUser?.AdditionalPoints ?? 0;
            set
            {
                if (m_dbUser == null) return;
                m_dbUser.AdditionalPoints = value;
                UpdateClient(ClientUpdateType.Atributes, value);
                Save();
            }
        }

        public override byte Stamina
        {
            get => m_pStamina;
            set
            {
                m_pStamina = value;
                UpdateClient(ClientUpdateType.Stamina, value);
            }
        }

        public override byte MaxStamina => 100;

        public byte XpPoints
        {
            get => m_pXpPoints;
            set => m_pXpPoints = value;
        }

        #endregion

        #region Localization

        public Screen Screen => m_pScreen ?? (m_pScreen = new Screen(this));

        public bool HasSpawnFinished => !m_tRespawn.IsActive() || m_tRespawn.IsTimeOut();

        public override ushort MapX
        {
            get => m_usMapX;
            set => m_usMapX = m_packet.MapX = value;
        }

        public override ushort MapY
        {
            get => m_usMapY;
            set => m_usMapY = m_packet.MapY = value;
        }

        public uint RecordMapIdentity
        {
            get => m_dbUser.MapId;
            set => m_dbUser.MapId = value;
        }
        public ushort RecordMapX
        {
            get => m_dbUser.MapX;
            set => m_dbUser.MapX = value;
        }
        public ushort RecordMapY
        {
            get => m_dbUser.MapY;
            set => m_dbUser.MapY = value;
        }

        #endregion

        #region Level and Experience

        public override byte Level
        {
            get => m_dbUser?.Level ?? 0;
            set
            {
                if (m_dbUser == null) return;
                if (value > MAX_UPLEVEL)
                {
                    value = MAX_UPLEVEL;
                    if (value != m_dbUser.Level)
                        SendMessage(Language.MaxLevelReached);
                }

                m_dbUser.Level = value;
                m_packet.Level = value;
                UpdateClient(ClientUpdateType.Level, value);
                Save();

                DoLevelCheck();
            }
        }

        public override byte Metempsychosis
        {
            get => m_dbUser?.Metempsychosis ?? 0;
            set
            {
                if (m_dbUser == null) return;
                if (value > MAX_METEMPSYCHOSIS)
                    value = MAX_METEMPSYCHOSIS;
                m_dbUser.Metempsychosis = value;
                m_packet.Metempsychosis = value;
                UpdateClient(ClientUpdateType.Reborn, value);
                Save();
            }
        }

        public uint Reincarnation
        {
            get => m_dbUser.Reincarnation;
        }

        public ulong Experience
        {
            get => m_dbUser?.Experience ?? 0UL;
            set
            {
                if (m_dbUser == null || Level >= MAX_UPLEVEL)
                    return;
                m_dbUser.Experience = value;
                UpdateClient(ClientUpdateType.Experience, value);
                //Save();
            }
        }

        public ulong HangUpExperience { get; set; }

        #endregion

        #region Battle Attributes

        public uint KoCount { get; set; }

        public override int MinAttack => m_minAttack;
        public override int MaxAttack => m_maxAttack;
        public override int MagicAttack => m_magicAttack;
        public override int Defense => m_defense;
        public override int MagicDefense => m_magicDefense;
        public override float ReduceDamage => m_blessing;
        public override int Accuracy => m_dexterity;
        public override int AttackSpeed => m_attackSpeed;
        public override int Dodge => m_dodge;
        public override int MagicDefenseBonus => m_magicDefenseBonus;
        public override int FinalAttack => m_finalAttack;
        public override int FinalDefense => m_finalDefense;
        public override int FinalMagicAttack => m_finalMagicAttack;
        public override int FinalMagicDefense => m_finalMagicDefense;
        public override int CriticalStrike => m_criticalStrike;
        public override int SkillCriticalStrike => m_skillCriticalStrike;
        public override int Immunity => m_immunity;
        public override int Penetration => m_penetration;
        public override int Breakthrough => m_breakthrough;
        public override int Counteraction => m_counteraction;
        public override int Block => m_block;
        public override int Detoxication => m_detoxication;
        public override int FireResistance => m_fireResistance;
        public override int WaterResistance => m_waterResistance;
        public override int WoodResistance => m_woodResistance;
        public override int EarthResistance => m_earthResistance;
        public override int MetalResistance => m_metalResistance;

        public override double DragonGem => m_dragonGem;
        public override double PhoenixGem => m_phoenixGem;
        public override double RainbowGem => m_rainbowGem;
        public override double MoonGem => m_moonGem;
        public override double FuryGem => m_furyGem;
        public override double KylinGem => m_kylinGem;
        public override double VioletGem => m_violetGem;
        public override double TortoiseGem => Math.Min(0.54d, m_tortoiseGem);

        #endregion

        #region Pk

        public ushort PkPoints
        {
            get => m_dbUser?.PkPoints ?? 0;
            set
            {
                if (m_dbUser == null)
                    return;

                if (value > 99 && QueryStatus(FlagInt.BLACK_NAME) == null)
                {
                    DetachStatus(FlagInt.RED_NAME);
                    AttachStatus(this, FlagInt.BLACK_NAME, 0, int.MaxValue, 0, 0);
                }
                else if (value > 29 && QueryStatus(FlagInt.RED_NAME) == null)
                {
                    DetachStatus(FlagInt.BLACK_NAME);
                    AttachStatus(this, FlagInt.RED_NAME, 0, int.MaxValue, 0, 0);
                }
                else if (value < 30 && (QueryStatus(FlagInt.BLACK_NAME) != null || QueryStatus(FlagInt.RED_NAME) != null))
                {
                    DetachStatus(FlagInt.BLACK_NAME);
                    DetachStatus(FlagInt.RED_NAME);
                }

                m_dbUser.PkPoints = value;
                UpdateClient(ClientUpdateType.PkPoints, value);
                Save();
            }
        }

        public PkModeType PkMode { get; set; }

        #endregion

        #region Profession

        public byte ProfessionSort => (byte) ((m_dbUser?.Profession ?? 0) / 10);

        public byte ProfessionLevel => (byte) ((m_dbUser?.Profession ?? 0) % 10);

        public ProfessionType Profession
        {
            get => (ProfessionType) (m_dbUser?.Profession ?? 0);
            set
            {
                if (m_dbUser == null) return;
                if (!Enum.IsDefined(typeof(ProfessionType), value))
                    return;
                m_dbUser.Profession = (ushort) value;
                m_packet.Profession = value;
                UpdateClient(ClientUpdateType.Class, (ushort) value);
                Save();
            }
        }

        public ProfessionType FirstProfession
        {
            get => (ProfessionType) (m_dbUser?.FirstProfession ?? 0);
            set
            {
                if (m_dbUser == null) return;
                if (!Enum.IsDefined(typeof(ProfessionType), value))
                    return;
                m_dbUser.FirstProfession = (ushort) value;
                m_packet.FirstProfession = value;
                Save();
            }
        }

        public ProfessionType LastProfession
        {
            get => (ProfessionType) (m_dbUser?.LastProfession ?? 0);
            set
            {
                if (m_dbUser == null) return;
                if (!Enum.IsDefined(typeof(ProfessionType), value))
                    return;
                m_dbUser.LastProfession = (ushort) value;
                m_packet.LastProfession = value;
                Save();
            }
        }

        #endregion

        #region Equipment

        public bool IsAlternativeEquipment { get; set; }

        public UserPackage UserPackage => m_userPackage ?? (m_userPackage = new UserPackage(this));

        public uint Helmet
        {
            get => m_packet.Helmet;
            set => m_packet.Helmet = value;
        }

        public ItemColor HelmetColor
        {
            get => (ItemColor) m_packet.HelmetColor;
            set => m_packet.HelmetColor = (ushort) value;
        }

        public uint Armor
        {
            get => m_packet.Armor;
            set => m_packet.Armor = value;
        }

        public ItemColor ArmorColor
        {
            get => (ItemColor) m_packet.ArmorColor;
            set => m_packet.ArmorColor = (ushort) value;
        }

        public uint LeftHand
        {
            get => m_packet.LeftHand;
            set => m_packet.LeftHand = value;
        }

        public uint RightHand
        {
            get => m_packet.RightHand;
            set => m_packet.RightHand = value;
        }

        public ItemColor ShieldColor
        {
            get => (ItemColor) m_packet.ShieldColor;
            set => m_packet.ShieldColor = (ushort) value;
        }

        public uint Garment
        {
            get => m_packet.Garment;
            set => m_packet.Garment = value;
        }

        public uint RightHandAccessory
        {
            get => m_packet.RightAccessory;
            set => m_packet.RightAccessory = value;
        }

        public uint LeftHandAccessory
        {
            get => m_packet.LeftAccessory;
            set => m_packet.LeftAccessory = value;
        }

        public uint Steed
        {
            get => m_packet.MountType;
            set => m_packet.MountType = value;
        }

        public uint SteedColor
        {
            get => m_packet.MountColor;
            set => m_packet.MountColor = value;
        }

        public byte SteedAddition
        {
            get => m_packet.MountPlus;
            set => m_packet.MountPlus = value;
        }

        public uint SteedArmor
        {
            get => m_packet.MountArmor;
            set => m_packet.MountArmor = value;
        }

        public uint HelmetArtifact
        {
            get => m_packet.HelmetArtifact;
            set => m_packet.HelmetArtifact = value;
        }

        public uint ArmorArtifact
        {
            get => m_packet.ArmorArtifact;
            set => m_packet.ArmorArtifact = value;
        }

        public uint RightHandArtifact
        {
            get => m_packet.RightHandArtifact;
            set => m_packet.RightHandArtifact = value;
        }

        public uint LeftHandArtifact
        {
            get => m_packet.LeftHandArtifact;
            set => m_packet.LeftHandArtifact = value;
        }

        #endregion

        #region Currency

        public uint Silver
        {
            get => m_dbUser?.Money ?? 0;
            set
            {
                if (m_dbUser == null) return;
                
                m_dbUser.Money = Math.Min(MAX_MONEY_LIMIT, value);
                UpdateClient(ClientUpdateType.Money, m_dbUser.Money);
                Save();
            }
        }

        public uint Emoney
        {
            get => m_dbUser?.Emoney ?? 0;
            private set
            {
                if (m_dbUser == null) return;
                
                m_dbUser.Emoney = Math.Min(MAX_MONEY_LIMIT, value);
                UpdateClient(ClientUpdateType.ConquerPoints, m_dbUser.Emoney);
                Save();
            }
        }

        public uint BoundEmoney
        {
            get => m_dbUser?.BoundEmoney ?? 0;
            set
            {
                if (m_dbUser == null) return;
                
                m_dbUser.BoundEmoney = Math.Min(MAX_MONEY_LIMIT, value);
                UpdateClient(ClientUpdateType.BoundConquerPoints, m_dbUser.BoundEmoney);
                Save();
            }
        }

        public uint MoneySaved
        {
            get => m_dbUser?.MoneySaved ?? 0;
            set
            {
                if (m_dbUser == null) return;
                if (value > int.MaxValue)
                    Program.WriteLog($"({Identity}:{Name}) {value} MoneySaved (limit exceed)", LogType.WARNING);

                m_dbUser.MoneySaved = value;
                Save();
            }
        }

        public uint CoinMoney
        {
            get => m_dbUser?.CoinMoney ?? 0;
            set
            {
                if (m_dbUser == null) return;
                m_dbUser.CoinMoney = value;
                Save();

                if (value > 0)
                    Send(new MsgAction(Identity, 1197u, 0, 0, GeneralActionType.OpenCustom));
            }
        }

        #endregion

        #region Vip Features

        public byte VipLevel => Owner.VipLevel;

        #endregion

        #region Quiz

        public uint StudentPoints
        {
            get => m_dbUser?.StudentPoints ?? 0;
            set
            {
                if (m_dbUser == null) return;
                m_dbUser.StudentPoints = value;
                m_packet.QuizPoints = value;
                UpdateClient(ClientUpdateType.QuizPoints, value, true);
                Save();
            }
        }

        #endregion

        #region Login Completion

        public bool LoginComplete { get; set; }

        #endregion

        #region Chi

        public RefineBreath ChiSystem => m_chiSystem ?? (m_chiSystem = new RefineBreath(this));

        public uint ChiPoints
        {
            get => m_dbUser.ChiPoints;
            set
            {
                m_dbUser.ChiPoints = value;
                Save();
            }
        }

        #endregion

        #region Country

        public PlayerCountry Country
        {
            get => (PlayerCountry) m_dbUser.CountryFlag;
            set
            {
                m_dbUser.CountryFlag = (ushort) value;
                m_packet.CountryFlag = (uint) value;
                Save();
            }
        }

        #endregion

        #region Subclass

        public SubClass SubClass => m_subClass ?? (m_subClass = new SubClass(this));

        public uint StudyPoints
        {
            get => m_dbUser.StudyPoints;
            set
            {
                m_dbUser.StudyPoints = value;
                Save();
            }
        }

        public SubClasses ActiveSubclass
        {
            get => (SubClasses) m_dbUser.ActiveSubclass;
            set
            {
                m_dbUser.ActiveSubclass = (byte) value;
                Save();
            }
        }

        #endregion

        #region Item or NPC Interaction

        public ScreenObject InteractingNpc;
        public Item InteractingItem;

        #endregion

        #region Virtue Points

        public uint VirtuePoints
        {
            get => m_dbUser?.Virtue ?? 0;
            set
            {
                if (m_dbUser == null)
                    return;

                m_dbUser.Virtue = value;
                Save();
            }
        }

        #endregion

        #region Transformation

        public Transformation QueryTransformation
        {
            get => m_pTransformation;
            set => m_pTransformation = value;
        }

        #endregion

        #region WeaponSkill

        public WeaponSkill WeaponSkill => m_pWeaponSkill ?? (m_pWeaponSkill = new WeaponSkill(this));

        #endregion

        #region User Layout

        public byte CurrentLayout
        {
            get => m_dbUser.CurrentLayout;
            set
            {
                m_packet.CurrentLayout = m_dbUser.CurrentLayout = (byte) (value % 4);
                // Screen.RefreshSpawnForObservers();
                Save();
            }
        }

        #endregion

        #region Statistics

        //public ConcurrentDictionary<ulong, StatisticEntity> Statistics = new ConcurrentDictionary<ulong, StatisticEntity>();
        public UserStatistic Statistics;

        #endregion

        #region Title

        public UserTitle Title
        {
            get => (UserTitle) m_dbUser.SelectedTitle;
            set
            {
                m_dbUser.SelectedTitle = (byte) value;
                m_packet.Title = (byte) value;
                Screen.RefreshSpawnForObservers();
                Save();
            }
        }

        public ConcurrentDictionary<UserTitle, TitleEntity> Titles = new ConcurrentDictionary<UserTitle, TitleEntity>();

        #endregion

        #region Password

        public ulong WarehousePassword
        {
            get => m_dbUser.LockKey;
            set
            {
                m_dbUser.LockKey = value;
                Save();
            }
        }

        #endregion

        #region Flowers

        public uint FlowerCharm
        {
            get => m_packet?.FlowerRanking ?? 0;
            set
            {
                if (m_packet == null) return;
                m_packet.FlowerRanking = value;
                Screen.RefreshSpawnForObservers();
            }
        }

        public uint RedRoses
        {
            get => m_dbUser?.RedRoses ?? 0;
            set
            {
                if (m_dbUser == null) return;
                m_dbUser.RedRoses = value;
                Save();
            }
        }

        public uint WhiteRoses
        {
            get => m_dbUser?.WhiteRoses ?? 0;
            set
            {
                if (m_dbUser == null) return;
                m_dbUser.WhiteRoses = value;
                Save();
            }
        }

        public uint Orchids
        {
            get => m_dbUser?.Orchids ?? 0;
            set
            {
                if (m_dbUser == null) return;
                m_dbUser.Orchids = value;
                Save();
            }
        }

        public uint Tulips
        {
            get => m_dbUser?.Tulips ?? 0;
            set
            {
                if (m_dbUser == null) return;
                m_dbUser.Tulips = value;
                Save();
            }
        }

        #endregion

        #region Team

        public Team Team { get; set; }

        #endregion

        #region Status

        public bool Scapegoat { get; set; }

        public override ulong Flag1
        {
            get => m_packet.Flag1;
            set
            {
                m_packet.Flag1 = value;
                UpdateClient(ClientUpdateType.StatusFlag, Flag1, Flag2, Flag3, true);
            }
        }

        public override ulong Flag2
        {
            get => m_packet.Flag2;
            set
            {
                m_packet.Flag2 = value;
                UpdateClient(ClientUpdateType.StatusFlag, Flag1, Flag2, Flag3, true);
            }
        }

        public override uint Flag3
        {
            get => m_packet.Flag3;
            set
            {
                m_packet.Flag3 = value;
                UpdateClient(ClientUpdateType.StatusFlag, Flag1, Flag2, Flag3, true);
            }
        }

        #endregion

        #region Mount

        public uint Vigor
        {
            get => m_dwVigor;
            set
            {
                m_dwVigor = value > m_dwMaxVigor ? m_dwMaxVigor : value;

                MsgData pMsg = new MsgData();
                pMsg.WriteUInt(2, 8);
                pMsg.WriteUInt(m_dwVigor, 12);
                Send(pMsg);

                m_tLastVigor.Update();
            }
        }

        public uint MaxVigor => m_dwMaxVigor;

        #endregion

        #region Lottery

        public byte LotteryLastColor = 0;
        public byte LotteryLastType = 0;
        public byte LotteryLastRank = 0;
        public string LotteryLastItemName = "";
        public ItemEntity LotteryTemporaryItem = null;

        #endregion

        #region Honor

        public uint HonorPoints
        {
            get => m_dbUser.HonorPoints;
            set
            {
                m_dbUser.HonorPoints = value;
                Save();
            }
        }

        #endregion

        #region Family

        public Family Family
        {
            get => m_pFamily;
            set => m_pFamily = value;
        }

        public FamilyMember FamilyMember
        {
            get => m_pFamilyMember;
            set => m_pFamilyMember = value;
        }

        public uint FamilyIdentity
        {
            get => m_pFamily?.Identity ?? 0;
            set => m_packet.FamilyIdentity = value;
        }

        public string FamilyName
        {
            get => m_pFamily?.Name ?? Language.StrNone;
            set
            {
                m_packet.SetNames(Name, Mate, value);
                Screen.RefreshSpawnForObservers();
            }
        }

        public FamilyRank FamilyPosition
        {
            get => m_pFamilyMember?.Position ?? FamilyRank.None;
            set => m_packet.FamilyRank = value;
        }

        public uint FamilyBattlePower => 0;

        #endregion

        #region Achievement

        public UserAchievements Achievement => m_pAchievement;

        #endregion

        #endregion

        #region Initialization

        public bool Create()
        {
            m_pScreen = new Screen(this);

            #region Location

            m_idMap = m_dbUser.MapId;
            if (!ServerKernel.Maps.TryGetValue(MapIdentity, out m_pMap))
            {
                m_pMap = ServerKernel.Maps[1002];
                m_idMap = 1002;
                m_usMapX = 430;
                m_usMapY = 378;
            }
            else
            {
                m_usMapX = m_dbUser.MapX;
                m_usMapY = m_dbUser.MapY;
            }

#if !DEBUG
            if (IsGm())
            {
                m_pMap = ServerKernel.Maps[5000];
                m_idMap = 5000;
                m_usMapX = 50;
                m_usMapY = 60;
            }
#endif

            #endregion

            #region Appearence

            m_usBody = (ushort) (m_dbUser.Lookface % 10000);
            m_usAvatar = (ushort) ((m_dbUser.Lookface % 10000000 - m_usBody) / 10000);
            m_dwLookface = m_dbUser.Lookface;

            #endregion

            m_userPackage = new UserPackage(this);
            m_chiSystem = new RefineBreath(this);
            m_chiSystem.Init();
            m_subClass = new SubClass(this);
            m_gameAction = new GameAction(this);
            m_nobility = new Nobility(this);
            m_pMailBox = new UserMailBox(this);
            PkExploit = new PkExploit(this);
            PkExploit.Create();
            m_pJiangHu = new JiangHu(this);
            m_pAchievement = new UserAchievements(this);

            m_packet = new MsgPlayer(Identity)
            {
                Mesh = Lookface,
                MapX = MapX,
                MapY = MapY,
                Hairstyle = Hair,
                Action = EntityAction.Stand,
                Direction = Direction,
                Level = Level,
                Metempsychosis = Metempsychosis,
                QuizPoints = StudentPoints,
                FirstProfession = FirstProfession,
                LastProfession = LastProfession,
                Profession = Profession,
                Name = Name,
                Spouse = Mate,
                CountryFlag = (uint) Country,
                CurrentLayout = CurrentLayout,
                Away = IsAway,
                Flag1 = Flag1,
                Flag2 = Flag2,
                Flag3 = Flag3,
                Nobility = (byte) NobilityRank,
                Subclass = (byte) ActiveSubclass,
                Title = (byte) Title
            };

            m_tSynchro.Update();
            m_tXp.Update();
            m_tStamina.Update();

            BattleSystem = new BattleSystem(this);
            Magics = new MagicData(this);
            Statistics = new UserStatistic(this);
            Statistics.Initialize();

            if (Life == 0)
                Life = 1;

            m_dbUser.LastLogin = (uint) UnixTimestamp.Now();
            return true;
        }

        #endregion

        #region Currency

        public void AwardMoney(int amount)
        {
            if (amount <= 0)
                return;
            
            Silver += (uint) amount;
        }

        public bool SpendMoney(int amount, bool error = false)
        {
            if (amount <= 0 || Silver < amount)
            {
                if (error)
                    SendSysMessage(Language.StrNotEnoughMoney);
                return false;
            }

            Silver -= (uint) amount;
            return true;
        }

        public bool ChangeMoney(int amount)
        {
            if (amount == 0)
                return false;
            if (amount > 0)
            {
                AwardMoney(amount);
                return true;
            }

            return SpendMoney(amount * -1);
        }

        public void AwardEmoney(int amount, EmoneySourceType source, ScreenObject role)
        {
            if (amount <= 0)
                return;

            EmoneyLogEntity emoney = new EmoneyLogEntity
            {
                Amount = amount,
                MapIdentity = MapIdentity,
                MapX = MapX,
                MapY = MapY,
                OperationType = (ushort) EmoneyOperationType.Award,
                SourceType = (byte) source,
                TargetIdentity = Identity,
                TargetBalance = Emoney,
                Timestamp = DateTime.Now
            };
            if (role != null)
            {
                emoney.SourceIdentity = role.Identity;
                if (source == EmoneySourceType.User && role is Character user)
                {
                    emoney.SourceBalance = user.Emoney;
                }
            }

            new EmoneyLogRepository().Save(emoney);
            
            Emoney += (uint) amount;
        }

        public bool SpendEmoney(int amount, EmoneySourceType source, ScreenObject role, bool error = false)
        {
            if (amount <= 0 || Emoney < amount || !IsUnlocked())
            {
                if (error)
                    SendSysMessage(Language.StrNotEnoughCPs);
                return false;
            }

            EmoneyLogEntity emoney = new EmoneyLogEntity
            {
                Amount = amount * -1,
                MapIdentity = MapIdentity,
                MapX = MapX,
                MapY = MapY,
                OperationType = (ushort)EmoneyOperationType.Spend,
                SourceType = (byte) source,
                TargetIdentity = Identity,
                TargetBalance = Emoney,
                Timestamp = DateTime.Now
            };
            if (role != null)
            {
                emoney.SourceIdentity = role.Identity;
                if (source == EmoneySourceType.User && role is Character user)
                {
                    emoney.SourceBalance = user.Emoney;
                }
            }

            new EmoneyLogRepository().Save(emoney);

            Emoney -= (uint) amount;
            return true;
        }

        public bool ChangeEmoney(int amount, EmoneySourceType source, ScreenObject role)
        {
            if (amount == 0)
                return false;
            if (amount > 0)
            {
                AwardEmoney(amount, source, role);
                return true;
            }

            return SpendEmoney(amount * -1, source, role);
        }

        public void AwardBoundEmoney(int amount)
        {
            if (amount <= 0)
                return;

            BoundEmoney += (uint) amount;
        }

        public bool SpendBoundEmoney(int amount)
        {
            if (amount <= 0 || BoundEmoney < amount)
                return false;

            BoundEmoney -= (uint) amount;
            return true;
        }

        public bool ChangeBoundEmoney(int amount)
        {
            if (amount == 0)
                return false;
            if (amount > 0)
            {
                AwardBoundEmoney(amount);
                return true;
            }

            return SpendBoundEmoney(amount * -1);
        }

        public bool AwardMoneySaved(int amount)
        {
            if (amount <= 0 || MoneySaved + amount > MAX_MONEY_LIMIT)
                return false;

            if (MoneySaved + amount > MAX_MONEY_LIMIT)
                Program.WriteLog($"{Identity}:{Name} is over {int.MaxValue} BoundEmoney (value: {BoundEmoney})",
                    LogType.WARNING);

            MoneySaved += (uint) amount;
            return true;
        }

        public bool SpendMoneySaved(int amount)
        {
            if (amount <= 0 || MoneySaved < amount)
                return false;

            MoneySaved -= (uint) amount;
            return true;
        }

        public bool ChangeMoneySaved(int amount)
        {
            if (amount == 0)
                return false;
            if (amount > 0)
            {
                AwardMoneySaved(amount);
                return true;
            }

            return SpendMoneySaved(amount * -1);
        }

        public bool ChangeChiPoints(int amount)
        {
            if (amount == 0) return false;
            if (amount > 0)
            {
                AwardChiPoints(amount);
                return true;
            }
            return SpendChiPoints(amount * -1);
        }

        public void AwardChiPoints(int amount)
        {
            if (amount <= 0)
                return;

            if (ChiPoints + amount > int.MaxValue)
                Program.WriteLog($"{Identity}:{Name} is over {int.MaxValue} ChiPoints (value: {ChiPoints})",
                    LogType.WARNING);

            ChiPoints += (uint) amount;
        }

        public bool SpendChiPoints(int amount)
        {
            if (amount <= 0 || ChiPoints < amount)
                return false;

            ChiPoints -= (uint) amount;
            return true;
        }

        public void AwardStudyPoints(int amount)
        {
            if (amount <= 0)
                return;

            if (StudyPoints + amount > int.MaxValue)
                Program.WriteLog($"{Identity}:{Name} is over {int.MaxValue} StudyPoints (value: {ChiPoints})",
                    LogType.WARNING);

            Send(new MsgSubPro
            {
                Action = SubClassActions.UpdateStudy,
                AwardedStudy = (uint) amount
            });

            StudyPoints += (uint) amount;
        }

        public bool ReduceStudyPoints(long amount)
        {
            if (amount <= 0 || StudyPoints < amount)
                return false;

            StudyPoints -= (uint) amount;
            return true;
        }

        public bool ChangeStudyPoints(int amount)
        {
            if (amount == 0) return false;
            if (amount > 0)
            {
                AwardStudyPoints(amount);
                return true;
            }

            return ReduceStudyPoints(amount * -1);
        }

        public void AwardAttributePoints(int amount)
        {
            if (amount <= 0)
                return;

            if (AdditionalPoints + amount > short.MaxValue)
                Program.WriteLog($"{Identity}:{Name} is over {int.MaxValue} BoundEmoney (value: {BoundEmoney})",
                    LogType.WARNING);

            AdditionalPoints += (ushort) amount;
        }

        public bool SpendAttributePoints(int amount)
        {
            if (amount <= 0 || AdditionalPoints < amount)
                return false;

            AdditionalPoints -= (ushort) amount;
            return true;
        }

        public bool ChangeAttributePoints(int amount)
        {
            if (amount == 0)
                return false;
            if (amount > 0)
            {
                AwardAttributePoints(amount);
                return true;
            }

            return SpendAttributePoints(amount * -1);
        }

        public void AwardStamina(byte amount)
        {
            if (amount <= 0)
                return;
            Stamina = (byte) Math.Min(MaxStamina, Stamina + amount);
        }

        public bool SpendStamina(byte amount)
        {
            if (amount <= 0 || Stamina < amount)
                return false;

            Stamina -= amount;
            return true;
        }

        public bool ChangeStamina(short amount)
        {
            if (amount == 0)
            {
                Stamina = 0;
                return true;
            }

            if (amount > 0)
            {
                AwardStamina((byte) amount);
                return true;
            }

            return SpendStamina((byte) (amount * -1));
        }

        public void AwardPkPoints(ushort amount)
        {
            if (amount <= 0)
                return;
            PkPoints = (byte) Math.Min(MaxStamina, Stamina + amount);
        }

        public bool SpendPkPoints(ushort amount)
        {
            if (amount <= 0 || Stamina < amount)
                return false;

            PkPoints -= amount;
            return true;
        }

        public bool ChangePkPoints(short amount)
        {
            if (amount == 0)
            {
                PkPoints = 0;
                return true;
            }

            if (amount > 0)
            {
                AwardPkPoints((ushort) amount);
                return true;
            }

            return SpendPkPoints((ushort) (amount * -1));
        }

        #endregion

        #region XP

        public void ProcXpVal()
        {
            if (!IsAlive)
            {
                ClsXpVal();
                return;
            }

            IStatus pStatus = QueryStatus(Effect0.START_XP);
            if (pStatus != null)
                return;

            if (m_pXpPoints >= 100)
            {
                BurstXp();
                SetXp(0);
                m_tXp.Update();
            }
            else
            {
                if (Map != null && Map.IsBoothEnable())
                    return;
                AddXp(1);
            }
        }

        public bool BurstXp()
        {
            if (m_pXpPoints < 100)
                return false;

            IStatus pStatus = QueryStatus(Effect0.START_XP);
            if (pStatus != null)
                return true;

            AttachStatus(this, FlagInt.START_XP, 0, 20, 0, 0);
            return true;
        }

        public void SetXp(byte nXp)
        {
            if (nXp > 100 || QueryStatus(Effect0.START_XP) != null)
                return;
            XpPoints = nXp;
        }

        public void AddXp(byte nXp)
        {
            if (nXp <= 0 || !IsAlive || QueryStatus(Effect0.START_XP) != null)
                return;
            XpPoints += nXp;
        }

        public void ClsXpVal()
        {
            XpPoints = 0;
            Status.DelObj(Effect0.START_XP);
        }

        #endregion

        #region Variables

        /// <summary>
        /// Iterator is used for storing identities for action usage.
        /// </summary>
        public long Iterator = 0;
#if DEBUG
        public long[] Data = new long[8];
        public string[] DataString = new string[8];
#else
        // double think about this. Might require a higher amount of RAM memory on the server.
        // may not scale well.
        public long[] Data = new long[16];
        public string[] DataString = new string[16];
#endif

        #endregion

        #region Mate and Marriage

        public bool IsMate(Character user)
        {
            return user.Name == Mate;
        }

        public bool IsMate(string name)
        {
            return name == Mate;
        }

        #endregion

        #region Basic Attributes

        public bool ChangeForce(int amount)
        {
            if (amount < 0)
            {
                amount *= -1;
                if ((ushort) amount > Force)
                    return false;
                Force -= (ushort) amount;
                return true;
            }

            if ((ushort) amount + Force > ushort.MaxValue)
                return false;
            Force += (ushort) amount;
            return true;
        }

        public bool ChangeSpeed(int amount)
        {
            if (amount < 0)
            {
                amount *= -1;
                if ((ushort) amount > Agility)
                    return false;
                Agility -= (ushort) amount;
                return true;
            }

            if ((ushort) amount + Agility > ushort.MaxValue)
                return false;
            Agility += (ushort) amount;
            return true;
        }

        public bool ChangeHealth(int amount)
        {
            if (amount < 0)
            {
                amount *= -1;
                if ((ushort) amount > Vitality)
                    return false;
                Vitality -= (ushort) amount;
                return true;
            }

            if ((ushort) amount + Vitality > ushort.MaxValue)
                return false;
            Vitality += (ushort) amount;
            return true;
        }

        public bool ChangeSoul(int amount)
        {
            if (amount < 0)
            {
                amount *= -1;
                if ((ushort) amount > Spirit)
                    return false;
                Spirit -= (ushort) amount;
                return true;
            }

            if ((ushort) amount + Spirit > ushort.MaxValue)
                return false;
            Spirit += (ushort) amount;
            return true;
        }

        #endregion

        #region Level and Experience
        
        public bool AutoAllot
        {
            get => m_dbUser?.AutoAllot != 0;
            set
            {
                if (m_dbUser == null)
                    return;
                m_dbUser.AutoAllot = (byte) (value ? 1 : 0);
                Save();
            }
        }

        public bool IsNewbie()
        {
            return Level < 70;
        }

        public bool SetProfession(ushort prof)
        {
            if (prof <= 0
                || prof > 255)
                return false;
            Profession = (ProfessionType) prof;
            return true;
        }

        public bool IsPureClass()
        {
            return ProfessionSort == (int) LastProfession / 10 && ProfessionSort == (int) FirstProfession / 10;
        }

        public bool AwardLevel(ushort amount)
        {
            if (Level >= ServerKernel.MaxLevel)
                return false;

            if (Level + amount <= 0)
                return false;

            int addLev = amount;
            if (addLev + Level > ServerKernel.MaxLevel)
                addLev = ServerKernel.MaxLevel - Level;

            if (addLev < 0)
                return false;

            AdditionalPoints += (ushort) (addLev * 3);
            Level += (byte) addLev;
            Screen.Send(new MsgAction(Identity, 0, MapX, MapY, GeneralActionType.Leveled), true);
            return true;
        }

        public bool AwardExperience(long amount)
        {
            if (Level > ServerKernel.LevelExperience.Count)
                return true;

            if (QueryStatus(FlagInt.AUTO_HUNTING) != null)
                HangUpExperience += (ulong) amount;

            amount += (long) Experience;
            bool leveled = false;
            uint pointAmount = 0;
            byte newLevel = Level;
            while (newLevel < ServerKernel.MaxLevel && amount >= (long) ServerKernel.LevelExperience[newLevel].Exp)
            {
                amount -= (long) ServerKernel.LevelExperience[newLevel].Exp;
                leveled = true;
                newLevel++;
                if (!AutoAllot || Level > 120)
                {
                    pointAmount += 3;
                    continue;
                }

                if (newLevel < ServerKernel.MaxLevel) continue;
                amount = 0;
                break;
            }

            uint metLev = 0;
            var leveXp = ServerKernel.LevelExperience.Values.FirstOrDefault(x => x.Level == newLevel);
            if (leveXp != null)
            {
                float fExp = amount / (float)leveXp.Exp;
                metLev = (uint)(newLevel * 10000 + fExp * 1000);
            }

            byte checkLevel = (byte)(m_dbUser.Reincarnation > 0 ? 110 : 130);
            if (newLevel >= checkLevel && Metempsychosis > 0 && m_dbUser.MeteLevel > metLev)
            {
                byte extra = 0;
                if (newLevel >= checkLevel && m_dbUser.MeteLevel/10000 > newLevel)
                {
                    var mete = m_dbUser.MeteLevel / 10000;
                    extra += (byte) (mete - newLevel);
                    pointAmount += (uint) (extra * 3);
                    leveled = true;
                    amount = 0;
                }

                newLevel += extra;

                if (newLevel >= ServerKernel.MaxLevel)
                {
                    newLevel = (byte) ServerKernel.MaxLevel;
                    amount = 0;
                }
                else if (m_dbUser.MeteLevel/* / 10000*/ >= newLevel * 10000)
                {
                    amount = (long) (ServerKernel.LevelExperience[newLevel].Exp * ((m_dbUser.MeteLevel % 10000) / 1000d));
                }
            }

            if (leveled)
            {
                byte job;
                if ((int) Profession > 100)
                    job = 10;
                else
                    job = (byte) (((int) Profession - (int) Profession % 10) / 10);

                var allot = GetPointAllot(job, newLevel);
                Level = newLevel;
                if (AutoAllot && allot != null)
                {
                    Force = allot.Strength;
                    Agility = allot.Agility;
                    Vitality = allot.Vitality;
                    Spirit = allot.Spirit;
                }

                if (pointAmount > 0)
                    AwardAttributePoints((int) pointAmount);

                UpdateAttributes();
                Life = MaxLife;
                Mana = MaxMana;
                Screen.Send(new MsgAction(Identity, 0, 0, 0, GeneralActionType.Leveled), true);

                if (ProfessionSort == 5)
                {
                    if (Magics[11230] == null)
                        Magics.Create(11230, 0);
                }
            }

            Experience = (ulong)amount;
            return true;
        }

        public void DoLevelCheck()
        {
            if (m_pJiangHu == null)
                m_pJiangHu = new JiangHu(this);

            if (!m_pJiangHu.HasStarted && Metempsychosis >= 2 && Level > 29)
            {
                Send(new MsgOwnKongfuBase
                {
                    Mode = KongfuBaseMode.IconBar
                });
            }
        }
         
        public void AwardBattleExp(long nExp, bool bGemEffect)
        {
            if (Metempsychosis == 2)
                nExp /= 3;

            if (QueryStatus(FlagInt.OBLIVION) != null)
                m_lAccumulateExp += nExp;

            float mult = 1f + BattlePower/100f;
            if (IsBlessed)
                mult += .3f;
            if (HasMultipleExp)
                mult += ExperienceMultiplier;
            if (VipLevel > 1 && VipLevel < 3)
                mult += 1;
            else if (VipLevel >= 3)
            {
                mult += 1;
                mult *= 2;
            }

            nExp = (long) (nExp * mult);

            if (nExp == 0)
                return;

            if (nExp < 0)
            {
                AddAttrib(ClientUpdateType.Experience, nExp);
                return;
            }

            if (bGemEffect)
            {
                nExp += (int) (nExp * (1 + (GetExpGemEffect()/100d)));

                if (m_rainbowGem > 0 && IsPm())
                    SendSysMessage($"got gem exp add percent: {GetExpGemEffect():0.00}%");
            }

            if (Level >= ServerKernel.MaxLevel)
                return;

            if (Level >= 120)
                nExp /= 2;

            if (IsPm())
                SendSysMessage($"got battle exp: {nExp}");

            AwardExperience(nExp);
        }

        public long AdjustExperience(Role pTarget, long nRawExp, bool bNewbieBonusMsg)
        {
            if (pTarget == null) return 0;
            long nExp = nRawExp;
            nExp = BattleSystem.AdjustExp(nExp, Level, pTarget.Level);
            DynamicNpc pNpc = null;
            if (pTarget is DynamicNpc npc)
                pNpc = npc;

            if (pNpc?.IsGoal() == true)
                nExp /= 10;
            
            if (m_rainbowGem > 0)
                nExp = (int) (nExp * (1 + m_rainbowGem));
            return nExp;
        }

        public void ResetOblivion()
        {
            if (QueryStatus(FlagInt.OBLIVION) != null)
                DetachStatus(FlagInt.OBLIVION);
            m_nKoCount = 0;
            m_lAccumulateExp = 0;
        }

        public void AwardOblivion(bool bFull = true)
        {
            if (m_lAccumulateExp == 0)
                return;
            m_nKoCount = 0;
            if (bFull)
                AwardBattleExp(m_lAccumulateExp * 2, true);
            else
                AwardBattleExp(m_lAccumulateExp, true);
        }

        private PointAllotEntity GetPointAllot(byte job, byte newLevel)
        {
            return ServerKernel.PointAllot.FirstOrDefault(x => x.Level == newLevel && x.Profession == job);
        }

        public void AddWeaponSkillExp(ushort usType, int nExp)
        {
            if (!WeaponSkill.Skills.TryGetValue(usType, out WeaponSkillEntity skill))
            {
                WeaponSkill.Create(usType, 0);
                if (!WeaponSkill.Skills.TryGetValue(usType, out skill))
                    return;
            }

            if (skill.Level >= MAX_WEAPONSKILLLEVEL)
                return;

            nExp = (int) (nExp * (1 + RainbowGem));

            uint nIncreaseLev = 0;
            if (skill.Level > MASTER_WEAPONSKILLLEVEL)
            {
                int nRatio = (int) (100 - (skill.Level - MASTER_WEAPONSKILLLEVEL) * 20);
                if (nRatio < 10)
                    nRatio = 10;
                nExp = Calculations.MulDiv(nExp, nRatio, 100) / 2;
            }

            int nNewExp = (int) Math.Max(nExp + skill.Experience, skill.Experience);

#if DEBUG
            if (IsPm())
                SendSysMessage($"Add Weapon Skill exp: {nExp}, CurExp: {nNewExp}");
#endif

            int nLevel = (int) skill.Level;
            if (nLevel >= 1 && nLevel < MAX_WEAPONSKILLLEVEL)
            {
                if (nNewExp > MsgWeaponSkill.EXP_PER_LEVEL[nLevel] ||
                    nLevel >= skill.OldLevel / 2 && nLevel < skill.OldLevel)
                {
                    nNewExp = 0;
                    nIncreaseLev = 1;
                }
            }

            if (skill.Level < Level / 10 + 1
                || skill.Level >= MASTER_WEAPONSKILLLEVEL)
            {
                skill.Experience = (uint) nNewExp;

                if (nIncreaseLev > 0)
                {
                    skill.Level += nIncreaseLev;
                    Send(new MsgWeaponSkill(skill.Type, skill.Level, skill.Experience));
                    SendSysMessage(Language.StrWeaponSkillUp);
                }
                else
                {
                    Send(new MsgWeaponSkill(skill.Type, skill.Level, skill.Experience));
                }

                Database.WeaponSkillRepository.Save(skill);
            }
        }

        public long CalculateExpBall(int amount = EXPBALL_AMOUNT)
        {
            long exp = 0;

            if (Level >= ServerKernel.MaxLevel)
                return 0;

            byte level = Level;
            if (Experience > 0)
            {
                double pct = 1.00 - Experience / (double) ServerKernel.LevelExperience[Level].Exp;
                if (amount > pct * ServerKernel.LevelExperience[Level].UpLevTime)
                {
                    amount -= (int) (pct * ServerKernel.LevelExperience[Level].UpLevTime);
                    exp += (long) (ServerKernel.LevelExperience[Level].Exp - Experience);
                    level++;
                }
            }
            
            while (amount > ServerKernel.LevelExperience[level].UpLevTime)
            {
                amount -= ServerKernel.LevelExperience[level].UpLevTime;
                exp += (long) ServerKernel.LevelExperience[level].Exp;

                if (level >= ServerKernel.MaxLevel)
                    return exp;
                level++;
            }

            exp += (long) (amount / (double) ServerKernel.LevelExperience[Level].UpLevTime *
                           ServerKernel.LevelExperience[Level].Exp);
            return exp;
        }

        public void IncrementExpBall()
        {
            m_dbUser.ExpBallUsage = uint.Parse(DateTime.Now.ToString("yyyyMMdd"));
            m_dbUser.ChkSum += 1;
            Save();
        }

        public bool CanUseExpBall()
        {
            if (Level >= ServerKernel.MaxLevel)
                return false;

            if (m_dbUser.ExpBallUsage < uint.Parse(DateTime.Now.ToString("yyyyMMdd")))
            {
                m_dbUser.ChkSum = 0;
                return Save();
            }
            return m_dbUser.ChkSum < 10;
        }

        #region Rebirth

        public void FixAttributes()
        {
            if (Metempsychosis < 1 || Level < 15)
            {
                SendSysMessage(Language.StrNoFirstMetempsychosis);
                return;
            }

            byte prof = (byte)((byte)Profession / 10 >= 10 ? 10 : (((byte)Profession - ((byte)Profession % 10)) / 10));
            var pData = ServerKernel.PointAllot.FirstOrDefault(x => x.Profession == prof && x.Level == 1);
            if (pData == null)
            {
                Program.WriteLog($"Could not fetch attribute points data. ResetAttrPoints for user {Identity}");
                return;
            }

            Force = pData.Strength;
            Agility = pData.Agility;
            Vitality = pData.Vitality;
            Spirit = pData.Spirit;

            ushort usAdd = 0;
            if (Metempsychosis == 1)
            {
                usAdd = (ushort)(30 + Math.Min((1 + (130 - 120)) * (130 - 120) / 2, 55));
                usAdd += (ushort)((Level - 15) * 3);
            }
            else if (Metempsychosis == 2)
            {
                byte met1 = (byte)((m_dbUser.MeteLevel - (m_dbUser.MeteLevel % 10000)) / 10000);
                usAdd = (ushort)(30 + Math.Min((1 + (met1 - 120)) * (met1 - 120) / 2, 55));
                usAdd += (ushort)(52 + Math.Min((1 + (met1 - 120)) * (met1 - 120) / 2, 55));
                usAdd += (ushort)((Level - 15) * 3);
            }

            if (Statistics != null)
            {
                if (Statistics.GetValue(10) != 0) // primeiro rb
                {
                    usAdd += 50;
                }

                if (Statistics.GetValue(61, 1) == 4) // segundo rb
                {
                    usAdd += 100;
                }
            }

            AdditionalPoints = (ushort)(usAdd - 10);
        }

        public int GetRebirthAddPoint(int nOldProf, int nOldLevel, int metempsychosis)
        {
            ushort usAdd = 0;
            if (metempsychosis == 0)
            {
                if (nOldProf == HIGHEST_WATER_WIZARD_PROF)
                    usAdd = (ushort)(30 + Math.Min((1 + (nOldLevel - 110) / 2) * ((nOldLevel - 110) / 2) / 2, 55));
                else
                    usAdd = (ushort)(30 + Math.Min((1 + (nOldLevel - 120)) * (nOldLevel - 120) / 2, 55));
            }
            else if (metempsychosis == 1)
            {
                byte firstMetempsychosisLev = (byte)((m_dbUser.MeteLevel - (m_dbUser.MeteLevel % 10000)) / 10000);
                if ((ushort) FirstProfession == HIGHEST_WATER_WIZARD_PROF)
                    usAdd = (ushort)(30 + Math.Min((1 + (firstMetempsychosisLev - 110) / 2) * ((firstMetempsychosisLev - 110) / 2) / 2, 55));
                else
                    usAdd = (ushort)(30 + Math.Min((1 + (firstMetempsychosisLev - 120)) * (nOldLevel - 120) / 2, 55));
                if (nOldProf == HIGHEST_WATER_WIZARD_PROF)
                    usAdd += (ushort)(52 + Math.Min((1 + (nOldLevel - 110) / 2) * ((nOldLevel - 110) / 2) / 2, 55));
                else
                    usAdd += (ushort)(52 + Math.Min((1 + (nOldLevel - 120)) * (nOldLevel - 120) / 2, 55));
            }

            if (Statistics.GetValue(10) != 0)
            {
                usAdd += 50;
            }

            if (Statistics.GetValue(61, 1) == 4)
            {
                usAdd += 100;
            }

            return usAdd;
        }

        public void ResetAttrPoints()
        {
            //byte prof = (byte)(Profession >= ProfessionType.InternTaoist ? 10 : (byte)(((ushort) Profession - ((ushort) Profession % 10)) / 10));
            //if (prof > 10) prof = 10;
            //var pData = ServerKernel.PointAllot.FirstOrDefault(x => x.Profession == prof && x.Level == 1);
            //if (pData == null)
            //{
            //    return;
            //}
            //ushort totalPoints = (ushort)(AdditionalPoints + Force + Agility + Vitality + Spirit - 10);

            //Force = pData.Strength;
            //Agility = pData.Agility;
            //Vitality = pData.Vitality;
            //Spirit = pData.Spirit;
            //AdditionalPoints = totalPoints;
            FixAttributes();
        }

        public bool Rebirth(ushort nProf, ushort nLook)
        {
            byte rebirth = (byte)(Metempsychosis == 2 ? 2 : Metempsychosis + 1); ;
            var pData = ServerKernel.Rebirths.Values.FirstOrDefault(x => x.NeedProfession == (ushort) Profession && x.NewProfession == nProf && x.Metempsychosis == rebirth);

            if (pData != null)
            {
                if (Level < pData.NeedLevel)
                {
                    SendSysMessage(Language.StrNotEnoughLevel);
                    return false;
                }

                if (Level >= 130) // old stats (mete_lev) level * 10000 + exp percent
                {
                    var leveXp = ServerKernel.LevelExperience.Values.FirstOrDefault(x => x.Level == Level);
                    if (leveXp != null)
                    {
                        float fExp = Experience / (float) leveXp.Exp;
                        uint metLev = (uint) (Level * 10000 + fExp * 1000);
                        if (metLev > m_dbUser.MeteLevel)
                            m_dbUser.MeteLevel = metLev;
                    }
                    else if (Level == ServerKernel.MaxLevel)
                        m_dbUser.MeteLevel = (uint)ServerKernel.MaxLevel * 10000;
                }

                ushort oldProf = (ushort) Profession;
                ResetUserAttribute((ushort) Profession, Level, (byte)(Metempsychosis == 2 ? 2 : rebirth - 1), nProf, nLook);

                foreach (var item in UserPackage.GetAllEquipment())
                    item.DegradeItem(false);

                var removeSkills = ServerKernel.Magictypeops.Values.FirstOrDefault(x => x.ProfessionAgo == oldProf && x.ProfessionNow / 10 == (ushort) Profession / 10 && x.Operation == 0);
                var reloadSkills = ServerKernel.Magictypeops.Values.FirstOrDefault(x => x.ProfessionAgo == oldProf && x.ProfessionNow / 10 == (ushort)Profession / 10 && x.Operation == 2);
                var learnSkills = ServerKernel.Magictypeops.Values.FirstOrDefault(x => x.ProfessionAgo == oldProf && x.ProfessionNow / 10 == (ushort)Profession / 10 && x.Operation == 7);

                if (removeSkills != null)
                {
                    foreach (var magic in removeSkills.Magics)
                    {
                        Magics.Delete(magic);
                    }
                }

                if (reloadSkills != null)
                {
                    foreach (var magic in reloadSkills.Magics)
                    {
                        Magic _magic = Magics[magic];
                        if (_magic == null)
                            continue;
                        _magic.OldLevel = (byte) _magic.Level;
                        _magic.Level = 0;
                    }
                }

                if (learnSkills != null)
                {
                    foreach (var magic in learnSkills.Magics)
                        Magics.Create(magic, 0);
                }

                if (UserPackage[ItemPosition.LeftHand] != null && !UserPackage[ItemPosition.LeftHand].IsArrowSort())
                    UserPackage.Unequip(ItemPosition.LeftHand);

                UpdateAttributes();

                return true;
            }
            return false;
        }

        public bool Reincarnate(ushort nProf, ushort nLook)
        {
            const byte rebirth = 2;
            var pData = ServerKernel.Rebirths.Values.FirstOrDefault(x => x.NeedProfession == (int) Profession && x.NewProfession == nProf && x.Metempsychosis == rebirth);

            if (pData != null)
            {
                if (Level < 110)
                {
                    SendSysMessage(Language.StrNotEnoughLevel);
                    return false;
                }

                if (Level >= 110) // old stats (mete_lev) level * 10000 + exp percent
                {
                    var leveXp = ServerKernel.LevelExperience.Values.FirstOrDefault(x => x.Level == Level);
                    if (leveXp != null)
                    {
                        float fExp = (Experience / (float) leveXp.Exp);
                        uint metLev = (uint) ((uint)Level * 10000 + (fExp*1000));
                        if (metLev > m_dbUser.MeteLevel)
                            m_dbUser.MeteLevel = metLev;
                    }
                    else if (Level == ServerKernel.MaxLevel)
                        m_dbUser.MeteLevel = (uint)ServerKernel.MaxLevel * 10000;
                }

                ushort oldProf = (ushort) Profession;
                ushort firstProf = (ushort)(((int) FirstProfession - ((int) FirstProfession % 10)) / 10);
                ResetUserAttribute((ushort) Profession, Level, 2, nProf, nLook);
                firstProf = (ushort)(firstProf * 10 + 1);
                if (firstProf > 100)
                    firstProf += 1;

                foreach (var item in UserPackage.GetAllEquipment())
                    item.DegradeItem(false);

                var removeSkills = ServerKernel.Magictypeops.Values.FirstOrDefault(x => x.ProfessionAgo == oldProf && x.ProfessionNow / 10 == (ushort) Profession / 10 && x.Operation == 0);
                var reloadSkills = ServerKernel.Magictypeops.Values.FirstOrDefault(x => x.ProfessionAgo == oldProf && x.ProfessionNow / 10 == (ushort) Profession / 10 && x.Operation == 2);
                var learnSkills = ServerKernel.Magictypeops.Values.FirstOrDefault(x => x.ProfessionAgo == oldProf && x.ProfessionNow / 10 == (ushort) Profession / 10 && x.Operation == 7);
                var firstLifeSkills = ServerKernel.Magictypeops.Values.FirstOrDefault(x => x.ProfessionNow == firstProf && x.Operation == 4);

                if (removeSkills != null)
                {
                    foreach (var magic in removeSkills.Magics)
                    {
                        Magics.Delete(magic);
                    }
                }

                if (firstLifeSkills != null && reloadSkills != null)
                {
                    foreach (var magic in firstLifeSkills.Magics)
                    {
                        if (!reloadSkills.Magics.Contains(magic))
                            Magics.Delete(magic);
                    }
                }

                if (reloadSkills != null)
                {
                    foreach (var magic in reloadSkills.Magics)
                    {
                        var _magic = Magics[magic];
                        if (_magic == null)
                            continue;
                        _magic.OldLevel = (byte) _magic.Level;
                        _magic.Level = 0;
                    }
                }

                if (learnSkills != null)
                {
                    foreach (var magic in learnSkills.Magics)
                        Magics.Create(magic, 0);
                }

                if (UserPackage[ItemPosition.LeftHand] != null && !UserPackage[ItemPosition.LeftHand].IsArrowSort())
                    UserPackage.Unequip(ItemPosition.LeftHand);

                UpdateAttributes();

                new ActionRepository().ExecuteQuery($"INSERT INTO cq_log (`user_id`,`task_id`,`profession`,`level`,`time`) " +
                                                    $"VALUES ('{Identity}','{TaskIdentity.REINCARNATION}','{(int) Profession}','{Level}','{UnixTimestamp.Now()}')");

                m_dbUser.Reincarnation += 1;
                Save();

                ServerKernel.Log.GmLog("reincarnation", $"{Name} has reincarnated from {LastProfession} to {Profession}");

                ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrMeteSpecialSuccess, Name), ChatTone.System);
                return true;
            }
            return false;
        }

        public void ResetUserAttribute(ushort nOldProf, byte nOldLev, byte nMete, ushort nNewProf, ushort nNewLook)
        {
            if (nNewProf == 0) nNewProf = (ushort)((int) Profession / 10 * 10 + 1);
            byte prof = (byte)(nNewProf > 100 ? 10 : nNewProf / 10);

            var pointAllot = ServerKernel.PointAllot.FirstOrDefault(x => x.Profession == prof && x.Level == 1);
            if (pointAllot != null)
            {
                Force = pointAllot.Strength;
                Agility = pointAllot.Agility;
                Vitality = pointAllot.Vitality;
                Spirit = pointAllot.Spirit;
            }
            else if (prof == 1)
            {
                Force = 5;
                Agility = 2;
                Vitality = 3;
                Spirit = 0;
            }
            else if (prof == 2)
            {
                Force = 5;
                Agility = 2;
                Vitality = 3;
                Spirit = 0;
            }
            else if (prof == 4)
            {
                Force = 2;
                Agility = 7;
                Vitality = 1;
                Spirit = 0;
            }
            else if (prof == 5)
            {
                Force = 7;
                Agility = 2;
                Vitality = 4;
                Spirit = 0;
            }
            else if (prof == 6)
            {
                Force = 7;
                Agility = 2;
                Vitality = 4;
                Spirit = 0;
            }
            else if (prof == 7)
            {
                Force = 4;
                Agility = 3;
                Vitality = 3;
                Spirit = 0;
            }
            else if (prof > 10)
            {
                Force = 0;
                Agility = 2;
                Vitality = 3;
                Spirit = 5;
            }

            AutoAllot = false;
            int attrPoints = GetRebirthAddPoint((int) Profession, Level, nMete >= 2 ? 1 : nMete);
            AdditionalPoints = (ushort)attrPoints;
            if (nNewLook > 0 && nNewLook != Lookface)
                Lookface = (Lookface - (Lookface % 10)) + nNewLook;
            Level = 15;
            Experience = 0;
            // UpdateAttributes();
            Life = MaxLife;
            Mana = MaxMana;
            Stamina = 100;

            if (nMete == 0)
            {
                FirstProfession = Profession;
                SetProfession(nNewProf);
                Metempsychosis = 1;
            }
            else if (nMete == 1)
            {
                LastProfession = Profession;
                SetProfession(nNewProf);
                Metempsychosis = 2;
            }
            else
            {
                FirstProfession = LastProfession;
                LastProfession = Profession;
                SetProfession(nNewProf);
            }

            if (Profession >= ProfessionType.InternMonk && Profession <= ProfessionType.NirvanaMonk)
                Hair = 0;

            UpdateClient(ClientUpdateType.Reborn, Metempsychosis);
            Save();
        }

        #endregion

        #endregion

        #region Metempsychosis

        #endregion

        #region Battle

        #region Crime

        public bool IsPeaceModeDisabled()
        {
            if (MapIdentity == 1927 || MapIdentity == 2055 || MapIdentity == 2056) // Grottos Exception
                return true;
            return Map.IsSynMap() || Map.IsFamilyMap() || Map.IsMineField() || Map.IsPkField() || Map.IsPkGameMap() || Map.IsPrisionMap();
        }

        public void SetCrimeStatus(uint nTime, int flag = FlagInt.BLUE_NAME)
        {
            AttachStatus(this, flag, 0, (int) nTime, 0, 0);
        }

        #endregion

        #region Reborn and Life

        public bool CanRevive()
        {
            return !IsAlive && m_tRevive.IsTimeOut() && QueryStatus(FlagInt.SHACKLED) == null;
        }

        public void Reborn(bool chgMap, bool isSpell = false)
        {
            if (IsAlive || !CanRevive() && !isSpell) // || Status[FlagInt.SHACKLED] != null) // shackle in 5103 is funny
            {
                if (QueryStatus(FlagInt.GHOST) != null)
                {
                    DetachStatus(FlagInt.GHOST);
                }

                if (QueryStatus(FlagInt.DEAD) != null)
                {
                    DetachStatus(FlagInt.DEAD);
                }

                if (Transformation == 98 || Transformation == 99)
                    ClearTransformation();
                return;
            }

            BattleSystem.ResetBattle();
            DetachStatus(FlagInt.GHOST);
            DetachStatus(FlagInt.DEAD);
            ClearTransformation();

            Stamina = 100;
            Life = (ushort) MaxLife;
            Mana = MaxMana;

            if (chgMap || !IsBlessed && !isSpell)
            {
                FlyMap(m_dbUser.MapId, m_dbUser.MapX, m_dbUser.MapY);
            }
            else
            {
                if (!isSpell && (m_pMap.IsPrisionMap()
                                 || m_pMap.IsPkField()
                                 || m_pMap.IsPkGameMap()
                                 || m_pMap.IsSynMap()))
                {
                    FlyMap(m_dbUser.MapId, m_dbUser.MapX, m_dbUser.MapY);
                }
                else
                {
                    FlyMap(m_idMap, m_usMapX, m_usMapY);
                }
            }

            GameAction.ProcessAction(8000002, this, this, null, null);

            m_tRespawn.SetInterval(CHGMAP_LOCK_SECS);
            m_tRespawn.Update();
        }

        #endregion

        #endregion

        #region Syndicate

        public uint SyndicateIdentity
        {
            get => m_pSyndicate?.Identity ?? 0u;
            set => m_packet.GuildIdentity = (ushort) value;
        }

        public string SyndicateName => m_pSyndicate?.Name ?? "None";

        public string SyndicateLeaderName => m_pSyndicate?.LeaderName ?? "None";

        public uint SyndicateLeaderIdentity => m_pSyndicate?.LeaderIdentity ?? 0u;

        public Syndicate Syndicate
        {
            get { return m_pSyndicate; }
            set
            {
                m_pSyndicate = value;
                if (value != null)
                {
                    SyndicateIdentity = value.Identity;
                }
            }
        }

        public SyndicateMember SyndicateMember
        {
            get => m_pSyndicateMember;
            set
            {
                m_pSyndicateMember = value;
                if (value != null)
                {
                    SyndicateIdentity = value.SyndicateIdentity;
                    SyndicatePosition = value.Position;
                }
            }
        }

        public SyndicateRank SyndicatePosition
        {
            get => SyndicateMember?.Position ?? SyndicateRank.None;
            set
            {
                if (!Enum.IsDefined(typeof(SyndicateRank), value))
                    m_packet.GuildRank = SyndicateRank.Member;
                else
                {
                    m_packet.GuildRank = value;
                }
            }
        }

        public void CreateSyndicate(string szName, uint dwMoney = 1000000)
        {
            if (Syndicate != null || SyndicateMember != null)
                return;

            if (!Handlers.CheckName(szName))
            {
                SendSysMessage(Language.StrSynForbbidenName);
                return;
            }

            if (ServerKernel.SyndicateManager.GetSyndicate(szName) != null)
            {
                SendSysMessage(Language.StrSynNameAlreadyInUse);
                return;
            }

            if (!SpendMoney((int) dwMoney, true))
            {
                return;
            }

            var newSyn = new Syndicate();
            if (!newSyn.Create(szName, this))
            {
                ServerKernel.Log.GmLog("synerror", $"Could not create syn {szName} - {Identity}");
                AwardMoney((int) dwMoney);
                return;
            }

            newSyn.Save();
            var newLeader = new SyndicateMember(newSyn);
            if (!newLeader.Create(this))
            {
                ServerKernel.Log.GmLog("synerror",
                    $"Could not create synmember {newSyn.Identity} - {newSyn.Name} - {Identity}");
                newSyn.Delete();
                AwardMoney((int) dwMoney);
                return;
            }

            newLeader.Position = SyndicateRank.GuildLeader;
            newLeader.IncreaseMoney(500000);
            newLeader.Save();

            newSyn.Members.TryAdd(newLeader.Identity, newLeader);

            ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrSyndicateCreate, szName, Name),
                Color.White, ChatTone.Talk);
            if (!ServerKernel.SyndicateManager.AddNewSyndicate(newSyn) || !ServerKernel.SyndicateManager.AddSyndicateMember(newLeader))
            {
                SendSysMessage(Language.StrSynAddError);
            }

            Syndicate = newSyn;
            SyndicateMember = newLeader;
            SyndicateIdentity = newSyn.Identity;
            SyndicatePosition = newLeader.Position;

            SendSyndicate();
            Syndicate.SendName(this, true);

            Screen.RefreshSpawnForObservers();
        }

        public void DisbandSyndicate()
        {
            if (Syndicate == null || SyndicateMember == null)
                return;

            if (Syndicate.LeaderIdentity != Identity)
                return;

            if (Syndicate.MemberCount > 1)
            {
                SendSysMessage(Language.StrNoDisband);
                return;
            }

            Syndicate.DisbandSyndicate(this);
        }

        public void SendSyndicate()
        {
            if (Syndicate != null && SyndicateMember != null)
            {
                SyndicateMember.SendSyndicate();
                var announce = new MsgTalk(Syndicate.Announcement, ChatTone.GuildAnnouncement);
                Send(announce);
            }
        }

        public bool QuitSyndicate()
        {
            if (Syndicate == null || SyndicateMember == null)
                return false;

            if (SyndicateMember.Position == SyndicateRank.GuildLeader)
            {
                SendSysMessage(Language.StrSynLeaderCannotQuit);
                return false;
            }

            if (SyndicateMember.SilverDonation < 20000)
            {
                SendSysMessage(Language.StrSynQuitNoDonation);
                return false;
            }

            SyndicateMember target;
            if (!Syndicate.Members.TryRemove(Identity, out target))
                return false;

            Syndicate.MemberCount -= 1;
            ushort usId = (ushort) SyndicateIdentity;
            SyndicateMember.Delete();
            SyndicateMember = null;

            Send(new MsgSyndicate
            {
                Action = SyndicateRequest.SYN_DISBAND,
                Param = usId
            });

            Screen.RefreshSpawnForObservers();

            ServerKernel.Log.GmLog("syndicate", $"(userid:{Identity}, name:{Name}) has quit (synid:{usId})");
            return true;
        }

        #endregion

        #region Family

        public bool CreateFamily(string szName, uint dwPrice, uint dwDonation)
        {
            if (Family != null || FamilyMember != null)
                return false;

            if (!Handlers.CheckName(szName))
            {
                SendSysMessage(Language.StrFamilyInvalidName);
                return false;
            }

            if (ServerKernel.Families.Values.FirstOrDefault(x => x.Name == szName) != null)
            {
                SendSysMessage(Language.StrFamilyNameAlreadyInUse);
                return false;
            }

            if (Silver < dwPrice)
            {
                SendSysMessage(Language.StrNotEnoughMoney);
                return false;
            }

            Family fam = new Family();
            if (!fam.Create(this, szName))
            {
                return false;
            }

            FamilyMember mem = new FamilyMember(fam);
            if (!mem.Create(this))
            {
                return false;
            }
            mem.Position = FamilyRank.ClanLeader;

            SpendMoney((int) dwPrice, true);
            fam.MoneyFunds += dwDonation;
            mem.Donation += dwDonation;

            fam.Members.TryAdd(Identity, mem);

            fam.Save();
            mem.Save();

            Family = fam;
            FamilyMember = mem;
            FamilyIdentity = fam.Identity;
            FamilyName = fam.Name;
            FamilyPosition = mem.Position;

            fam.SendFamily(this);
            fam.SendRelation(this);

            try
            {
                Screen.RefreshSpawnForObservers();
            }
            catch
            {
                ServerKernel.Log.SaveLog("Error refreshing screen after family creation");
            }
            return ServerKernel.Families.TryAdd(fam.Identity, fam);
        }

        public bool DisbandFamily()
        {
            if (Family == null || FamilyMember == null)
                return false;

            if (FamilyMember.Position != FamilyRank.ClanLeader)
            {
                SendSysMessage(Language.StrFamilyNotLeader);
                return false;
            }

            if (Family.MembersCount > 1)
            {
                SendSysMessage(Language.StrFamilyTooBig);
                return false;
            }

            Family.Delete();
            FamilyMember.Delete();

            FamilyIdentity = 0;
            FamilyName = Language.StrNone;
            FamilyPosition = 0;

            Screen.RefreshSpawnForObservers();

            SendEmptyFamily();

            Family = null;
            FamilyMember = null;
            return true;
        }

        public void SendEmptyFamily()
        {
            MsgFamily pMsg = new MsgFamily
            {
                Identity = Identity,
                Type = FamilyType.Info
            };
            pMsg.AddString(string.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11}",
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0));
            pMsg.AddString("");
            pMsg.AddString(Name);
            Send(pMsg);

            pMsg.Type = FamilyType.Quit;
            Send(pMsg);
        }

        #endregion

        #region Transformation

        public bool Transform(uint dwLook, int nKeepSecs, bool bSynchro)
        {
            bool bBack = false;

            if (QueryTransformation != null)
            {
                QueryTransformation = null;
                ClearTransformation();
                bBack = true;
            }

            MonstertypeEntity pType;
            if (!ServerKernel.Monsters.TryGetValue(dwLook, out pType))
                return false;

            Transformation pTransform = new Transformation(this);
            if (pTransform.Create(pType))
            {
                QueryTransformation = pTransform;
                Transformation = (ushort) pTransform.Lookface;
                m_tTransformation = new TimeOut(nKeepSecs);
                m_tTransformation.Startup(nKeepSecs);
                if (bSynchro)
                    SynchroTransform();
            }
            else
            {
                pTransform = null;
            }

            StopMine();

            if (bBack)
                SynchroTransform();

            return false;
        }

        public void ClearTransformation()
        {
            Transformation = 0;
            QueryTransformation = null;
            m_tTransformation.Clear();
        }

        public bool SynchroTransform()
        {
            UpdateClient(ClientUpdateType.Mesh, Lookface, true);
            UpdateClient(ClientUpdateType.Hitpoints, Life, true);
            UpdateClient(ClientUpdateType.MaxHitpoints, MaxLife, true);
            return true;
        }

        public void SetGhost()
        {
            if (IsAlive) return;
            ushort trans = 98;
            if (Gender == 2)
                trans = 99;
            Transformation = trans;
        }

        #endregion

        #region Mining

        public void Mine()
        {
            if (!IsAlive)
                return;

            if (m_tMine.IsActive())
                return;

            if (QueryTransformation != null)
                return;

            if (UserPackage[ItemPosition.RightHand] == null || !UserPackage[ItemPosition.RightHand].IsPick)
            {
                SendSysMessage(Language.StrMineWithPecker);
                return;
            }

            m_tMine.Startup(3);
            m_nMineCount = 0;
        }

        public void StopMine()
        {
            m_tMine.Clear();
            m_nMineCount = 0;
        }

        public void ProcessMineTimer()
        {
            if (!IsAlive)
            {
                StopMine();
                return;
            }

            if (UserPackage[ItemPosition.RightHand] == null || !UserPackage[ItemPosition.RightHand].IsPick)
            {
                SendSysMessage(Language.StrMineWithPecker);
                StopMine();
                return;
            }

            float nChance = 15f;
            if (WeaponSkill.Skills.ContainsKey(562))
                nChance += (float) WeaponSkill.Skills[562].Level / 2;

            Random pRand = new Random();

            uint idItem = 0;

            if (UserPackage.IsPackSpare(1) && Calculations.ChanceCalc(nChance))
            {
                // todo create and handle all mines
                // all mines will drop the same shit for now

                if (MapIdentity == 1028) // tc mine
                {
                    if (Calculations.ChanceCalc(5f)) // euxenite
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.EUXINITE_ORE));
                        SendSysMessage(Language.StrEuxeniteOre);
                    }
                    else if (Calculations.ChanceCalc(2f)) // Meteor
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_METEOR));
                        SendSysMessage(Language.StrGainedMeteor);
                    }
                    else if (Calculations.ChanceCalc(1f)) // DragonBall
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_DRAGONBALL));
                        SendSysMessage(Language.StrMiningDragonBall);
                    }
                    else if (Calculations.ChanceCalc(0.05f)) // Power Exp Ball
                    {
                        UserPackage.AwardItem(Item.CreateEntity(723744));
                        ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrMiningPowerExpBall, Name),
                            ChatTone.Talk);
                    }
                    else if (Calculations.ChanceCalc(1f)) // Triple Exp Potion
                    {
                        UserPackage.AwardItem(Item.CreateEntity(720393));
                        SendSysMessage(Language.StrMiningExpPill);
                    }
                    else if (Calculations.ChanceCalc(1f)) // +1 Stone
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_STONE1));
                        SendSysMessage(Language.StrMiningStone1);
                    }
                    else if (Calculations.ChanceCalc(1f)) // 500,000 silvers
                    {
                        AwardMoney(300000);
                        SendSysMessage(Language.StrMining300kGold);
                    }
                    else if (Calculations.ChanceCalc(1f)) // random quality tem
                    {
                        UserPackage.AwardItem(Item.CreateEntity(GenerateRandomGem()));
                        SendSysMessage(Language.StrMiningRandomGem);
                    }
                    else if (Calculations.ChanceCalc(5f)) // Copper Ore
                    {
                        UserPackage.AwardItem(Item.CreateEntity((uint) (SpecialItem.COPPER_ORE + pRand.Next(0, 9))));
                        SendSysMessage(Language.StrCopperOre);
                    }
                    else if (Calculations.ChanceCalc(30f)) // Iron Ore
                    {
                        UserPackage.AwardItem(Item.CreateEntity((uint) (SpecialItem.IRON_ORE + pRand.Next(0, 9))));
                        SendSysMessage(Language.StrIronOre);
                    }
                    else if (Calculations.ChanceCalc(0.55f))
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.PERMANENT_STONE));
                        ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrMinePermanentStone, Name), Color.White);
                    }
                }
                else if (MapIdentity == 1025) // Phoenix
                {
                    if (Calculations.ChanceCalc(2f)) // silver
                    {
                        AwardMoney(500000);
                        SendSysMessage(Language.StrMining500kGold);
                    }
                    else if (Calculations.ChanceCalc(2f))
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_STONE1));
                        SendSysMessage(Language.StrMiningStone1);
                    }
                    else if (Calculations.ChanceCalc(2f)) // Meteor
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_METEOR));
                        SendSysMessage(Language.StrGainedMeteor);
                    }
                    else if (Calculations.ChanceCalc(1f)) // DragonBall
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_DRAGONBALL));
                        SendSysMessage(Language.StrMiningDragonBall);
                    }
                    else if (Calculations.ChanceCalc(10f)) // Copper
                    {
                        UserPackage.AwardItem(Item.CreateEntity((uint) (SpecialItem.COPPER_ORE + pRand.Next(0, 9))));
                        SendSysMessage(Language.StrCopperOre);
                    }
                    else if (Calculations.ChanceCalc(5f)) // Gold
                    {
                        UserPackage.AwardItem(Item.CreateEntity((uint) (SpecialItem.GOLD_ORE + pRand.Next(0, 9))));
                        SendSysMessage(Language.StrGoldOre);
                    }
                    else if (Calculations.ChanceCalc(30f)) // Iron
                    {
                        UserPackage.AwardItem(Item.CreateEntity((uint) (SpecialItem.IRON_ORE + pRand.Next(0, 9))));
                        SendSysMessage(Language.StrIronOre);
                    }
                    else if (Calculations.ChanceCalc(1f)) // random quality tem
                    {
                        UserPackage.AwardItem(Item.CreateEntity(GenerateRandomGem()));
                        SendSysMessage(Language.StrMiningRandomGem);
                    }
                    else if (Calculations.ChanceCalc(0.65f))
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.PERMANENT_STONE));
                        ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrMinePermanentStone, Name), Color.White);
                    }
                }
                else if (MapIdentity == 1026) // Ape
                {
                    if (Calculations.ChanceCalc(0.5f)) // silver
                    {
                        AwardMoney(1500000);
                        SendSysMessage(Language.StrMining1500kGold);
                    }
                    else if (Calculations.ChanceCalc(2f))
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_STONE1));
                        SendSysMessage(Language.StrMiningStone1);
                    }
                    else if (Calculations.ChanceCalc(1f))
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_STONE2));
                        SendSysMessage(Language.StrMiningStone2);
                    }
                    else if (Calculations.ChanceCalc(2f)) // Meteor
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_METEOR));
                        SendSysMessage(Language.StrGainedMeteor);
                    }
                    else if (Calculations.ChanceCalc(1f)) // DragonBall
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_DRAGONBALL));
                        SendSysMessage(Language.StrMiningDragonBall);
                    }
                    else if (Calculations.ChanceCalc(10f)) // Copper
                    {
                        UserPackage.AwardItem(Item.CreateEntity((uint) (SpecialItem.COPPER_ORE + pRand.Next(0, 9))));
                        SendSysMessage(Language.StrCopperOre);
                    }
                    else if (Calculations.ChanceCalc(5f)) // Gold
                    {
                        UserPackage.AwardItem(Item.CreateEntity((uint) (SpecialItem.GOLD_ORE + pRand.Next(0, 9))));
                        SendSysMessage(Language.StrGoldOre);
                    }
                    else if (Calculations.ChanceCalc(30f)) // Iron
                    {
                        UserPackage.AwardItem(Item.CreateEntity((uint) (SpecialItem.IRON_ORE + pRand.Next(0, 9))));
                        SendSysMessage(Language.StrIronOre);
                    }
                    else if (Calculations.ChanceCalc(1f)) // random quality tem
                    {
                        UserPackage.AwardItem(Item.CreateEntity(GenerateRandomGem()));
                        SendSysMessage(Language.StrMiningRandomGem);
                    }
                    else if (Calculations.ChanceCalc(0.65f))
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.PERMANENT_STONE));
                        ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrMinePermanentStone, Name), Color.White);
                    }
                }
                else if (MapIdentity == 1027) // Desert
                {
                    if (Calculations.ChanceCalc(0.5f)) // silver
                    {
                        AwardMoney(1000000);
                        SendSysMessage(Language.StrMining1kkGold);
                    }
                    else if (Calculations.ChanceCalc(2f))
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_STONE1));
                        SendSysMessage(Language.StrMiningStone1);
                    }
                    else if (Calculations.ChanceCalc(1f))
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_STONE2));
                        SendSysMessage(Language.StrMiningStone2);
                    }
                    else if (Calculations.ChanceCalc(2f)) // Meteor
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_METEOR));
                        SendSysMessage(Language.StrGainedMeteor);
                    }
                    else if (Calculations.ChanceCalc(1f)) // DragonBall
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_DRAGONBALL));
                        SendSysMessage(Language.StrMiningDragonBall);
                    }
                    else if (Calculations.ChanceCalc(10f)) // Copper
                    {
                        UserPackage.AwardItem(Item.CreateEntity((uint) (SpecialItem.COPPER_ORE + pRand.Next(0, 9))));
                        SendSysMessage(Language.StrCopperOre);
                    }
                    else if (Calculations.ChanceCalc(5f)) // Gold
                    {
                        UserPackage.AwardItem(Item.CreateEntity((uint) (SpecialItem.GOLD_ORE + pRand.Next(0, 9))));
                        SendSysMessage(Language.StrGoldOre);
                    }
                    else if (Calculations.ChanceCalc(30f)) // Iron
                    {
                        UserPackage.AwardItem(Item.CreateEntity((uint) (SpecialItem.IRON_ORE + pRand.Next(0, 9))));
                        SendSysMessage(Language.StrIronOre);
                    }
                    else if (Calculations.ChanceCalc(5f))
                    {
                        UserPackage.AwardItem(Item.CreateEntity((uint) (SpecialItem.SILVER_ORE + pRand.Next(0, 9))));
                        SendSysMessage(Language.StrIronOre);
                    }
                    else if (Calculations.ChanceCalc(0.3f))
                    {
                        int amount = pRand.Next(15, 215);
                        AwardEmoney(amount, EmoneySourceType.Mining, this);
                        SendMessage(string.Format(Language.StrMiningCPs, amount), ChatTone.Talk);
                    }
                    else if (Calculations.ChanceCalc(1f)) // random quality tem
                    {
                        UserPackage.AwardItem(Item.CreateEntity(GenerateRandomGem()));
                        SendSysMessage(Language.StrMiningRandomGem);
                    }
                    else if (Calculations.ChanceCalc(0.75f))
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.PERMANENT_STONE));
                        ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrMinePermanentStone, Name), Color.White);
                    }
                }
                else if (MapIdentity == 1218) // Adventure Zone
                {
                    if (Calculations.ChanceCalc(0.25f)) // silver
                    {
                        AwardMoney(5000000);
                        SendSysMessage(Language.StrMining5kkGold);
                    }
                    else if (Calculations.ChanceCalc(2f))
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_STONE1));
                        SendSysMessage(Language.StrMiningStone1);
                    }
                    else if (Calculations.ChanceCalc(1f))
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_STONE2));
                        SendSysMessage(Language.StrMiningStone2);
                    }
                    else if (Calculations.ChanceCalc(1f))
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_STONE3));
                        SendSysMessage(Language.StrMiningStone3);
                    }
                    else if (Calculations.ChanceCalc(2f)) // Meteor
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_METEOR));
                        SendSysMessage(Language.StrGainedMeteor);
                    }
                    else if (Calculations.ChanceCalc(1f)) // DragonBall
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_DRAGONBALL));
                        SendSysMessage(Language.StrMiningDragonBall);
                    }
                    else if (Calculations.ChanceCalc(10f)) // Copper
                    {
                        UserPackage.AwardItem(Item.CreateEntity((uint) (SpecialItem.COPPER_ORE + pRand.Next(0, 9))));
                        SendSysMessage(Language.StrCopperOre);
                    }
                    else if (Calculations.ChanceCalc(5f)) // Gold
                    {
                        UserPackage.AwardItem(Item.CreateEntity((uint) (SpecialItem.GOLD_ORE + pRand.Next(0, 9))));
                        SendSysMessage(Language.StrGoldOre);
                    }
                    else if (Calculations.ChanceCalc(30f)) // Iron
                    {
                        UserPackage.AwardItem(Item.CreateEntity((uint) (SpecialItem.IRON_ORE + pRand.Next(0, 9))));
                        SendSysMessage(Language.StrIronOre);
                    }
                    else if (Calculations.ChanceCalc(5f))
                    {
                        UserPackage.AwardItem(Item.CreateEntity((uint) (SpecialItem.SILVER_ORE + pRand.Next(0, 9))));
                        SendSysMessage(Language.StrIronOre);
                    }
                    else if (Calculations.ChanceCalc(0.2f))
                    {
                        int amount = pRand.Next(30, 430);
                        AwardEmoney(amount, EmoneySourceType.Mining, this);
                        SendMessage(string.Format(Language.StrMiningCPs, amount), ChatTone.Talk);
                    }
                    else if (Calculations.ChanceCalc(0.2f))
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_METEOR_SCROLL));
                        SendSysMessage("You found a Meteor Scroll.");
                    }
                    else if (Calculations.ChanceCalc(0.2f))
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.TYPE_DRAGONBALL_SCROLL));
                        SendSysMessage("You found a DragonBall Scroll.");
                    }
                    else if (Calculations.ChanceCalc(1f)) // random quality tem
                    {
                        UserPackage.AwardItem(Item.CreateEntity(GenerateRandomGem()));
                        SendSysMessage(Language.StrMiningRandomGem);
                    }
                    else if (Calculations.ChanceCalc(0.55f))
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.PERMANENT_STONE));
                        ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrMinePermanentStone, Name), Color.White);
                    }
                    else if (Calculations.ChanceCalc(0.225f))
                    {
                        UserPackage.AwardItem(Item.CreateEntity(SpecialItem.BIGPERMANENT_STONE));
                        ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrMineBigPermanentStone, Name), Color.White);
                    }
                }
            }

            Screen.Send(new MsgAction(Identity, 0, MapX, MapY, GeneralActionType.Mine), true);
        }

        private uint GenerateRandomGem()
        {
            uint idItem = (uint) (700000 + ThreadSafeRandom.RandGet(0, 7) * 10);
            if (Calculations.ChanceCalc(5f))
                idItem += 3;
            else if (Calculations.ChanceCalc(20f))
                idItem += 2;
            else
                idItem += 1;
            return idItem;
        }

        #endregion

        #region Equipment and Inventory

        public bool AddItem(Item item)
        {
            if (item == null)
                return false;

            if (item.PlayerIdentity != Identity)
                item.PlayerIdentity = Identity;

            if (!m_userPackage.AddItem(item, false))
                return false;

            Send(item.CreateMsgItemInfo());
            return true;
        }

        #endregion

        #region Map Item

        public bool DropItem(uint idItem, int x, int y, bool force = false)
        {
            var pos = new Point(x, y);
            if (!Map.FindDropItemCell(9, ref pos))
                return false;

            Item pItem = UserPackage[idItem];
            //bool bDropItem;
            if (pItem == null)
                return false;

            ServerKernel.Log.GmLog("drop_item",
                $"{Name}({Identity}) drop item:[id={pItem.Identity}, type={pItem.Type}], dur={pItem.Durability}, max_dur={pItem.MaxDurability}\r\n\t{pItem.ToJson()}");

            if ((pItem.CanBeDropped || force) && pItem.DisappearWhenDropped)
                return UserPackage.RemoveFromInventory(pItem.Identity, RemovalType.Delete);

            if (pItem.CanBeDropped || force)
            {
                UserPackage.RemoveFromInventory(pItem.Identity, RemovalType.RemoveAndDisappear);
                //bDropItem = true;
            }
            else
            {
                SendSysMessage(Language.StrItemCannotDiscard);
                return false;
            }

            pItem.PlayerIdentity = 0;
            pItem.OwnerIdentity = Identity;

            //if (bDropItem)
            {
                var pMapItem = new MapItem((uint) ServerKernel.RoleManager.FloorIdentity.GetNextIdentity);
                if (pMapItem.Create(Map, pos, pItem, Identity))
                {
                    //Map.AddItem(pMapItem);
                }
                else
                {
                    ServerKernel.RoleManager.FloorIdentity.ReturnIdentity(pMapItem.Identity);
                    SendSysMessage(Language.StrItemFailedCreate);
                    pMapItem = null;
                }
            }

            return true;
        }

        public bool PickMapItem(uint idItem)
        {
            MapItem pMapItem = Map.QueryRole(idItem) as MapItem;
            if (pMapItem == null)
                return false;

            if (GetDistance(pMapItem.MapX, pMapItem.MapY) >= 1)
            {
                SendSysMessage(Language.StrTargetNotInRange);
                return false;
            }

            if (!pMapItem.IsMoney()
                && !UserPackage.IsPackSpare(1))
            {
                SendSysMessage(Language.StrYourInventoryIsFull);
                return false;
            }

            uint idOwner = pMapItem.GetPlayerId();
            if (pMapItem.IsPriv() && idOwner != Identity)
            {
                var pOwner = Map.QueryRole(idOwner) as Character;
                if (pOwner != null && !IsMate(pOwner))
                {
                    if (Team != null && Team.IsTeamMember(pOwner))
                    {
                        if (pMapItem.IsMoney() && Team.IsCloseMoney ||
                            (pMapItem.Type == SpecialItem.TYPE_DRAGONBALL ||
                             pMapItem.Type == SpecialItem.TYPE_METEOR) && Team.IsCloseGem
                            || !pMapItem.IsMoney() && pMapItem.Type != SpecialItem.TYPE_DRAGONBALL &&
                            pMapItem.Type != SpecialItem.TYPE_METEOR && Team.IsCloseItem)
                        {
                            SendSysMessage(Language.StrCantPickupOtherItems);
                            return false;
                        }
                    }
                    else
                    {
                        SendSysMessage(Language.StrCantPickupOtherItems);
                        return false;
                    }
                }
                else if (pOwner != null && pOwner != this)
                {
                    if (!IsMate(pOwner))
                    {
                        SendSysMessage(Language.StrCantPickupOtherItems);
                        return false;
                    }
                }
            }

            if (pMapItem.IsMoney())
            {
                AwardMoney((int) pMapItem.GetAmount());
                if (pMapItem.GetAmount() > 1000)
                    Send(new MsgAction(Identity, pMapItem.GetAmount(), MapX, MapY, GeneralActionType.GetMoney));
                SendSysMessage(string.Format(Language.StrPickMoney, pMapItem.GetAmount()));
            }
            else
            {
                var pItem = pMapItem.GetInfo(this);
                if (pItem != null)
                {
                    //Inventory.Add(pItem);
                    UserPackage.AddItem(pItem);

                    ServerKernel.Log.GmLog("pick_item",
                        $"{Name}({Identity}) pick item:[id={pItem.Identity}, type={pItem.Type}], dur={pItem.Durability}, max_dur={pItem.MaxDurability}");
                    SendSysMessage(string.Format(Language.StrGotItem, pItem.Itemtype.Name));
                }
            }

            ServerKernel.RoleManager.RemoveRole(pMapItem.Identity);

            var msg = new MsgMapItem
            {
                DropType = DropType.Pickup,
                Identity = pMapItem.Identity,
                MapX = pMapItem.MapX,
                MapY = pMapItem.MapY
            };
            Map.SendToRegion(msg, pMapItem.MapX, pMapItem.MapY);
            msg.DropType = DropType.Disappear;
            Map.SendToRegion(msg, pMapItem.MapX, pMapItem.MapY);
            return true;
        }

        #endregion

        #region PM/GM

        public bool IsAdministrator()
        {
            return Owner?.Authority >= 7;
        }

        public bool IsPm()
        {
            return Name.Contains("[PM]");
        }

        public bool IsGm()
        {
            return Name.Contains("[GM]") || IsPm();
        }

        public bool CanExecuteCommands => false;

        #endregion

        #region Movement

        #region Teletransport

        public override void EnterMap()
        {
            if (ServerKernel.Maps.TryGetValue(m_idMap, out Map map))
            {
                m_pMap = map;

                Map.EnterRoom(this);
                Map.SendMapInfo(this);

                m_tRespawn.SetInterval(CHGMAP_LOCK_SECS);
                m_tRespawn.Update();

                UpdateItems();
                UpdateAttributes();
            }
            else
            {
                ServerKernel.Log.GmLog("cheat", $"{Identity}:{Name} invalid map {m_idMap}");
                ServerKernel.UserManager.KickoutSocket(this);
            }
        }

        public override void LeaveMap()
        {
            if (Magics.QueryMagic() != null)
                Magics.AbortMagic(true);

            if (BattleSystem.IsActive())
                BattleSystem.ResetBattle();

            Screen.RemoveFromObservers();
            Map?.LeaveRoom(Identity);
        }

        public void UpdateItems()
        {
            foreach (var item in UserPackage.GetAll())
                item.UserChangeMap();
        }

        #endregion

        #region Location

        public void ChangeDirection(FacingDirection dir)
        {
            Direction = dir;
        }

        public bool IsSynchro()
        {
            return !(m_tSynchro.IsActive() && !m_tSynchro.TimeOver());
        }

        public void SetSynchro(bool bSynchro)
        {
            if (bSynchro)
                m_tSynchro.Clear();
            else
                m_tSynchro.Startup(SYNCHRO_SECS);
        }

        public void SetRecordPos(uint map, ushort x, ushort y)
        {
            m_dbUser.MapId = map;
            m_dbUser.MapX = x;
            m_dbUser.MapY = y;
        }

        public bool Kickback()
        {
            if (!IsSynchro())
                return false;

            Send(new MsgAction(Identity, (uint) Direction, MapX, MapY, GeneralActionType.NewCoordinates));
            SetSynchro(false);
            return true;
        }

        public bool ChangeMap(uint idx)
        {
            int[] nX = {0, 1, -1, 1, 0, 1, -1, -1, 0};
            int[] nY = {0, 1, -1, 0, 1, -1, 1, 0, -1};

            uint map = 0;
            Point pos = new Point();
            if (!Map.GetPassageMap(ref map, ref pos, idx)
                || !FlyMap(map, pos.X, pos.Y))
            {
                FlyMap(1002, 430, 378);
                ServerKernel.UserManager.KickoutSocket(this, $"could not ChangeMap from [{MapIdentity}:{MapX},{MapY}]");
                return false;
            }

            return true;
        }

        public bool FlyMap(uint id, int x, int y)
        {
            if (Map == null)
                return false;

            if (id == 0)
                id = MapIdentity;

            if (!ServerKernel.Maps.TryGetValue(id, out Map map))
            {
                Program.WriteLog($"FLY to map [{id}] dont exist or invalid coord [{x}][{y}]");
                return false;
            }

            Map.SendToRegion(new MsgAction(Identity, 0, 0, 0, GeneralActionType.RemoveEntity), MapX, MapY, 24);
            LeaveMap();
            m_idMap = map.Identity;
            MapX = (ushort) x;
            MapY = (ushort) y;
            Send(new MsgAction(Identity, map.MapDoc, MapX, MapY, GeneralActionType.ChangeMap));
            EnterMap();

            if (PkMode == PkModeType.Peace && IsPeaceModeDisabled())
            {
                ChangePkMode(PkModeType.Capture);
            }

            return true;
        }

        public bool SynPosition(ushort x, ushort y, int nMaxDislocation)
        {
            if (nMaxDislocation <= 0 || x == 0 && y == 0) // ignore in this condition
                return true;

            int nDislocation = GetDistance(x, y);
            if (nDislocation >= nMaxDislocation)
                return false;

            if (nDislocation <= 0)
                return true;

            if (IsGm())
                SendSysMessage($"syn move: ({MapX},{MapY})->({x},{y})", Color.Red);

            if (!Map.IsValidPoint(x, y))
                return false;

            Screen.Send(new MsgAction(Identity, (uint) Direction, x, y, GeneralActionType.NewCoordinates), false);

            ProcessOnMove(MovementType.SYNCHRO);

            return true;
        }

        public bool IsInTwinCity()
        {
            return MapIdentity == 1002 && MapX >= 348 && MapX <= 500 && MapY >= 212 && MapY <= 420;
        }

        #endregion

        #region Activity

        public bool IsAway
        {
            get => m_bIsAway;
            set
            {
                m_bIsAway = value;
                m_packet.Away = value;
            }
        }

        #endregion

        #endregion

        #region Update User Attributes

        #region Update

        public void UpdateAttributes()
        {
            #region Reset Attributes

            m_minAttack = 0;
            m_minAttackEx = 0;
            m_maxAttack = 0;
            m_maxAttackEx = 0;
            m_magicAttack = 0;
            m_magicAttackEx = 0;
            m_defense = 0;
            m_magicDefense = 0;
            m_dexterity = 0;
            m_attackSpeed = 0;
            m_dodge = 0;
            m_magicDefenseBonus = 0;
            m_finalAttack = 0;
            m_finalDefense = 0;
            m_finalMagicAttack = 0;
            m_finalMagicDefense = 0;
            m_criticalStrike = 0;
            m_skillCriticalStrike = 0;
            m_immunity = 0;
            m_penetration = 0;
            m_breakthrough = 0;
            m_counteraction = 0;
            m_block = 0;
            m_detoxication = 0;
            m_fireResistance = 0;
            m_waterResistance = 0;
            m_woodResistance = 0;
            m_earthResistance = 0;
            m_metalResistance = 0;
            m_blessing = 0;
            m_dragonGem = 0;
            m_phoenixGem = 0;
            m_rainbowGem = 0;
            m_violetGem = 0;
            m_moonGem = 0;
            m_kylinGem = 0;
            m_furyGem = 0;
            m_tortoiseGem = 0;
            m_dwMaxVigor = 0;

            #endregion

            ushort usAddLife = (ushort) ((Force + Agility + Spirit) * 3);
            if (ProfessionSort == 1)
            {
                float multiplier = 1f;
                switch (ProfessionLevel)
                {
                    case 1:
                        multiplier = 1.05f;
                        break;
                    case 2:
                        multiplier = 1.08f;
                        break;
                    case 3:
                        multiplier = 1.1f;
                        break;
                    case 4:
                        multiplier = 1.12f;
                        break;
                    case 5:
                        multiplier = 1.15f;
                        break;
                }

                usAddLife += (ushort) (24f * multiplier * Vitality);
            }
            else
            {
                usAddLife += (ushort) (24 * Vitality);
            }

            ushort baseMana = 5;
            switch (Profession)
            {
                case ProfessionType.WaterTaoist:
                case ProfessionType.FireTaoist:
                    baseMana = 15;
                    break;
                case ProfessionType.WaterWizard:
                case ProfessionType.FireWizard:
                    baseMana = 20;
                    break;
                case ProfessionType.WaterMaster:
                case ProfessionType.FireMaster:
                    baseMana = 25;
                    break;
                case ProfessionType.WaterSaint:
                case ProfessionType.FireSaint:
                    baseMana = 30;
                    break;
            }

            ushort usAddMana = (ushort) (baseMana * Spirit);

            m_dexterity += (Agility / 3);

            if (Map?.IsSkillMap() != true)
            {
                foreach (var subclass in SubClass.Professions.Values)
                {
                    switch (subclass.Class)
                    {
                        case SubClasses.Apothecary:
                        {
                            m_detoxication += SubClass.PowerApothecary[subclass.Promotion];
                            continue;
                        }

                        case SubClasses.ChiMaster:
                        {
                            m_immunity += SubClass.PowerChiMaster[subclass.Promotion];
                            continue;
                        }

                        case SubClasses.MartialArtist:
                        {
                            m_criticalStrike += SubClass.PowerMartialArtist[subclass.Promotion];
                            continue;
                        }

                        case SubClasses.Performer:
                        {
                            m_minAttack += SubClass.PowerPerformer[subclass.Promotion];
                            m_minAttackEx += SubClass.PowerPerformer[subclass.Promotion];
                            m_maxAttack += SubClass.PowerPerformer[subclass.Promotion];
                            m_maxAttackEx += SubClass.PowerPerformer[subclass.Promotion];
                            m_magicAttack += SubClass.PowerPerformer[subclass.Promotion];
                            m_magicAttackEx += SubClass.PowerPerformer[subclass.Promotion];
                            continue;
                        }

                        case SubClasses.Sage:
                        {
                            m_penetration += SubClass.PowerSage[subclass.Promotion];
                            continue;
                        }

                        case SubClasses.Warlock:
                        {
                            m_skillCriticalStrike += SubClass.PowerWarlock[subclass.Promotion];
                            continue;
                        }

                        case SubClasses.Wrangler:
                        {
                            usAddLife += (ushort) SubClass.PowerWrangler[subclass.Promotion];
                            continue;
                        }
                    }
                }

                foreach (var chi in ChiSystem.GetPowers())
                {
                    int value = chi.Value;
                    switch (chi.Key)
                    {
                        case ChiAttributeType.Criticalstrike:
                            m_criticalStrike += value * 10;
                            break;
                        case ChiAttributeType.Skillcriticalstrike:
                            m_skillCriticalStrike += value * 10;
                            break;
                        case ChiAttributeType.Immunity:
                            m_immunity += value * 10;
                            break;
                        case ChiAttributeType.Breakthrough:
                            m_breakthrough += value;
                            break;
                        case ChiAttributeType.Counteraction:
                            m_counteraction += value;
                            break;
                        case ChiAttributeType.Health:
                            usAddLife += (ushort) value;
                            break;
                        case ChiAttributeType.Attack:
                            m_minAttack += value;
                            m_minAttackEx += value;
                            m_maxAttack += value;
                            m_maxAttackEx += value;
                            break;
                        case ChiAttributeType.Magicattack:
                            m_magicAttack += value;
                            m_magicAttackEx += value;
                            break;
                        case ChiAttributeType.Mdefense:
                            m_magicDefense += value;
                            break;
                        case ChiAttributeType.Finalattack:
                            m_finalAttack += value;
                            break;
                        case ChiAttributeType.Finalmagicattack:
                            m_finalMagicAttack += value;
                            break;
                        case ChiAttributeType.Damagereduction:
                            m_finalDefense += value;
                            break;
                        case ChiAttributeType.Magicdamagereduction:
                            m_finalMagicDefense += value;
                            break;
                    }
                }

                ChiSystem.SendRank();

                foreach (var jiang in KongFu.GetPowers())
                {
                    int value = jiang.Value;
                    switch (jiang.Key)
                    {
                        case JiangHuAttrType.MaxLife:
                            usAddLife += (ushort) value;
                            break;
                        case JiangHuAttrType.PAttack:
                            m_minAttack += value;
                            m_minAttackEx += value;
                            m_maxAttack += value;
                            m_maxAttackEx += value;
                            break;
                        case JiangHuAttrType.MAttack:
                            m_magicAttack += value;
                            m_magicAttackEx += value;
                            break;
                        case JiangHuAttrType.PDefense:
                            m_defense += value;
                            break;
                        case JiangHuAttrType.Mdefense:
                            m_magicDefense += value;
                            break;
                        case JiangHuAttrType.FinalAttack:
                            m_finalAttack += value;
                            break;
                        case JiangHuAttrType.FinalMagicAttack:
                            m_finalMagicAttack += value;
                            break;
                        case JiangHuAttrType.FinalDefense:
                            m_finalDefense += value;
                            break;
                        case JiangHuAttrType.FinalMagicDefense:
                            m_finalMagicDefense += value;
                            break;
                        case JiangHuAttrType.CriticalStrike:
                            m_criticalStrike += value * 10;
                            break;
                        case JiangHuAttrType.SkillCriticalStrike:
                            m_skillCriticalStrike += value * 10;
                            break;
                        case JiangHuAttrType.Immunity:
                            m_immunity += value * 10;
                            break;
                        case JiangHuAttrType.Breakthrough:
                            m_breakthrough += value;
                            break;
                        case JiangHuAttrType.Counteraction:
                            m_counteraction += value;
                            break;
                        case JiangHuAttrType.MaxMana:
                            usAddMana += (ushort) value;
                            break;
                    }
                }
            }

            for (int i = FlagInt.TYRANT_AURA; i <= FlagInt.EARTH_AURA; i += 2)
            {
                if (QueryStatus(i) == null)
                    continue;
                switch (i)
                {
                    case FlagInt.TYRANT_AURA:
                        m_criticalStrike += QueryStatus(i).Power * 100;
                        m_skillCriticalStrike += QueryStatus(i).Power * 100;
                        break;
                    case FlagInt.FEND_AURA:
                        m_immunity += QueryStatus(i).Power * 100;
                        break;
                    case FlagInt.EARTH_AURA:
                        m_earthResistance += QueryStatus(i).Power;
                        break;
                    case FlagInt.WATER_AURA:
                        m_waterResistance += QueryStatus(i).Power;
                        break;
                    case FlagInt.FIRE_AURA:
                        m_fireResistance += QueryStatus(i).Power;
                        break;
                    case FlagInt.WOOD_AURA:
                        m_woodResistance += QueryStatus(i).Power;
                        break;
                    case FlagInt.METAL_AURA:
                        m_metalResistance += QueryStatus(i).Power;
                        break;
                }
            }

            foreach (var item in UserPackage.GetEquipment())
            {
                if (item.IsBroken())
                    continue;

                int itemSubtype = item.GetItemSubtype();
                bool isSingleHand = itemSubtype == 421 || item.Position == ItemPosition.RightHand
                                    && (item.GetSort() == ItemSort.ItemsortWeaponDoubleHand &&
                                        UserPackage[ItemPosition.LeftHand] == null);
                if (itemSubtype == 201
                    || itemSubtype == 202)
                {
                    m_finalAttack += item.MaxAttack;
                    m_finalDefense += item.Defense;
                    m_finalMagicAttack += item.MagicAttack;
                    m_finalMagicDefense += item.MagicDefense;
                }
                else
                {
                    if (item.Position == ItemPosition.LeftHand
                        && (item.GetSort() == ItemSort.ItemsortWeaponSingleHand
                            || item.GetSort() == ItemSort.ItemsortWeaponProfBased && !item.IsPistol()))
                    {
                        m_minAttack += item.MinAttack / 2;
                        m_maxAttack += item.MaxAttack / 2;
                        m_minAttackEx += item.MinAttack / 2;
                        m_maxAttackEx += item.MaxAttack / 2;
                    }
                    else
                    {
                        m_minAttack += item.MinAttack;
                        m_maxAttack += item.MaxAttack;
                        m_minAttackEx += item.MinAttack;
                        m_maxAttackEx += item.MaxAttack;
                    }
                }

                m_magicAttack += item.MagicAttack;
                m_magicAttackEx += item.MagicAttack;
                m_magicDefense += item.MagicDefense;
                m_defense += item.Defense;

                if (item.Position >= ItemPosition.RightHand
                    && item.Position <= ItemPosition.Ring
                    || item.Position >= ItemPosition.AltWeaponR
                    && item.Position <= ItemPosition.AltRing)
                    m_attackSpeed += item.AttackSpeed;

                int nMultiply = isSingleHand ? 2 : 1;
                if (itemSubtype != 300 && itemSubtype != 203)
                {
                    m_dexterity += item.Dexterity * nMultiply;
                }

                if (Map?.IsSkillMap() != true)
                {
                    switch (item.SocketTwo)
                    {
                        case SocketGem.NormalDragonGem:
                        case SocketGem.RefinedDragonGem:
                        case SocketGem.SuperDragonGem:
                            m_dragonGem += Calculations.CalculateGemPercentage(item.SocketTwo);
                            break;
                        case SocketGem.NormalPhoenixGem:
                        case SocketGem.RefinedPhoenixGem:
                        case SocketGem.SuperPhoenixGem:
                            m_phoenixGem += Calculations.CalculateGemPercentage(item.SocketTwo);
                            break;
                        case SocketGem.NormalFuryGem:
                        case SocketGem.RefinedFuryGem:
                        case SocketGem.SuperFuryGem:
                            m_furyGem += Calculations.CalculateGemPercentage(item.SocketTwo);
                            break;
                        case SocketGem.NormalRainbowGem:
                        case SocketGem.RefinedRainbowGem:
                        case SocketGem.SuperRainbowGem:
                            m_rainbowGem += Calculations.CalculateGemPercentage(item.SocketTwo) * nMultiply;
                            break;
                        case SocketGem.NormalKylinGem:
                        case SocketGem.RefinedKylinGem:
                        case SocketGem.SuperKylinGem:
                            m_kylinGem += Calculations.CalculateGemPercentage(item.SocketTwo);
                            break;
                        case SocketGem.NormalVioletGem:
                        case SocketGem.RefinedVioletGem:
                        case SocketGem.SuperVioletGem:
                            m_violetGem += Calculations.CalculateGemPercentage(item.SocketTwo) * nMultiply;
                            break;
                        case SocketGem.NormalMoonGem:
                        case SocketGem.RefinedMoonGem:
                        case SocketGem.SuperMoonGem:
                            m_moonGem += Calculations.CalculateGemPercentage(item.SocketTwo) * nMultiply;
                            break;
                        case SocketGem.NormalTortoiseGem:
                        case SocketGem.RefinedTortoiseGem:
                        case SocketGem.SuperTortoiseGem:
                            m_tortoiseGem += Calculations.CalculateGemPercentage(item.SocketTwo);
                            break;
                        case SocketGem.NormalThunderGem:
                        case SocketGem.RefinedThunderGem:
                        case SocketGem.SuperThunderGem:
                            m_finalAttack += Calculations.GetTalismanGemAttr(item.SocketTwo);
                            m_finalMagicAttack += Calculations.GetTalismanGemAttr(item.SocketTwo);
                            break;
                        case SocketGem.NormalGloryGem:
                        case SocketGem.RefinedGloryGem:
                        case SocketGem.SuperGloryGem:
                            m_finalDefense += Calculations.GetTalismanGemAttr(item.SocketTwo);
                            m_finalMagicDefense += Calculations.GetTalismanGemAttr(item.SocketTwo);
                            break;
                    }
                }

                switch (item.SocketOne)
                {
                    case SocketGem.NormalDragonGem:
                    case SocketGem.RefinedDragonGem:
                    case SocketGem.SuperDragonGem:
                        m_dragonGem += Calculations.CalculateGemPercentage(item.SocketOne);
                        break;
                    case SocketGem.NormalPhoenixGem:
                    case SocketGem.RefinedPhoenixGem:
                    case SocketGem.SuperPhoenixGem:
                        m_phoenixGem += Calculations.CalculateGemPercentage(item.SocketOne);
                        break;
                    case SocketGem.NormalFuryGem:
                    case SocketGem.RefinedFuryGem:
                    case SocketGem.SuperFuryGem:
                        m_furyGem += Calculations.CalculateGemPercentage(item.SocketOne);
                        break;
                    case SocketGem.NormalRainbowGem:
                    case SocketGem.RefinedRainbowGem:
                    case SocketGem.SuperRainbowGem:
                        m_rainbowGem += Calculations.CalculateGemPercentage(item.SocketOne) * nMultiply;
                        break;
                    case SocketGem.NormalKylinGem:
                    case SocketGem.RefinedKylinGem:
                    case SocketGem.SuperKylinGem:
                        m_kylinGem += Calculations.CalculateGemPercentage(item.SocketOne);
                        break;
                    case SocketGem.NormalVioletGem:
                    case SocketGem.RefinedVioletGem:
                    case SocketGem.SuperVioletGem:
                        m_violetGem += Calculations.CalculateGemPercentage(item.SocketOne) * nMultiply;
                        break;
                    case SocketGem.NormalMoonGem:
                    case SocketGem.RefinedMoonGem:
                    case SocketGem.SuperMoonGem:
                        m_moonGem += Calculations.CalculateGemPercentage(item.SocketOne) * nMultiply;
                        break;
                    case SocketGem.NormalTortoiseGem:
                    case SocketGem.RefinedTortoiseGem:
                    case SocketGem.SuperTortoiseGem:
                        m_tortoiseGem += Calculations.CalculateGemPercentage(item.SocketOne) ;
                        break;
                    case SocketGem.NormalThunderGem:
                    case SocketGem.RefinedThunderGem:
                    case SocketGem.SuperThunderGem:
                        m_finalAttack += Calculations.GetTalismanGemAttr(item.SocketOne);
                        m_finalMagicAttack += Calculations.GetTalismanGemAttr(item.SocketOne);
                        break;
                    case SocketGem.NormalGloryGem:
                    case SocketGem.RefinedGloryGem:
                    case SocketGem.SuperGloryGem:
                        m_finalDefense += Calculations.GetTalismanGemAttr(item.SocketOne);
                        m_finalMagicDefense += Calculations.GetTalismanGemAttr(item.SocketOne);
                        break;
                }

                if (item.Position == ItemPosition.Armor || item.Position == ItemPosition.Headwear
                                                        || item.Position == ItemPosition.AltArmor
                                                        || item.Position == ItemPosition.AltHead)
                    m_magicDefenseBonus += item.MagicDefenseBonus;

                if (Map?.IsSkillMap() == true)
                {
                    m_blessing += Math.Max(0, Math.Min(3, (int) item.ReduceDamage));
                }
                else
                {
                    m_blessing += item.ReduceDamage;
                }
                m_dodge += item.Dodge;

                usAddLife += item.Enchantment;
                usAddLife += (ushort) item.Life;
                usAddMana += (ushort) item.Mana;

                if (Map?.IsSkillMap() != true)
                {
                    m_criticalStrike += (int) item.CriticalStrike;
                    m_skillCriticalStrike += (int) item.SkillCriticalStrike;
                    m_immunity += (int) item.Immunity;
                    m_breakthrough += (int) item.Breakthrough;
                    m_penetration += (int) item.Penetration;
                    m_counteraction += (int) item.Counteraction;
                    m_metalResistance += (int) item.MetalResistance;
                    m_woodResistance += (int) item.WoodResistance;
                    m_earthResistance += (int) item.EarthResistance;
                    m_waterResistance += (int) item.WaterResistance;
                    m_fireResistance += (int) item.FireResistance;
                    m_block += (int) item.Block;
                }

                m_detoxication += (int) item.Detoxication;
                m_dwMaxVigor += item.Vigor;
            }

            if (QueryStatus(FlagInt.SUPER_SHIELD_HALO) != null)
            {
                m_block += (QueryStatus(FlagInt.SUPER_SHIELD_HALO)?.Power ?? 0) * 100;
            }
            
            m_dragonGem = (float) (Math.Ceiling(m_dragonGem * 100) / 100f);
            m_phoenixGem = (float) (Math.Ceiling(m_phoenixGem * 100) / 100f);

            m_minAttack = (int) (m_minAttack * (1 + m_dragonGem));
            m_maxAttack = (int) (m_maxAttack * (1 + m_dragonGem));
            m_magicAttack = (int) (m_magicAttack * (1 + m_phoenixGem));

            m_usMaxLife = usAddLife;
            m_usMaxMana = usAddMana;

            if (MaxLife < Life)
                Life = MaxLife;
            if (MaxMana < Mana)
                Mana = MaxMana;

            //UpdateClient(ClientUpdateType.Hitpoints, Life);
            UpdateClient(ClientUpdateType.Mana, Mana);
            SendUserAttribInfo();
        }

        #endregion

        #region Update Packet

        public void SendUserAttribInfo(Character target = null)
        {
            MsgPlayerAttribInfo msg = new MsgPlayerAttribInfo
            {
                Identity = Identity,
                Accuracy = (uint)m_dexterity,
                Agility = (uint)GetInterAtkRate(),
                Bless = (uint) m_blessing,
                Block = (uint) m_block,
                Breakthrough = (uint) m_breakthrough,
                Counteraction = (uint) m_counteraction,
                CriticalStrike = (uint) m_criticalStrike,
                Detoxication = (uint) m_detoxication,
                Dodge = (uint) m_dodge,
                DragonGemBonus = (uint) (m_dragonGem * 100),
                EarthDefense = (uint) m_earthResistance,
                FinalDefense = (uint) m_finalDefense,
                FinalMagicDamage = (uint) m_finalMagicAttack,
                FinalMagicDefense = (uint) m_finalMagicDefense,
                FinalPhysicalDamage = (uint) m_finalDefense,
                FireDefense = (uint) m_fireResistance,
                Immunity = (uint) m_immunity,
                Life = m_usMaxLife,
                MagicDefense = (uint) m_magicDefense,
                MagicDefenseBonus = (uint) m_magicDefenseBonus,
                MagicalAttack = (uint) m_magicAttackEx,
                Mana = m_usMaxMana,
                MaxAttack = (uint) m_maxAttackEx,
                MetalDefense = (uint) m_metalResistance,
                MinAttack = (uint) m_minAttackEx,
                Penetration = (uint) m_penetration,
                SkillCriticalStrike = (uint) m_skillCriticalStrike,
                PhoenixGemBonus = (uint) (m_phoenixGem*100),
                PhysicalDefense = (uint) m_defense,
                TortoiseGemBonus = (uint) (TortoiseGem * 100),
                WaterDefense = (uint) m_waterResistance,
                WoodDefense = (uint) m_woodResistance
            };
            if (target == null)
            {
                Send(msg);
                ChiSystem?.SendInfo(true);
            }
            else
            {
                target.Send(msg);
                ChiSystem?.SendInfo(target, true);
            }
        }

        public override void SendEffect(string effectName, bool screen)
        {
            if (m_pOwner == null || Screen == null)
                return;

            var msg = new MsgName
            {
                Identity = Identity,
                Action = StringAction.RoleEffect
            };
            msg.Append(effectName);
            Send(msg);

            if (screen && LoginComplete)
                Screen?.Send(msg, true);
        }

        /// <summary>
        /// Send a update packet containing a byte value.
        /// </summary>
        public override void UpdateClient(ClientUpdateType type, byte value, bool broadcast = true)
        {
            if (Owner == null || Screen == null)
                return;

            var updatePacket = new MsgUserAttrib {Identity = Identity};
            updatePacket.Append(type, value);
            Send(updatePacket);
            if (broadcast && LoginComplete)
                Screen?.Send(updatePacket, true);
        }

        /// <summary>
        /// Send a update packet containing a uint value.
        /// </summary>
        public override void UpdateClient(ClientUpdateType type, uint value, bool broadcast = false)
        {
            if (Owner == null || Screen == null)
                return;

            var updatePacket = new MsgUserAttrib {Identity = Identity};
            updatePacket.Append(type, value);
            Send(updatePacket);
            if (broadcast && LoginComplete)
                Screen?.Send(updatePacket, true);
        }

        /// <summary>
        /// Send a update packet containing a ushort value.
        /// </summary>
        public override void UpdateClient(ClientUpdateType type, ushort value, bool broadcast = false)
        {
            if (Owner == null || Screen == null)
                return;

            var updatePacket = new MsgUserAttrib {Identity = Identity};
            updatePacket.Append(type, value);
            Send(updatePacket);
            if (broadcast && LoginComplete)
                Screen?.Send(updatePacket, true);
        }

        /// <summary>
        /// Send a update packet containing a ulong value.
        /// </summary>
        public override void UpdateClient(ClientUpdateType type, ulong value, bool broadcast = false)
        {
            if (Owner == null || Screen == null)
                return;

            var updatePacket = new MsgUserAttrib {Identity = Identity};
            updatePacket.Append(type, value);
            Send(updatePacket);
            if (broadcast && LoginComplete)
                Screen?.Send(updatePacket, true);
        }

        public override void UpdateClient(ClientUpdateType type, ulong value1, ulong value2, bool broadcast = false)
        {
            if (Owner == null || Screen == null)
                return;

            var updatePacket = new MsgUserAttrib {Identity = Identity};
            updatePacket.Append(type, value1, value2);
            Owner.Send(updatePacket);
            if (broadcast && LoginComplete)
                Screen?.Send(updatePacket, true);
        }

        public override void UpdateClient(ClientUpdateType type, ulong value1, ulong value2, ulong value3,
            bool broadcast = false)
        {
            if (Owner == null || Screen == null)
                return;

            var updatePacket = new MsgUserAttrib { Identity = Identity };
            updatePacket.Append(type, value1, value2, value3);
            Owner.Send(updatePacket);
            if (broadcast && LoginComplete)
                Screen?.Send(updatePacket, true);
        }

        public override void UpdateClient(ClientUpdateType type, uint value1, uint value2, uint value3, uint value4,
            bool broadcast = false)
        {
            if (Owner == null || Screen == null)
                return;

            var updatePacket = new MsgUserAttrib { Identity = Identity };
            updatePacket.Append(type, value1, value2, value3, value4);
            Owner.Send(updatePacket);
            if (broadcast && LoginComplete)
                Screen?.Send(updatePacket, true);
        }

        public void UpdateSoulShackle(int nTime)
        {
            if (Owner == null || Screen == null)
                return;

            var updatePacket = new MsgUserAttrib {Identity = Identity};
            updatePacket.Append(ClientUpdateType.SoulShackleTimer, 111u, (uint) nTime);
            Owner.Send(updatePacket);
            Screen?.Send(updatePacket, true);
        }

        public override void UpdateAzureShield(int nTime, int nPower, byte level)
        {
            if (Owner == null || Screen == null)
                return;

            var updatePacket = new MsgUserAttrib {Identity = Identity};
            updatePacket.Append(ClientUpdateType.AzureShield, 93u, (uint) nTime, (uint) nPower, level);
            Owner.Send(updatePacket);
            Screen?.Send(updatePacket, true);
        }

        #endregion

        public int IsFullQuality
        {
            get
            {
                int quality = 0;
                foreach (var item in UserPackage.GetEquipment())
                {
                    switch (item.Position)
                    {
                        case ItemPosition.Headwear:
                        case ItemPosition.AltHead:
                        case ItemPosition.Necklace:
                        case ItemPosition.AltNecklace:
                        case ItemPosition.Ring:
                        case ItemPosition.AltRing:
                        case ItemPosition.RightHand:
                        case ItemPosition.AltWeaponR:
                        case ItemPosition.LeftHand:
                        case ItemPosition.AltWeaponL:
                        case ItemPosition.Boots:
                        case ItemPosition.AltBoots:
                        case ItemPosition.Armor:
                        case ItemPosition.AltArmor:
                        case ItemPosition.AttackTalisman:
                        case ItemPosition.AltFan:
                        case ItemPosition.DefenceTalisman:
                        case ItemPosition.AltTower:
                        case ItemPosition.Crop:
                            quality += item.GetQuality()%5;

                            if (item.Position == ItemPosition.RightHand && 
                                (item.IsBackswordType() || item.IsWeaponTwoHand()) && UserPackage[ItemPosition.LeftHand] == null)
                                quality += item.GetQuality() % 5;
                            break;
                    }
                }

                if (quality == 40)
                    return 3;
                if (quality > 30)
                    return 2;
                if (quality > 20)
                    return 1;
                return 0;
            }
        }

        /// <summary>
        /// The user battle power. It's automatically updated no mather the map or anything.
        /// </summary>
        public override int BattlePower
        {
            get
            {
                if (Map?.IsSkillMap() == true)
                    return 1;

                // We we'll recalculate this everytime to avoid inconsistences.
                int ret = Level + Metempsychosis * 5 + Nobility.BattlePower;
                ret += UserPackage.GetEquipment().Sum(x => x.BattlePower);
                ret += (int) (Syndicate?.Arsenal.SharedBattlePower(this) ?? 0);
                // todo guide
                // todo clan

                m_packet.TotalBattlePower = (ushort) ret;
                return ret;
            }
        }

        public uint TotemBattlePower
        {
            get => Syndicate?.Arsenal.SharedBattlePower(this) ?? 0;
            set => m_packet.TutorBattlePower = value;
        }

        public override bool AddAttrib(ClientUpdateType type, long value)
        {
            switch (type)
            {
                case ClientUpdateType.Experience:
                    {
                        if (value > 0)
                        {
                            AwardExperience(value);
                        }
                        else
                        {
                            Experience = (ulong) Math.Max(0, ((long) Experience - value));
                        }
                        return true;
                    }
            }

            return base.AddAttrib(type, value);
        }

        #endregion

        #region Role

        public override int AdjustDefense(Role attacker)
        {
            int defense = Defense;
            if (ProfessionLevel >= 3 && Metempsychosis >= 1)
            {
                defense = (int)(defense * 1.3d);
            }
            return defense;
        }

        public override int AdjustMagicDefense(Role attacker)
        {
            int defense = MagicDefense;
            if (ProfessionLevel >= 3 && Metempsychosis >= 1)
            {
                defense = (int) (defense * 1.3d);
            }
            return defense;
        }

        public override bool IsPlayer()
        {
            return true;
        }

        public override void ProcessOnMove(MovementType mode)
        {
            base.ProcessOnMove(mode);

            StopMine();

            m_tRespawn.Clear();

            m_bIsAway = false;

            ProcessAfterMove();
        }

        public override void ProcessAfterMove()
        {
            base.ProcessAfterMove();

            if (Team != null)
            {
                foreach (var member in Team.Members.Values)
                {
                    member.ProcessAura();
                }
            }

            var roles = Screen.GetAroundScreenObjects;
            foreach (var role in roles)
            {
                if (role is MapTrap trap && trap.IsTrapSort() && trap.IsInRange(this))
                {
                    trap.TrapAttack(this);
                }
                else if (role is EventFlag flag && GetDistance(flag.MapX, flag.MapY) <= 1)
                {
                    flag.Grab(this);
                }
            }

        }

        public override void ProcessOnAttack()
        {
            base.ProcessOnAttack();

            m_tRespawn.Clear();

            if (QueryStatus(FlagInt.RIDING) != null && UserPackage[ItemPosition.Crop] == null)
                DetachStatus(FlagInt.RIDING);

            m_bIsAway = false;

            StopMine();
        }

        #endregion

        #region Game Action

        public new GameAction GameAction => m_gameAction ?? (m_gameAction = new GameAction(this));

        public void CancelInteraction()
        {
            ClearTaskId();
            InteractingItem = null;
            InteractingNpc = null;
        }

        public void ClearTaskId()
        {
            // InteractingNpc = null;
            m_setTaskId.Clear();
        }

        public bool CheckItem(GameTaskEntity task)
        {
            if (task.Itemname1.Length > 0)
            {
                if (UserPackage[task.Itemname1] == null)
                    return false;

                if (task.Itemname2.Length > 0)
                {
                    if (UserPackage[task.Itemname2] == null)
                        return false;
                }
            }

            return true;
        }

        public bool TestTask(GameTaskEntity task)
        {
            if (task == null) return false;

            try
            {
                if (!CheckItem(task))
                    return false;

                if (Silver < task.Money)
                    return false;

                if (task.Profession != 0 && Profession != (ProfessionType) task.Profession)
                    return false;

                if (task.Sex != 0 && task.Sex != 999 && task.Sex != Gender)
                    return false;

                if (PkPoints < task.MinPk || PkPoints > task.MaxPk)
                    return false;

                if (task.Marriage >= 0)
                {
                    if (task.Marriage == 0 && Mate != MATE_NONE)
                        return false;
                    if (task.Marriage == 1 && Mate == MATE_NONE)
                        return false;
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                Program.WriteLog($"Could not TestTask({task.Id})", LogType.ERROR);
                Program.WriteLog(ex.ToString());
#else
                ServerKernel.Log.SaveLog($"Could not TestTask({task.Id})", LogType.ERROR);
                ServerKernel.Log.SaveLog(ex.ToString());
#endif
                return false;
            }

            return true;
        }

        public Item GetTaskItem(string szItemname)
        {
            return null;
        }

        public byte PushTaskId(uint idTask)
        {
            if (idTask != 0 && m_setTaskId.Count < MAX_MENUTASKSIZE)
            {
                m_setTaskId.Add(idTask);
                return (byte) m_setTaskId.Count;
            }

            return 0;
        }

        public uint GetTaskId(int idx)
        {
            return idx > 0 && idx <= m_setTaskId.Count ? m_setTaskId[idx - 1] : 0u;
        }

        #endregion

        #region Nobility

        private Nobility m_nobility;

        public Nobility Nobility => m_nobility ?? (m_nobility = new Nobility(this));

        public NobilityLevel NobilityRank
        {
            get => Nobility.GetNobility;
            set
            {
                if (m_packet.Nobility != (byte) value)
                {
                    m_packet.Nobility = (byte) value;
                    Screen.RefreshSpawnForObservers();
                }
            }
        }

        public long NobilityDonation
        {
            get => m_dbUser.Donation;
            set
            {
                m_dbUser.Donation = value;
                Save();
            }
        }

        #endregion

        #region Bonus

        public int BonusCount()
        {
            return new BonusRepository().BonusAmount(Owner.AccountIdentity);
        }

        public bool DoBonus()
        {
            if (UserPackage.IsPackSpare(10))
            {
                SendSysMessage(string.Format(Language.StrNotEnoughSpaceN, 10));
                return false;
            }

            var repo = new BonusRepository();
            var bonus = repo.CatchBonus(Owner.AccountIdentity);
            if (bonus == null || bonus.Flag > 0)
            {
                SendSysMessage(Language.StrNoBonus);
                return false;
            }

            if (!GameAction.ProcessAction(bonus.Action, this, this, null, null))
            {
                bonus.Flag = 1;
                return repo.Save(bonus);
            }

            ServerKernel.Log.GmLog("bonus",
                $"AccountID({bonus.AccountIdentity}), Player({Identity}) take bonus ({bonus.Identity}) action ({bonus.Action})");

            bonus.Time = UnixTimestamp.Now();
            bonus.Flag = 1;
            return repo.Save(bonus);
        }

        #endregion

        #region Heaven Blessing

        #region Multiple Exp

        public bool HasMultipleExp =>
            m_dbUser.ExperienceMultiplier > 1 && m_dbUser.ExperienceExpires >= UnixTimestamp.Now();

        public float ExperienceMultiplier =>
            !HasMultipleExp || m_dbUser.ExperienceMultiplier <= 0 ? 1f : m_dbUser.ExperienceMultiplier;

        public void SendMultipleExp()
        {
            if (RemainingExperienceSeconds > 0)
                UpdateClient(ClientUpdateType.DoubleExpTimer, 0, RemainingExperienceSeconds,
                    0, (uint)(ExperienceMultiplier * 100), false);
        }

        public uint RemainingExperienceSeconds
        {
            get
            {
                int now = UnixTimestamp.Now();
                if (m_dbUser.ExperienceExpires < now)
                {
                    m_dbUser.ExperienceMultiplier = 1;
                    return m_dbUser.ExperienceExpires = 0;
                }

                return (uint) (m_dbUser.ExperienceExpires - now);
            }
        }

        public bool SetExperienceMultiplier(uint nSeconds, float nMultiplier = 2f)
        {
            m_dbUser.ExperienceExpires = (uint) (UnixTimestamp.Now() + nSeconds);
            m_dbUser.ExperienceMultiplier = nMultiplier;
            SendMultipleExp();
            return true;
        }

        #endregion

        #region Heaven Blessing

        public void SendBless()
        {
            if (IsBlessed)
            {
                int now = UnixTimestamp.Now();
                UpdateClient(ClientUpdateType.HeavensBlessing, (uint) (HeavenBlessingExpires - now));

                if (Map != null && !Map.IsTrainingMap())
                    UpdateClient(ClientUpdateType.OnlineTraining, 0, false);
                else
                    UpdateClient(ClientUpdateType.OnlineTraining, 1, false);

                AttachStatus(this, FlagInt.HEAVEN_BLESS, 0, (int) (HeavenBlessingExpires - now), 0, 0);
            }
        }

        /// <summary>
        /// This method will update the user blessing time.
        /// </summary>
        /// <param name="amount">The amount of minutes to be added.</param>
        /// <returns>If the heaven blessing has been added successfully.</returns>
        public bool AddBlessing(uint amount)
        {
            int now = UnixTimestamp.Now();
            if (m_dbUser.HeavenBlessing > 0 && m_dbUser.HeavenBlessing >= now)
                m_dbUser.HeavenBlessing += 60 * 60 * amount;
            else
                m_dbUser.HeavenBlessing = (uint) (now + 60 * 60 * amount);

            SendBless();

            //if (Mentor != null)
            //    AddMentorBlessing((ushort)(amount / 10));
            return true;
        }

        public uint HeavenBlessingExpires => m_dbUser.HeavenBlessing;

        public bool IsBlessed => m_dbUser.HeavenBlessing > UnixTimestamp.Now();

        #endregion

        #endregion

        #region Warehouse

        public Dictionary<uint, Warehouse> Warehouses = new Dictionary<uint, Warehouse>();

        #endregion

        #region Secondary Password

        public bool IsUnlocked()
        {
            if (WarehousePassword == 0)
                return true;
            if (Data[0] == 0)
                return false;
            return true;
        }

        public void Unlock2ndPassword()
        {
            Data[0] = 1;
        }

        public bool CanUnlock2ndPassword()
        {
            return Data[1] <= 2;
        }

        public void IncrementUnlockAttempt()
        {
            Data[1]++;
        }

        #endregion

        #region Application Handle

        public void SendRelation(Character target)
        {
            Send(new MsgRelation
            {
                BattlePower = (uint) target.BattlePower,
                IsApprentice = false,
                IsSpouse = target.IsMate(this),
                IsTradePartner = false,
                Level = Level,
                SenderIdentity = target.Identity,
                TargetIdentity = Identity
            });
        }

        public void SetFriendRequest(uint dwTarget)
        {
            m_dwFriendRequest = dwTarget;
        }

        public bool FetchFriendRequest(uint dwTarget)
        {
            return m_dwFriendRequest == dwTarget;
        }

        public void ClearFriendRequest()
        {
            m_dwFriendRequest = 0;
        }

        public void SetMarryRequest(uint dwTarget)
        {
            m_dwMarryRequest = dwTarget;
        }

        public bool FetchMarryRequest(uint dwTarget)
        {
            return m_dwMarryRequest == dwTarget;
        }

        public void ClearMarryRequest()
        {
            m_dwMarryRequest = 0;
        }

        public void SetTradeRequest(uint dwTarget)
        {
            m_dwTradeRequest = dwTarget;
        }

        public bool FetchTradeRequest(uint dwTarget)
        {
            return m_dwTradeRequest == dwTarget;
        }

        public bool HasRequestedTrade()
        {
            return m_dwTradeRequest != 0;
        }

        public void ClearTradeRequest()
        {
            m_dwTradeRequest = 0;
        }

        public void SetTeamJoin(uint dwTarget)
        {
            m_dwTeamJoin = dwTarget;
        }

        public bool FetchTeamJoin(uint dwTarget)
        {
            return m_dwTeamJoin == dwTarget;
        }

        public void ClearTeamJoin()
        {
            m_dwTeamJoin = 0;
        }

        public void SetTeamInvite(uint dwTarget)
        {
            m_dwTeamInvite = dwTarget;
        }

        public bool FetchTeamInvite(uint dwTarget)
        {
            return m_dwTeamInvite == dwTarget;
        }

        public void ClearTeamInvite()
        {
            m_dwTeamInvite = 0;
        }

        public void SetSynJoin(uint dwTarget)
        {
            m_dwSynJoin = dwTarget;
        }

        public bool FetchSynJoin(uint dwTarget)
        {
            return m_dwSynJoin == dwTarget;
        }

        public void ClearSynJoin()
        {
            m_dwSynJoin = 0;
        }

        public void SetSynInvite(uint dwTarget)
        {
            m_dwSynInvite = dwTarget;
        }

        public bool FetchSynInvite(uint dwTarget)
        {
            return m_dwSynInvite == dwTarget;
        }

        public void ClearSynInvite()
        {
            m_dwSynInvite = 0;
        }

        public void SetSynAllyRequest(uint dwTarget)
        {
            m_dwSynAlly = dwTarget;
        }

        public bool FetchSynAllyRequest(uint dwTarget)
        {
            return m_dwSynAlly == dwTarget;
        }

        public void ClearSynAllyRequest()
        {
            m_dwSynAlly = 0;
        }

        public void SetTradeBuddyRequest(uint dwTarget)
        {
            m_dwTradeBuddy = dwTarget;
        }

        public bool FetchTradeBuddyRequest(uint dwTarget)
        {
            return m_dwTradeBuddy == dwTarget;
        }

        public void ClearTradeBuddyRequest()
        {
            m_dwTradeBuddy = 0;
        }

        public void SetGuideRequest(uint dwTarget)
        {
            m_dwGuideRequest = dwTarget;
        }

        public bool FetchGuideRequest(uint dwTarget)
        {
            return m_dwGuideRequest == dwTarget;
        }

        public void ClearGuideRequest()
        {
            m_dwGuideRequest = 0;
        }

        public void SetStudentRequest(uint dwTarget)
        {
            m_dwStudentRequest = dwTarget;
        }

        public bool FetchStudentRequest(uint dwTarget)
        {
            return m_dwStudentRequest == dwTarget;
        }

        public void ClearStudentRequest()
        {
            m_dwStudentRequest = 0;
        }

        public void SetFamilyRecruitRequest(uint dwTarget)
        {
            m_dwFamilyRequest = dwTarget;
        }

        public bool FetchFamilyRecruitRequest(uint dwTarget)
        {
            return m_dwFamilyRequest == dwTarget;
        }

        public void ClearFamilyRecruitRequest()
        {
            m_dwFamilyRequest = 0;
        }

        public void SetFamilyJoinRequest(uint dwTarget)
        {
            m_dwJoinFamilyRequest = dwTarget;
        }

        public bool FetchFamilyJoinRequest(uint dwTarget)
        {
            return m_dwJoinFamilyRequest == dwTarget;
        }

        public void ClearFamilyJoinRequest()
        {
            m_dwJoinFamilyRequest = 0;
        }

        #endregion

        #region Statistics

        public uint GetStatisticValue(uint type, uint datatype)
        {
            return Statistics.GetValue(type, datatype);
        }

        #endregion

        #region Friends

        public bool ContainsFriend(uint idRole)
        {
            return Friends.ContainsKey(idRole);
        }

        public bool CreateFriend(Character pTarget)
        {
            if (ContainsFriend(pTarget.Identity))
            {
                SendSysMessage(string.Format(Language.StrYourFriendAlready));
                return false;
            }

            if (Friends.Count > Handlers.FriendVipAmount[m_pOwner.VipLevel])
            {
                SendSysMessage(Language.StrFriendListFull);
                return false;
            }

            if (pTarget.Friends.Count > Handlers.FriendVipAmount[pTarget.Owner.VipLevel])
            {
                pTarget.SendSysMessage(Language.StrTargetFrieldListFull);
                return false;
            }

            SetFriendRequest(0);
            pTarget.SetFriendRequest(0);

            var friend0 = new FriendEntity
            {
                UserIdentity = Identity,
                Friend = pTarget.Identity,
                FriendName = pTarget.Name
            };
            var friend1 = new FriendEntity
            {
                UserIdentity = pTarget.Identity,
                Friend = Identity,
                FriendName = Name
            };

            new FriendRepository().Save(friend0);
            new FriendRepository().Save(friend1);

            Screen.Send(string.Format(Language.StrMakeFriend, Name, pTarget.Name), true);

            var newFr = new MsgFriend
            {
                Mode = RelationAction.ADD_FRIEND,
                Identity = pTarget.Identity,
                Name = pTarget.Name,
                Online = true
            };
            Send(newFr);
            newFr = new MsgFriend
            {
                Mode = RelationAction.ADD_FRIEND,
                Identity = Identity,
                Name = Name,
                Online = true
            };
            pTarget.Send(newFr);

            var relation0 = new Relationship
            {
                Database = friend0,
                Identity = pTarget.Identity,
                Name = pTarget.Name
            };
            var relation1 = new Relationship
            {
                Database = friend1,
                Identity = Identity,
                Name = Name
            };
            return Friends.TryAdd(pTarget.Identity, relation0) && pTarget.Friends.TryAdd(Identity, relation1);
        }

        public bool AddFriend(FriendEntity entity)
        {
            if (ContainsFriend(entity.Friend)) return false;

            var relation = new Relationship
            {
                Database = entity,
                Identity = entity.Friend,
                Name = entity.FriendName
            };

            var info = new MsgFriend
            {
                Mode = RelationAction.ADD_FRIEND,
                Identity = entity.Friend,
                Name = entity.FriendName,
                Online = relation.IsOnline
            };

            Send(info);

            return Friends.TryAdd(entity.Friend, relation);
        }

        public bool DeleteFriend(uint idRole)
        {
            if (Friends.TryRemove(idRole, out var pTarget) && pTarget.Database is FriendEntity)
            {
                if (pTarget.IsOnline)
                {
                    MsgFriend pMsg = new MsgFriend
                    {
                        Mode = RelationAction.REMOVE_FRIEND,
                        Identity = Identity,
                        Name = Name,
                        Online = true
                    };
                    pTarget.User.Send(pMsg);
                    pTarget.User.Friends.TryRemove(Identity, out var trash);
                }

                MsgFriend msg = new MsgFriend
                {
                    Mode = RelationAction.REMOVE_FRIEND,
                    Identity = idRole,
                    Name = pTarget.Name,
                    Online = pTarget.IsOnline
                };
                Send(msg);
                Screen.Send(string.Format(Language.StrBreakFriend, Name, pTarget.Name), true);
                new FriendRepository().DeleteFriends(Identity, idRole);
                return true;
            }

            return false;
        }

        #endregion

        #region Enemy

        public bool CreateEnemy(Character pTarget)
        {
            if (pTarget == null) return false;
            if (pTarget.Identity == Identity) return false;
            if (Enemies.ContainsKey(pTarget.Identity)) return false;

            var dbEnemy = new EnemyEntity
            {
                EnemyIdentity = pTarget.Identity,
                Enemyname = pTarget.Name,
                Time = (uint) UnixTimestamp.Now(),
                UserIdentity = Identity
            };

            new EnemyRepository().Save(dbEnemy);

            Relationship pObj = new Relationship {Identity = pTarget.Identity, Name = pTarget.Name, Database = dbEnemy};

            var pMsg = new MsgFriend
            {
                Identity = pTarget.Identity,
                Name = pTarget.Name,
                Mode = RelationAction.ADD_ENEMY,
                Online = pObj.IsOnline
            };
            Send(pMsg);
            return Enemies.TryAdd(pObj.Identity, pObj);
        }

        public bool AddEnemy(EnemyEntity pTarget)
        {
            if (ContainsEnemy(pTarget.EnemyIdentity)) return false;
            if (pTarget.EnemyIdentity == Identity) return false;

            Relationship pObj = new Relationship
            {
                Identity = pTarget.EnemyIdentity, Name = pTarget.Enemyname, Database = pTarget
            };

            var pMsg = new MsgFriend
            {
                Identity = pTarget.EnemyIdentity,
                Name = pTarget.Enemyname,
                Mode = RelationAction.ADD_ENEMY,
                Online = pObj.IsOnline
            };
            Send(pMsg);

            return Enemies.TryAdd(pTarget.EnemyIdentity, pObj);
        }

        public bool DeleteEnemy(uint idTarget)
        {
            if (Enemies.TryRemove(idTarget, out var pTarget) && pTarget.Database is EnemyEntity entity)
            {
                var pMsg = new MsgFriend
                {
                    Identity = idTarget,
                    Name = pTarget.Name,
                    Mode = RelationAction.REMOVE_ENEMY,
                    Online = pTarget.IsOnline
                };
                Send(pMsg);
                return new EnemyRepository().Delete(entity);
            }

            return false;
        }

        public bool ContainsEnemy(uint idRole)
        {
            return Enemies.ContainsKey(idRole);
        }

        #endregion

        #region Mail Box

        public UserMailBox MailBox => m_pMailBox ?? (m_pMailBox = new UserMailBox(this));

        #endregion

        #region Battle

        public void ChangePkMode(PkModeType type)
        {
            PkMode = type;
            Send(new MsgAction(Identity, (ushort) type, 0, GeneralActionType.ChangePkMode));
        }

        public void ProcessAura()
        {
            const int _AURA_DISTANCE = 30;
            if (Team != null)
            {
                IStatus aura = GetCurrentAura();
                if (aura != null)
                {
                    foreach (var member in Team.Members.Values.Where(x => x.Identity != Identity))
                    {
                        if (member.GetDistance(MapX, MapY) > _AURA_DISTANCE)
                        {
                            member.DetachStatus(aura.Identity);
                        }
                        else
                        {
                            if (member.QueryStatus(aura.Identity) == null || member.QueryStatus(aura.Identity)?.Power < aura.Power)
                                member.AttachStatus(this, aura.Identity, aura.Power, int.MaxValue, 0, aura.Level);
                        }
                    }
                }
            }
        }

        public IStatus GetCurrentAura()
        {
            for (int i = FlagInt.TYRANT_AURA; i <= FlagInt.EARTH_AURA; i++)
                if (QueryStatus(i)?.CasterId == Identity)
                    return QueryStatus(i);
            return null;
        }

        public bool IsAssassin => QueryStatus(FlagInt.ASSASSIN) != null;

        public bool IsVirtuous()
        {
            return (Flag1 & KEEP_EFFECT_NOT_VIRTUOUS) == 0;
        }

        public bool IsCrime()
        {
            return QueryStatus(FlagInt.BLUE_NAME) != null;
        }

        public bool IsPker()
        {
            return QueryStatus(FlagInt.BLACK_NAME) != null;
        }

        public override bool IsBowman => UserPackage[ItemPosition.RightHand]?.IsBow() ?? false;

        public short Elevation => Map?[MapX, MapY].Elevation ?? 999;

        public bool CheckScapegoat(Role pTarget)
        {
            Magic scapegoat = Magics.Magics.Values.FirstOrDefault(x => x.Type == 6003);
            if (scapegoat != null && Scapegoat && scapegoat.IsReady())
            {
                return ProcessMagicAttack(scapegoat.Type, pTarget.Identity, pTarget.MapX, pTarget.MapY);
            }

            return false;
        }

        public override bool CheckWeaponSubType(uint idItem, uint dwNum = 0)
        {
            uint[] items = new uint[idItem.ToString().Length / 3];
            for (int i = 0; i < items.Length; i++)
            {
                if (idItem > 999 && idItem != 40000 && idItem != 50000)
                {
                    int idx = i * 3; // + (i > 0 ? -1 : 0);
                    items[i] = uint.Parse(idItem.ToString().Substring(idx, 3));
                }
                else
                {
                    items[i] = uint.Parse(idItem.ToString());
                }
            }

            if (items.Length <= 0) return false;

            foreach (var dwItem in items)
            {
                if (dwItem <= 0) continue;

                if (UserPackage[ItemPosition.RightHand] != null &&
                    UserPackage[ItemPosition.RightHand].GetItemSubtype() == dwItem &&
                    UserPackage[ItemPosition.RightHand].Durability >= dwNum)
                    return true;
                if (UserPackage[ItemPosition.LeftHand] != null &&
                    UserPackage[ItemPosition.LeftHand].GetItemSubtype() == dwItem &&
                    UserPackage[ItemPosition.LeftHand].Durability >= dwNum)
                    return true;

                ushort[] set1Hand = {410, 420, 421, 430, 440, 450, 460, 480, 481, 490};
                ushort[] set2Hand = {510, 530, 540, 560, 561, 580};
                ushort[] setSword = {420, 421};
                ushort[] setSpecial = {601, 610, 611, 612, 613};

                if (dwItem == 40000 || dwItem == 400)
                {
                    if (UserPackage[ItemPosition.RightHand] != null)
                    {
                        Item item = UserPackage[ItemPosition.RightHand];
                        for (int i = 0; i < set1Hand.Length; i++)
                        {
                            if (item.GetItemSubtype() == set1Hand[i] && item.Durability >= dwNum)
                                return true;
                        }
                    }
                }

                if (dwItem == 50000)
                {
                    if (UserPackage[ItemPosition.RightHand] != null)
                    {
                        if (dwItem == 50000) return true;

                        Item item = UserPackage[ItemPosition.RightHand];
                        for (int i = 0; i < set2Hand.Length; i++)
                        {
                            if (item.GetItemSubtype() == set2Hand[i] && item.Durability >= dwNum)
                                return true;
                        }
                    }
                }

                if (dwItem == 50) // arrow
                {
                    if (UserPackage[ItemPosition.RightHand] != null &&
                        UserPackage[ItemPosition.LeftHand] != null)
                    {
                        Item item = UserPackage[ItemPosition.RightHand];
                        Item arrow = UserPackage[ItemPosition.LeftHand];
                        if (arrow.GetItemSubtype() == 1050 && arrow.Durability >= dwNum)
                            return true;
                    }
                }

                if (dwItem == 500)
                {
                    if (UserPackage[ItemPosition.RightHand] != null &&
                        UserPackage[ItemPosition.LeftHand] != null)
                    {
                        Item item = UserPackage[ItemPosition.RightHand];
                        if (item.GetItemSubtype() == idItem && item.Durability >= dwNum)
                            return true;
                    }
                }

                if (dwItem == 420)
                {
                    if (UserPackage[ItemPosition.RightHand] != null)
                    {
                        Item item = UserPackage[ItemPosition.RightHand];
                        for (int i = 0; i < setSword.Length; i++)
                        {
                            if (item.GetItemSubtype() == setSword[i] && item.Durability >= dwNum)
                                return true;
                        }
                    }
                }

                if (dwItem == 601 || dwItem == 610 || dwItem == 611 || dwItem == 612 || dwItem == 613)
                {
                    if (UserPackage[ItemPosition.RightHand] != null)
                    {
                        Item item = UserPackage[ItemPosition.RightHand];
                        if (item.GetItemSubtype() == dwItem && item.Durability >= dwNum)
                            return true;
                    }
                }
            }

            return false;
        }

        public bool AutoSkillAttack(Role target)
        {
            foreach (var magic in Magics.Magics.Values)
            {
                float percent = magic.Percent;
                if (magic.Type == 10490 && target is Character) // triple exception
                    percent = 33.33333333f;
                if (magic.AutoActive > 0
                    && QueryTransformation == null
                    && (magic.WeaponSubtype == 0
                        || CheckWeaponSubType(magic.WeaponSubtype, magic.UseItemNum))
                    && Calculations.ChanceCalc(percent))
                {
                    return ProcessMagicAttack(magic.Type, target.Identity, target.MapX, target.MapY, magic.AutoActive);
                }
            }

            return false;
        }

        public void AdrenalineRush()
        {
            Magic pMagic = Magics[11030];
            if (pMagic == null)
                return;
            pMagic.ClearDelay();
            MsgMagicEffect pMsg = new MsgMagicEffect
            {
                Identity = Identity,
                SkillIdentity = 11130,
                SkillLevel = 0
            };
            pMsg.AppendTarget(Identity, 11030, false, 0, 0);
            Screen.Send(pMsg, true);
        }

        public override bool SetAttackTarget(Role target)
        {
            if (target == null)
            {
                BattleSystem.ResetBattle();
                return false;
            }

            if (!target.IsAttackable(this))
                return false;

            if (target.IsWing && !IsWing && !IsBowman)
                return false;

            if (target is DynamicNpc npc && npc.IsGoal() && (npc.Level > Level || npc.Type != 21))
                return false;

            if (GetDistance(target.MapX, target.MapY) > GetAttackRange(SizeAddition) + 1 &&
                QueryStatus(FlagInt.FATAL_STRIKE) == null)
            {
                BattleSystem.ResetBattle();
                return false;
            }

            BattleSystem.CreateBattle(target.Identity);

            CalculateFightRate();
            return true;
        }

        public override int CalculateFightRate()
        {
            return m_nFightPause = GetInterAtkRate();
        }

        public int GetInterAtkRate()
        {
            if (QueryTransformation != null)
                return QueryTransformation.InterAtkRate;

            int nRate = USER_ATTACK_SPEED;
            int nRateR = 0, nRateL = 0;

            if (UserPackage[ItemPosition.RightHand] != null)
                nRateR = UserPackage[ItemPosition.RightHand].Itemtype.AtkSpeed;
            if (UserPackage[ItemPosition.LeftHand] != null)
                nRateL = UserPackage[ItemPosition.LeftHand].Itemtype.AtkSpeed;

            if (nRateR > 0 && nRateL > 0)
                nRate = (nRateR + nRateL) / 2;
            else if (nRateR > 0)
                nRate = nRateR;
            else if (nRateL > 0)
                nRate = nRateL;

#if DEBUG
            if (QueryStatus(FlagInt.CYCLONE) != null)
            {
                nRate = Calculations.CutTrail(0,
                    Calculations.AdjustData(nRate, QueryStatus(FlagInt.CYCLONE).Power));
                if (IsPm())
                    SendSysMessage($"atack speed+: {nRate}");
            }
            else if (QueryStatus(FlagInt.OBLIVION) != null)
            {
                nRate /= 2;
                if (IsPm())
                    SendSysMessage($"atack speed+: {nRate}");
            }
#endif

            return Math.Max(500, 250+nRate);
        }

        public override int GetAttackRange(int nTargetSizeAdd)
        {
            if (QueryTransformation != null)
                return QueryTransformation.GetAttackRange(nTargetSizeAdd);

            int nRange = 1, nRangeL = 0, nRangeR = 0;

            if (UserPackage[ItemPosition.RightHand] != null && UserPackage[ItemPosition.RightHand].IsWeapon())
                nRangeR = UserPackage[ItemPosition.RightHand].AttackRange;
            if (UserPackage[ItemPosition.LeftHand] != null && UserPackage[ItemPosition.LeftHand].IsWeapon())
                nRangeL = UserPackage[ItemPosition.LeftHand].AttackRange;

            if (nRangeR > 0 && nRangeL > 0)
                nRange = (nRangeR + nRangeL) / 2;
            else if (nRangeR > 0)
                nRange = nRangeR;
            else if (nRangeL > 0)
                nRange = nRangeL;

            nRange += (SizeAddition + nTargetSizeAdd + 1) / 2;

            return nRange + 1;
        }

        public override int Attack(Role target, ref InteractionEffect effect)
        {
            if (target == null)
                return 0;
            DynamicNpc npc = null;
            if (target is DynamicNpc)
                npc = target as DynamicNpc;

            int nDamage = BattleSystem.CalcPower(0, this, target, ref effect);

            int nLoseLife = (int) Calculations.CutOverflow(nDamage, target.Life);

            //if (npc != null && npc.IsSynFlag())
                //nDamage *= SYNWAR_NOMONEY_DAMAGETIMES;    

            if (!target.IsEvil() && Map.IsDeadIsland() || target is Monster mob && mob.IsGuard())
                SetCrimeStatus(15);

            return nDamage;
        }

        public override bool IsImmunity(Role target)
        {
            if (base.IsImmunity(target))
                return true;

            if (IsArenaWitness)
                return true;

            if (target is Character user)
            {
                if (user.IsArenaWitness && user.MapIdentity >= 900000)
                    return true;

                switch (PkMode)
                {
                    case PkModeType.FreePk:
                        if (Level >= 70 && user.Level < 27) // newbie protection
                            return true;
                        return false;
                    case PkModeType.Capture:
                        return !user.IsEvil();
                    case PkModeType.Peace:
                        return true;
                    case PkModeType.Team:
                        if (Team != null && Team.IsTeamMember(user))
                            return true;
                        if (ContainsFriend(user.Identity))
                            return true;
                        if (IsMate(user))
                            return true;
                        if (Syndicate != null && (Syndicate.Members.ContainsKey(user.Identity) ||
                                                  Syndicate.IsFriendly((ushort) user.SyndicateIdentity)))
                            return true;
                        return false;
                    case PkModeType.Revenge:
                        return !ContainsEnemy(user.Identity);
                    case PkModeType.JiangHu:
                    {
                        return !user.JiangHuActive && !user.IsEvil();
                    }
                }
            }
            else if (target is Monster mob)
            {
                switch (PkMode)
                {
                    case PkModeType.Revenge:
                    case PkModeType.Peace:
                        return false;
                    case PkModeType.Team:
                    case PkModeType.Capture:
                        if (mob.IsGuard())
                            return true;
                        return false;
                    case PkModeType.FreePk:
                    case PkModeType.JiangHu:
                        return false;
                }
            }
            else if (target is DynamicNpc npc)
            {
                return false;
            }
            return true;
        }

        public override bool BeAttack(int magic, Role attacker, int nPower, bool bReflectEnable)
        {
            if (attacker == null)
                return false;

            StopMine();

            if ((LastProfession == ProfessionType.WarriorMaster || FirstProfession == ProfessionType.WarriorMaster)
                && bReflectEnable)
            {
                if (Calculations.ChanceCalc(15f))
                {
                    nPower = Math.Min(1700, nPower);
                    attacker.BeAttack(magic, this, nPower, false);
                    Screen.Send(new MsgInteract
                    {
                        Action = InteractionType.ACT_ITR_REFLECT_MAGIC,
                        EntityIdentity = Identity,
                        TargetIdentity = attacker.Identity,
                        CellX = MapX,
                        CellY = MapY,
                        Data = (uint) nPower
                    }, true);
                    nPower = 0;
                }
            }

            if (nPower > 0)
            {
                uint nLoseLife = (uint) Math.Min(Life, nPower);
                AddAttrib(ClientUpdateType.Hitpoints, nLoseLife * -1);

                if (ServerKernel.ArenaQualifier.IsUserInsideMatch(Identity))
                {
                    ServerKernel.ArenaQualifier.FindMatch(Identity).AddPoints(Identity, nLoseLife);
                }
            }

            if (!IsAlive)
            {
                if (ServerKernel.ArenaQualifier.IsUserInsideMatch(Identity))
                    ServerKernel.ArenaQualifier.FindMatch(Identity).QuitMatch(Identity);

                BeKill(attacker);
            }
            else
            {
                Magic pAdrenaline = Magics[11130];
                Magic pEagleEye = Magics[11030];
                if (pAdrenaline != null && Calculations.ChanceCalc(pAdrenaline.Percent)
                                        && pEagleEye != null && !pEagleEye.IsReady())
                {
                    AdrenalineRush();
                }
            }

            if (!Map.IsTrainingMap())
                DecEquipmentDurability(true, magic, 1);

            if (Action == EntityAction.Sit)
                Stamina /= 2;

            return true;
        }

        public override void BeKill(Role attacker)
        {
            if (QueryStatus(FlagInt.DEAD) != null)
                return;

            BattleSystem.ResetBattle();
            BattleSystem.DestroyAutoAttack();

            DetachStatus(FlagInt.BLUE_NAME);
            DetachAllStatus();
            AttachStatus(this, FlagInt.DEAD, 0, int.MaxValue, 0, 0);
            AttachStatus(this, FlagInt.GHOST, 0, int.MaxValue, 0, 0);
            m_tDoGhost.Startup(3);

            ClsXpVal();

            bool arena = ServerKernel.ArenaQualifier.IsUserInsideMatch(Identity);
            var pos = new Point(MapX, MapY);
            uint idMap = 0u;
            if (Map.GetRebornMap(ref idMap, ref pos) && !arena)
                SetRecordPos(idMap, (ushort) pos.X, (ushort) pos.Y);

            if (Map.IsPkField() || Map.IsSynMap())
            {
                if (!Map.IsDeadIsland())
                    UserPackage.RandDropItem(1, 30);

                if (Map.IsSynMap() && !Map.IsWarTime() && !arena)
                    SetRecordPos(1002, 438, 398);
                return;
            }

            if (attacker is Character atkUsr && atkUsr.PkMode == PkModeType.JiangHu && JiangHuActive)
                return;

            if (Map.IsPrisionMap())
            {
                if (!Map.IsDeadIsland())
                {
                    int nChance = Math.Min(90, 20 + PkPoints / 2);
                    UserPackage.RandDropItem(3, nChance);
                }
                return;
            }

            if (attacker == null)
                return;

            if (!Map.IsDeadIsland())
            {
                if (attacker is Character attackerUser && attacker != this)
                    CreateEnemy(attackerUser);

                int nChance = 0;
                if (PkPoints < 30)
                    nChance = 10 + ThreadSafeRandom.RandGet(40);
                else if (PkPoints < 100)
                    nChance = 50 + ThreadSafeRandom.RandGet(50);
                else
                    nChance = 100;

                int nItems = UserPackage.GetInventory().Count;
                int nDropItem = nItems * nChance / 100;

                UserPackage.RandDropItem(nDropItem);

                if (attacker != this && attacker is Character targetUser)
                {
                    if (IsBlessed && !targetUser.IsBlessed)
                    {
                        const int _CURSE_TIME = 300000;
                        const int _MAX_CURSE_TIME = 300000 * 12;
                        if (QueryStatus(FlagInt.CURSED) != null)
                            QueryStatus(FlagInt.CURSED).IncTime(_CURSE_TIME, _MAX_CURSE_TIME);
                        else
                            targetUser.AttachStatus(this, FlagInt.CURSED, 0, _CURSE_TIME, 0, 0);
                    }

                    float nLossPercent;
                    if (PkPoints < 30)
                        nLossPercent = 0.01f;
                    else if (PkPoints < 100)
                        nLossPercent = 0.02f;
                    else nLossPercent = 0.03f;
                    long nLevExp = (long) Experience;
                    long nLostExp = (long) (nLevExp * nLossPercent);

                    if (nLostExp > 0)
                    {
                        AddAttrib(ClientUpdateType.Experience, nLostExp * -1);
                        attacker.AddAttrib(ClientUpdateType.Experience, nLostExp / 3);
                    }

                    int nLevDiff = targetUser.Level - Level;
                    if (Syndicate != null && targetUser.Syndicate != null && nLevDiff <= 5)
                    {
                        Syndicate.Send(string.Format(Language.StrSynEnemyKill0,
                            SyndicateMember.GetRankName(), Name, targetUser.Name,
                            targetUser.SyndicateMember.GetRankName(), targetUser.Syndicate.Name, Map.Name));
                        targetUser.Syndicate.Send(string.Format(Language.StrSynEnemyKill1,
                            targetUser.SyndicateMember.GetRankName(), targetUser.Name, Name,
                            SyndicateMember.GetRankName(), SyndicateName, Map.Name));

                        SyndicateMember.DecreasePkDonation(3);
                        targetUser.SyndicateMember.IncreasePkDonation(3);
                    }

                    if (PkPoints >= 300)
                    {
                        DropRandomEquipmentFloor();
                        DropRandomEquipmentFloor();
                    }
                    else if (PkPoints >= 100)
                    {
                        DropRandomEquipmentFloor();
                    }
                    else if (PkPoints >= 30 && Calculations.ChanceCalc(30f))
                    {
                        DropRandomEquipmentFloor();
                    }

                    if (PkPoints > 99)
                    {
                        SetRecordPos(6000, 31, 72);
                        FlyMap(6000, 31, 72);
                        ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrGoToJail, attacker.Name, Name),
                            ChatTone.Talk);
                    }
                }
            }
            else if (attacker is Character && Map.IsDeadIsland())
            {
                if (ContainsEnemy(attacker.Identity))
                    CreateEnemy(attacker as Character);
            }
            else if (attacker is Monster monster)
            {
                if (monster.IsGuard() && PkPoints > 99)
                {
                    SetRecordPos(6000, 31, 72);
                    FlyMap(6000, 31, 72);
                    ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrGoToJail, attacker.Name, Name),
                        ChatTone.Talk);
                }
            }

            if (attacker is Character user)
            {
                Database.KillDeathRepository.Save(new KillDeathEntity
                {
                    TargetIdentity = Identity,
                    TargetName = Name,
                    TargetMap = MapIdentity,
                    TargetMapX =  MapX,
                    TargetMapY = MapY,
                    TargetBattlePower = (ushort) BattlePower,
                    TargetLevel = Level,
                    TargetProfession = (byte) Profession,
                    AttackerIdentity = user.Identity,
                    AttackerName = user.Name,
                    AttackerMap = user.MapIdentity,
                    AttackerMapX = user.MapX,
                    AttackerMapY = user.MapY,
                    AttackerBattlePower = (ushort) user.BattlePower,
                    AttackerLevel = user.Level,
                    AttackerProfession = (byte) user.Profession,
                    Timestamp = DateTime.Now,
                    BattleMode = 0 // todo add classic mode
                });
            }

            //GameAction.ProcessAction(8000001, this, this, null, null);

            if (Scapegoat)
            {
                Scapegoat = false;
                Send(new MsgInteract
                {
                    Action = InteractionType.ACT_ITR_COUNTER_KILL_SWITCH,
                    TargetIdentity = Identity,
                    EntityIdentity = Identity,
                    Damage = 0,
                    CellX = MapX,
                    CellY = MapY
                });
            }
        }

        public void DropRandomEquipmentFloor()
        {
            var equipment = UserPackage.GetEquipment();
            equipment.RemoveAll(x => !x.IsPkLossEnable());
            if (equipment.Count == 0)
                return;

            var item = equipment[ThreadSafeRandom.RandGet(equipment.Count) % equipment.Count];
            item.SystemInstantUnlock();
            if (item.IsInscribed)
            {
                Syndicate?.Arsenal.UninscribeItem(item, this);
                item.IsInscribed = false;
            }
            item.CreateMsgItemInfo(ItemMode.Update);
            UserPackage.Unequip(item.Position);
            DropItem(item.Identity, MapX, MapY, true);
            ServerKernel.Log.GmLog("pk_drop_item", $"Item dropped by: {Name}:{Identity} on {MapIdentity}:{MapX},{MapY}\r\n\t{item.ToJson()}");
        }

        public void ProcessPk(Character target)
        {
            if (!Map.IsPkField() && !Map.IsPkGameMap() && !Map.IsSynMap() && !Map.IsPrisionMap())
            {
                // innocent kill
                if (PkMode == PkModeType.JiangHu && target.JiangHuActive)
                {
                    return;
                }

                if (!Map.IsDeadIsland() && !target.IsEvil())
                {
                    int nAddPk = 10;
                    if (target.Level < 130)
                    {
                        nAddPk = 20;
                    }
                    else
                    {
                        if (Syndicate?.IsHostile((ushort) target.SyndicateIdentity) == true)
                            nAddPk = 3;
                        else if (ContainsEnemy(target.Identity))
                            nAddPk = 5;
                        if (target.PkPoints > 29)
                            nAddPk /= 2;
                    }

                    PkPoints += (ushort) nAddPk;

                    SetCrimeStatus(60);

                    if (PkPoints > 29)
                        SendSysMessage(Language.StrKillingTooMuch);
                }
            }
        }

        public override void Kill(Role target, uint dwDieWay)
        {
            if (target == null)
                return;

            Character pTargetUser = target as Character;
            if (pTargetUser != null)
            {
                Screen.Send(new MsgInteract
                {
                    Action = InteractionType.ACT_ITR_KILL,
                    EntityIdentity = Identity,
                    TargetIdentity = target.Identity,
                    CellX = target.MapX,
                    CellY = target.MapY,
                    MagicType = (ushort) dwDieWay,
                    MagicLevel = 0
                }, true);

                GameAction.ProcessAction(8000000, this, this, null, null);

                if (PkMode == PkModeType.JiangHu && pTargetUser.JiangHuActive)
                {
                    if (KongFu.CurrentTalent <= pTargetUser.KongFu.CurrentTalent)
                    {
                        KongFu.CurrentTalent += 1;
                        pTargetUser.KongFu.CurrentTalent -= 1;
                    }
                }

                if (PkMode != PkModeType.JiangHu)
                    PkExploit?.Kill(pTargetUser);

                if (Magics.QueryMagic()?.Sort != MagicData.MAGICSORT_ACTIVATESWITCH)
                    ProcessPk(pTargetUser);
            }
            else
            {
                if (QueryStatus(FlagInt.CYCLONE) != null || QueryStatus(FlagInt.SUPERMAN) != null)
                {
                    m_nKoCount += 1;
                    var status = QueryStatus(FlagInt.CYCLONE) ?? QueryStatus(FlagInt.SUPERMAN);
                    status?.IncTime(700, 30000);
                }

                if (QueryStatus(FlagInt.OBLIVION) != null)
                {
                    m_nKoCount += 1;

                    if (m_nKoCount >= 32)
                    {
                        AwardOblivion();
                        ResetOblivion();
                    }
                }

                XpPoints += 1;
            }

            target.BeKill(this);
        }

        public void AwardSynWarScore(DynamicNpc pNpc, int nScore)
        {
            if (pNpc == null || nScore <= 0)
                return;

            if (Syndicate == null || SyndicateMember == null || pNpc.OwnerIdentity == Syndicate.Identity)
                return;

            int nAddProffer = Calculations.MulDiv(nScore, SYNWAR_PROFFER_PERCENT, 100);

            if (nAddProffer > 0)
                Syndicate.ChangeFunds(nAddProffer);

            //int nAddMoney = Calculations.MulDiv(nScore, SYNWAR_MONEY_PERCENT, 100); 2019-10-18 felipe
            if (nAddProffer > 0)
            {
                Syndicate syn = ServerKernel.SyndicateManager.GetSyndicate(pNpc.OwnerIdentity);
                if (syn != null)
                {
                    nAddProffer = (int) Calculations.CutOverflow(nAddProffer, (long) syn.SilverDonation);
                    syn.ChangeFunds(nAddProffer * -1);
                    AwardMoney(nAddProffer);
                }
            }

            // if (Syndicate != null) 2015-02-18 removed for being redundant #felipe
            Syndicate?.AddSynWarScore(pNpc, (uint) nScore);
        }

        public void SendWeaponMagic2(Role pTarget = null)
        {
            Item item = null;

            if (UserPackage[ItemPosition.RightHand] != null &&
                UserPackage[ItemPosition.RightHand].Effect != ItemEffect.None)
                item = UserPackage[ItemPosition.RightHand];
            if (UserPackage[ItemPosition.LeftHand] != null &&
                UserPackage[ItemPosition.LeftHand].Effect != ItemEffect.None)
                if (item != null && Calculations.ChanceCalc(50f) || item == null)
                    item = UserPackage[ItemPosition.LeftHand];

            if (item != null)
            {
                switch (item.Effect)
                {
                    case ItemEffect.Life:
                    {
                        if (!Calculations.ChanceCalc(15f))
                            return;
                        AddAttrib(ClientUpdateType.Hitpoints, 310);
                        var msg = new MsgMagicEffect
                        {
                            Identity = m_pOwner.Identity,
                            SkillIdentity = 1005
                        };
                        msg.AppendTarget(Identity, 310, false, 0, 0);
                        Screen.Send(msg, true);
                        break;
                    }

                    case ItemEffect.Mana:
                    {
                        if (!Calculations.ChanceCalc(17.5f))
                            return;
                        AddAttrib(ClientUpdateType.Mana, 310);
                        var msg = new MsgMagicEffect
                        {
                            Identity = Identity,
                            SkillIdentity = 1195
                        };
                        msg.AppendTarget(Identity, 310, false, 0, 0);
                        Screen.Send(msg, true);
                        break;
                    }

                    case ItemEffect.Poison:
                    {
                        if (pTarget == null || pTarget is DynamicNpc)
                            return;

                        if (!Calculations.ChanceCalc(5f))
                            return;

                        var msg = new MsgMagicEffect
                        {
                            Identity = Identity,
                            SkillIdentity = 1320
                        };
                        msg.AppendTarget(pTarget.Identity, 210, true, 0, 0);
                        Screen.Send(msg, true);

                        pTarget.AttachStatus(this, FlagInt.POISONED, 310, 2, 20, 0);
                        var special = InteractionEffect.None;
                        int nTargetLifeLost = Attack(pTarget, ref special);
                        SendDamageMsg(pTarget.Identity, nTargetLifeLost, special);

                        if (!pTarget.IsAlive)
                        {
                            int dwDieWay = 1;
                            if (nTargetLifeLost > pTarget.MaxLife / 3)
                                dwDieWay = 2;

                            Kill(pTarget, IsBowman ? 5 : (uint) dwDieWay);
                        }

                        break;
                    }
                }
            }
        }

        public override bool SpendEquipItem(uint dwItem, uint dwAmount, bool bSynchro)
        {
            if (dwItem <= 0)
                return false;

            Item item = null;
            if (UserPackage[ItemPosition.RightHand]?.GetItemSubtype() == dwItem &&
                UserPackage[ItemPosition.RightHand]?.Durability >= dwAmount)
                item = UserPackage[ItemPosition.RightHand];
            else if (UserPackage[ItemPosition.LeftHand]?.GetItemSubtype() == dwItem
                     && UserPackage[ItemPosition.LeftHand]?.Durability >= dwAmount)
                item = UserPackage[ItemPosition.LeftHand];

            if (item == null)
                return false;

            if (!item.IsExpend() && item.Durability < dwAmount)
                return false;

            if (item.IsExpend() && item.Durability >= dwAmount)
            {
                item.Durability -= (ushort) dwAmount;
                if (bSynchro)
                    Send(item.CreateMsgItemInfo(ItemMode.Update));
            }
            else
            {
                if (item.IsNonsuchItem())
                {
                    ServerKernel.Log.GmLog("SpendEquipItem",
                        $"{Name}({Identity}) Spend item:[id={item.Identity}, type={item.Type}], dur={item.Durability}, max_dur={item.MaxDurability}");
                }
            }

            return true;
        }

        public override bool DecEquipmentDurability(bool bAttack, int hitByMagic, ushort useItemNum)
        {
            int nInc = -1 * useItemNum;

            for (ItemPosition i = ItemPosition.Headwear; i <= ItemPosition.Crop; i++)
            {
                if (i == ItemPosition.Garment || i == ItemPosition.Gourd || i == ItemPosition.Steed
                    || i == ItemPosition.SteedArmor || i == ItemPosition.LeftHandAccessory ||
                    i == ItemPosition.RightHandAccessory)
                    continue;
                if (hitByMagic == 1)
                {
                    if (i == ItemPosition.Ring
                        || i == ItemPosition.RightHand
                        || i == ItemPosition.LeftHand
                        || i == ItemPosition.Boots)
                    {
                        if (!bAttack)
                            AddEquipmentDurability(i, nInc);
                    }
                    else
                    {
                        if (bAttack)
                            AddEquipmentDurability(i, nInc);
                    }
                }
                else
                {
                    if (i == ItemPosition.Ring
                        || i == ItemPosition.RightHand
                        || i == ItemPosition.LeftHand
                        || i == ItemPosition.Boots)
                    {
                        if (!bAttack)
                            AddEquipmentDurability(i, -1);
                    }
                    else
                    {
                        if (bAttack)
                            AddEquipmentDurability(i, nInc);
                    }
                }
            }

            return true;
        }

        public void AddEquipmentDurability(ItemPosition pos, int nInc)
        {
            if (nInc >= 0)
                return;

            Item item = UserPackage[pos];
            if (item == null
                || !item.IsEquipment()
                || item.GetItemSubtype() == 2100)
                return;

            ushort nOldDur = item.Durability;
            ushort nDurability = (ushort) Math.Max(0, item.Durability + nInc);

            if (nDurability < 100)
            {
                if (nDurability % 10 == 0)
                    SendSysMessage(string.Format(Language.StrDamagedRepair, item.Itemtype.Name));
            }
            else if (nDurability < 200)
            {
                if (nDurability % 10 == 0)
                    SendSysMessage(string.Format(Language.StrDurabilityRepair, item.Itemtype.Name));
            }

            item.Durability = nDurability;

            int noldDur = (int) Math.Floor(nOldDur / 100f);
            int nnewDur = (int) Math.Floor(nDurability / 100f);

            if (nDurability <= 0)
            {
                Send(item.CreateMsgItem(ItemAction.Durability));
            }
            else if (noldDur != nnewDur)
            {
                Send(item.CreateMsgItemInfo(ItemMode.Update));
            }
        }

        public void SendGemEffect()
        {
            var setGem = new List<SocketGem>();

            foreach (var item in UserPackage.GetEquipment().Where(x => x.SocketOne != SocketGem.NoSocket))
            {
                setGem.Add(item.SocketOne);
                if (item.SocketTwo != SocketGem.NoSocket)
                    setGem.Add(item.SocketTwo);
            }

            int nGems = setGem.Count;
            if (nGems <= 0)
                return;

            string strEffect = "";
            switch (setGem[ThreadSafeRandom.RandGet(0, nGems)])
            {
                case SocketGem.SuperPhoenixGem:
                    strEffect = "phoenix";
                    break;
                case SocketGem.SuperDragonGem:
                    strEffect = "goldendragon";
                    break;
                case SocketGem.SuperFuryGem:
                    strEffect = "fastflash";
                    break;
                case SocketGem.SuperRainbowGem:
                    strEffect = "rainbow";
                    break;
                case SocketGem.SuperKylinGem:
                    strEffect = "goldenkylin";
                    break;
                case SocketGem.SuperVioletGem:
                    strEffect = "purpleray";
                    break;
                case SocketGem.SuperMoonGem:
                    strEffect = "moon";
                    break;
            }

            SendEffect(strEffect, true);
        }

        public override int AdjustWeaponDamage(int nDamage)
        {
            nDamage = Calculations.MulDiv(nDamage, Defense2, Calculations.DEFAULT_DEFENCE2);

            int type1 = 0, type2 = 0;
            if (UserPackage[ItemPosition.RightHand] != null)
                type1 = UserPackage[ItemPosition.RightHand].GetItemSubtype();
            if (UserPackage[ItemPosition.LeftHand] != null)
                type2 = UserPackage[ItemPosition.LeftHand].GetItemSubtype();

            if (type1 > 0 && WeaponSkill.Skills.ContainsKey((ushort) type1) &&
                WeaponSkill.Skills[(ushort) type1].Level > 12)
            {
                nDamage = (int) (nDamage * (1 + (20 - WeaponSkill.Skills[(ushort) type1].Level) / 100f));
            }
            else if (type2 > 0 && WeaponSkill.Skills.ContainsKey((ushort) type2) &&
                     WeaponSkill.Skills[(ushort) type2].Level > 12)
            {
                nDamage = (int) (nDamage * (1 + (20 - WeaponSkill.Skills[(ushort) type2].Level) / 100f));
            }

            return nDamage;
        }

        #endregion

        #region Lottery

        public void AcceptLotteryPrize()
        {
            if (LotteryTemporaryItem == null)
                return;

            Item item = UserPackage.AwardItem(LotteryTemporaryItem);

            if (item == null)
                return;

            if (LotteryLastRank < 5)
            {
                ServerKernel.UserManager.SendToAllUser($"{Name} won a {LotteryLastItemName} from the lottery.", Color.White, ChatTone.Talk);
            }
            else
            {
                SendSysMessage($"You won {LotteryLastItemName} from the lottery.", Color.White);
            }

            Statistics.AddOrUpdate(22, 1, 0, true);
            Statistics.AddOrUpdate(22, 2, 1, true);
            Lottery.Clear(this);
        }

        public void LotteryTryAgain()
        {
            if (LotteryTemporaryItem == null)
                return; // user has really tried lottery before? There's no prize pending.

            if (GetStatisticValue(22, 1) >= 3)
                return; // user has already tried 3 times

            if (!UserPackage.SpendItem(SpecialItem.SMALL_LOTTERY_TICKET, 1))
            {
                SendSysMessage(Language.StrEmbedNoRequiredItem);
                return;
            }

            Lottery.GenerateItem(this, false);
        }

        #endregion

        #region House

        public uint HomeIdentity => m_dbUser.HomeId;

        public uint HomeMateIdentity
        {
            get
            {
                if (Mate == "None" || GetMate() == null)
                    return 0;
                return GetMate().HomeId;
            }
        }

        public void SetHomeId(uint idMap)
        {
            m_dbUser.HomeId = idMap;
            Save();
        }

        public CharacterEntity GetMate()
        {
            return Mate == "None" ? null : new CharacterRepository().SearchByName(Mate);
        }

        #endregion

        #region Jiang Hu

        public int CurrentJiangHuStage => m_pJiangHu?.CurrentStage ?? 0;

        public JiangHu KongFu => m_pJiangHu;

        public byte TalentPoints
        {
            get => m_pJiangHu?.CurrentTalent ?? 0;
            set
            {
                if (m_pJiangHu == null)
                    return;
                m_packet.TalentPoints = value;
                Screen.RefreshSpawnForObservers();
            }
        }

        public bool JiangHuActive
        {
            get => m_pJiangHu.IsJiangActive;
            set
            {
                m_packet.KongFuActive = value;
                Screen.RefreshSpawnForObservers();
            }
        }

        #endregion

        #region Arena Qualifier

        public bool ChangeHonorPoints(int amount)
        {
            if (amount > 0)
            {
                HonorPoints += (uint) amount;
                PlayerQualifier.TotalHonorPoints += (uint)amount;
                PlayerQualifier.Save();
                Save();
                return true;
            }

            if (amount < 0 && amount*-1 <= HonorPoints)
            {
                HonorPoints -= (uint) (amount * -1);
                return true;
            }
            return false;
        }

        public void SendArenaInformation(Character pTarget)
        {
            pTarget.Send(new MsgQualifyingInteractive
            {
                Type = ArenaType.START_THE_FIGHT,
                Name = Name,
                Level = Level,
                Profession = (uint)Profession,
                ArenaPoints = PlayerQualifier.Points,
                Rank = (uint)PlayerQualifier.Ranking,
                Identity = Identity
            });
        }

        public void SendArenaScreen()
        {
            try
            {
                if (PlayerQualifier == null && !ServerKernel.ArenaQualifier.GenerateFirstData(this))
                    return;
                if (PlayerQualifier == null)
                    return; // double checking

                Send(new MsgQualifyingDetailInfo
                {
                    Ranking = (uint)PlayerQualifier.Ranking,
                    ArenaPoints = PlayerQualifier.Points,
                    CurrentHonor = HonorPoints,
                    TotalHonor = PlayerQualifier.TotalHonorPoints,
                    TotalLose = PlayerQualifier.TotalLoss,
                    TodayWins = PlayerQualifier.Wins,
                    TodayLose = PlayerQualifier.Loss,
                    TotalWins = PlayerQualifier.TotalWins,
                    Status = PlayerQualifier.Status
                });
            }
            catch (Exception ex)
            {
                Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
            }
        }

        public void SetWitness(bool isWitness)
        {
            m_packet.IsArenaWitness = isWitness;
        }

        public bool IsArenaWitness => m_packet.IsArenaWitness && MapIdentity >= 900000;

        #endregion

        #region Team Qualifier

        public void TeamArenaScreen()
        {
            try
            {
                if (TeamPlayerQualifier == null && !ServerKernel.TeamQualifier.GenerateFirstData(this))
                    return;
                if (TeamPlayerQualifier == null)
                    return; // double checking

                Send(new MsgQualifyingDetailInfo
                {
                    Ranking = (uint)TeamPlayerQualifier.Ranking,
                    ArenaPoints = TeamPlayerQualifier.Points,
                    CurrentHonor = HonorPoints,
                    TotalHonor = TeamPlayerQualifier.TotalHonorPoints,
                    TotalLose = TeamPlayerQualifier.TotalLoss,
                    TodayWins = TeamPlayerQualifier.Wins,
                    TodayLose = TeamPlayerQualifier.Loss,
                    TotalWins = TeamPlayerQualifier.TotalWins,
                    Status = TeamPlayerQualifier.Status
                });
            }
            catch (Exception ex)
            {
                Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
            }
        }

        public void SendTeamArenaInformation(Character pTarget)
        {
            pTarget.Send(new MsgQualifyingInteractive
            {
                Type = ArenaType.START_THE_FIGHT,
                Name = Name,
                Level = Level,
                Profession = (uint)Profession,
                ArenaPoints = PlayerQualifier.Points,
                Rank = (uint)PlayerQualifier.Ranking,
                Identity = Identity
            });
        }

        #endregion

        #region OnTimer

        public void OnSecondaryTimer()
        {
            try
            {
                //send date time packet
                if (m_timePacket.ToNextTime())
                {
                    Save();
                    if (Send(new MsgData()) < 0)
                    {
                        ServerKernel.UserManager.KickoutSocket(this, $"User socket could not be found.");
                        return;
                    }
                }
            }
            catch
            {
                Program.WriteLog("TIME PACKET ERROR");
            }

            try
            {
                if (m_tItemCheck.ToNextTime())
                {
                    List<Item> items = UserPackage.GetAll();
                    foreach (var warehouse in Warehouses.Values)
                    {
                        items.AddRange(warehouse.Items.Values.ToList());
                    }

                    foreach (var item in items)
                    {
                        item.CheckForPurificationExpired();
                        if (item.ItemExpired())
                        {
                            if (item.Position == ItemPosition.Inventory)
                                UserPackage.RemoveFromInventory(item, RemovalType.Delete);
                            else
                                UserPackage.Unequip(item.Position, RemovalType.Delete);

                            ServerKernel.Log.GmLog($"item_expire", $"Userid: {Identity}, Username: {Name}\r\n\t{item.ToJson()}");
                        }

                        if (item.IsLocked)
                            item.TryUnlock();
                    }
                }
            }
            catch
            {
                Program.WriteLog("ITEMS EXPIRE CHECK ERROR");
            }

            try
            {
                if (KongFu.HasStarted)
                    KongFu.OnTimer();
            }
            catch
            {
                Program.WriteLog("KONG FU CHECK ERROR");
            }
        }

        public override void OnTimer()
        {
            try
            {
                // send team leader position
                if (m_tTeamPos.ToNextTime() && Team != null && Team.Leader != this &&
                    Team.Leader.MapIdentity == MapIdentity)
                    Team.SendLeaderPosition(this);
            }
            catch
            {
                Program.WriteLog("ERROR TEAM LEADER POS");
            }

            try
            {
                if (m_tPkDecrease.ToNextTime() && PkPoints > 0)
                {
                    ChangePkPoints((short) (MapIdentity == 6000 ? -3 : -1));
                }
            }
            catch
            {
                Program.WriteLog("ERROR PK DECREASe");
            }

            try
            {
                if (IsBlessed && m_tHeavenBlessing.ToNextTime() && !m_pMap.IsTrainingMap())
                {
                    m_pBlessPoints++;
                    if (m_pBlessPoints >= 5)
                    {
                        m_pBlessPoints = 0;
                        AwardExperience(CalculateExpBall(100));
                        UpdateClient(ClientUpdateType.OnlineTraining, 5);
                        UpdateClient(ClientUpdateType.OnlineTraining, 0);
                    }
                    else
                    {
                        UpdateClient(ClientUpdateType.OnlineTraining, 4);
                        UpdateClient(ClientUpdateType.OnlineTraining, 3);
                    }
                }
            }
            catch
            {
                Program.WriteLog("ERROR BLESS ADD CHECK");
            }

            try
            {
                if (m_tStatusCheck.ToNextTime())
                {
                    foreach (var stts in Status.Status.Values)
                    {
                        stts.OnTimer();
                        if (!stts.IsValid && stts.Identity != FlagInt.GHOST && stts.Identity != FlagInt.DEAD)
                        {
                            m_nFightPause = GetInterAtkRate();
                            UpdateAttributes();

                            if (stts.Identity == FlagInt.OBLIVION
                                && Status.DelObj(stts.Identity))
                            {
                                AwardOblivion(false);
                                ResetOblivion();
                            }
                            else if (Status.DelObj(stts.Identity) &&
                                     (stts.Identity == FlagInt.CYCLONE || stts.Identity == FlagInt.SUPERMAN))
                            {
                                if (m_nKoCount < MIN_SUPERMAP_KILLS)
                                    break;

                                int nPosition = 0;
                                if (ServerKernel.Superman.Count == 0)
                                    nPosition = 1;
                                foreach (var ko in ServerKernel.Superman.Values.OrderByDescending(x => x.KoCount)
                                    .ThenBy(x => x.Identity))
                                {
                                    if (nPosition++ >= 100)
                                        break;
                                    if (ko.KoCount > m_nKoCount)
                                        continue;
                                    break;
                                }

                                if (nPosition < 101)
                                {
                                    IKoCount temp;
                                    if (!ServerKernel.Superman.TryRemove(Identity, out temp))
                                    {
                                        SupermanEntity pSuper = new SupermanEntity
                                        {
                                            Identity = Identity,
                                            Amount = (uint) m_nKoCount,
                                            Name = Name
                                        };
                                        temp = new IKoCount(pSuper);
                                    }
                                    else
                                    {
                                        if (m_nKoCount > temp.KoCount)
                                        {
                                            temp.KoCount = (uint) m_nKoCount;
                                            ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrSupermanBroadcast, Name, m_nKoCount, nPosition), Color.White,
                                                ChatTone.Talk);
                                        }
                                    }
                                    ServerKernel.Superman.TryAdd(Identity, temp);
                                }

                                m_nKoCount = 0;
                            }
                            else
                            {
                                Status.DelObj(stts.Identity);
                            }
                        }
                    }

                    Status.CheckDeadMark();
                }
            }
            catch
            {
                Program.WriteLog("ERROR USER STATUS");
            }

            if (!IsAlive && m_tDoGhost.IsTimeOut())
            {
                SetGhost();
                m_tDoGhost.Clear();
                return;
            }

            if (!IsAlive)
                return;

            try
            {
                if (m_tStamina.IsTimeOut() && m_tLastVigor.IsTimeOut() && Vigor < MaxVigor)
                {
                    Vigor += (uint) (MaxVigor * 0.005);
                }
            }
            catch
            {
                Program.WriteLog("VIGOR ERROR");
            }

            try
            {
                //if (QueryStatus(FlagInt.LUCKY_DIFFUSE) != null || QueryStatus(FlagInt.LUCKY_ABSORB) != null)
                //{
                //    // todo increase lucky timer
                //}

                if (QueryStatus(FlagInt.LUCKY_ABSORB) != null)
                {
                    IStatus status = QueryStatus(FlagInt.LUCKY_ABSORB);
                    if (status.CasterId != Identity)
                    {
                        Character caster = ServerKernel.UserManager.GetUser(status.CasterId);
                        if (caster == null || GetDistance(caster.MapX, caster.MapY) > 5 || caster.QueryStatus(FlagInt.LUCKY_DIFFUSE) == null)
                        {
                            DetachStatus(FlagInt.LUCKY_ABSORB);
                        }
                        else
                        {
                            if (Action != caster.Action)
                            {
                                Action = caster.Action;
                                Screen.Send(new MsgAction(Identity, (uint) caster.Action, MapX, MapY, GeneralActionType.ChangeAction), true);
                            }
                        }
                    }
                }
                else if (QueryStatus(FlagInt.LUCKY_DIFFUSE) == null 
                         && QueryStatus(FlagInt.LUCKY_ABSORB) == null 
                         && m_tCheckLuckyScreen.ToNextTime()
                         && Metempsychosis < 2)
                {
                    Character around = Map.CollectMapUser(5, new Point(MapX, MapY)).FirstOrDefault(x => x.QueryStatus(FlagInt.LUCKY_DIFFUSE) != null);
                    if (around != null)
                    {
                        AttachStatus(around, FlagInt.LUCKY_ABSORB, 0, int.MaxValue, 0, 0);
                    }
                }
            }
            catch
            {
                Program.WriteLog("LUCKY TIMER ERROR");
            }

            try
            {
                if (m_tMine.IsActive() && m_tMine.ToNextTime())
                    ProcessMineTimer();
            }
            catch
            {
                Program.WriteLog("MINE TIMER ERROR");
            }

            try
            {
                // xp points handle
                if (m_tXp.ToNextTime())
                    ProcXpVal();
            }
            catch
            {
                Program.WriteLog("ERRORR CHECK XP");
            }

            byte maxStamina = 180;
            if (!IsBlessed)
                maxStamina = 100;

            try
            {
                if (m_tStamina.ToNextTime() && Stamina < maxStamina && !IsWing)
                {
                    byte add = 0;
                    if (Action == EntityAction.Sit)
                    {
                        add = ADD_ENERGY_STAND * 2;
                    }
                    else if (Action == EntityAction.Lie)
                    {
                        add = ADD_ENERGY_STAND;
                    }
                    else
                    {
                        add = ADD_ENERGY_STAND / 3;
                    }

                    if (UserPackage[ItemPosition.Crop] != null)
                        add += 3;

                    if (add + Stamina > maxStamina)
                        add = (byte) (maxStamina - Stamina);
                    Stamina += add;
                }
            }
            catch
            {
                Program.WriteLog("ERROR STAMINA USER");
            }

            try
            {
                // automatic life recover
                if (m_lifeRecover.ToNextTime()
                    && IsAlive
                    && Life > 0
                    && Life < MaxLife)
                {
                    ushort add = (ushort) (MaxLife / 300);
                    if (add + Life > MaxLife)
                        add = (ushort) (MaxLife - Life);
                    Life += add;
                }
            }
            catch
            {
                Program.WriteLog("ERROR RECOVER LIFE");
            }

            try
            {
                // handle transformation timing
                if (m_tTransformation.IsActive() && m_tTransformation.IsTimeOut())
                    ClearTransformation();
            }
            catch
            {
                Program.WriteLog("ERROR TRANSFORMATION CHECK");
            }

            try
            {
                int nFightPause = m_nFightPause - m_dexterity;
                if (QueryStatus(FlagInt.FATAL_STRIKE) != null)
                    nFightPause /= 10;

                if (BattleSystem != null && BattleSystem.IsActive()
                                         && BattleSystem.NextAttack(nFightPause))
                    //&& (BattleSystem.QueryMagic() == null))
                {
                    BattleSystem.ProcessAttack();

                    if (QueryStatus(FlagInt.FATAL_STRIKE) != null)
                        BattleSystem.ResetBattle();
                }
            }
            catch
            {
                Program.WriteLog("AUTO ATTACK ERROR");
            }

            try
            {
                if (Magics.QueryMagic() != null)
                    Magics.OnTimer();
            }
            catch
            {
                Program.WriteLog("MAGIC TIMER ERROR");
            }
        }

        #endregion

        #region Socket

        public void OnDisconnect()
        {
            try
            {
                if ((!Map.IsRecordDisable() || Map.IsPrisionMap()) && IsAlive)
                {
                    SetRecordPos(MapIdentity, MapX, MapY);
                }

#if !DEBUG
            if (IsGm())
                SetRecordPos(5000, 50, 60);
#endif

                m_dbUser.LastLogout = (uint) UnixTimestamp.Now();

                new GameLoginRcdRepository().Save(new GameLoginRcdEntity
                {
                    UserIdentity = Identity,
                    AccountIdentity = Owner.AccountIdentity,
                    LoginTime = m_dbUser.LastLogin,
                    LogoutTime = m_dbUser.LastLogout,
                    OnlineTime = m_dbUser.LastLogout - m_dbUser.LastLogin,
                    ServerVersion = ServerKernel.Version
                });
                
                try
                {
                    Team?.LeaveTeam(this, null);

                    if (LotteryTemporaryItem != null)
                        AcceptLotteryPrize();

                    if (Booth?.Vending == true)
                        Booth.Destroy();

                    if (ServerKernel.ArenaQualifier.IsUserInsideMatch(Identity))
                        ServerKernel.ArenaQualifier.FindMatch(Identity).QuitMatch(Identity);
                    if (ServerKernel.ArenaQualifier.IsUserQueued(Identity))
                        ServerKernel.ArenaQualifier.Uninscribe(this);

                    if (KongFu.HasStarted)
                        KongFu.Logout();
                }
                catch (Exception ex)
                {
                    Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
                }

                Screen.RemoveFromObservers();
                LeaveMap();

                if (Life <= 0)
                    Life = 1;

                foreach (var friend in Friends.Values.Where(x => x.IsOnline))
                {
                    friend.User.Send(new MsgFriend(RelationAction.SET_OFFLINE_FRIEND, Identity, Name, false));
                }

                foreach (var enemy in Enemies.Values.Where(x => x.IsOnline))
                {
                    enemy.User.Send(new MsgFriend(RelationAction.SET_OFFLINE_ENEMY, Identity, Name, false));
                }

                if (m_bDeleted)
                {
                    Syndicate?.QuitSyndicate(this, true);

                    foreach (var friend in Friends.Values)
                        DeleteFriend(friend.Identity);
                    foreach (var enemy in Enemies.Values)
                        DeleteEnemy(enemy.Identity);

                    ServerKernel.Nobility.TryRemove(Identity, out DynaRankRecordsEntity rec);
                    if (rec != null)
                        new DynamicRankingRecordsRepository().Delete(rec);

                    UserManager.UserRepository.DeletePlayer(Identity);
                }
                else
                {
                    Save();
                }

                UserPackage.SaveAllInfo();
                Statistics.SaveAll();
            }
            catch (Exception ex)
            {
                Program.WriteLog($"Error when disconnecting user: {Identity}:{Name}", LogType.ERROR);
                Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
            }
            finally
            {
                ServerKernel.LoginServer.Send(new MsgLoginUserInfo
                {
                    AccountIdentity = Owner.AccountIdentity,
                    UserIdentity = Identity,
                    Mode = LoginUserInfoMode.Disconnect
                });

                Owner.UserState = PlayerState.Disconnected;
            }
        }

        public void SendMessage(string msg, ChatTone tone = ChatTone.TopLeft)
        {
            m_pOwner?.Send(new MsgTalk(msg, tone));
        }

        public void SendMessage(string msg, Color color, ChatTone tone)
        {
            m_pOwner?.Send(new MsgTalk(msg, tone, color));
        }

        public void SendSysMessage(string msg)
        {
            SendSysMessage(msg, Color.Red);
        }

        public void SendSysMessage(string msg, Color color)
        {
            m_pOwner?.Send(new MsgTalk(msg, ChatTone.TopLeft, color));
        }

        public override int Send(byte[] msg)
        {
            return m_pOwner?.Send(msg) ?? -1;
        }

        public void Disconnect()
        {
            m_pOwner?.Disconnect();
        }

        public void SendWindowSpawnTo(Character target)
        {
            m_packet.WindowSpawn = true;
            m_packet.TotemBattlePower = TotemBattlePower;
            m_packet.FamilyBattlePower = FamilyBattlePower;
            m_packet.FateBattlePower = 0;
            m_packet.TutorBattlePower = 0;
            target.Send(m_packet);
        }

        public override void SendSpawnTo(Character target)
        {
            if (IsArenaWitness)
                return;

            try
            {
                Syndicate?.SendName(target);
                Syndicate?.SendName(target);
            }
            catch
            {

            }

            m_packet.WindowSpawn = false;
            m_packet.TotemBattlePower = TotemBattlePower;
            m_packet.FamilyBattlePower = FamilyBattlePower;
            m_packet.FateBattlePower = 0;
            m_packet.TutorBattlePower = 0;
            target.Send(m_packet);

            try
            {
                Syndicate?.SendName(target);
                Syndicate?.SendName(target);
            }
            catch
            {

            }
            
            if (KongFu.HasStarted && KongFu.IsJiangActive)
                KongFu.Send(KongfuBaseMode.UpdateTalent, target);

            if (Booth?.Vending == true)
            {
                Booth.SendHawkMessage(target);
            }
        }

        public override void ExchangeSpawnPackets(Character target)
        {
            target.SendSpawnTo(this);

            if (!IsArenaWitness)
                SendSpawnTo(target);
        }

        public override bool CheckCrime(Role pRole)
        {
            if (pRole == null) return false;
            if (!pRole.IsEvil() && !pRole.IsMonster())
            {
                if (!Map.IsTrainingMap() && !Map.IsDeadIsland() && !Map.IsDeadIsland() && !Map.IsPrisionMap() &&
                    !Map.IsFamilyMap() && !Map.IsPkGameMap() && !Map.IsPkField())
                {
                    if (pRole is Character targetUser && PkMode == PkModeType.JiangHu && targetUser.JiangHuActive)
                    {
                        return false;
                    }
                    SetCrimeStatus(25);
                }
                return true;
            }

            if (pRole.IsMonster() && pRole.IsGuard())
            {
                SetCrimeStatus(25);
                return true;
            }

            return false;
        }

        #endregion

        #region Database

        public void SetDailyUpdate()
        {
            m_dbUser.LastUpdate = uint.Parse(DateTime.Now.ToString("yyyyMMdd"));
            Save();
        }

        public bool Save()
        {
            return m_dbUser != null && UserManager.UserRepository.Save(m_dbUser);
        }

        /// <summary>
        /// Should not be called unless really wants to delete from database.
        /// </summary>
        public bool Delete()
        {
            if (SyndicatePosition == SyndicateRank.GuildLeader)
                return false;

            m_bDeleted = true;

            ServerKernel.DeletedCharacters.TryAdd(Identity, m_dbUser);
            //Disconnect();
            return true;
        }

        #endregion
    }
}