﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - BaseNpc.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common.Enums;
using GameServer.World;

#endregion

namespace GameServer.Structures.Entities
{
    public abstract class BaseNpc : Role
    {
        #region Constants

        public const int
            NPC_NONE = 0, // Í¨ÓÃNPC
            SHOPKEEPER_NPC = 1, // ÉÌµêNPC
            TASK_NPC = 2, // ÈÎÎñNPC(ÒÑ×÷·Ï£¬½öÓÃÓÚ¼æÈÝ¾ÉÊý¾Ý)
            STORAGE_NPC = 3, // ¼Ä´æ´¦NPC
            TRUNCK_NPC = 4, // Ïä×ÓNPC
            FACE_NPC = 5, // ±äÍ·ÏñNPC
            FORGE_NPC = 6, // ¶ÍÔìNPC		(only use for client)
            EMBED_NPC = 7, // ÏâÇ¶NPC
            STATUARY_NPC = 9, // µñÏñNPC
            SYNFLAG_NPC = 10, // °ïÅÉ±ê¼ÇNPC
            ROLE_PLAYER = 11, // ÆäËûÍæ¼Ò		(only use for client)
            ROLE_HERO = 12, // ×Ô¼º			(only use for client)
            ROLE_MONSTER = 13, // ¹ÖÎï			(only use for client)
            BOOTH_NPC = 14, // °ÚÌ¯NPC		(CBooth class)
            SYNTRANS_NPC = 15, // °ïÅÉ´«ËÍNPC, ¹Ì¶¨ÄÇ¸ö²»ÒªÓÃ´ËÀàÐÍ! (ÓÃÓÚ00:00ÊÕ·Ñ)(LINKIDÎª¹Ì¶¨NPCµÄID£¬ÓëÆäËüÊ¹ÓÃLINKIDµÄ»¥³â)
            ROLE_BOOTH_FLAG_NPC = 16, // Ì¯Î»±êÖ¾NPC	(only use for client)
            ROLE_MOUSE_NPC = 17, // Êó±êÉÏµÄNPC	(only use for client)
            ROLE_MAGICITEM = 18, // ÏÝÚå»ðÇ½		(only use for client)
            ROLE_DICE_NPC = 19, // ÷»×ÓNPC
            ROLE_SHELF_NPC = 20, // ÎïÆ·¼Ü
            WEAPONGOAL_NPC = 21, // ÎäÆ÷°Ð×ÓNPC
            MAGICGOAL_NPC = 22, // Ä§·¨°Ð×ÓNPC
            BOWGOAL_NPC = 23, // ¹­¼ý°Ð×ÓNPC
            ROLE_TARGET_NPC = 24, // °¤´ò£¬²»´¥·¢ÈÎÎñ	(only use for client)
            ROLE_FURNITURE_NPC = 25, // ¼Ò¾ßNPC	(only use for client)
            ROLE_CITY_GATE_NPC = 26, // ³ÇÃÅNPC	(only use for client)
            ROLE_NEIGHBOR_DOOR = 27, // ÁÚ¾ÓµÄÃÅ
            ROLE_CALL_PET = 28, // ÕÙ»½ÊÞ	(only use for client)
            EUDEMON_TRAINPLACE_NPC = 29, // »ÃÊÞÑ±ÑøËù
            AUCTION_NPC = 30, // ÅÄÂòNPC	ÎïÆ·ÁìÈ¡NPC  LW
            ROLE_MINE_NPC = 31, // ¿óÊ¯NPC		
            ROLE_CTFBASE_NPC = 40,
            ROLE_3DFURNITURE_NPC = 101, // 3D¼Ò¾ßNPC 
            SYN_NPC_WARLETTER = 110; //Ôö¼ÓÐÂµÄ£Î£Ð£ÃÀàÐÍ¡¡×¨ÃÅÓÃÀ´¡¡ÏÂÕ½ÊéµÄ¡¡°ïÅÉ£Î£Ð£Ã

        #endregion

        protected BaseNpc(uint idObj)
            : base(idObj)
        {
            Task0 = 0;
            Task1 = 0;
            Task2 = 0;
            Task3 = 0;
            Task4 = 0;
            Task5 = 0;
            Task6 = 0;
            Task7 = 0;
        }

        public virtual ushort Type { get; }

        public virtual uint OwnerType { get; set; }

        public bool ChangePos(uint idMap, ushort x, ushort y)
        {
            if (ServerKernel.Maps.TryGetValue(idMap, out Map map))
            {
                if (!map.IsStandEnable(x, y))
                    return false;

                LeaveMap();
                m_idMap = idMap;
                m_usMapX = x;
                m_usMapY = y;
                EnterMap();
                return true;
            }
            return false;
        }

        #region Npc Task and Data

        public virtual int GetData(string szAttr)
        {
            switch (szAttr.ToLower())
            {
                case "data0": return Data0;
                case "data1": return Data1;
                case "data2": return Data2;
                case "data3": return Data3;
            }
            return 0;
        }

        public virtual void SetData(string szAttr, int value)
        {
            switch (szAttr.ToLower())
            {
                case "data0": Data0 = value; break;
                case "data1": Data1 = value; break;
                case "data2": Data2 = value; break;
                case "data3": Data3 = value; break;
            }
        }

        public virtual uint OwnerIdentity { get; set; }
        public virtual bool Vending { get; set; }
        public virtual uint Task0 { get; }
        public virtual uint Task1 { get; }
        public virtual uint Task2 { get; }
        public virtual uint Task3 { get; }
        public virtual uint Task4 { get; }
        public virtual uint Task5 { get; }
        public virtual uint Task6 { get; }
        public virtual uint Task7 { get; }

        public virtual int Data0 { get; set; }
        public virtual int Data1 { get; set; }
        public virtual int Data2 { get; set; }
        public virtual int Data3 { get; set; }

        #endregion

        #region Common Checks

        public bool IsShopNpc()
        {
            return Type == SHOPKEEPER_NPC;
        }

        public bool IsTaskNpc()
        {
            return Type == TASK_NPC;
        }

        public bool IsStorageNpc()
        {
            return Type == STORAGE_NPC;
        }

        public override bool IsDynaNpc()
        {
            return m_idObj >= IdentityRange.DYNANPCID_FIRST && m_idObj < IdentityRange.DYNANPCID_LAST;
        }

        public bool IsUserNpc()
        {
            return OwnerType == 1;
        }

        public bool IsSynNpc()
        {
            return OwnerType == 2;
        }

        public bool IsSynFlag()
        {
            return (Type == SYNFLAG_NPC || Type == ROLE_CTFBASE_NPC) && IsSynNpc();
        }

        public bool IsCtfFlag()
        {
            return IsSynFlag() && Type == ROLE_CTFBASE_NPC;
        }

        #endregion

        public virtual bool DelNpc()
        {
            return false;
        }
    }
}