﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Automatic Events.cs
// Last Edit: 2019/11/24 19:03
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Concurrent;
using System.Timers;
using FtwCore.Common;
using FtwCore.Database.Entities;
using GameServer.Threads;

#endregion

namespace GameServer.Structures.Actions
{
    public class AutomaticEvents
    {
        private Timer _timer;

        private const int _ACTION_SYSTEM_EVENT = 2000000;
        private const int _ACTION_SYSTEM_EVENT_LIMIT = 100;
        private readonly ConcurrentDictionary<uint, GameActionEntity> m_dicActions;
        private GameAction m_pGameAction = new GameAction();

        public AutomaticEvents()
        {
            m_dicActions = new ConcurrentDictionary<uint, GameActionEntity>(1, _ACTION_SYSTEM_EVENT_LIMIT);
            for (int a = 0; a < _ACTION_SYSTEM_EVENT_LIMIT; a++)
            {
                if (ServerKernel.Actions.TryGetValue((uint) (_ACTION_SYSTEM_EVENT + a), out var action))
                    m_dicActions.TryAdd(action.Identity, action);
            }
        }

        public void RefreshEvents()
        {
            m_dicActions.Clear();
            for (int a = 0; a < _ACTION_SYSTEM_EVENT_LIMIT; a++)
            {
                if (ServerKernel.Actions.TryGetValue((uint) (_ACTION_SYSTEM_EVENT + a), out var action))
                    m_dicActions.TryAdd(action.Identity, action);
            }
        }

        public void StartCheck()
        {
            _timer = new Timer();
            _timer.Elapsed += Execute;
            _timer.Interval = CalculateInterval();
            _timer.Start();

            ThreadManager.RunningThreads |= ServerThreads.GameActionsThread;
        }

        private void Execute(object sender, ElapsedEventArgs e)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (now.Hour == 0 && now.Minute == 0)
                    DailyUpdate.Do();
            }
            catch (Exception ex)
            {
                Program.WriteLog("Could not make daily update", LogType.ERROR);
                Program.WriteLog(ex.ToString(), LogType.EXCEPTION, false);
            }

            try
            {
                foreach (var action in m_dicActions.Values)
                {
                    if (m_pGameAction == null)
                        m_pGameAction = new GameAction();
                    m_pGameAction.ProcessAction(action.Identity, null, null, null, null);
                }

                RefreshEvents();
            }
            catch (Exception ex)
            {
                Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
            }
            finally
            {
                if (ThreadManager.StopThreading)
                {
                    Stop();
                }
                else 
                {
                    _timer.Interval = CalculateInterval(); //every 00 of every minute
                }
            }
        }

        private int CalculateInterval()
        {
            DateTime now = DateTime.Now;

            DateTime future = now.AddSeconds(60 - now.Second % 60).AddMilliseconds(now.Millisecond * -1);
            TimeSpan interval0 = future - now;
            return (int) interval0.TotalMilliseconds;
        }

        public bool IsRunning => _timer.Enabled;

        public void Stop()
        {
            _timer.Stop();
            _timer.Dispose();
            ThreadManager.SetThreadEnded(ServerThreads.GameActionsThread);
        }
    }
}