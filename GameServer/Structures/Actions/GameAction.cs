﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - GameAction.cs
// Last Edit: 2019/12/07 23:49
// Created: 2019/12/07 18:33
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Networking.GameServer;
using GameServer.Structures.Entities;
using GameServer.Structures.Groups.Families;
using GameServer.Structures.Groups.Syndicates;
using GameServer.Structures.Items;
using GameServer.World;

#endregion

namespace GameServer.Structures.Actions
{
    public sealed class GameAction
    {
        private Item m_pItem;

        private List<MsgTaskDialog> m_pReplies = new List<MsgTaskDialog>();
        private ScreenObject m_pRole;
        private Character m_pUser;

        public GameAction()
        {
        }

        public GameAction(ScreenObject role)
        {
            if (role is Character character)
                m_pUser = character;
            else
                m_pRole = role;
        }

        public bool ProcessAction(uint idAction, Character pUser, ScreenObject role, Item item, string szAccept)
        {
            m_pReplies = new List<MsgTaskDialog>();
            if (idAction == 0)
                return false;

            m_pUser = pUser;
            m_pRole = role;
            m_pItem = item;

            if (m_pUser != null)
                m_pUser.InteractingNpc = m_pRole;

            try
            {
                const int _MAX_ACTION = 32;
                int nActionCount = 0;

                while (idAction != 0)
                {
                    if (nActionCount > _MAX_ACTION)
                    {
                        Program.WriteLog($"Error: too many game action, last action: {idAction}");
                        return false;
                    }

                    if (idAction == 0)
                        return false;

                    if (!ServerKernel.Actions.TryGetValue(idAction, out GameActionEntity action))
                    {
                        Program.WriteLog($"Error: game action {idAction} not found");
                        return false;
                    }

                    string param = ReplaceAttrStr(action.Param, pUser, role, item, szAccept);

                    bool bRet = false;

                    if (m_pUser != null && m_pUser.IsPm())
                    {
                        m_pUser.SendMessage(
                            $"{action.Identity}: [{action.IdNext},{action.IdNextfail}]. type[{action.Type}], data[{action.Data}], param:[{param}].",
                            ChatTone.System);
                    }

                    TaskActionType type = (TaskActionType) action.Type;
                    if (type > TaskActionType.ActionSysFirst && type < TaskActionType.ActionSysLimit)
                        bRet = ProcessActionSys(action, param, pUser, role, item, szAccept);
                    else if (type > TaskActionType.ActionNpcFirst && type < TaskActionType.ActionNpcLimit)
                        bRet = ProcessActionNpc(action, param, pUser, role, item, szAccept);
                    else if (type > TaskActionType.ActionMapFirst && type < TaskActionType.ActionMapLimit)
                        bRet = ProcessActionMap(action, param, pUser, role, item, szAccept);
                    else if (type > TaskActionType.ActionItemonlyFirst && type < TaskActionType.ActionItemLimit)
                        bRet = ProcessActionItem(action, param, pUser, role, item, szAccept);
                    else if (type > TaskActionType.ActionSynFirst && type < TaskActionType.ActionSynLimit)
                        bRet = ProcessActionSyndicate(action, param, pUser, role, item, szAccept);
                    else if (type > TaskActionType.ActionMstFirst && type < TaskActionType.ActionMstLimit)
                        bRet = ProcessActionMonster(action, param, pUser, role, item, szAccept);
                    else if (type > TaskActionType.ActionFamilyFirst && type < TaskActionType.ActionFamilyLimit)
                        bRet = ProcessActionFamily(action, param, pUser, role, item, szAccept);
                    else if (type > TaskActionType.ActionUserFirst && type < TaskActionType.ActionUserLimit)
                        bRet = ProcessActionUser(action, param, pUser, role, item, szAccept);
                    else if (type > TaskActionType.ActionEventFirst && type < TaskActionType.ActionEventLimit)
                        bRet = ProcessEventAction(action, param, pUser, role, item, szAccept);
                    else if (type > TaskActionType.ActionTrapFirst && type < TaskActionType.ActionTrapLimit)
                        bRet = ProcessTrapAction(action, param, pUser, role, item, szAccept);

                    idAction = bRet ? action.IdNext : action.IdNextfail;
                    nActionCount++;
                }
            }
            catch (Exception ex)
            {
                Program.WriteLog($"Error while running action id: {idAction}");
                Program.WriteLog(ex.ToString());
            }

            return false;
        }

        private bool ProcessActionSys(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            switch ((TaskActionType) action.Type)
            {
                case TaskActionType.ActionMenutext: return MenuText101(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionMenulink: return MenuLink102(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionMenuedit: return MenuEdit103(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionMenupic: return MenuPic104(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionMenucreate: return MenuCreate120(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionRand: return MenuRand121(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionRandaction:
                    return MenuRandAction122(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionChktime: return MenuChkTime123(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionPostcmd:
                    return MenuActionPostCmd124(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionBrocastmsg:
                    return MenuBroadcastMsg125(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionMessagebox:
                    return MenuActionMessageBox126(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionExecutequery:
                    return MenuActionExecuteQuery127(action, param, user, role, item, pszAccept);
            }
#if DEBUG
            Program.WriteLog($"Action {action.Identity} unhandled type {action.Type}", LogType.WARNING);
#else
            ServerKernel.Log.SaveLog($"Action {action.Identity} unhandled  type {action.Type}", LogType.WARNING);
#endif
            return false;
        }

        private bool ProcessActionNpc(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            switch ((TaskActionType) action.Type)
            {
                case TaskActionType.ActionNpcAttr: return ActionNpcAttr201(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionNpcErase:
                    return ActionNpcErase205(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionNpcResetsynowner:
                    return ActionNpcResetSynOwner207(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionNpcFindNextTable:
                    return ActionNpcFindNextTable208(action, param, user, role, item, pszAccept);
            }
#if DEBUG
            Program.WriteLog($"Action {action.Identity} unhandled type {action.Type}", LogType.WARNING);
#else
            ServerKernel.Log.SaveLog($"Action {action.Identity} unhandled  type {action.Type}", LogType.WARNING);
#endif
            return false;
        }

        private bool ProcessActionMap(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            switch ((TaskActionType) action.Type)
            {
                case TaskActionType.ActionMapMovenpc:
                    return ActionMapMoveNpc301(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionMapMapuser:
                    return ActionMapMapUser302(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionMapBrocastmsg:
                    return ActionMapBroadcastMsg303(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionMapDropitem:
                    return ActionMapDropItem304(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionMapSetstatus:
                    return ActionMapSetStatus305(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionMapAttrib:
                    return ActionMapAttribute306(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionMapRegionMonster:
                    return ActionMapRegionMonster307(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionMapChangeweather:
                    return ActionMapChangeWeather310(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionMapChangelight:
                    return ActionMapChangeLight311(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionMapMapeffect:
                    return ActionMapEffect312(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionMapFireworks:
                    return ActionMapFireworks314(action, param, user, role, item, pszAccept);
            }
#if DEBUG
            Program.WriteLog($"Action {action.Identity} unhandled type {action.Type}", LogType.WARNING);
#else
            ServerKernel.Log.SaveLog($"Action {action.Identity} unhandled  type {action.Type}", LogType.WARNING);
#endif
            return false;
        }

        private bool ProcessActionItem(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            switch ((TaskActionType) action.Type)
            {
                case TaskActionType.ActionItemAdd: return ActionItemAdd501(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionItemDel: return ActionItemDel502(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionItemCheck:
                    return ActionItemCheck503(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionItemHole:
                    return ActionItemHole504(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionItemMultidel:
                    return ActionItemMultiDel506(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionItemMultichk:
                    return ActionItemMultiChk507(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionItemLeavespace:
                    return ActionItemLeaveSpace508(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionItemUpequipment:
                    return ActionItemUpEquipment509(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionItemEquiptest:
                    return ActionItemEquipTest510(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionItemEquipexist:
                    return ActionItemEquipExist511(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionItemEquipcolor:
                    return ActionItemEquipColor512(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionItemRemoveAny:
                    return ActionItemRemoveAny513(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionItemCheckrand:
                    return ActionItemCheckRand516(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionItemModify:
                    return ActionItemModify517(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionItemJarCreate:
                    return ActionItemJarCreate528(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionItemJarVerify:
                    return ActionItemJarVerify529(action, param, user, role, item, pszAccept);
            }

#if DEBUG
            Program.WriteLog($"Action {action.Identity} unhandled type {action.Type}", LogType.WARNING);
#else
            ServerKernel.Log.SaveLog($"Action {action.Identity} unhandled  type {action.Type}", LogType.WARNING);
#endif
            return false;
        }

        private bool ProcessActionSyndicate(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            switch ((TaskActionType) action.Type)
            {
                case TaskActionType.ActionSynCreate:
                    return ActionSynCreate701(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionSynDestroy:
                    return ActionSynDestroy702(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionSynChangeLeader:
                    return ActionSynChangeLeader709(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionSynAttr: return ActionSynAttr717(action, param, user, role, item, pszAccept);
            }
#if DEBUG
            Program.WriteLog($"Action {action.Identity} unhandled type {action.Type}", LogType.WARNING);
#else
            ServerKernel.Log.SaveLog($"Action {action.Identity} unhandled  type {action.Type}", LogType.WARNING);
#endif
            return false;
        }

        private bool ProcessActionMonster(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            switch ((TaskActionType) action.Type)
            {
                case TaskActionType.ActionMstDropitem:
                    return ActionMstDropItem801(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionMstRefinery:
                    return ActionMstRefinery803(action, param, user, role, item, pszAccept);
            }
#if DEBUG
            Program.WriteLog($"Action {action.Identity} unhandled type {action.Type}", LogType.WARNING);
#else
            ServerKernel.Log.SaveLog($"Action {action.Identity} unhandled  type {action.Type}", LogType.WARNING);
#endif
            return false;
        }

        private bool ProcessActionFamily(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            switch ((TaskActionType) action.Type)
            {
                case TaskActionType.ActionFamilyCreate:
                    return ActionFamilyCreate901(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionFamilyDestroy:
                    return ActionFamilyDestroy902(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionFamilyAttr:
                    return ActionFamilyAttr917(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionFamilyUplev:
                    return ActionFamilyUpLev918(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionFamilyBpuplev:
                    return ActionFamilyBpUpLev919(action, param, user, role, item, pszAccept);
            }
#if DEBUG
            Program.WriteLog($"Action {action.Identity} unhandled type {action.Type}", LogType.WARNING);
#else
            ServerKernel.Log.SaveLog($"Action {action.Identity} unhandled  type {action.Type}", LogType.WARNING);
#endif
            return false;
        }

        private bool ProcessActionUser(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            switch ((TaskActionType) action.Type)
            {
                case TaskActionType.ActionUserAttr: return UserAttr1001(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserFull: return UserFillAttr1002(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserChgmap: return UserChgmap1003(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserRecordpoint:
                    return UserSaveLocation1004(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserHair:
                    return ActionUserHair1005(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserChgmaprecord:
                    return UserChgmapRecord1006(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserTransform:
                    return UserTransform1008(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserIspure:
                    return UserPureClass1009(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserTalk: return UserTalk1010(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserMagic: return UserMagic1020(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserWeaponskill:
                    return UserWeaponSkill1021(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserLog: return ActionUserLog1022(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserBonus:
                    return ActionUserBonus1023(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserDivorce:
                    return ActionUserDivorce1024(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserMarriage:
                    return ActionUserMarriage1025(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserSex: return ActionUserSex1026(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserEffect:
                    return ActionUserEffect1027(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserMediaplay:
                    return ActionUserMediaPlay1029(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserAddTitle:
                    return ActionUserAddTitle1031(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserRemoveTitle:
                    return ActionUserRemoveTitle1032(action, param, user, role, item, pszAccept);
                case TaskActionType.ACTION_USER_CREATEMAP:
                    return ActionUserCreateMap1033(action, param, user, role, item, pszAccept);
                case TaskActionType.ACTION_USER_ENTER_HOME:
                    return ActionUserEnterHome1034(action, param, user, role, item, pszAccept);
                case TaskActionType.ACTION_USER_ENTER_MATE_HOME:
                    return ActionUserEnterMateHome1035(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserRebirth:
                    return ActionUserRebirth1040(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserWebpage:
                    return ActionUserWebPage1041(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserBbs: return ActionUserBbs1042(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserUnlearnSkill: return true;
                case TaskActionType.ActionUserFixAttr:
                    return ActionUserFixAttr1045(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserOpenDialog:
                    return ActionUserOpenDialog1046(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserPointAllot:
                    return ActionUserPointAllot1047(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserExpMultiply:
                    return ActionUserExpMultiply1048(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserWhPassword:
                    return ActionUserWhPassword1052(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserSetWhPassword:
                    return ActionUserSetWhPassword1053(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserOpeninterface:
                    return ActionUserOpenInterface1054(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserVarCompare:
                    return ActionUserVarCompare1060(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserVarDefine:
                    return ActionUserVarDefine1061(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserVarCalc:
                    return ActionUserVarCalc1064(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserStcCompare:
                    return ActionUserStcCompare1073(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserStcOpe:
                    return ActionUserStrOpe1074(action, param, user, role, item, pszAccept);
                case (TaskActionType) 1075: return true;
                case TaskActionType.ActionUserTaskManager:
                    return ActionUserTaskManager1080(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserTaskOpe:
                    return ActionUserTaskOpe1081(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserAttachStatus:
                    return ActionUserAttachStatus1082(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserGodTime:
                    return ActionUserGodTime1083(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserExpballExp:
                    return ActionUserExpBallExp1086(action, param, user, role, item, pszAccept);
                case (TaskActionType) 1095: return true;
                case TaskActionType.ActionUserStatusCreate:
                    return ActionUserStatusCreate1096(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionUserStatusCheck:
                    return ActionUserStatusCheck1098(action, param, user, role, item, pszAccept);

                case TaskActionType.ActionTeamAttr:
                    return ActionTeamAttr1102(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionTeamBroadcast:
                    return ActionTeamBroadcast1101(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionTeamLeavespace:
                    return ActionTeamLeaveSpace1103(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionTeamItemAdd:
                    return ActionTeamItemAdd1104(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionTeamItemDel:
                    return ActionTeamItemDel1105(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionTeamItemCheck:
                    return ActionTeamItemCheck1106(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionTeamChgmap:
                    return ActionTeamChgMap1107(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionTeamChkIsleader:
                    return ActipmTeamChkIsLeader1108(action, param, user, role, item, pszAccept);

                case TaskActionType.ActionGeneralLottery:
                    return ActionSpecialLottery1508(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionGeneraSubclassManagement:
                    return ActionGeneralSubclassManagement1509(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionGeneralSkillLineEnabled:
                    return ActionGeneralSkillLineEnabled1510(action, param, user, role, item, pszAccept);
            }

#if DEBUG
            Program.WriteLog($"Action {action.Identity} unhandled type {action.Type}", LogType.WARNING);
#else
            ServerKernel.Log.SaveLog($"Action {action.Identity} unhandled  type {action.Type}", LogType.WARNING);
#endif
            return false;
        }

        private bool ProcessEventAction(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            switch ((TaskActionType) action.Type)
            {
                case TaskActionType.ActionEventSetstatus:
                    return ActionEventSetStatus2001(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionEventDelnpcGenid:
                    return ActionEventDelNpcGenId2002(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionEventCompare:
                    return ActionEventCompare2003(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionEventCompareUnsigned:
                    return ActionEventCompareUnsigned2004(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionEventCreatepet:
                    return ActionEventCreatePet2006(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionEventCreatenewNpc:
                    return ActionEventCreateNewNpc2007(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionEventCountmonster:
                    return ActionEventCountMonster2008(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionEventDeletemonster:
                    return ActionEventDeleteMonster2009(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionEventBbs:
                    return ActionEventBbs2010(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionEventErase:
                    return ActionEventErase2011(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionEventTeleport:
                    return ActionEventTeleport2012(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionEventMassaction:
                    return ActionEventMassAction2013(action, param, user, role, item, pszAccept);
            }
#if DEBUG
            Program.WriteLog($"Action {action.Identity} unhandled type {action.Type}", LogType.WARNING);
#else
            ServerKernel.Log.SaveLog($"Action {action.Identity} unhandled  type {action.Type}", LogType.WARNING);
#endif
            return false;
        }

        private bool ProcessTrapAction(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            switch ((TaskActionType) action.Type)
            {
                case TaskActionType.ActionTrapCreate:
                    return ActionTrapCreate2101(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionTrapErase:
                    return ActionTrapErase2102(action, param, user, role, item, pszAccept);
                case TaskActionType.ActionTrapCount:
                    return ActionTrapCount2103(action, param, user, role, item, pszAccept);
            }
#if DEBUG
            Program.WriteLog($"Action {action.Identity} unhandled type {action.Type}", LogType.WARNING);
#else
            ServerKernel.Log.SaveLog($"Action {action.Identity} unhandled  type {action.Type}", LogType.WARNING);
#endif
            return false;
        }

        private string[] GetSafeParam(string param, int limit = 0)
        {
            if (limit == 0)
                return param.Split(new[] {" "}, StringSplitOptions.RemoveEmptyEntries);
            return param.Split(new[] {" "}, limit, StringSplitOptions.RemoveEmptyEntries);
        }

        private string GetParenthesys(string szParam)
        {
            int varIdx = szParam.IndexOf("(", StringComparison.CurrentCulture) + 1;
            int endIdx = szParam.IndexOf(")", StringComparison.CurrentCulture);
            return szParam.Substring(varIdx, endIdx - varIdx);
        }

        private byte VarId(string szParam)
        {
            int varIdx = szParam.IndexOf("(", StringComparison.CurrentCulture) + 1;
            return byte.Parse(szParam.Substring(varIdx, 1));
        }

        public string ReplaceAttrStr(string str, Character user, ScreenObject role, Item item, string pszAccept)
        {
            str = str.Replace("%server_time", DateTime.Now.ToString("R"));

            if (user != null)
            {
                str = str.Replace("%user_id", user.Identity.ToString())
                    .Replace("%user_name", user.Name)
                    .Replace("%user_mate", user.Mate)
                    .Replace("%user_lev", user.Level.ToString())
                    .Replace("%user_pro", ((int) user.Profession).ToString())
                    .Replace("%account_id", user.Owner.AccountIdentity.ToString())
                    .Replace("%user_map_name", user.Map.Name)
                    .Replace("%user_map_id", user.MapIdentity.ToString())
                    .Replace("%user_map_x", user.MapX.ToString())
                    .Replace("%user_map_y", user.MapY.ToString())
                    .Replace("%user_nobility_rank", ((int) user.NobilityRank).ToString())
                    .Replace("%user_nobility_position", user.Nobility.Position.ToString())
                    .Replace("%syn_id", user.SyndicateIdentity.ToString())
                    .Replace("%syn_name", user.SyndicateName)
                    .Replace("%syn_leadername", user.SyndicateLeaderName)
                    .Replace("%syn_leaderid", user.SyndicateLeaderIdentity.ToString())
                    .Replace("%syn_membernum", (user.Syndicate?.MemberCount ?? 0).ToString())
                    .Replace("%syn_allynum", (user.Syndicate?.Allies.Count ?? 0).ToString())
                    .Replace("%syn_enemynum", (user.Syndicate?.Enemies.Count ?? 0).ToString())
                    .Replace("%user_home_id", user.HomeIdentity.ToString());

                str = str.Replace("%map_owner_id", user.Map.OwnerIdentity.ToString());

                if (str.Contains("%levelup_exp"))
                {
                    LevelExperienceEntity db =
                        ServerKernel.LevelExperience.Values.FirstOrDefault(x => x.Level == m_pUser.Level);
                    if (db != null)
                        str = str.Replace("%levelup_exp", db.Exp.ToString());
                    else
                        str = str.Replace("%levelup_exp", "0");
                }

                for (int i = 0; i < user.Data.Length; i++)
                {
                    str = str.Replace($"%iter_var_data{i}", user.Data[i].ToString());
                    str = str.Replace($"%iter_var_str{i}", user.DataString[i]);
                }

                if (str.Contains("%iter_upquality_gem"))
                {
                    Item pItem = user.UserPackage[(ItemPosition) user.Iterator];
                    if (pItem != null)
                        str = str.Replace("%iter_upquality_gem", pItem.GetUpQualityGemAmount().ToString());
                    else
                        str = str.Replace("%iter_upquality_gem", "0");
                }

                if (str.Contains("%iter_itembound"))
                {
                    Item pItem = user.UserPackage[(ItemPosition) user.Iterator];
                    if (pItem != null)
                        str = str.Replace("%iter_itembound", pItem.IsBound ? "1" : "0");
                    else
                        str = str.Replace("%iter_itembound", "0");
                }

                if (str.Contains("%iter_uplevel_gem"))
                {
                    Item pItem = user.UserPackage[(ItemPosition) user.Iterator];
                    if (pItem != null)
                        str = str.Replace("%iter_uplevel_gem", pItem.GetUpgradeGemAmount().ToString());
                    else
                        str = str.Replace("%iter_uplevel_gem", "0");
                }
            }
            else
            {
                str = str.Replace("%user_id", "0")
                    .Replace("%user_name", "")
                    .Replace("%user_mate", "")
                    .Replace("%user_lev", "0")
                    .Replace("%user_pro", "0")
                    .Replace("%account_id", "0")
                    .Replace("%user_map_name", "")
                    .Replace("%user_map_id", "0")
                    .Replace("%user_map_x", "0")
                    .Replace("%user_map_y", "0")
                    .Replace("%user_nobility_rank", "0")
                    .Replace("%user_nobility_position", "0")
                    .Replace("%syn_id", "0")
                    .Replace("%syn_name", "")
                    .Replace("%syn_leadername", "")
                    .Replace("%syn_leaderid", "0")
                    .Replace("%syn_membernum", "0")
                    .Replace("%syn_allynum", "0")
                    .Replace("%syn_enemynum", "0")
                    .Replace("%iter_upquality_gem", "0")
                    .Replace("%iter_itembound", "0")
                    .Replace("%iter_uplevel_gem", "0")
                    .Replace("%iter_var_data0", "0")
                    .Replace("%user_home_id", "0");
            }

            if (role != null)
            {
                if (role is BaseNpc npc)
                {
                    str = str.Replace("%data0", npc.Data0.ToString())
                        .Replace("%data1", npc.Data1.ToString())
                        .Replace("%data2", npc.Data2.ToString())
                        .Replace("%data3", npc.Data3.ToString())
                        .Replace("%npc_ownerid", npc.OwnerIdentity.ToString());
                }
                else
                {
                    str = str.Replace("%data0", "0")
                        .Replace("%data1", "0")
                        .Replace("%data2", "0")
                        .Replace("%data3", "0")
                        .Replace("%npc_ownerid", "0");
                }

                str = str.Replace("%map_owner_id", role.Map.OwnerIdentity.ToString());
            }
            else
            {
                str = str.Replace("%data0", "0")
                    .Replace("%data1", "0")
                    .Replace("%data2", "0")
                    .Replace("%data3", "0")
                    .Replace("%npc_ownerid", "0");
            }

            str = str.Replace("%family_id", user?.FamilyIdentity.ToString())
                .Replace("%family_name", user?.FamilyName)
                .Replace("%family_leadername", user?.Family?.LeaderName ?? Language.StrNone)
                .Replace("%family_level", user?.Family?.Level.ToString() ?? "0")
                .Replace("%family_bp_lev", user?.Family?.BpTower.ToString() ?? "0")
                .Replace("%family_funds", user?.Family?.MoneyFunds.ToString() ?? "0")
                .Replace("%family_bp_perc", user?.Family?.SharedPercent.ToString() ?? "0");

            str = str.Replace("%item_data", item?.Identity.ToString() ?? "0")
                .Replace("%item_name", item?.Name ?? Language.StrNone)
                .Replace("%item_type", item?.Type.ToString() ?? "0")
                .Replace("%item_time", item?.RemainingTime.ToString() ?? "0")
                .Replace("%item_id", item?.Identity.ToString() ?? "0");

            str = str.Replace("%map_name", user?.Map?.Name ?? role?.Map?.Name ?? Language.StrNone);
            str = str.Replace("%iter_time", UnixTimestamp.Now().ToString());
            str = str.Replace("%timestamp", UnixTimestamp.Now().ToString());
            str = str.Replace("%server_version", ServerKernel.Version);
            return str;
        }

        #region 100 - Menu

        #region 101 - MenuText

        private bool MenuText101(GameActionEntity action, string param, Character user, ScreenObject role, Item item,
            string pszAccept)
        {
            if (user == null)
                return false;
            m_pReplies.Add(new MsgTaskDialog(MsgTaskDialog.DIALOG, param));
            return true;
        }

        #endregion

        #region 102 - MenuLink

        private bool MenuLink102(GameActionEntity action, string param, Character user, ScreenObject role, Item item,
            string pszAccept)
        {
            if (user == null)
                return false;

            string[] parsed = GetSafeParam(param, 3);
            string text = parsed[0];
            uint idTask = uint.Parse(parsed[1]);
            int nAlign = 0;
            if (parsed.Length > 2)
                nAlign = int.Parse(parsed[2]);
            m_pReplies.Add(new MsgTaskDialog(MsgTaskDialog.OPTION, text)
            {
                OptionId = user.PushTaskId(idTask),
                TaskId = idTask
            });
            return true;
        }

        #endregion

        #region 103 - MenuEdit

        private bool MenuEdit103(GameActionEntity action, string param, Character user, ScreenObject role, Item item,
            string pszAccept)
        {
            if (user == null)
                return false;

            string[] pstrParams = GetSafeParam(param, 3);
            m_pReplies.Add(new MsgTaskDialog(MsgTaskDialog.INPUT, pstrParams[2])
            {
                InputMaxLength = ushort.Parse(pstrParams[0]),
                OptionId = user.PushTaskId(uint.Parse(pstrParams[1]))
            });
            return true;
        }

        #endregion

        #region 104 - MenuPic

        private bool MenuPic104(GameActionEntity action, string param, Character user, ScreenObject role, Item item,
            string pszAccept)
        {
            if (user == null)
                return false;

            m_pReplies.Add(new MsgTaskDialog
            {
                InteractType = MsgTaskDialog.AVATAR,
                InputMaxLength = ushort.Parse(param.Split(' ')[2])
            });
            return true;
        }

        #endregion

        #region 120 - MenuCreate

        private bool MenuCreate120(GameActionEntity action, string param, Character user, ScreenObject role, Item item,
            string pszAccept)
        {
            if (user == null)
                return false;

            foreach (var msg in m_pReplies)
                user.Send(msg);
            user.Send(new MsgTaskDialog(MsgTaskDialog.FINISH, "") {DontDisplay = false});
            return true;
        }

        #endregion

        #region 121 - MenuRand

        private bool MenuRand121(GameActionEntity action, string param, Character user, ScreenObject role, Item item,
            string pszAccept)
        {
            string[] pszParam = param.Split(' ');
            float percent;
            int val1 = int.Parse(pszParam[0]);
            int val2 = int.Parse(pszParam[1]);
            if (val1 > val2)
                percent = 100;
            else
                percent = (float) val1 / val2;
            percent *= 100;
            return Calculations.ChanceCalc(percent);
        }

        #endregion

        #region 122 - MenuRandAction

        private bool MenuRandAction122(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            string[] pszParam = param.Split(' ');
            int i = 0;
            i += pszParam.Length;
            if (i <= 0)
                return false;
            i--;
            var nextaction = ThreadSafeRandom.RandGet(0, i % pszParam.Length);
            var npc = new MsgTaskDialog
            {
                InteractType = MsgTaskDialog.DIALOG
            };

            ProcessAction(uint.Parse(pszParam[nextaction]), m_pUser, m_pRole, m_pItem, pszAccept);
            return true;
        }

        #endregion

        #region 123 - MenuChkTime

        private bool MenuChkTime123(GameActionEntity action, string param, Character user, ScreenObject role, Item item,
            string pszAccept)
        {
            string[] pszParam = param.Split(' ');

            DateTime actual = DateTime.Now;
            var nCurWeekDay = (int) actual.DayOfWeek;
            int nCurHour = actual.Hour;
            int nCurMinute = actual.Minute;

            switch (action.Data)
            {
                #region Complete date (yyyy-mm-dd hh:mm yyyy-mm-dd hh:mm)

                case 0:
                {
                    if (pszParam
                            .Length < 4)
                        return false;

                    string[] time0 = pszParam
                        [1].Split(':');
                    string[] date0 = pszParam
                        [0].Split('-');
                    string[] time1 = pszParam
                        [3].Split(':');
                    string[] date1 = pszParam
                        [2].Split('-');

                    var dTime0 = new DateTime(int.Parse(date0[0]), int.Parse(date0[1]),
                        int.Parse(date0[2]), int.Parse(time0[0]), int.Parse(time0[1]), 0);
                    var dTime1 = new DateTime(int.Parse(date1[0]), int.Parse(date1[1]),
                        int.Parse(date1[2]), int.Parse(time1[0]), int.Parse(time1[1]), 59);

                    int timestamp0 = UnixTimestamp.Timestamp(dTime0);
                    int timestamp1 = UnixTimestamp.Timestamp(dTime1);
                    int usertimestamp = UnixTimestamp.Now();

                    return timestamp0 <= usertimestamp && timestamp1 >= usertimestamp;
                }

                #endregion

                #region On Year date (mm-dd hh:mm mm-dd hh:mm)

                case 1:
                {
                    if (pszParam
                            .Length < 4)
                        return false;

                    string[] time0 = pszParam
                        [1].Split(':');
                    string[] date0 = pszParam
                        [0].Split('-');
                    string[] time1 = pszParam
                        [3].Split(':');
                    string[] date1 = pszParam
                        [2].Split('-');

                    var dTime0 = new DateTime(DateTime.Now.Year, int.Parse(date0[1]),
                        int.Parse(date0[2]), int.Parse(time0[0]), int.Parse(time0[1]), 0);
                    var dTime1 = new DateTime(DateTime.Now.Year, int.Parse(date1[1]),
                        int.Parse(date1[2]), int.Parse(time1[0]), int.Parse(time1[1]), 59);

                    int timestamp0 = UnixTimestamp.MonthDayStamp(dTime0);
                    int timestamp1 = UnixTimestamp.MonthDayStamp(dTime1);
                    int usertimestamp = UnixTimestamp.MonthDayStamp();

                    return timestamp0 <= usertimestamp && timestamp1 >= usertimestamp;
                }

                #endregion

                #region Day of the month (dd hh:mm dd hh:mm)

                case 2:
                {
                    if (pszParam
                            .Length < 4)
                        return false;

                    string[] time0 = pszParam
                        [1].Split(':');
                    string date0 = pszParam
                        [0];
                    string[] time1 = pszParam
                        [3].Split(':');
                    string date1 = pszParam
                        [2];

                    var dTime0 = new DateTime(DateTime.Now.Year, DateTime.Now.Month,
                        int.Parse(date0), int.Parse(time0[0]), int.Parse(time0[1]), 0);
                    var dTime1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month,
                        int.Parse(date1), int.Parse(time1[0]), int.Parse(time1[1]), 59);

                    int timestamp0 = UnixTimestamp.DayOfTheMonthStamp(dTime0);
                    int timestamp1 = UnixTimestamp.DayOfTheMonthStamp(dTime1);
                    int usertimestamp = UnixTimestamp.DayOfTheMonthStamp();

                    return timestamp0 <= usertimestamp && timestamp1 >= usertimestamp;
                }

                #endregion

                #region Day of the week (dw hh:mm dw hh:mm)

                case 3:
                {
                    if (pszParam
                            .Length < 4)
                        return false;

                    string[] time0 = pszParam
                        [1].Split(':');
                    string[] time1 = pszParam
                        [3].Split(':');

                    int nDay0 = int.Parse(pszParam
                        [0]);
                    int nDay1 = int.Parse(pszParam
                        [2]);
                    int nHour0 = int.Parse(time0[0]);
                    int nHour1 = int.Parse(time1[0]);
                    int nMinute0 = int.Parse(time0[1]);
                    int nMinute1 = int.Parse(time1[1]);

                    int timeNow = nCurWeekDay * 24 * 60 + nCurHour * 60 + nCurMinute;
                    int from = nDay0 * 24 * 60 + nHour0 * 60 + nMinute0;
                    int to = nDay1 * 24 * 60 + nHour1 * 60 + nMinute1;

                    return timeNow >= from && timeNow <= to;
                }

                #endregion

                #region Hour check (hh:mm hh:mm)

                case 4:
                {
                    if (pszParam
                            .Length < 2)
                        return false;

                    string[] time0 = pszParam
                        [0].Split(':');
                    string[] time1 = pszParam
                        [1].Split(':');

                    int nHour0 = int.Parse(time0[0]);
                    int nHour1 = int.Parse(time1[0]);
                    int nMinute0 = int.Parse(time0[1]);
                    int nMinute1 = int.Parse(time1[1]);

                    int timeNow = nCurHour * 60 + nCurMinute;
                    int from = nHour0 * 60 + nMinute0;
                    int to = nHour1 * 60 + nMinute1;

                    return timeNow >= from && timeNow <= to;
                }

                #endregion

                #region Minute check (mm mm)

                case 5:
                {
                    if (pszParam
                            .Length < 2)
                        return false;

                    return nCurMinute >= int.Parse(pszParam
                               [0]) && nCurMinute <= int.Parse(pszParam
                               [1]);
                }

                #endregion
            }

            return false;
        }

        #endregion

        #region 124 - MenuActionPostCmd

        private bool MenuActionPostCmd124(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            if (m_pUser == null) return false;
            Program.WriteLog("MenuActionPostCmd124", LogType.WARNING);
            return true;
        }

        #endregion

        #region 125 - MenuBroadcastMsg

        private bool MenuBroadcastMsg125(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            ServerKernel.UserManager.SendToAllUser(param, (ChatTone) action.Data);
            return true;
        }

        #endregion

        #region 126 - MenuActionMessageBox

        private bool MenuActionMessageBox126(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            if (m_pUser == null) return false;
            Program.WriteLog("MenuActionMessageBox", LogType.WARNING);

            return true;
        }

        #endregion

        #region 127 - MenuActionExecuteQuery

        private bool MenuActionExecuteQuery127(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            try
            {
                new ActionRepository().ExecuteQuery(param);
            }
            catch
            {
                Program.WriteLog("Could not execute query ACTION_EXECUTEQUERY::" + action.Param, LogType.ERROR);
            }

            return true;
        }

        #endregion

        #endregion

        #region 200 - Npc

        #region 201 - ActionNpcAttr

        private bool ActionNpcAttr201(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 3) return false;

            try
            {
                if (user != null && user.InteractingNpc == null && pszParam.Length <= 3)
                    return false;
            }
            catch (Exception ex)
            {
                Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
                return false;
            }

            DynamicNpc targetNpc;
            if (pszParam.Length >= 4)
                targetNpc = ServerKernel.RoleManager.GetRole(uint.Parse(pszParam[3])) as DynamicNpc;
            else
                targetNpc = m_pUser?.InteractingNpc as DynamicNpc;

            if (targetNpc == null)
                return false;

            string szAttr = pszParam[0];
            string szOpt = pszParam[1];
            string szData = pszParam[2];

            switch (szAttr)
            {
                #region Life

                case "life":
                {
                    uint life = uint.Parse(szData);
                    switch (szOpt)
                    {
                        case "=":
                            targetNpc.SetAttrib(ClientUpdateType.Hitpoints, life);
                            return true;
                        case "==":
                            return targetNpc.Life == life;
                        case "<":
                            return targetNpc.Life < life;
                        case ">=":
                            return targetNpc.Life >= life;
                        case "+=":
                            return targetNpc.AddAttrib(ClientUpdateType.Hitpoints, int.Parse(szData));
                    }

                    break;
                }

                #endregion

                #region Data

                case "data0":
                case "data1":
                case "data2":
                case "data3":
                {
                    switch (szOpt)
                    {
                        case "=":
                            return targetNpc.SetAttrib(szAttr, int.Parse(szData));
                        case "+=":
                            return targetNpc.SetAttrib(szAttr, int.Parse(szData) + targetNpc.GetData(szAttr));
                        case "==":
                            return targetNpc.GetData(szAttr) == int.Parse(szData);
                        case "<":
                            return targetNpc.GetData(szAttr) < int.Parse(szData);
                        case "<=":
                            return targetNpc.GetData(szAttr) <= int.Parse(szData);
                        case ">":
                            return targetNpc.GetData(szAttr) > int.Parse(szData);
                        case ">=":
                            return targetNpc.GetData(szAttr) >= int.Parse(szData);
                    }

                    break;
                }

                #endregion

                #region OwnerIdentity

                case "ownerid":
                {
                    switch (szOpt)
                    {
                        case "=":
                            return targetNpc.SetOwnerIdentity(uint.Parse(szData));
                        case "==":
                            return targetNpc.OwnerIdentity == long.Parse(szData);
                    }

                    break;
                }

                #endregion

                #region OwnerType

                case "ownertype":
                {
                    if (szOpt == "==")
                        return targetNpc.OwnerType == long.Parse(szData);
                    return false;
                }

                #endregion

                #region Lookface

                case "lookface":
                {
                    switch (szOpt)
                    {
                        case "==":
                            return targetNpc.Lookface == long.Parse(szData);
                        case "=":
                            return targetNpc.SetAttrib(ClientUpdateType.Mesh, long.Parse(szData));
                    }

                    return false;
                }

                #endregion

                #region MaxLife

                case "maxlife":
                {
                    if (szOpt == "=")
                        return targetNpc.SetAttrib(ClientUpdateType.MaxHitpoints, uint.Parse(szData));
                    break;
                }

                #endregion
            }

            return false;
        }

        #endregion

        #region 205 - ActionNpcErase

        private bool ActionNpcErase205(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            var pNpc = user?.InteractingNpc as DynamicNpc;
            if (pNpc == null)
                return false;

            uint nType = action.Data;
            if (nType == 0)
            {
                user.InteractingNpc = null;
                return pNpc.DelNpc();
            }

            foreach (var npc in m_pUser.Map.RoleSet.Values.Where(x => x is DynamicNpc npc && npc.Type == nType))
            {
                if (npc is DynamicNpc dynamicNpc && !dynamicNpc.DelNpc())
                    return false;
            }

            return true;
        }

        #endregion

        #region 207 - ActionNpcResetSynOwner

        private bool ActionNpcResetSynOwner207(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            var pNpc = m_pRole as DynamicNpc;
            if (pNpc == null) return false;

            if (!pNpc.Map.IsSynMap()) return false;

            // todo pNpc->SetSynOwnerID(pSynAtt->GetID(), false);		// true: with link map

            var score = pNpc.Scores.Values.OrderByDescending(x => x.Score).FirstOrDefault();
            if (score != null)
            {
                Syndicate syn = ServerKernel.SyndicateManager.GetSyndicate(score.Identity);
                if (pNpc.IsCtfFlag())
                {
                    string szBase = "None";
                    switch (pNpc.Identity%10)
                    {
                        case 1: szBase = Language.StrCaptureTheFlagBottom; break;
                        case 2: szBase = Language.StrCaptureTheFlagMiddle; break;
                        case 3: szBase = Language.StrCaptureTheFlagTop; break;
                    }

                    ServerKernel.SyndicateManager.GetSyndicate(pNpc.OwnerIdentity)?.Send(new MsgWarFlag
                    {
                        Type = WarFlagType.WarBaseDominate,
                        Identity = 0
                    });
                    
                    syn?.Send(new MsgWarFlag
                    {
                        Type = WarFlagType.WarBaseDominate,
                        Identity = pNpc.Identity % 10,
                        MapX = pNpc.MapX,
                        MapY = pNpc.MapY
                    });

                    ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrCaptureTheFlagBaseNotify, syn.Name, szBase));
                }
                else
                {
                    ServerKernel.UserManager.SendToAllUser(string.Format(Language.StrWon, score.Name), ChatTone.Center);
                    if (syn != null)
                        pNpc.Map.OwnerIdentity = pNpc.OwnerIdentity;
                }

                if (syn != null)
                {
                    pNpc.SetOwnerIdentity(syn.Identity);
                    pNpc.Scores.Clear();
                    pNpc.Map.Save();
                }
            }

            foreach (var usr in pNpc.Map.PlayerSet.Values)
            {
                usr.SetAttackTarget(null);
            }

            if (pNpc.IsSynFlag() && !pNpc.IsCtfFlag())
            {
                foreach (var npc in pNpc.Map.RoleSet.Values)
                {
                    if (npc is Npc npc1)
                    {
                        npc1.OwnerIdentity = pNpc.OwnerIdentity;
                    }

                    if (npc is DynamicNpc dynaNpc && npc != pNpc)
                    {
                        dynaNpc.SetOwnerIdentity(pNpc.OwnerIdentity);
                        if (dynaNpc.Identity == 820)
                        {
                            dynaNpc.Data0 = (int) pNpc.OwnerIdentity;
                            dynaNpc.Data1 = 0;
                            dynaNpc.Data2 = 0;
                            dynaNpc.Data3 = 0;
                            dynaNpc.Save();
                        }
                    }
                }
            }

            return true;
        }

        #endregion

        #region 208 - ActionNpcFindNextTable

        private bool ActionNpcFindNextTable208(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (m_pRole == null) return false;

            string[] pszParam = GetSafeParam(action.Param);
            if (pszParam.Length < 4)
                return false;

            uint idNpc = uint.Parse(pszParam[0]);
            uint idMap = uint.Parse(pszParam[1]);
            ushort usMapX = ushort.Parse(pszParam[2]);
            ushort usMapY = ushort.Parse(pszParam[3]);

            DynamicNpc pNpc = ServerKernel.RoleManager.GetRole(idNpc) as DynamicNpc;
            if (pNpc == null) return false;

            pNpc.Data0 = (int) idMap;
            pNpc.Data1 = usMapX;
            pNpc.Data2 = usMapY;

            pNpc.Save();

            return true;
        }

        #endregion

        #endregion

        #region 300 - Map

        #region 301 - ActionMapMoveNpc

        private bool ActionMapMoveNpc301(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 3)
                return false;

            uint idMap = uint.Parse(pszParam[0]);
            ushort nPosX = ushort.Parse(pszParam[1]), nPosY = ushort.Parse(pszParam[2]);

            if (idMap <= 0 || nPosX <= 0 || nPosY <= 0)
                return false;

            DynamicNpc pNpc =
                (from map in ServerKernel.Maps.Values
                    select map.RoleSet.Values.FirstOrDefault(x => x is DynamicNpc && x.Identity == action.Data)
                    into obj
                    where obj != null
                    select obj as DynamicNpc).FirstOrDefault();

            if (pNpc == null)
                return false;

            if (!pNpc.IsDynaNpc())
                return false;

            return pNpc.ChangePos(idMap, nPosX, nPosY);
        }

        #endregion

        #region 302 - ActionMapMapUser

        private bool ActionMapMapUser302(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 3) return false;

            int amount = 0;

            switch (pszParam[0])
            {
                case "map_user":
                {
                    Map map;
                    if (!ServerKernel.Maps.TryGetValue(action.Data, out map))
                        return false;
                    amount = map.PlayerSet.Count;
                    break;
                }

                case "alive_user":
                {
                    Map map;
                    if (!ServerKernel.Maps.TryGetValue(action.Data, out map))
                        return false;
                    foreach (var usr in map.PlayerSet.Values.Where(x => x.IsAlive))
                        amount++;
                    break;
                }

                default:
                    Program.WriteLog($"ERROR: ACTION {action.Identity} invalid param", LogType.ERROR);
                    break;
            }

            switch (pszParam[1])
            {
                case "==":
                    return amount == int.Parse(pszParam[2]);
                case "<=":
                    return amount <= int.Parse(pszParam[2]);
                case ">=":
                    return amount >= int.Parse(pszParam[2]);
            }

            return false;
        }

        #endregion

        #region 303 - ActionMapBroadcastMsg

        private bool ActionMapBroadcastMsg303(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (param.Length > 127)
                return false;

            if (!ServerKernel.Maps.TryGetValue(action.Data, out var map))
                return false;

            map.SendMessageToMap(param, (ChatTone) action.Data);
            return true;
        }

        #endregion

        #region 304 - ActionMapDropItem

        private bool ActionMapDropItem304(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 4)
                return false;

            uint idMap = uint.Parse(pszParam[0]);
            uint idItemtype = uint.Parse(pszParam[3]);
            ushort x = ushort.Parse(pszParam[1]);
            ushort y = ushort.Parse(pszParam[2]);

            Map map;
            if (!ServerKernel.Maps.TryGetValue(idMap, out map))
                return false;

            var gItem = new MapItem((uint) ServerKernel.RoleManager.FloorIdentity.GetNextIdentity);
            if (!gItem.Create(map, new Point(x, y), idItemtype, 0, 0, 0, 0))
                return false;
            return true;
        }

        #endregion

        #region 305 - ActionMapSetStatus

        private bool ActionMapSetStatus305(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 3)
                return false;

            var idMap = uint.Parse(pszParam[0]);
            var dwStatus = byte.Parse(pszParam[1]);
            bool flag = pszParam[2] != "0";

            if (!ServerKernel.Maps.TryGetValue(idMap, out var temp))
                return false;

            temp.SetStatus(dwStatus, flag);
            return true;
        }

        #endregion

        #region 306 - ActionMapAttribute

        private bool ActionMapAttribute306(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);
            if (pszParam.Count() < 3) return false;

            string szField = pszParam[0];
            string szOpt = pszParam[1];
            int data = int.Parse(pszParam[2]);
            uint idMap = 0;

            if (pszParam.Count() >= 4)
                idMap = uint.Parse(pszParam[3]);

            Map pTaskMap = null;
            if (idMap == 0)
                pTaskMap = user.Map;
            else
                pTaskMap = ServerKernel.Maps.Values.FirstOrDefault(x => x.Identity == idMap);

            if (pTaskMap == null)
                return false;

            switch (szField)
            {
                case "status":
                {
                    switch (szOpt)
                    {
                        case "test":
                            return pTaskMap.IsWarTime();
                        case "set":
                            pTaskMap.SetStatus((byte) data, true); // |= (byte)data;
                            return true;
                        case "reset":
                            pTaskMap.SetStatus((byte) data, false);
                            break;
                    }

                    break;
                }

                case "type":
                {
                    switch (szOpt)
                    {
                        case "test":
                            return (pTaskMap.Type & data) != 0;
                    }

                    break;
                }
            }

            return true;
        }

        #endregion

        #region 307 - ActionMapRegionMonster

        private bool ActionMapRegionMonster307(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 8)
            {
                Program.WriteLog($"ERROR: Invalid param amount on actionid: [{action.Identity}]", LogType.ERROR);
                return false;
            }

            string szOpt = pszParam[6];
            uint idMap = uint.Parse(pszParam[0]);
            uint idType = uint.Parse(pszParam[5]);
            ushort nRegionX = ushort.Parse(pszParam[1]),
                nRegionY = ushort.Parse(pszParam[2]),
                nRegionCX = ushort.Parse(pszParam[3]),
                nRegionCY = ushort.Parse(pszParam[4]);
            int nData = int.Parse(pszParam[7]);

            Map map = null;
            if (idMap == 0)
            {
                if (user == null)
                {
                    Program.WriteLog($"ACTION {action.Identity}: Invalid map identity or _pUser class is set null",
                        LogType.ERROR);
                    return false;
                }

                map = user.Map;
            }
            else
            {
                if (!ServerKernel.Maps.TryGetValue(idMap, out map))
                    return false;
            }

            int nCount = 0;

            foreach (var mst in map.RoleSet.Values.Where(x => x is Monster))
            {
                if (mst.MapX >= nRegionX && mst.MapX < nRegionX - nRegionCX
                                         && mst.MapY >= nRegionY && mst.MapY < nRegionY - nRegionCY)
                    nCount++;
            }

            if (szOpt == "==")
                return nCount == nData;
            if (szOpt == "<")
                return nCount < nData;
            return false;
        }

        #endregion

        #region 310 - ActionMapChangeWeather

        private bool ActionMapChangeWeather310(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user == null)
                return false;

            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 5) return false;

            int nType = int.Parse(pszParam[0]), nIntensity = int.Parse(pszParam[1]), nDir = int.Parse(pszParam[2]);
            uint dwColor = uint.Parse(pszParam[3]), dwKeepSecs = uint.Parse(pszParam[4]);

            user.Map.Weather.SetNewWeather((Weather.WeatherType) nType, nIntensity, nDir, (int) dwColor,
                (int) dwKeepSecs, 0);
            user.Map.SendWeather();
            return true;
        }

        #endregion

        #region 311 - ActionMapChangeLight

        private bool ActionMapChangeLight311(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user == null)
                return false;

            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 3) return false;

            uint idMap = uint.Parse(pszParam[0]), dwRgb = uint.Parse(pszParam[1]);
            int nSecs = int.Parse(pszParam[2]);

            if (ServerKernel.Maps.TryGetValue(idMap, out Map map))
            {
                if (nSecs == 0)
                    map.Light = dwRgb;

                map.SendToMap(new MsgAction(1, dwRgb, 0, 0, GeneralActionType.MapArgb));
            }

            return false;
        }

        #endregion

        #region 312 - ActionMapEffect

        private bool ActionMapEffect312(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);

            if (pszParam.Length < 4) return false;

            uint idMap = uint.Parse(pszParam[0]);
            ushort posX = ushort.Parse(pszParam[1]), posY = ushort.Parse(pszParam[2]);
            string szEffect = pszParam[3];

            if (idMap <= 0) return false;

            if (!ServerKernel.Maps.TryGetValue(idMap, out var map))
                return false;

            var sPacket = new MsgName {Action = StringAction.MapEffect, PosX = posX, PosY = posY};
            sPacket.Append(szEffect);
            map.SendToRegion(sPacket, posX, posY);
            return true;
        }

        #endregion

        #region 314 - ActionMapFireworks

        private bool ActionMapFireworks314(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user == null)
                return false;

            var msg = new MsgItem
            {
                Action = ItemAction.Fireworks,
                Identity = user.Identity
            };
            user.Screen.Send(msg, true);
            return true;
        }

        #endregion

        #endregion

        #region 500 - Item

        #region 501 - ActionItemAdd

        private bool ActionItemAdd501(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user?.UserPackage == null)
                return false;

            if (!user.UserPackage.IsPackSpare(1))
                return false;

            if (!ServerKernel.Itemtype.TryGetValue(action.Data, out var itemtype))
                return false;

            string[] pszParam = GetSafeParam(param);

            var newItem = Item.CreateEntity(itemtype.Type);
            newItem.OwnerId = user.Identity;
            for (int i = 0; i < pszParam.Length; i++)
            {
                uint value = uint.Parse(pszParam[i]);
                if (value <= 0) continue;

                switch (i)
                {
                    case 0:
                        newItem.Amount = (ushort) value;
                        break;
                    case 1:
                        newItem.AmountLimit = (ushort) value;
                        break;
                    case 2:
                        // Socket Progress
                        newItem.Data = value;
                        break;
                    case 3:
                        if (Enum.IsDefined(typeof(SocketGem), (byte) value))
                            newItem.Gem1 = (byte) value;
                        break;
                    case 4:
                        if (Enum.IsDefined(typeof(SocketGem), (byte) value))
                            newItem.Gem2 = (byte) value;
                        break;
                    case 5:
                        if (Enum.IsDefined(typeof(ItemEffect), (ushort) value))
                            newItem.Magic1 = (byte) value;
                        break;
                    case 6:
                        // magic2.. w/e
                        break;
                    case 7:
                        if (value > 0 && value < 256)
                            newItem.Magic3 = (byte) value;
                        break;
                    case 8:
                        if (value > 0
                            && value < 8)
                            newItem.ReduceDmg = (byte) value;
                        break;
                    case 9:
                        if (value > 0
                            && value < 256)
                            newItem.AddLife = (byte) value;
                        break;
                    case 10:
                        newItem.Plunder = value;
                        break;
                    case 11:
                        if (value == 0)
                            value = 3;
                        if (Enum.IsDefined(typeof(ItemColor), value))
                            newItem.Color = (byte) value;
                        break;
                    case 12:
                        if (value > 0 && value < 256)
                            newItem.Monopoly = (byte) value;
                        break;
                    case 13:
                    case 14:
                    case 15:
                        // R -> For Steeds only
                        // G -> For Steeds only
                        // B -> For Steeds only
                        // G == 8 R == 16
                        newItem.Data = value | (uint.Parse(pszParam[14]) << 8) | (uint.Parse(pszParam[13]) << 16);
                        break;
                    case 16: // amount
                        newItem.StackAmount = (ushort) value;
                        break;
                    case 17:
                        newItem.RemainingTime = (uint) (UnixTimestamp.Now() + value);
                        break;
                }
            }

            if (pszParam.Length < 13 && item != null) // no monopoly set
            {
                newItem.Monopoly = (byte) (item.IsBound ? 3 : newItem.Monopoly);
            }

            return user.UserPackage.AwardItem(newItem) != null;
        }

        #endregion

        #region 502 - ActionItemDel

        private bool ActionItemDel502(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user?.UserPackage == null)
                return false;

            if (action.Data != 0)
                return user.UserPackage.MultiSpendItem(action.Data, action.Data, 1);
            if (action.Param != string.Empty)
            {
                Item pItem = user.UserPackage.GetInventory().FirstOrDefault(x => x.Itemtype.Name == action.Param);
                if (pItem == null)
                    return false;
                return user.UserPackage.SpendItem(pItem);
            }

            return false;
        }

        #endregion

        #region 503 - ActionItemCheck

        private bool ActionItemCheck503(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user?.UserPackage == null)
                return false;

            if (action.Data != 0)
                return user.UserPackage.MultiCheckItem(action.Data, action.Data, 1);
            if (param != string.Empty)
            {
                return user.UserPackage.GetInventory().FirstOrDefault(x => x.Itemtype.Name == param) != null;
            }

            return false;
        }

        #endregion

        #region 504 - ActionItemHole

        private bool ActionItemHole504(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (m_pUser == null || user.UserPackage == null)
                return false;

            string[] pszParam = GetSafeParam(action.Param);
            if (pszParam.Length < 2)
            {
                Program.WriteLog(
                    $"ACTION: invalid number of parameters (id:{action.Identity}, type{action.Type}): ChkHole/MakeHole id",
                    LogType.DEBUG);
                return false;
            }

            if (pszParam[0].ToLower() == "chkhole")
            {
                Item pItem = user.UserPackage[ItemPosition.RightHand];
                if (pItem == null)
                {
                    m_pUser.SendSysMessage(Language.StrUnexistentItem);
                    return false;
                }

                return pszParam[1] == "1"
                    ? pItem.SocketOne > SocketGem.NoSocket
                    : pszParam[1] == "2" && pItem.SocketTwo > SocketGem.NoSocket;
            }

            if (pszParam[0].ToLower() == "makehole")
            {
                Item pItem = user.UserPackage[ItemPosition.RightHand];
                if (pItem == null)
                {
                    m_pUser.SendSysMessage(Language.StrUnexistentItem);
                    return false;
                }

                if (pszParam[1] == "1" && pItem.SocketOne <= SocketGem.NoSocket)
                {
                    ServerKernel.Log.GmLog("make_hole", $"item[{pItem.Identity}:{pItem.Type}] make hole 1");
                    pItem.SocketOne = SocketGem.EmptySocket;
                }
                else if (pszParam[1] == "2" && pItem.SocketTwo <= SocketGem.NoSocket)
                {
                    ServerKernel.Log.GmLog("make_hole", $"item[{pItem.Identity}:{pItem.Type}] make hole 2");
                    pItem.SocketTwo = SocketGem.EmptySocket;
                }
                else
                {
                    return false;
                }

                m_pUser.Send(pItem.CreateMsgItemInfo(ItemMode.Update));
            }
            else
            {
                Program.WriteLog($"ACTION: invalid param data (id: {action.Identity})", LogType.DEBUG);
                return false;
            }

            return true;
        }

        #endregion

        #region 506 - ActionItemMultiDel

        private bool ActionItemMultiDel506(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (m_pUser == null || user.UserPackage == null)
                return false;

            string[] Params = GetSafeParam(param);

            if (action.Data == SpecialItem.TYPE_METEOR)
            {
                int.TryParse(Params[0], out int amount0);
                return user.UserPackage.SpendMeteors(amount0);
            }

            if (action.Data == SpecialItem.TYPE_DRAGONBALL)
            {
                if (Params.Length < 1)
                    return user.UserPackage.SpendDragonBalls(1, true);

                int amount0 = int.Parse(Params[0]);
                if (Params.Length < 2)
                    return user.UserPackage.SpendDragonBalls(amount0, true);

                uint bound = uint.Parse(Params[1]);
                return user.UserPackage.SpendDragonBalls(amount0, bound > 0);
            }

            if (action.Data > 0)
            {
                return false;
            }

            if (Params.Length < 3)
                return false;
            uint first = uint.Parse(Params[0]);
            uint last = uint.Parse(Params[1]);
            byte amount = byte.Parse(Params[2]);

            if (Params.Length < 4)
                return user.UserPackage.MultiSpendItem(first, last, amount);

            bool bbound = uint.Parse(Params[3]) > 0;
            return user.UserPackage.MultiSpendItem(first, last, amount, bbound);
        }

        #endregion

        #region 507 - ActionItemMultiChk

        private bool ActionItemMultiChk507(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user == null || user.UserPackage == null)
                return false;

            string[] pszParam = GetSafeParam(param);

            if (action.Data == SpecialItem.TYPE_METEOR)
            {
                uint.TryParse(pszParam[0], out uint amount0);
                return user.UserPackage.MeteorAmount() >= amount0;
            }

            if (action.Data == SpecialItem.TYPE_DRAGONBALL)
            {
                if (pszParam.Length < 1)
                    return user.UserPackage.DragonBallAmount() >= 1;

                uint amount0 = uint.Parse(pszParam[0]);
                if (pszParam.Length < 2)
                    return user.UserPackage.DragonBallAmount() >= amount0;

                uint bound = uint.Parse(pszParam[1]);
                if (pszParam.Length < 3)
                    return user.UserPackage.DragonBallAmount(bound > 0) >= amount0;

                uint onlybound = uint.Parse(pszParam[2]);
                return user.UserPackage.DragonBallAmount(bound > 0, onlybound > 0) >= amount0;
            }

            if (action.Data > 0)
                return false;

            if (pszParam.Length < 3)
                return false;
            uint first = uint.Parse(pszParam[0]);
            uint last = uint.Parse(pszParam[1]);
            byte amount = byte.Parse(pszParam[2]);
            if (pszParam.Length < 4)
                return user.UserPackage.MultiCheckItem(first, last, amount);

            uint bbound = uint.Parse(pszParam[1]);
            if (pszParam.Length < 3)
                return user.UserPackage.MultiCheckItem(first, last, amount, bbound == 0);
            return user.UserPackage.MultiCheckItem(first, last, amount, bbound == 0);
        }

        #endregion

        #region 508 - ActionItemLeaveSpace

        private bool ActionItemLeaveSpace508(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            return user.UserPackage.IsPackSpare((int) action.Data);
        }

        #endregion

        #region 509 - ActionItemUpEquipment

        private bool ActionItemUpEquipment509(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 2)
                return false;

            string szCmd = pszParam[0];
            byte nPos = byte.Parse(pszParam[1]);

            Item pItem = user.UserPackage[(ItemPosition) nPos];
            if (pItem == null)
                return false;

            switch (szCmd)
            {
                case "up_lev":
                {
                    return pItem.UpEquipmentLevel();
                }

                case "recover_dur":
                {
                    var szPrice = (uint) pItem.GetRecoverDurCost();
                    return user.SpendMoney((int) szPrice) && pItem.RecoverDurability();
                }

                case "up_levultra":
                {
                    return pItem.UpUltraEquipmentLevel();
                }

                case "up_quality":
                {
                    return pItem.UpItemQuality();
                }

                default:
                    Program.WriteLog($"ERROR: [509] [0] [{param}] not properly handled.", LogType.WARNING);
                    return false;
            }
        }

        #endregion

        #region 510 - ActionItemEquipTest

        private bool ActionItemEquipTest510(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            /* param: position type opt value (4 quality == 9) */
            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 4)
                return false;

            byte nPosition = byte.Parse(pszParam[0]);
            string szCmd = pszParam[1];
            string szOpt = pszParam[2];
            int nData = int.Parse(pszParam[3]);

            Item pItem = user.UserPackage[(ItemPosition) nPosition];
            if (pItem == null)
                return false;

            int nTestData = 0;
            switch (szCmd)
            {
                case "level":
                    nTestData = pItem.GetLevel();
                    break;
                case "quality":
                    nTestData = pItem.GetQuality();
                    break;
                case "durability":
                    if (nData == -1)
                        nData = pItem.MaxDurability / 100;
                    nTestData = pItem.MaxDurability / 100;
                    break;
                case "max_dur":
                {
                    if (nData == -1)
                        nData = pItem.Itemtype.AmountLimit / 100;
                    // TODO Kylin Gem Support
                    nTestData = pItem.MaxDurability / 100;
                    break;
                }

                default:
                    Program.WriteLog($"ACTION: EQUIPTEST error {szCmd}", LogType.WARNING);
                    return false;
            }

            if (szOpt == "==")
                return nTestData == nData;
            if (szOpt == "<=")
                return nTestData <= nData;
            if (szOpt == ">=")
                return nTestData >= nData;
            return false;
        }

        #endregion

        #region 511 - ActionItemEquipExist

        private bool ActionItemEquipExist511(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user == null)
                return false;

            string[] Params = GetSafeParam(param);
            if (param.Length >= 1 && user.UserPackage[(ItemPosition) action.Data] != null)
                return user.UserPackage[(ItemPosition) action.Data].GetItemSubtype() == ushort.Parse(Params[0]);
            return user.UserPackage[(ItemPosition) action.Data] != null;
        }

        #endregion

        #region 512 - ActionItemEquipColor

        private bool ActionItemEquipColor512(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);

            if (pszParam.Length < 2)
                return false;
            if (!Enum.IsDefined(typeof(ItemColor), byte.Parse(pszParam[1])))
                return false;

            Item pItem = user.UserPackage[(ItemPosition) byte.Parse(pszParam[0])];
            if (pItem == null)
                return false;

            ItemPosition pos = Calculations.GetItemPosition(pItem.Type);
            if (pos != ItemPosition.Armor
                && pos != ItemPosition.Headwear
                && (pos != ItemPosition.LeftHand || pItem.GetSort() != ItemSort.ItemsortWeaponShield))
                return false;

            pItem.Color = (ItemColor) byte.Parse(pszParam[1]);
            pItem.Save();
            user.Send(pItem.CreateMsgItemInfo(ItemMode.Update));
            return true;
        }

        #endregion

        #region 513 - ActionItemRemoveAny

        private bool ActionItemRemoveAny513(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            uint idItem = action.Data;
            if (!ServerKernel.Itemtype.TryGetValue(idItem, out _))
                return false;

            var allItem = Database.ItemRepository.FetchAllByType(idItem);
            if (allItem == null) return true;

            foreach (var pItem in allItem)
            {
                Character owner = ServerKernel.UserManager.GetUser(pItem.PlayerId);
                if (owner != null)
                {
                    bool stop = false;
                    if (owner.UserPackage.RemoveFromInventory(pItem.Id, RemovalType.Delete))
                        continue;
                    foreach (var equip in owner.UserPackage.GetEquipment())
                    {
                        if (equip.Identity == pItem.Id)
                        {
                            owner.UserPackage.Unequip(equip.Position, RemovalType.Delete);
                            stop = true;
                            break;
                        }
                    }

                    if (stop) continue;
                    //foreach (var wh in owner.Warehouses.Values) todo handle warehouses
                    //{
                    //    foreach (var hid in wh.Items.Values)
                    //    {
                    //        if (hid.Identity == pItem.Id)
                    //        {
                    //            wh.Remove(hid.Identity);
                    //            hid.Delete();
                    //            stop = true;
                    //            break;
                    //        }
                    //    }
                    //    if (stop) break;
                    //}
                }
                else
                {
                    Database.ItemRepository.Delete(pItem);
                }
            }

            return true;
        }

        #endregion

        #region 516 - ActionItemCheckRand

        private bool ActionItemCheckRand516(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 6)
                return false;

            byte initValue = byte.Parse(pszParam[3]), endValue = byte.Parse(pszParam[5]);

            List<ItemPosition> lPos = new List<ItemPosition>(15);

            byte pIdx = byte.Parse(pszParam[1]);

            if (initValue == 0 && pIdx == 14)
                initValue = 1;

            for (ItemPosition i = ItemPosition.Headwear; i <= ItemPosition.Crop + 1; i++)
            {
                if (user.UserPackage[i] != null)
                {
                    if (pIdx == 14 && user.UserPackage[i].Position == ItemPosition.Steed)
                        continue;

                    switch (pIdx)
                    {
                        case 14:
                            if (user.UserPackage[i].ReduceDamage >= initValue
                                && user.UserPackage[i].ReduceDamage <= endValue)
                                continue;
                            break;
                    }

                    lPos.Add(i);
                }
            }

            byte pos = 0;

            if (lPos.Count > 0)
                pos = (byte) lPos[ThreadSafeRandom.RandGet(lPos.Count) % lPos.Count];

            if (pos == 0)
                return false;

            Item pItem = user.UserPackage[(ItemPosition) pos];
            if (pItem == null)
                return false;

            byte pPos = byte.Parse(pszParam[0]);
            string opt = pszParam[2];

            switch (pIdx)
            {
                case 14: // bless
                    user.Data[7] = pos;
                    return true;
                default:
                    Program.WriteLog("ACTION: 516:" + pIdx + " not handled id:" + action.Identity, LogType.WARNING);
                    break;
            }

            return false;
        }

        #endregion

        #region 517 - ActionItemModify

        private bool ActionItemModify517(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            // structure param:
            // pos  type    action  value   update
            // 1    7       ==      1       1
            // pos = Item Position
            // type = 7 Reduce Damage
            // action = Operator == or set
            // value = value lol
            // update = if the client will update live

            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 5)
            {
                Program.WriteLog(
                    $"ACTION: incorrect param, pos type action value update, for action (id:{action.Identity})",
                    LogType.ERROR);
                return false;
            }

            int pos = int.Parse(pszParam[0]);
            int type = int.Parse(pszParam[1]);
            string opt = pszParam[2];
            long value = int.Parse(pszParam[3]);
            bool update = int.Parse(pszParam[4]) > 0;

            Item pItem = user.UserPackage[(ItemPosition) pos];
            if (pItem == null)
            {
                user.SendSysMessage(Language.StrUnexistentItem);
                return false;
            }

            switch (type)
            {
                case 1: // itemtype
                {
                    if (opt == "set")
                    {
                        ItemtypeEntity itemt;
                        if (!ServerKernel.Itemtype.TryGetValue((uint) value, out itemt))
                        {
                            // new item doesnt exist
                            Program.WriteLog($"ACTION: itemtype not found (type:{value}, action:{action.Identity})",
                                LogType.DEBUG);
                            return false;
                        }

                        if (pItem.Type / 1000 != itemt.Type / 1000)
                        {
                            Program.WriteLog(
                                $"ACTION: cant change to different type (type:{pItem.Type}, new:{value}, action:{action.Identity})",
                                LogType.DEBUG);
                            return false;
                        }

                        if (!pItem.ChangeType(itemt.Type, true))
                            return false;
                    }
                    else if (opt == "==")
                    {
                        return pItem.Type == value;
                    }
                    else if (opt == "<")
                    {
                        return pItem.Type < value;
                    }
                    else
                    {
                        return false;
                    }

                    break;
                }

                case 2: // owner id
                case 3: // player id
                    return false;
                case 4: // dura
                {
                    if (opt == "set")
                    {
                        if (value > ushort.MaxValue)
                            value = ushort.MaxValue;
                        else if (value < 0)
                            value = 0;

                        pItem.Durability = (ushort) value;
                    }
                    else if (opt == "==")
                    {
                        return pItem.Durability == value;
                    }
                    else if (opt == "<")
                    {
                        return pItem.Durability < value;
                    }
                    else
                    {
                        return false;
                    }

                    break;
                }

                case 5: // max dura
                {
                    if (opt == "set")
                    {
                        if (value > ushort.MaxValue)
                            value = ushort.MaxValue;
                        else if (value < 0)
                            value = 0;

                        if (value < pItem.Durability)
                            pItem.Durability = (ushort) value;

                        pItem.MaxDurability = (ushort) value;
                    }
                    else if (opt == "==")
                    {
                        return pItem.MaxDurability == value;
                    }
                    else if (opt == "<")
                    {
                        return pItem.MaxDurability < value;
                    }
                    else
                    {
                        return false;
                    }

                    break;
                }

                case 6:
                case 7: // position
                {
                    return false;
                }

                case 8: // gem1
                {
                    if (opt == "set")
                    {
                        pItem.SocketOne = (SocketGem) value;
                    }
                    else if (opt == "==")
                    {
                        return pItem.SocketOne == (SocketGem) value;
                    }
                    else if (opt == "<")
                    {
                        return pItem.SocketOne < (SocketGem) value;
                    }
                    else
                    {
                        return false;
                    }

                    break;
                }

                case 9: // gem2
                {
                    if (opt == "set")
                    {
                        pItem.SocketTwo = (SocketGem) value;
                    }
                    else if (opt == "==")
                    {
                        return pItem.SocketTwo == (SocketGem) value;
                    }
                    else if (opt == "<")
                    {
                        return pItem.SocketTwo < (SocketGem) value;
                    }
                    else
                    {
                        return false;
                    }

                    break;
                }

                case 10: // magic1
                {
                    if (opt == "set")
                    {
                        if (value < 200 || value > 203)
                            return false;
                        pItem.Effect = (ItemEffect) value;
                    }
                    else if (opt == "==")
                    {
                        return pItem.Effect == (ItemEffect) value;
                    }
                    else
                    {
                        return false;
                    }

                    break;
                }

                case 11: // magic2
                    return false;
                case 12: // magic3
                {
                    if (opt == "set")
                    {
                        if (value < 0)
                            value = 0;
                        else if (value > 12)
                            value = 12;

                        pItem.Addition = (byte) value;
                    }
                    else if (opt == "==")
                    {
                        return pItem.Addition == value;
                    }
                    else if (opt == "<")
                    {
                        return pItem.Addition < value;
                    }
                    else
                    {
                        return false;
                    }

                    break;
                }

                case 13: // data
                {
                    if (opt == "set")
                    {
                        if (value < 0)
                            value = 0;
                        else if (value > 20000)
                            value = 20000;

                        pItem.SocketProgress = (ushort) value;
                    }
                    else if (opt == "==")
                    {
                        return pItem.SocketProgress == value;
                    }
                    else if (opt == "<")
                    {
                        return pItem.SocketProgress < value;
                    }
                    else
                    {
                        return false;
                    }

                    break;
                }

                case 14: // reduce damage
                {
                    if (opt == "set")
                    {
                        if (value < 0)
                            value = 0;
                        else if (value > 7)
                            value = 7;

                        pItem.ReduceDamage = (byte) value;
                    }
                    else if (opt == "==")
                    {
                        return pItem.ReduceDamage == value;
                    }
                    else if (opt == "<")
                    {
                        return pItem.ReduceDamage < value;
                    }
                    else
                    {
                        return false;
                    }

                    break;
                }

                case 15: // add life
                {
                    if (opt == "set")
                    {
                        if (value < 0)
                            value = 0;
                        else if (value > 255)
                            value = 255;

                        pItem.Enchantment = (byte) value;
                    }
                    else if (opt == "==")
                    {
                        return pItem.Enchantment == value;
                    }
                    else if (opt == "<")
                    {
                        return pItem.Enchantment < value;
                    }
                    else
                    {
                        return false;
                    }

                    break;
                }

                case 16: // anti monster
                case 17: // chk sum
                case 18: // plunder
                case 19: // special flag
                    return false;
                case 20: // color
                {
                    if (opt == "set")
                    {
                        if (!Enum.IsDefined(typeof(ItemColor), value))
                            return false;

                        pItem.Color = (ItemColor) value;
                    }
                    else if (opt == "==")
                    {
                        return pItem.Color == (ItemColor) value;
                    }
                    else if (opt == "<")
                    {
                        return pItem.Color < (ItemColor) value;
                    }
                    else
                    {
                        return false;
                    }

                    break;
                }

                case 21: // add lev exp
                {
                    if (opt == "set")
                    {
                        if (value < 0)
                            value = 0;
                        if (value > ushort.MaxValue)
                            value = ushort.MaxValue;

                        pItem.AdditionProgress = (ushort) value;
                    }
                    else if (opt == "==")
                    {
                        return pItem.AdditionProgress == value;
                    }
                    else if (opt == "<")
                    {
                        return pItem.AdditionProgress < value;
                    }
                    else
                    {
                        return false;
                    }

                    break;
                }

                case 22: // monopoly
                case 23: // inscribed
                case 24: // artifact type
                case 25: // artifact start
                case 26: // artifact expire
                case 27: // artifact stabilization
                case 28: // refinery type
                case 29: // refinery level
                    return false;
                default:
                    return false;
            }

            if (update)
                user.Send(pItem.CreateMsgItemInfo(ItemMode.Update));
            return true;
        }

        #endregion

        #region 528 - ActionItemJarCreate

        private bool ActionItemJarCreate528(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (m_pUser == null)
                return false;

            if (!m_pUser.UserPackage.IsPackSpare(1))
                return false;

            if (!ServerKernel.Itemtype.TryGetValue(action.Data, out var itemtype))
                return false;
            string[] pszParam = GetSafeParam(param);

            var newItem = new ItemEntity
            {
                AddLife = 0,
                AddlevelExp = 0,
                AntiMonster = 0,
                ArtifactExpire = 0,
                ArtifactType = 0,
                ChkSum = 0,
                Color = 3,
                Data = 0,
                Gem1 = 0,
                Gem2 = 0,
                Ident = 0,
                Magic1 = 0,
                Magic2 = 0,
                ReduceDmg = 0,
                Plunder = 0,
                Specialflag = 0,
                Inscribed = 0,
                StackAmount = 1,
                RefineryExpire = 0,
                RefineryLevel = 0,
                RefineryType = 0,
                Type = itemtype.Type,
                Position = 0,
                PlayerId = user.Identity,
                Monopoly = 0,
                Magic3 = itemtype.Magic3,
                Amount = itemtype.Amount,
                AmountLimit = itemtype.AmountLimit
            };
            for (int i = 0; i < pszParam.Length; i++)
            {
                uint value = uint.Parse(pszParam[i]);
                if (value <= 0) continue;

                switch (i)
                {
                    case 0:
                        newItem.Amount = (ushort) value;
                        break;
                    case 1:
                        newItem.AmountLimit = (ushort) value;
                        break;
                    case 2:
                        // Socket Progress
                        newItem.Data = value;
                        break;
                    case 3:
                        if (Enum.IsDefined(typeof(SocketGem), (byte) value))
                            newItem.Gem1 = (byte) value;
                        break;
                    case 4:
                        if (Enum.IsDefined(typeof(SocketGem), (byte) value))
                            newItem.Gem2 = (byte) value;
                        break;
                    case 5:
                        if (Enum.IsDefined(typeof(ItemEffect), (ushort) value))
                            newItem.Magic1 = (byte) value;
                        break;
                    case 6:
                        // magic2.. w/e
                        break;
                    case 7:
                        if (value > 0 && value < 256)
                            newItem.Magic3 = (byte) value;
                        break;
                    case 8:
                        if (value > 0
                            && value < 8)
                            newItem.ReduceDmg = (byte) value;
                        break;
                    case 9:
                        if (value > 0
                            && value < 256)
                            newItem.AddLife = (byte) value;
                        break;
                    case 10:
                        newItem.Plunder = value;
                        break;
                    case 11:
                        if (value == 0)
                            value = 3;
                        if (Enum.IsDefined(typeof(ItemColor), value))
                            newItem.Color = (byte) value;
                        break;
                    case 12:
                        if (value > 0 && value < 256)
                            newItem.Monopoly = (byte) value;
                        break;
                    case 13:
                    case 14:
                    case 15:
                        // R -> For Steeds only
                        // G -> For Steeds only
                        // B -> For Steeds only
                        // G == 8 R == 16
                        newItem.Data = value | (uint.Parse(pszParam[14]) << 8) | (uint.Parse(pszParam[13]) << 16);
                        break;
                }
            }

            m_pUser.UserPackage.AwardItem(newItem);

            MsgInteract pMsg = new MsgInteract
            {
                EntityIdentity = m_pUser.Identity,
                Amount = m_pUser.GetStatisticValue(6, 12)
            };
            m_pUser.Send(pMsg);
            return true;
        }

        #endregion

        #region 529 - ActionItemJarVerify

        private bool ActionItemJarVerify529(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user == null)
                return false;

            if (!user.UserPackage.IsPackSpare(1))
                return false;

            string[] pszParam = GetSafeParam(param);

            if (pszParam.Length < 2) return false;

            uint amount = uint.Parse(pszParam[1]);
            uint monster = uint.Parse(pszParam[0]);

            if (user.GetStatisticValue(6, 5) != monster)
                return false;

            Item jar = null;
            foreach (var pItem in user.UserPackage.GetInventory().Where(x => x.Type == action.Data))
            {
                if (pItem.MaxDurability == monster)
                {
                    jar = pItem;
                    break;
                }
            }

            return jar != null && jar.Durability < amount;
        }

        #endregion

        #endregion

        #region 700 - Syndicate

        #region 701 - ActionSynCreate

        private bool ActionSynCreate701(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] _params = GetSafeParam(param);

            if (user == null || user.Syndicate != null)
                return false;

            if (user.Level < int.Parse(_params[0]))
            {
                user.SendSysMessage(string.Format(Language.StrSynCreateNotEnoughLevel, _params[0])); // _params[0]
                return false;
            }

            if (user.Silver < uint.Parse(_params[1]))
            {
                user.SendSysMessage(string.Format(Language.StrSynCreateNotEnoughMoney, _params[1])); // _params[1]
                return false;
            }

            user.CreateSyndicate(pszAccept, uint.Parse(_params[1]));
            return true;
        }

        #endregion

        #region 702 - ActionSynDestroy

        private bool ActionSynDestroy702(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user?.Syndicate == null)
                return false;

            if (user.Syndicate == null || user.SyndicateMember == null)
            {
                user.SendSysMessage(Language.StrNoDisband);
                return false;
            }

            if (user.SyndicatePosition != SyndicateRank.GuildLeader)
            {
                user.SendSysMessage(Language.StrNoDisbandLeader);
                return false;
            }

            user.DisbandSyndicate();
            return true;
        }

        #endregion

        #region 709 - ActionSynChangeLeader

        private bool ActionSynChangeLeader709(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user?.Syndicate == null)
                return false;

            if (user.SyndicatePosition != SyndicateRank.GuildLeader)
            {
                user.SendSysMessage(Language.StrSynNotAuthorized);
                return false;
            }

            return user.Syndicate.AbdicateSyndicate(user.SyndicateMember, pszAccept);
        }

        #endregion

        #region 717 - ActionSynAttr

        private bool ActionSynAttr717(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user?.Syndicate == null)
                return false;

            string[] pszParam = GetSafeParam(param);

            if (pszParam.Length < 3)
                return false;

            string szField = pszParam[0], szOpt = pszParam[1];
            int nData = int.Parse(pszParam[2]);
            Syndicate syn = null;
            if (pszParam.Length >= 4 &&
                (syn = ServerKernel.SyndicateManager.GetSyndicate(uint.Parse(pszParam[3]))) == null)
                return false;
            if (pszParam.Length < 4 && user?.Syndicate != null) syn = user.Syndicate;
            if (syn == null)
                return false;

            switch (szField)
            {
                case "money":
                    switch (szOpt)
                    {
                        case "+=":
                            return syn.ChangeFunds(nData);
                        case "<":
                            return syn.SilverDonation < (uint) nData;
                    }

                    break;
                case "emoney":
                    switch (szOpt)
                    {
                        case "+=":
                            return syn.ChangeEmoneyFunds(nData);
                        case "<":
                            return syn.EmoneyDonation < (uint) nData;
                    }

                    break;
                case "membernum":
                    switch (szOpt)
                    {
                        case "<":
                            return syn.MemberCount < (uint) nData;
                    }

                    break;
                case "level":
                    switch (szOpt)
                    {
                        case "==":
                            return syn.Level == nData;
                        case "<":
                            return syn.Level < nData;
                    }

                    break;
            }

            return false;
        }

        #endregion

        #endregion

        #region 800 - Monster

        #region 801 - ActionMstDropItem

        private bool ActionMstDropItem801(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            Monster pMonster = role as Monster;
            if (action == null || pMonster == null) return false;

            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 2) return false;

            string type = pszParam[0];
            uint data = uint.Parse(pszParam[1]);

            int percent = 100;

            if (pszParam.Length >= 3)
                percent = int.Parse(pszParam[2]);

            switch (type)
            {
                case "dropitem":
                {
                    uint idUser = 0;
                    if (m_pUser != null)
                        idUser = m_pUser.Identity;
                    return pMonster.DropItem(data, idUser, 0, 0, 0, 0);
                }

                case "dropmoney":
                {
                    percent %= 100;
                    uint dwMoneyDrop = (uint) (data * (percent + Calculations.Random.Next(100 - percent)) / 100);
                    if (dwMoneyDrop <= 0)
                        return false;
                    uint idUser = 0;
                    if (m_pUser != null)
                        idUser = m_pUser.Identity;
                    return pMonster.DropMoney(data, idUser);
                }
            }

            return true;
        }

        #endregion

        #region 803 - ActionMstRefinery

        private bool ActionMstRefinery803(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            Monster pMonster = role as Monster;
            if (pMonster == null)
                return false;

            byte refLevel = (byte) (action.Data % 5);

            if (refLevel == 0)
                refLevel = (byte) Math.Max(ThreadSafeRandom.RandGet(0, 6) % 6, 1);

            List<RefineryEntity> pDrop = ServerKernel.Refinery.Values.Where(x => x.Level == refLevel).ToList();
            List<uint> remove = new List<uint>();

            foreach (var pRef in pDrop)
            {
                ItemtypeEntity itemtype;
                if (!ServerKernel.Itemtype.TryGetValue(pRef.Id, out itemtype))
                {
                    remove.Add(pRef.Id);
                    continue;
                }

                if (itemtype.Monopoly == 9 || itemtype.Name.Contains("(B)"))
                {
                    remove.Add(pRef.Id);
                }
            }

            foreach (var rem in remove)
            {
                pDrop.RemoveAll(x => x.Id == rem);
            }

            RefineryEntity refinery = null;
            try
            {
                refinery = pDrop[ThreadSafeRandom.RandGet() % pDrop.Count];
            }
            catch
            {
                return false;
            }

            if (refinery == null)
                return false;

            uint idUser = 0u;
            if (m_pUser != null)
                idUser = m_pUser.Identity;

            return pMonster.DropItem(refinery.Id, idUser, 0, 0, 0, 0);
        }

        #endregion

        #endregion

        #region 900 Family

        #region 901 - ActionFamilyCreate

        private bool ActionFamilyCreate901(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user == null)
                return false;

            if (user.Family != null)
            {
                user.SendSysMessage(Language.StrFamilyAlreadyJoined);
                return false;
            }

            uint dwPrice = 500000;
            uint dwDonation = 250000;
            byte pLev = 50;

            string[] pParam = GetSafeParam(param);
            if (pParam.Length > 0)
                pLev = byte.Parse(pParam[0]);
            if (pParam.Length > 1)
                dwPrice = uint.Parse(pParam[1]);
            if (pParam.Length > 2)
                dwDonation = uint.Parse(pParam[2]);

            if (user.Level < 50)
            {
                user.SendSysMessage(Language.StrFamilyCreateLowLevel);
                return false;
            }

            if (!Handlers.CheckName(pszAccept))
            {
                user.SendSysMessage(Language.StrFamilyInvalidName);
                return false;
            }

            if (user.Silver < dwPrice)
            {
                user.SendSysMessage(Language.StrNotEnoughMoney);
                return false;
            }

            return user.CreateFamily(pszAccept, dwPrice, dwDonation);
        }

        #endregion

        #region 902 - ActionFamilyDestroy

        private bool ActionFamilyDestroy902(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user?.Family == null || user.FamilyMember == null)
                return false;
            return user.DisbandFamily();
        }

        #endregion

        #region 917 - ActionFamilyAttr

        private bool ActionFamilyAttr917(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user?.Family == null)
                return false;

            string[] safeParam = GetSafeParam(param);

            if (safeParam.Length < 3)
                return false;

            string szField = safeParam[0], szOpt = safeParam[1];
            int nData = int.Parse(safeParam[2]);
            Family family;
            if (safeParam.Length >= 4 && !ServerKernel.Families.TryGetValue(uint.Parse(safeParam[3]), out family))
                return false;
            if (safeParam.Length < 4 && user?.Family != null)
                family = user.Family;
            else return false;

            if (family == null)
                return false;

            switch (szField)
            {
                case "money":
                    switch (szOpt)
                    {
                        case "+=":
                            return family.ChangeFunds(nData);
                        case "<":
                            return family.MoneyFunds < (uint) nData;
                    }

                    break;
                case "membernum":
                    switch (szOpt)
                    {
                        case "<":
                            return family.MembersCount < (uint) nData;
                    }

                    break;
                case "level":
                    switch (szOpt)
                    {
                        case "==":
                            return family.Level == nData;
                        case "<":
                            return family.Level < nData;
                    }

                    break;
                case "bptower":
                    switch (szOpt)
                    {
                        case "==":
                            return family.BpTower == nData;
                        case "<":
                            return family.BpTower < nData;
                    }

                    break;
            }

            return false;
        }

        #endregion

        #region 918 - ActionFamilyUpLev

        private bool ActionFamilyUpLev918(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user?.Family == null)
                return false;

            if (user.Family.Level >= 5)
                return false; // max level

            user.Family.Level += 1;
            user.Family.SendFamily(user);
            return true;
        }

        #endregion

        #region 919 - ActionFamilyBpUpLev

        private bool ActionFamilyBpUpLev919(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user?.Family == null)
                return false;

            if (user.Family.BpTower >= 4 || user.Family.BpTower >= user.Family.Level)
                return false; // max level

            user.Family.BpTower += 1;
            user.Family.SendFamily(user);
            return true;
        }

        #endregion

        #endregion

        #region 1000 - User

        #region 1001 - UserAttr

        private bool UserAttr1001(GameActionEntity action, string param, Character user, ScreenObject role, Item item,
            string pszAccept)
        {
            if (user == null)
                return false;

            string[] Params = GetSafeParam(param);
            string type, opt, value, last = "";
            if (Params.Length < 3)
                return false;
            if (Params.Length > 3)
                last = Params[3];
            type = Params[0];
            opt = Params[1];
            value = Params[2];

            switch (type)
            {
                #region force (+=, ==, <)

                case "strength":
                case "force":
                {
                    switch (opt)
                    {
                        case "+=":
                            return user.ChangeForce(short.Parse(value));
                        case "==":
                            return user.Force == ushort.Parse(value);
                        case "<":
                            return user.Force < ushort.Parse(value);
                    }

                    return false;
                }

                #endregion

                #region speed (+=, ==, <)

                case "agility":
                case "speed":
                {
                    switch (opt)
                    {
                        case "+=":
                            return user.ChangeSpeed(short.Parse(value));
                        case "==":
                            return user.Agility == ushort.Parse(value);
                        case "<":
                            return user.Agility < ushort.Parse(value);
                    }

                    return false;
                }

                #endregion

                #region health (+=, ==, <)

                case "vitality":
                case "health":
                {
                    switch (opt)
                    {
                        case "+=":
                            return user.ChangeHealth(short.Parse(value));
                        case "==":
                            return user.Vitality == ushort.Parse(value);
                        case "<":
                            return user.Vitality < ushort.Parse(value);
                    }

                    return false;
                }

                #endregion

                #region soul (+=, ==, <)

                case "spirit":
                case "soul":
                {
                    switch (opt)
                    {
                        case "+=":
                            return user.ChangeSoul(short.Parse(value));
                        case "==":
                            return user.Spirit == ushort.Parse(value);
                        case "<":
                            return user.Spirit < ushort.Parse(value);
                    }

                    return false;
                }

                #endregion

                #region metempsychosis (==, <)

                case "metempsychosis":
                {
                    switch (opt)
                    {
                        case "==":
                            return user.Metempsychosis == byte.Parse(value);
                        case "<":
                            return user.Metempsychosis < byte.Parse(value);
                    }

                    return false;
                }

                #endregion

                #region reincarnation (==, <)

                case "reincarnation":
                {
                    switch (opt)
                    {
                        case "==":
                            return user.Reincarnation == uint.Parse(value);
                        case "<":
                            return user.Reincarnation < uint.Parse(value);
                    }

                    return false;
                }

                #endregion

                #region nobility_rank (==, <)

                case "nobility":
                case "nobility_rank":
                {
                    switch (opt)
                    {
                        case "==":
                            return user.Nobility.Level == (NobilityLevel) ushort.Parse(value);
                        case "<":
                            return user.Nobility.Level == (NobilityLevel) ushort.Parse(value);
                    }

                    return false;
                }

                #endregion

                #region level (+=, ==, <)

                case "level":
                {
                    switch (opt)
                    {
                        case "+=":
                        {
                            byte hue = byte.Parse(value);
                            return user.AwardLevel(hue);
                        }

                        case "==":
                            return user.Level == ushort.Parse(value);
                        case "<":
                            return user.Level < ushort.Parse(value);
                    }

                    return false;
                }

                #endregion

                #region money (+=, ==, <)

                case "money":
                {
                    switch (opt)
                    {
                        case "+=":
                            return user.ChangeMoney(int.Parse(value));
                        case "==":
                            return user.Silver == uint.Parse(value);
                        case "<":
                            return user.Silver < uint.Parse(value);
                    }

                    return false;
                }

                #endregion

                #region e_money (+=, ==, <)

                case "emoney":
                case "e_money":
                {
                    switch (opt)
                    {
                        case "+=":
                            EmoneySourceType source = EmoneySourceType.System;

                            if (role is BaseNpc)
                            {
                                source = EmoneySourceType.Npc;
                            }
                            else if (user != m_pUser)
                            {
                                source = EmoneySourceType.User;
                            }

                            return user.ChangeEmoney(int.Parse(value), source, role);
                        case "==":
                            return user.Emoney == uint.Parse(value);
                        case "<":
                            return user.Emoney < uint.Parse(value);
                    }

                    return false;
                }

                #endregion

                #region e_money2 (+=, ==, <) (CPs Bound)

                case "emoney2":
                case "e_money2":
                {
                    switch (opt)
                    {
                        case "+=":
                            return user.ChangeBoundEmoney(int.Parse(value));
                        case "==":
                            return user.BoundEmoney == uint.Parse(value);
                        case "<":
                            return user.BoundEmoney < uint.Parse(value);
                    }

                    return false;
                }

                #endregion

                #region profession (==, >=, <=, set)

                case "profession":
                {
                    switch (opt)
                    {
                        case "==":
                            return user.Profession == (ProfessionType) ushort.Parse(value);
                        case ">=":
                            return user.Profession >= (ProfessionType) ushort.Parse(value);
                        case "<":
                            return user.Profession < (ProfessionType) ushort.Parse(value);
                        case "<=":
                            return user.Profession <= (ProfessionType) ushort.Parse(value);
                        case "set":
                            user.Profession = (ProfessionType) ushort.Parse(value);
                            return true;
                    }

                    return false;
                }

                #endregion

                #region First Profession (==, >=, <=)

                case "first_profession":
                {
                    switch (opt)
                    {
                        case "==":
                            return user.FirstProfession == (ProfessionType) ushort.Parse(value);
                        case ">=":
                            return user.FirstProfession >= (ProfessionType) ushort.Parse(value);
                        case "<=":
                            return user.FirstProfession <= (ProfessionType) ushort.Parse(value);
                    }

                    return false;
                }

                #endregion

                #region Last Profession (==, >=, <=)

                case "last_profession":
                {
                    switch (opt)
                    {
                        case "==":
                            return user.LastProfession == (ProfessionType) ushort.Parse(value);
                        case ">=":
                            return user.LastProfession >= (ProfessionType) ushort.Parse(value);
                        case "<=":
                            return user.LastProfession <= (ProfessionType) ushort.Parse(value);
                    }

                    return false;
                }

                #endregion

                #region pk (+=, ==, <)

                case "pk":
                {
                    switch (opt)
                    {
                        case "==":
                            return user.PkPoints == ushort.Parse(value);
                        case "<":
                            return user.PkPoints < ushort.Parse(value);
                        case "+=":
                            return user.ChangePkPoints(short.Parse(value));
                    }

                    return false;
                }

                #endregion

                #region exp (+=, ==, <)

                case "exp":
                {
                    switch (opt)
                    {
                        case "+=":
                            long exp = long.Parse(value);
                            if (exp < 0)
                                exp = 0;
                            return user.AwardExperience(exp);
                        case "==":
                            return user.Experience == (ulong) long.Parse(value);
                        case "<":
                            return user.Experience < (ulong) long.Parse(value);
                    }

                    return false;
                }

                #endregion

                #region vip (<, ==)

                case "vip":
                {
                    switch (opt)
                    {
                        case "<":
                            return user.Owner.VipLevel < byte.Parse(value);
                        case "==":
                            return user.Owner.VipLevel == byte.Parse(value);
                    }

                    return false;
                }

                #endregion

                #region iterator (=)

                case "iterator":
                {
                    switch (opt)
                    {
                        case "=":
                        {
                            user.Iterator = long.Parse(value);
                            return true;
                        }
                    }

                    return false;
                }

                #endregion

                #region rankshow (<, ==)

                case "rankshow":
                {
                    switch (opt)
                    {
                        case ">":
                            return (ushort) user.SyndicatePosition > ushort.Parse(value);
                        case "<":
                            return (ushort) user.SyndicatePosition < ushort.Parse(value);
                        case "==":
                            return (ushort) user.SyndicatePosition == ushort.Parse(value);
                    }

                    return false;
                }

                #endregion

                #region crime (==, set)

                case "crime":
                {
                    switch (opt)
                    {
                        case "==":
                        {
                            if (value == "0")
                                return user.QueryStatus(FlagInt.BLUE_NAME) == null;
                            return user.QueryStatus(FlagInt.BLUE_NAME) != null;
                        }

                        case "set":
                        {
                            if (value == "0")
                            {
                                user.DetachStatus(FlagInt.BLUE_NAME);
                            }
                            else
                            {
                                user.SetCrimeStatus(60);
                            }

                            return true;
                        }
                    }

                    return false;
                }

                #endregion

                #region ep (+=, <, >, ==)

                case "ep":
                {
                    switch (opt)
                    {
                        case "+=":
                        {
                            return user.ChangeStamina(sbyte.Parse(value));
                        }

                        case "<":
                        {
                            return user.Stamina < sbyte.Parse(value);
                        }

                        case ">":
                        {
                            return user.Stamina > sbyte.Parse(value);
                        }

                        case "==":
                        {
                            return user.Stamina == sbyte.Parse(value);
                        }
                    }

                    return false;
                }

                #endregion

                #region Attribute Points (+=)

                case "attr_points":
                {
                    switch (opt)
                    {
                        case "+=":
                            return user.ChangeAttributePoints(Convert.ToInt32(value));
                    }

                    return false;
                }

                #endregion

                #region Virtue

                case "virtue":
                    switch (opt)
                    {
                        case "==":
                            return user.VirtuePoints == uint.Parse(value);
                        case "<":
                            return user.VirtuePoints < uint.Parse(value);
                        case "<=":
                            return user.VirtuePoints <= uint.Parse(value);
                        case ">":
                            return user.VirtuePoints > uint.Parse(value);
                        case ">=":
                            return user.VirtuePoints >= uint.Parse(value);
                        case "!=":
                        case "<>":
                            return user.VirtuePoints != uint.Parse(value);
                        case "+=":
                        {
                            int nVal = int.Parse(value);
                            if (nVal < 0)
                            {
                                nVal *= -1;
                                if (user.VirtuePoints - nVal < 0)
                                    return false;
                                user.VirtuePoints -= (uint) nVal;
                                return true;
                            }

                            if (nVal > 0)
                            {
                                if (nVal + user.VirtuePoints > int.MaxValue)
                                    user.VirtuePoints = int.MaxValue;
                                else
                                {
                                    user.VirtuePoints += (uint) nVal;
                                }

                                return true;
                            }

                            return false;
                        }
                    }

                    return false;

                #endregion

                #region transformation

                case "transformation":
                {
                    switch (opt)
                    {
                        case "==":
                            return user.QueryTransformation.Lookface == uint.Parse(value);
                        case "<>":
                        case "!=":
                            return user.QueryTransformation.Lookface != uint.Parse(value);
                    }

                    return false;
                }

                #endregion

                #region study_points

                case "study":
                case "study_points":
                {
                    switch (opt)
                    {
                        case "+=":
                        {
                            return user.ChangeStudyPoints(int.Parse(value));
                        }

                        case "<=": return user.StudyPoints <= int.Parse(value);
                        case "<":
                        {
                            return user.StudyPoints < int.Parse(value);
                        }

                        case ">=": return user.StudyPoints >= int.Parse(value);
                        case ">":
                        {
                            return user.StudyPoints > int.Parse(value);
                        }

                        case "==":
                        {
                            return user.StudyPoints == int.Parse(value);
                        }
                    }

                    return false;
                }

                #endregion

                #region family_rank

                case "family_rank":
                {
                    switch (opt)
                    {
                        case "<":
                            return (ushort) m_pUser.FamilyPosition < ushort.Parse(value);
                        case "==":
                            return (ushort) m_pUser.FamilyPosition == ushort.Parse(value);
                    }

                    return false;
                }

                #endregion

                #region transform

                case "transform":
                {
                    switch (opt)
                    {
                        case "==": return user.Transformation == ushort.Parse(value);
                    }

                    return false;
                }

                #endregion

                #region look

                case "look":
                {
                    switch (opt)
                    {
                        case "==": return user.Lookface % 10 == ushort.Parse(value);
                        case "set":
                        {
                            ushort usVal = ushort.Parse(value);
                            if (user.Gender == 1 && (usVal == 3 || usVal == 4))
                            {
                                user.Body = (ushort) (1000 + usVal);
                                return true;
                            }

                            if (user.Gender == 0 && (usVal == 1 || usVal == 2))
                            {
                                user.Body = (ushort) (2000 + usVal);
                                return true;
                            }

                            return false;
                        }
                    }

                    return false;
                }

                #endregion

                #region body

                case "body":
                {
                    switch (opt)
                    {
                        case "set":
                        {
                            ushort usNewBody = ushort.Parse(value);
                            if (usNewBody == 1003 || usNewBody == 1004)
                            {
                                if (user.Body != 2001 && user.Body != 2002)
                                    return false; // to change body use the fucking item , asshole

                                if (user.UserPackage[ItemPosition.Garment] != null)
                                    user.UserPackage.Unequip(ItemPosition.Garment);
                                if (user.UserPackage[ItemPosition.AltGarment] != null)
                                    user.UserPackage.Unequip(ItemPosition.AltGarment);

                                user.Body = usNewBody;
                                return true;
                            }

                            if (usNewBody == 2001 || usNewBody == 2002)
                            {
                                if (user.Body != 1003 && user.Body != 1004)
                                    return false; // to change body use the fucking item , asshole

                                if (user.UserPackage[ItemPosition.Garment] != null)
                                    user.UserPackage.Unequip(ItemPosition.Garment);
                                if (user.UserPackage[ItemPosition.AltGarment] != null)
                                    user.UserPackage.Unequip(ItemPosition.AltGarment);

                                user.Body = usNewBody;
                                return true;
                            }

                            return false;
                        }
                    }

                    return false;
                }

                #endregion

                #region storage_money

                case "storage_money":
                {
                    switch (opt)
                    {
                        case "+=":
                            return user.ChangeMoneySaved(int.Parse(value));
                        case "==":
                            return user.MoneySaved == uint.Parse(value);
                        case "<":
                            return user.MoneySaved < uint.Parse(value);
                    }

                    return false;
                }

                #endregion

                #region syn_age

                case "syn_age":
                {
                    if (user.Syndicate == null)
                        return false;
                    int nValue = int.Parse(value);
                    int nDays = (DateTime.Now - user.SyndicateMember.JoinDate).Days;
                    switch (opt)
                    {
                        case "==": return nDays == nValue;
                        case "!=": return nDays != nValue;
                        case "<=": return nDays <= nValue;
                        case ">=": return nDays >= nValue;
                        case "<": return nDays < nValue;
                        case ">": return nDays > nValue;
                    }

                    return false;
                }

                #endregion

                #region chi_points

                case "chi_points":
                {
                    switch (opt)
                    {
                        case "+=":
                        {
                            int nVal = int.Parse(value);
                            if (!string.IsNullOrEmpty(last) && last.ToLower() == "nolimit" || nVal <= 0)
                                return user.ChangeChiPoints(nVal);
                            if (nVal + user.ChiPoints > 4000)
                                nVal = (int) (user.ChiPoints - nVal);
                            return user.ChangeChiPoints(nVal);
                        }
                        case "<=": return user.ChiPoints <= int.Parse(value);
                        case "<":
                        {
                            return user.ChiPoints < int.Parse(value);
                        }

                        case ">=": return user.ChiPoints >= int.Parse(value);
                        case ">":
                        {
                            return user.ChiPoints > int.Parse(value);
                        }

                        case "==":
                        {
                            return user.ChiPoints == int.Parse(value);
                        }
                    }

                    return false;
                }

                #endregion

                #region gender (<=,<,>=,>,==)
                case "gender":
                {
                    switch (opt)
                    {
                        case "<=": return user.Gender <= int.Parse(value);
                        case "<":
                        {
                            return user.Gender < int.Parse(value);
                        }

                        case ">=": return user.Gender >= int.Parse(value);
                        case ">":
                        {
                            return user.Gender > int.Parse(value);
                        }

                        case "==":
                        {
                            return user.Gender == int.Parse(value);
                        }
                    }

                    return false;
                }
                #endregion

                #region jiang_stage (<=,<,>=,>,==)
                case "jiang_stage":
                {
                    switch (opt)
                    {
                        case "<=": return user.CurrentJiangHuStage <= int.Parse(value);
                        case "<":
                        {
                            return user.CurrentJiangHuStage < int.Parse(value);
                        }

                        case ">=": return user.CurrentJiangHuStage >= int.Parse(value);
                        case ">":
                        {
                            return user.CurrentJiangHuStage > int.Parse(value);
                        }

                        case "==":
                        {
                            return user.CurrentJiangHuStage == int.Parse(value);
                        }
                    }

                    return false;
                }
                #endregion

                default:
                    Program.WriteLog("[1001] " + param + " not handled.", LogType.WARNING, false);
                    return false;
            }
        }

        #endregion

        #region 1002 - UserFillAttr

        private bool UserFillAttr1002(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            if (user == null)
                return false;

            switch (param)
            {
                case "life":
                    user.Life = user.MaxLife;
                    return true;
                case "mana":
                    user.Mana = user.MaxMana;
                    return true;
                case "xp":
                    user.SetXp(100);
                    user.BurstXp();
                    return true;
            }

            return false;
        }

        #endregion

        #region 1003 - UserChgmap

        private bool UserChgmap1003(GameActionEntity action, string param, Character user, ScreenObject role, Item item,
            string pszAccept)
        {
            if (user == null)
                return false;

            string[] Params = GetSafeParam(param);
            uint map = uint.Parse(Params[0]);
            ushort x = ushort.Parse(Params[1]);
            ushort y = ushort.Parse(Params[2]);
            if (map == 0 || x == 0 || y == 0)
                return false;
            user.FlyMap(map, x, y);
            return true;
        }

        #endregion

        #region 1004 - UserSaveLocation

        private bool UserSaveLocation1004(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            if (user == null)
                return false;

            string[] Params = GetSafeParam(param);
            uint Map = uint.Parse(Params[0]);
            ushort X = ushort.Parse(Params[1]);
            ushort Y = ushort.Parse(Params[2]);

            if (Map == 0)
            {
                user.RecordMapIdentity = user.MapIdentity;
                user.RecordMapX = user.MapX;
                user.RecordMapY = user.MapY;
                return true;
            }

            if (X == 0 || Y == 0)
                return false;

            user.RecordMapIdentity = Map;
            user.RecordMapX = X;
            user.RecordMapY = Y;
            user.SetRecordPos(Map, X, Y);
            return true;
        }

        #endregion

        #region 1005 - UserHair

        private bool ActionUserHair1005(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            if (user == null) return false;

            string[] pszParam = GetSafeParam(param);

            if (pszParam.Length < 2) return false;

            switch (pszParam[0])
            {
                case "style":
                {
                    if ((int) user.Profession / 10 == 6 && user.Gender == 1)
                    {
                        user.Hair = 0;
                        return false;
                    }

                    user.Hair = (ushort) (ushort.Parse(pszParam[1]) + (user.Hair - user.Hair % 100));
                    break;
                }

                case "color":
                {
                    user.Hair = (ushort) (user.Hair % 100 + ushort.Parse(pszParam[1]) * 100);
                    break;
                }

                default:
                    return false;
            }

            return true;
        }

        #endregion

        #region 1006 - UserChgmapRecord

        private bool UserChgmapRecord1006(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            if (user == null)
                return false;

            if (user.RecordMapIdentity == 0)
            {
                user.FlyMap(1002, 430, 380);
            }
            else
            {
                user.FlyMap(user.RecordMapIdentity, user.RecordMapX, user.RecordMapY);
            }

            return true;
        }

        #endregion

        #region 1008 - UserTransform

        private bool UserTransform1008(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 4)
                return false;

            int unknown0 = int.Parse(pszParam[0]);
            int unknown1 = int.Parse(pszParam[1]);
            uint transform = uint.Parse(pszParam[2]);
            int time = int.Parse(pszParam[3]);

            return user.Transform(transform, time, true);
        }

        #endregion

        #region 1009 - UserPureClass

        private bool UserPureClass1009(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            return user != null && user.IsPureClass();
        }

        #endregion

        #region 1010 - UserTalk

        private bool UserTalk1010(GameActionEntity action, string param, Character user, ScreenObject role, Item item,
            string pszAccept)
        {
            if (user == null)
                return false;

//            if (!Enum.IsDefined(typeof(ChatTone), (ushort) action.Data))
//            {
//                action.Data = (uint) ChatTone.Talk;
//#if DEBUG
//                Program.WriteLog(
//                    $"Action ({action.Identity}) of type UserTalk(1010) has an invalid data value ({action.Data})");
//#endif
//            }

            user.Send(new MsgTalk(param, (ChatTone) action.Data));
            return true;
        }

        #endregion

        #region 1020 - UserMagic

        private bool UserMagic1020(GameActionEntity action, string param, Character user, ScreenObject role, Item item,
            string pszAccept)
        {
            if (user?.Magics == null)
                return false;

            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 2)
            {
                Program.WriteLog($"ACTION[{action.Identity}]: Type 1020 not enough params, need 2");
                return false;
            }

            switch (pszParam[0].ToLower())
            {
                case "check":
                    if (pszParam.Length == 3)
                        return user.Magics.CheckLevel(ushort.Parse(pszParam[1]), ushort.Parse(pszParam[2]));
                    return user.Magics.CheckType(ushort.Parse(pszParam[1]));
                case "learn":
                    if (pszParam.Length == 3)
                        return user.Magics.Create(ushort.Parse(pszParam[1]), byte.Parse(pszParam[2]));
                    return user.Magics.Create(ushort.Parse(pszParam[1]), 0);
                case "uplevel":
                    return user.Magics.UpLevelByTask(ushort.Parse(pszParam[1]));
                case "addexp":
                    return user.Magics.AwardExp(ushort.Parse(pszParam[1]), 0, int.Parse(pszParam[2]));
                default:
                    Program.WriteLog("Unhandled type on 1020:" + pszParam[0], LogType.WARNING);
                    return false;
            }
        }

        #endregion

        #region 1021 - UserWeaponSkill

        private bool UserWeaponSkill1021(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user == null)
                return false;

            ushort nType = 0;
            long nValue = 0;
            string[] safeParam = GetSafeParam(param);

            if (safeParam.Length < 3)
                return false;

            nType = ushort.Parse(safeParam[1]);
            nValue = long.Parse(safeParam[2]);

            switch (safeParam[0])
            {
                case "check":
                    WeaponSkillEntity pSkill;
                    if (user.WeaponSkill.Skills.TryGetValue(nType, out pSkill))
                    {
                        return pSkill.Level >= nValue;
                    }

                    break;
                case "learn":
                    return user.WeaponSkill.Create(nType, (byte) nValue);
                case "addexp":
                {
                    return user.WeaponSkill.AwardExperience(nType, (int) nValue);
                }
            }

            return false;
        }

        #endregion

        #region 1022 - ActionUserLog

        private bool ActionUserLog1022(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            char[] ar = {' '};
            string[] pszParam = param.Split(ar, 2);

            if (pszParam.Length < 2)
            {
                Program.WriteLog($"Coult not write to GMLog ({action.Identity}:{param})");
                return false;
            }

            if (pszParam[0].StartsWith("gmlog/"))
                pszParam[0] = pszParam[0].Remove(0, "gmlog/".Length);

            ServerKernel.Log.GmLog(pszParam[0], pszParam[1]);
            return true;
        }

        #endregion

        #region 1023 - ActionUserBonus

        private bool ActionUserBonus1023(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            return user.DoBonus();
        }

        #endregion

        #region 1024 - ActionUserDivorce

        private bool ActionUserDivorce1024(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            if (user == null) return false;

            if (user.Mate == Language.StrNone) return false;

            Character pMate = ServerKernel.UserManager.GetUser(user.Mate);
            if (pMate == null)
            {
                CharacterEntity dbMate = new CharacterRepository().SearchByName(m_pUser.Mate);
                if (dbMate != null)
                {
                    dbMate.Mate = Language.StrNone;
                    new CharacterRepository().Save(dbMate);
                    return true;
                }

                return false;
            }

            pMate.Mate = Language.StrNone;
            m_pUser.Mate = Language.StrNone;
            return true;
        }

        #endregion

        #region 1025 - ActionUserMarriage

        private bool ActionUserMarriage1025(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            if (user == null) return false;
            return user.Mate != Language.StrNone;
        }

        #endregion

        #region 1026 - ActionUserSex

        private bool ActionUserSex1026(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            if (user == null) return false;
            return user.Gender == 1;
        }

        #endregion

        #region 1027 - ActionUserEffect

        private bool ActionUserEffect1027(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            if (user == null)
                return false;

            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 2) return false;

            switch (pszParam[0])
            {
                case "self":
                    MsgName msg1 = new MsgName
                    {
                        Action = StringAction.RoleEffect,
                        Identity = user.Identity
                    };
                    msg1.Append(pszParam[1]);
                    user.Screen.Send(msg1, true);
                    return true;
                case "couple":
                    if (m_pUser.Mate == Language.StrNone)
                    {
                        return false;
                    }

                    Character mate = ServerKernel.UserManager.GetUser(user.Mate);
                    var msg0 = new MsgName
                    {
                        Action = StringAction.RoleEffect,
                        Identity = user.Identity
                    };
                    msg0.Append(pszParam[1]);
                    user.Screen.Send(msg0, true);
                    msg0.Identity = mate.Identity;
                    mate.Screen.Send(msg0, true);
                    return true;
                case "team":
                    //if (m_pUser.Team == null)
                    //    return false;
                    //foreach (var member in m_pUser.Team.Members.Values)
                    //{
                    //    var msg = new MsgName
                    //    {
                    //        Action = StringAction.ROLE_EFFECT,
                    //        Identity = member.Identity
                    //    };
                    //    msg.Append(pszParam[1]);
                    //    member.Owner.Screen.Send(msg, true);
                    //}
                    return false; // todo implement teams
            }

            return false;
        }

        #endregion

        #region 1029 - ActionUserMediaPlay

        private bool ActionUserMediaPlay1029(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            if (user == null)
                return false;

            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 2)
                return false;

            var msg = new MsgName {Action = StringAction.PlayerWave};
            msg.Append(pszParam[1]);

            switch (pszParam[0])
            {
                case "play":
                    user.Send(msg);
                    return true;
                case "broadcast":
                    user.Screen.Send(msg, true);
                    return true;
            }

            return false;
        }

        #endregion

        #region 1031 - ActionUserAddTitle

        /// <summary>
        ///     This type method will add a new title to the user titles table. It wont activate,
        ///     it will set the title and endtime according to the user input. If the time is set
        ///     0, it will add 1 week seconds.
        /// </summary>
        /// <param name="param">Param is: title time | And time cannot be null, should be 0 or higher.</param>
        private bool ActionUserAddTitle1031(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param); //not yet in version 5103
            if (pszParam.Length < 2)
                return false;

            byte title = byte.Parse(pszParam[0]);
            uint timestamp = uint.Parse(pszParam[1]);

            if (timestamp <= 0)
                timestamp = 604800;

            timestamp += (uint) UnixTimestamp.Now();

            var newTitle = new TitleEntity
            {
                Timestamp = timestamp,
                Title = title,
                Userid = user.Identity
            };

            new TitleRepository().Save(newTitle);

            var tPacket = new MsgTitle
            {
                Identity = user.Identity, Action = TitleAction.AddTitle, SelectedTitle = (UserTitle) title
            };
            m_pUser.Send(tPacket);

            if (user.Title <= 0)
            {
                user.Title = (UserTitle) title;
                user.Screen.RefreshSpawnForObservers();
            }

            return user.Titles.TryAdd((UserTitle) title, newTitle);
        }

        #endregion

        #region 1032 - ActionUserRemoveTitle

        private bool ActionUserRemoveTitle1032(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (action.Data == 0)
                return true;
            if (user.Titles.ContainsKey((UserTitle) action.Data))
            {
                var tPacket = new MsgTitle();
                tPacket.Identity = user.Identity;
                tPacket.Action = TitleAction.RemoveTitle;
                tPacket.SelectedTitle = (UserTitle) action.Data;
                user.Send(tPacket);
                TitleEntity trash;
                user.Titles.TryRemove((UserTitle) action.Data, out trash);
                return new TitleRepository().Delete(trash);
            }

            return false;
        }

        #endregion

        #region 1033 - ActionUserCreateMap

        private bool ActionUserCreateMap1033(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user == null)
                return false;

            string[] safeParam = GetSafeParam(param);

            if (safeParam.Length < 10)
            {
                ServerKernel.Log.SaveLog($"ERROR: action ({action.Identity}) with invalid param length");
                return false;
            }

            string szName = safeParam[0];
            uint idOwner = uint.Parse(safeParam[2]),
                idRebornMap = uint.Parse(safeParam[7]);
            byte nOwnerType = byte.Parse(safeParam[1]);
            uint nMapDoc = uint.Parse(safeParam[3]);
            uint nType = uint.Parse(safeParam[4]);
            uint nRebornPortal = uint.Parse(safeParam[8]);
            byte nResLev = byte.Parse(safeParam[9]);
            ushort usPortalX = ushort.Parse(safeParam[5]),
                usPortalY = ushort.Parse(safeParam[6]);

            DynamicMapEntity pMapInfo = new DynamicMapEntity
            {
                Name = szName,
                OwnerId = idOwner,
                OwnerType = nOwnerType,
                Description = $"{user.Name}`{szName}",
                RebornMapid = idRebornMap,
                Portal0X = usPortalX,
                Portal0Y = usPortalY,
                LinkMap = user.MapIdentity,
                LinkX = user.MapX,
                LinkY = user.MapY,
                MapDoc = nMapDoc,
                Type = nType,
                RebornPortal = nRebornPortal,
                ResLev = nResLev
            };

            string path = Map.FindMapPath(nMapDoc);

            if (string.IsNullOrEmpty(path))
                return false;

            pMapInfo.FileName = path;

            if (!new DynamicMapRepository().Save(pMapInfo) || pMapInfo.Identity < 1000000)
            {
                ServerKernel.Log.SaveLog($"ERROR: Could not save dynamic map info action {action.Identity}");
                return false;
            }

            user.SetHomeId(pMapInfo.Identity);

            Map pNewMap = new Map(pMapInfo);
            return ServerKernel.Maps.TryAdd(pNewMap.Identity, pNewMap);
        }

        #endregion

        #region 1034 - ActionUserEnterHome

        private bool ActionUserEnterHome1034(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user == null)
                return false;

            if (user.HomeIdentity == 0)
                return false;

            if (!ServerKernel.Maps.TryGetValue(user.HomeIdentity, out var pHome)) return false;

            uint idOldMap = user.MapIdentity;
            ushort usOldX = user.MapX;
            ushort usOldY = user.MapY;

            user.FlyMap(pHome.Identity, pHome.PortalX, pHome.PortalY);

            if (user.Team != null && m_pUser.Team.Leader == user)
            {
                foreach (var member in m_pUser.Team.Members.Values)
                {
                    if (member.Identity == user.Identity || member.GetDistance(user.MapX, user.MapY) > 5)
                        continue;
                    member.FlyMap(pHome.Identity, pHome.PortalX, pHome.PortalY);
                }
            }

            return true;
        }

        #endregion

        #region 1035 - ActionUserEnterMateHome

        private bool ActionUserEnterMateHome1035(GameActionEntity action, string param, Character user,
            ScreenObject role,
            Item item, string pszAccept)
        {
            if (user == null)
                return false;

            uint idHome = user.HomeMateIdentity;
            if (idHome == 0)
                return false;

            if (!ServerKernel.Maps.TryGetValue(idHome, out var pHome))
                return false;

            user.FlyMap(pHome.Identity, pHome.PortalX, pHome.PortalY);
            return true;
        }

        #endregion

        #region 1040 - ActionUserRebirth

        private bool ActionUserRebirth1040(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user == null)
            {
                Program.WriteLog($"ACTION {action.Identity}: no user.", LogType.ERROR);
                return false;
            }

            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 2)
            {
                Program.WriteLog($"ACTION {action.Identity}: invalid param num", LogType.ERROR);
                return false;
            }

            ushort nProf = ushort.Parse(pszParam[0]);
            ushort nLook = ushort.Parse(pszParam[1]);
            if (!user.Rebirth(nProf, nLook)) return false;

            ServerKernel.Log.GmLog("rebirth",
                $"User[{user.Name}][{user.Identity}], prof[{user.Profession}], level[{user.Level}], rebirth to prof[{nProf}] look[{nLook}]");
            return true;
        }

        #endregion

        #region 1041 - ActionUserWebPage

        private bool ActionUserWebPage1041(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user == null)
            {
                Program.WriteLog($"ACTION {action.Identity}: no user.", LogType.ERROR);
                return false;
            }

            user.SendMessage(param, ChatTone.Website);
            return true;
        }

        #endregion

        #region 1042 - ActionUserBbs

        private bool ActionUserBbs1042(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user == null)
            {
                Program.WriteLog($"ACTION {action.Identity}: user not set", LogType.ERROR);
                return false;
            }

            user.SendMessage(param, (ChatTone) 2206);
            return true;
        }

        #endregion

        #region 1045 - ActionUserFixAttr

        private bool ActionUserFixAttr1045(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user == null) return false;

            if (user.Metempsychosis < 1)
            {
                user.SendSysMessage(Language.StrNoReborn);
                return false;
            }

            user.FixAttributes();
            return true;
        }

        #endregion

        #region 1046 - ActionUserOpenDialog

        private bool ActionUserOpenDialog1046(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user == null)
                return false;

            switch ((OpenWindow) action.Data)
            {
                case OpenWindow.VIP_WAREHOUSE:
                {
                    if (user.VipLevel == 0)
                        return false;
                    break;
                }
            }

            user.Send(new MsgAction(user.Identity, action.Data, user.MapX, user.MapY, GeneralActionType.OpenWindow));
            return true;
        }

        #endregion

        #region 1047 - ActionUserPointAllot

        private bool ActionUserPointAllot1047(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user == null) return false;
            user.ResetAttrPoints();
            return true;
        }

        #endregion

        #region 1048 - ActionUserExpMultiply

        private bool ActionUserExpMultiply1048(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 2)
                return false;

            uint time = uint.Parse(pszParam[1]);
            float multiply = int.Parse(pszParam[0]) / 100f;
            user.SetExperienceMultiplier(time, multiply);
            return true;
        }

        #endregion

        #region 1052 - ActionUserWhPassword

        private bool ActionUserWhPassword1052(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (user == null)
                return false;

            if (user.WarehousePassword == 0 || user.IsUnlocked())
                return true;

            if (string.IsNullOrEmpty(pszAccept))
                return false;

            if (pszAccept.Length < 4 || pszAccept.Length > ulong.MaxValue.ToString().Length)
                return false;

            if (!ulong.TryParse(pszAccept, out var dwPassword))
                return false;

            return user.WarehousePassword == dwPassword;
        }

        #endregion

        #region 1053 - ActionUserSetWhPassword

        private bool ActionUserSetWhPassword1053(GameActionEntity action, string param, Character user,
            ScreenObject role,
            Item item, string pszAccept)
        {
            if (user == null || pszAccept == null || pszAccept == string.Empty)
                return false;

            if (pszAccept.Length < 4 && pszAccept != "0" || pszAccept.Length > ulong.MaxValue.ToString().Length)
            {
                user.SendMessage($"Your password must be between 1000 and {ulong.MaxValue}.");
                return false;
            }

            if (!ulong.TryParse(pszAccept, out ulong dwPass))
                return false;

            user.WarehousePassword = dwPass;
            return true;
        }

        #endregion

        #region 1054 - ActionUserOpenInterface

        private bool ActionUserOpenInterface1054(GameActionEntity action, string param, Character user,
            ScreenObject role,
            Item item, string pszAccept)
        {
            if (user == null)
                return false;

            user.Send(new MsgAction(user.Identity, action.Data, user.MapX, user.MapY, GeneralActionType.OpenCustom));
            return true;
        }

        #endregion

        #region 1060 - ActionUserVarCompare

        private bool ActionUserVarCompare1060(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 3)
                return false;

            byte varId = VarId(pszParam[0]);
            string opt = pszParam[1];
            long value = long.Parse(pszParam[2]);

            switch (opt)
            {
                case "==":
                    return user.Data[varId] == value;
                case ">=":
                    return user.Data[varId] >= value;
                case "<=":
                    return user.Data[varId] <= value;
                case ">":
                    return user.Data[varId] > value;
                case "<":
                    return user.Data[varId] < value;
                case "!=":
                    return user.Data[varId] != value;
                default:
                    return false;
            }
        }

        #endregion

        #region 1061 - ActionUserVarDefine

        private bool ActionUserVarDefine1061(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] safeParam = GetSafeParam(param);
            if (safeParam.Length < 3)
                return false;

            byte varId = VarId(safeParam[0]);
            string opt = safeParam[1];
            long value = long.Parse(safeParam[2]);

            try
            {
                switch (opt)
                {
                    case "set":
                        user.Data[varId] = value;
                        return true;
                }
            }
            catch
            {
                return false;
            }

            return false;
        }

        #endregion

        #region 1064 - ActionUserVarCalc

        private bool ActionUserVarCalc1064(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] safeParam = GetSafeParam(param);
            if (safeParam.Length < 3)
                return false;

            byte varId = VarId(safeParam[0]);
            string opt = safeParam[1];
            long value = long.Parse(safeParam[2]);

            if (opt == "/=" && value == 0)
                return false; // division by zero

            switch (opt)
            {
                case "+=":
                    user.Data[varId] += value;
                    return true;
                case "-=":
                    user.Data[varId] -= value;
                    return true;
                case "*=":
                    user.Data[varId] *= value;
                    return true;
                case "/=":
                    user.Data[varId] /= value;
                    return true;
                case "mod=":
                    user.Data[varId] %= value;
                    return true;
                default:
                    return false;
            }
        }

        #endregion

        #region 1073 - ActionUserStcCompare

        private bool ActionUserStcCompare1073(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 3)
                return false;

            string szStc = GetParenthesys(pszParam[0]);
            string opt = pszParam[1];
            long value = long.Parse(pszParam[2]);

            string[] pStc = szStc.Trim().Split(',');

            if (pStc.Length < 2)
                return false;

            uint idEvent = uint.Parse(pStc[0]);
            uint idType = uint.Parse(pStc[1]);

            StatisticEntity dbStc = user.Statistics.GetStc(idEvent, idType);
            if (dbStc == null)
                return false;

            switch (opt)
            {
                case ">=":
                    return dbStc.Data >= value;
                case "<=":
                    return dbStc.Data <= value;
                case ">":
                    return dbStc.Data > value;
                case "<":
                    return dbStc.Data < value;
                case "!=":
                case "<>":
                    return dbStc.Data != value;
                case "==":
                    return dbStc.Data == value;
            }

            return false;
        }

        #endregion

        #region 1074 - ActionUserStrOpe

        private bool ActionUserStrOpe1074(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 3)
                return false;

            string szStc = GetParenthesys(pszParam[0]);
            string opt = pszParam[1];
            long value = long.Parse(pszParam[2]);
            bool bUpdate = pszParam[3] != "0";

            string[] pStc = szStc.Trim().Split(',');

            if (pStc.Length < 2)
                return false;

            uint idEvent = uint.Parse(pStc[0]);
            uint idType = uint.Parse(pStc[1]);

            if (!user.Statistics.HasEvent(idEvent, idType))
            {
                return user.Statistics.AddOrUpdate(idEvent, idType, (uint) value, bUpdate);
            }

            switch (opt)
            {
                case "+=":
                    if (value == 0) return false;

                    long tempValue = user.Statistics.GetValue(idEvent, idType) + value;
                    return user.Statistics.AddOrUpdate(idEvent, idType, (uint) Math.Max(0, tempValue), bUpdate);
                case "=":
                    if (value < 0) return false;
                    return user.Statistics.AddOrUpdate(idEvent, idType, (uint) Math.Max(0, value), bUpdate);
            }

            return false;
        }

        #endregion

        #region 1080 - ActionUserTaskManager

        private bool ActionUserTaskManager1080(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 3)
                return false;

            string szStc = GetParenthesys(pszParam[0]);
            string opt = pszParam[1];
            long value = long.Parse(pszParam[2]);

            string[] pStc = szStc.Trim().Split(',');

            if (pStc.Length <= 2)
                return false;

            uint idEvent = uint.Parse(pStc[0]);
            uint idType = uint.Parse(pStc[1]);
            ulong idx = idEvent + (idType << 32);
            byte mode = byte.Parse(pStc[2]);

            if (value < 0)
                return false;

            StatisticEntity dbStc = user.Statistics.GetStc(idEvent, idType);
            if (dbStc == null)
                return true;

            if (dbStc.Timestamp == 0)
                return true;

            switch (mode)
            {
                case 0: // seconds
                {
                    int timeStamp = UnixTimestamp.Now();
                    int nDiff = (int) (timeStamp - dbStc.Timestamp + value);
                    switch (opt)
                    {
                        case "==": return nDiff == value;
                        case "<": return nDiff < value;
                        case ">": return nDiff > value;
                        case "<=": return nDiff <= value;
                        case ">=": return nDiff >= value;
                        case "<>":
                        case "!=": return nDiff != value;
                    }

                    return false;
                }

                case 1: // days
                    int interval = int.Parse(DateTime.Now.ToString("yyyyMMdd")) -
                                   int.Parse(UnixTimestamp.ToDateTime(dbStc.Timestamp).ToString("yyyyMMdd"));
                    switch (opt)
                    {
                        case "==": return interval == value;
                        case "<": return interval < value;
                        case ">": return interval > value;
                        case "<=": return interval <= value;
                        case ">=": return interval >= value;
                        case "!=":
                        case "<>": return interval != value;
                    }

                    return false;
                default:
                    Program.WriteLog($"Unhandled Time mode ({mode}) on action (id:{action.Identity})", LogType.WARNING);
                    return false;
            }
        }

        #endregion

        #region 1081 - ActionUserTaskOpe

        private bool ActionUserTaskOpe1081(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 3)
                return false;

            string szStc = GetParenthesys(pszParam[0]);
            string opt = pszParam[1];
            long value = long.Parse(pszParam[2]);

            string[] pStc = szStc.Trim().Split(',');

            uint idEvent = uint.Parse(pStc[0]);
            uint idType = uint.Parse(pStc[1]);

            switch (opt)
            {
                case "set":
                {
                    if (value > 0)
                        return user.Statistics.SetTimestamp(idEvent, idType, (uint) value);
                    return user.Statistics.SetTimestamp(idEvent, idType, (uint) UnixTimestamp.Now());
                }
            }

            return false;
        }

        #endregion

        #region 1082 - ActionUserAttachStatus

        private bool ActionUserAttachStatus1082(GameActionEntity action, string param, Character user,
            ScreenObject role,
            Item item, string pszAccept)
        {
            // self add 64 200 900 0
            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 6)
                return false;

            string target = pszParam[0].ToLower();
            string opt = pszParam[1].ToLower();
            int status = StatusSet.InvertFlag(ulong.Parse(pszParam[2]));
            int multiply = int.Parse(pszParam[3]);
            uint seconds = uint.Parse(pszParam[4]);
            int times = int.Parse(pszParam[5]);
            // last param unknown

            //if (target == "team" && m_pUser.Team == null)
            //    return false;

            if (target == "self")
            {
                user.AttachStatus(user, status, multiply, (int) seconds, times, 0);
                return true;
            }

            //if (target == "team")
            //{
            //    foreach (var member in m_pUser.Team.Members.Values)
            //        member.AttachStatus(member, status, multiply, (int)seconds, times, 0, m_pUser.Identity);
            //    return true;
            //}
            if (target == "couple")
            {
                Character mate = ServerKernel.UserManager.GetUser(user.Mate);
                if (mate == null)
                    return false;
                user.AttachStatus(user, status, multiply, (int) seconds, times, 0);
                mate.AttachStatus(user, status, multiply, (int) seconds, times, 0);
            }

            return false;
        }

        #endregion

        #region 1083 - ActionUserGodTime

        private bool ActionUserGodTime1083(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParma = GetSafeParam(param);

            if (pszParma.Length < 2)
                return false;

            string opt = pszParma[0];
            uint minutes = uint.Parse(pszParma[1]);

            switch (opt)
            {
                case "+=":
                {
                    return m_pUser.AddBlessing(minutes);
                }
            }

            return true;
        }

        #endregion

        #region 1086 - ActionUserExpBallExp

        private bool ActionUserExpBallExp1086(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);

            if (pszParam.Length < 2)
                return false;

            int dwExpTimes = int.Parse(pszParam[0]);
            byte idData = byte.Parse(pszParam[1]);

            if (idData >= m_pUser.Data.Length) return false;

            long exp = user.CalculateExpBall(dwExpTimes);
            m_pUser.Data[idData] = exp;
            return true;
        }

        #endregion

        #region 1096 - ActionUserStatusCreate

        private bool ActionUserStatusCreate1096(GameActionEntity action, string param, Character user,
            ScreenObject role,
            Item item, string pszAccept)
        {
            // sort leave_times remain_time end_time interval_time
            // 200 0 604800 0 604800 1
            if (action.Data == 0)
            {
                Program.WriteLog("ERROR: invalid data num " + action.Identity, LogType.ERROR);
                return false;
            }

            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 5)
            {
                Program.WriteLog("ERROR: invalid param num " + action.Identity, LogType.ERROR);
                return false;
            }

            uint sort = uint.Parse(pszParam[0]);
            uint leaveTimes = uint.Parse(pszParam[1]);
            uint remainTime = uint.Parse(pszParam[2]);
            uint intervalTime = uint.Parse(pszParam[4]);
            bool unknown = pszParam[5] != "0"; // ??

            var status = new StatusRepository().FindStatus(user.Identity, action.Data);
            if (status != null)
            {
                status.EndTime = (uint) UnixTimestamp.Now() + remainTime;
                status.IntervalTime = intervalTime;
                status.LeaveTimes = leaveTimes;
                status.RemainTime = remainTime;
                status.Sort = sort;
                if (!new StatusRepository().Save(status))
                {
                    Program.WriteLog(
                        string.Format("ERROR: Could not update status {0}[{2}] to {1}", status.Id, action.Data,
                            status.Status), LogType.ERROR);
                    return false;
                }
            }
            else
            {
                var query = new StatusEntity
                {
                    EndTime = (uint) UnixTimestamp.Now() + remainTime,
                    IntervalTime = intervalTime,
                    LeaveTimes = leaveTimes,
                    OwnerId = user.Identity,
                    Power = 0,
                    RemainTime = remainTime,
                    Status = action.Data,
                    Sort = sort
                };
                if (!new StatusRepository().Save(query))
                {
                    Program.WriteLog("ERROR: Could not save status", LogType.ERROR);
                    return false;
                }
            }

            user.AttachStatus(user, (int) action.Data, 0, (int) remainTime, (int) leaveTimes, 0);
            return true;
        }

        #endregion

        #region 1098 - ActionUserStatusCheck

        private bool ActionUserStatusCheck1098(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            if (m_pUser.Status == null) return false;

            string[] status = GetSafeParam(action.Param);

            switch (action.Data)
            {
                case 0: // check
                    foreach (var st in status)
                        if (m_pUser.QueryStatus(int.Parse(st)) == null)
                            return false;
                    return true;
                case 1:
                    foreach (var st in status)
                        if (m_pUser.QueryStatus(int.Parse(st)) != null)
                        {
                            m_pUser.DetachStatus(int.Parse(st));
                            StatusEntity db = new StatusRepository().FindStatus(m_pUser.Identity, uint.Parse(st));
                            if (db != null)
                                new StatusRepository().Delete(db);
                        }

                    return true;
            }

            return false;
        }

        #endregion

        #endregion

        #region 1100 - Team

        #region 1101 - ActionTeamBroadcast

        private bool ActionTeamBroadcast1101(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            if (user.Team == null || user.Team.Leader != m_pUser || user.Team.MembersCount() <= 1)
                return false;
            foreach (var member in user.Team.Members.Values)
                member.Send(new MsgTalk(param, ChatTone.Team, Color.White));
            return true;
        }

        #endregion

        #region 1102 - ActionTeamAttr

        private bool ActionTeamAttr1102(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            if (user.Team == null)
                return false;

            string[] safeParam = GetSafeParam(param);

            if (safeParam.Length < 3)
                return false;

            string szParam = safeParam[0];
            string szOpt = safeParam[1];
            int nValue = int.Parse(safeParam[2]);

            switch (szParam)
            {
                case "count":
                {
                    switch (szOpt)
                    {
                        case "<":
                            return user.Team.MembersCount() < nValue;
                        case ">":
                            return user.Team.MembersCount() > nValue;
                        case "<=":
                            return user.Team.MembersCount() <= nValue;
                        case ">=":
                            return user.Team.MembersCount() >= nValue;
                        case "==":
                            return user.Team.MembersCount() == nValue;
                        case "!=":
                        case "<>":
                            return user.Team.MembersCount() != nValue;
                    }

                    break;
                }
                case "money":
                {
                    foreach (var member in user.Team.Members.Values)
                    {
                        switch (szOpt)
                        {
                            case "+=":
                                member.AwardMoney(nValue);
                                return true;
                            case ">=":
                                if (member.Silver <= nValue)
                                    return false;
                                break;
                            case "<=":
                                if (member.Silver >= nValue)
                                    return false;
                                break;
                            case ">":
                                if (member.Silver < nValue)
                                    return false;
                                break;
                            case "<":
                                if (member.Silver > nValue)
                                    return false;
                                break;
                            case "==":
                                if (member.Silver != nValue)
                                    return false;
                                break;
                        }
                    }

                    return true;
                }
                case "level":
                {
                    foreach (var member in user.Team.Members.Values)
                    {
                        switch (szOpt)
                        {
                            case ">=":
                                if (member.Level <= nValue)
                                    return false;
                                break;
                            case "<=":
                                if (member.Level >= nValue)
                                    return false;
                                break;
                            case ">":
                                if (member.Level < nValue)
                                    return false;
                                break;
                            case "<":
                                if (member.Level > nValue)
                                    return false;
                                break;
                            case "==":
                                if (member.Level != nValue)
                                    return false;
                                break;
                        }
                    }

                    return true;
                }
                case "mate":
                {
                    foreach (var member in user.Team.Members.Values)
                        if (member.Name != user.Mate)
                            return false;

                    return true;
                }
                case "friend":
                {
                    foreach (var member in user.Team.Members.Values)
                        if (!user.ContainsFriend(member.Identity))
                            return false;
                    return true;
                }
                case "count_near":
                {
                    foreach (var member in user.Team.Members.Values)
                        if (!Calculations.InScreen(user.MapX, user.MapY, member.MapX, member.MapY))
                            return false;
                    return true;
                }
                case "study_points":
                {
                    foreach (var member in user.Team.Members.Values)
                    {
                        member.ChangeStudyPoints(nValue);
                    }

                    return true;
                }
            }

            return false;
        }

        #endregion

        #region 1103 - ActionTeamLeaveSpace

        private bool ActionTeamLeaveSpace1103(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            string[] safeParam = GetSafeParam(param);
            if (safeParam.Length < 1)
                return false;
            int nSpace = int.Parse(safeParam[0]);
            foreach (var member in user.Team.Members.Values)
                if (!member.UserPackage.IsPackSpare(nSpace))
                    return false;
            return true;
        }

        #endregion

        #region 1104 - ActionTeamItemAdd

        private bool ActionTeamItemAdd1104(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            foreach (var member in user.Team.Members.Values)
                member.UserPackage.AwardItem(Item.CreateEntity(action.Data));
            return true;
        }

        #endregion

        #region 1105 - ActionTeamItemDel

        private bool ActionTeamItemDel1105(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            foreach (var member in user.Team.Members.Values)
                member.UserPackage.SpendItem(action.Data);
            return true;
        }

        #endregion

        #region 1106 - ActionTeamItemCheck

        private bool ActionTeamItemCheck1106(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            foreach (var member in user.Team.Members.Values)
                if (!member.UserPackage.MultiCheckItem(action.Data, action.Data, 1))
                    return false;
            return true;
        }

        #endregion

        #region 1107 - ActionTeamChgMap

        private bool ActionTeamChgMap1107(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            string[] safeParam = GetSafeParam(param);

            if (safeParam.Length < 3)
                return false;

            if (user.Team == null) return false;

            uint idMap = uint.Parse(safeParam[0]);
            ushort x = ushort.Parse(safeParam[1]);
            ushort y = ushort.Parse(safeParam[2]);

            if (!ServerKernel.Maps.ContainsKey(idMap))
                return false;

            foreach (var member in m_pUser.Team.Members.Values)
            {
                if (user.MapIdentity == member.MapIdentity &&
                    Calculations.GetDistance(m_pUser.MapX, m_pUser.MapY, member.MapX, member.MapY) <=
                    Calculations.SCREEN_DISTANCE)
                {
                    member.FlyMap(idMap, x, y);
                }
            }

            return true;
        }

        #endregion

        #region 1108 - ActipmTeamChkIsLeader

        private bool ActipmTeamChkIsLeader1108(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            return user.Team.Leader == user;
        }

        #endregion

        #endregion

        #region 1500 - Special

        #region 1508 - ActionSpecialLottery

        private bool ActionSpecialLottery1508(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item,
            string pszAccept)
        {
            if (user == null) return false;

            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 1) return false;

            if (user.Statistics.GetValue(22) >= Lottery.MAX_ATTEMPTS)
            {
                return false;
            }

            user.LotteryLastType = byte.Parse(pszParam[0]);
            user.LotteryLastColor = (byte) action.Data;

            ItemEntity dbItem = Lottery.GenerateItem(user, true);
            return dbItem != null;
        }

        #endregion

        #region 1509 - ActionGeneralSubclassManagement

        private bool ActionGeneralSubclassManagement1509(GameActionEntity action, string param, Character user,
            ScreenObject role, Item item,
            string pszAccept)
        {
            if (user == null) return false;

            if (user.Level < 70 && user.Metempsychosis < 1)
            {
                user.SendSysMessage(Language.StrSubProLowLevel, Color.White);
                return false;
            }

            string[] values = GetSafeParam(param);

            if (values.Length < 2) return false;
            string opt = values[0];
            SubClasses subpro = (SubClasses) int.Parse(values[1]);
            int value = 0;

            if (values.Length > 2)
                value = int.Parse(values[2]);

            switch (opt)
            {
                case "learn":
                {
                    if (user.SubClass.Professions.ContainsKey(subpro))
                    {
                        user.SendSysMessage(Language.StrSubProAlreadyLearned, Color.White);
                        return false;
                    }

                    if (!Enum.IsDefined(typeof(SubClasses), subpro))
                        return false;

                    return user.SubClass.Create(subpro);
                }
                case "check": // check if exists
                {
                    return user.SubClass.Professions.ContainsKey(subpro);
                }
                case "level":
                {
                    if (!user.SubClass.Professions.ContainsKey(subpro))
                    {
                        user.SendSysMessage(Language.StrSubProNotLearned, Color.White);
                        return false;
                    }

                    if (!Enum.IsDefined(typeof(SubClasses), subpro))
                        return false;

                    if (value >= 9) return false;
                    return user.SubClass.Professions[subpro].Level >= value;
                }
                case "pro":
                {
                    return user.SubClass.Professions.ContainsKey(subpro) &&
                           user.SubClass.Professions[subpro].Promotion >= value;
                }
                case "promote":
                {
                    if (!user.SubClass.Professions.ContainsKey(subpro))
                    {
                        user.SendSysMessage(Language.StrSubProNotLearned, Color.White);
                        return false;
                    }

                    if (!Enum.IsDefined(typeof(SubClasses), subpro))
                        return false;

                    return user.SubClass.Promote(subpro);
                }
                case "uplev":
                {
                    if (!user.SubClass.Professions.ContainsKey(subpro))
                    {
                        user.SendSysMessage(Language.StrSubProNotLearned, Color.White);
                        return false;
                    }

                    if (!Enum.IsDefined(typeof(SubClasses), subpro))
                        return false;

                    return user.SubClass.Uplev(subpro);
                }
                default:
                    return false;
            }
        }

        #endregion

        #region 1510 - ActionGeneralSkillLineEnabled

        private bool ActionGeneralSkillLineEnabled1510(GameActionEntity action, string param, Character user,
            ScreenObject role, Item item,
            string pszAccept)
        {
            if (user == null || ServerKernel.LineSkillPk?.IsReady != true)
                return false;
            return ServerKernel.LineSkillPk.IsEnterEnable(user);
        }

        #endregion

        #endregion

        #region 2000 - Event

        #region 2001 - ActionEventSetStatus

        private bool ActionEventSetStatus2001(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);

            if (pszParam.Length < 3) return false;

            uint idMap = uint.Parse(pszParam[0]);
            int nStatus = int.Parse(pszParam[1]);
            int nFlag = int.Parse(pszParam[2]);

            Map map;
            if (!ServerKernel.Maps.TryGetValue(idMap, out map))
                return false;

            map.SetStatus((byte) nStatus, nFlag != 0);
            return true;
        }

        #endregion

        #region 2002 - ActionEventDelNpcGenId

        private bool ActionEventDelNpcGenId2002(GameActionEntity action, string param, Character user,
            ScreenObject role,
            Item item, string pszAccept)
        {
            Generator pGen = null;

            foreach (var gen in ServerKernel.Generators.Values)
            {
                if (gen.Identity == action.Data)
                {
                    pGen = gen;
                    break;
                }
            }

            if (pGen == null) return false;

            foreach (var mob in pGen.Collection.Values)
            {
                ServerKernel.RoleManager.RemoveRole(mob.Identity);
            }

            pGen.Collection.Clear();

            return true;
        }

        #endregion

        #region 2003 - ActionEventCompare

        private bool ActionEventCompare2003(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);

            if (pszParam.Length < 3)
                return false;

            long nData1 = long.Parse(pszParam[0]), nData2 = long.Parse(pszParam[2]);
            string szOpt = pszParam[1];

            switch (szOpt)
            {
                case "==":
                    return nData1 == nData2;
                case "<":
                    return nData1 < nData2;
                case ">":
                    return nData1 > nData2;
                case "<=":
                    return nData1 <= nData2;
                case ">=":
                    return nData1 >= nData2;
            }

            return false;
        }

        #endregion

        #region 2004 - ActionEventCompareUnsigned

        private bool ActionEventCompareUnsigned2004(GameActionEntity action, string param, Character user,
            ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);

            if (pszParam.Length < 3)
                return false;

            ulong nData1 = ulong.Parse(pszParam[0]), nData2 = ulong.Parse(pszParam[2]);
            string szOpt = pszParam[1];

            switch (szOpt)
            {
                case "==":
                    return nData1 == nData2;
                case "<":
                    return nData1 < nData2;
                case ">":
                    return nData1 > nData2;
            }

            return false;
        }

        #endregion

        #region 2006 - ActionEventCreatePet

        private bool ActionEventCreatePet2006(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(action.Param);

            if (pszParam.Length < 7) return false;

            uint dwOwnerType = uint.Parse(pszParam[0]);
            uint idOwner = uint.Parse(pszParam[1]);
            uint idMap = uint.Parse(pszParam[2]);
            ushort usPosX = ushort.Parse(pszParam[3]);
            ushort usPosY = ushort.Parse(pszParam[4]);
            uint idGen = uint.Parse(pszParam[5]);
            uint idType = uint.Parse(pszParam[6]);
            uint dwData = 0;
            string szName = "";

            if (pszParam.Length >= 8)
                dwData = uint.Parse(pszParam[7]);
            if (pszParam.Length >= 9)
                szName = pszParam[8];

            if (!ServerKernel.Monsters.TryGetValue(idType, out var dbMonster) || !ServerKernel.Maps.ContainsKey(idMap))
                return false;

            Generator pGen = ServerKernel.Generators.Values.FirstOrDefault(x => x.Identity == idGen);
            if (pGen == null)
            {
                pGen = new Generator(idMap, idType, usPosX, usPosY, 1, 1);
                try
                {
                    ServerKernel.Generators.TryAdd(pGen.Identity, pGen);
                }
                catch (Exception)
                {
                    return false;
                }
            }

            Monster monster = new Monster(dbMonster, (uint) ServerKernel.RoleManager.MonsterIdentity.GetNextIdentity,
                pGen);
            if (!monster.Initialize(idMap, usPosX, usPosY))
                return false;
            return pGen.Collection.TryAdd(monster.Identity, monster) && ServerKernel.RoleManager.AddRole(monster);
        }

        #endregion

        #region 2007 - ActionEventCreateNewNpc

        private bool ActionEventCreateNewNpc2007(GameActionEntity action, string param, Character user,
            ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 9)
                return false;

            string szName = pszParam[0];
            ushort nType = ushort.Parse(pszParam[1]);
            ushort nSort = ushort.Parse(pszParam[2]);
            ushort nLookface = ushort.Parse(pszParam[3]);
            uint nOwnerType = uint.Parse(pszParam[4]);
            uint nOwner = uint.Parse(pszParam[5]);
            uint idMap = uint.Parse(pszParam[6]);
            ushort nPosX = ushort.Parse(pszParam[7]);
            ushort nPosY = ushort.Parse(pszParam[8]);
            uint nLife = 0;
            uint idBase = 0;
            uint idLink = 0;
            uint setTask0 = 0;
            uint setTask1 = 0;
            uint setTask2 = 0;
            uint setTask3 = 0;
            uint setTask4 = 0;
            uint setTask5 = 0;
            uint setTask6 = 0;
            uint setTask7 = 0;
            int setData0 = 0;
            int setData1 = 0;
            int setData2 = 0;
            int setData3 = 0;
            string szData = "";
            if (pszParam.Length > 9)
            {
                nLife = uint.Parse(pszParam[9]);
                if (pszParam.Length > 10)
                    idBase = uint.Parse(pszParam[10]);
                if (pszParam.Length > 11)
                    idLink = uint.Parse(pszParam[11]);
                if (pszParam.Length > 12)
                    setTask0 = uint.Parse(pszParam[12]);
                if (pszParam.Length > 13)
                    setTask1 = uint.Parse(pszParam[13]);
                if (pszParam.Length > 14)
                    setTask2 = uint.Parse(pszParam[14]);
                if (pszParam.Length > 15)
                    setTask3 = uint.Parse(pszParam[15]);
                if (pszParam.Length > 16)
                    setTask4 = uint.Parse(pszParam[16]);
                if (pszParam.Length > 17)
                    setTask5 = uint.Parse(pszParam[17]);
                if (pszParam.Length > 18)
                    setTask6 = uint.Parse(pszParam[18]);
                if (pszParam.Length > 19)
                    setTask7 = uint.Parse(pszParam[19]);
                if (pszParam.Length > 20)
                    setData0 = int.Parse(pszParam[20]);
                if (pszParam.Length > 21)
                    setData1 = int.Parse(pszParam[21]);
                if (pszParam.Length > 22)
                    setData2 = int.Parse(pszParam[22]);
                if (pszParam.Length > 23)
                    setData3 = int.Parse(pszParam[23]);
                if (pszParam.Length > 24)
                    szData = pszParam[24];
            }

            if (!ServerKernel.Maps.ContainsKey(idMap))
                return false;

            var npc = new DynamicNpcEntity
            {
                Name = szName,
                Base = idBase,
                Cellx = nPosX,
                Celly = nPosY,
                Data0 = setData0,
                Data1 = setData1,
                Data2 = setData2,
                Data3 = setData3,
                Datastr = szData,
                Defence = 0,
                Life = nLife,
                Maxlife = nLife,
                Linkid = idLink,
                Task0 = setTask0,
                Task1 = setTask1,
                Task2 = setTask2,
                Task3 = setTask3,
                Task4 = setTask4,
                Task5 = setTask5,
                Task6 = setTask6,
                Task7 = setTask7,
                Ownerid = nOwner,
                Ownertype = nOwnerType,
                Lookface = nLookface,
                Type = nType,
                Mapid = idMap,
                Sort = nSort
            };
            new DynamicNpcRepository().Save(npc);
            var newNpc = new DynamicNpc(npc);
            if (!newNpc.Initialize())
                return false;
            return ServerKernel.RoleManager.AddRole(newNpc);
        }

        #endregion

        #region 2008 - ActionEventCountMonster

        private bool ActionEventCountMonster2008(GameActionEntity action, string param, Character user,
            ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);

            if (pszParam.Length < 5)
                return false;

            uint idMap = uint.Parse(pszParam[0]);
            string szField = pszParam[1];
            string szData = pszParam[2];
            string szOpt = pszParam[3];
            int nNum = int.Parse(pszParam[4]);
            int nCount = 0;

            switch (szField)
            {
                case "name":
                {
                    //foreach (var gen in ServerKernel.Generators.Values.Where(x => x.MapIdentity == idMap))
                    //    if (gen.MonsterName == szData)
                    //        nCount += gen.Collection.Values.Count(x => x.IsAlive);
                    nCount += ServerKernel.RoleManager.CountByName(szData, idMap);
                    break;
                }

                case "gen_id":
                {
                    Generator pGen =
                        ServerKernel.Generators.Values.FirstOrDefault(x => x.Identity == uint.Parse(szData));
                    if (pGen == null) return false;
                    nCount += pGen.Collection.Values.Count(x => x.IsAlive);
                    break;
                }
            }

            switch (szOpt)
            {
                case "==":
                    return nCount == nNum;
                case "<":
                    return nCount < nNum;
                case ">":
                    return nCount > nNum;
            }

            return false;
        }

        #endregion

        #region 2009 - ActionEventDeleteMonster

        private bool ActionEventDeleteMonster2009(GameActionEntity action, string param, Character user,
            ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);

            if (pszParam.Length < 2)
                return false;

            uint idMap = uint.Parse(pszParam[0]);
            uint idType = uint.Parse(pszParam[1]);
            int nData = 0;
            string szName = "";

            if (pszParam.Length >= 3)
                nData = int.Parse(pszParam[2]);
            if (pszParam.Length >= 4)
                szName = pszParam[3];

            bool ret = false;

            foreach (var pGen in ServerKernel.Generators.Values.Where(x => x.MapIdentity == idMap))
            {
                if (idType == pGen.RoleType || pGen.MonsterName == szName)
                {
                    foreach (var pRole in pGen.Collection.Values)
                        ServerKernel.RoleManager.RemoveRole(pRole.Identity);
                    pGen.Collection.Clear();
                    ret = true;
                }
            }

            return ret;
        }

        #endregion

        #region 2010 - ActionEventBbs

        private bool ActionEventBbs2010(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            MsgTalk pMsg = new MsgTalk(action.Param, ChatTone.System);
            ServerKernel.UserManager.SendToAllUser(pMsg);
            return true;
        }

        #endregion

        #region 2011 - ActionEventErase

        private bool ActionEventErase2011(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);
            if (pszParam.Count() < 2)
                return false;

            if (!ServerKernel.Maps.TryGetValue(uint.Parse(pszParam[0]), out var map))
                return false;

            foreach (var obj in map.RoleSet.Values.Where(x => x is DynamicNpc))
            {
                var pNpc = obj as DynamicNpc;
                if (pNpc?.Type == uint.Parse(pszParam[1]))
                    pNpc.DelNpc();
            }

            return true;
        }

        #endregion

        #region 2012 - ActionEventTeleport

        private bool ActionEventTeleport2012(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);

            if (pszParam.Length < 4)
                return false;

            if (!uint.TryParse(pszParam[0], out var idSource) || !uint.TryParse(pszParam[1], out var idTarget) ||
                !ushort.TryParse(pszParam[2], out var usMapX) || !ushort.TryParse(pszParam[3], out var usMapY))
                return false;

            if (!ServerKernel.Maps.TryGetValue(idSource, out var pSourceMap)
                || !ServerKernel.Maps.TryGetValue(idTarget, out var pTargetMap))
                return false;

            if (pSourceMap.IsTeleportDisable())
                return false;

            if (pTargetMap[usMapX, usMapY].Access <= TileType.Monster)
                return false;

            foreach (var plr in pSourceMap.PlayerSet.Values)
                plr.FlyMap(idTarget, usMapX, usMapY);

            return true;
        }

        #endregion

        #region 2013 - ActionEventMassAction

        private bool ActionEventMassAction2013(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] pszParam = GetSafeParam(param);
            if (pszParam.Length < 3)
                return false;

            if (!uint.TryParse(pszParam[0], out var idMap) || !uint.TryParse(pszParam[1], out var idAction)
                                                           || !int.TryParse(pszParam[2], out var nAmount))
                return false;

            if (!ServerKernel.Maps.TryGetValue(idMap, out var pTargetMap))
                return false;

            if (nAmount <= 0)
                nAmount = int.MaxValue;

            foreach (var plr in pTargetMap.PlayerSet.Values)
                if (nAmount-- > 0)
                    plr.GameAction.ProcessAction(idAction, m_pUser, m_pRole, m_pItem, null);

            return true;
        }

        #endregion

        #endregion

        #region 2100 Trap

        #region 2101 - ActionTrapCreate

        private bool ActionTrapCreate2101(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] safeParam = GetSafeParam(param);

            if (safeParam.Length < 7)
            {
                ServerKernel.Log.SaveLog($"ERROR: Action[{action.Identity}] invalid param");
                return false;
            }

            uint idMap = uint.Parse(safeParam[3]);

            Map pMap;
            if (!ServerKernel.Maps.TryGetValue(idMap, out pMap))
            {
                ServerKernel.Log.SaveLog($"ERROR: ActionTrapCreate() invalid map id Action[{action.Identity}]");
                return false;
            }

            TrapEntity dbTrap = new TrapEntity
            {
                Id = (uint) ServerKernel.RoleManager.TrapIdentity.GetNextIdentity,
                Look = uint.Parse(safeParam[1]),
                Type = uint.Parse(safeParam[0]),
                OwnerId = uint.Parse(safeParam[2]),
                PosY = ushort.Parse(safeParam[5]),
                PosX = ushort.Parse(safeParam[4]),
                Data = uint.Parse(safeParam[6]),
                MapId = idMap
            };

            MapTrap pTrap = new MapTrap(dbTrap);
            if (!pTrap.Init())
            {
                ServerKernel.Log.SaveLog($"ERROR: ActionTrapCreate() could not init trap action[{action.Identity}]");
                return false;
            }

            return true;
        }

        #endregion

        #region 2102 - ActionTrapErase

        private bool ActionTrapErase2102(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            MapTrap pTrap = role as MapTrap;
            if (pTrap == null)
            {
                ServerKernel.Log.SaveLog($"ERROR: ActionTrapErase() not called by trap Action [{action.Identity}]");
                return false;
            }

            pTrap.DelTrap();
            pTrap.LeaveMap();
            return true;
        }

        #endregion

        #region 2103 - ActionTrapCount

        private bool ActionTrapCount2103(GameActionEntity action, string param, Character user, ScreenObject role,
            Item item, string pszAccept)
        {
            string[] safeParam = GetSafeParam(param);

            if (safeParam.Length < 7)
            {
                ServerKernel.Log.SaveLog($"ERROR: Action[{action.Identity}] invalid param");
                return false;
            }

            uint idMap = uint.Parse(safeParam[0]);
            uint nType = uint.Parse(safeParam[6]);
            int nCount = int.Parse(safeParam[5]);
            int nNum = 0;

            if (!ServerKernel.Maps.TryGetValue(idMap, out var pMap))
            {
                ServerKernel.Log.SaveLog($"ERROR: ActionTrapCreate() invalid map id Action[{action.Identity}]");
                return false;
            }

            foreach (var obj in pMap.RoleSet.Values)
            {
                if (obj is MapTrap && (obj as MapTrap).Type == nType)
                    nNum++;
            }

            return nNum < nCount;
        }

        #endregion

        #endregion
    }
}