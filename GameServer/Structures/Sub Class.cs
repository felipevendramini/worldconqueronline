﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Sub Class.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Items;

#endregion

namespace GameServer.Structures
{
    public sealed class SubClass
    {
        private SubclassRepository m_repository = new SubclassRepository();

        private readonly int[] LEVEL_STUDY_MARTIAL_ARTIST = {0, 300, 900, 1800, 2700, 3600, 5100, 6900, 8700, 0};
        private readonly int[] LEVEL_STUDY_WARLOCK = {0, 300, 900, 1800, 2700, 3600, 5100, 6900, 8700, 0};
        private readonly int[] LEVEL_STUDY_CHI_MASTER = {0, 600, 1800, 3600, 5400, 7200, 10200, 13800, 17400, 0};
        private readonly int[] LEVEL_STUDY_SAGE = {0, 400, 1200, 2400, 3600, 4800, 6800, 9200, 11600, 0};
        private readonly int[] LEVEL_STUDY_APOTHECARY = {0, 100, 200, 300, 400, 500, 1000, 4000, 9000, 0};
        private readonly int[] LEVEL_STUDY_PERFORMER = {0, 400, 1200, 2400, 3600, 4800, 6800, 9200, 11600, 0};
        private readonly int[] LEVEL_STUDY_WRANGLER = {0, 400, 1200, 2400, 3600, 4800, 6800, 9200, 11600, 0};

        public readonly int[] PowerMartialArtist = {0, 100, 200, 300, 400, 600, 800, 1000, 1200, 1500};
        public readonly int[] PowerWarlock = {0, 100, 200, 300, 400, 600, 800, 1000, 1200, 1500};
        public readonly int[] PowerChiMaster = {0, 100, 200, 300, 400, 600, 800, 1000, 1200, 1500};
        public readonly int[] PowerSage = {0, 100, 200, 300, 400, 600, 800, 1000, 1200, 1500};
        public readonly int[] PowerApothecary = {0, 8, 16, 24, 32, 40, 48, 56, 64, 72};
        public readonly int[] PowerPerformer = {0, 100, 200, 300, 400, 500, 600, 700, 800, 1000};
        public readonly int[] PowerWrangler = {0, 100, 200, 300, 400, 500, 600, 800, 1000, 1200};

        private Character m_pOwner;
        public ConcurrentDictionary<SubClasses, SubclassStruct> Professions;

        public SubClass(Character pOwner)
        {
            m_pOwner = pOwner;
            Professions = new ConcurrentDictionary<SubClasses, SubclassStruct>();
        }

        public bool Init()
        {
            IList<SubclassEntity> _sub = m_repository.GetAllSubclasses(m_pOwner.Identity);
            if (_sub != null)
            {
                foreach (SubclassEntity sub in _sub)
                {
                    SubclassStruct iclass = new SubclassStruct
                    {
                        Database = sub,
                        Class = (SubClasses) sub.Class,
                        Level = sub.Level,
                        Promotion = sub.Promotion
                    };

                    if (!m_pOwner.SubClass.Add(iclass))
                        Program.WriteLog($"Could not load [{iclass.Class}] from [{m_pOwner.Identity}]");
                }

                m_pOwner.SubClass.LearnAll();
                m_pOwner.SubClass.SendAll();
            }

            if (m_pOwner.SubClass.Professions.ContainsKey(m_pOwner.ActiveSubclass))
            {
                m_pOwner.SubClass.Active(m_pOwner.ActiveSubclass);
            }

            return true;
        }

        public bool Learn(SubClasses type)
        {
            if (Professions.ContainsKey(type))
                return false;

            Item item = null;
            switch (type)
            {
                case SubClasses.Wrangler: // saddle
                    if (!m_pOwner.UserPackage.MultiSpendItem(723903, 723903, 40))
                        return false;
                    break;
                case SubClasses.Performer: // 
                    item = m_pOwner.UserPackage.GetItemByType(711679);
                    if (!m_pOwner.UserPackage.SpendItem(item))
                        return false;
                    break;
                case SubClasses.Apothecary:
                    if (!m_pOwner.UserPackage.MultiSpendItem(1088001, 1088002, 10))
                        return false;
                    break;
                case SubClasses.Sage:
                    if (!m_pOwner.UserPackage.MultiSpendItem(723087, 723087, 20))
                        return false;
                    break;
                case SubClasses.ChiMaster:
                    item = m_pOwner.UserPackage.GetItemByType(711188);
                    if (!m_pOwner.UserPackage.SpendItem(item))
                        return false;
                    break;
                case SubClasses.Warlock:
                    if (!m_pOwner.UserPackage.MultiSpendItem(721261, 721261, 10))
                        return false;
                    break;
                case SubClasses.MartialArtist:
                    if (!m_pOwner.UserPackage.MultiSpendItem(721259, 721259, 5))
                        return false;
                    break;
            }

            return Create(type);
        }

        public bool Create(SubClasses pType)
        {
            if (Professions.ContainsKey(pType))
                return false;

            var dbClass = new SubclassEntity
            {
                Class = (byte) pType,
                Level = 1,
                Promotion = 1,
                Userid = m_pOwner.Identity
            };

            if (!m_repository.Save(dbClass))
                return false;

            var pClass = new SubclassStruct
            {
                Database = dbClass,
                Class = pType,
                Level = 1,
                Promotion = 1
            };

            if (!Professions.TryAdd(pClass.Class, pClass))
                return false;

            var pMsg = new MsgSubPro
            {
                Action = SubClassActions.Learn,
                Subclass = pType
            };
            m_pOwner.Send(pMsg);
            return true;
        }

        public bool Add(SubclassStruct isub)
        {
            return Professions.TryAdd(isub.Class, isub);
        }

        public bool Active(SubClasses sClass)
        {
            if (sClass == SubClasses.None)
            {
                var nsPacket = new MsgSubPro
                {
                    Action = SubClassActions.Activate,
                    Subclass = 0
                };
                m_pOwner.Send(nsPacket);
                return true;
            }

            if (!Professions.ContainsKey(sClass))
                return false;

            m_pOwner.ActiveSubclass = sClass;
            var sPacket = new MsgSubPro
            {
                Action = SubClassActions.Activate,
                Subclass = sClass
            };
            m_pOwner.Send(sPacket);
            return true;
        }

        public bool Uplev(SubClasses sClass)
        {
            if (!Professions.TryGetValue(sClass, out var iclass))
                return false;

            if (iclass.Level + 1 > 10) return false; // max level

            switch (sClass)
            {
                case SubClasses.Apothecary:
                {
                    if (!SpendStudy(LEVEL_STUDY_APOTHECARY[iclass.Level]))
                        return false;
                    break;
                }
                case SubClasses.ChiMaster:
                {
                    if (!SpendStudy(LEVEL_STUDY_CHI_MASTER[iclass.Level]))
                        return false;
                    break;
                }
                case SubClasses.MartialArtist:
                {
                    if (!SpendStudy(LEVEL_STUDY_MARTIAL_ARTIST[iclass.Level]))
                        return false;
                    break;
                }
                case SubClasses.Performer:
                {
                    if (!SpendStudy(LEVEL_STUDY_PERFORMER[iclass.Level]))
                        return false;
                    break;
                }
                case SubClasses.Sage:
                {
                    if (!SpendStudy(LEVEL_STUDY_SAGE[iclass.Level]))
                        return false;
                    break;
                }
                case SubClasses.Warlock:
                {
                    if (!SpendStudy(LEVEL_STUDY_WARLOCK[iclass.Level]))
                        return false;
                    break;
                }
                case SubClasses.Wrangler:
                {
                    if (!SpendStudy(LEVEL_STUDY_WRANGLER[iclass.Level]))
                        return false;
                    break;
                }
                default:
                    return false;
            }

            iclass.Level += 1;
            if (!Save(iclass))
                return false;
            var sPacket = new MsgSubPro();
            sPacket.Action = SubClassActions.MartialUplev;
            sPacket.Subclass = sClass;
            m_pOwner.Send(sPacket);
            return true;
        }

        public bool Promote(SubClasses sClass)
        {
            SubclassStruct iClass;
            if (!Professions.TryGetValue(sClass, out iClass) || iClass.Promotion >= iClass.Level)
                return false;

            if (iClass.Promotion + 1 > 9) return false; // max level

            iClass.Promotion += 1;
            if (!Save(iClass))
                return false;

            var sPacket = new MsgSubPro();
            sPacket.Action = SubClassActions.MartialPromoted;
            sPacket.Subclass = sClass;
            sPacket.WriteByte(iClass.Promotion, 11);
            m_pOwner.Send(sPacket);
            m_pOwner.UpdateAttributes();
            return true;
        }

        public bool AwardStudy(uint dwAmount)
        {
            if (dwAmount + m_pOwner.StudyPoints > uint.MaxValue)
                dwAmount = uint.MaxValue - m_pOwner.StudyPoints;

            if (dwAmount == 0) return false;

            m_pOwner.StudyPoints += dwAmount;

            SendStudy();
            return true;
        }

        public bool SpendStudy(int dwAmount)
        {
            if (dwAmount < 0) dwAmount *= -1;
            if (m_pOwner.StudyPoints - dwAmount < 0)
                return false;

            m_pOwner.StudyPoints -= (uint) dwAmount;

            SendStudy();
            return true;
        }

        public void SendStudy()
        {
            var pMsg = new MsgSubPro
            {
                Action = SubClassActions.UpdateStudy,
                StudyPoints = m_pOwner.StudyPoints
            };
            m_pOwner.Send(pMsg);
        }

        public bool Save(SubclassStruct pObj)
        {
            return m_repository.Save(pObj.Database);
        }

        public bool SaveAll()
        {
            foreach (var prof in Professions.Values)
                if (!Save(prof))
                    return false;
            return true;
        }

        public void SendAll()
        {
            var pMsg = new MsgSubPro
            {
                Action = SubClassActions.ShowGui,
                StudyPoints = m_pOwner.StudyPoints
            };
            foreach (var prof in Professions.Values)
                pMsg.Append(prof.Class, prof.Promotion, prof.Level);
            m_pOwner.Send(pMsg);
        }

        public void LearnAll()
        {
            foreach (var prof in Professions.Values)
            {
                var pMsg = new MsgSubPro
                {
                    Action = SubClassActions.Learn,
                    Subclass = prof.Class
                };
                pMsg.WriteByte(prof.Level, 11);
                m_pOwner.Send(pMsg);
                pMsg.Action = SubClassActions.MartialPromoted;
                pMsg.WriteByte(prof.Promotion, 11);
                m_pOwner.Send(pMsg);
            }
        }

        public SubclassStruct this[byte sub]
        {
            get { return Professions.Values.FirstOrDefault(x => x.Class == (SubClasses) sub); }
        }

        public SubclassStruct this[SubClasses sub]
        {
            get { return Professions.Values.FirstOrDefault(x => x.Class == sub); }
        }
    }

    public struct SubclassStruct
    {
        public SubClasses Class
        {
            get { return (SubClasses) Database.Class; }
            set { Database.Class = (byte) value; }
        }

        public byte Level
        {
            get { return Database.Level; }
            set { Database.Level = value; }
        }

        public byte Promotion
        {
            get { return Database.Promotion; }
            set { Database.Promotion = value; }
        }

        public SubclassEntity Database;
    }
}