﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Booth Item.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace GameServer.Structures.Items
{
    public class BoothItem
    {
        private Item m_pItem;
        private uint m_dwValue;
        private bool m_bSilver;

        public bool Create(Item item, uint dwMoney, bool bSilver)
        {
            m_pItem = item;
            m_dwValue = dwMoney;
            m_bSilver = bSilver;

            return m_dwValue > 0;
        }

        public Item Item => m_pItem;
        public uint Value => m_dwValue;
        public bool IsSilver => m_bSilver;
    }
}