﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Map Item.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Drawing;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.World;

#endregion

namespace GameServer.Structures.Items
{
    public class MapItem : ScreenObject
    {
        private const uint _ITEM_SILVER_MIN = 1;
        private const uint _ITEM_SILVER_MAX = 9;
        private const uint _ITEM_SYCEE_MIN = 10;
        private const uint _ITEM_SYCEE_MAX = 99;
        private const uint _ITEM_GOLD_MIN = 100;
        private const uint _ITEM_GOLD_MAX = 999;
        private const uint _ITEM_GOLDBULLION_MIN = 1000;
        private const uint _ITEM_GOLDBULLION_MAX = 1999;
        private const uint _ITEM_GOLDBAR_MIN = 2000;
        private const uint _ITEM_GOLDBAR_MAX = 4999;
        private const uint _ITEM_GOLDBARS_MIN = 5000;
        private const uint _ITEM_GOLDBARS_MAX = 10000000;

        private const int _PICKUP_TIME = 30;
        private const int _DISAPPEAR_TIME = 60;
        private const int _MAPITEM_ONTIMER_SECS = 5;
        private const int _MAPITEM_MONSTER_ALIVESECS = 60;
        private const int _MAPITEM_USERMAX_ALIVESECS = 90;
        private const int _MAPITEM_USERMIN_ALIVESECS = 60;

        private const int
            _MAPITEM_ALIVESECS_PERPRICE =
                1000 / (_MAPITEM_USERMAX_ALIVESECS -
                        _MAPITEM_USERMIN_ALIVESECS); // Íæ¼ÒÈÓµÄµØÃæÎïÆ·µÄÉú´æÊ±¼ä(Ã¿¶àÉÙÇ®¼Ó1Ãë)

        private const int _MAPITEM_PRIV_SECS = 30;
        private const int _PICKMAPITEMDIST_LIMIT = 0;
        private bool m_bTaken;
        private uint m_dwAmount;
        private uint m_idOwner;
        private byte m_nDmg;
        private short m_nDura;
        private byte m_nPlus;
        private bool m_bIsDeleted = false;

        private Item m_pItem;
        private ItemtypeEntity m_pItemtype;
        private MsgMapItem m_pPacket;

        // double check to avoid duplicate
        private object m_pSyncRoot = new object();
        private TimeOut m_tAlive;
        private TimeOut m_tPriv;

        public MapItem(uint idFloor)
            : base(idFloor)
        {
            m_pPacket = new MsgMapItem();
        }

        ~MapItem()
        {
            try
            {
                ServerKernel.RoleManager.FloorIdentity.ReturnIdentity(Identity);
            }
            catch
            {

            }
        }

        #region IScreen Object
        
        public override string Name
        {
            get => m_pItemtype.Name;
            set { }
        }

        public ushort MapX
        {
            get => m_usMapX;
            set
            {
                m_usMapX = value;
                m_pPacket.MapX = value;
            }
        }

        public ushort MapY
        {
            get => m_usMapY;
            set
            {
                m_usMapY = value;
                m_pPacket.MapY = value;
            }
        }
        
        public override void SendSpawnTo(Character pObj)
        {
            pObj.Send(m_pPacket);
        }

        public void SendRemoveFromScreen(Character pObj)
        {
            m_pPacket.DropType = DropType.Disappear;
            pObj.Send(m_pPacket);
        }

        #endregion

        #region Creation

        public bool Create(Map map, Point pos, uint idType, uint idOwner, byte nPlus, byte nDmg,
            short nDura)
        {
            if (ServerKernel.Itemtype.TryGetValue(idType, out m_pItemtype))
            {
                return Create(map, pos, m_pItemtype, idOwner, nPlus, nDmg, nDura);
            }

            return false;
        }

        public bool Create(Map map, Point pos, ItemtypeEntity idType, uint idOwner, byte nPlus,
            byte nDmg,
            short nDura)
        {
            if (map == null || idType == null) return false;

            m_tAlive = new TimeOut(_DISAPPEAR_TIME);
            m_tAlive.Startup(_DISAPPEAR_TIME);

            m_pMap = map;
            m_idMap = map.Identity;
            MapX = (ushort) pos.X;
            MapY = (ushort) pos.Y;

            m_nPlus = nPlus;
            //m_pPacket.Quality = (uint)(idType.Type % 10);
            //m_pPacket.Addition = nPlus;

            m_nDmg = nDmg;
            m_nDura = nDura;

            m_pItemtype = idType;
            Type = m_pItemtype.Type;
            m_pPacket.Identity = m_idObj;

            if (idOwner != 0)
            {
                m_idOwner = idOwner;
                m_tPriv = new TimeOut(_MAPITEM_PRIV_SECS);
                m_tPriv.Startup(_MAPITEM_PRIV_SECS);
                m_tPriv.Update();
            }

            m_pPacket.DropType = DropType.Drop;
            //m_pPacket.Addition = m_nPlus;
            m_pPacket.ItemColor = 3;
            //m_pPacket.ItemName = m_pItemtype.Name;
            EnterMap();
            return true;
        }

        public bool Create(Map map, Point pos, Item pInfo, uint idOwner)
        {
            if (map == null || pInfo == null) return false;

            int nAliveSecs = _MAPITEM_USERMAX_ALIVESECS;
            if (pInfo.Itemtype != null)
                nAliveSecs = (int) (pInfo.Itemtype.Price / _MAPITEM_ALIVESECS_PERPRICE + _MAPITEM_USERMIN_ALIVESECS);
            if (nAliveSecs > _MAPITEM_USERMAX_ALIVESECS)
                nAliveSecs = _MAPITEM_USERMAX_ALIVESECS;

            m_tAlive = new TimeOut(nAliveSecs);
            m_tAlive.Update();

            m_pMap = map;
            m_idMap = map.Identity;
            MapX = (ushort) pos.X;
            MapY = (ushort) pos.Y;

            m_pItem = pInfo;
            m_pPacket.Identity = m_idObj;
            m_pItemtype = pInfo.Itemtype;
            m_pItem.OwnerIdentity = 0;
            m_pItem.PlayerIdentity = idOwner;

            m_pItem.Position = (ItemPosition) 254;

            m_pPacket.ItemColor = 3;
            m_pPacket.DropType = DropType.Drop;
            m_pPacket.Itemtype = Type = pInfo.Type;
            //m_pPacket.Quality = (uint)(pInfo.Type % 10);
            //m_pPacket.Addition = pInfo.Addition;
            //m_pPacket.ItemName = m_pItemtype?.Name ?? "";

            EnterMap();
            return true;
        }

        public bool CreateMoney(Map map, Point pos, uint dwMoney, uint idOwner)
        {
            if (map == null || m_idObj == 0) return false;

            int nAliveSecs = _MAPITEM_MONSTER_ALIVESECS;
            if (idOwner == 0)
            {
                nAliveSecs = (int) (dwMoney / _MAPITEM_ALIVESECS_PERPRICE + _MAPITEM_USERMIN_ALIVESECS);
                if (nAliveSecs > _MAPITEM_USERMAX_ALIVESECS)
                    nAliveSecs = _MAPITEM_USERMAX_ALIVESECS;
            }

            m_tAlive = new TimeOut(nAliveSecs);
            m_tAlive.Update();

            m_pMap = map;
            m_idMap = map.Identity;
            MapX = (ushort) pos.X;
            MapY = (ushort) pos.Y;

            uint idType;
            if (dwMoney < _ITEM_SILVER_MAX)
                idType = 1090000;
            else if (dwMoney < _ITEM_SYCEE_MAX)
                idType = 1090010;
            else if (dwMoney < _ITEM_GOLD_MAX)
                idType = 1090020;
            else if (dwMoney < _ITEM_GOLDBULLION_MAX)
                idType = 1091000;
            else if (dwMoney < _ITEM_GOLDBAR_MAX)
                idType = 1091010;
            else
                idType = 1091020;

            m_pPacket.Identity = m_idObj;
            m_dwAmount = dwMoney;

            Type = idType;

            if (idOwner != 0)
            {
                m_idOwner = idOwner;
                m_tPriv = new TimeOut(_MAPITEM_PRIV_SECS);
                m_tPriv.Startup(_MAPITEM_PRIV_SECS);
                m_tPriv.Update();
            }

            m_pPacket.DropType = DropType.Drop;
            try
            {
                EnterMap();
            }
            catch (Exception ex)
            {
                ServerKernel.Log.SaveLog(ex.ToString());
            }

            return true;
        }

        public Item GeneratePickUp(Character pPicker)
        {
            lock (m_pSyncRoot)
            {
                if (m_bTaken || m_bIsDeleted)
                    return null;

                m_bTaken = true;

                m_pItem = new Item();
                m_pItem.Create(pPicker, Item.CreateEntity(Type));
                m_pItem.OwnerIdentity = m_idOwner;
                m_pItem.PlayerIdentity = pPicker.Identity;
                m_pItem.Durability = (ushort) (m_pItemtype.Amount / 60);
                m_pItem.MaxDurability = (ushort) (m_pItemtype.AmountLimit + m_nDura);
                m_pItem.Color = (ItemColor) ThreadSafeRandom.RandGet(2, 9);

                if (m_pItem.Durability > 1)
                {
                    const int nRate = 50;
                    if (Calculations.ChanceCalc(nRate))
                        m_pItem.Durability = (ushort) (m_pItem.Durability * (ThreadSafeRandom.RandGet(15) + 20) / 100);
                    else
                        m_pItem.Durability = (ushort) (m_pItem.Durability * (ThreadSafeRandom.RandGet(15) + 35) / 100);

                    m_pItem.Durability = Calculations.CutTrail((ushort) 1, m_pItem.Durability);
                }

                m_pItem.Position = ItemPosition.Floor;

                m_pItem.SocketOne = (SocketGem) m_pItemtype.Gem1;
                m_pItem.SocketTwo = (SocketGem) m_pItemtype.Gem2;
                m_pItem.Effect = (ItemEffect) m_pItemtype.Magic1;

                m_pItem.Addition = m_nPlus > 0 ? m_nPlus : m_pItemtype.Magic3;

                if (m_nDmg > 0 && (m_pItem.IsEquipment() || m_pItem.IsWeapon()))
                    m_pItem.ReduceDamage = m_nDmg;
                return m_pItem;
            }
        }

        public uint Type
        {
            get => m_pPacket.Itemtype;
            set => m_pPacket.Itemtype = value;
        }

        public void SelfDelete()
        {
            if (m_pItem != null && m_pItem.Identity != 0)
            {
                m_pItem.Delete();
            }
            ServerKernel.RoleManager.RemoveRole(Identity);
        }

        public bool IsMoney()
        {
            return Type >= 1090000 && Type <= 1091020;
        }

        public uint GetPlayerId()
        {
            return m_pItem?.PlayerIdentity ?? 0;
        }

        public bool IsPriv()
        {
            if (m_tPriv == null) return false;
            return !m_tPriv.IsTimeOut();
        }

        public bool IsDisappear()
        {
            return m_tAlive.IsTimeOut();
        }

        public uint GetAmount()
        {
            return m_dwAmount;
        }

        public Item GetInfo(Character pPicker)
        {
            if (m_pItem == null)
                GeneratePickUp(pPicker);

            if (m_pItem == null)
                return null;

            //m_pItem.Type = Type;
            m_pItem.ChangeOwner(pPicker);
            //m_pItem.OwnerIdentity = pPicker.Identity;
            if (m_pItem.StackAmount <= 0)
                m_pItem.StackAmount = 1;
            return m_pItem;
        }

        #endregion

        #region Map

        public override void EnterMap()
        {
            if (ServerKernel.Maps.TryGetValue(m_idMap, out m_pMap))
            {
                Map.EnterRoom(this);
                ServerKernel.RoleManager.AddRole(this);
            }
        }

        public override void LeaveMap()
        {
            m_bIsDeleted = true;
            m_pPacket.DropType = DropType.Disappear;
            Map?.LeaveRoom(Identity);
            Map?.SendToRegion(m_pPacket, MapX, MapY);
            ServerKernel.RoleManager.RemoveRole(Identity);
            ServerKernel.RoleManager.FloorIdentity.ReturnIdentity(Identity);
        }

        #endregion

        public override void OnTimer()
        {
            if (IsDisappear())
            {
                LeaveMap();
                SelfDelete();
            }
        }
    }
}