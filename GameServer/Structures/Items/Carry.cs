﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Carry.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Structures.Items
{
    public sealed class Carry
    {
        public const int MAX_RECORDS = 10;
        public const ushort MAX_USAGE = 30;

        private Character m_pOwner;
        private Item m_pItem;
        private List<CarryEntity> m_pLocations = new List<CarryEntity>(10);

        public Carry(Character owner, Item item)
        {
            m_pOwner = owner;
            m_pItem = item;

            var list = new CarryRepository().FetchByItem(item.Identity);
            if (list != null)
                foreach (var obj in list)
                {
                    m_pLocations.Add(obj);
                }
        }

        public bool Usable => m_pItem.Durability > 0;

        public int Count => m_pLocations.Count;

        public bool AddLocation(uint idMap, ushort mapX, ushort mapY)
        {
            if (Count >= MAX_RECORDS)
                return false;
            CarryEntity newCarry = new CarryEntity
            {
                ItemIdentity = m_pItem.Identity,
                RecordMapId = idMap,
                RecordMapY = mapY,
                RecordMapX = mapX,
                Name = ""
            };
            new CarryRepository().Save(newCarry);
            m_pLocations.Add(newCarry);
            Send();
            return true;
        }

        public bool UpdateLocation(int idx)
        {
            if (idx >= m_pLocations.Count)
                return false;

            CarryEntity carry;
            try
            {
                carry = m_pLocations[idx];
            }
            catch
            {
                return false;
            }

            carry.Name = "";
            carry.RecordMapId = m_pOwner.MapIdentity;
            carry.RecordMapX = m_pOwner.MapX;
            carry.RecordMapY = m_pOwner.MapY;
            Send();
            return new CarryRepository().Save(carry);
        }

        public void SaveName(string name, int idx)
        {
            if (idx >= m_pLocations.Count)
                return;

            CarryEntity carry;
            try
            {
                carry = m_pLocations[idx];
            }
            catch
            {
                return;
            }

            carry.Name = name;
            Send();
            new CarryRepository().Save(carry);
        }

        public CarryEntity Fetch(uint id)
        {
            if (id > m_pLocations.Count)
                return null;
            return m_pLocations[(int) id];
        }

        public void Send()
        {
            MsgSuperFlag pMsg = new MsgSuperFlag
            {
                Durability = m_pItem.Durability,
                ItemIdentity = m_pItem.Identity
            };
            for (int i = 0; i < m_pLocations.Count; i++)
                pMsg.AddLocation((uint) i, m_pLocations[i].RecordMapId, m_pLocations[i].RecordMapX,
                    m_pLocations[i].RecordMapY, m_pLocations[i].Name);
            m_pOwner.Send(pMsg);
        }
    }
}