﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - User Package.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;

#endregion

namespace GameServer.Structures.Items
{
    public sealed class UserPackage
    {
        public const int MAX_INVENTORY_CAPACITY = 40;
        public static readonly uint[] SystemWarehouses = {230, 231, 232, 233, 234, 235, 236};

        private readonly ConcurrentDictionary<ItemPosition, Item> m_equipment =
            new ConcurrentDictionary<ItemPosition, Item>();

        private readonly ConcurrentDictionary<uint, Item> m_inventory = new ConcurrentDictionary<uint, Item>();

        private readonly Character m_pUser;

        public UserPackage(Character player)
        {
            if (player == null)
            {
                Program.WriteLog("Could not start UserPackage, user null.");
                return;
            }

            m_pUser = player;

            foreach (uint i in SystemWarehouses)
                if (m_pUser.Warehouses.ContainsKey(i))
                    ServerKernel.Log.GmLog(@"character_loading",
                        $"Failed to create warehouse [{i}] for user [{m_pUser.Identity}]");
                else
                    m_pUser.Warehouses.Add(i, new Warehouse(m_pUser, (ushort) i));

            IList<ItemEntity> package = Database.ItemRepository.FetchByUser(player.Identity);
            if (package == null)
                return;

            foreach (var dbItem in package)
            {
                Item item = new Item();
                if (!item.Create(player, dbItem))
                {
                    Program.WriteLog($"Could not load item {dbItem.Id} from database (user: {dbItem.PlayerId}",
                        LogType.WARNING);
                    continue;
                }

                if (item.Position == ItemPosition.Inventory) // todo temp
                {
                    item.Position = ItemPosition.Inventory;
                    if (!m_inventory.TryAdd(item.Identity, item))
                        Program.WriteLog($"Could not add item {dbItem.Id} to inventory, possibly duplicate of ids",
                            LogType.WARNING);
                }
                else if (item.Position >= ItemPosition.EquipmentBegin && item.Position <= ItemPosition.Crop
                         || item.Position >= ItemPosition.AltHead && item.Position <= ItemPosition.AltSteed)
                {
                    if (item.RequiredLevel > m_pUser.Level)
                        item.DegradeItem(false);

                    if (!m_equipment.TryAdd(item.Position, item))
                    {
                        item.Position = ItemPosition.Inventory;
                        m_inventory.TryAdd(item.Identity, item);
                    }
                }
                else if (item.Position >= ItemPosition.WarehouseBegin && item.Position <= ItemPosition.WarehouseEnd)
                {
                    try
                    {
                        m_pUser.Warehouses[(uint) item.Position].Add(item, true);
                    }
                    catch (Exception ex)
                    {
                        Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
                    }
                }
                else
                {
                    Program.WriteLog($"Item {item.Identity}:{item.Position} doesnt have a place :C");
                }

                /**
                 * Double security checks.
                 */
                if (item.IsBackswordType() &&
                    (item.Position == ItemPosition.LeftHand || item.Position == ItemPosition.AltWeaponL))
                {
                    m_equipment.TryRemove(item.Position, out _);
                    m_inventory.TryAdd(item.Identity, item);
                }

                if (this[ItemPosition.RightHand] != null && this[ItemPosition.LeftHand]?.IsShield() == true)
                {
                    Item left = this[ItemPosition.LeftHand];
                    Item right = this[ItemPosition.RightHand];

                    if (right.IsBackswordType() || right.IsWeaponProBased())
                        Unequip(ItemPosition.LeftHand);
                }
            }
        }

        /// <summary>
        ///     Takes an item from the equipments by its position.
        /// </summary>
        /// <param name="pos">The position you want to find the item.</param>
        /// <returns>The item equiped.</returns>
        public Item this[ItemPosition pos] => m_equipment.TryGetValue(pos, out var ret) ? ret : null;

        public Item this[string name] => m_inventory.Values.FirstOrDefault(x =>
            string.Equals(x.Name, name, StringComparison.CurrentCultureIgnoreCase));

        /// <summary>
        ///     Takes an item from the inventory list by the item identity.
        /// </summary>
        /// <param name="idItem">The identity of the item.</param>
        /// <returns>The required item.</returns>
        public Item this[uint idItem] => m_inventory.TryGetValue(idItem, out var ret) ? ret : null;

        public Item AwardItem(ItemEntity dbItem)
        {
            Item item = new Item();
            if (!item.Create(m_pUser, dbItem))
                return null;

            if (IsPackFull(item))
            {
                m_pUser.SendSysMessage(Language.StrYourBagIsFull, Color.Red);
                return null;
            }

            Item combine = FindCombineItem(item);
            if (item.IsCountable() || item.StackLimit > 1 && combine != null)
            {
                do
                {
                    CombineItem(item, combine);
                } while (item.StackAmount > 1 && item.StackAmount > item.StackLimit &&
                         (combine = FindCombineItem(item)) != null);

                if (item.StackAmount == 0)
                {
                    item.Delete();
                    return combine;
                }

                if (item.StackAmount > item.StackLimit)
                {
                    if (item.StackAmount > item.StackLimit)
                    {
                        var amount0 = item.StackAmount - item.StackLimit;
                        item.StackAmount = item.StackLimit;
                        m_pUser.AddItem(item);
                        ItemEntity newEntity = Item.CreateEntity(dbItem.Type);
                        newEntity.Monopoly = dbItem.Monopoly;
                        newEntity.StackAmount = (ushort) amount0;
                        return AwardItem(newEntity);
                    }
                }

                m_pUser.AddItem(item);
                return item;
            }

            int amount = 0;
            if (item.StackAmount > item.StackLimit)
            {
                amount = item.StackAmount - item.StackLimit;
                item.StackAmount = item.StackLimit;
                m_pUser.AddItem(item);
                ItemEntity newEntity = Item.CreateEntity(dbItem.Type);
                newEntity.Monopoly = dbItem.Monopoly;
                newEntity.StackAmount = (ushort) amount;
                return AwardItem(newEntity);
            }

            m_pUser.AddItem(item);
            return item;
        }

        [Obsolete("Use RemoveFromInventory instead.", true)]
        public bool EraseItem(uint idItem, bool bSynchro)
        {
            Item item = this[idItem];
            if (item == null)
                return false;
            if (bSynchro)
                m_pUser.Send(item.CreateMsgItem(ItemAction.Remove));
            item.Delete();
            return true;
        }

        public bool EquipItem(Item item, ItemPosition position)
        {
            if (item == null)
                return false;

            if (position == ItemPosition.Inventory)
            {
                if (m_pUser.IsWing && !item.IsArrowSort())
                    return false;

                position = item.GetPosition();
                if (this[ItemPosition.RightHand] != null
                    && this[ItemPosition.LeftHand] == null
                    && item.IsWeaponOneHand()
                    && !item.IsBackswordType())
                    position = ItemPosition.LeftHand;
                else if (this[ItemPosition.RightHandAccessory] != null
                         && this[ItemPosition.LeftHandAccessory] == null
                         && item.IsOneHandAccessory())
                    position = ItemPosition.LeftHandAccessory;
            }

            switch (position)
            {
                case ItemPosition.RightHand:
                    if (!item.IsHoldEnable())
                        return false;
                    if (item.IsWeaponTwoHand())
                    {
                        if (!(this[ItemPosition.RightHand] != null
                              && this[ItemPosition.LeftHand] != null
                              && !IsPackSpare(2)))
                        {
                            Unequip(ItemPosition.RightHand);

                            if (this[ItemPosition.LeftHand] != null && !this[ItemPosition.LeftHand].IsArrow() &&
                                item.IsBow()
                                || m_pUser.Magics[MagicData.PURE_WARRIOR_ID] == null &&
                                this[ItemPosition.LeftHand]?.IsShield() == true)
                                Unequip(ItemPosition.LeftHand);
                        }
                    }
                    else if (item.IsWeaponOneHand() || item.IsWeaponProBased())
                    {
                        if (item.IsPistol())
                            return false;

                        Item pLeft = this[ItemPosition.LeftHand];
                        if (!(pLeft != null && pLeft.IsArrow() && !IsPackSpare(2)))
                        {
                            Unequip(ItemPosition.RightHand);
                            if (pLeft != null && pLeft.IsArrow())
                                Unequip(ItemPosition.LeftHand);
                        }
                    }

                    break;
                case ItemPosition.LeftHand:
                    if (!item.IsHoldEnable())
                        return false;

                    Item pRight = this[ItemPosition.RightHand];
                    if (pRight == null)
                        return false;

                    if (item.IsBackswordType() || pRight.IsBackswordType())
                        return false;

                    if (pRight.IsBackswordType() && item.IsShield())
                        return false;

                    if (pRight.IsWeaponOneHand() && !pRight.IsBackswordType() &&
                        (item.IsWeaponOneHand() || item.IsShield())
                        || pRight.IsBow() && item.IsArrow())
                    {
                        Unequip(ItemPosition.LeftHand);
                    }
                    else if (pRight.IsWeaponProBased())
                    {
                        if (pRight.GetItemSubtype() == item.GetItemSubtype()
                            || pRight.IsRapier() && item.IsPistol())
                        {
                            if (item.IsShield())
                                return false;

                            Unequip(ItemPosition.LeftHand);
                        }
                    }
                    else if (pRight.IsWeaponTwoHand())
                    {
                        if (!item.IsShield())
                            return false;

                        if (m_pUser.ProfessionSort != 2
                            || !m_pUser.IsPureClass()
                            || m_pUser.Magics[MagicData.PURE_WARRIOR_ID] == null)
                            return false;
                    }

                    break;
                default:
                    if (position >= ItemPosition.AltHead
                        && position <= ItemPosition.AltSteed)
                    {
                        ItemPosition pos = item.GetPosition();
                        switch (position)
                        {
                            case ItemPosition.AltHead:
                            {
                                if (pos != ItemPosition.Headwear)
                                    return false;
                                break;
                            }

                            case ItemPosition.AltNecklace:
                            {
                                if (pos != ItemPosition.Necklace)
                                    return false;
                                break;
                            }

                            case ItemPosition.AltRing:
                            {
                                if (pos != ItemPosition.Ring)
                                    return false;
                                break;
                            }

                            case ItemPosition.AltWeaponR:
                            {
                                if (pos != ItemPosition.RightHand)
                                    return false;
                                break;
                            }

                            case ItemPosition.AltWeaponL:
                            {
                                if (pos != ItemPosition.LeftHand && pos != ItemPosition.RightHand)
                                    return false;
                                break;
                            }

                            case ItemPosition.AltArmor:
                            {
                                if (pos != ItemPosition.Armor)
                                    return false;
                                break;
                            }

                            case ItemPosition.AltBoots:
                            {
                                if (pos != ItemPosition.Boots)
                                    return false;
                                break;
                            }

                            case ItemPosition.AltBottle:
                            {
                                if (pos != ItemPosition.Gourd)
                                    return false;
                                break;
                            }

                            case ItemPosition.AltGarment:
                            {
                                if (pos != ItemPosition.Garment)
                                    return false;
                                break;
                            }

                            case ItemPosition.AltFan:
                            {
                                if (pos != ItemPosition.AttackTalisman)
                                    return false;
                                break;
                            }

                            case ItemPosition.AltTower:
                            {
                                if (pos != ItemPosition.DefenceTalisman)
                                    return false;
                                break;
                            }

                            default:
                                return false;
                        }
                    }

                    if (position > ItemPosition.AltSteed)
                        return false;

                    Unequip(position);
                    break;
            }

            if (!RemoveFromInventory(item))
                return false;

            if (!m_equipment.TryAdd(position, item))
            {
                AddItem(item, false, true);
                return false;
            }

            item.UserChangeMap();
            item.Position = position;
            m_pUser.Send(item.CreateMsgItem(ItemAction.Equip, (uint) position));
            SendEquipmentInfo();
            m_pUser.Send(item.CreateMsgItemInfo(ItemMode.Update));
            item.SendPurification();
            item.TryUnlock();

            switch (item.Position)
            {
                case ItemPosition.Headwear:
                    m_pUser.Helmet = item.Type;
                    m_pUser.HelmetColor = item.Color;
                    if (item.ArtifactIsActive())
                        m_pUser.HelmetArtifact = item.ArtifactType;
                    break;
                case ItemPosition.Armor:
                    m_pUser.Armor = item.Type;
                    m_pUser.ArmorColor = item.Color;
                    if (item.ArtifactIsActive())
                        m_pUser.ArmorArtifact = item.ArtifactType;
                    break;
                case ItemPosition.RightHand:
                    m_pUser.RightHand = item.Type;
                    if (item.ArtifactIsActive())
                        m_pUser.RightHandArtifact = item.ArtifactType;
                    break;
                case ItemPosition.RightHandAccessory:
                    m_pUser.RightHandAccessory = item.Type;
                    break;
                case ItemPosition.LeftHand:
                    m_pUser.LeftHand = item.Type;
                    m_pUser.ShieldColor = item.Color;
                    if (item.ArtifactIsActive())
                        m_pUser.LeftHandArtifact = item.ArtifactType;
                    break;
                case ItemPosition.LeftHandAccessory:
                    m_pUser.LeftHandAccessory = item.Type;
                    break;
                case ItemPosition.Garment:
                    m_pUser.Garment = item.Type;
                    break;
                case ItemPosition.Steed:
                    m_pUser.Steed = item.Type;
                    m_pUser.SteedAddition = item.Addition;
                    m_pUser.SteedColor = item.SocketProgress;
                    break;
                case ItemPosition.SteedArmor:
                    m_pUser.SteedArmor = item.Type;
                    break;
            }

            m_pUser.UpdateAttributes();
            m_pUser.Screen.RefreshSpawnForObservers();
            return true;
        }

        public bool Unequip(ItemPosition position, RemovalType mode = RemovalType.RemoveOnly)
        {
            Item item = this[position];
            if (item == null)
                return false;

            if (position == ItemPosition.RightHand
                && this[ItemPosition.LeftHand] != null)
            {
                if (!IsPackSpare(2))
                    return false;

                if (!Unequip(ItemPosition.LeftHand))
                    return false;
            }
            else
            {
                if (!IsPackSpare(1))
                    return false;
            }

            m_equipment.TryRemove(position, out _);

            item.Position = ItemPosition.Inventory;
            m_pUser.Send(item.CreateMsgItem(ItemAction.Unequip, (uint) position));
            m_pUser.Send(item.CreateMsgItemInfo(ItemMode.Update));

            if (mode == RemovalType.RemoveOnly)
            {
                AddItem(item, false, true);
                item.Save();
            }
            else item.Delete();

            switch (position)
            {
                case ItemPosition.Headwear:
                    m_pUser.Helmet = 0;
                    m_pUser.HelmetColor = 0;
                    m_pUser.HelmetArtifact = 0;
                    break;
                case ItemPosition.Armor:
                    m_pUser.Armor = 0;
                    m_pUser.ArmorColor = 0;
                    m_pUser.ArmorArtifact = 0;
                    break;
                case ItemPosition.RightHand:
                    m_pUser.RightHand = 0;
                    m_pUser.RightHandArtifact = 0;
                    break;
                case ItemPosition.RightHandAccessory:
                    m_pUser.RightHandAccessory = 0;
                    break;
                case ItemPosition.LeftHand:
                    m_pUser.LeftHand = 0;
                    m_pUser.ShieldColor = 0;
                    m_pUser.LeftHandArtifact = 0;
                    break;
                case ItemPosition.LeftHandAccessory:
                    m_pUser.LeftHandAccessory = 0;
                    break;
                case ItemPosition.Garment:
                    m_pUser.Garment = 0;
                    break;
                case ItemPosition.Steed:
                    m_pUser.Steed = 0;
                    m_pUser.SteedAddition = 0;
                    m_pUser.SteedColor = 0;

                    if (m_pUser.QueryStatus(FlagInt.RIDING) != null)
                        m_pUser.DetachStatus(FlagInt.RIDING);
                    break;
                case ItemPosition.SteedArmor:
                    m_pUser.SteedArmor = 0;
                    break;
            }

            m_pUser.UpdateAttributes();
            SendEquipmentInfo();
            return true;
        }

        public bool RemoveFromInventory(uint idItem, RemovalType mode = RemovalType.RemoveOnly)
        {
            return m_inventory.TryGetValue(idItem, out Item item) && RemoveFromInventory(item, mode);
        }

        public bool RemoveFromInventory(Item item, RemovalType mode = RemovalType.RemoveOnly)
        {
            if (!m_inventory.TryRemove(item.Identity, out _))
                return false;

            switch (mode)
            {
                case RemovalType.RemoveAndDisappear:
                case RemovalType.DropItem:
                    m_pUser.Send(item.CreateMsgItem(ItemAction.Remove));
                    break;
                case RemovalType.Delete:
                    m_pUser.Send(item.CreateMsgItem(ItemAction.Remove));
                    item.Delete();
                    break;
            }

            return true;
        }

        public bool TryItem(uint idItem, ItemPosition position)
        {
            Item item = this[idItem];
            if (item == null)
                return false;

            if (item.RequiredLevel > m_pUser.Level)
                return false;

            if (item.RequiredGender > 0 && item.RequiredGender != m_pUser.Gender)
                return false;

            if (!item.IsMount() && item.Durability == 0 && !item.IsExpend())
                return false;

            if (m_pUser.Metempsychosis > 0 && m_pUser.Level >= 70)
                return true;

            if (item.RequiredProfession > 0)
            {
                int nRequireProfSort = item.RequiredProfession % 1000 / 10;
                int nRequireProfLevel = item.RequiredProfession % 10;
                int nProfSort = m_pUser.ProfessionSort;
                int nProfLevel = m_pUser.ProfessionLevel;

                if (nRequireProfSort == 19)
                {
                    if (nProfSort < 10 || position == ItemPosition.LeftHand)
                        return false;
                }
                else
                {
                    if (nRequireProfSort != nProfSort)
                        return false;
                }

                if (nProfLevel < nRequireProfLevel)
                    return false;
            }

            if (item.RequiredForce > m_pUser.Force
                || item.RequiredAgility > m_pUser.Agility
                || item.RequiredVitality > m_pUser.Vitality
                || item.RequiredSpirit > m_pUser.Spirit)
                return false;

            return true;
        }

        public bool UseItem(uint idItem, ItemPosition position)
        {
            if (!TryItem(idItem, position))
                return false;

            Item item = this[idItem];
            if (item == null)
                return false;

            if (item.Type == SpecialItem.MEMORY_AGATE)
            {
                item.SendCarry();
                return true;
            }

            if (item.Type == SpecialItem.TYPE_EXP_BALL)
            {
                if (!m_pUser.CanUseExpBall())
                    return false;

                m_pUser.IncrementExpBall();

                m_pUser.AwardExperience(m_pUser.CalculateExpBall());
                m_pUser.UserPackage.MultiSpendItem(item.Type, item.Type, 1);
                return true;
            }

            if (item.Type >= 1060020 && item.Type <= 1060039 || item.Type == 1060102)
            {
                if (m_pUser.Map.IsChgMapDisable())
                    return false;
                m_pUser.UserPackage.SpendItem(item);
            }
            else if (item.Type / 1000 == 1060)
                m_pUser.UserPackage.SpendItem(item);

            if (item.IsTaskItem())
            {
                m_pUser.GameAction.ProcessAction(item.Action, m_pUser, m_pUser, item, "");

                if (item.Type >= 725000 && item.Type <= 725050)
                    SpendItem(item);

                return true;
            }

            if (item.IsEquipEnable())
            {
                return EquipItem(item, position);
            }

            if (item.Type == 723726) // Life Fruit
            {
                if (!SpendItem(item))
                    return false;

                m_pUser.Life = m_pUser.MaxLife;
                m_pUser.Mana = m_pUser.MaxMana;
                return true;
            }

            if (item.IsMedicine() && m_pUser.QueryStatus(FlagInt.POISON_STAR) == null)
            {
                if (item.Life > 0 && m_pUser.Life == m_pUser.MaxLife ||
                    item.Mana > 0 && m_pUser.MaxMana == m_pUser.Mana)
                    return false;

                if (!SpendItem(item))
                    return false;

                if (item.Life > 0)
                    m_pUser.AddAttrib(ClientUpdateType.Hitpoints, item.Life);
                if (item.Mana > 0)
                    m_pUser.AddAttrib(ClientUpdateType.Mana, item.Mana);
                return true;
            }

            return false;
        }

        public bool AddItem(Item item, bool stack = true, bool bUpdate = false)
        {
            if (IsPackFull(item, stack))
                return false;

            if (item.StackAmount == 0)
                item.StackAmount = 1;

            item.PlayerIdentity = m_pUser.Identity;

            if (stack && item.StackLimit > 1)
            {
                Item combine = null;
                while ((combine = FindCombineItem(item)) != null && item.StackAmount > 0)
                {
                    ushort add = (ushort) Math.Min(item.StackAmount, combine.StackLimit - combine.StackAmount);
                    combine.StackAmount += add;
                    item.StackAmount -= add;
                    m_pUser.Send(combine.CreateMsgItemInfo(ItemMode.Update));
                }

                if (item.StackAmount == 0)
                {
                    if (!RemoveFromInventory(item, RemovalType.Delete))
                        item.Delete();
                    return true;
                }
            }

            item.Position = ItemPosition.Inventory;

            m_inventory.TryAdd(item.Identity, item);
            m_pUser.Send(item.CreateMsgItemInfo(bUpdate ? ItemMode.Update : ItemMode.Default));
            item.SendPurification();
            item.TryUnlock();
            return true;
        }

        public bool SpendItem(uint type, int nNum = 1, bool bSynchro = true)
        {
            return SpendItem(GetItemByType(type), nNum, bSynchro);
        }

        public bool SpendItem(Item item, int nNum = 1, bool bSynchro = true)
        {
            if (item == null)
                return false;

            if (item.IsPileEnable() && item.StackAmount > nNum)
            {
                item.StackAmount -= (ushort) nNum;
                if (bSynchro)
                    m_pUser.Send(item.CreateMsgItemInfo(ItemMode.Update));
            }
            else
            {
                if (item.IsPileEnable() && item.StackAmount < nNum)
                    return false;
                return item.Position == ItemPosition.Inventory &&
                       RemoveFromInventory(item.Identity, RemovalType.Delete);
            }

            return true;
        }

        public bool SplitItem(uint item, int nNum)
        {
            return m_inventory.TryGetValue(item, out var pItem) && SplitItem(pItem, nNum);
        }

        public bool SplitItem(Item item, int nNum)
        {
            if (item == null)
                return false;

            if (!item.IsCountable() && !item.IsExpend() || nNum <= 0 || nNum >= item.StackLimit)
                return false;

            if (IsPackFull(item, false))
                return false;

            Item other = item.Split(nNum);
            if (other == null)
                return false;

            m_pUser.AddItem(other);
            m_pUser.Send(item.CreateMsgItemInfo(ItemMode.Update));
            return true;
        }

        public bool CombineItem(uint idItem, uint idOther)
        {
            return m_inventory.TryGetValue(idItem, out var item)
                   && m_inventory.TryGetValue(idOther, out var other)
                   && CombineItem(item, other);
        }

        public bool CombineItem(Item item, Item other)
        {
            if (item == null || other == null || !item.IsPileEnable() || item.Type != other.Type)
                return false;

            ushort nNewNum = (ushort) (item.StackAmount + other.StackAmount);
            if (nNewNum > item.StackLimit)
            {
                item.StackAmount = (ushort) (nNewNum - other.StackLimit);
                other.StackAmount = item.StackLimit;
                m_pUser.Send(other.CreateMsgItemInfo(ItemMode.Update));
                m_pUser.Send(item.CreateMsgItemInfo(ItemMode.Update));
            }
            else
            {
                item.StackAmount = 0;
                RemoveFromInventory(item.Identity, RemovalType.Delete);
                other.StackAmount = nNewNum;
                m_pUser.Send(other.CreateMsgItemInfo(ItemMode.Update));
            }

            return true;
        }

        public Item FindCombineItem(Item item)
        {
            return m_inventory.Values.FirstOrDefault(x => x.Type == item.Type
                                                          && x.StackAmount < x.StackLimit
                                                          && item.IsBound == x.IsBound);
        }

        public int MeteorAmount()
        {
            int amount = 0;
            foreach (var item in GetInventory())
            {
                switch (item.Type)
                {
                    case SpecialItem.TYPE_METEOR:
                    case SpecialItem.TYPE_METEORTEAR:
                        amount++;
                        break;
                    case SpecialItem.TYPE_METEOR_SCROLL:
                        amount += 10;
                        break;
                }
            }

            return amount;
        }

        public int DragonBallAmount(bool bound = true, bool onlyBound = false)
        {
            int amount = 0;
            foreach (var item in GetInventory())
            {
                if (!bound && item.IsBound)
                    continue;
                if (!item.IsBound && onlyBound)
                    continue;

                switch (item.Type)
                {
                    case SpecialItem.TYPE_DRAGONBALL:
                        amount++;
                        break;
                    case SpecialItem.TYPE_DRAGONBALL_SCROLL:
                        amount += 10;
                        break;
                }
            }

            return amount;
        }

        public bool SpendMeteors(int amount)
        {
            if (amount > MeteorAmount())
                return false;

            List<Item> items = new List<Item>();
            int meteor = MultiGetItem(SpecialItem.TYPE_METEOR, SpecialItem.TYPE_METEORTEAR, 0, ref items);
            int meteorScroll =
                MultiGetItem(SpecialItem.TYPE_METEOR_SCROLL, SpecialItem.TYPE_METEOR_SCROLL, 0, ref items);

            int taken = 0;
            if (meteor >= amount)
            {
                foreach (var item in items.OrderBy(x => x.Type).Reverse())
                {
                    if (item.Type == SpecialItem.TYPE_METEOR || item.Type == SpecialItem.TYPE_METEORTEAR)
                    {
                        RemoveFromInventory(item, RemovalType.Delete);
                        items.Remove(item);
                        taken++;
                    }

                    if (taken >= amount)
                        break;
                }

                return taken >= amount;
            }

            if (amount % 10 > 0 && meteor % 10 >= amount % 10)
            {
                int take = amount % 10;
                foreach (var item in items.OrderBy(x => x.Type).Reverse())
                {
                    if (item.Type == SpecialItem.TYPE_METEOR || item.Type == SpecialItem.TYPE_METEORTEAR)
                    {
                        RemoveFromInventory(item, RemovalType.Delete);
                        items.Remove(item);
                        taken++;
                        take--;
                    }

                    if (take == 0)
                        break;
                }
            }

            int amountMS = (int) Math.Ceiling(amount / 10f);
            int returnMeteor = 0;
            if (amountMS * 10 > amount)
                returnMeteor = amountMS * 10 - amount;

            foreach (var item in items.Where(x => x.Type == SpecialItem.TYPE_METEOR_SCROLL).Reverse())
            {
                RemoveFromInventory(item, RemovalType.Delete);
                items.Remove(item);
                taken += 10;

                if (taken >= amount)
                {
                    while (returnMeteor > 0)
                    {
                        ItemEntity unpack = Item.CreateEntity(SpecialItem.TYPE_METEOR);
                        unpack.Monopoly = (byte) (item.IsBound ? 3 : 0);
                        AwardItem(unpack);

                        returnMeteor--;
                    }

                    return true;
                }
            }

            return true;
        }

        public bool SpendDragonBalls(int amount, bool allowBound = false)
        {
            if (amount > DragonBallAmount())
                return false;

            List<Item> items = new List<Item>();
            int db = MultiGetItem(SpecialItem.TYPE_DRAGONBALL, SpecialItem.TYPE_DRAGONBALL, 0, ref items);
            int dbScroll = MultiGetItem(SpecialItem.TYPE_DRAGONBALL_SCROLL, SpecialItem.TYPE_DRAGONBALL_SCROLL, 0,
                ref items);

            int taken = 0;
            if (db >= amount)
            {
                foreach (var item in items.OrderBy(x => x.Type).Reverse())
                {
                    if (!allowBound && item.IsBound)
                        continue;

                    if (item.Type == SpecialItem.TYPE_DRAGONBALL)
                    {
                        RemoveFromInventory(item, RemovalType.Delete);
                        items.Remove(item);
                        taken++;
                    }

                    if (taken >= amount)
                        break;
                }

                return taken >= amount;
            }

            if (amount % 10 > 0 && db % 10 >= amount % 10)
            {
                int take = amount % 10;
                foreach (var item in items.OrderBy(x => x.Type).Reverse())
                {
                    if (!allowBound && item.IsBound)
                        continue;

                    if (item.Type == SpecialItem.TYPE_DRAGONBALL)
                    {
                        RemoveFromInventory(item, RemovalType.Delete);
                        items.Remove(item);
                        taken++;
                        take--;
                    }

                    if (take == 0)
                        break;
                }
            }

            int amountDbs = (int) Math.Ceiling(amount / 10f);

            if (dbScroll < amountDbs)
                return false;

            int returnDb = 0;
            if (amountDbs * 10 > amount)
                returnDb = amountDbs * 10 - amount;

            foreach (var item in items.Where(x => x.Type == SpecialItem.TYPE_DRAGONBALL_SCROLL).Reverse())
            {
                if (!allowBound && item.IsBound)
                    continue;

                RemoveFromInventory(item, RemovalType.Delete);
                items.Remove(item);
                taken += 10;

                if (taken >= amount)
                {
                    while (returnDb > 0)
                    {
                        ItemEntity unpack = Item.CreateEntity(SpecialItem.TYPE_DRAGONBALL);
                        unpack.Monopoly = (byte) (item.IsBound ? 3 : 0);
                        AwardItem(unpack);

                        returnDb--;
                    }

                    return true;
                }
            }

            return true;
        }

        public bool MultiSpendItem(uint idFirst, uint idLast, int nNum, bool dontAllowBound = false)
        {
            int temp = nNum;
            List<Item> items = new List<Item>();
            if (MultiGetItem(idFirst, idLast, nNum, ref items, dontAllowBound) < nNum)
                return false;

            foreach (var item in items)
            {
                if (dontAllowBound && item.IsBound)
                    continue;

                if (item.IsPileEnable() && item.StackAmount > nNum)
                {
                    item.StackAmount -= (ushort) nNum;
                    m_pUser.Send(item.CreateMsgItemInfo(ItemMode.Update));
                    return true;
                }

                nNum -= item.StackAmount;
                RemoveFromInventory(item, RemovalType.Delete);
            }

            if (nNum > 0)
                Program.WriteLog(
                    $"Something went wrong when MultiSpendItem({idFirst}, {idLast}, {temp}) {nNum} left???");
            return nNum == 0;
        }

        public int MultiGetItem(uint idFirst, uint idLast, int nNum, ref List<Item> pItems, bool dontAllowBound = false)
        {
            int nAmount = 0;
            foreach (var item in m_inventory
                .Values
                .Where(x => x.Type >= idFirst && x.Type <= idLast)
                .OrderBy(x => x.Type)
                .ThenByDescending(x => x.StackAmount))
            {
                if (dontAllowBound && item.IsBound)
                    continue;

                pItems.Add(item);
                nAmount += Math.Max((ushort) 1, item.StackAmount);

                if (nNum > 0 && nAmount >= nNum)
                    return nAmount;
            }

            return nAmount;
        }

        public bool MultiCheckItem(uint idFirst, uint idLast, int nNum, bool dontAllowBound = false)
        {
            int nAmount = 0;
            foreach (var item in m_inventory.Values.Where(x => x.Type >= idFirst && x.Type <= idLast))
            {
                if (dontAllowBound && item.IsBound)
                    continue;

                nAmount += Math.Max((ushort) 1, item.StackAmount);
                if (nAmount >= nNum)
                    return true;
            }

            return nAmount >= nNum;
        }

        public int RandDropItem(int nMapType, int nChance)
        {
            if (m_pUser == null)
                return 0;
            int nDropNum = 0;
            foreach (var item in m_inventory.Values)
            {
                if (Calculations.ChanceCalc(nChance))
                {
                    if (item.IsNeverDropWhenDead())
                        continue;

                    switch (nMapType)
                    {
                        case 0: // NONE
                            break;
                        case 1: // PK
                        case 2: // SYN
                            if (!item.IsArmor())
                                continue;
                            break;
                        case 3: // PRISON
                            break;
                    }

                    var pos = new Point(m_pUser.MapX, m_pUser.MapY);
                    if (m_pUser.Map.FindDropItemCell(5, ref pos))
                    {
                        if (!RemoveFromInventory(item.Identity, RemovalType.DropItem))
                            continue;

                        item.PlayerIdentity = 0;
                        item.OwnerIdentity = m_pUser.Identity;

                        var pMapItem = new MapItem((uint) ServerKernel.RoleManager.FloorIdentity.GetNextIdentity);
                        if (pMapItem.Create(m_pUser.Map, pos, item, m_pUser.Identity))
                        {
                            ServerKernel.Log.GmLog("drop_item",
                                $"{m_pUser.Name}({m_pUser.Identity}) drop item:[id={item.Identity}, type={item.Type}], dur={item.Durability}, max_dur={item.MaxDurability}");
                        }

                        nDropNum++;
                    }
                }
            }

            return nDropNum;
        }

        public int RandDropItem(int nDropNum)
        {
            if (m_pUser == null)
                return 0;

            var temp = new List<Item>();
            foreach (var item in m_inventory.Values)
            {
                if (item.IsNeverDropWhenDead())
                    continue;
                temp.Add(item);
            }

            int nTotalItemCount = temp.Count;
            if (nTotalItemCount == 0)
                return 0;
            int nRealDropNum = 0;
            for (int i = 0; i < Math.Min(nDropNum, nTotalItemCount); i++)
            {
                int nIdx = Calculations.Random.Next(nTotalItemCount);
                Item item;
                try
                {
                    item = temp[nIdx];
                }
                catch
                {
                    continue;
                }

                var pos = new Point(m_pUser.MapX, m_pUser.MapY);
                if (m_pUser.Map.FindDropItemCell(5, ref pos))
                {
                    if (!RemoveFromInventory(item.Identity, RemovalType.DropItem))
                        continue;

                    item.PlayerIdentity = 0;
                    item.OwnerIdentity = m_pUser.Identity;

                    var pMapItem = new MapItem((uint) ServerKernel.RoleManager.FloorIdentity.GetNextIdentity);
                    if (pMapItem.Create(m_pUser.Map, pos, item, m_pUser.Identity))
                    {
                        ServerKernel.Log.GmLog("drop_item",
                            $"{m_pUser.Name}({m_pUser.Identity}) drop item:[id={item.Identity}, type={item.Type}], dur={item.Durability}, max_dur={item.MaxDurability}");
                    }

                    nRealDropNum++;
                }
            }

            return nRealDropNum;
        }

        public bool IsPackFull(Item item, bool stack = true)
        {
            if (stack && item.StackLimit > 1)
            {
                int nFree = 0;
                foreach (var temp in m_inventory.Values.Where(x =>
                    x.Type == item.Type && x.StackAmount < x.StackLimit))
                {
                    nFree += temp.StackLimit - temp.StackAmount;
                    if (nFree >= item.StackAmount)
                        return false;
                }
            }

            int nAmount = Math.Min(1, (int) Math.Ceiling(item.StackAmount / (float) item.StackLimit));
            return m_inventory.Count + nAmount > MAX_INVENTORY_CAPACITY;
        }

        public bool IsPackFull(uint type, int amount = 1)
        {
            if (!ServerKernel.Itemtype.TryGetValue(type, out ItemtypeEntity itemtype))
                return true;

            int nFree = 0;
            foreach (var temp in m_inventory.Values.Where(x =>
                x.Type == type && x.StackAmount < x.StackLimit))
            {
                nFree += temp.StackLimit - temp.StackAmount;
                if (nFree >= amount)
                    return false;
            }

            return nFree + (MAX_INVENTORY_CAPACITY - m_inventory.Count) * Math.Max(1u, itemtype.StackLimit) < amount;
        }

        public bool IsPackSpare(int size)
        {
            return m_inventory.Count + size <= MAX_INVENTORY_CAPACITY;
        }

        public void SaveAllInfo()
        {
            foreach (var equip in m_equipment.Values)
                equip.Save();
            foreach (var item in m_inventory.Values)
                item.Save();
        }

        public void SendAllItems()
        {
            foreach (var item in m_inventory.Values)
            {
                m_pUser.Send(item.CreateMsgItemInfo());
                item.SendPurification();
                item.TryUnlock();
            }

            MsgItem msg = new MsgItem();
            msg.Action = ItemAction.DisplayGears;
            foreach (var item in m_equipment.Values)
            {
                m_pUser.Send(item.CreateMsgItemInfo());
                item.SendPurification();
                item.TryUnlock();
                msg.Append(item.Position, item.Identity);
            }

            SetCharacter();
            m_pUser.Send(msg);
        }

        public void SendEquipmentInfo()
        {
            MsgItem msg = new MsgItem
            {
                Action = ItemAction.DisplayGears
            };
            foreach (var item in m_equipment.Values)
            {
                msg.Append(item.Position, item.Identity);
            }

            m_pUser.Send(msg);
        }

        public void SetCharacter()
        {
            foreach (var item in m_equipment.Values)
            {
                switch (item.Position)
                {
                    case ItemPosition.Headwear:
                        m_pUser.Helmet = item.Type;
                        m_pUser.HelmetColor = item.Color;
                        if (item.ArtifactIsActive())
                            m_pUser.HelmetArtifact = item.ArtifactType;
                        break;
                    case ItemPosition.Armor:
                        m_pUser.Armor = item.Type;
                        m_pUser.ArmorColor = item.Color;
                        if (item.ArtifactIsActive())
                            m_pUser.ArmorArtifact = item.ArtifactType;
                        break;
                    case ItemPosition.RightHand:
                        m_pUser.RightHand = item.Type;
                        if (item.ArtifactIsActive())
                            m_pUser.RightHandArtifact = item.ArtifactType;
                        break;
                    case ItemPosition.RightHandAccessory:
                        m_pUser.RightHandAccessory = item.Type;
                        break;
                    case ItemPosition.LeftHand:
                        m_pUser.LeftHand = item.Type;
                        m_pUser.ShieldColor = item.Color;
                        if (item.ArtifactIsActive())
                            m_pUser.LeftHandArtifact = item.ArtifactType;
                        break;
                    case ItemPosition.LeftHandAccessory:
                        m_pUser.LeftHandAccessory = item.Type;
                        break;
                    case ItemPosition.Garment:
                        m_pUser.Garment = item.Type;
                        break;
                    case ItemPosition.Steed:
                        m_pUser.Steed = item.Type;
                        m_pUser.SteedAddition = item.Addition;
                        m_pUser.SteedColor = item.SocketProgress;
                        break;
                    case ItemPosition.SteedArmor:
                        m_pUser.SteedArmor = item.Type;
                        break;
                }
            }
        }

        public List<Item> GetInventory()
        {
            return m_inventory.Values.ToList();
        }

        public List<Item> GetAllEquipment()
        {
            return m_equipment.Values.ToList();
        }

        public List<Item> GetEquipment()
        {
            bool isAlternative = m_pUser.IsAlternativeEquipment;
            List<Item> items = new List<Item>();
            foreach (var item in m_equipment.Values)
            {
                switch (item.Position)
                {
                    case ItemPosition.Headwear:
                    {
                        if (isAlternative && this[ItemPosition.AltHead] != null)
                            continue;
                        break;
                    }

                    case ItemPosition.Necklace:
                    {
                        if (isAlternative && this[ItemPosition.AltNecklace] != null)
                            continue;
                        break;
                    }

                    case ItemPosition.Ring:
                    {
                        if (isAlternative && this[ItemPosition.AltRing] != null)
                            continue;
                        break;
                    }

                    case ItemPosition.RightHand:
                    {
                        if (isAlternative && this[ItemPosition.AltWeaponR] != null)
                            continue;
                        break;
                    }

                    case ItemPosition.LeftHand:
                    {
                        if (isAlternative && this[ItemPosition.AltWeaponL] != null)
                            continue;
                        break;
                    }

                    case ItemPosition.Armor:
                    {
                        if (isAlternative && this[ItemPosition.AltArmor] != null)
                            continue;
                        break;
                    }

                    case ItemPosition.Boots:
                    {
                        if (isAlternative && this[ItemPosition.AltBoots] != null)
                            continue;
                        break;
                    }

                    case ItemPosition.Gourd:
                    {
                        if (isAlternative && this[ItemPosition.AltBottle] != null)
                            continue;
                        break;
                    }

                    case ItemPosition.Garment:
                    {
                        if (isAlternative && this[ItemPosition.AltGarment] != null)
                            continue;
                        break;
                    }

                    case ItemPosition.AttackTalisman:
                    {
                        if (isAlternative && this[ItemPosition.AltFan] != null)
                            continue;
                        break;
                    }

                    case ItemPosition.DefenceTalisman:
                    {
                        if (isAlternative && this[ItemPosition.AltTower] != null)
                            continue;
                        break;
                    }

                    case ItemPosition.AltHead:
                    case ItemPosition.AltNecklace:
                    case ItemPosition.AltRing:
                    case ItemPosition.AltWeaponR:
                    case ItemPosition.AltWeaponL:
                    case ItemPosition.AltArmor:
                    case ItemPosition.AltBoots:
                    case ItemPosition.AltBottle:
                    case ItemPosition.AltSteed:
                    case ItemPosition.AltGarment:
                    case ItemPosition.AltFan:
                    case ItemPosition.AltTower:
                    {
                        if (!isAlternative)
                            continue;
                        break;
                    }
                }

                items.Add(item);
            }

            Item leftHand = items.FirstOrDefault(x =>
                x.Position == ItemPosition.LeftHand || x.Position == ItemPosition.AltWeaponL);
            Item rightHand = items.FirstOrDefault(x =>
                x.Position == ItemPosition.RightHand || x.Position == ItemPosition.AltWeaponR);
            if (leftHand != null && rightHand != null)
            {
                if (leftHand.IsShield()
                    && (rightHand.IsBackswordType() || rightHand.IsWeaponProBased()))
                {
                    items.Remove(leftHand);
                    Unequip(leftHand.Position);
                }

                if (leftHand.IsShield()
                    && rightHand.IsWeaponTwoHand()
                    && (!m_pUser.IsPureClass() || m_pUser.Magics[MagicData.PURE_WARRIOR_ID] == null))
                {
                    items.Remove(leftHand);
                    Unequip(leftHand.Position);
                }
            }

            return items;
        }

        public Item GetEquipById(uint id)
        {
            return m_equipment.Values.FirstOrDefault(x => x.Identity == id);
        }

        public Item GetItemByType(uint type)
        {
            return m_inventory.Values.FirstOrDefault(x => x.Type == type);
        }

        public List<Item> GetAll()
        {
            List<Item> ret = new List<Item>(m_equipment.Values);
            ret.AddRange(m_inventory.Values);
            return ret;
        }

        public Item Find(uint identity)
        {
            foreach (var warehouse in m_pUser.Warehouses.Values)
            {
                if (warehouse[identity] != null)
                    return warehouse[identity];
            }

            return m_equipment.Values.FirstOrDefault(x => x.Identity == identity) ?? m_inventory.Values.FirstOrDefault(x => x.Identity == identity);
        }
    }

    public enum RemovalType
    {
        RemoveAndDisappear,
        RemoveOnly,
        Delete,
        DropItem
    }
}