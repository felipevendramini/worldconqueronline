﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Artifact.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common;
using FtwCore.Database.Entities;

#endregion

namespace GameServer.Structures.Items
{
    public struct Artifact
    {
        public bool Avaiable;
        public uint ItemIdentity;
        public uint ArtifactType;
        public byte ArtifactLevel;
        public uint ArtifactExpireTime;
        public uint ArtifactStartTime;
        public uint StabilizationPoints;
        public ItemtypeEntity ArtifactItemtype;
        public bool HasExpired => ArtifactExpireTime < UnixTimestamp.Now();
    }
}