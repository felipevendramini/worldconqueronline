﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - EventsManager.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/11/12 11:02
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using FtwCore.Common;

#endregion

namespace GameServer.Structures.Managers
{
    public interface IGameEvent
    {
        string Name { get; }

        long LastUpdateTime { get; }

        void OnTimer();
    }

    public sealed class EventsManager
    {
        private readonly List<IGameEvent> m_lEvents = new List<IGameEvent>();

        public void Initialize()
        {
        }

        public void OnTimer(long nTime)
        {
            foreach (var @event in m_lEvents)
                try
                {
                    @event.OnTimer();
                }
                catch (Exception ex)
                {
                    Program.WriteLog($"Error occured on {@event.Name}", LogType.ERROR);
                    Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
                }

            var nEndTime = UnixTimestamp.NowMs();
            if (nEndTime - nTime > 1000)
                Program.WriteLog($"Events Timer took {nEndTime - nTime}ms to complete processing", LogType.WARNING);
        }
    }
}