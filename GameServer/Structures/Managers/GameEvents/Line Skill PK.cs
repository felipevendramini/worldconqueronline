﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Line Skill PK.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/11/12 11:13
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Concurrent;
using System.Drawing;
using System.Linq;
using FtwCore;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using GameServer.Structures.Entities;
using GameServer.Structures.Items;
using GameServer.World;

#endregion

namespace GameServer.Structures.Managers.GameEvents
{
    public class LineSkillPkTournament : IGameEvent
    {
        public const int DISQUALIFY_AMOUNT = 1000;

        public const uint MAP_ID_U = 7505;
        private const string _ERROR_FILE = "lineskillpkt_error";
        private const uint _RANK_TYPE_U = 10;
        public static GeneralDynamicRankingRepository Repository;

        public static uint[] MoneyReward =
        {
            15000000,
            10000000,
            5000000,
            2000000
        };

        public static uint[] EmoneyReward =
        {
            2150,
            1075,
            645,
            215
        };

        private readonly Map m_pMap;

        private readonly ConcurrentDictionary<uint, LineSkillPkStatistic> m_pStatistic =
            new ConcurrentDictionary<uint, LineSkillPkStatistic>();

        private readonly TimeOut m_pTableTimeOut = new TimeOut(10);
        private GameEventState m_pState = GameEventState.Error;

        public LineSkillPkTournament()
        {
            Name = "Line Skill PK Tournament";
            Repository = new GeneralDynamicRankingRepository();

            if (!ServerKernel.Maps.TryGetValue(MAP_ID_U, out m_pMap))
            {
                Program.WriteLog("ERROR MAP NOT FOUND LINE SKILL PK", LogType.ERROR);
                return;
            }

            var allRank = Repository.FetchByType(_RANK_TYPE_U);
            if (allRank != null)
            {
                foreach (var rank in allRank.Where(x => x.PlayerIdentity > 0))
                {
                    LineSkillPkStatistic obj = new LineSkillPkStatistic(rank);
                    m_pStatistic.TryAdd(rank.PlayerIdentity, obj);
                }
            }

            m_pState = GameEventState.Idle;
        }

        public bool IsReady => m_pState > GameEventState.Error;

        public string Name { get; }

        public long LastUpdateTime { get; private set; }

        public void OnTimer()
        {
            if (m_pState == GameEventState.Error) return;

            DateTime now = DateTime.Now;
            int nNow = int.Parse(now.ToString("HHmmss"));

            switch (m_pState)
            {
                case GameEventState.Idle:
                {
                    if (nNow % 10000 < 1000 || nNow % 10000 >= 2000)
                        return;

                    m_pState = GameEventState.Starting;
                    break;
                }
                case GameEventState.Starting:
                {
                    foreach (var plr in m_pStatistic.Values)
                    {
                        plr.HitsDealtNow = 0;
                        plr.HitsTakenNow = 0;
                    }

                    ServerKernel.UserManager.SendToAllUser(Language.StrLineSPKTStarted, Color.White);
                    m_pState = GameEventState.Running;
                    break;
                }
                case GameEventState.Running:
                {
                    if (nNow % 10000 >= 1000
                        && nNow % 10000 < 2000)
                    {
                        if (m_pTableTimeOut.ToNextTime())
                            SendTable();
                    }
                    else
                    {
                        m_pState = GameEventState.Finishing;
                    }

                    break;
                }
                case GameEventState.Finishing:
                {
                    foreach (var plr in m_pStatistic.Values
                        .Where(x => x.HitsDealtNow > 0 || x.HitsTakenNow > 0)
                        .OrderByDescending(x => x.HitsDealtNow)
                        .ThenBy(x => x.HitsTakenNow))
                    {
                        Character player = ServerKernel.UserManager.GetUser(plr.Identity);

                        uint gold = 0;
                        uint emoney = 0;
                        try
                        {
                            switch (GetRank(plr.Identity))
                            {
                                case 1:
                                    gold = MoneyReward[0];
                                    emoney = EmoneyReward[0];
                                    ServerKernel.UserManager.SendToAllUser(
                                        string.Format(Language.StrLineSPKT1stPlace, plr.Name), ChatTone.Talk);

                                    TitleEntity title = new TitleEntity
                                    {
                                        Title = (byte) UserTitle.LineSkillPkTournament,
                                        Userid = plr.Identity,
                                        Timestamp = (uint) (UnixTimestamp.Now() + UnixTimestamp.TIME_SECONDS_HOUR)
                                    };
                                    new TitleRepository().Save(title);
                                    if (player != null)
                                    {
                                        player.Titles.TryAdd(UserTitle.LineSkillPkTournament, title);
                                        player.Send(new MsgTitle
                                        {
                                            Action = TitleAction.AddTitle,
                                            Identity = player.Identity,
                                            SelectedTitle = UserTitle.LineSkillPkTournament
                                        });
                                        player.Send(new MsgTitle
                                        {
                                            Action = TitleAction.SelectTitle,
                                            Identity = player.Identity,
                                            SelectedTitle = UserTitle.LineSkillPkTournament
                                        });
                                        player.Title = UserTitle.LineSkillPkTournament;
                                    }

                                    player.UserPackage.AwardItem(Item.CreateEntity(720730));
                                    player.UserPackage.AwardItem(Item.CreateEntity(720730));
                                    player.UserPackage.AwardItem(Item.CreateEntity(720730));
                                    break;
                                case 2:
                                    gold = MoneyReward[1];
                                    emoney = EmoneyReward[1];
                                    ServerKernel.UserManager.SendToAllUser(
                                        string.Format(Language.StrLineSPKT2ndPlace, plr.Name), ChatTone.Talk);
                                    player.UserPackage.AwardItem(Item.CreateEntity(720730));
                                    player.UserPackage.AwardItem(Item.CreateEntity(720730));
                                    break;
                                case 3:
                                    gold = MoneyReward[2];
                                    emoney = EmoneyReward[2];
                                    ServerKernel.UserManager.SendToAllUser(
                                        string.Format(Language.StrLineSPKT3rdPlace, plr.Name), ChatTone.Talk);
                                    player.UserPackage.AwardItem(Item.CreateEntity(720730));
                                    break;
                                default:
                                    gold = MoneyReward[3];
                                    emoney = EmoneyReward[3];
                                    break;
                            }
                        }
                        catch
                        {
                            Program.WriteLog("ERROR MESSAGE GET PRIZE LINE SKILL", LogType.ERROR);
                        }

                        if (player != null)
                        {
                            player.AwardMoney((int) gold);
                            player.AwardEmoney((int) emoney, EmoneySourceType.Event, null);
                            player.SendMessage(string.Format(Language.StrLineSPKTReward, gold, emoney),
                                Color.White, ChatTone.Talk);
                        }
                        else
                        {
                            CharacterEntity user = Database.CharacterRepository.SearchByIdentity(plr.Identity);
                            if (user == null)
                                continue;
                            if (user.Money + gold > int.MaxValue)
                                user.Money = int.MaxValue;
                            else
                                user.Money += gold;
                            if (user.Emoney + emoney > int.MaxValue)
                                user.Emoney = int.MaxValue;
                            else
                                user.Emoney += emoney;
                            Database.CharacterRepository.Save(user);
                        }
                    }

                    m_pState = GameEventState.Ended;
                    break;
                }
                case GameEventState.Ended:
                {
                    foreach (var plr in m_pMap.PlayerSet.Values)
                    {
                        plr.FlyMap(plr.RecordMapIdentity, plr.RecordMapX, plr.RecordMapY);
                    }

                    ServerKernel.UserManager.SendToAllUser(Language.StrLineSPKTEnded);
                    m_pState = GameEventState.Idle;
                    break;
                }
            }

            LastUpdateTime = UnixTimestamp.NowMs();
        }

        public int GetRank(uint idMember)
        {
            int pos = 1;
            foreach (var pls in m_pStatistic.Values
                .Where(x => x.HitsDealtNow > 0 || x.HitsTakenNow > 0)
                .OrderByDescending(x => x.HitsDealtNow)
                .ThenBy(x => x.HitsTakenNow))
            {
                if (pls.Name.Contains("[GM]") || pls.Name.Contains("[PM]"))
                    continue;
                if (pls.Identity == idMember)
                    return pos;
                pos++;
            }

            return 999;
        }

        private void AwardPoint(Character pRole, int point)
        {
            LineSkillPkStatistic stt;
            if (!m_pStatistic.TryGetValue(pRole.Identity, out stt))
            {
                stt = new LineSkillPkStatistic(pRole);
                m_pStatistic.TryAdd(pRole.Identity, stt);
            }

            if (point > 0)
            {
                stt.HitsDealt += 1;
                stt.HitsDealtNow += 1;
            }
            else if (point < 0)
            {
                stt.HitsTaken += 1;
                stt.HitsTakenNow += 1;
            }
        }

        public void Hit(Character pAtker, Character pTarget)
        {
            if (pAtker == null || pTarget == null || pTarget == pAtker || pTarget.Owner.IpAddress == pAtker.Owner.IpAddress)
                return;

            AwardPoint(pAtker, 1);
            AwardPoint(pTarget, -1);

            LineSkillPkStatistic target = m_pStatistic.Values.FirstOrDefault(x => x.Identity == pTarget.Identity);

            if (target?.HitsTakenNow >= 1000)
            {
                pTarget.FlyMap(1002, 430, 378);
                pTarget.SendMessage(string.Format(Language.StrLineSPKTDisqualified, DISQUALIFY_AMOUNT), Color.White,
                    ChatTone.Talk);
            }
        }

        public bool IsEnterEnable(Character pTarget)
        {
            LineSkillPkStatistic target = m_pStatistic.Values.FirstOrDefault(x => x.Identity == pTarget.Identity);
            if (target == null) return true; // ??
            return target.HitsTakenNow < DISQUALIFY_AMOUNT;
        }

        public void SendTable()
        {
            m_pMap.SendMessageToMap("Line Skill Tournament - DT(Dealt/Taken)", ChatTone.EventRanking);
            int nRank = 0;
            foreach (var plr in m_pStatistic.Values.OrderByDescending(x => x.HitsDealtNow)
                .ThenBy(x => x.HitsTakenNow))
            {
                if (nRank++ >= 8)
                    break;
                m_pMap.SendMessageToMap(
                    $"Nº {nRank} - {plr.Name,16} - {plr.KDA:0.00}({plr.HitsDealtNow}/{plr.HitsTakenNow})",
                    ChatTone.EventRankingNext);
            }

            foreach (var plr in m_pStatistic.Values)
            {
                Character user = ServerKernel.UserManager.GetUser(plr.Identity);
                if (user?.MapIdentity == MAP_ID_U)
                {
                    user.SendMessage($"You: {plr.KDA:0.00}({plr.HitsDealtNow},{plr.HitsTakenNow})",
                        ChatTone.EventRankingNext);
                    user.SendMessage($"Lifetime Statistic: {plr.LifetimeKda:0.00}({plr.HitsDealt}/{plr.HitsTaken})",
                        ChatTone.EventRankingNext);
                }
            }
        }
    }

    public sealed class LineSkillPkStatistic
    {
        private readonly GeneralDynamicRankingEntity m_pDynaRank;

        public LineSkillPkStatistic(Character pUser)
        {
            m_pDynaRank = new GeneralDynamicRankingEntity
            {
                PlayerName = pUser.Name,
                PlayerIdentity = pUser.Identity,
                RankType = LineSkillPkTournament.MAP_ID_U,
                ObjectIdentity = 0,
                ObjectName = "NONE"
            };

            Save();
        }

        public LineSkillPkStatistic(GeneralDynamicRankingEntity pRank)
        {
            m_pDynaRank = pRank;
        }

        public uint Identity => m_pDynaRank.PlayerIdentity;
        public string Name => m_pDynaRank.PlayerName;

        public int HitsDealt
        {
            get => (int) m_pDynaRank.Value3;
            set
            {
                m_pDynaRank.Value3 = value;
                Save();
            }
        }

        public int HitsTaken
        {
            get => (int) m_pDynaRank.Value4;
            set
            {
                m_pDynaRank.Value4 = value;
                Save();
            }
        }

        public int HitsDealtNow
        {
            get => (int) m_pDynaRank.Value1;
            set
            {
                m_pDynaRank.Value1 = value;
                Save();
            }
        }

        public int HitsTakenNow
        {
            get => (int) m_pDynaRank.Value2;
            set
            {
                m_pDynaRank.Value2 = value;
                Save();
            }
        }

        public float LifetimeKda
        {
            get
            {
                if (HitsTaken == 0)
                    return HitsDealt;
                if (HitsTaken < 0)
                    return 0;
                return HitsDealt / (float) HitsTaken;
            }
        }

        public float KDA => HitsDealtNow / Math.Max(1f, HitsTakenNow);

        public bool Save()
        {
            return LineSkillPkTournament.Repository.Save(m_pDynaRank);
        }
    }
}