﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - JiangHuManager.cs
// Last Edit: 2019/12/16 18:57
// Created: 2019/12/16 18:56
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Concurrent;

#endregion

namespace GameServer.Structures.Managers
{
    public static class JiangHuManager
    {
        private static ConcurrentDictionary<uint, JiangHu> m_cdicKongFu = new ConcurrentDictionary<uint, JiangHu>();

        public static void Initialize()
        {

        }

        public static JiangHu Get(uint idUser) => m_cdicKongFu.TryGetValue(idUser, out JiangHu value) ? value : null;
    }
}