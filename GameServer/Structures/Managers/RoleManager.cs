﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - RoleManager.cs
// Last Edit: 2019/12/07 22:50
// Created: 2019/12/07 18:33
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Concurrent;
using System.Linq;
using FtwCore.Common.Enums;
using GameServer.Structures.Entities;
using GameServer.Structures.Items;

#endregion

namespace GameServer.Structures.Managers
{
    public sealed class RoleManager
    {
        public IdentityGenerator FloorIdentity;

        private readonly ConcurrentDictionary<uint, ScreenObject> m_pRoles = new ConcurrentDictionary<uint, ScreenObject>();

        public IdentityGenerator MonsterIdentity;
        public IdentityGenerator TrapIdentity;

        public RoleManager()
        {
            MonsterIdentity = new IdentityGenerator(IdentityRange.MONSTERID_FIRST, IdentityRange.MONSTERID_LAST);
            FloorIdentity = new IdentityGenerator(IdentityRange.MAPITEM_FIRST, IdentityRange.MAPITEM_LAST);
            TrapIdentity = new IdentityGenerator(IdentityRange.TRAPID_FIRST, IdentityRange.TRAPID_LAST);
        }

        public int MonsterCount => m_pRoles.Values.Count(x => x is Monster);

        public int MapItemCount => m_pRoles.Values.Count(x => x is MapItem);

        public int NpcCount => m_pRoles.Values.Count(x => x is BaseNpc);

        public int Count => m_pRoles.Count;

        public bool AddRole(ScreenObject role)
        {
            if (role is Character)
                return false;
            if (m_pRoles.TryAdd(role.Identity, role))
            {
                role.EnterMap();
                return true;
            }

            return false;
        }

        public ScreenObject GetRole(uint idRole)
        {
            return m_pRoles.TryGetValue(idRole, out ScreenObject result) ? result : null;
        }

        public bool GetRole(uint idRole, out ScreenObject role)
        {
            return m_pRoles.TryGetValue(idRole, out role);
        }

        public bool GetRole(uint idRole, out Character user)
        {
            return (user = ServerKernel.UserManager.GetUser(idRole)) != null;
        }

        public int CountByName(string name, uint idMap = 0)
        {
            int count = 0;
            foreach (var mob in m_pRoles.Values.Where(x => x is Role).Cast<Role>())
            {
                if (!mob.IsAlive)
                    continue;

                if (idMap == 0 && mob.Name == name)
                {
                    count++;
                    continue;
                }

                if (idMap == mob.MapIdentity && mob.Name == name)
                {
                    count++;
                }
            }

            return count;
        }

        public bool RemoveRole(uint idRole)
        {
            if (m_pRoles.TryRemove(idRole, out var role))
            {
                role.LeaveMap();
                if (role is MapItem)
                    FloorIdentity.ReturnIdentity(idRole);
                else if (role is Monster monster)
                {
                    MonsterIdentity.ReturnIdentity(idRole);
                    monster.Life = 0;
                    monster.DisappearNow = true;
                }

                return true;
            }

            return false;
        }

        public void OnTimer()
        {
            foreach (var obj in m_pRoles.Values)
            {
                if (obj is Monster monster)
                {
                    if (monster.Map?.PlayerSet.Count == 0) // && !(monster.IsGuard() && monster.IsRighteous()))
                        continue;
                }

                obj.OnTimer();
            }
        }
    }
}