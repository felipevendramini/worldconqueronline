﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - SyndicateManager.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using GameServer.Structures.Groups.Syndicates;
using GameServer.Structures.Items;

#endregion

namespace GameServer.Structures.Managers
{
    public sealed class SyndicateManager
    {
        private ConcurrentDictionary<uint, Syndicate> m_dicSyndicates;
        private ConcurrentDictionary<uint, SyndicateMember> m_dicSyndicateMembers;

        public SyndicateManager()
        {
        }

        public void LoadSyndicates()
        {
            #region Loading Syndicates

            ActionRepository action = new ActionRepository(MysqlTargetConnection.Game);
            action.ExecutePureQuery("UPDATE cq_item SET inscribed=0");

            m_dicSyndicates = new ConcurrentDictionary<uint, Syndicate>();
            m_dicSyndicateMembers = new ConcurrentDictionary<uint, SyndicateMember>();

            SyndicateRepository synRepo = new SyndicateRepository();
            SyndicateMemberRepository synMemRepo = new SyndicateMemberRepository();
            CharacterRepository userRepo = new CharacterRepository();

            ICollection<SyndicateEntity> allSyndicate = synRepo.FetchAll();
            if (allSyndicate != null)
            {
                foreach (var cqSyndicate in allSyndicate)
                {
                    var syn = new Syndicate();

                    if (!syn.Create(cqSyndicate))
                    {
                        ServerKernel.Log.SaveLog($"Could not create syndicate [{syn.Identity}]");
                        continue;
                    }

                    if (!m_dicSyndicates.TryAdd(syn.Identity, syn))
                    {
                        ServerKernel.Log.SaveLog($"Could not add to dictionary syndicate [{syn.Identity}]");
                        continue;
                    }

                    ICollection<SyndicateMemberEntity> allMembers = synMemRepo.FetchBySyndicate(syn.Identity);
                    foreach (var member in allMembers)
                    {
                        var user = userRepo.SearchByIdentity(member.Id);
                        if (user == null)
                            continue;

                        var nMember = new SyndicateMember(syn);
                        if (!nMember.Create(member, user))
                            continue;

                        nMember.Name = user.Name;
                        nMember.Profession = user.Profession;
                        nMember.Level = user.Level;
                        nMember.ArsenalDonation = 0;

                        switch (nMember.Position)
                        {
                            case SyndicateRank.DeputyLeader:
                                syn.DeputyLeaderCount += 1;
                                break;
                            case SyndicateRank.HonoraryDeputyLeader:
                                syn.HonoraryDeputyLeaderCount += 1;
                                break;
                            case SyndicateRank.HonoraryManager:
                                syn.HonoraryManagerCount += 1;
                                break;
                            case SyndicateRank.HonorarySupervisor:
                                syn.HonorarySupervisorCount += 1;
                                break;
                            case SyndicateRank.HonorarySteward:
                                syn.HonoraryStewardCount += 1;
                                break;
                            case SyndicateRank.Aide:
                                syn.AideCount += 1;
                                break;
                            case SyndicateRank.LeaderSpouse:
                                syn.LeaderSpouseAideCount += 1;
                                break;
                            case SyndicateRank.DeputyLeaderAide:
                                syn.DeputyLeaderAideCount += 1;
                                break;
                            case SyndicateRank.ManagerAide:
                                syn.ManagerAideCount += 1;
                                break;
                            case SyndicateRank.SupervisorAide:
                                syn.SupervisorAideCount += 1;
                                break;
                        }

                        if (!m_dicSyndicates[syn.Identity].Members.ContainsKey(nMember.Identity))
                            if (!m_dicSyndicates[syn.Identity].Members.TryAdd(nMember.Identity, nMember))
                                ServerKernel.Log.SaveLog($"Could not load member [{nMember.Identity}] to syn [{nMember.SyndicateIdentity}]");

                        if (!m_dicSyndicateMembers.ContainsKey(nMember.Identity))
                            m_dicSyndicateMembers.TryAdd(nMember.Identity, nMember);
                    }

                    while (syn.KingCount() > 1)
                    {
                        syn.KickKingMember(true);
                    }

                    syn.MemberCount = (ushort)syn.Members.Count;
                    syn.Save();

                    // load arsenals
                    if (syn.Arsenal == null)
                        syn.Arsenal = new Arsenal(syn);

                    // open totem pole (arsenal holders)
                    if (syn.TotemHeadwear)
                        syn.Arsenal.Poles.TryAdd(TotemPoleType.TOTEM_HEADGEAR,
                            new TotemPole(TotemPoleType.TOTEM_HEADGEAR) { Locked = false });

                    if (syn.TotemNecklace)
                        syn.Arsenal.Poles.TryAdd(TotemPoleType.TOTEM_NECKLACE,
                            new TotemPole(TotemPoleType.TOTEM_NECKLACE) { Locked = false });

                    if (syn.TotemRing)
                        syn.Arsenal.Poles.TryAdd(TotemPoleType.TOTEM_RING,
                            new TotemPole(TotemPoleType.TOTEM_RING) { Locked = false });

                    if (syn.TotemWeapon)
                        syn.Arsenal.Poles.TryAdd(TotemPoleType.TOTEM_WEAPON,
                            new TotemPole(TotemPoleType.TOTEM_WEAPON) { Locked = false });

                    if (syn.TotemArmor)
                        syn.Arsenal.Poles.TryAdd(TotemPoleType.TOTEM_ARMOR,
                            new TotemPole(TotemPoleType.TOTEM_ARMOR) { Locked = false });

                    if (syn.TotemBoots)
                        syn.Arsenal.Poles.TryAdd(TotemPoleType.TOTEM_BOOTS,
                            new TotemPole(TotemPoleType.TOTEM_BOOTS) { Locked = false });

                    if (syn.TotemHeavenFan)
                        syn.Arsenal.Poles.TryAdd(TotemPoleType.TOTEM_FAN,
                            new TotemPole(TotemPoleType.TOTEM_FAN) { Locked = false });

                    if (syn.TotemStarTower)
                        syn.Arsenal.Poles.TryAdd(TotemPoleType.TOTEM_TOWER,
                            new TotemPole(TotemPoleType.TOTEM_TOWER) { Locked = false });

                    ICollection<SyndicateTotemEntity> allTotem = new SyndicateTotemRepository().GetBySyndicate(syn.Identity);
                    foreach (var totem in allTotem)
                    {
                        ItemEntity cqItem = Database.ItemRepository.FetchByIdentity(totem.Itemid);
                        if (cqItem == null)
                            continue;

                        var item = new Item();
                        item.Create(null, cqItem);
                        item.IsInscribed = true;

                        var tClient = new Totem(totem, item);
                        if (syn.Members.TryGetValue(totem.Userid, out var pMember))
                            pMember.ArsenalDonation += tClient.Donation();

                        syn.Arsenal.AddItem(item, tClient);
                    }

                    syn.Arsenal.UpdatePoles();

                    syn.Level = 1;

                    foreach (var pole in syn.Arsenal.Poles.Values)
                        if (pole.Donation >= 5000000)
                            syn.Level += 1;

                    // load member positions
                    var memberList = new List<SyndicateMember>(810);
                    foreach (var member in syn.Members.Values.Where(x => !x.IsUserSetPosition()))
                    {
                        member.Position = SyndicateRank.Member;
                        memberList.Add(member);
                    }

                    uint amount = 0;
                    uint maxamount = syn.MaxPositionAmount(SyndicateRank.Manager);

                    List<SyndicateMember> remove = new List<SyndicateMember>();
                    #region Manager
                    foreach (var member in memberList.OrderByDescending(x => x.TotalDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.Manager;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }

                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion

                    #region Rose Supervisor

                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.RoseSupervisor);

                    foreach (var member in memberList.OrderByDescending(c => c.RedRoseDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.RoseSupervisor;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }

                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region White Rose Supervisor

                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.LilySupervisor);

                    foreach (var member in memberList.OrderByDescending(x => x.WhiteRoseDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.LilySupervisor;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }

                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region Orchid Supervisor
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.OrchidSupervisor);

                    foreach (var member in memberList.OrderByDescending(x => x.OrchidDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.OrchidSupervisor;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }

                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region Tulip Supervisor
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.TulipSupervisor);

                    foreach (var member in memberList.OrderByDescending(x => x.TulipDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.TulipSupervisor;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }

                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region Pk Supervisor
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.PkSupervisor);

                    foreach (var member in memberList.OrderByDescending(x => x.PkDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.PkSupervisor;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }

                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region Guide Supervisor
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.GuideSupervisor);

                    foreach (var member in memberList.OrderByDescending(x => x.GuideDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.GuideSupervisor;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }

                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region Silver Supervisor
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.SilverSupervisor);

                    foreach (var member in memberList.OrderByDescending(x => x.SilverDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.SilverSupervisor;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }

                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region CPs Supervisor
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.CpSupervisor);

                    foreach (var member in memberList.OrderByDescending(x => x.EmoneyDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.CpSupervisor;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }

                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region Arsenal Supervisor
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.ArsenalSupervisor);

                    foreach (var member in memberList.OrderByDescending(x => x.ArsenalDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.ArsenalSupervisor;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }

                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region Supervisor
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.Supervisor);

                    foreach (var member in memberList.OrderByDescending(x => x.TotalDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.Supervisor;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }

                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion

                    #region Steward
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.Steward);

                    foreach (var member in memberList.OrderByDescending(x => x.TotalDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.Steward;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }

                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region Deputy Steward

                    foreach (var member in memberList.Where(x => x.TotalDonation >= 170000))
                    {
                        member.Position = SyndicateRank.DeputySteward;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }

                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();

                    #endregion

                    #region Rose Agent

                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.RoseAgent);

                    foreach (var member in memberList.OrderByDescending(x => x.RedRoseDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.RoseAgent;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }

                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region White Rose Agent

                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.LilyAgent);

                    foreach (var member in memberList.OrderByDescending(x => x.WhiteRoseDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.LilyAgent;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }
                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region Orchid Agent
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.OrchidAgent);

                    foreach (var member in memberList.OrderByDescending(x => x.OrchidDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.OrchidAgent;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }
                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region Tulip Agent
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.TulipAgent);

                    foreach (var member in memberList.OrderByDescending(x => x.TulipDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.TulipAgent;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }
                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region Pk Agent
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.PkAgent);

                    foreach (var member in memberList.OrderByDescending(x => x.PkDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.PkAgent;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }
                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region Guide Agent
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.GuideAgent);

                    foreach (var member in memberList.OrderByDescending(x => x.GuideDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.GuideAgent;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }
                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region Silver Agent
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.SilverAgent);

                    foreach (var member in memberList.OrderByDescending(x => x.SilverDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.SilverAgent;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }
                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region CPs Agent
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.CpAgent);

                    foreach (var member in memberList.OrderByDescending(x => x.EmoneyDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.CpAgent;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }
                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region Arsenal Agent
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.ArsenalAgent);

                    foreach (var member in memberList.OrderByDescending(x => x.ArsenalDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.ArsenalAgent;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }
                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region Agent
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.Agent);

                    foreach (var member in memberList.OrderByDescending(x => x.TotalDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.Agent;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }
                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion

                    #region Rose Follower

                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.RoseFollower);

                    foreach (var member in memberList.OrderByDescending(x => x.RedRoseDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.RoseFollower;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }

                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region White Rose Follower

                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.LilyFollower);

                    foreach (var member in memberList.OrderByDescending(x => x.WhiteRoseDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.LilyFollower;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }

                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region Orchid Follower
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.OrchidFollower);

                    foreach (var member in memberList.OrderByDescending(x => x.OrchidDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.OrchidFollower;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }
                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region Tulip Follower
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.TulipFollower);

                    foreach (var member in memberList.OrderByDescending(x => x.TulipDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.TulipFollower;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }
                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region Pk Follower
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.PkFollower);

                    foreach (var member in memberList.OrderByDescending(x => x.PkDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.PkFollower;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }
                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region Guide Follower
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.GuideFollower);

                    foreach (var member in memberList.OrderByDescending(x => x.GuideDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.GuideFollower;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }
                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region Silver Follower
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.SilverFollower);

                    foreach (var member in memberList.OrderByDescending(x => x.SilverDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.SilverFollower;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }
                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region CPs Follower
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.CpFollower);

                    foreach (var member in memberList.OrderByDescending(x => x.EmoneyDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.CpFollower;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }
                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region Arsenal Follower
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.ArsenalFollower);

                    foreach (var member in memberList.OrderByDescending(x => x.ArsenalDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.ArsenalFollower;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }
                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                    #region Follower
                    amount = 0;
                    maxamount = syn.MaxPositionAmount(SyndicateRank.Follower);

                    foreach (var member in memberList.OrderByDescending(x => x.TotalDonation))
                    {
                        if (amount >= maxamount)
                            break;

                        member.Position = SyndicateRank.Follower;
                        member.Save();
                        amount++;
                        remove.Add(member);
                    }
                    foreach (var rem in remove)
                        memberList.Remove(rem);
                    remove.Clear();
                    #endregion
                }

                // all the guilds loaded, we get the allies and enemies
                foreach (var syn in m_dicSyndicates.Values.Where(x => !x.IsDeleted))
                {
                    ICollection<SyndicateAllyEntity> allies = new SyndicateAllyRepository().FetchBySyndicate(syn.Identity);
                    ICollection<SyndicateEnemyEntity> enemies = new SyndicateEnemyRepository().FetchBySyndicate(syn.Identity);

                    foreach (var ally in allies)
                    {
                        var sSyn = m_dicSyndicates.Values.FirstOrDefault(x => x.Identity == ally.Allyid && !x.IsDeleted);
                        if (sSyn == null) continue;

                        if (!sSyn.Allies.TryAdd(syn.Identity, syn) && !syn.Allies.TryAdd(sSyn.Identity, sSyn))
                            Program.WriteLog($"WARNING: Could not find ally [{ally.Allyid}][{ally.Allyname}] for guild [{syn.Identity}][{syn.Name}]");
                    }

                    foreach (var enemy in enemies)
                    {
                        var sSyn = m_dicSyndicates.Values.FirstOrDefault(x => x.Identity == enemy.Enemyid && !x.IsDeleted);
                        if (sSyn == null) continue;

                        if (!syn.Enemies.TryAdd(sSyn.Identity, sSyn))
                            Program.WriteLog($"WARNING: Could not find enemy [{enemy.Enemyid}][{enemy.Enemyname}] for guild [{syn.Identity}][{syn.Name}]");
                    }
                }
            }

            #endregion
        }

        public ConcurrentDictionary<uint, Syndicate> Syndicates => new ConcurrentDictionary<uint, Syndicate>(m_dicSyndicates);

        public Syndicate GetSyndicate(uint idSyn)
        {
            return m_dicSyndicates.TryGetValue(idSyn, out var syn) ? syn : null;
        }

        public Syndicate GetSyndicate(string name)
        {
            return m_dicSyndicates.Values.FirstOrDefault(x => x.Name == name);
        }

        public SyndicateMember GetMember(uint idMember)
        {
            return m_dicSyndicateMembers.TryGetValue(idMember, out var member) ? member : null;
        }

        public SyndicateMember GetMember(string name)
        {
            return m_dicSyndicateMembers.Values.FirstOrDefault(x => x.Name == name);
        }

        public bool AddNewSyndicate(Syndicate syn)
        {
            if (m_dicSyndicates.ContainsKey(syn.Identity))
                return false;
            return m_dicSyndicates.TryAdd(syn.Identity, syn);
        }

        public bool AddSyndicateMember(SyndicateMember member)
        {
            return m_dicSyndicateMembers.TryAdd(member.Identity, member);
        }

        public void RemoveSyndicateMember(uint idMember)
        {
            m_dicSyndicateMembers.TryRemove(idMember, out _);
        }

        public void OnTimer()
        {

        }
    }
}