﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Identity Generator.cs
// Last Edit: 2019/11/24 19:04
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Concurrent;
using System.Linq;
using System.Threading;

#endregion

namespace GameServer.Structures.Managers
{
    public sealed class IdentityGenerator
    {
        private readonly ConcurrentQueue<long> m_cqidQueue = new ConcurrentQueue<long>();
        private readonly long m_idMax = uint.MaxValue;
        private readonly long m_idMin;
        private long m_idNext;


        public IdentityGenerator(long min, long max)
        {
            m_idNext = m_idMin = min;
            m_idMax = max;

            for (long i = m_idMin; i <= m_idMax; i++)
            {
                m_cqidQueue.Enqueue(i);
            }

            m_idNext = m_idMax + 1;
        }

        public long GetNextIdentity
        {
            get
            {
                if (!m_cqidQueue.IsEmpty)
                {
                    if (m_cqidQueue.TryDequeue(out long result))
                        return result;
                    return m_idNext < m_idMax ? Interlocked.Increment(ref m_idNext) : 0;
                }

                return m_idNext < m_idMax ? Interlocked.Increment(ref m_idNext) : 0;
            }
        }

        public void ReturnIdentity(long id)
        {
            if (!m_cqidQueue.Contains(id))
                m_cqidQueue.Enqueue(id);
        }
    }
}