﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Program.cs
// Last Edit: 2019/12/18 15:38
// Created: 2019/12/07 18:33
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using FtwCore.Common;
using FtwCore.Database;
using FtwCore.Database.Migrations;
using FtwCore.Database.Repositories;
using FtwCore.Security;
using FtwCore.Win32Events;
using GameServer.Networking.GameServer;
using GameServer.Structures.Entities;
using GameServer.Threads;

#endregion

namespace GameServer
{
    internal static class Program
    {
        public const int ERR_WSAEREFUSED = 0x2780;

        private const string _GAME_SERVER_CONFIG_S = "GameServer.xml";
        private const string _ACCOUNT_SERVER_CONFIG_S = "AccountServer.xml";

        public static InputConsoleBox StatisticBox = new InputConsoleBox(0, 9);

        private static InputConsoleBox m_outputBox = new InputConsoleBox(9, 16)
        {
            AutoDraw = true
        };

        private static InputConsoleBox m_inputBox = new InputConsoleBox(26, 1)
        {
            InputPrompt = "Command: "
        };

        private static int Main()
        {
            Console.Title = @"Game server is now loading...";

            Maximize();

            StatisticBox = new InputConsoleBox(0, 9);
            m_outputBox = new InputConsoleBox(9, (short) (Console.WindowHeight - 10))
            {
                AutoDraw = true
            };
            m_inputBox = new InputConsoleBox((short) (Console.WindowHeight - 1), 1)
            {
                InputPrompt = "Command: "
            };

            ConsoleWriter writer = new ConsoleWriter();
            writer.WriteEvent += WriterOnWriteEvent;
            writer.WriteLineEvent += WriterOnWriteEvent;
            Console.SetOut(writer);

            StatisticBox.Write("".PadRight(Console.BufferWidth, '='));
            StatisticBox.WriteLine("Server statistic will be disponible in a while...");
            StatisticBox.WriteLine("Server will now:");
            StatisticBox.WriteLine("\tConnect to database");
            StatisticBox.WriteLine("\tInitialize game database and events");
            StatisticBox.WriteLine("\tSet up threads for inner processing");
            StatisticBox.WriteLine("\tSet up the login and game server socket");
            StatisticBox.WriteLine("\tAllow console commands and game connections");
            StatisticBox.WriteLine("Then your server will be ready to go! :D This can take a few seconds...");
            StatisticBox.Write("".PadRight(Console.BufferWidth, '='));
            StatisticBox.Draw();

            #region Server information logging

            WriteLog("Project WConquer: Conquer Online Private Server Emulator");
            WriteLog("Developed by Felipe Vieira (FTW! Masters)");
            WriteLog("April 21th, 2019 - All Rights Reserved");

            WriteLog(Environment.CurrentDirectory, LogType.DEBUG);
            WriteLog($"Computer Name: {Environment.MachineName}", LogType.DEBUG);
            WriteLog($"User Name: {Environment.UserName}", LogType.DEBUG);
            WriteLog($"System Directory: {Environment.SystemDirectory}", LogType.DEBUG);
            WriteLog("Some environment variables:", LogType.DEBUG);
            WriteLog($"OS: {Environment.OSVersion.VersionString}", LogType.DEBUG);
            WriteLog($"NUMBER_OF_PROCESSORS: {Environment.ProcessorCount}", LogType.DEBUG);
            WriteLog($"PROCESSOR_ARCHITETURE: {(Environment.Is64BitProcess ? "x64" : "x86")}", LogType.DEBUG);
            WriteLog($"WorkingSet: {Environment.WorkingSet}", LogType.DEBUG);

            HardwareLogging.DoLog();

            #endregion

            if (!File.Exists("GameServer.xml") || !File.Exists("AccountServer.xml"))
            {
                WriteLog("Could not find GameServer.xml or AccountServer.xml in the server folder.", LogType.ERROR);
                Console.ReadLine();
                return 1;
            }

            ReadXmlConfig();

            if (!SessionFactory.StartAccountConnection(
                ServerKernel.AccountDbConfig.Host,
                ServerKernel.AccountDbConfig.User,
                ServerKernel.AccountDbConfig.Pass,
                ServerKernel.AccountDbConfig.Database,
                ServerKernel.AccountDbConfig.Port
            ))
            {
                WriteLog("Could not connect to account database server!", LogType.ERROR);
                WriteLog(SessionFactory.LastException, LogType.EXCEPTION);
                Console.ReadLine();
                return ERR_WSAEREFUSED;
            }

            if (!SessionFactory.StartLogConnection(
                ServerKernel.LogDbConfig.Host,
                ServerKernel.LogDbConfig.User,
                ServerKernel.LogDbConfig.Pass,
                ServerKernel.LogDbConfig.Database,
                ServerKernel.LogDbConfig.Port
            ))
            {
                WriteLog("Could not connect to log database server!", LogType.ERROR);
                Console.ReadLine();
                return ERR_WSAEREFUSED;
            }

            if (!SessionFactory.StartResourceConnection(
                ServerKernel.ResourceDbConfig.Host,
                ServerKernel.ResourceDbConfig.User,
                ServerKernel.ResourceDbConfig.Pass,
                ServerKernel.ResourceDbConfig.Database,
                ServerKernel.ResourceDbConfig.Port
            ))
            {
                WriteLog("Could not connect to resource database server!", LogType.ERROR);
                Console.ReadLine();
                return ERR_WSAEREFUSED;
            }

            if (!SessionFactory.StartGameConnection(
                ServerKernel.GameDbConfig.Host,
                ServerKernel.GameDbConfig.User,
                ServerKernel.GameDbConfig.Pass,
                ServerKernel.GameDbConfig.Database,
                ServerKernel.GameDbConfig.Port
            ))
            {
                WriteLog("Could not connect to game database server!", LogType.ERROR);
                Console.ReadLine();
                return ERR_WSAEREFUSED;
            }

            if (!Database.Initialize())
            {
                WriteLog("Something went wrong when loading server data from database.", LogType.ERROR);
                Console.ReadLine();
                return 0;
            }

            ServerKernel.Analytics = new Analytics();
            ThreadManager.Init();

            WriteLog("Server will now start to listen...");
            ServerKernel.GameSocket = new GameSocket();
            ServerKernel.GameSocket.Bind("0.0.0.0", 5816);
            ServerKernel.GameSocket.Listen(5);

            new ActionRepository(MysqlTargetConnection.Account).ExecutePureQuery(
                $"INSERT INTO ftw_cq_gameserverlog VALUES (0, '{ServerKernel.Version}','{DateTime.Now:yyyy-MM-dd HH:mm:ss}')");

            WriteLog("Server is now ready to login!");

            ConsoleHandle();

            ServerKernel.UserManager.LogoutAllUser();
            ThreadManager.Stop();
            WriteLog("Server will exit in 3 seconds");
            for (int i = 3; i > 0; i--)
            {
                WriteLog($"Exiting in {i}...");
                Thread.Sleep(1000);
            }

            writer.Close();
            writer.Dispose();
            ServerKernel.GameSocket.Dispose();
            return 0;
        }

        private static void WriterOnWriteEvent(object sender, ConsoleWriterEventArgs e)
        {
            m_outputBox.WriteLine(e.Value);
        }

        private static void ConsoleHandle()
        {
            string command = string.Empty;
            while ((command = m_inputBox.ReadLine())?.ToLower() != "exit")
            {
                if (string.IsNullOrEmpty(command))
                    continue;

                WriteLog(command, LogType.CONSOLE, false);

                string[] pszCommand = command.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                if (pszCommand.Length <= 0)
                    continue;

                switch (pszCommand[0].ToLower())
                {
                    case "kickout":
                        if (pszCommand.Length < 2)
                        {
                            Console.WriteLine(@"kickout [character_name]");
                            break;
                        }

                        Character user = ServerKernel.UserManager.GetUser(pszCommand[1]);
                        if (user != null)
                        {
                            ServerKernel.UserManager.KickoutSocket(user, "CONSOLE_COMMAND");
                        }
                        else
                        {
                            Console.WriteLine(@"User not found.");
                        }

                        break;
                    case "players":
                        Console.WriteLine(@"=".PadRight(Console.BufferWidth));
                        foreach (var player in ServerKernel.UserManager.Players.Values
                            .OrderBy(x => x.IsGm()))
                        {
                            Console.WriteLine(
                                $@"|  {player.Identity.ToString(),10}  |  {player.Name,16}  |  {player.Level.ToString(),3}  |  {player.Map.Name,24}({player.MapX.ToString(),3},{player.MapY.ToString(),3})  |");
                        }

                        Console.WriteLine(@"=".PadRight(Console.BufferWidth));
                        break;
                }
            }
        }

        private static void ReadXmlConfig()
        {
            MyXml xml = new MyXml(_GAME_SERVER_CONFIG_S);
            string hostname = xml.GetStr("GameServerConfiguration", "GameMySQL", "Hostname");
            string username = xml.GetStr("GameServerConfiguration", "GameMySQL", "Username");
            string password = xml.GetStr("GameServerConfiguration", "GameMySQL", "Password");
            string database = xml.GetStr("GameServerConfiguration", "GameMySQL", "Database");
            string szPort = xml.GetStr("GameServerConfiguration", "GameMySQL", "Port");
            if (ushort.TryParse(szPort, out ushort usPort))
                usPort = 3306;
            ServerKernel.GameDbConfig = new MySqlConfig(hostname, username, password, database, usPort);

            hostname = xml.GetStr("GameServerConfiguration", "LogMySQL", "Hostname");
            username = xml.GetStr("GameServerConfiguration", "LogMySQL", "Username");
            password = xml.GetStr("GameServerConfiguration", "LogMySQL", "Password");
            database = xml.GetStr("GameServerConfiguration", "LogMySQL", "Database");
            szPort = xml.GetStr("GameServerConfiguration", "LogMySQL", "Port");
            if (ushort.TryParse(szPort, out usPort))
                usPort = 3306;
            ServerKernel.LogDbConfig = new MySqlConfig(hostname, username, password, database, usPort);

            hostname = xml.GetStr("GameServerConfiguration", "ResourceMySQL", "Hostname");
            username = xml.GetStr("GameServerConfiguration", "ResourceMySQL", "Username");
            password = xml.GetStr("GameServerConfiguration", "ResourceMySQL", "Password");
            database = xml.GetStr("GameServerConfiguration", "ResourceMySQL", "Database");
            szPort = xml.GetStr("GameServerConfiguration", "ResourceMySQL", "Port");
            if (ushort.TryParse(szPort, out usPort))
                usPort = 3306;
            ServerKernel.ResourceDbConfig = new MySqlConfig(hostname, username, password, database, usPort);

            ServerKernel.BlowfishKey = xml.GetStr("GameServerConfiguration", "BlowfishKey");
            ServerKernel.TransferKey = xml.GetStr("GameServerConfiguration", "TransferKey", "Key");
            ServerKernel.TransferSalt = xml.GetStr("GameServerConfiguration", "TransferKey", "Salt");

            ServerKernel.AccountServerAddress =
                xml.GetStr("GameServerConfiguration", "AccountServer", "AccountAddress");

            Cast5Cipher.InitialKey = Encoding.ASCII.GetBytes(ServerKernel.BlowfishKey);

            if (!int.TryParse(xml.GetStr("GameServerConfiguration", "AccountServer", "AccountPort"),
                out ServerKernel.AccountServerPort))
                ServerKernel.AccountServerPort = 9865;

            ServerKernel.ServerName = xml.GetStr("GameServerConfiguration", "AccountServer", "ServerName");
            ServerKernel.AccountUsername = xml.GetStr("GameServerConfiguration", "AccountServer", "LoginName");
            ServerKernel.AccountPassword = xml.GetStr("GameServerConfiguration", "AccountServer", "Password");
            ServerKernel.MaxOnlinePlayers =
                uint.Parse(xml.GetStr("GameServerConfiguration", "AccountServer", "MaxOnlinePlayers"));
            ServerKernel.MaxLoginOvertime =
                uint.Parse(xml.GetStr("GameServerConfiguration", "AccountServer", "LoginOvertimeSecs"));

            xml = new MyXml(_ACCOUNT_SERVER_CONFIG_S);
            hostname = xml.GetStr("AccountServerConfiguration", "MySQL", "Hostname");
            username = xml.GetStr("AccountServerConfiguration", "MySQL", "Username");
            password = xml.GetStr("AccountServerConfiguration", "MySQL", "Password");
            database = xml.GetStr("AccountServerConfiguration", "MySQL", "Database");
            szPort = xml.GetStr("AccountServerConfiguration", "MySQL", "Port");
            if (ushort.TryParse(szPort, out usPort))
                usPort = 3306;
            ServerKernel.AccountDbConfig = new MySqlConfig(hostname, username, password, database, usPort);
        }

        public static void WriteLog(string text, LogType type = LogType.MESSAGE, bool bOutput = true)
        {
            string msg = ServerKernel.Log.SaveLog(text, LogWriter.STR_SYSLOG_GAMESERVER, type);
            m_outputBox.WriteLine(msg);
        }

        #region Maximize 

        [DllImport("kernel32.dll", ExactSpelling = true)]
        private static extern IntPtr GetConsoleWindow();

        private static readonly IntPtr s_thisConsole = GetConsoleWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        private const int HIDE = 0;
        private const int MAXIMIZE = 3;
        private const int MINIMIZE = 6;
        private const int RESTORE = 9;

        private static void Maximize()
        {
            ShowWindow(s_thisConsole, MAXIMIZE); //SW_MAXIMIZE = 3
            Console.SetBufferSize(Console.WindowWidth, Console.WindowHeight);
        }

        #endregion
    }
}