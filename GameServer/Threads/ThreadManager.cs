﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - ThreadManager.cs
// Last Edit: 2019/11/26 19:19
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using FtwCore.Common;
using FtwCore.Networking.Packets;
using FtwCore.Networking.Sockets;
using FtwCore.Win32Events;
using GameServer.Networking.LoginClient;
using GameServer.Structures.Actions;
using GameServer.Structures.Login;

#endregion

namespace GameServer.Threads
{
    public static class ThreadManager
    {
        private static readonly Thread m_uiThread = new Thread(UiManager) {Priority = ThreadPriority.BelowNormal};
        private static Thread m_userManager = new Thread(UserManager) {Priority = ThreadPriority.AboveNormal};
        private static Thread m_gameManager = new Thread(GameManager) {Priority = ThreadPriority.Normal};
        private static Thread m_generatorManager = new Thread(GeneratorManager) {Priority = ThreadPriority.BelowNormal};
        private static Thread m_aiThread = new Thread(AiManager) {Priority = ThreadPriority.Normal};
        private static readonly AutomaticEvents Events = new AutomaticEvents();

        public static DateTime UserManagerLastUpdate = DateTime.Now;
        public static DateTime GameManagerLastUpdate = DateTime.Now;
        public static DateTime GeneratorLastUpdate = DateTime.Now;
        public static DateTime AiLastUpdate = DateTime.Now;

        public static long UserManagerMs;
        public static long GameManagerMs;
        public static long GeneratorManagerMs;
        public static long AiManagerMs;

        public static bool StopThreading = false;

        public static ServerThreads RunningThreads = ServerThreads.None;

        public static void Init()
        {
            m_userManager.Start();
            m_gameManager.Start();
            m_generatorManager.Start();
            m_aiThread.Start();
            m_uiThread.Start();

            Events.StartCheck();
        }

        public static void Stop()
        {
            StopThreading = true;
            Events.Stop();
            while (!CanClose())
            {
                Thread.Sleep(150);
            }
        }

        public static bool CanClose()
        {
            return RunningThreads == ServerThreads.None;
        }

        private static void UiManager()
        {
            const string windowTitle = "[{0}] Conquer Online Game Server - Server Time: {2} - Version: {1}";
            string[,] headerStrings =
            {
                { "Online Time: {0} days, {1} hours, {2} minutes and {3} seconds"                    , "Players Online: {0} (max: {1})" },
                { "SentPackets: {0:N0}, RecvPackets: {1:N0}"  , "Generators: {0} (AI Count: {1})" },
                { "SentBytes: {0}, RecvBytes: {1}"      , "Total Roles: {0}" },
                { "UserThread: {0} {1}ms"               , "" },
                { "AiThread: {0} {1}ms"                 , "" },
                { "GeneratorThread: {0} {1}ms"          , "" },
                { "GameThread: {0} {1}ms"               , "" },
            };

            TimeOut time = new TimeOut(1);
            TimeOutMS loginMs = new TimeOutMS(800);
            TimeOut login = new TimeOut(1);

            RunningThreads |= ServerThreads.UiThread;

            while (true)
            {
                try
                {
                    int width = Program.StatisticBox.Width;//Console.BufferWidth - Console.BufferWidth % 2;
                    string[] lines =
                    {
                        $"||{{0,-{(width/2)-3}}}||{{1,-{(width/2)-3}}}||",
                        $"||{{0,-{(width-4)}}}||"
                    };

                    DateTime now = DateTime.Now;
                    var interval = now - ServerKernel.Analytics.StartTime;
                    if (time.ToNextTime()) // title and analytics
                    {
                        Console.Title = string.Format(windowTitle, ServerKernel.ServerName, ServerKernel.Version,
                            now.ToString("dd-mm-yyyy HH:mm:ss"));

                        Program.StatisticBox.Write("".PadRight(width, '=')); // first line 0
                        
                        Program.StatisticBox.Write(string.Format(lines[0], string.Format(headerStrings[0, 0], interval.Days, interval.Hours, interval.Minutes, interval.Seconds),
                            string.Format(headerStrings[0, 1], ServerKernel.UserManager.Count, Analytics.Statistic.MaxOnlinePlayers)));
                        
                        Program.StatisticBox.Write(string.Format(lines[0], string.Format(headerStrings[1, 0], Analytics.Statistic.SentPackets, Analytics.Statistic.RecvPackets),
                            string.Format(headerStrings[1, 1], ServerKernel.Generators.Count, ServerKernel.RoleManager.MonsterCount)));
                        
                        Program.StatisticBox.Write(string.Format(lines[0], string.Format(headerStrings[2, 0], Calculations.FormatDataSize(Analytics.Statistic.SentBytes), Calculations.FormatDataSize(Analytics.Statistic.RecvBytes)), 
                            string.Format(headerStrings[2, 1], ServerKernel.RoleManager.Count)));
                        
                        Program.StatisticBox.Write(string.Format(lines[0], string.Format(headerStrings[3, 0], UserManagerLastUpdate.ToString("HH:mm:ss"), UserManagerMs), 
                            string.Format(headerStrings[3, 1])));
                        
                        Program.StatisticBox.Write(string.Format(lines[0], string.Format(headerStrings[4, 0], AiLastUpdate.ToString("HH:mm:ss"), AiManagerMs), 
                            string.Format(headerStrings[4, 1])));
                        
                        Program.StatisticBox.Write(string.Format(lines[0], string.Format(headerStrings[5, 0], GeneratorLastUpdate.ToString("HH:mm:ss"), GeneratorManagerMs), 
                            string.Format(headerStrings[5, 1])));
                        
                        Program.StatisticBox.Write(string.Format(lines[0], string.Format(headerStrings[6, 0], GameManagerLastUpdate.ToString("HH:mm:ss"), GameManagerMs), 
                            string.Format(headerStrings[6, 1])));
                        
                        Program.StatisticBox.Write("".PadRight(width, '=')); // last line 8
                        Program.StatisticBox.Draw();

                        bool bThreadStuck = false;
                        if ((DateTime.Now - UserManagerLastUpdate).TotalSeconds > 5)
                        {
                            Program.WriteLog("User thread has not been responding for more than 5 seconds",
                                LogType.WARNING);
                            new MyStackTrace("GameServer").DoLog();
                            bThreadStuck = true;
                        }

                        if ((DateTime.Now - AiLastUpdate).TotalSeconds > 5)
                        {
                            Program.WriteLog("AI thread has not been responding for more than 5 seconds",
                                LogType.WARNING);
                            new MyStackTrace("GameServer").DoLog();
                            bThreadStuck = true;
                        }

                        if ((DateTime.Now - GameManagerLastUpdate).TotalSeconds > 5)
                        {
                            Program.WriteLog("Game thread has not been responding for more than 5 seconds",
                                LogType.WARNING);
                            new MyStackTrace("GameServer").DoLog();
                            bThreadStuck = true;
                        }

                        if ((DateTime.Now - GeneratorLastUpdate).TotalSeconds > 5)
                        {
                            Program.WriteLog("Generator thread has not been responding for more than 5 seconds",
                                LogType.WARNING);
                            new MyStackTrace("GameServer").DoLog();
                            bThreadStuck = true;
                        }

                        if (bThreadStuck)
                        {
                            // reset all threads? not sure if idea is good, lets give a try
                            m_userManager.Abort();
                            m_generatorManager.Abort();
                            m_aiThread.Abort();
                            m_gameManager.Abort();

                            m_userManager = new Thread(UserManager) { Priority = ThreadPriority.AboveNormal };
                            m_gameManager = new Thread(GameManager) { Priority = ThreadPriority.Normal };
                            m_generatorManager = new Thread(GeneratorManager) { Priority = ThreadPriority.BelowNormal };
                            m_aiThread = new Thread(AiManager) { Priority = ThreadPriority.Normal };

                            m_userManager.Start();
                            m_gameManager.Start();
                            m_generatorManager.Start();
                            m_aiThread.Start();
                        }

                        ServerKernel.Analytics.OnTimer(UnixTimestamp.Now());
                    }

                    // handle socket connection
                    if (ServerKernel.LoginServer == null && loginMs.ToNextTime())
                    {
                        Program.WriteLog("Connect to the account server...");

                        try
                        {
                            var pSocket = new LoginSocket();
                            pSocket.ConnectTo(ServerKernel.AccountServerAddress, ServerKernel.AccountServerPort);
                            ServerKernel.LoginServer = new LoginUser(pSocket, pSocket, null);
                            try
                            {
                                Program.WriteLog("Connected to the account server!");
                                var pMsg = new MsgLoginSvAuthRequest(ServerKernel.HelloSendString);
                                ServerKernel.LoginServer.Send(pMsg);
                            }
                            catch
                            {
                                ServerKernel.LoginServer = null;
                                pSocket.Dispose();
                                pSocket = null;
                            }
                        }
                        catch (SocketException ex)
                        {
                            if (ex.ErrorCode != 10061 && ex.ErrorCode != 10057)
                            {
                                Program.WriteLog("Exception thrown while trying to connect to login server",
                                    LogType.ERROR);
                                Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
                            }
                        }
                    }

                    // clean the login request list
                    if (ServerKernel.LoginServer != null)
                    {
                        if (login.ToNextTime() && ServerKernel.LoginQueue.Count > 0)
                        {
                            List<LoginRequest> temp = ServerKernel.LoginQueue.Values.Where(x => x.IsExpired()).ToList();
                            foreach (var pReq in temp)
                                ServerKernel.LoginQueue.TryRemove(pReq.AccountIdentity, out _);
                        }
                    }
                }
                catch (Exception ex)
                {
                }

                if (StopThreading)
                    break;
            }

            SetThreadEnded(ServerThreads.UiThread);
        }

        private static void UserManager()
        {
            RunningThreads |= ServerThreads.UserThread;

            UserManagerLastUpdate = DateTime.Now;
            Stopwatch perf = new Stopwatch();
            while (true)
            {
                try
                {
                    perf.Start();
                    ServerKernel.UserManager?.OnTimer();
                    UserManagerLastUpdate = DateTime.Now;
                }
                catch (Exception ex)
                {
                    Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
                }
                finally
                {
                    perf.Stop();
                    UserManagerMs = perf.ElapsedMilliseconds;
                    perf = new Stopwatch();
                }

                if (StopThreading)
                    break;

                Thread.Sleep(80);
            }

            SetThreadEnded(ServerThreads.UserThread);
            Program.WriteLog("User thread has exit...", LogType.WARNING);
        }

        private static void GameManager()
        {
            RunningThreads |= ServerThreads.GameThread;

            GameManagerLastUpdate = DateTime.Now;
            TimeOut synRecruit = new TimeOut(60);
            synRecruit.Update();

            TimeOut pigeon = new TimeOut(1);
            pigeon.Update();

            TimeOutMS arena = new TimeOutMS(1000);
            TimeOut player = new TimeOut(5);
            TimeOut item = new TimeOut(1);

            Stopwatch perf = new Stopwatch();
            while (true)
            {
                try
                {
                    perf.Start();
                    if (synRecruit.ToNextTime())
                        ServerKernel.SyndicateRecruitment.CheckSyndicates();
                    if (pigeon.ToNextTime())
                        ServerKernel.Pigeon.OnTimer();
                    if (arena.ToNextTime())
                    {
                        ServerKernel.ArenaQualifier.OnTimer();

                        if (ServerKernel.LineSkillPk?.IsReady == true)
                            ServerKernel.LineSkillPk.OnTimer();

                        ServerKernel.CaptureTheFlag.OnTimer();

                        foreach (var map in ServerKernel.Maps.Values)
                        {
                            map.Weather.OnTimer();
                        }
                    }

                    if (player.ToNextTime())
                    {
                        foreach (var map in ServerKernel.Maps.Values.Where(x => x.PlayerSet.Count > 0))
                        {
                            foreach (var user in map.PlayerSet.Values)
                            {
                                if (ServerKernel.UserManager.GetUser(user.Identity) == null ||
                                    user?.Owner?.UserState >= PlayerState.Disconnecting)
                                    map.LeaveRoom(user.Identity);
                            }
                        }
                    }

                    if (item.ToNextTime())
                        ServerKernel.UserManager.OnSecondaryTimer();

                    ServerKernel.QuizShow.OnTimer();

                    GameManagerLastUpdate = DateTime.Now;
                }
                catch (Exception ex)
                {
                    Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
                }
                finally
                {
                    perf.Stop();
                    GameManagerMs = perf.ElapsedMilliseconds;
                    perf = new Stopwatch();
                }

                if (StopThreading)
                    break;

                Thread.Sleep(500);
            }

            SetThreadEnded(ServerThreads.GameThread);
            Program.WriteLog("Game thread has exit...", LogType.WARNING);
        }

        private static void GeneratorManager()
        {
            RunningThreads |= ServerThreads.GeneratorThread;

            GeneratorLastUpdate = DateTime.Now;
            Stopwatch perf = new Stopwatch();
            while (true)
            {
                if (StopThreading)
                    break;

                try
                {
                    perf.Start();
                    foreach (var gen in ServerKernel.Generators.Values)
                    {
                        gen.OnTimer();
                    }

                    GeneratorLastUpdate = DateTime.Now;
                }
                catch (Exception ex)
                {
                    Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
                }
                finally
                {
                    perf.Stop();
                    GeneratorManagerMs = perf.ElapsedMilliseconds;
                    perf = new Stopwatch();
                    Thread.Sleep(2500);
                }
            }

            SetThreadEnded(ServerThreads.GeneratorThread);
            Program.WriteLog("Generator thread has exit...", LogType.WARNING);
        }

        private static void AiManager()
        {
            RunningThreads |= ServerThreads.AiThread;

            AiLastUpdate = DateTime.Now;
            Stopwatch perf = new Stopwatch();
            while (true)
            {
                try
                {
                    perf.Start();
                    ServerKernel.RoleManager.OnTimer();
                    AiLastUpdate = DateTime.Now;
                }
                catch (Exception ex)
                {
                    Program.WriteLog(ex.ToString(), LogType.EXCEPTION);
                }
                finally
                {
                    perf.Stop();
                    AiManagerMs = perf.ElapsedMilliseconds;
                    perf = new Stopwatch();
                }

                if (StopThreading)
                    break;

                Thread.Sleep(500);
            }

            SetThreadEnded(ServerThreads.AiThread);
            Program.WriteLog("AI thread has exit...", LogType.WARNING);
        }

        public static void SetThreadEnded(ServerThreads thread)
        {
            RunningThreads &= ~thread;
        }
    }

    [Flags]
    public enum ServerThreads : ushort
    {
        None = 0,
        UserThread = 0x1,
        GameThread = 0x2,
        UiThread = 0x4,
        GeneratorThread = 0x8,
        AiThread = 0x10,
        GameActionsThread = 0x20
    }
}