﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - GameServer - Analytics.cs
// Last Edit: 2020/01/06 19:07
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Threading;
using FtwCore.Common;
using GameServer.Structures;
using GameServer.Structures.Entities;

#endregion

namespace GameServer
{
    public class Analytics
    {
#if DEBUG
        public const int ANALYTIC_INTERVAL = 86400;
#else
        public const int ANALYTIC_INTERVAL = 300;
#endif

        public const string ANALYTIC_FILENAME = "MsgAnalytics";

        public static GameStatistic Statistic;

        private int m_lastTick;

        private readonly LogWriter m_pLog;

        public Analytics(string szFilename = null)
        {
            if (szFilename == null)
                szFilename = Environment.CurrentDirectory + @"\";

            m_pLog = new LogWriter(szFilename);
            StartTime = DateTime.Now;

            m_lastTick = UnixTimestamp.Now();
        }

        public DateTime StartTime { get; }

        public void IncrementSentBytes(long bytes)
        {
            Interlocked.Add(ref Statistic.SentBytes, bytes);
        }

        public void IncrementRecvBytes(long bytes)
        {
            Interlocked.Add(ref Statistic.RecvBytes, bytes);
        }

        public void IncrementSentPackets(long packets = 1)
        {
            Interlocked.Add(ref Statistic.SentPackets, packets);
        }

        public void IncrementRecvPackets(long packets = 1)
        {
            Interlocked.Add(ref Statistic.RecvPackets, packets);
        }

        public void SetMaxOnlinePlayers(int num)
        {
            Statistic.MaxOnlinePlayers = num;
        }

        public void SetLastLoginUser(Character user)
        {
            Statistic.LastLogin = user.Identity;
            Statistic.LastLoginName = user.Name;
        }

        private void SaveLog(string msg, LogType type = LogType.MESSAGE)
        {
            m_pLog.SaveLog(msg, ANALYTIC_FILENAME, type);
        }

        public void OnTimer(int nTick)
        {
            if (nTick < m_lastTick + ANALYTIC_INTERVAL)
                return; // invalid time

            SaveLog("");
            SaveLog("========================================================================");
            SaveLog($"[{ServerKernel.ServerName}] Conquer Online Server - Game Server");
            SaveLog($"Server Startup Time: {DateTime.Now.ToShortDateString()}");
            var interval = DateTime.Now - StartTime;
            SaveLog(
                $"Server takes: {interval.Days} days, {interval.Hours} hours, {interval.Minutes} minutes, {interval.Seconds} secs");
            SaveLog("========================================================================");
            SaveLog($"SentPackets:{Statistic.SentPackets:N0}\tSendBytes:{Statistic.SentBytes:N0}");
            SaveLog($"RecvPackets:{Statistic.RecvPackets:N0}\tRecvBytes:{Statistic.RecvBytes:N0}");
            SaveLog($"LastLoginUID:{Statistic.LastLogin}\tLastLoginUser:{Statistic.LastLoginName}");
            SaveLog("========================================================================");

            int max = Statistic.MaxOnlinePlayers;
            Statistic = new GameStatistic {MaxOnlinePlayers = max};

            if (nTick > m_lastTick + ANALYTIC_INTERVAL + 10)
                m_pLog.SaveLog("Analytics overtime", ANALYTIC_FILENAME, LogType.WARNING);

            m_lastTick = UnixTimestamp.Now();
        }
    }
}