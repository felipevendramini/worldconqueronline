﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5729 (auto hunt feature)
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - WebDbUpdate - Program.cs
// Last Edit: 2019/10/16 15:29
// Created: 2019/10/07 14:53
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using FtwCore.Common;
using FtwCore.Database;
using FtwCore.Database.Repositories;
using SevenZip;

#endregion

namespace WebDbUpdate
{
    class Program
    {
        private static readonly string[] TablesToUpdate =
        {
            TableName.AD_LOG,
            TableName.ARENA,
            TableName.DELUSER,
            TableName.DYNAMAP,
            TableName.DYNA_RANK_REC,
            TableName.DYNAMAP,
            TableName.DYNANPC,
            TableName.ENEMY,
            TableName.FATE_RANK,
            TableName.FAMILY,
            TableName.FAMILY_ATTR,
            TableName.FLOWER,
            TableName.FRIEND,
            TableName.GAME_LOGIN_RCD,
            TableName.ITEM,
            TableName.KILL_DEATH,
            TableName.MAGIC,
            TableName.PK_BONUS,
            TableName.PK_EXPLOIT,
            TableName.PK_ITEM,
            TableName.STATISTIC,
            TableName.STATUS,
            TableName.SUPERMAN,
            TableName.SUBCLASS,
            TableName.SYNDICATE_ATTR,
            TableName.SYNDICATE,
            TableName.SYNDICATE_WAR,
            TableName.TITLE,
            TableName.TRADE,
            TableName.TRADE_ITEM,
            TableName.TRAINING_VITALITY,
            TableName.TUTOR,
            TableName.USER,
            TableName.WEAPON_SKILL
            //TableName.EMONEY,
        };

        private static MySqlConfig Account;
        private static MySqlConfig Game;

        private static string _szMySqlPath = "C:\\Program Files\\MySQL\\MySQL Server 5.7\\bin";

        static void Main(string[] args)
        {
            if (args.Length > 0)
                _szMySqlPath = args[0];

            if (!Directory.Exists(_szMySqlPath))
            {
                Console.WriteLine(@"Invalid MySQL Directory.");
                Console.ReadKey();
                return;
            }

            Console.WriteLine(@"Database updater");

            if (!File.Exists(@"Config.xml"))
            {
                Console.WriteLine(@"No Config.xml file found.");
                Console.ReadKey();
                return;
            }

            MyXml xml = new MyXml("Config.xml");
            Account = new MySqlConfig(xml.GetStr("Config", "AccountServer", "MySQL", "Hostname"),
                xml.GetStr("Config", "AccountServer", "MySQL", "Username"),
                xml.GetStr("Config", "AccountServer", "MySQL", "Password"),
                xml.GetStr("Config", "AccountServer", "MySQL", "Database"),
                xml.GetInt("Config", "AccountServer", "MySQL", "Port"));

            Game = new MySqlConfig(xml.GetStr("Config", "GameServer", "MySQL", "Hostname"),
                xml.GetStr("Config", "GameServer", "MySQL", "Username"),
                xml.GetStr("Config", "GameServer", "MySQL", "Password"),
                xml.GetStr("Config", "GameServer", "MySQL", "Database"),
                xml.GetInt("Config", "GameServer", "MySQL", "Port"));

            Console.WriteLine(@"Connecting to account database");
            if (!SessionFactory.StartAccountConnection(Account.Host, Account.User, Account.Pass, Account.Database, Account.Port))
            {
                Console.WriteLine(@"Could not connect to account server...");
                Console.WriteLine("Exception: {0}", SessionFactory.LastException);
                Console.WriteLine("\tHost: {0}", Account.Host);
                Console.WriteLine("\tUser: {0}", Account.User);
                Console.WriteLine("\tPass: {0}", Account.Pass);
                Console.WriteLine("\tDatabase: {0}", Account.Database);
                Console.WriteLine("\tPort: {0}", Account.Port);
                Console.ReadLine();
                return;
            }
            Console.WriteLine(@"Connecting to game database");
            
            if (!SessionFactory.StartGameConnection(Game.Host, Game.User, Game.Pass, Game.Database, Game.Port))
            {
                Console.WriteLine(@"Could not connect to game server...");
                Console.WriteLine("Exception: {0}", SessionFactory.LastException);
                Console.WriteLine("\tHost: {0}", Game.Host);
                Console.WriteLine("\tUser: {0}", Game.User);
                Console.WriteLine("\tPass: {0}", Game.Pass);
                Console.WriteLine("\tDatabase: {0}", Game.Database);
                Console.WriteLine("\tPort: {0}", Game.Port);
                Console.ReadLine();
                return;
            }

            ActionRepository account = new ActionRepository(MysqlTargetConnection.Account);
            ActionRepository game = new ActionRepository(MysqlTargetConnection.Game);

            Stopwatch sw = new Stopwatch();
            sw.Start();
            Console.WriteLine(@"Doing dirty job");
            string outputDate = DateTime.Now.ToString("yyyyMMddHHmmss");
            foreach (var table in TablesToUpdate)
            {
                BackupTable(table, outputDate);
            }

            AccountRestore(outputDate);
            sw.Stop();
            account.ExecuteQuery($"INSERT INTO ftw_site_update_time (`last_update`,`elapsed_ms`) VALUES ('{DateTime.Now:yyyy/MM/dd HH:mm:ss}', '{sw.ElapsedMilliseconds}')");
            Console.WriteLine(@"Exiting...");
            for (int i = 0; i < 5; i++)
            {
                Console.Write(@".");
                Thread.Sleep(1000);
            }

            File.Delete($"{Environment.CurrentDirectory}\\backups\\{outputDate}.sql");
        }

        static void BackupTable(string table, string date)
        {
            string mysqldmpPath = $"{_szMySqlPath}\\mysqldump.exe";
            string commandLine =
                $"-h {Game.Host} -u {Game.User} -p{Game.Pass} --default-character-set=utf8mb4 {Game.Database} {table}";

            Process dump = new Process
            {
                StartInfo =
                {
                    FileName = mysqldmpPath,
                    Arguments = commandLine,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true,
                    StandardOutputEncoding = Encoding.UTF8
                }
            };
            dump.Start();

            if (!Directory.Exists($"{Environment.CurrentDirectory}\\backups"))
                Directory.CreateDirectory($"{Environment.CurrentDirectory}\\backups");

            StreamWriter writer = new StreamWriter($"{Environment.CurrentDirectory}\\backups\\{date}.sql", true, Encoding.UTF8);
            writer.Write(dump.StandardOutput.ReadToEnd());
            writer.Close();
            writer.Dispose();

            Console.WriteLine("Table {0} has been back up as \"{1}.sql\"", table, date);
        }

        static void AccountRestore(string date)
        {
            string mysqldmpPath = $"{_szMySqlPath}\\mysql.exe";
            string _COMMAND_LINE =
                $"-h {Account.Host} -u {Account.User} -p{Account.Pass} {Account.Database} < \"{{0}}\"";

            StreamWriter writer = new StreamWriter("temp.cmd");
            writer.WriteLine("\"{0}\" {1}", mysqldmpPath, string.Format(_COMMAND_LINE, $"{Environment.CurrentDirectory}\\backups\\{date}.sql"));
            writer.Close();
            writer.Dispose();

            Process dump = new Process
            {
                StartInfo =
                {
                    FileName = "temp.cmd",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    CreateNoWindow = true,
                    Verb = "runas"
                }
            };
            dump.Start();

            dump.WaitForExit();

            Console.WriteLine($@"'{date}.sql' has been restored in Account Server");

            Console.WriteLine($@"Starting to zip {date}.sql file...");

            var path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), Environment.Is64BitProcess ? "x64" : "x86", "7z.dll");
            SevenZipBase.SetLibraryPath(path);

            SevenZipCompressor compress = new SevenZipCompressor();
            compress.CompressFiles($"{Environment.CurrentDirectory}\\backups\\bak{date}.7z", $"{Environment.CurrentDirectory}\\backups\\{date}.sql");
            Console.WriteLine(@"Done!");
        }
    }
}