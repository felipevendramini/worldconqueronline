﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - IAsynchronousSocket.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;

#endregion

namespace FtwCore.Interfaces
{
    /// <summary>
    /// An asynchronous socket interface encapsulates an asynchronous socket type, either client or server. This
    /// interface allows the passport to be used for server and client socket systems.
    /// </summary>
    public interface IAsynchronousSocket
    {
        string Name { get; set; } // The name of the server.
        int FooterLength { get; set; } // The length of the footer for each packet.
        string Footer { get; set; } // The text for the footer at the end of each packet.
        void Disconnect(IAsyncResult result); // Disconnection method for the client / server.
    }
}