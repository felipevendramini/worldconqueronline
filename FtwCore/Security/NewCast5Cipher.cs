﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - NewCast5Cipher.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using CO2_CORE_DLL.Security.Cryptography;
using FtwCore.Interfaces;

#endregion

namespace FtwCore.Security
{
    public sealed unsafe class NewCast5Cipher : ICipher
    {
        CAST5 _cast5;

        public NewCast5Cipher()
        {
            _cast5 = new CAST5();
            _cast5.GenerateKey(Cast5Cipher.InitialKey);
        }

        public void Decrypt(byte[] packet, byte[] buffer, int length, int position)
        {
            fixed (byte* buff = buffer)
            {
                _cast5.Decrypt(buff, length);
            }
        }

        public byte[] Encrypt(byte[] packet, int length)
        {
            fixed (byte* buff = packet)
                _cast5.Encrypt(buff, length);
            return packet;
        }

        public void GenerateKeys(int account, int authentication)
        {
        }

        public void KeySchedule(byte[] key)
        {
            _cast5.GenerateKey(key);
        }

        public byte[] Decrypt(byte[] buffer, int length)
        {
            fixed (byte* buff = buffer)
                _cast5.Decrypt(buff, length);
            return buffer;
        }

        public void SetIvs(byte[] i1, byte[] i2)
        {
            _cast5.SetIVs(i1, i2);
        }
    }
}