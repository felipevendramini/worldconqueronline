﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - MyStackTrace.cs
// Last Edit: 2019/11/26 19:23
// Created: 2019/10/15 06:22
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using FtwCore.Common;
using Microsoft.Diagnostics.Runtime;

#endregion

namespace FtwCore.Win32Events
{
    public class MyStackTrace
    {
        private readonly LogWriter m_myWriter;
        private readonly string m_szHandler;

        public MyStackTrace(string handlerName)
        {
            m_szHandler = handlerName;
            m_myWriter = new LogWriter($"{Environment.CurrentDirectory}\\thread_log\\");
        }

        public void DoLog()
        {
            var result = new Dictionary<int, string[]>();

            var pid = Process.GetCurrentProcess().Id;

            using (var dataTarget = DataTarget.AttachToProcess(pid, 5000, AttachFlag.Passive))
            {
                ClrInfo runtimeInfo = dataTarget.ClrVersions[0];
                var runtime = runtimeInfo.CreateRuntime();

                foreach (var t in runtime.Threads)
                {
                    try
                    {
                        Console.WriteLine($@"Thread {t.OSThreadId} has {t.LockCount} unfinished locks.");
                        foreach (var locked in t.BlockingObjects)
                            Console.WriteLine(
                                $@"LockingObj(Reason: {locked.Reason}, OwnerID: {locked.Owner.OSThreadId})");
                    }
                    catch
                    {
                        Console.WriteLine(@"Could not get all lock info...");
                    }

                    result.Add(
                        t.ManagedThreadId,
                        t.StackTrace.Select(f =>
                        {
                            if (f.Method != null)
                            {
                                return f.Thread.OSThreadId + ">>>" + f.Method.Type.Name + "." + f.Method.Name;
                            }

                            return null;
                        }).ToArray()
                    );
                }
            }

            foreach (var res in result)
            {
                m_myWriter.SaveLog($"Stack trace about locked thread: {res.Key}",
                    $"proc_stacktrace-{m_szHandler}-{DateTime.Now:yyyyMMddHHmmss}.log");
                foreach (var str in res.Value)
                {
                    m_myWriter.SaveLog(str, $"proc_stacktrace-{m_szHandler}-{DateTime.Now:yyyyMMddHHmmss}.log");
                }
            }
        }
    }
}