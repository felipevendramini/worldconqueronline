﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Mail Attachment.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;

#endregion

namespace FtwCore.Database.Entities
{
    public class MailAttachmentEntity
    {
        public virtual uint Identity { get; set; }
        public virtual uint MailIdentity { get; set; }

        /// <summary>
        ///     If set, itemtype must be zero. Cannot set item_id and itemtype at once.
        /// </summary>
        public virtual uint ItemIdentity { get; set; }

        /// <summary>
        ///     If set, item_id must be zero. Cannot set itemtype and item_id at once.
        /// </summary>
        public virtual uint Itemtype { get; set; }

        public virtual DateTime? ClaimTime { get; set; }
        public virtual int Flag { get; set; }
    }
}