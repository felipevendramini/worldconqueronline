﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Trap.cs
// Last Edit: 2019/12/07 22:23
// Created: 2019/12/07 22:23
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Database.Entities
{
    public class TrapEntity
    {
        public virtual uint Id { get; set; }
        public virtual uint Type { get; set; }
        public virtual uint Look { get; set; }
        public virtual uint OwnerId { get; set; }
        public virtual uint MapId { get; set; }
        public virtual ushort PosX { get; set; }
        public virtual ushort PosY { get; set; }
        public virtual uint Data { get; set; }
    }
}