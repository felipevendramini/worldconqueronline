﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Arena.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/14 19:21
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Database.Entities
{
    public class ArenaEntity
    {
        public virtual uint Identity { get; set; }
        public virtual uint PlayerIdentity { get; set; }
        public virtual string Name { get; set; }
        public virtual uint Lookface { get; set; }
        public virtual byte Level { get; set; }
        public virtual ushort Profession { get; set; }
        public virtual uint WinsToday { get; set; }
        public virtual uint WinsTotal { get; set; }
        public virtual uint LossToday { get; set; }
        public virtual uint LossTotal { get; set; }
        public virtual uint Points { get; set; }

        /// <summary>
        ///     Arena Qualifier = 0
        ///     Team Qualifier = 1
        /// </summary>
        public virtual byte Mode { get; set; }

        public virtual uint TotalHonorPoints { get; set; }
        public virtual uint LastRank { get; set; }
        public virtual uint LastWin { get; set; }
        public virtual uint LastLoss { get; set; }
        public virtual uint LastPoints { get; set; }
        public virtual uint LockReleaseTime { get; set; }
        public virtual byte RankType { get; set; }
    }
}