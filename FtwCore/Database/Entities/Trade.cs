﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Trade.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;

#endregion

namespace FtwCore.Database.Entities
{
    public class TradeEntity
    {
        public virtual uint Identity { get; set; }
        public virtual uint UserIdentity { get; set; }
        public virtual uint TargetIdentity { get; set; }
        public virtual uint UserMoney { get; set; }
        public virtual uint TargetMoney { get; set; }
        public virtual uint UserEmoney { get; set; }
        public virtual uint TargetEmoney { get; set; }
        public virtual uint MapIdentity { get; set; }
        public virtual ushort UserX { get; set; }
        public virtual ushort UserY { get; set; }
        public virtual ushort TargetX { get; set; }
        public virtual ushort TargetY { get; set; }
        public virtual DateTime Timestamp { get; set; }
        public virtual string UserIpAddress { get; set; }
        public virtual string UserMacAddress { get; set; }

        public virtual string TargetIpAddress { get; set; }
        public virtual string TargetMacAddress { get; set; }
    }
}