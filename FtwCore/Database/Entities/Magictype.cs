﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Magictype.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Database.Entities
{
    public class MagictypeEntity
    {
        public virtual uint Id { get; set; }
        public virtual uint Type { get; set; }
        public virtual uint Sort { get; set; }
        public virtual string Name { get; set; }
        public virtual byte Crime { get; set; }
        public virtual byte Ground { get; set; }
        public virtual byte Multi { get; set; }
        public virtual uint Target { get; set; }
        public virtual uint Level { get; set; }
        public virtual uint UseMp { get; set; }
        public virtual int Power { get; set; }
        public virtual uint IntoneSpeed { get; set; }
        public virtual uint Percent { get; set; }
        public virtual uint StepSecs { get; set; }
        public virtual uint Range { get; set; }
        public virtual uint Distance { get; set; }
        public virtual long Status { get; set; }
        public virtual uint NeedProf { get; set; }
        public virtual int NeedExp { get; set; }
        public virtual uint NeedLevel { get; set; }
        public virtual byte UseXp { get; set; }
        public virtual uint WeaponSubtype { get; set; }
        public virtual uint ActiveTimes { get; set; }
        public virtual byte AutoActive { get; set; }
        public virtual uint FloorAttr { get; set; }
        public virtual byte AutoLearn { get; set; }
        public virtual uint LearnLevel { get; set; }
        public virtual byte DropWeapon { get; set; }
        public virtual uint UseEp { get; set; }
        public virtual byte WeaponHit { get; set; }
        public virtual uint UseItem { get; set; }
        public virtual uint NextMagic { get; set; }
        public virtual uint DelayMs { get; set; }
        public virtual uint UseItemNum { get; set; }
        public virtual byte WeaponSubtypeNum { get; set; } // 2015-02-12
        public virtual byte ElementType { get; set; } // 2016-12-11
        public virtual uint ElementPower { get; set; } // 2016-12-12
        public virtual uint MaximumDashRange { get; set; } // 2017-03-13
        public virtual uint Timeout { get; set; } // 2017-03-13
        public virtual uint EmoneyPrice { get; set; } // 2019-10-17
    }
}