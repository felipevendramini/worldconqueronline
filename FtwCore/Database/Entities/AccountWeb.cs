﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - AccountWeb.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using FluentNHibernate.Mapping;
using Microsoft.AspNet.Identity;

#endregion

namespace FtwCore.Database.Entities
{
    public class AccountWebEntity : IUser<int>
    {
        //public virtual string Password { get; set; }
        public virtual string PasswordHash { get; set; }
        public virtual byte Type { get; set; }
        public virtual byte Flag { get; set; }
        public virtual string Hash { get; set; }
        public virtual byte VipLevel { get; set; }
        public virtual ulong VipCoins { get; set; }
        public virtual uint VipPoints { get; set; }
        public virtual ulong SecurityCode { get; set; }
        public virtual string SecurityQuestion { get; set; }
        public virtual string SecurityAnswer { get; set; }
        public virtual string Country { get; set; }
        public virtual string Language { get; set; }
        public virtual string RealName { get; set; }
        public virtual byte Gender { get; set; }
        public virtual DateTime? Age { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Address { get; set; }
        public virtual uint AddressNum { get; set; }
        public virtual string NetBarIp { get; set; }
        public virtual DateTime? CreationDate { get; set; }
        public virtual string Locale { get; set; } // redundant??
        public virtual DateTime? FirstLogin { get; set; }
        public virtual DateTime? LastLogin { get; set; }
        public virtual int Id { get; protected set; }
        public virtual string UserName { get; set; }


        public class AccountWebMap : ClassMap<AccountWebEntity>
        {
            public AccountWebMap()
            {
                Table(TableName.FTW_ACCOUNT_TABLE);
                LazyLoad();
                Id(x => x.Id).Column("id").GeneratedBy.Identity().Not.Nullable();
                Map(x => x.UserName).Column("email").Not.Nullable();
                //Map(x => x.Password).Column("password").Not.Nullable();
                Map(x => x.PasswordHash).Column("password").Not.Nullable();
                Map(x => x.Type).Column("type").Not.Nullable().Default("0");
                Map(x => x.Flag).Column("flag").Not.Nullable().Default("0");
                Map(x => x.Hash).Column("hash").Not.Nullable();
                Map(x => x.VipLevel).Column("vip").Not.Nullable().Default("0");
                Map(x => x.VipCoins).Column("vip_coins").Not.Nullable().Default("0");
                Map(x => x.VipPoints).Column("vip_points").Not.Nullable().Default("0");
                Map(x => x.SecurityCode).Column("security_code").Not.Nullable();
                Map(x => x.SecurityQuestion).Column("security_question").Not.Nullable();
                Map(x => x.SecurityAnswer).Column("security_answer").Not.Nullable();
                Map(x => x.Country).Column("country").Not.Nullable().Default("BRA");
                Map(x => x.Language).Column("language").Not.Nullable().Default("pt-br");
                Map(x => x.RealName).Column("real_name").Not.Nullable();
                Map(x => x.Gender).Column("sex").Not.Nullable().Default("255");
                Map(x => x.Age).Column("age").Not.Nullable().Default("0");
                Map(x => x.Phone).Column("phone").Not.Nullable();
                Map(x => x.Address).Column("address").Not.Nullable();
                Map(x => x.AddressNum).Column("addr_num").Not.Nullable().Default("0");
                Map(x => x.NetBarIp).Column("netbar_ip").Not.Nullable().Default("127.0.0.1");
                Map(x => x.CreationDate).Column("creation_date");
                Map(x => x.Locale).Column("locale").Not.Nullable().Default("pt-br");
                Map(x => x.FirstLogin).Column("first_login");
                Map(x => x.LastLogin).Column("last_login");
            }
        }
    }
}