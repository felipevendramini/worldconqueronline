﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Syndicate Totem Enhance.cs
// Last Edit: 2020/01/17 00:14
// Created: 2020/01/17 00:11
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;

#endregion

namespace FtwCore.Database.Entities
{
    public class SyndicateTotemEnhanceEntity
    {
        public virtual uint Identity { get; protected set; }

        /// <summary>
        ///     The syndicate who owns the enhancement.
        /// </summary>
        public virtual uint SyndicateIdentity { get; set; }

        /// <summary>
        /// The arsenal type.
        /// </summary>
        public virtual byte Totem { get; set; }

        /// <summary>
        ///     The amount of extra battle power.
        /// </summary>
        public virtual byte Enhancement { get; set; }

        /// <summary>
        ///     When the enhancement started.
        /// </summary>
        public virtual DateTime StartTime { get; set; }

        /// <summary>
        ///     When the enhancement will end.
        /// </summary>
        public virtual DateTime EndTime { get; set; }

        /// <summary>
        ///     The user who started the enhancement.
        /// </summary>
        public virtual uint UserIdentity { get; set; }
    }
}