﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Generator.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Database.Entities
{
    public class GeneratorEntity
    {
        public virtual uint Id { get; set; }
        public virtual uint Mapid { get; set; }
        public virtual ushort BoundX { get; set; }
        public virtual ushort BoundY { get; set; }
        public virtual ushort BoundCx { get; set; }
        public virtual ushort BoundCy { get; set; }
        public virtual int MaxNpc { get; set; }
        public virtual int RestSecs { get; set; }
        public virtual int MaxPerGen { get; set; }
        public virtual uint Npctype { get; set; }
        public virtual int TimerBegin { get; set; }
        public virtual int TimerEnd { get; set; }
        public virtual int BornX { get; set; }
        public virtual int BornY { get; set; }
    }
}