﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Monarchy.cs
// Last Edit: 2019/11/28 12:08
// Created: 2019/11/28 12:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;

#endregion

namespace FtwCore.Database.Entities
{
    public class MonarchyEntity
    {
        public virtual uint Identity { get; set; }
        public virtual uint EmperorIdentity { get; set; }
        public virtual string EmperorName { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime? Abdication { get; set; }
        public virtual long CurrentMoney { get; set; }
        public virtual long StartMoney { get; set; }
        public virtual long AbdicateMoney { get; set; }
        public virtual long CurrentEMoney { get; set; }
        public virtual long StartEMoney { get; set; }
        public virtual long AbdicateEMoney { get; set; }
    }
}