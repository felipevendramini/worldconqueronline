﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - FateRank.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/11/08 19:05
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Database.Entities
{
    public class FateRankEntity
    {
        public virtual uint Identity { get; set; }
        public virtual byte Fate { get; set; }
        public virtual byte Ranking { get; set; }
        public virtual uint Power0 { get; set; }
        public virtual uint Power1 { get; set; }
        public virtual uint Power2 { get; set; }
        public virtual uint Power3 { get; set; }
    }
}