﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Magictype Operation.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Database.Entities
{
    public class MagictypeOperationEntity
    {
        public virtual uint Id { get; set; }
        public virtual byte RebirthTime { get; set; }
        public virtual ushort ProfessionAgo { get; set; }
        public virtual ushort ProfessionNow { get; set; }
        public virtual byte MagictypeOp { get; set; }
        public virtual ushort Skill1 { get; set; }
        public virtual ushort Skill2 { get; set; }
        public virtual ushort Skill3 { get; set; }
        public virtual ushort Skill4 { get; set; }
        public virtual ushort Skill5 { get; set; }
        public virtual ushort Skill6 { get; set; }
        public virtual ushort Skill7 { get; set; }
        public virtual ushort Skill8 { get; set; }
        public virtual ushort Skill9 { get; set; }
        public virtual ushort Skill10 { get; set; }
        public virtual ushort Skill11 { get; set; }
        public virtual ushort Skill12 { get; set; }
        public virtual ushort Skill13 { get; set; }
        public virtual ushort Skill14 { get; set; }
        public virtual ushort Skill15 { get; set; }
        public virtual ushort Skill16 { get; set; }
        public virtual ushort Skill17 { get; set; }
        public virtual ushort Skill18 { get; set; }
        public virtual ushort Skill19 { get; set; }
        public virtual ushort Skill20 { get; set; }
        public virtual ushort Skill21 { get; set; }
        public virtual ushort Skill22 { get; set; }
        public virtual ushort Skill23 { get; set; }
        public virtual ushort Skill24 { get; set; }
        public virtual ushort Skill25 { get; set; }
        public virtual ushort Skill26 { get; set; }
        public virtual ushort Skill27 { get; set; }
        public virtual ushort Skill28 { get; set; }
        public virtual ushort Skill29 { get; set; }
        public virtual ushort Skill30 { get; set; }
        public virtual ushort Skill31 { get; set; }
        public virtual ushort Skill32 { get; set; }
        public virtual ushort Skill33 { get; set; }
        public virtual ushort Skill34 { get; set; }
        public virtual ushort Skill35 { get; set; }
        public virtual ushort Skill36 { get; set; }
        public virtual ushort Skill37 { get; set; }
        public virtual ushort Skill38 { get; set; }
        public virtual ushort Skill39 { get; set; }
        public virtual ushort Skill40 { get; set; }
        public virtual ushort Skill41 { get; set; }
        public virtual ushort Skill42 { get; set; }
        public virtual ushort Skill43 { get; set; }
        public virtual ushort Skill44 { get; set; }
        public virtual ushort Skill45 { get; set; }
        public virtual ushort Skill46 { get; set; }
        public virtual ushort Skill47 { get; set; }
        public virtual ushort Skill48 { get; set; }
        public virtual ushort Skill49 { get; set; }
        public virtual ushort Skill50 { get; set; }
        public virtual ushort Skill51 { get; set; }
        public virtual ushort Skill52 { get; set; }
        public virtual ushort Skill53 { get; set; }
        public virtual ushort Skill54 { get; set; }
        public virtual ushort Skill55 { get; set; }
        public virtual ushort Skill56 { get; set; }
        public virtual ushort Skill57 { get; set; }
        public virtual ushort Skill58 { get; set; }
        public virtual ushort Skill59 { get; set; }
        public virtual ushort Skill60 { get; set; }
    }
}