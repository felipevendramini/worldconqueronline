﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Family.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/11/24 18:55
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Database.Entities
{
    public class FamilyEntity
    {
        public virtual uint Identity { get; set; }
        public virtual uint CreationDate { get; set; }
        public virtual string Name { get; set; }
        public virtual string LeaderName { get; set; }
        public virtual uint LeaderIdentity { get; set; }
        public virtual ushort Amount { get; set; }
        public virtual string Announce { get; set; }
        public virtual uint Money { get; set; }
        public virtual byte DelFlag { get; set; }
        public virtual uint Ally0 { get; set; }
        public virtual uint Ally1 { get; set; }
        public virtual uint Ally2 { get; set; }
        public virtual uint Ally3 { get; set; }
        public virtual uint Ally4 { get; set; }
        public virtual uint Enemy0 { get; set; }
        public virtual uint Enemy1 { get; set; }
        public virtual uint Enemy2 { get; set; }
        public virtual uint Enemy3 { get; set; }
        public virtual uint Enemy4 { get; set; }
        public virtual uint OccupyDays { get; set; }
        public virtual uint OccupyMap { get; set; }
        public virtual byte Level { get; set; }
        public virtual byte BattlePowerTower { get; set; }
    }
}