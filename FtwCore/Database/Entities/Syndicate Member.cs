﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Syndicate Member.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;

#endregion

namespace FtwCore.Database.Entities
{
    public class SyndicateMemberEntity
    {
        public virtual uint Id { get; set; }
        public virtual uint SynId { get; set; }
        public virtual ushort Rank { get; set; }
        public virtual long Proffer { get; set; }
        public virtual ulong ProfferTotal { get; set; }
        public virtual uint Emoney { get; set; }
        public virtual uint EmoneyTotal { get; set; }
        public virtual int Pk { get; set; }
        public virtual uint PkTotal { get; set; }
        public virtual uint Guide { get; set; }
        public virtual uint GuideTotal { get; set; }
        public virtual uint Exploit { get; set; }
        public virtual uint Arsenal { get; set; }
        public virtual uint Expiration { get; set; }
        public virtual DateTime JoinDate { get; set; }
    }
}