﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Lottery.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Database.Entities
{
    public class LotteryEntity
    {
        public virtual uint Identity { get; set; }
        public virtual byte Type { get; set; }
        public virtual byte Rank { get; set; }
        public virtual byte Chance { get; set; }
        public virtual string Itemname { get; set; }
        public virtual uint ItemIdentity { get; set; }
        public virtual byte Color { get; set; }
        public virtual byte SocketNum { get; set; }
        public virtual byte Plus { get; set; }
    }
}