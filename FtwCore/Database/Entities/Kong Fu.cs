﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Kong Fu.cs
// Last Edit: 2019/12/15 17:55
// Created: 2019/12/12 23:38
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;

#endregion

namespace FtwCore.Database.Entities
{
    public class KongFuEntity
    {
        public virtual uint Identity { get; set; }
        public virtual uint UserIdentity { get; set; }
        public virtual string Name { get; set; }
        public virtual uint Score { get; set; }
        public virtual byte CurrentTalent { get; set; }
        public virtual uint FreeCourses { get; set; }
        public virtual ushort CoursesAvailable { get; set; }
        public virtual uint TalentPoints { get; set; }
        public virtual DateTime UnlockDate { get; set; }
        public virtual uint TrainingCount { get; set; }
        public virtual uint TrainingCountToday { get; set; }
        public virtual DateTime? LastTraining { get; set; }
        public virtual uint RemainingToExit { get; set; }
    }
}