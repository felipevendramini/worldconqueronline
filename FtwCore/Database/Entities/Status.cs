﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Status.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Database.Entities
{
    public class StatusEntity
    {
        public virtual uint Id { get; set; }
        public virtual uint OwnerId { get; set; }
        public virtual uint Status { get; set; }
        public virtual int Power { get; set; }
        public virtual uint Sort { get; set; }
        public virtual uint LeaveTimes { get; set; }
        public virtual uint RemainTime { get; set; }
        public virtual uint EndTime { get; set; }
        public virtual uint IntervalTime { get; set; }
    }
}