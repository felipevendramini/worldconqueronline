﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - KillDeath.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;

#endregion

namespace FtwCore.Database.Entities
{
    public class KillDeathEntity
    {
        public virtual uint Identity { get; protected set; }
        public virtual uint AttackerIdentity { get; set; }
        public virtual string AttackerName { get; set; }
        public virtual uint AttackerMap { get; set; }
        public virtual ushort AttackerMapX { get; set; }
        public virtual ushort AttackerMapY { get; set; }
        public virtual ushort AttackerBattlePower { get; set; }
        public virtual byte AttackerLevel { get; set; }
        public virtual byte AttackerProfession { get; set; }
        public virtual uint TargetIdentity { get; set; }
        public virtual string TargetName { get; set; }
        public virtual uint TargetMap { get; set; }
        public virtual ushort TargetMapX { get; set; }
        public virtual ushort TargetMapY { get; set; }
        public virtual ushort TargetBattlePower { get; set; }
        public virtual byte TargetLevel { get; set; }
        public virtual byte TargetProfession { get; set; }
        public virtual DateTime Timestamp { get; set; }
        public virtual byte BattleMode { get; set; }
    }
}