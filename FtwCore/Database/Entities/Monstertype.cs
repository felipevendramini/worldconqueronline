﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Monstertype.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Database.Entities
{
    public class MonstertypeEntity
    {
        public virtual uint Id { get; set; }
        public virtual string Name { get; set; }
        public virtual uint Type { get; set; }
        public virtual ushort Lookface { get; set; }
        public virtual int Life { get; set; }
        public virtual uint Mana { get; set; }
        public virtual int AttackMax { get; set; }
        public virtual int AttackMin { get; set; }
        public virtual int Defence { get; set; }
        public virtual uint Dexterity { get; set; }
        public virtual uint Dodge { get; set; }
        public virtual uint HelmetType { get; set; }
        public virtual uint ArmorType { get; set; }
        public virtual uint WeaponrType { get; set; }
        public virtual uint WeaponlType { get; set; }
        public virtual int AttackRange { get; set; }
        public virtual int ViewRange { get; set; }
        public virtual int EscapeLife { get; set; }
        public virtual int AttackSpeed { get; set; }
        public virtual int MoveSpeed { get; set; }
        public virtual ushort Level { get; set; }
        public virtual uint AttackUser { get; set; }
        public virtual uint DropMoney { get; set; }
        public virtual uint DropItemtype { get; set; }
        public virtual uint SizeAdd { get; set; }
        public virtual uint Action { get; set; }
        public virtual uint RunSpeed { get; set; }
        public virtual byte DropArmet { get; set; }
        public virtual byte DropNecklace { get; set; }
        public virtual byte DropArmor { get; set; }
        public virtual byte DropRing { get; set; }
        public virtual byte DropWeapon { get; set; }
        public virtual byte DropShield { get; set; }
        public virtual byte DropShoes { get; set; }
        public virtual uint DropHp { get; set; }
        public virtual uint DropMp { get; set; }
        public virtual uint MagicType { get; set; }
        public virtual int MagicDef { get; set; }
        public virtual uint MagicHitrate { get; set; }
        public virtual uint AiType { get; set; }
        public virtual uint Defence2 { get; set; }
        public virtual ushort StcType { get; set; }
        public virtual byte AntiMonster { get; set; }
        public virtual ushort ExtraBattlelev { get; set; }
        public virtual short ExtraExp { get; set; }
        public virtual short ExtraDamage { get; set; }
        public virtual int WaterAtk { get; set; }
        public virtual int FireAtk { get; set; }
        public virtual int EarthAtk { get; set; }
        public virtual int WoodAtk { get; set; }
        public virtual int MetalAtk { get; set; }
        public virtual byte WaterDef { get; set; }
        public virtual byte FireDef { get; set; }
        public virtual byte EarthDef { get; set; }
        public virtual byte WoodDef { get; set; }
        public virtual byte MetalDef { get; set; }
        public virtual byte Boss { get; set; }
    }
}