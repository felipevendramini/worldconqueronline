﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - EmoneyLog.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;

#endregion

namespace FtwCore.Database.Entities
{
    public class EmoneyLogEntity
    {
        public virtual ulong Identity { get; set; }
        public virtual uint TargetIdentity { get; set; }
        public virtual uint SourceIdentity { get; set; }
        public virtual byte SourceType { get; set; }
        public virtual ushort OperationType { get; set; }
        public virtual int Amount { get; set; }
        public virtual uint MapIdentity { get; set; }
        public virtual ushort MapX { get; set; }
        public virtual ushort MapY { get; set; }
        public virtual DateTime Timestamp { get; set; }
        public virtual uint SourceBalance { get; set; }
        public virtual uint TargetBalance { get; set; }
    }
}