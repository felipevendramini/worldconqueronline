﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Weapon Skill.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Database.Entities
{
    public class WeaponSkillEntity
    {
        /// <summary>
        ///     The unique identification of the weapon skill.
        /// </summary>
        public virtual uint Identity { get; set; }

        /// <summary>
        ///     The actual level of the weapon skill.
        /// </summary>
        public virtual uint Level { get; set; }

        /// <summary>
        ///     The amount of experience of the actual level.
        /// </summary>
        public virtual uint Experience { get; set; }

        /// <summary>
        ///     The owner unique identity (character identity).
        /// </summary>
        public virtual uint OwnerIdentity { get; set; }

        /// <summary>
        ///     The old level of the weapon skill before reborn (if higher)
        /// </summary>
        public virtual uint OldLevel { get; set; }

        /// <summary>
        ///     If the weapon skill is active. 1 Means that it is waiting the level hit the
        ///     old level to restore the old status.
        /// </summary>
        public virtual uint Unlearn { get; set; }

        /// <summary>
        ///     The 3 digit type of weapon. (410 - Blade)
        /// </summary>
        public virtual uint Type { get; set; }
    }
}