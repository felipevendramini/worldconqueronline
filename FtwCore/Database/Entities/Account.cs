﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Account.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Database.Entities
{
    public class AccountEntity
    {
        /// <summary>
        ///     The user account unique identification.
        /// </summary>
        public virtual uint Identity { get; set; }

        /// <summary>
        ///     The name the player uses to log in.
        /// </summary>
        public virtual string Username { get; set; }

        /// <summary>
        ///     The password set by the player on registration.
        /// </summary>
        public virtual string Password { get; set; }

        public virtual uint CreationDate { get; set; }

        /// <summary>
        ///     The user VIP level.
        /// </summary>
        public virtual byte Vip { get; set; }

        /// <summary>
        ///     The user authority level. 0 = None 8 = Administrator
        /// </summary>
        public virtual byte Type { get; set; }

        /// <summary>
        ///     The user account status or the code ID of the error. 0 means normal.
        /// </summary>
        public virtual byte Lock { get; set; }

        /// <summary>
        ///     The date when the user logged in for the last time.
        /// </summary>
        public virtual int LastLogin { get; set; }

        public virtual string MacAddress { get; set; } // 2017-03-01 felipe
        public virtual uint LockExpire { get; set; } // 2017-03-01 felipe
        public virtual string IpAddress { get; set; } // 2017-03-12 felipe
        public virtual uint FirstLogin { get; set; } // 2019-04-24 felipe
        public virtual uint AccountIdentity { get; set; } // 2019-09-12 felipe
    }
}