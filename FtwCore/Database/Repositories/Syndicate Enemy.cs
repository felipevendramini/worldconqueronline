﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Syndicate Enemy.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using FtwCore.Database.Entities;
using NHibernate.Criterion;

#endregion

namespace FtwCore.Database.Repositories
{
    public sealed class SyndicateEnemyRepository : HibernateDataRow<SyndicateEnemyEntity>
    {
        public SyndicateEnemyRepository()
            : base(SessionFactory.GameConnection)
        {
        }

        public IList<SyndicateEnemyEntity> FetchBySyndicate(uint dwId)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<SyndicateEnemyEntity>()
                    .Add(Restrictions.Eq("Synid", dwId))
                    .List<SyndicateEnemyEntity>();
        }

        public void ClearAlliesAndEnemies(ushort syndicate)
        {
            using (var sessionFactory = GetSession())
            {
                sessionFactory.CreateSQLQuery("CALL SynClearAllyAndEnemy (?);")
                    .AddEntity(typeof(SyndicateAllyEntity))
                    .SetParameter(0, syndicate)
                    .ExecuteUpdate();
            }
        }

        public void DeleteAntagonize(ushort syndicate0, ushort syndicate1)
        {
            using (var sessionFactory = GetSession())
            {
                sessionFactory.CreateSQLQuery("CALL SynDeleteAntagonize (?,?);")
                    .AddEntity(typeof(SyndicateEnemyEntity))
                    .SetParameter(0, syndicate0)
                    .SetParameter(1, syndicate1)
                    .ExecuteUpdate();
            }
        }
    }
}