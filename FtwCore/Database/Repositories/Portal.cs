﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Portal.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using FtwCore.Database.Entities;
using NHibernate.Criterion;

#endregion

namespace FtwCore.Database.Repositories
{
    public sealed class PortalRepository : HibernateDataRow<PortalEntity>
    {
        public PortalRepository()
            : base(SessionFactory.ResourceConnection)
        {
        }

        public PortalEntity GetByIndex(uint mapid, uint index)
        {
            using (var sessionFactory = GetSession())
            {
                return sessionFactory
                    .CreateCriteria<PortalEntity>()
                    .Add(Restrictions.And(Restrictions.Eq("MapId", mapid), Restrictions.Eq("PortalIndex", index)))
                    .SetMaxResults(1)
                    .UniqueResult<PortalEntity>();
            }
        }

        public IList<PortalEntity> GetAllPortals()
        {
            using (var sessionFactory = GetSession())
            {
                return sessionFactory
                    .CreateCriteria<PortalEntity>()
                    .List<PortalEntity>();
            }
        }
    }
}