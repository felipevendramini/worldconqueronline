﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Arena.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/14 19:45
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using FtwCore.Database.Entities;
using NHibernate.Criterion;

#endregion

namespace FtwCore.Database.Repositories
{
    public sealed class ArenaRepository : HibernateDataRow<ArenaEntity>
    {
        public ArenaRepository()
            : base(SessionFactory.GameConnection)
        {
        }

        public IList<ArenaEntity> FetchAll()
        {
            using (var pSession = GetSession())
                return pSession
                    .CreateCriteria<ArenaEntity>()
                    .List<ArenaEntity>();
        }

        public IList<ArenaEntity> FetchAll(byte mode)
        {
            using (var session = GetSession())
                return session.CreateCriteria<ArenaEntity>()
                    .Add(Restrictions.Eq("RankType", mode))
                    .List<ArenaEntity>();
        }

        public ArenaEntity GetById(uint id, byte mode)
        {
            using (var pSession = GetSession())
                return pSession
                    .CreateCriteria<ArenaEntity>()
                    .Add(Restrictions.And(Restrictions.Eq("PlayerIdentity", id), Restrictions.Eq("Mode", mode)))
                    .SetMaxResults(1)
                    .UniqueResult<ArenaEntity>();
        }
    }
}