﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Account.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using FtwCore.Database.Entities;
using NHibernate.Criterion;

#endregion

namespace FtwCore.Database.Repositories
{
    public class AccountRepository : HibernateDataRow<AccountEntity>
    {
        public AccountRepository()
            : base(SessionFactory.AccountConnection)
        {
        }

        /// <summary>
        ///     This method gatters the user information by the name.
        /// </summary>
        /// <param name="szName">The account username.</param>
        /// <returns>Returns all information of the account. Null if not found.</returns>
        public AccountEntity SearchByName(string szName)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<AccountEntity>()
                    .Add(Restrictions.Eq("Username", szName))
                    .SetMaxResults(1)
                    .UniqueResult<AccountEntity>();
        }

        /// <summary>
        ///     Gatters the user information by his Key. (ID)
        /// </summary>
        /// <param name="nIdentity">The account ID.</param>
        /// <returns>Returns all information of the account. Null if not found.</returns>
        public AccountEntity SearchByIdentity(uint nIdentity)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<AccountEntity>()
                    .Add(Restrictions.Eq("Identity", nIdentity))
                    .SetMaxResults(1)
                    .UniqueResult<AccountEntity>();
        }

        public IList<AccountEntity> SearchByParent(uint idParent)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<AccountEntity>()
                    .Add(Restrictions.Eq("AccountIdentity", idParent))
                    .List<AccountEntity>();
        }
    }
}