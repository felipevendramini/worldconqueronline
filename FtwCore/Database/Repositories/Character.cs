﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Character.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using FtwCore.Database.Entities;
using NHibernate.Criterion;

#endregion

namespace FtwCore.Database.Repositories
{
    public sealed class CharacterRepository : HibernateDataRow<CharacterEntity>
    {
        public CharacterRepository()
            : base(SessionFactory.GameConnection)
        {
        }

        public CharacterRepository(MysqlTargetConnection target)
            : base(ActionRepository.GetConnection(target))
        {
        }

        public CharacterEntity SearchByName(string szName)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<CharacterEntity>()
                    .Add(Restrictions.Eq("Name", szName))
                    .SetMaxResults(1)
                    .UniqueResult<CharacterEntity>();
        }

        public CharacterEntity SearchByIdentity(uint nId)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<CharacterEntity>()
                    .Add(Restrictions.Eq("Identity", nId))
                    .SetMaxResults(1)
                    .UniqueResult<CharacterEntity>();
        }

        public CharacterEntity SearchByAccount(uint nId)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<CharacterEntity>()
                    .Add(Restrictions.Eq("AccountId", nId))
                    .SetMaxResults(1)
                    .UniqueResult<CharacterEntity>();
        }

        public bool AccountHasCharacter(uint nAccountId)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<CharacterEntity>()
                           .Add(Restrictions.Eq("AccountId", nAccountId))
                           .SetMaxResults(1)
                           .UniqueResult<CharacterEntity>() != null;
        }

        public bool CharacterExists(string szName)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<CharacterEntity>()
                           .Add(Restrictions.Eq("Name", szName))
                           .SetMaxResults(1)
                           .UniqueResult<CharacterEntity>() != null;
        }

        public bool CharacterExists(uint dwId)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<CharacterEntity>()
                           .Add(Restrictions.Eq("Identity", dwId))
                           .SetMaxResults(1)
                           .UniqueResult<CharacterEntity>() != null;
        }

        public bool CreateNewCharacter(CharacterEntity newUser)
        {
            try
            {
                using (var sessionFactory = GetSession())
                {
                    bool check = sessionFactory.CreateCriteria<CharacterEntity>()
                                     .Add(Restrictions.Eq("Name", newUser.Name))
                                     .UniqueResult<CharacterEntity>() != null;

                    return !check && Save(newUser);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
        }

        public IList<CharacterEntity> FetchAll()
        {
            using (var pSession = GetSession())
                return pSession
                    .CreateCriteria<CharacterEntity>()
                    .List<CharacterEntity>();
        }

        public int DeletePlayer(uint identity0)
        {
            using (var sessionFactory = GetSession())
            {
                return sessionFactory.CreateSQLQuery("CALL DeleteUser (?);")
                    .AddEntity(typeof(CharacterEntity))
                    .SetParameter(0, identity0)
                    .ExecuteUpdate();
            }
        }

        public void ChangeName(uint idUser, string szNewName, string szOldName)
        {
            using (var sessionFactory = GetSession())
            {
                sessionFactory.CreateSQLQuery("CALL ChangeName (?,?,?);")
                    .AddEntity(typeof(CharacterEntity))
                    .SetParameter(0, idUser)
                    .SetParameter(1, szNewName)
                    .SetParameter(2, szOldName)
                    .ExecuteUpdate();
            }
        }
    }
}