﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Item.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using FtwCore.Database.Entities;
using NHibernate.Criterion;

#endregion

namespace FtwCore.Database.Repositories
{
    public sealed class ItemRepository : HibernateDataRow<ItemEntity>
    {
        public ItemRepository()
            : base(SessionFactory.GameConnection)
        {
        }

        public IList<ItemEntity> FetchByUser(uint dwId)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<ItemEntity>()
                    .Add(Restrictions.Eq("PlayerId", dwId))
                    .List<ItemEntity>();
        }

        public IList<ItemEntity> FetchInventory(uint dwId)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<ItemEntity>()
                    .Add(Restrictions.And(Restrictions.Eq("PlayerId", dwId), Restrictions.Eq("Position", 0)))
                    .SetMaxResults(40)
                    .List<ItemEntity>();
        }

        public IList<ItemEntity> FetchAll()
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<ItemEntity>()
                    .List<ItemEntity>();
        }

        public IList<ItemEntity> FetchAll(uint dwId)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<ItemEntity>()
                    .Add(Restrictions.Eq("PlayerId", dwId))
                    .List<ItemEntity>();
        }

        public IList<ItemEntity> FetchAllByType(uint dwId)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<ItemEntity>()
                    .Add(Restrictions.Eq("Type", dwId))
                    .List<ItemEntity>();
        }

        public ItemEntity FetchByIdentity(uint dwId)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<ItemEntity>()
                    .Add(Restrictions.Eq("Id", dwId))
                    .SetMaxResults(1)
                    .UniqueResult<ItemEntity>();
        }
    }
}