﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Business.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using FtwCore.Database.Entities;
using NHibernate.Criterion;

#endregion

namespace FtwCore.Database.Repositories
{
    public sealed class BusinessRepository : HibernateDataRow<BusinessEntity>
    {
        public BusinessRepository()
            : base(SessionFactory.GameConnection)
        {
        }

        public IList<BusinessEntity> FetchByUser(uint dwId)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<BusinessEntity>()
                    .Add(Restrictions.Eq("Userid", dwId))
                    .List<BusinessEntity>();
        }

        /// <summary>
        ///     This procedure will delete offline trade partnership.
        /// </summary>
        /// <param name="idTarget">The identity of the target (business)</param>
        /// <param name="idSource">The identity of the asker or offline user (Userid)</param>
        public void DeleteBusiness(uint idTarget, uint idSource)
        {
            using (var sessionFactory = GetSession())
            {
                sessionFactory.CreateSQLQuery("CALL DeleteTradeBuddy (?,?);")
                    .AddEntity(typeof(BusinessEntity))
                    .SetParameter(0, idTarget)
                    .SetParameter(1, idSource)
                    .ExecuteUpdate();
            }
        }
    }
}