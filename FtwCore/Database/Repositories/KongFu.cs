﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - KongFu.cs
// Last Edit: 2020/01/12 19:12
// Created: 2019/12/14 18:01
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using FtwCore.Database.Entities;
using NHibernate.Criterion;

#endregion

namespace FtwCore.Database.Repositories
{
    public sealed class KongFuRepository : HibernateDataRow<KongFuEntity>
    {
        public KongFuRepository()
            : base(SessionFactory.GameConnection)
        {
        }

        public bool HasJiangHu(uint idUser)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<KongFuEntity>()
                           .Add(Restrictions.Eq("UserIdentity", idUser))
                           .SetMaxResults(1)
                           .UniqueResult<KongFuEntity>() != null;
        }

        public bool CheckNameExists(string szName)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<KongFuEntity>()
                           .Add(Restrictions.Eq("Name", szName))
                           .SetMaxResults(1)
                           .UniqueResult<KongFuEntity>() != null;
        }

        public KongFuEntity FetchByIdentity(uint idUser)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<KongFuEntity>()
                    .Add(Restrictions.Eq("UserIdentity", idUser))
                    .SetMaxResults(1)
                    .UniqueResult<KongFuEntity>();
        }

        public IList<KongFuEntity> FetchRank(int from, int to)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<KongFuEntity>()
                    .Add(Restrictions.Gt("Score", 0u))
                    .AddOrder(Order.Desc("Score"))
                    .AddOrder(Order.Desc("LastTraining"))
                    .SetFirstResult(from)
                    .SetMaxResults(to)
                    .List<KongFuEntity>();
        }
    }
}