﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Monarchy Config.cs
// Last Edit: 2019/11/28 17:43
// Created: 2019/11/28 17:07
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using FtwCore.Database.Entities;
using NHibernate.Criterion;

#endregion

namespace FtwCore.Database.Repositories
{
    public sealed class MonarchyConfigRepository : HibernateDataRow<MonarchyConfigEntity>
    {
        public MonarchyConfigRepository()
            : base(SessionFactory.GameConnection)
        {
        }

        public IList<MonarchyConfigEntity> FetchLatest(uint dwId)
        {
            using (var pSession = GetSession())
                return pSession.QueryOver<MonarchyConfigEntity>()
                    .Select(
                        Projections.Distinct(Projections.Property<MonarchyConfigEntity>(entity => entity.ConfigStr)))
                    .OrderBy(entity => entity.Identity).Desc
                    .List<MonarchyConfigEntity>();
        }
    }
}