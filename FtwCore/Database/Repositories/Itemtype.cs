﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Itemtype.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using FtwCore.Database.Entities;
using NHibernate.Criterion;

#endregion

namespace FtwCore.Database.Repositories
{
    public sealed class ItemtypeRepository : HibernateDataRow<ItemtypeEntity>
    {
        public ItemtypeRepository()
            : base(SessionFactory.ResourceConnection)
        {
        }

        public IList<ItemtypeEntity> FetchAll()
        {
            using (var pSession = GetSession())
                return pSession
                    .CreateCriteria<ItemtypeEntity>()
                    .List<ItemtypeEntity>();
        }

        public IList<ItemtypeEntity> FetchAllByType(uint dwType)
        {
            if (dwType > 1000)
                return null;
            uint min = dwType * 1000;
            uint max = min + 999;
            using (var pSession = GetSession())
                return pSession
                    .CreateCriteria<ItemtypeEntity>()
                    .Add(Restrictions.Between("Type", min, max))
                    .SetMaxResults(1000)
                    .List<ItemtypeEntity>();
        }
    }
}