﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - NameChangeLog.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/11/12 22:49
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using FtwCore.Database.Entities;
using NHibernate.Criterion;

#endregion

namespace FtwCore.Database.Repositories
{
    public sealed class NameChangeLogRepository : HibernateDataRow<NameChangeLogEntity>
    {
        public NameChangeLogRepository()
            : base(SessionFactory.GameConnection)
        {
        }

        public int RemainingAmount(uint userId, int time, int maxAmount = 3)
        {
            return int.Parse(
                ExecutePureQuery(
                        $"SELECT {maxAmount}-COUNT(*) FROM cq_user_name_log WHERE user_id={userId} AND `timestamp`>unix_timestamp()-{time} AND `timestamp`<unix_timestamp();")
                    .ToString());
        }

        public bool HasQueuedChange(uint userId)
        {
            return int.Parse(ExecutePureQuery($"SELECT `changed` FROM cq_user_name_log WHERE user_id={userId} LIMIT 1")
                                 ?.ToString() ?? "1") == 0;
        }

        public bool NameIsDisponible(string name)
        {
            return int.Parse(ExecutePureQuery(
                                     "SELECT COUNT(*) FROM cq_user_name_log WHERE old_name='' AND `timestamp`>unix_timestamp()-60*60*24*30*12;")
                                 ?.ToString() ?? "0") == 0;
        }

        public IList<NameChangeLogEntity> FetchAllQueued()
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<NameChangeLogEntity>()
                    .Add(Restrictions.Eq("Changed", (byte)0))
                    .List<NameChangeLogEntity>();
        }
    }
}