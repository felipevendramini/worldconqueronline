﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Achievements.cs
// Last Edit: 2020/01/14 16:23
// Created: 2020/01/14 16:22
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using FtwCore.Database.Entities;
using NHibernate.Criterion;

#endregion

namespace FtwCore.Database.Repositories
{
    public sealed class AchievementsRepository : HibernateDataRow<AchievementsEntity>
    {
        public AchievementsRepository()
            : base(SessionFactory.GameConnection)
        {
        }

        public IList<AchievementsEntity> FetchByUser(uint idUser)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<AchievementsEntity>()
                    .Add(Restrictions.Eq("UserIdentity", idUser))
                    .List<AchievementsEntity>();
        }
    }
}