﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Trap Type.cs
// Last Edit: 2019/12/07 22:30
// Created: 2019/12/07 22:30
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using FtwCore.Database.Entities;
using NHibernate.Criterion;

#endregion

namespace FtwCore.Database.Repositories
{
    public sealed class TrapTypeRepository : HibernateDataRow<TrapTypeEntity>
    {
        public TrapTypeRepository()
            : base(SessionFactory.GameConnection)
        {
        }

        public IList<TrapTypeEntity> FetchAll()
        {
            using (var pSession = GetSession())
                return pSession
                    .CreateCriteria<TrapTypeEntity>()
                    .List<TrapTypeEntity>();
        }

        public TrapTypeEntity GetById(uint id)
        {
            using (var pSession = GetSession())
                return pSession
                    .CreateCriteria<TrapTypeEntity>()
                    .Add(Restrictions.Eq("Id", id))
                    .SetMaxResults(1)
                    .UniqueResult<TrapTypeEntity>();
        }
    }
}