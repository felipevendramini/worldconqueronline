﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - KongFuAttr.cs
// Last Edit: 2019/12/14 18:02
// Created: 2019/12/14 18:01
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using System.Linq;
using FtwCore.Database.Entities;
using NHibernate.Criterion;

#endregion

namespace FtwCore.Database.Repositories
{
    public sealed class KongFuAttrRepository : HibernateDataRow<KongFuAttrEntity>
    {
        public KongFuAttrRepository()
            : base(SessionFactory.GameConnection)
        {
        }

        public IList<KongFuAttrEntity> FetchAll(uint idUser)
        {
            using (var pSession = GetSession())
                return pSession.CreateCriteria<KongFuAttrEntity>()
                    .Add(Restrictions.Eq("UserIdentity", idUser))
                    .AddOrder(Order.Asc("Stage"))
                    .AddOrder(Order.Asc("Star"))
                    .List<KongFuAttrEntity>();
        }
    }
}