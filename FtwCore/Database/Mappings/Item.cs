﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Item.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    public class ItemMapping : ClassMap<ItemEntity>
    {
        public ItemMapping()
        {
            Table(TableName.ITEM);
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("id");
            Map(x => x.Type).Column("type").Not.Nullable();
            Map(x => x.OwnerId).Column("owner_id").Not.Nullable();
            Map(x => x.PlayerId).Column("player_id").Not.Nullable();
            Map(x => x.Amount).Column("amount").Not.Nullable();
            Map(x => x.AmountLimit).Column("amount_limit").Not.Nullable();
            Map(x => x.Ident).Column("ident").Not.Nullable();
            Map(x => x.Position).Column("position").Not.Nullable();
            Map(x => x.Gem1).Column("gem1").Not.Nullable();
            Map(x => x.Gem2).Column("gem2").Not.Nullable();
            Map(x => x.Magic1).Column("magic1").Not.Nullable();
            Map(x => x.Magic2).Column("magic2").Not.Nullable();
            Map(x => x.Magic3).Column("magic3").Not.Nullable();
            Map(x => x.Data).Column("data").Not.Nullable();
            Map(x => x.ReduceDmg).Column("reduce_dmg").Not.Nullable();
            Map(x => x.AddLife).Column("add_life").Not.Nullable();
            Map(x => x.AntiMonster).Column("anti_monster").Not.Nullable();
            Map(x => x.ChkSum).Column("chk_sum").Not.Nullable();
            Map(x => x.Plunder).Column("plunder").Not.Nullable();
            Map(x => x.RemainingTime).Column("remaining_time").Not.Nullable();
            Map(x => x.Specialflag).Column("SpecialFlag").Not.Nullable();
            Map(x => x.Color).Column("color").Not.Nullable();
            Map(x => x.AddlevelExp).Column("Addlevel_exp").Not.Nullable();
            Map(x => x.Monopoly).Column("monopoly").Not.Nullable();
            Map(x => x.Inscribed).Column("inscribed").Not.Nullable();
            Map(x => x.ArtifactType).Column("artifact_type").Not.Nullable();
            Map(x => x.ArtifactStart).Column("artifact_start").Not.Nullable();
            Map(x => x.ArtifactExpire).Column("artifact_expire").Not.Nullable();
            Map(x => x.ArtifactStabilization).Column("artifact_stabilization").Not.Nullable();
            Map(x => x.RefineryType).Column("refinery_type").Not.Nullable();
            Map(x => x.RefineryLevel).Column("refinery_level").Not.Nullable();
            Map(x => x.RefineryStart).Column("refinery_start").Not.Nullable();
            Map(x => x.RefineryExpire).Column("refinery_expire").Not.Nullable();
            Map(x => x.RefineryStabilization).Column("refinery_stabilization").Not.Nullable();
            Map(x => x.StackAmount).Column("stack_amount").Not.Nullable().Default("1");
        }
    }
}