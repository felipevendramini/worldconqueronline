﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - ItemAddition.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    public class ItemAdditionMapping : ClassMap<ItemAdditionEntity>
    {
        public ItemAdditionMapping()
        {
            Table(TableName.ITEMADDITION);
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("id");
            Map(x => x.TypeId).Column("typeid").Not.Nullable();
            Map(x => x.Level).Default("0").Column("level").Not.Nullable();
            Map(x => x.Life).Default("0").Column("life").Not.Nullable();
            Map(x => x.AttackMax).Column("attack_max").Default("0").Not.Nullable();
            Map(x => x.AttackMin).Column("attack_min").Default("0").Not.Nullable();
            Map(x => x.Defense).Column("defense").Default("0").Not.Nullable();
            Map(x => x.MagicAtk).Column("magic_atk").Default("0").Not.Nullable();
            Map(x => x.MagicDef).Column("magic_def").Default("0").Not.Nullable();
            Map(x => x.Dexterity).Column("dexterity").Default("0").Not.Nullable();
            Map(x => x.Dodge).Column("dodge").Default("0").Not.Nullable();
        }
    }
}