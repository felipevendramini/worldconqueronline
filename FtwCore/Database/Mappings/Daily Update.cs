﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Daily Update.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class DailyUpdateMapping : ClassMap<DailyUpdateEntity>
    {
        public DailyUpdateMapping()
        {
            Table(TableName.DAILY_UPDATE);
            LazyLoad();
            Id(x => x.Identity, "id").Not.Nullable().GeneratedBy.Identity();
            Map(x => x.Timestamp, "timestamp").Not.Nullable();
            Map(x => x.ElapsedMilliseconds, "elapsed_ms").Not.Nullable();
            Map(x => x.OnlinePlayers, "online_players").Not.Nullable();
            Map(x => x.OfflinePlayers, "offline_players").Not.Nullable();
            Map(x => x.ServerVersion, "server_version").Not.Nullable();
        }
    }
}