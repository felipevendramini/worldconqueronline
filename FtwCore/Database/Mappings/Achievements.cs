﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Achievements.cs
// Last Edit: 2020/01/14 16:22
// Created: 2020/01/14 16:20
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class AchievementsMapping : ClassMap<AchievementsEntity>
    {
        public AchievementsMapping()
        {
            Table(TableName.ACHIEVEMENTS);
            LazyLoad();
            Id(x => x.Identity, "id").GeneratedBy.Identity();
            Map(x => x.UserIdentity, "user_id").Not.Nullable();
            Map(x => x.Achievement, "achievement_flag").Not.Nullable();
            Map(x => x.EarnDate, "earn_date").Not.Nullable();
        }
    }
}