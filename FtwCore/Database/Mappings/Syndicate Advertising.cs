﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Syndicate Advertising.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class SyndicateAdvertisingMapping : ClassMap<SyndicateAdvertisingEntity>
    {
        public SyndicateAdvertisingMapping()
        {
            Table(TableName.SYNDICATE_ADVERTISE);
            LazyLoad();
            Id(x => x.Identity).GeneratedBy.Identity().Not.Nullable().Default("0").Column("id");
            Map(x => x.SyndicateIdentity).Column("syn_id").Default("0").Not.Nullable();
            Map(x => x.SyndicateName).Column("syn_name").Default("NONE").Not.Nullable();
            Map(x => x.Message).Column("message").Default("Join my guild please.").Not.Nullable();
            Map(x => x.Donation).Column("amount").Default("0").Not.Nullable();
            Map(x => x.Timestamp).Column("add_date").Default("0").Not.Nullable();
            Map(x => x.RequiredLevel).Column("req_lev").Default("1").Not.Nullable();
            Map(x => x.RequiredMetempsychosis).Column("req_pro").Default("0").Not.Nullable();
            Map(x => x.RequiredProfession).Column("req_metempsychosis").Default("0").Not.Nullable();
            Map(x => x.AutoRecruit).Column("auto").Default("1").Not.Nullable();
        }
    }
}