﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Monarchy.cs
// Last Edit: 2019/11/28 16:25
// Created: 2019/11/28 12:12
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class MonarchyMapping : ClassMap<MonarchyEntity>
    {
        public MonarchyMapping()
        {
            Table(TableName.MONARCHY);
            LazyLoad();
            Id(x => x.Identity, "id").Not.Nullable().GeneratedBy.Identity();
            Map(x => x.EmperorIdentity, "emperor_id").Not.Nullable().Default("0");
            Map(x => x.EmperorName, "emperor_name").Not.Nullable().Default("");
            Map(x => x.StartDate, "start_date").Not.Nullable().Default("");
            Map(x => x.Abdication, "abdication").Nullable().Default("");
            Map(x => x.CurrentMoney, "current_money").Not.Nullable().Default("0");
            Map(x => x.StartMoney, "start_money").Not.Nullable().Default("0");
            Map(x => x.AbdicateMoney, "abdication_money").Not.Nullable().Default("0");
            Map(x => x.CurrentEMoney, "current_emoney").Not.Nullable().Default("0");
            Map(x => x.StartEMoney, "start_emoney").Not.Nullable().Default("0");
            Map(x => x.AbdicateEMoney, "abdication_emoney").Not.Nullable().Default("0");
        }
    }
}