﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Generator.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class GeneratorMapping : ClassMap<GeneratorEntity>
    {
        public GeneratorMapping()
        {
            Table(TableName.GENERATOR);
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("id");
            Map(x => x.Mapid).Column("mapid").Not.Nullable();
            Map(x => x.BoundX).Column("bound_x").Not.Nullable();
            Map(x => x.BoundY).Column("bound_y").Not.Nullable();
            Map(x => x.BoundCx).Column("bound_cx").Not.Nullable();
            Map(x => x.BoundCy).Column("bound_cy").Not.Nullable();
            Map(x => x.MaxNpc).Column("maxnpc").Not.Nullable();
            Map(x => x.RestSecs).Column("rest_secs").Not.Nullable();
            Map(x => x.MaxPerGen).Column("max_per_gen").Not.Nullable();
            Map(x => x.Npctype).Column("npctype").Not.Nullable();
            Map(x => x.TimerBegin).Column("timer_begin").Not.Nullable();
            Map(x => x.TimerEnd).Column("timer_end").Not.Nullable();
            Map(x => x.BornX).Column("born_x").Not.Nullable();
            Map(x => x.BornY).Column("born_y").Not.Nullable();
        }
    }
}