﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - KongFu.cs
// Last Edit: 2019/12/15 17:55
// Created: 2019/12/14 17:50
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class KongFuMapping : ClassMap<KongFuEntity>
    {
        public KongFuMapping()
        {
            Table(TableName.KONGFU);
            LazyLoad();
            Id(x => x.Identity, "id").GeneratedBy.Identity();
            Map(x => x.UserIdentity, "user_id").Not.Nullable();
            Map(x => x.Name, "name").Not.Nullable();
            Map(x => x.Score, "score").Not.Nullable();
            Map(x => x.CurrentTalent, "current_talent").Not.Nullable();
            Map(x => x.FreeCourses, "free_courses").Not.Nullable();
            Map(x => x.CoursesAvailable, "courses_available").Not.Nullable();
            Map(x => x.TalentPoints, "talent_points").Not.Nullable();
            Map(x => x.UnlockDate, "unlock_date").Not.Nullable();
            Map(x => x.TrainingCount, "training_count").Not.Nullable();
            Map(x => x.TrainingCountToday, "training_count_today").Not.Nullable();
            Map(x => x.LastTraining, "last_training").Nullable();
            Map(x => x.RemainingToExit, "remaining_leave_jiang").Not.Nullable();
        }
    }
}