﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Statistic.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class Statistic : ClassMap<StatisticEntity>
    {
        public Statistic()
        {
            Table(TableName.STATISTIC);
            LazyLoad();
            Id(x => x.Identity).Column("id").GeneratedBy.Identity().Not.Nullable();
            Map(x => x.PlayerIdentity).Column("player_id").Not.Nullable().Default("0");
            Map(x => x.PlayerName).Column("player_name").Not.Nullable().Default("Undefined");
            Map(x => x.EventType).Column("event_type").Not.Nullable().Default("0");
            Map(x => x.DataType).Column("data_type").Not.Nullable().Default("0");
            Map(x => x.Data).Column("data").Not.Nullable().Default("0");
            Map(x => x.Timestamp).Column("timestamp").Not.Nullable().Default("0");
        }
    }
}