﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Account.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    public class AccountMapping : ClassMap<AccountEntity>
    {
        public AccountMapping()
        {
            Table(TableName.ACCOUNT_TABLE);
            Id(x => x.Identity).Column("id").Not.Nullable();
            Map(x => x.Username).Column("name").Not.Nullable();
            Map(x => x.Password).Column("password").Not.Nullable();
            Map(x => x.CreationDate).Column("create_date").Not.Nullable();
            Map(x => x.FirstLogin).Column("first_login").Not.Nullable();
            Map(x => x.Vip).Column("vip").Not.Nullable().Default("0");
            Map(x => x.Lock).Column("lock").Not.Nullable().Default("0");
            Map(x => x.Type).Column("type").Not.Nullable().Default("2");
            Map(x => x.LastLogin).Column("last_login").Not.Nullable().Default("0");
            Map(x => x.MacAddress).Column("mac_addr").Not.Nullable().Default("000000000000");
            Map(x => x.LockExpire).Column("lock_expire").Not.Nullable().Default("0");
            Map(x => x.IpAddress).Column("netbar_ip").Not.Nullable().Default("127.0.0.1");
            Map(x => x.AccountIdentity).Column("account_id").Not.Nullable().Default("0");
        }
    }
}