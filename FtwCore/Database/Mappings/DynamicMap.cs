﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - DynamicMap.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    public class DynamicMapMapping : ClassMap<DynamicMapEntity>
    {
        public DynamicMapMapping()
        {
            Table(TableName.DYNAMAP);
            LazyLoad();
            Id(x => x.Identity).GeneratedBy.Identity().Column("id");
            Map(x => x.Name).Column("name").Not.Nullable();
            Map(x => x.Description).Column("describe_text").Not.Nullable();
            Map(x => x.MapDoc).Column("mapdoc").Not.Nullable();
            Map(x => x.Type).Column("type").Not.Nullable();
            Map(x => x.OwnerId).Column("owner_id").Not.Nullable();
            Map(x => x.Mapgroup).Column("mapgroup").Not.Nullable();
            Map(x => x.Idxserver).Column("idxserver").Not.Nullable();
            Map(x => x.Weather).Column("weather").Not.Nullable();
            Map(x => x.Bgmusic).Column("bgmusic").Not.Nullable();
            Map(x => x.BgmusicShow).Column("bgmusic_show").Not.Nullable();
            Map(x => x.Portal0X).Column("portal0_x").Not.Nullable();
            Map(x => x.Portal0Y).Column("portal0_y").Not.Nullable();
            Map(x => x.RebornMapid).Column("reborn_mapid").Not.Nullable();
            Map(x => x.RebornPortal).Column("reborn_portal").Not.Nullable();
            Map(x => x.ResLev).Column("res_lev").Not.Nullable();
            Map(x => x.OwnerType).Column("owner_type").Not.Nullable();
            Map(x => x.LinkMap).Column("link_map").Not.Nullable();
            Map(x => x.LinkX).Column("link_x").Not.Nullable();
            Map(x => x.LinkY).Column("link_y").Not.Nullable();
            Map(x => x.DelFlag).Column("del_flag").Not.Nullable();
            Map(x => x.Color).Column("color");
            Map(x => x.FileName).Column("file_name").Not.Nullable();
        }
    }
}