﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Monstertype.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class MonstertypeMapping : ClassMap<MonstertypeEntity>
    {
        public MonstertypeMapping()
        {
            Table(TableName.MONSTERTYPE);
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("id");
            Map(x => x.Name).Column("name").Not.Nullable();
            Map(x => x.Type).Column("type").Not.Nullable();
            Map(x => x.Lookface).Column("lookface").Not.Nullable();
            Map(x => x.Life).Column("life").Not.Nullable();
            Map(x => x.Mana).Column("mana").Not.Nullable();
            Map(x => x.AttackMax).Column("attack_max").Not.Nullable();
            Map(x => x.AttackMin).Column("attack_min").Not.Nullable();
            Map(x => x.Defence).Column("defence").Not.Nullable();
            Map(x => x.Dexterity).Column("dexterity").Not.Nullable();
            Map(x => x.Dodge).Column("dodge").Not.Nullable();
            Map(x => x.HelmetType).Column("helmet_type").Not.Nullable();
            Map(x => x.ArmorType).Column("armor_type").Not.Nullable();
            Map(x => x.WeaponrType).Column("weaponr_type").Not.Nullable();
            Map(x => x.WeaponlType).Column("weaponl_type").Not.Nullable();
            Map(x => x.AttackRange).Column("attack_range").Not.Nullable();
            Map(x => x.ViewRange).Column("view_range").Not.Nullable();
            Map(x => x.EscapeLife).Column("escape_life").Not.Nullable();
            Map(x => x.AttackSpeed).Column("attack_speed").Not.Nullable();
            Map(x => x.MoveSpeed).Column("move_speed").Not.Nullable();
            Map(x => x.Level).Column("level").Not.Nullable();
            Map(x => x.AttackUser).Column("attack_user").Not.Nullable();
            Map(x => x.DropMoney).Column("drop_money").Not.Nullable();
            Map(x => x.DropItemtype).Column("drop_itemtype").Not.Nullable();
            Map(x => x.SizeAdd).Column("size_add").Not.Nullable();
            Map(x => x.Action).Column("action").Not.Nullable();
            Map(x => x.RunSpeed).Column("run_speed").Not.Nullable();
            Map(x => x.DropArmet).Column("drop_armet").Not.Nullable();
            Map(x => x.DropNecklace).Column("drop_necklace").Not.Nullable();
            Map(x => x.DropArmor).Column("drop_armor").Not.Nullable();
            Map(x => x.DropRing).Column("drop_ring").Not.Nullable();
            Map(x => x.DropWeapon).Column("drop_weapon").Not.Nullable();
            Map(x => x.DropShield).Column("drop_shield").Not.Nullable();
            Map(x => x.DropShoes).Column("drop_shoes").Not.Nullable();
            Map(x => x.DropHp).Column("drop_hp").Not.Nullable();
            Map(x => x.DropMp).Column("drop_mp").Not.Nullable();
            Map(x => x.MagicType).Column("magic_type").Not.Nullable();
            Map(x => x.MagicDef).Column("magic_def").Not.Nullable();
            Map(x => x.MagicHitrate).Column("magic_hitrate").Not.Nullable();
            Map(x => x.AiType).Column("ai_type").Not.Nullable();
            Map(x => x.Defence2).Column("defence2").Not.Nullable();
            Map(x => x.StcType).Column("stc_type").Not.Nullable();
            Map(x => x.AntiMonster).Column("anti_monster").Not.Nullable();
            Map(x => x.ExtraBattlelev).Column("extra_battlelev").Not.Nullable();
            Map(x => x.ExtraExp).Column("extra_exp").Not.Nullable();
            Map(x => x.ExtraDamage).Column("extra_damage").Not.Nullable();
            Map(x => x.WaterAtk).Column("water_atk").Not.Nullable();
            Map(x => x.FireAtk).Column("fire_atk").Not.Nullable();
            Map(x => x.EarthAtk).Column("earth_atk").Not.Nullable();
            Map(x => x.WoodAtk).Column("wood_atk").Not.Nullable();
            Map(x => x.MetalAtk).Column("metal_atk").Not.Nullable();
            Map(x => x.WaterDef).Column("water_def").Not.Nullable();
            Map(x => x.FireDef).Column("fire_def").Not.Nullable();
            Map(x => x.EarthDef).Column("earth_def").Not.Nullable();
            Map(x => x.WoodDef).Column("wood_def").Not.Nullable();
            Map(x => x.MetalDef).Column("metal_def").Not.Nullable();
            Map(x => x.Boss).Column("boss").Not.Nullable();
        }
    }
}