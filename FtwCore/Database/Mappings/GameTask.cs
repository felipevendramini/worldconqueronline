﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - GameTask.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    public class GameTaskMapping : ClassMap<GameTaskEntity>
    {
        public GameTaskMapping()
        {
            Table(TableName.TASK);
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("id");
            Map(x => x.IdNext).Column("id_next");
            Map(x => x.IdNextfail).Column("id_nextfail");
            Map(x => x.Itemname1).Column("itemname1");
            Map(x => x.Itemname2).Column("itemname2");
            Map(x => x.Money).Column("money");
            Map(x => x.Profession).Column("profession");
            Map(x => x.Sex).Column("sex");
            Map(x => x.MinPk).Column("min_pk");
            Map(x => x.MaxPk).Column("max_pk");
            Map(x => x.Team).Column("team");
            Map(x => x.Metempsychosis).Column("metempsychosis");
            Map(x => x.Query).Column("query").Not.Nullable();
            Map(x => x.Marriage).Column("marriage").Not.Nullable();
            Map(x => x.ClientActive).Column("client_active").Not.Nullable();
        }
    }
}