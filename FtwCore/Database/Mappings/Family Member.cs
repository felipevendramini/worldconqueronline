﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Family Member.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/11/24 18:58
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class FamilyMemberMapping : ClassMap<FamilyMemberEntity>
    {
        public FamilyMemberMapping()
        {
            Table(TableName.FAMILY_ATTR);
            LazyLoad();
            Id(x => x.Identity).Column("id").Not.Nullable().Default("0");
            Map(x => x.FamilyIdentity).Column("family_id").Not.Nullable().Default("0");
            Map(x => x.Position).Column("rank").Not.Nullable().Default("0");
            Map(x => x.Money).Column("money").Not.Nullable().Default("0");
            Map(x => x.JoinDate).Column("join_date").Not.Nullable().Default("0");
        }
    }
}