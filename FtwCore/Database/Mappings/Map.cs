﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Map.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    public class MapMapping : ClassMap<MapEntity>
    {
        public MapMapping()
        {
            Table(TableName.MAP);
            LazyLoad();
            Id(x => x.Identity).GeneratedBy.Identity().Not.Nullable().Column("id");
            Map(x => x.Name).Not.Nullable().Column("name").Default("");
            Map(x => x.Description).Column("describe_text").Default("by Felipe Vieira");
            Map(x => x.MapDoc).Column("mapdoc");
            Map(x => x.Type).Column("type").Not.Nullable();
            Map(x => x.OwnerId).Column("owner_id");
            Map(x => x.MapGroup).Column("mapgroup");
            Map(x => x.IdXServer).Column("idxserver");
            Map(x => x.PortalX).Column("portal0_x");
            Map(x => x.PortalY).Column("portal0_y");
            Map(x => x.RebornMap).Column("reborn_map");
            Map(x => x.RebornPortal).Column("reborn_portal");
            Map(x => x.ResLevel).Column("res_lev");
            Map(x => x.Path).Column("file_name").Not.Nullable();
        }
    }
}