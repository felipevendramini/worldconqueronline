﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Npc.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    public class Npc : ClassMap<NpcEntity>
    {
        public Npc()
        {
            Table(TableName.NPC);
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("id");
            Map(x => x.Ownerid).Column("ownerid");
            Map(x => x.Playerid).Column("playerid");
            Map(x => x.Name).Column("name").Not.Nullable();
            Map(x => x.Type).Column("type");
            Map(x => x.Lookface).Column("lookface");
            Map(x => x.Idxserver).Column("idxserver");
            Map(x => x.Mapid).Column("mapid");
            Map(x => x.Cellx).Column("cellx");
            Map(x => x.Celly).Column("celly");
            Map(x => x.Task0).Column("task0");
            Map(x => x.Task1).Column("task1");
            Map(x => x.Task2).Column("task2");
            Map(x => x.Task3).Column("task3");
            Map(x => x.Task4).Column("task4");
            Map(x => x.Task5).Column("task5");
            Map(x => x.Task6).Column("task6");
            Map(x => x.Task7).Column("task7");
            Map(x => x.Data0).Column("data0").Not.Nullable();
            Map(x => x.Data1).Column("data1").Not.Nullable();
            Map(x => x.Data2).Column("data2").Not.Nullable();
            Map(x => x.Data3).Column("data3").Not.Nullable();
            Map(x => x.Datastr).Column("datastr");
            Map(x => x.Linkid).Column("linkid").Not.Nullable();
            Map(x => x.Life).Column("life").Not.Nullable();
            Map(x => x.Maxlife).Column("maxlife").Not.Nullable();
            Map(x => x.Base).Column("base").Not.Nullable();
            Map(x => x.Sort).Column("sort").Not.Nullable();
            Map(x => x.Itemid).Column("itemid");
        }
    }
}