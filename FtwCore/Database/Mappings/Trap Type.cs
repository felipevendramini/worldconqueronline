﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Trap Type.cs
// Last Edit: 2019/12/07 22:29
// Created: 2019/12/07 22:29
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class TrapTypeMapping : ClassMap<TrapTypeEntity>
    {
        public TrapTypeMapping()
        {
            Table(TableName.TRAP_TYPE);
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("id");
            Map(x => x.Sort).Column("sort").Not.Nullable();
            Map(x => x.Look).Column("look").Not.Nullable();
            Map(x => x.ActionId).Column("action_id").Not.Nullable();
            Map(x => x.Level).Column("level").Not.Nullable();
            Map(x => x.AttackMax).Column("attack_max").Not.Nullable();
            Map(x => x.AttackMin).Column("attack_min").Not.Nullable();
            Map(x => x.Dexterity).Column("dexterity").Not.Nullable();
            Map(x => x.AttackSpeed).Column("attack_speed").Not.Nullable();
            Map(x => x.ActiveTimes).Column("active_times").Not.Nullable();
            Map(x => x.MagicType).Column("magic_type").Not.Nullable();
            Map(x => x.MagicHitrate).Column("magic_hitrate").Not.Nullable();
            Map(x => x.Size).Column("size").Not.Nullable();
            Map(x => x.AtkMode).Column("atk_mode").Not.Nullable();
        }
    }
}