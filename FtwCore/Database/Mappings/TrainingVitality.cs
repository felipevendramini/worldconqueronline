﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - TrainingVitality.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    public class TrainingVitalityMapping : ClassMap<TrainingVitalityEntity>
    {
        public TrainingVitalityMapping()
        {
            Table(TableName.TRAINING_VITALITY);
            LazyLoad();
            Id(x => x.Identity).Column("id").GeneratedBy.Identity().Not.Nullable();
            Map(x => x.UserIdentity).Column("user_id").Not.Nullable().Default("0");
            Map(x => x.Mode).Column("type").Not.Nullable().Default("0");
            Map(x => x.Power1).Column("power1").Not.Nullable().Default("0");
            Map(x => x.Power2).Column("power2").Not.Nullable().Default("0");
            Map(x => x.Power3).Column("power3").Not.Nullable().Default("0");
            Map(x => x.Power4).Column("power4").Not.Nullable().Default("0");
            Map(x => x.StageLock).Column("stage_lock").Not.Nullable().Default("0");
        }
    }
}