﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Trap.cs
// Last Edit: 2019/12/07 22:24
// Created: 2019/12/07 22:24
// ////////////////////////////////////////////////////////////////////////////////////

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

namespace FtwCore.Database.Mappings
{
    internal class TrapMapping : ClassMap<TrapEntity>
    {
        public TrapMapping()
        {
            Table(TableName.TRAP);
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("id");
            Map(x => x.Type).Column("type").Not.Nullable();
            Map(x => x.Look).Column("look").Not.Nullable();
            Map(x => x.OwnerId).Column("owner_id").Not.Nullable();
            Map(x => x.MapId).Column("map_id").Not.Nullable();
            Map(x => x.PosX).Column("pos_x").Not.Nullable();
            Map(x => x.PosY).Column("pos_y").Not.Nullable();
            Map(x => x.Data).Column("data").Not.Nullable();
        }
    }
}