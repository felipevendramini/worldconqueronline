﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Monster Magic.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class MonsterMagicMapping : ClassMap<MonsterMagicEntity>
    {
        public MonsterMagicMapping()
        {
            Table(TableName.MONSTER_MAGIC);
            LazyLoad();
            Id(x => x.Identity).GeneratedBy.Identity().Not.Nullable().Column("id");
            Map(x => x.OwnerIdentity).Not.Nullable().Default("0").Column("monster_id");
            Map(x => x.MagicIdentity).Not.Nullable().Default("0").Column("magic_id");
            Map(x => x.MagicLevel).Not.Nullable().Default("0").Column("magic_level");
            Map(x => x.Chance).Not.Nullable().Default("10000").Column("chance");
        }
    }
}