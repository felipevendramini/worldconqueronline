﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Status.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class StatusMapping : ClassMap<StatusEntity>
    {
        public StatusMapping()
        {
            Table(TableName.STATUS);
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("id");
            Map(x => x.OwnerId).Column("owner_id").Not.Nullable();
            Map(x => x.Status).Column("status").Not.Nullable();
            Map(x => x.Power).Column("power").Not.Nullable();
            Map(x => x.Sort).Column("sort").Not.Nullable();
            Map(x => x.LeaveTimes).Column("leave_times").Not.Nullable();
            Map(x => x.RemainTime).Column("remain_time").Not.Nullable();
            Map(x => x.EndTime).Column("end_time").Not.Nullable();
            Map(x => x.IntervalTime).Column("interval_time").Not.Nullable();
        }
    }
}