﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Mail.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class MailMapping : ClassMap<MailEntity>
    {
        public MailMapping()
        {
            Table(TableName.MAIL);
            LazyLoad();
            Id(x => x.Identity, "id").GeneratedBy.Identity().Not.Nullable();
            Map(x => x.SenderIdentity, "sender_id").Default("0").Not.Nullable();
            Map(x => x.SenderName, "sender_name").Default("").Not.Nullable();
            Map(x => x.TargetIdentity, "target_id").Default("0").Not.Nullable();
            Map(x => x.TargetName, "target_name").Default("").Not.Nullable();
            Map(x => x.Title, "title").Default("").Not.Nullable();
            Map(x => x.Content, "message").Default("").Not.Nullable();
            Map(x => x.Money, "money").Default("0").Not.Nullable();
            Map(x => x.Emoney, "emoney").Default("0").Not.Nullable();
            Map(x => x.SendTime, "send_time").Not.Nullable();
            Map(x => x.ReadTime, "read_time").Nullable();
            Map(x => x.ExpireTime, "expire_time").Nullable();
            Map(x => x.Flag, "flag").Default("0").Not.Nullable();
        }
    }
}