﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - FateRank.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/11/08 19:07
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class FateRankMapping : ClassMap<FateRankEntity>
    {
        public FateRankMapping()
        {
            Table(TableName.FATE_RANK);
            LazyLoad();
            Id(x => x.Identity, "id").GeneratedBy.Identity();
            Map(x => x.Fate, "fate").Not.Nullable().Default("0");
            Map(x => x.Ranking, "rank").Not.Nullable().Default("0");
            Map(x => x.Power0, "value0").Not.Nullable().Default("0");
            Map(x => x.Power1, "value1").Not.Nullable().Default("0");
            Map(x => x.Power2, "value2").Not.Nullable().Default("0");
            Map(x => x.Power3, "value3").Not.Nullable().Default("0");
        }
    }
}