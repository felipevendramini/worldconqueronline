﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Game Login Rcd.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/11/21 23:24
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class GameLoginRcdMapping : ClassMap<GameLoginRcdEntity>
    {
        public GameLoginRcdMapping()
        {
            Table(TableName.GAME_LOGIN_RCD);
            LazyLoad();
            Id(x => x.Identity, "id").GeneratedBy.Identity().Not.Nullable();
            Map(x => x.AccountIdentity, "account_id").Not.Nullable().Default("0");
            Map(x => x.UserIdentity, "user_id").Not.Nullable().Default("0");
            Map(x => x.OnlineTime, "session_secs").Not.Nullable().Default("0");
            Map(x => x.LoginTime, "login_time").Not.Nullable().Default("0");
            Map(x => x.LogoutTime, "logout_time").Not.Nullable().Default("0");
            Map(x => x.ServerVersion, "server_version").Not.Nullable().Default("");
        }
    }
}