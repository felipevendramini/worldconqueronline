﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - LoginRcd.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    public class LoginRcdMapping : ClassMap<LoginRcdEntity>
    {
        public LoginRcdMapping()
        {
            Table("login_rcd");
            LazyLoad();
            Id(x => x.Identity).GeneratedBy.Identity().Not.Nullable().Column("id");
            Map(x => x.UserIdentity).Not.Nullable().Default("0").Column("account_id");
            Map(x => x.LoginTime).Not.Nullable().Default("0").Column("login_time");
            Map(x => x.OnlineSecond).Not.Nullable().Default("0").Column("online_second");
            Map(x => x.MacAddress).Not.Nullable().Default("000000000000").Column("mac_adr");
            Map(x => x.IpAddress).Not.Nullable().Default("127.0.0.1").Column("ip_adr");
            Map(x => x.ResourceSource).Not.Nullable().Default("2").Column("res_src");
            Map(x => x.Geolocation).Not.Nullable().Default("").Column("geolocation");
        }
    }
}