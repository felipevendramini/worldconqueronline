﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Arena.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/14 19:24
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class ArenaMapping : ClassMap<ArenaEntity>
    {
        public ArenaMapping()
        {
            Table(TableName.ARENA);
            LazyLoad();
            Id(x => x.Identity).GeneratedBy.Identity().Not.Nullable().Default("0").Column("id");
            Map(x => x.PlayerIdentity).Not.Nullable().Default("0").Column("user_id");
            Map(x => x.Name).Not.Nullable().Default("FELIPZAO").Column("name");
            Map(x => x.Lookface).Not.Nullable().Default("0").Column("lookface");
            Map(x => x.Level).Not.Nullable().Default("0").Column("level");
            Map(x => x.Profession).Not.Nullable().Default("0").Column("profession");
            Map(x => x.WinsToday).Not.Nullable().Default("0").Column("win");
            Map(x => x.WinsTotal).Not.Nullable().Default("0").Column("total_win");
            Map(x => x.LossToday).Not.Nullable().Default("0").Column("loss");
            Map(x => x.LossTotal).Not.Nullable().Default("0").Column("total_loss");
            Map(x => x.Points).Not.Nullable().Default("0").Column("points");
            Map(x => x.TotalHonorPoints).Not.Nullable().Default("0").Column("total_honor_points");
            Map(x => x.Mode).Not.Nullable().Default("0").Column("mode");
            Map(x => x.LastRank).Not.Nullable().Default("0").Column("last_rank");
            Map(x => x.LastWin).Not.Nullable().Default("0").Column("last_win");
            Map(x => x.LastLoss).Not.Nullable().Default("0").Column("last_loss");
            Map(x => x.LastPoints).Not.Nullable().Default("0").Column("last_points");
            Map(x => x.LockReleaseTime).Not.Nullable().Default("0").Column("lock_release");
            Map(x => x.RankType).Not.Nullable().Default("0").Column("rank_type");
        }
    }
}