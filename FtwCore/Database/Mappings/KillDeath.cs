﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - KillDeath.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FluentNHibernate.Mapping;
using FtwCore.Database.Entities;

#endregion

namespace FtwCore.Database.Mappings
{
    internal class KillDeathMapping : ClassMap<KillDeathEntity>
    {
        public KillDeathMapping()
        {
            Table(TableName.KILL_DEATH);
            LazyLoad();
            Id(x => x.Identity).GeneratedBy.Identity().Column("id");
            Map(x => x.AttackerIdentity, "attacker_id").Not.Nullable();
            Map(x => x.AttackerName, "attacker_name").Not.Nullable();
            Map(x => x.AttackerMap, "attacker_map_id").Not.Nullable();
            Map(x => x.AttackerMapX, "attacker_map_x").Not.Nullable();
            Map(x => x.AttackerMapY, "attacker_map_y").Not.Nullable();
            Map(x => x.AttackerBattlePower, "attacker_battlepower").Not.Nullable();
            Map(x => x.AttackerLevel, "attacker_level").Not.Nullable();
            Map(x => x.AttackerProfession, "attacker_profession").Not.Nullable();
            Map(x => x.TargetIdentity, "target_id").Not.Nullable();
            Map(x => x.TargetName, "target_name").Not.Nullable();
            Map(x => x.TargetMap, "target_map_id").Not.Nullable();
            Map(x => x.TargetMapX, "target_map_x").Not.Nullable();
            Map(x => x.TargetMapY, "target_map_y").Not.Nullable();
            Map(x => x.TargetBattlePower, "target_battlepower").Not.Nullable();
            Map(x => x.TargetLevel, "target_level").Not.Nullable();
            Map(x => x.TargetProfession, "target_profession").Not.Nullable();
            Map(x => x.Timestamp, "timestamp").Not.Nullable();
            Map(x => x.BattleMode, "battle_mode").Not.Nullable();
        }
    }
}