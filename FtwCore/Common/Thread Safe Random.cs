﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Thread Safe Random.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;

#endregion

namespace FtwCore.Common
{
    public static class ThreadSafeRandom
    {
        private static readonly Random m_global = new Random();
        [ThreadStatic] private static Random _local;
        private static int count = 0;

        public static int RandGet()
        {
            Random inst = _local;
            if (inst == null || count++ >= 10000)
            {
                int seed;
                lock (m_global) seed = m_global.Next();
                _local = inst = new Random(seed);
                count = 0;
            }

            return inst.Next();
        }

        public static int RandGet(int nValue)
        {
            Random inst = _local;
            if (inst == null || count++ >= 10000)
            {
                int seed;
                lock (m_global) seed = m_global.Next(nValue);
                _local = inst = new Random(seed);
                count = 0;
            }

            return inst.Next(nValue);
        }

        public static int RandGet(int nMin, int nMax)
        {
            Random inst = _local;
            if (inst == null || count++ >= 10000)
            {
                int seed;
                lock (m_global) seed = m_global.Next(nMin, nMax);
                _local = inst = new Random(seed);
                count = 0;
            }

            return inst.Next(nMin, nMax);
        }
    }
}