﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - MyXml.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/16 14:00
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;

#endregion

namespace FtwCore.Common
{
    public sealed class MyXml
    {
        private XDocument m_xdRead;
        private string m_szFile;

        public MyXml(string file)
        {
            m_szFile = file;
            m_xdRead = XDocument.Load(file);
        }

        #region Getters

        public int GetInt(params string[] where)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var str in where)
                sb.AppendFormat("/{0}", str);
            return int.TryParse(m_xdRead.XPathSelectElement(sb.ToString())?.Value, out int result) ? result : 0;
        }

        public string GetStr(params string[] where)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var str in where)
                sb.AppendFormat("/{0}", str);
            return m_xdRead.XPathSelectElement(sb.ToString())?.Value ?? string.Empty;
        }

        public MyXmlElement GetCollection(params string[] where)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var str in where)
                sb.AppendFormat("/{0}", str);
            return new MyXmlElement(m_xdRead.XPathSelectElements(sb.ToString()), m_xdRead);
        }

        #endregion

        #region Setters

        public void SetValue(int value, params string[] where)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var str in where)
                sb.AppendFormat("/{0}", str);
            try
            {
                m_xdRead.XPathSelectElement(sb.ToString()).Value = value.ToString();
            }
            finally
            {
                m_xdRead.Save(m_szFile);
            }
        }

        public void SetValue(string value, params string[] where)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var str in where)
                sb.AppendFormat("/{0}", str);
            try
            {
                m_xdRead.XPathSelectElement(sb.ToString()).Value = value;
            }
            finally
            {
                m_xdRead.Save(m_szFile);
            }
        }

        #endregion
    }

    public class MyXmlElement
    {
        private XDocument m_xdRead;
        private List<XElement> m_myElements;
        private int m_nCurrentIdx;

        public MyXmlElement(IEnumerable<XElement> elements, XDocument doc)
        {
            m_xdRead = doc;
            m_myElements = new List<XElement>(elements);
        }

        public int Count => m_myElements.Count;

        public XElement Current => m_myElements[Math.Max(0, Math.Min(m_myElements.Count - 1, m_nCurrentIdx))];

        public void First()
        {
            m_nCurrentIdx = 0;
        }

        public bool Next()
        {
            if (m_nCurrentIdx + 1 >= m_myElements.Count)
                return false;

            m_nCurrentIdx++;
            return true;
        }

        public bool Previous()
        {
            if (m_nCurrentIdx - 1 < 0)
                return false;

            m_nCurrentIdx--;
            return true;
        }

        public void Last()
        {
            m_nCurrentIdx = m_myElements.Count - 1;
        }

        #region Getters

        public int GetInt(string where)
        {
            return int.TryParse(Current.Element(where)?.Value, out int result) ? result : 0;
        }

        public string GetStr(string where)
        {
            return Current.Element(where)?.Value ?? string.Empty;
        }

        #endregion

        #region Setters

        public void SetValue(int value, string where)
        {
            try
            {
                Current.Element(where).Value = value.ToString();
            }
            finally
            {
                m_xdRead.Save(m_xdRead.BaseUri);
            }
        }

        public void SetValue(string value, string where)
        {
            try
            {
                Current.Element(where).Value = value.ToString();
            }
            finally
            {
                m_xdRead.Save(m_xdRead.BaseUri);
            }
        }

        #endregion
    }
}