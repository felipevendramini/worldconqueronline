﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Refinery Type.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Common.Enums
{
    public enum RefineryMode : uint
    {
        None,
        RefMdefense = 1,
        RefCriticalStrike = 2,
        RefScriticalStrike = 3,
        RefImmunity = 4,
        RefBreakthrough = 5,
        RefCounteraction = 6,
        RefDetoxication = 7,
        RefBlock = 8,
        RefPenetration = 9, // REF_PENES = 9, HEUEHUEHUE BRBRBR
        RefIntensification = 10,
        RefFireResist = 11,
        RefWaterResist = 12,
        RefWoodResist = 13,
        RefMetalResist = 14,
        RefEarthResist = 15,
        RefFinalMattack = 16, // Addicts +FinalMDamage
        RefFinalMdamage = 17 // Reduces -FinalMDefense
    }
}