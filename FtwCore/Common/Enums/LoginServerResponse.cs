﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - LoginServerResponse.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Common.Enums
{
    public enum LoginServerResponse : byte
    {
        /// <summary>
        /// Sent by the LoginServer after a successful connection.
        /// </summary>
        LoginSuccessful = 10,

        /// <summary>
        /// Sent by the LoginServer after an denied attempt of the MsgServer to login with a wrong username or password.
        /// </summary>
        LoginDeniedLogin,

        /// <summary>
        /// Sent by the LoginServer after denying the MsgServer to login due to a invalid IP Address.
        /// </summary>
        LoginDeniedIpaddress,

        /// <summary>
        /// Sent by the LoginServer after denying the MsgServer to login with a unauthorized Server Name.
        /// </summary>
        LoginDeniedServerNotExist,

        /// <summary>
        /// Semt by the LoginServer after a manual disconnection.
        /// </summary>
        LoginDeniedDisconnected
    }
}