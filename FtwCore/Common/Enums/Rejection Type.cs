﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Rejection Type.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Common.Enums
{
    /// <summary>
    /// This enumeration type defines the identities for error messages that can be displayed in the client from
    /// the login screen. These messages can only be sent and displayed in the account verification phase 
    /// (account server only). Most of these types are not supported by the target client (5187). 
    /// </summary>
    public enum RejectionType
    {
        InvalidPassword = 1,
        PointCardExpired = 6,
        MonthlyCardExpired = 7,
        ServerMaintenance = 10,
        PleaseTryAgainLater = 11,
        AccountBanned = 12,
        ServerFull = 20,
        ServerBusy = 21,
        AccountLocked = 22,
        AccountNotActivated = 30,
        FailedToActivateAccount = 31,
        NonCooperatorAccount = 50,
        MaximumLoginAttempts = 51,
        InvalidAccount = 57,
        ValidationTimeout = 58,
        ServersNotConfigured = 59,
        DatabaseError = 64,
        ServerLocked = 70,
        InvalidAuthenticationProtocol = 73
    }
}