﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Map Type Flags.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;

#endregion

namespace FtwCore.Common.Enums
{
    [Flags]
    public enum MapTypeFlags
    {
        Normal = 0,
        PkField = 1 << 0, //0x1 1
        ChangeMapDisable = 1 << 1, //0x2 2
        RecordDisable = 1 << 2, //0x4 4 
        PkDisable = 1 << 3, //0x8 8
        BoothEnable = 1 << 4, //0x10 16
        TeamDisable = 1 << 5, //0x20 32
        TeleportDisable = 1 << 6, // 0x40 64
        GuildMap = 1 << 7, // 0x80 128
        PrisonMap = 1 << 8, // 0x100 256
        WingDisable = 1 << 9, // 0x200 512
        Family = 1 << 10, // 0x400 1024
        MineField = 1 << 11, // 0x800 2048
        PkGame = 1 << 12, // 0x1000 4098
        NeverWound = 1 << 13, // 0x2000 8196
        DeadIsland = 1 << 14, // 0x4000 16392
        SkillMap = 1 << 17, // 0x20000 65568
        LineSkillOnly = 1 << 18
    }
}