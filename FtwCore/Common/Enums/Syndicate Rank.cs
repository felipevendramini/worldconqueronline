﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Syndicate Rank.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Common.Enums
{
    public enum SyndicateRank : ushort
    {
        GuildLeader = 1000,

        DeputyLeader = 990,
        HonoraryDeputyLeader = 980,
        LeaderSpouse = 920,

        Manager = 890,
        HonoraryManager = 880,

        TulipSupervisor = 859,
        OrchidSupervisor = 858,
        CpSupervisor = 857,
        ArsenalSupervisor = 856,
        SilverSupervisor = 855,
        GuideSupervisor = 854,
        PkSupervisor = 853,
        RoseSupervisor = 852,
        LilySupervisor = 851,
        Supervisor = 850,
        HonorarySupervisor = 840,

        Steward = 690,
        HonorarySteward = 680,
        DeputySteward = 650,
        DeputyLeaderSpouse = 620,
        DeputyLeaderAide = 611,
        LeaderSpouseAide = 610,
        Aide = 602,

        TulipAgent = 599,
        OrchidAgent = 598,
        CpAgent = 597,
        ArsenalAgent = 596,
        SilverAgent = 595,
        GuideAgent = 594,
        PkAgent = 593,
        RoseAgent = 592,
        LilyAgent = 591,
        Agent = 590,
        SupervisorSpouse = 521,
        ManagerSpouse = 520,
        SupervisorAide = 511,
        ManagerAide = 510,

        TulipFollower = 499,
        OrchidFollower = 498,
        CpFollower = 497,
        ArsenalFollower = 496,
        SilverFollower = 495,
        GuideFollower = 494,
        PkFollower = 493,
        RoseFollower = 492,
        LilyFollower = 491,
        Follower = 490,
        StewardSpouse = 420,

        SeniorMember = 210,
        Member = 200,

        None = 0
    }
}