﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - TotemPoleType.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Common.Enums
{
    public enum TotemPoleType
    {
        TOTEM_HEADGEAR = 0,
        TOTEM_ARMOR = 1,
        TOTEM_WEAPON = 2,
        TOTEM_RING = 3,
        TOTEM_BOOTS = 4,
        TOTEM_NECKLACE = 5,
        TOTEM_FAN = 6,
        TOTEM_TOWER = 7,
        TOTEM_NONE = 8
    }
}