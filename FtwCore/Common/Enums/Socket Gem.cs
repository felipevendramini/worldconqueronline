﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Socket Gem.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Common.Enums
{
    public enum SocketGem : byte
    {
        NormalPhoenixGem = 1,
        RefinedPhoenixGem = 2,
        SuperPhoenixGem = 3,

        NormalDragonGem = 11,
        RefinedDragonGem = 12,
        SuperDragonGem = 13,

        NormalFuryGem = 21,
        RefinedFuryGem = 22,
        SuperFuryGem = 23,

        NormalRainbowGem = 31,
        RefinedRainbowGem = 32,
        SuperRainbowGem = 33,

        NormalKylinGem = 41,
        RefinedKylinGem = 42,
        SuperKylinGem = 43,

        NormalVioletGem = 51,
        RefinedVioletGem = 52,
        SuperVioletGem = 53,

        NormalMoonGem = 61,
        RefinedMoonGem = 62,
        SuperMoonGem = 63,

        NormalTortoiseGem = 71,
        RefinedTortoiseGem = 72,
        SuperTortoiseGem = 73,

        NormalThunderGem = 101,
        RefinedThunderGem = 102,
        SuperThunderGem = 103,

        NormalGloryGem = 121,
        RefinedGloryGem = 122,
        SuperGloryGem = 123,

        NoSocket = 0,
        EmptySocket = 255
    }
}