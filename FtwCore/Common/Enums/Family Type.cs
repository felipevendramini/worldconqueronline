﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Family Type.cs
// Last Edit: 2019/11/24 19:01
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Common.Enums
{
    public enum FamilyType : byte
    {
        Info = 1,
        Members = 4,
        Recruit = 9,
        AcceptRecruit = 10,
        Join = 11,
        AcceptJoinRequest = 12,
        SendEnemy = 13,
        AddEnemy = 14,
        DeleteEnemy = 15,
        SendAlly = 16,
        AddAlly = 17,
        AcceptAlliance = 18,
        DeleteAlly = 20,
        TransferLeader = 21,
        Kickout = 22,
        Quit = 23,
        Announce = 24,
        SetAnnouncement = 25,
        Dedicate = 26,
        MyClan = 29
    }

    public enum FamilyRank : ushort
    {
        ClanLeader = 100,
        Spouse = 11,
        Member = 10,
        None = 0
    }
}