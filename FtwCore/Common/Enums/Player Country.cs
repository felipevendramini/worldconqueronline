﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Player Country.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Common.Enums
{
    public enum PlayerCountry
    {
        UnitedArabEmirates = 1,
        Argentine,
        Australia,
        Belgium,
        Brazil,
        Canada,
        China,
        Colombia,
        CostaRica,
        CzechRepublic,
        Conquer,
        Germany,
        Denmark,
        DominicanRepublic,
        Egypt,
        Spain,
        Estland,
        Finland,
        France,
        UnitedKingdom,
        HongKong,
        Indonesia,
        India,
        Israel,
        Italy,
        Japan,
        Kuwait,
        SriLanka,
        Lithuania,
        Mexico,
        Macedonia,
        Malaysia,
        Netherlands,
        Norway,
        NewZealand,
        Peru,
        Philippines,
        Poland,
        PuertoRico,
        Portugal,
        Palestine,
        Qatar,
        Romania,
        Russia,
        SaudiArabia,
        Singapore,
        Sweden,
        Thailand,
        Turkey,
        UnitedStates,
        Venezuela,
        Vietnam = 52
    }
}