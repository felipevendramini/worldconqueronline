﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Task Action Type.cs
// Last Edit: 2019/11/24 20:23
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Common.Enums
{
    public enum TaskActionType
    {
        // System
        ActionSysFirst = 100,
        ActionMenutext = 101,
        ActionMenulink = 102,
        ActionMenuedit = 103,
        ActionMenupic = 104,
        ACTION_MENUBUTTON = 110,
        ACTION_MENULISTPART = 111,
        ActionMenucreate = 120,
        ActionRand = 121,
        ActionRandaction = 122,
        ActionChktime = 123,
        ActionPostcmd = 124,
        ActionBrocastmsg = 125,
        ActionMessagebox = 126,
        ActionExecutequery = 127,
        ActionSysLimit = 199,

        //NPC
        ActionNpcFirst = 200,
        ActionNpcAttr = 201,
        ActionNpcErase = 205,
        ActionNpcModify = 206,
        ActionNpcResetsynowner = 207,
        ActionNpcFindNextTable = 208,
        ACTION_NPC_ADD_TABLE = 209,
        ACTION_NPC_DEL_TABLE = 210,
        ACTION_NPC_DEL_INVALID = 211,
        ACTION_NPC_TABLE_AMOUNT = 212,
        ACTION_NPC_SYS_AUCTION = 213,
        ACTION_NPC_DRESS_SYNCLOTHING = 214,
        ACTION_NPC_TAKEOFF_SYNCLOTHING = 215,
        ACTION_NPC_AUCTIONING = 216,
        ActionNpcLimit = 299,

        //Map
        ActionMapFirst = 300,
        ActionMapMovenpc = 301,
        ActionMapMapuser = 302,
        ActionMapBrocastmsg = 303,
        ActionMapDropitem = 304,
        ActionMapSetstatus = 305,
        ActionMapAttrib = 306,
        ActionMapRegionMonster = 307,
        ActionMapChangeweather = 310,
        ActionMapChangelight = 311,
        ActionMapMapeffect = 312,
        ACTION_MAP_CREATEMAP = 313,
        ActionMapFireworks = 314,
        ActionMapLimit = 399,

        //Item
        ActionItemonlyFirst = 400,
        ACTION_ITEM_REQUESTLAYNPC = 401,
        ACTION_ITEM_COUNTNPC = 402,
        ACTION_ITEM_LAYNPC = 403,
        ACTION_ITEM_DELTHIS = 498,
        ActionItemonlyLimit = 499,
        ActionItemFirst = 500,
        ActionItemAdd = 501,
        ActionItemDel = 502,
        ActionItemCheck = 503,
        ActionItemHole = 504,
        ActionItemRepair = 505,
        ActionItemMultidel = 506,
        ActionItemMultichk = 507,
        ActionItemLeavespace = 508,
        ActionItemUpequipment = 509,
        ActionItemEquiptest = 510,
        ActionItemEquipexist = 511,
        ActionItemEquipcolor = 512,
        ActionItemRemoveAny = 513,
        ActionItemCheckrand = 516,
        ActionItemModify = 517,
        ActionItemJarCreate = 528,
        ActionItemJarVerify = 529,
        ActionItemLimit = 599,

        //Dyn NPCs
        ACTION_NPCONLY_FIRST = 600,
        ACTION_NPCONLY_CREATENEW_PET = 601,
        ACTION_NPCONLY_DELETE_PET = 602,
        ACTION_NPCONLY_MAGICEFFECT = 603,
        ACTION_NPCONLY_MAGICEFFECT2 = 604,
        ACTION_NPCONLY_LIMIT = 699,

        // Syndicate
        ActionSynFirst = 700,
        ActionSynCreate = 701,
        ActionSynDestroy = 702,
        ActionSynAttr = 717,
        ActionSynChangeLeader = 709,
        ActionSynLimit = 799,

        //Monsters
        ActionMstFirst = 800,
        ActionMstDropitem = 801,
        ACTION_MST_MAGIC = 802,
        ActionMstRefinery = 803,
        ActionMstLimit = 899,

        //Family
        ActionFamilyFirst = 900,
        ActionFamilyCreate = 901,
        ActionFamilyDestroy = 902,
        ActionFamilyAttr = 917,
        ActionFamilyUplev = 918,
        ActionFamilyBpuplev = 919,
        ActionFamilyLimit = 999,

        //User
        ActionUserFirst = 1000,
        ActionUserAttr = 1001,
        ActionUserFull = 1002, // Fill the user attributes. param is the attribute name. life/mana/xp/sp
        ActionUserChgmap = 1003, // Mapid Mapx Mapy savelocation
        ActionUserRecordpoint = 1004, // Records the user location, so he can be teleported back there later.
        ActionUserHair = 1005,
        ActionUserChgmaprecord = 1006,
        ACTION_USER_CHGLINKMAP = 1007,
        ActionUserTransform = 1008,
        ActionUserIspure = 1009,
        ActionUserTalk = 1010,
        ActionUserMagic = 1020,
        ActionUserWeaponskill = 1021,
        ActionUserLog = 1022,
        ActionUserBonus = 1023,
        ActionUserDivorce = 1024,
        ActionUserMarriage = 1025,
        ActionUserSex = 1026,
        ActionUserEffect = 1027,
        ACTION_USER_TASKMASK = 1028,
        ActionUserMediaplay = 1029,
        ACTION_USER_SUPERMANLIST = 1030,
        ActionUserAddTitle = 1031,
        ActionUserRemoveTitle = 1032,
        ACTION_USER_CREATEMAP = 1033,
        ACTION_USER_ENTER_HOME = 1034,
        ACTION_USER_ENTER_MATE_HOME = 1035,
        ACTION_USER_CHKIN_CARD2 = 1036,
        ACTION_USER_CHKOUT_CARD2 = 1037,
        ACTION_USER_FLY_NEIGHBOR = 1038,
        ACTION_USER_UNLEARN_MAGIC = 1039,
        ActionUserRebirth = 1040,
        ActionUserWebpage = 1041,
        ActionUserBbs = 1042,
        ActionUserUnlearnSkill = 1043,
        ACTION_USER_DROP_MAGIC = 1044,
        ActionUserFixAttr = 1045,
        ActionUserOpenDialog = 1046,
        ActionUserPointAllot = 1047,
        ActionUserExpMultiply = 1048,
        ACTION_USER_DEL_WPG_BADGE = 1049,
        ACTION_USER_CHK_WPG_BADGE = 1050,
        ACTION_USER_TAKESTUDENTEXP = 1051,
        ActionUserWhPassword = 1052,
        ActionUserSetWhPassword = 1053,
        ActionUserOpeninterface = 1054,
        ActionUserVarCompare = 1060,
        ActionUserVarDefine = 1061,
        ActionUserVarCalc = 1064,
        ActionUserStcCompare = 1073,
        ActionUserStcOpe = 1074,
        ActionUserTaskManager = 1080,
        ActionUserTaskOpe = 1081,
        ActionUserAttachStatus = 1082,
        ActionUserGodTime = 1083,
        ActionUserExpballExp = 1086,
        ActionUserStatusCreate = 1096,
        ActionUserStatusCheck = 1098,

        //User -> Team
        ActionTeamBroadcast = 1101,
        ActionTeamAttr = 1102,
        ActionTeamLeavespace = 1103,
        ActionTeamItemAdd = 1104,
        ActionTeamItemDel = 1105,
        ActionTeamItemCheck = 1106,
        ActionTeamChgmap = 1107,
        ActionTeamChkIsleader = 1501,

        // User -> General
        ActionGeneralLottery = 1508,
        ActionGeneraSubclassManagement = 1509,
        ActionGeneralSkillLineEnabled = 1510,

        // User -> Elite PK
        ACTION_ELITEPK_INSCRIBE = 1601,
        ACTION_ELITEPK_UNINSCRIBE = 1602,
        ACTION_ELITEPK_CHECKPRIZE = 1603,
        ACTION_ELITEPK_AWARDPRIZE = 1604,

        ActionUserLimit = 1999,

        //Events
        ActionEventFirst = 2000,
        ActionEventSetstatus = 2001,
        ActionEventDelnpcGenid = 2002,
        ActionEventCompare = 2003,
        ActionEventCompareUnsigned = 2004,
        ACTION_EVENT_CHANGEWEATHER = 2005,
        ActionEventCreatepet = 2006,
        ActionEventCreatenewNpc = 2007,
        ActionEventCountmonster = 2008,
        ActionEventDeletemonster = 2009,
        ActionEventBbs = 2010,
        ActionEventErase = 2011,
        ActionEventTeleport = 2012,
        ActionEventMassaction = 2013,
        ACTION_EVENT_SYN_SCORE_FINISH = 2014,
        ActionEventLimit = 2099,

        //Traps
        ActionTrapFirst = 2100,
        ActionTrapCreate = 2101,
        ActionTrapErase = 2102,
        ActionTrapCount = 2103,
        ACTION_TRAP_ATTR = 2104,
        ActionTrapLimit = 2199,

        // Detained Item
        ACTION_DETAIN_FIRST = 2200,
        ACTION_DETAIN_DIALOG = 2205,
        ACTION_DETAIN_LIMIT = 2299,

        //Wanted
        ACTION_WANTED_FIRST = 3000,
        ACTION_WANTED_NEXT = 3001,
        ACTION_WANTED_NAME = 3002,
        ACTION_WANTED_BONUTY = 3003,
        ACTION_WANTED_NEW = 3004,
        ACTION_WANTED_ORDER = 3005,
        ACTION_WANTED_CANCEL = 3006,
        ACTION_WANTED_MODIFYID = 3007,
        ACTION_WANTED_SUPERADD = 3008,
        ACTION_POLICEWANTED_NEXT = 3010,
        ACTION_POLICEWANTED_ORDER = 3011,
        ACTION_POLICEWANTED_CHECK = 3012,
        ACTION_WANTED_LIMIT = 3099,

        //Magic
        ACTION_MAGIC_FIRST = 4000,
        ACTION_MAGIC_ATTACHSTATUS = 4001,
        ACTION_MAGIC_ATTACK = 4002,
        ACTION_MAGIC_LIMIT = 4099
    }
}