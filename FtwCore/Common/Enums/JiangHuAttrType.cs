﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - JiangHuAttrType.cs
// Last Edit: 2019/12/14 18:24
// Created: 2019/12/14 18:22
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Common.Enums
{
    public enum JiangHuAttrType : byte
    {
        None,
        MaxLife,
        PAttack,
        MAttack,
        PDefense,
        Mdefense,
        FinalAttack,
        FinalMagicAttack,
        FinalDefense,
        FinalMagicDefense,
        CriticalStrike,
        SkillCriticalStrike,
        Immunity,
        Breakthrough,
        Counteraction,
        MaxMana
    }

    public enum JiangHuQuality : byte
    {
        None, Common, Sharp, Pure, Rare, Ultra, Epic
    }
}