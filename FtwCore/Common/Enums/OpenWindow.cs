﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - OpenWindow.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Common.Enums
{
    public enum OpenWindow
    {
        COMPOSE = 1,
        CRAFT = 2,
        WAREHOUSE = 4,
        CLAN_WINDOW = 64,
        DETAIN_REDEEM = 336,
        DETAIN_CLAIM = 337,
        VIP_WAREHOUSE = 341,
        BREEDING = 368,
        PURIFICATION_WINDOW = 455,
        STABILIZATION_WINDOW = 459,
        TALISMAN_UPGRADE = 347,
        GEM_COMPOSING = 422,
        OPEN_SOCKETS = 425,
        BLESSING = 426,
        TORTOISE_GEM_COMPOSING = 438,
        HORSE_RACING_STORE = 464,
        EDIT_CHARACTER_NAME = 489,
        GARMENT_TRADE = 502,
        DEGRADE_EQUIPMENT = 506
    }
}