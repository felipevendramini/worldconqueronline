﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Unix Timestamp.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;

#endregion

namespace FtwCore.Common
{
    public static class UnixTimestamp
    {
        public const int TIME_SECONDS_MINUTE = 60;
        public const int TIME_SECONDS_HOUR = 60 * TIME_SECONDS_MINUTE;
        public const int TIME_SECONDS_DAY = 24 * TIME_SECONDS_HOUR;
        public static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime();

        #region Date Time Related Functions

        public static DateTime ToDateTime(uint timestamp)
        {
            return UnixEpoch.AddSeconds(timestamp);
        }

        public static int Now()
        {
            return Convert.ToInt32((DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime())
                .TotalSeconds);
        }

        public static long NowMs()
        {
            return Convert.ToInt64((DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime())
                .TotalMilliseconds);
        }

        public static int Timestamp(DateTime time)
        {
            return Convert.ToInt32((time - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime()).TotalSeconds);
        }

        public static long LongTimestamp(DateTime time)
        {
            return Convert.ToInt64((time - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime()).TotalMilliseconds);
        }

        public static int MonthDayStamp()
        {
            return Convert.ToInt32((DateTime.Now - new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0, 0).ToLocalTime())
                .TotalSeconds);
        }

        public static int MonthDayStamp(DateTime time)
        {
            return Convert.ToInt32((time - new DateTime(DateTime.Now.Year, 1, 1, 0, 0, 0, 0).ToLocalTime())
                .TotalSeconds);
        }

        public static int DayOfTheMonthStamp()
        {
            return Convert.ToInt32((DateTime.Now -
                                    new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0, 0).ToLocalTime()
                ).TotalSeconds);
        }

        public static int DayOfTheMonthStamp(DateTime time)
        {
            return Convert.ToInt32(
                (time - new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0, 0).ToLocalTime())
                .TotalSeconds);
        }

        #endregion
    }
}