﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Item Default.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Common
{
    public static class ItemDefault
    {
        public const int MAX_UPGRADE_CHECK = 10;

        // Single hand weapon define 400000
        public const int SWEAPON_NONE = 00000;
        public const int SWEAPON_BLADE = 10000;
        public const int SWEAPON_SWORD = 20000;
        public const int SWEAPON_BACKSWORD = 21000;
        public const int SWEAPON_HOOK = 30000;
        public const int SWEAPON_WHIP = 40000;
        public const int SWEAPON_AXE = 50000;
        public const int SWEAPON_HAMMER = 60000;
        public const int SWEAPON_CLUB = 80000;
        public const int SWEAPON_SCEPTER = 81000;

        public const int SWEAPON_DAGGER = 90000;

        // Double hand weapon define 500000
        public const int DWEAPON_BOW = 00000;
        public const int DWEAPON_GLAVE = 10000;
        public const int DWEAPON_POLEAXE = 30000;
        public const int DWEAPON_LONGHAMMER = 40000;
        public const int DWEAPON_SPEAR = 60000;
        public const int DWEAPON_WANT = 61000;

        public const int DWEAPON_HALBERT = 80000;

        // Other weapon define 600000
        public const int SWEAPON_KATANA = 01000;
        public const int SWEAPON_PRAYER_BEADS = 10000;
    }

    public static class SpecialItem
    {
        //
        public const uint TYPE_DRAGONBALL = 1088000;
        public const uint TYPE_METEOR = 1088001;
        public const uint TYPE_METEORTEAR = 1088002;
        public const uint TYPE_TOUGHDRILL = 1200005;

        public const uint TYPE_STARDRILL = 1200006;

        //
        public const uint TYPE_DRAGONBALL_SCROLL = 720028; // Amount 10
        public const uint TYPE_METEOR_SCROLL = 720027; // Amount 10

        public const uint TYPE_METEORTEAR_PACK = 723711; // Amount 5

        //
        public const uint TYPE_STONE1 = 730001;
        public const uint TYPE_STONE2 = 730002;
        public const uint TYPE_STONE3 = 730003;
        public const uint TYPE_STONE4 = 730004;
        public const uint TYPE_STONE5 = 730005;
        public const uint TYPE_STONE6 = 730006;
        public const uint TYPE_STONE7 = 730007;

        public const uint TYPE_STONE8 = 730008;

        //
        public const uint TYPE_MOUNT_ID = 300000;

        //
        public const uint TYPE_EXP_BALL = 723700;

        public static readonly int[] BowmanArrows =
        {
            1050000, 1050001, 1050002, 1050020, 1050021, 1050022, 1050023, 1050030, 1050031, 1050032, 1050033, 1050040,
            1050041, 1050042, 1050043, 1050050, 1050051, 1050052
        };

        public const uint IRON_ORE = 1072010;
        public const uint COPPER_ORE = 1072020;
        public const uint EUXINITE_ORE = 1072031;
        public const uint SILVER_ORE = 1072040;
        public const uint GOLD_ORE = 1072050;

        public const uint OBLIVION_DEW = 711083;
        public const uint MEMORY_AGATE = 720828;

        public const uint PERMANENT_STONE = 723694;
        public const uint BIGPERMANENT_STONE = 723695;

        public const int LOTTERY_TICKET = 710212;
        public const uint SMALL_LOTTERY_TICKET = 711504;
    }

    public enum ItemSort
    {
        ItemsortFinery = 1,
        ItemsortSteed = 3,
        ItemsortWeaponSingleHand = 4,
        ItemsortWeaponDoubleHand = 5,
        ItemsortWeaponProfBased = 6,
        ItemsortUsable = 7,
        ItemsortWeaponShield = 9,
        ItemsortUsable2 = 10,
        ItemsortUsable3 = 12,
        ItemsortAccessory = 3,
        ItemsortTwohandAccessory = 35,
        ItemsortOnehandAccessory = 36,
        ItemsortBowAccessory = 37,
        ItemsortShieldAccessory = 38,
    }
}