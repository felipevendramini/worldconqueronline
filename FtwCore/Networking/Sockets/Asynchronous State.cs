﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Asynchronous State.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Net.Sockets;
using System.Threading;

#endregion

namespace FtwCore.Networking.Sockets
{
    /// <summary>
    /// This class encapsulates the asynchronous state from resulting asynchronous operations. It contains the
    /// player's client, packet buffer, and socket connection.
    /// </summary>
    public sealed class AsynchronousState
    {
        public object Client; // The client objected used in managing the player's instantiated game structures.
        public byte[] Buffer; // The client's packet buffer, used in managing packets.
        public Socket Socket; // The client's remote socket on the server.
        public ManualResetEvent Event; // A manual reset event for waiting on data.

        /// <summary>
        /// This class encapsulates the asynchronous state from resulting asynchronous operations. It contains the
        /// player's client, packet buffer, and socket connection.
        /// </summary>
        /// <param name="socket">The client's remote socket on the server.</param>
        public AsynchronousState(Socket socket)
        {
            Client = null;
            Buffer = null;
            Socket = socket;
            Event = new ManualResetEvent(false);
        }
    }
}