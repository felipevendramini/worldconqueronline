﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - MailMgr.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/11/24 15:41
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;

#endregion

namespace FtwCore.Networking.Mail
{
    public class MailMgr
    {
        private const string _SZ_TITLE_SUFFIX = " - FTW! Programação e Desenvolvimento";

        private readonly List<MyMailMessage> m_listMessages = new List<MyMailMessage>();
        private readonly string m_szPassword = "";
        private readonly string m_szSender = "nao-responda@ftwmasters.com.br";

        private readonly string m_szSenderName = "Dont Reply - FTW! Masters";

        public MailMgr()
        {
        }

        public MailMgr(string sender, string szSenderName, string password)
        {
            m_szSender = sender;
            m_szPassword = password;
            m_szSenderName = szSenderName;
        }

        public void Add(MyMailMessage message)
        {
            m_listMessages.Add(message);
        }

        public void Add(string target, string title, string message)
        {
            m_listMessages.Add(new MyMailMessage(target, title, message));
        }

        public bool Send()
        {
            MailAddress sender = new MailAddress(m_szSender, m_szSenderName, Encoding.UTF8);
            SmtpClient smtp = new SmtpClient("ftwmasters.com.br")
            {
                Credentials = new NetworkCredential(m_szSender, m_szPassword)
            };
            try
            {
                foreach (var message in m_listMessages)
                {
                    using (MailMessage msg = new MailMessage(sender, new MailAddress(message.Target))
                    {
                        Body = message.Message,
                        Subject = $"{message.Title}{_SZ_TITLE_SUFFIX}"
                    })
                        smtp.Send(msg);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
    }

    public class MyMailMessage
    {
        public MyMailMessage(string target, string title, string message)
        {
            Target = target;
            Title = title;
            Message = message;
        }

        public string Target { get; }

        public string Title { get; }

        public string Message { get; }
    }
}