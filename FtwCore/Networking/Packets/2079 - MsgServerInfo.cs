﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2079 - MsgServerInfo.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgServerInfo : PacketStructure
    {
        /// <summary>
        /// I still don't know why this packet is about, but without this the client
        /// wont let us signin.
        /// </summary>
        public MsgServerInfo()
            : base(20)
        {
            WriteHeader(Length - 8, PacketType.MsgServerInfo);
            ClassicMode = 0;
            PotencyMode = 0;
        }

        // 1 yes 0 no
        public ushort ClassicMode
        {
            get { return ReadUShort(4); }
            set { WriteUShort(value, 4); }
        }

        public ushort PotencyMode
        {
            get { return ReadUShort(8); }
            set { WriteUShort(value, 8); }
        }
    }
}