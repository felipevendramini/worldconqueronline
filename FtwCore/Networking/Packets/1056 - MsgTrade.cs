﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1056 - MsgTrade.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgTrade : PacketStructure
    {
        public MsgTrade()
            : base(PacketType.MsgTrade, 28, 20)
        {
        }

        public MsgTrade(byte[] packet)
            : base(packet)
        {
        }

        public uint Target
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public TradeType Type
        {
            get => (TradeType) ReadByte(8);
            set => WriteByte((byte) value, 8);
        }

        public uint Unknown
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public ushort UnknownLow
        {
            get => ReadUShort(12);
            set => WriteUShort(value, 12);
        }

        public ushort UnknownHigh
        {
            get => ReadUShort(14);
            set => WriteUShort(value, 14);
        }
    }

    public enum TradeType : byte
    {
        REQUEST = 1,
        CLOSE = 2,
        SHOW_TABLE = 3,
        HIDE_TABLE = 5,
        ADD_ITEM = 6,
        SET_MONEY = 7,
        SHOW_MONEY = 8,
        ACCEPT = 10,
        REMOVE_ITEM = 11,
        SHOW_CONQUER_POINTS = 12,
        SET_CONQUER_POINTS = 13,
        TIME_OUT = 17,
    }
}