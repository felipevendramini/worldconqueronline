﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 10014 - MsgPlayer.cs
// Last Edit: 2019/12/15 17:39
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common.Enums;

#endregion

namespace FtwCore.Networking.Packets
{
    /// <summary>
    ///     Packet Type: 10014. This packet is sent to the observing clients on the map server when the actor enters their
    ///     screen or an acting client observes the character as they enter its screen. The packet contains the player's
    ///     character spawn information. This class only encapsulates constants related to writing data to the packet
    ///     buffer. The character class handles writing to the packet as data changes.
    /// </summary>
    public sealed class MsgPlayer : PacketStructure
    {
        /// <summary>
        ///     This method should not be called, unless you're testing something. Has been created for packet research.
        /// </summary>
        public MsgPlayer(uint identity)
            : base(PacketType.MsgPlayer, 316, 308)
        {
            Identity = identity;
            StringCount = 1;
        }

        /// <summary>
        ///     Packet Type: 10014. This packet is sent to the observing clients on the map server when the actor enters their
        ///     screen or an acting client observes the character as they enter its screen. The packet contains the player's
        ///     character spawn information. This class only encapsulates constants related to writing data to the packet
        ///     buffer. The character class handles writing to the packet as data changes.
        ///     Updated to version 5517 by Felipe Vieira.
        /// </summary>
        /// <param name="characterInformation">The character initialization packet which contains all character information.</param>
        public MsgPlayer(MsgUserInfo characterInformation)
            : base(PacketType.MsgPlayer, 316, 308)
        {
            Identity = characterInformation.Identity;
            Mesh = characterInformation.Mesh;
            Hairstyle = characterInformation.Hairstyle;
            Direction = FacingDirection.SouthWest;
            Action = EntityAction.Stand;
            Life = characterInformation.Health;
            Level = characterInformation.Level;
            Metempsychosis = characterInformation.Metempsychosis;
            FirstProfession = (ProfessionType) characterInformation.AncestorProfession;
            LastProfession = (ProfessionType) characterInformation.PreviousProfession;
            Profession = (ProfessionType) characterInformation.Profession;
            QuizPoints = characterInformation.QuizPoints;
            StringCount = 4;
            Name = characterInformation.Name;
            Spouse = "None";
            FamilyName = "None";
        }

        /// <summary>
        ///     The mesh of the character, includes avatar, transformation, and body.
        /// </summary>
        public uint Mesh
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        /// <summary>
        ///     The global identification number for the player.
        /// </summary>
        public uint Identity
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        /// <summary>
        ///     The guild unique identification.
        /// </summary>
        public ushort GuildIdentity
        {
            get => ReadUShort(16);
            set => WriteUShort(value, 16);
        }

        /// <summary>
        ///     The user guild position.
        /// </summary>
        public SyndicateRank GuildRank
        {
            get => (SyndicateRank) ReadUShort(20);
            set => WriteUShort((ushort) value, 20);
        }

        /// <summary>
        ///     Effect and Status flag.
        /// </summary>
        public ulong Flag1
        {
            get => ReadULong(26);
            set => WriteULong(value, 26);
        }

        /// <summary>
        ///     Effect and Status flag.
        /// </summary>
        public ulong Flag2
        {
            get => ReadULong(34);
            set => WriteULong(value, 34);
        }

        public uint Flag3
        {
            get => ReadUInt(42);
            set => WriteUInt(value, 42);
        }

        public byte CurrentLayout
        {
            get => ReadByte(46);
            set => WriteByte(value, 46);
        }

        /// <summary>
        ///     The helmet the character is wearing.
        /// </summary>
        public uint Helmet
        {
            get => ReadUInt(48);
            set => WriteUInt(value, 48);
        }

        /// <summary>
        ///     The garment the character is wearing.
        /// </summary>
        public uint Garment
        {
            get => ReadUInt(52);
            set => WriteUInt(value, 52);
        }

        /// <summary>
        ///     The armor the character is wearing.
        /// </summary>
        public uint Armor
        {
            get => ReadUInt(56);
            set => WriteUInt(value, 56);
        }

        public uint LeftHand
        {
            get => ReadUInt(60);
            set => WriteUInt(value, 60);
        }

        public uint RightHand
        {
            get => ReadUInt(64);
            set => WriteUInt(value, 64);
        }

        public uint RightAccessory
        {
            get => ReadUInt(72);
            set => WriteUInt(value, 72);
        }

        public uint LeftAccessory
        {
            get => ReadUInt(68);
            set => WriteUInt(value, 68);
        }

        public uint MountType
        {
            get => ReadUInt(76);
            set => WriteUInt(value, 76);
        }

        public uint MountArmor
        {
            get => ReadUInt(80);
            set => WriteUInt(value, 80);
        }

        public uint Life
        {
            get => ReadUInt(90);
            set => WriteUInt(value, 90);
        }

        public byte MonsterLevel
        {
            get => ReadByte(96);
            set => WriteByte(value, 96);
        }

        public ushort MapX
        {
            get => ReadUShort(98);
            set => WriteUShort(value, 98);
        } // 80  - The x-coordinate of the player's character on the map.

        public ushort MapY
        {
            get => ReadUShort(100);
            set => WriteUShort(value, 100);
        } // 82  - The y-coordinate of the player's character on the map.

        public ushort Hairstyle
        {
            get => ReadUShort(102);
            set => WriteUShort(value, 102);
        } // 78  - The hairstyle for the character.

        public FacingDirection Direction
        {
            get => (FacingDirection) ReadByte(104);
            set => WriteByte((byte) value, 104);
        } // 84  - The direction the character is facing in.

        public EntityAction Action
        {
            get => (EntityAction) ReadUShort(105);
            set => WriteUShort((byte) value, 105);
        } // 85  - The action the character is currently performing.

        public byte Metempsychosis
        {
            get => ReadByte(112);
            set => WriteByte(value, 112);
        }

        public byte Level
        {
            get => ReadByte(113);
            set => WriteByte(value, 113);
        }

        public bool WindowSpawn
        {
            get => ReadBoolean(115);
            set => WriteBoolean(value, 115);
        }

        public bool Away
        {
            get => ReadBoolean(116);
            set => WriteBoolean(value, 116);
        }

        public uint TutorBattlePower
        {
            get => ReadUInt(117);
            set => WriteUInt(value, 117);
        }

        public uint FateBattlePower
        {
            get => ReadUInt(121);
            set => WriteUInt(value, 121);
        }

        public uint FlowerRanking
        {
            get => ReadUInt(133);
            set => WriteUInt(value, 133);
        }

        public byte Nobility
        {
            get => ReadByte(137);
            set => WriteByte(value, 137);
        }

        public ushort ArmorColor
        {
            get => ReadUShort(141);
            set => WriteUShort(value, 141);
        }

        public ushort ShieldColor
        {
            get => ReadUShort(143);
            set => WriteUShort(value, 143);
        }

        public ushort HelmetColor
        {
            get => ReadUShort(145);
            set => WriteUShort(value, 145);
        }

        public uint QuizPoints
        {
            get => ReadUInt(147);
            set => WriteUInt(value, 147);
        }

        public byte MountPlus
        {
            get => ReadByte(151);
            set => WriteByte(value, 151);
        }

        public uint MountColor
        {
            get => ReadUInt(157);
            set => WriteUInt(value, 157);
        }

        /// <summary>
        ///     100 Points == 1 Enlighten.
        ///     Shows the icon above the entity head :) "Finger pls"
        /// </summary>
        public ushort EnlightenPoints
        {
            get => ReadUShort(162);
            set => WriteUShort(value, 162);
        }

        public uint VipLevel
        {
            get => ReadUInt(169);
            set => WriteUInt(value, 169);
        }

        public uint FamilyIdentity
        {
            get => ReadUInt(173);
            set => WriteUInt(value, 173);
        }

        public FamilyRank FamilyRank
        {
            get => (FamilyRank) ReadUInt(177);
            set => WriteUInt((uint) value, 177);
        }

        public uint FamilyBattlePower
        {
            get => ReadUInt(181);
            set => WriteUInt(value, 181);
        }

        public byte Title
        {
            get => ReadByte(185);
            set => WriteByte(value, 185);
        }

        public uint TotemBattlePower
        {
            get => ReadUInt(192);
            set => WriteUInt(value, 192);
        }

        public bool IsArenaWitness
        {
            get => ReadBoolean(196);
            set => WriteBoolean(value, 196);
        }

        public bool IsBoss
        {
            get => ReadBoolean(181);
            set => WriteBoolean(value, 181);
        }


        public uint HelmetArtifact
        {
            get => ReadUInt(200);
            set => WriteUInt(value, 200);
        }

        public uint ArmorArtifact
        {
            get => ReadUInt(204);
            set => WriteUInt(value, 204);
        }

        public uint LeftHandArtifact
        {
            get => ReadUInt(208);
            set => WriteUInt(value, 208);
        }

        public uint RightHandArtifact
        {
            get => ReadUInt(212);
            set => WriteUInt(value, 212);
        }

        public byte Subclass
        {
            get => ReadByte(224);
            set => WriteByte(value, 224);
        }

        /// <summary>
        ///     First life profession (0 rebirths)
        /// </summary>
        public ProfessionType FirstProfession
        {
            get => (ProfessionType) ReadUShort(225);
            set => WriteUShort((ushort) value, 225);
        }

        /// <summary>
        ///     Second life profession (1 Rebirth)
        /// </summary>
        public ProfessionType LastProfession
        {
            get => (ProfessionType) ReadUShort(227);
            set => WriteUShort((ushort) value, 227);
        }

        /// <summary>
        ///     Third life profession (2 rebirth)
        /// </summary>
        public ProfessionType Profession
        {
            get => (ProfessionType) ReadUShort(229);
            set => WriteUShort((ushort) value, 229);
        }

        public uint CountryFlag
        {
            get => ReadUInt(231);
            set => WriteUInt(value, 231);
        }

        public ushort TotalBattlePower
        {
            get => ReadUShort(237);
            set => WriteUShort(value, 237);
        }

        public byte SkillSoul
        {
            get => ReadByte(240);
            set => WriteByte(value, 241);
        }

        public byte TalentPoints
        {
            get => ReadByte(241);
            set => WriteByte(value, 241);
        }

        public bool KongFuActive
        {
            get => ReadBoolean(242);
            set => WriteBoolean(value, 242);
        }

        public byte SkillSoul2
        {
            get => ReadByte(243);
            set => WriteByte(value, 243);
        }

        public byte StringCount
        {
            get => ReadByte(244);
            set => WriteByte(value, 244);
        } // 212 - The amount of strings in the packet.

        /// <summary>
        ///     The character name
        /// </summary>
        public string Name
        {
            get => ReadString(ReadByte(245), 246);
            set => WriteStringWithLength(value, 245);
        } // 219 - The character's name and the length of the character's name.

        public string Spouse
        {
            get
            {
                int length = Name.Length;
                return ReadString(ReadByte(246 + length), 247 + length);
            }
            set => WriteStringWithLength(value, 246 + Name.Length);
        }

        public string FamilyName
        {
            get
            {
                int length = Name.Length + Spouse.Length;
                return ReadString(ReadByte(247 + length), 248 + length);
            }
            set => WriteStringWithLength(value, 247 + Name.Length + Spouse.Length);
        }

        public void SetNames(string name, string spouse, string family)
        {
            Resize(258 + name.Length + spouse.Length + family.Length);
            WriteHeader(Length - 8, PacketType.MsgPlayer);
            StringCount = 3;
            Name = name;
            Spouse = spouse;
            FamilyName = family;
        }
    }
}