﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2702 - MsgOwnKongfuImproveFeedback.cs
// Last Edit: 2019/12/15 03:15
// Created: 2019/12/11 22:57
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgOwnKongfuImproveFeedback : PacketStructure
    {
        public MsgOwnKongfuImproveFeedback()
            : base(PacketType.MsgOwnKongfuImproveFeedback, 27, 19)
        {
        }

        public MsgOwnKongfuImproveFeedback(byte[] buffer)
            : base(buffer)
        {
        }

        public uint FreeCourse
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public byte High
        {
            get => ReadByte(8);
            set => WriteByte(value, 8);
        }

        public KongFuImproveFeedbackMode Mode
        {
            get => (KongFuImproveFeedbackMode) ReadByte(9);
            set => WriteByte((byte) value, 9);
        }

        public byte Star
        {
            get => ReadByte(10);
            set => WriteByte(value, 10);
        }

        public byte Stage
        {
            get => ReadByte(11);
            set => WriteByte(value, 11);
        }

        public uint Attribute
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public byte FreeCourseUsedtoday
        {
            get => ReadByte(14);
            set => WriteByte(value, 14);
        }

        public uint PaidRounds
        {
            get => ReadUInt(15);
            set => WriteUInt(value, 15);
        }
    }

    public enum KongFuImproveFeedbackMode
    {
        FreeCourse,
        PaidCourse
    }
}