﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1015 - MsgName.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;

#endregion

namespace FtwCore.Networking.Packets
{
    public sealed class MsgName : PacketStructure
    {
        public MsgName()
            : base(PacketType.MsgName, 19, 11)
        {
        }

        public MsgName(byte[] packet)
            : base(packet)
        {
        }

        public uint Identity
        {
            get { return ReadUInt(8); }
            set { WriteUInt(value, 8); }
        }

        public ushort PosX
        {
            get { return ReadUShort(8); }
            set { WriteUShort(value, 8); }
        }

        public ushort PosY
        {
            get { return ReadUShort(10); }
            set { WriteUShort(value, 10); }
        }

        public StringAction Action
        {
            get { return (StringAction) ReadByte(12); }
            set { WriteByte((byte) value, 12); }
        }

        public byte TextAmount
        {
            get { return ReadByte(13); }
            set
            {
                int newSize = 15 + value + m_totalStringLength + 8;
                Resize(newSize);
                WriteHeader(newSize - 8, PacketType.MsgName);
                WriteByte(value, 13);
            }
        }

        private int m_totalStringLength = 0;

        public void Append(string text)
        {
            m_totalStringLength += text.Length;
            TextAmount += 1;
            var offset = (ushort) (14 + (TextAmount - 1) + (m_totalStringLength - text.Length));
            WriteStringWithLength(text, offset);
        }

        public List<string> Strings()
        {
            var strList = new List<string>();
            var offset = (ushort) 14;
            for (int i = 0; i < TextAmount; i++)
            {
                string message = ReadString(ReadByte(offset++), offset);
                strList.Add(message);
                offset += (ushort) message.Length;
            }

            return strList;
        }
    }

    public enum StringAction : byte
    {
        None = 0,
        Fireworks,
        CreateGuild,
        Guild,
        ChangeTitle,
        DeleteRole = 5,
        Mate,
        QueryNpc,
        Wanted,
        MapEffect,
        RoleEffect = 10,
        MemberList,
        KickoutGuildMember,
        QueryWanted,
        QueryPoliceWanted,
        PoliceWanted = 15,
        QueryMate,
        AddDicePlayer,
        DeleteDicePlayer,
        DiceBonus,
        PlayerWave = 20,
        SetAlly,
        SetEnemy,
        WhisperWindowInfo = 26
    }
}