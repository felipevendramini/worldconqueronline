﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2211 - MsgArenicWitness.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/17 14:07
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgArenicWitness : PacketStructure
    {
        public const byte
            RequestView = 0,
            QuitView = 1,
            Watchers = 2,
            Leave = 3,
            Fighters = 4;

        public MsgArenicWitness()
            : base(PacketType.MsgArenicWitness, 98, 90)
        {
        }

        public MsgArenicWitness(byte[] pBuffer)
            : base(pBuffer)
        {
        }

        public ushort Action
        {
            get => ReadUShort(4);
            set => WriteUShort(value, 4);
        }

        public uint Param
        {
            get => ReadUInt(6);
            set => WriteUInt(value, 6);
        }

        public uint UserIdentity
        {
            get => ReadUInt(10);
            set => WriteUInt(value, 10);
        }

        public uint WatcherCount
        {
            get => ReadUInt(14);
            set => WriteUInt(value, 14);
        }

        public uint Cheers1
        {
            get => ReadUInt(18);
            set => WriteUInt(value, 18);
        }

        public uint Cheers2
        {
            get => ReadUInt(22);
            set => WriteUInt(value, 22);
        }

        public void AppendName(string szName)
        {
            int nOffset = (int) (26 + WatcherCount++ * 32);
            WriteString(szName, 16, nOffset);
        }

        public void AppendName(string szName, uint dwLookface)
        {
            int nOffset = (int) (26 + WatcherCount++ * 32);
            WriteUInt(dwLookface, nOffset);
            WriteString(szName, 16, nOffset + 4);
        }

        public void AppendName(string szName, uint dwLookface, uint idRole, uint dwLevel, uint dwProf, uint dwRank)
        {
            int nOffset = (int) (26 + WatcherCount++ * 36);
            WriteUInt(dwLookface, nOffset);
            WriteString(szName, 16, nOffset + 4);
            WriteUInt(idRole, nOffset + 20);
            WriteUInt(dwLevel, nOffset + 24);
            WriteUInt(dwProf, nOffset + 28);
            WriteUInt(dwRank, nOffset + 32);
        }
    }
}