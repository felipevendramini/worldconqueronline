﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2046 - MsgTradeBuddy.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public enum TradePartnerType : byte
    {
        REQUEST_PARTNERSHIP = 0,
        REJECT_REQUEST = 1,
        BREAK_PARTNERSHIP = 4,
        ADD_PARTNER = 5
    }

    public class MsgTradeBuddy : PacketStructure
    {
        public MsgTradeBuddy()
            : base(PacketType.MsgTradeBuddy, 40, 32)
        {
        }

        public MsgTradeBuddy(byte[] pBuffer)
            : base(pBuffer)
        {
        }

        public uint Identity
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public TradePartnerType Type
        {
            get => (TradePartnerType) ReadByte(8);
            set => WriteByte((byte) value, 8);
        }

        public bool Online
        {
            get => ReadBoolean(9);
            set => WriteBoolean(value, 9);
        }

        public int HoursLeft
        {
            get => ReadInt(10);
            set => WriteInt(value * 60, 10);
        }

        public string Name
        {
            get => ReadString(16, 16);
            set => WriteString(value, 16, 16);
        }
    }
}