﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2205 - MsgQualifyingInteractive.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/17 14:01
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgQualifyingInteractive : PacketStructure
    {
        public MsgQualifyingInteractive()
            : base(PacketType.MsgQualifyingInteractive, 64, 56)
        {
        }

        public MsgQualifyingInteractive(byte[] pBuffer)
            : base(pBuffer)
        {
        }

        public ArenaType Type
        {
            get => (ArenaType) ReadUInt(4);
            set => WriteUInt((uint) value, 4);
        }

        public uint Option
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint Identity
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public string Name
        {
            get => ReadString(16, 16);
            set => WriteString(value, 16, 16);
        }

        public uint Rank
        {
            get => ReadUInt(32);
            set => WriteUInt(value, 32);
        }

        public uint Profession
        {
            get => ReadUInt(36);
            set => WriteUInt(value, 36);
        }

        public uint Unknown
        {
            get => ReadUInt(40);
            set => WriteUInt(value, 40);
        }

        public uint ArenaPoints
        {
            get => ReadUInt(44);
            set => WriteUInt(value, 44);
        }

        public uint Level
        {
            get => ReadUInt(48);
            set => WriteUInt(value, 48);
        }
    }

    public enum ArenaType
    {
        ARENA_ICON_ON = 0,
        ARENA_ICON_OFF = 1,
        START_COUNT_DOWN = 2,
        ACCEPT_DIALOG = 3,
        OPPONENT_GAVE_UP = 4,
        BUY_ARENA_POINTS = 5,
        MATCH = 6,
        YOU_ARE_KICKED = 7,
        START_THE_FIGHT = 8,
        DIALOG = 9,
        END_DIALOG = 10,
        END_MATCH_JOIN = 11
    }
}