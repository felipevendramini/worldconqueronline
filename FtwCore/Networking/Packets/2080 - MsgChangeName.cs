﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2080 - MsgChangeName.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/11/12 22:42
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public enum ChangeNameMode
    {
        /// <summary>
        ///     This contains the name after a normal request.
        /// </summary>
        CHANGE_NAME = 0,

        /// <summary>
        ///     If the name has been changed successfuly, reply with this mode.
        /// </summary>
        CHANGED_SUCCESSFULY = 1,

        /// <summary>
        ///     This is sent as reply when someone inputs a already existent name.
        /// </summary>
        NAME_IN_USE = 2,

        /// <summary>
        ///     This is the common screen asking for how many times the user has changed his name
        ///     and how many times he still can change it.
        /// </summary>
        REQUEST_INFO = 3,

        /// <summary>
        ///     This is sent when the user has an invalid username or he can change his name
        ///     for free. This should not be sent if the character name has invalid characters.
        ///     Because first of all, if he input invalid characters, you don't have to register
        ///     the name change. This will reply with the same Mode.
        /// </summary>
        CHANGE_NAME_ERROR = 4
    }

    public sealed class MsgChangeName : PacketStructure
    {
        public MsgChangeName()
            : base(PacketType.MsgChangeName, 34, 26)
        {
        }

        public MsgChangeName(byte[] packet)
            : base(packet)
        {
        }

        public MsgChangeName(ushort editedAmount, ushort canEditAmount)
            : base(PacketType.MsgChangeName, 34, 26)
        {
            Mode = ChangeNameMode.REQUEST_INFO;
            Param1 = editedAmount;
            Param2 = canEditAmount;
        }

        public ChangeNameMode Mode
        {
            get => (ChangeNameMode) ReadUShort(4);
            set => WriteUShort((ushort) value, 4);
        }

        public ushort Param1
        {
            get => ReadUShort(6);
            set => WriteUShort(value, 6);
        }

        public ushort Param2
        {
            get => ReadUShort(8);
            set => WriteUShort(value, 8);
        }

        public string Name
        {
            get => ReadString(16, 10);
            set => WriteString(value, 16, 10);
        }
    }
}