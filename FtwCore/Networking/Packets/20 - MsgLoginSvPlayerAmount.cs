﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 20 - MsgLoginSvPlayerAmount.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common.Enums;

#endregion

namespace FtwCore.Networking.Packets
{
    public sealed class MsgLoginSvPlayerAmount : PacketStructure
    {
        /// <summary>
        /// This constructor build the packet to ask for the online amount.
        /// </summary>
        public MsgLoginSvPlayerAmount()
            : base(PacketType.MsgLoginRequestOnlineNumber, 16, 16)
        {
            Type = LoginPlayerAmountRequest.RequestOnlineAmount;
        }

        public MsgLoginSvPlayerAmount(ushort usValue, LoginPlayerAmountRequest request)
            : base(PacketType.MsgLoginRequestOnlineNumber, 16, 8)
        {
            Amount = usValue;
            Type = request;
        }

        /// <summary>
        /// Gets a built packet and deserialize it for data reading.
        /// </summary>
        /// <param name="pMsg">The buffer that will be read.</param>
        public MsgLoginSvPlayerAmount(byte[] pMsg)
            : base(pMsg)
        {
        }

        public LoginPlayerAmountRequest Type
        {
            get => (LoginPlayerAmountRequest) ReadUShort(4);
            set => WriteUShort((ushort) value, 4);
        }

        public ushort Amount
        {
            get => ReadUShort(6);
            set => WriteUShort(value, 6);
        }
    }
}