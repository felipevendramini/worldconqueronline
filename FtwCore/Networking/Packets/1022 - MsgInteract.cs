﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1022 - MsgInteract.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;

#endregion

namespace FtwCore.Networking.Packets
{
    public sealed class MsgInteract : PacketStructure
    {
        public MsgInteract()
            : base(PacketType.MsgInteract, 52, 44)
        {
        }

        public MsgInteract(byte[] packet)
            : base(packet)
        {
        }

        public uint Timestamp
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public uint EntityIdentity
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public uint TargetIdentity
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public ushort CellX
        {
            get => ReadUShort(20);
            set => WriteUShort(value, 20);
        }

        public ushort CellY
        {
            get => ReadUShort(22);
            set => WriteUShort(value, 22);
        }

        public InteractionType Action
        {
            get => (InteractionType) ReadUInt(24);
            set => WriteUInt((uint) value, 24);
        }

        public uint Data
        {
            get => ReadUInt(28);
            set => WriteUInt(value, 28);
        }

        public ushort Damage
        {
            get => (ushort) Data;
            set => Data = (uint) ((KoCount << 16) | value);
        }

        public ushort KoCount
        {
            get => (ushort) (Data >> 16);
            set => Data = (uint) ((value << 16) | Damage);
        }

        public ushort MagicType
        {
            get => (ushort) Data;
            set => Data = (uint) ((MagicLevel << 16) | value);
        }

        public ushort MagicLevel
        {
            get => (ushort) (Data >> 16);
            set => Data = (uint) ((value << 16) | MagicType);
        }

        public uint Amount
        {
            get => ReadUInt(32);
            set => WriteUInt(value, 32);
        }

        public InteractionEffect ActivationType
        {
            get => (InteractionEffect) ReadByte(36);
            set => WriteByte((byte) value, 36);
        }

        public uint ActivationValue
        {
            get => ReadUInt(40);
            set => WriteUInt(value, 40);
        }

        public void AppendInteraction(InteractionEffect effect)
        {
            ActivationType |= effect;
        }
    }

    public enum InteractionType
    {
        ACT_ITR_NONE = 0,
        ACT_ITR_STEAL = 1,
        ACT_ITR_ATTACK = 2,
        ACT_ITR_HEAL = 3,
        ACT_ITR_POISON = 4,
        ACT_ITR_ASSASSINATE = 5,
        ACT_ITR_FREEZE = 6,
        ACT_ITR_UNFREEZE = 7,
        ACT_ITR_COURT = 8,
        ACT_ITR_MARRY = 9,
        ACT_ITR_DIVORCE = 10,
        ACT_ITR_PRESENT_MONEY = 11,
        ACT_ITR_PRESENT_ITEM = 12,
        ACT_ITR_SEND_FLOWERS = 13,
        ACT_ITR_KILL = 14,
        ACT_ITR_JOIN_GUILD = 15,
        ACT_ITR_ACCEPT_GUILD_MEMBER = 16,
        ACT_ITR_KICKOUT_GUILD_MEMBER = 17,
        ACT_ITR_PRESENT_POWER = 18,
        ACT_ITR_QUERY_INFO = 19,
        ACT_ITR_RUSH_ATTACK = 20,
        ACT_ITR_UNKNOWN21 = 21,
        ACT_ITR_ABORT_MAGIC = 22,
        ACT_ITR_REFLECT_WEAPON = 23,
        ACT_ITR_MAGIC_ATTACK = 24,
        ACT_ITR_UNKNOWN = 25,
        ACT_ITR_REFLECT_MAGIC = 26,
        ACT_ITR_DASH = 27,
        ACT_ITR_SHOOT = 28,
        ACT_ITR_QUARRY = 29,
        ACT_ITR_CHOP = 30,
        ACT_ITR_HUSTLE = 31,
        ACT_ITR_SOUL = 32,
        ACT_ITR_ACCEPT_MERCHANT = 33,
        ACT_ITR_INCREASE_JAR = 36,
        ACT_ITR_PRESENT_EMONEY = 39,
        ACT_ITR_COUNTER_KILL = 43,
        ACT_ITR_COUNTER_KILL_SWITCH = 44,
        ACT_ITR_FATAL_STRIKE = 45,
        ACT_ITR_INTERACT_REQUEST = 46,
        ACT_ITR_INTERACT_CONFIRM,
        ACT_ITR_INTERACT,
        ACT_ITR_INTERACT_UNKNOWN,
        ACT_ITR_INTERACT_STOP,
        ACT_ITR_AZURE_DMG = 55
    }

    [Flags]
    public enum InteractionEffect : ushort
    {
        None = 0x0,
        Block = 0x1, // 1
        Penetration = 0x2, // 2
        CriticalStrike = 0x4, // 4
        Breakthrough = 0x2, // 8
        MetalResist = 0x10, // 16
        WoodResist = 0x20, // 32
        WaterResist = 0x40, // 64
        FireResist = 0x80, // 128
        EarthResist = 0x100,
    }
}