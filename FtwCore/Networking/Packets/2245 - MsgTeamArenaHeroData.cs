﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2245 - MsgTeamArenaHeroData.cs
// Last Edit: 2020/01/17 17:19
// Created: 2020/01/17 17:16
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgTeamArenaHeroData : PacketStructure
    {
        public MsgTeamArenaHeroData(byte[] buffer)
            : base(buffer)
        {
        }

        public MsgTeamArenaHeroData()
            : base(PacketType.MsgTeamArenaHeroData, 56, 48)
        {
        }

        public uint TodayRank
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public uint Status
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint TotalWin
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public uint TotalLoss
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public uint TodayWin
        {
            get => ReadUInt(20);
            set => WriteUInt(value, 20);
        }

        public uint TodayLoss
        {
            get => ReadUInt(24);
            set => WriteUInt(value, 24);
        }

        public uint HistoryHonor
        {
            get => ReadUInt(28);
            set => WriteUInt(value, 28);
        }

        public uint CurrentHonor
        {
            get => ReadUInt(32);
            set => WriteUInt(value, 32);
        }

        public uint ArenaPoints
        {
            get => ReadUInt(36);
            set => WriteUInt(value, 36);
        }

        public uint Unknown44
        {
            get => ReadUInt(40);
            set => WriteUInt(value, 40);
        }

        public uint Unknown48
        {
            get => ReadUInt(44);
            set => WriteUInt(value, 44);
        }
    }
}