﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2700 - MsgOwnKongfuBase.cs
// Last Edit: 2019/12/14 19:38
// Created: 2019/12/11 22:57
// ////////////////////////////////////////////////////////////////////////////////////

using System.Collections.Generic;
using System.Linq;

namespace FtwCore.Networking.Packets
{
    public class MsgOwnKongfuBase : PacketStructure
    {
        private List<string> m_strings = new List<string>();

        public MsgOwnKongfuBase()
            : base(PacketType.MsgOwnKongfuBase, 14, 6)
        {
        }

        public MsgOwnKongfuBase(byte[] buffer)
            : base(buffer)
        {
        }

        public KongfuBaseMode Mode
        {
            get => (KongfuBaseMode) ReadByte(4);
            set => WriteByte((byte) value, 4);
        }

        public byte Count
        {
            get => ReadByte(5);
            set => WriteByte(value, 5);
        }

        public string Name
        {
            get => ReadString(ReadByte(6), 7);
            set
            {
                Mode = KongfuBaseMode.SetName;
                Count = 1;
                Resize(14 + value.Length + 1);
                WriteHeader(Length - 8, PacketType.MsgOwnKongfuBase);
                WriteStringWithLength(value, 6);
            }
        }

        public void Append(params string[] strs)
        {
            m_strings.AddRange(strs);
        }

        public List<string> GetStrings()
        {
            List<string> result = new List<string>();
            int offset = 6;
            for (int i = 0; i < Count; i++)
            {
                string str = ReadString(ReadByte(offset), ++offset);
                offset += str.Length;
                result.Add(str);
            }
            return result;
        }

        public void Finish()
        {
            if (m_strings.Count > 0)
            {
                Count = (byte)m_strings.Count;
                int length = 14 + m_strings.Sum(x => x.Length) + m_strings.Count;
                if (length > 1024)
                    return;
                Resize(length);
                WriteHeader(Length - 8, PacketType.MsgOwnKongfuBase);
                int offset = 6;
                foreach (var str in m_strings)
                {
                    WriteStringWithLength(str, offset);
                    offset += str.Length + 1;
                }
            }
        }
    }

    public enum KongfuBaseMode : byte
    {
        IconBar = 0,
        SetName = 1,
        UpdateTalent = 5,
        SendStatus = 7,
        QueryTargetInfo = 9,
        RestoreStar = 10,
        UpdateStar = 11,
        OpenStage = 12,
        UpdateTime = 13,
        SendInfo = 14
    }
}