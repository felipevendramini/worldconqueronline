﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2208 - MsgQualifyingSeasonRankInfo.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/17 14:09
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgQualifyingSeasonRankList : PacketStructure
    {
        public MsgQualifyingSeasonRankList()
            : base(PacketType.MsgQualifyingSeasonRankList, 16, 8)
        {
        }

        public MsgQualifyingSeasonRankList(byte[] pBuffer)
            : base(pBuffer)
        {
        }

        public uint Count
        {
            get => ReadUInt(4);
            set
            {
                Resize((int) (16 + 52 * value));
                WriteHeader(Length - 8, PacketType.MsgQualifyingSeasonRankList);
                WriteUInt(value, 4);
            }
        }

        public void AddPlayer(uint idRole, string szName, uint dwMesh, uint dwLevel, uint dwProf, uint dwScore,
            uint dwRank,
            uint dwWin, uint dwLose)
        {
            int offset = (int) (8 + Count * 52);
            Count += 1;

            WriteUInt(idRole, offset);
            WriteString(szName, 16, offset + 4);
            WriteUInt(dwMesh, offset + 20);
            WriteUInt(dwLevel, offset + 24);
            WriteUInt(dwProf, offset + 28);
            WriteUInt(0, offset + 32);
            WriteUInt(dwRank, offset + 36);
            WriteUInt(dwScore, offset + 40);
            WriteUInt(dwWin, offset + 44); // win last
            WriteUInt(dwLose, offset + 48); // lose last
        }
    }
}