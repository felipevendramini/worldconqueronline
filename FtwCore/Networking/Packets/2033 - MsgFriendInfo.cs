﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2033 - MsgFriendInfo.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common.Enums;

#endregion

namespace FtwCore.Networking.Packets
{
    public sealed class MsgFriendInfo : PacketStructure
    {
        public MsgFriendInfo()
            : base(PacketType.MsgFriendInfo, 52, 44)
        {
        }

        public uint Identity
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public uint Mesh
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public byte Level
        {
            get => ReadByte(12);
            set => WriteByte(value, 12);
        }

        public byte Profession
        {
            get => ReadByte(13);
            set => WriteByte(value, 13);
        }

        public ushort PkPoints
        {
            get => ReadUShort(14);
            set => WriteUShort(value, 14);
        }

        public ushort SyndicateIdentity
        {
            get => ReadUShort(16);
            set => WriteUShort(value, 16);
        }

        public SyndicateRank SyndicateRank
        {
            get => (SyndicateRank) ReadUShort(20);
            set => WriteUShort((ushort) value, 20);
        }

        public string Mate
        {
            get => ReadString(16, 26);
            set => WriteString(value, 16, 26);
        }

        public bool IsEnemy
        {
            get => ReadBoolean(42);
            set => WriteBoolean(value, 42);
        }
    }
}