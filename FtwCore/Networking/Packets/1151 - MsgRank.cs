﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1151 - MsgRank.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public enum RankType : byte
    {
        Flower,
        ChiDragon,
        ChiPhoenix,
        ChiTiger,
        ChiTurtle
    }

    public class MsgRank : PacketStructure
    {
        public const uint RED_ROSE_TYPE = 0x1c9c382,
            WHITE_ROSE_TYPE = 0x1c9c3e6,
            ORCHIDS_TYPE = 0x1c9c44a,
            TULIPS_TYPE = 0x1c9c4ae;

        public MsgRank(byte[] pMsg)
            : base(pMsg)
        {
        }

        public MsgRank()
            : base(PacketType.MsgRank, 88, 80)
        {
        }

        public uint Mode
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public uint FlowerIdentity
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public RankType Type
        {
            get => (RankType) ReadByte(8);
            set => WriteByte((byte) value, 8);
        }

        public byte Subtype
        {
            get => ReadByte(9);
            set => WriteByte(value, 9);
        }

        public ushort Unknown
        {
            get => ReadUShort(12);
            set => WriteUShort(value, 12);
        }

        public ushort PageNumber
        {
            get => ReadUShort(14);
            set => WriteUShort(value, 14);
        }

        public uint RedRoses
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public uint RedRosesToday
        {
            get => ReadUInt(20);
            set => WriteUInt(value, 20);
        }

        public uint WhiteRoses
        {
            get => ReadUInt(24);
            set => WriteUInt(value, 24);
        }

        public uint WhiteRosesToday
        {
            get => ReadUInt(28);
            set => WriteUInt(value, 28);
        }

        public uint Orchids
        {
            get => ReadUInt(32);
            set => WriteUInt(value, 32);
        }

        public uint OrchidsToday
        {
            get => ReadUInt(36);
            set => WriteUInt(value, 36);
        }

        public uint Tulips
        {
            get => ReadUInt(40);
            set => WriteUInt(value, 40);
        }

        public uint TulipsToday
        {
            get => ReadUInt(44);
            set => WriteUInt(value, 44);
        }

        public ushort ObjectCount
        {
            get => ReadUShort(16);
            set
            {
                Resize(78 + value * 56);
                WriteHeader(Length - 8, PacketType.MsgRank);
                WriteUShort(value, 16);
            }
        }

        public void SetFlowers(string flowers)
        {
            Resize(88 + flowers.Length);
            WriteHeader(Length - 8, PacketType.MsgFlower);
            WriteByte(1, 16);
            WriteString(flowers, flowers.Length, 18);
        }

        public void SetCharm(int type, string name, uint amount, uint idUser, uint idCharm)
        {
            Mode = 2;
            FlowerIdentity = idCharm;
            WriteByte(1, 16); // idk
            WriteByte(1, 24); // neither
            WriteByte(1, 32); // ??
            WriteUInt(idUser, 40);
            WriteUInt(idUser, 44);
            string szData = $"{type} {amount} {idUser} {idUser} {name} {name}";
            Resize(104 + szData.Length);
            WriteHeader(Length - 8, PacketType.MsgRank);
            //WriteString(szData, szData.Length, 48);
            WriteString(szData, szData.Length, 64);
        }

        // according to sources, start writing the ranking data after 24
        public void AddUserData(uint rank, uint unknown0, uint flowers, uint unknown1)
        {
            WriteUInt(rank, 24);
            WriteUInt(0, 28);
            WriteUInt(flowers, 32);
            WriteUInt(0, 36);
            WriteUInt(2301694, 40);
        }

        public void AddToRanking(uint rank, uint flowers, uint idUser, string szName)
        {
            try
            {
                int offset = 24 + (56 * ObjectCount);
                ObjectCount += 1;
                WriteUInt(rank, offset);
                WriteULong(flowers, offset + 8);
                WriteUInt(idUser, offset + 16);
                //WriteUInt(idUser, offset + 20);
                WriteString(szName, 16, offset + 24);
                //WriteString(szName, 16, offset + 40);
            }
            catch
            {
            }
        }
    }
}