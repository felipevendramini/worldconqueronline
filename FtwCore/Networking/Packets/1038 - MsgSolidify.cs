﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1038 - MsgSolidify.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;

#endregion

namespace FtwCore.Networking.Packets
{
    public enum SolidifyMode
    {
        Refinery = 0,
        Artifact = 1
    }

    public sealed class MsgSolidify : PacketStructure
    {
        public MsgSolidify()
            : base(PacketType.MsgSolidify, 28, 20)
        {
        }

        public MsgSolidify(byte[] packet)
            : base(packet)
        {
        }

        public SolidifyMode Mode
        {
            get => (SolidifyMode) ReadUInt(4);
            set => WriteUInt((uint) value, 4);
        }

        public uint TargetItem
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint StoneAmount
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public List<uint> StoneItems
        {
            get
            {
                var items = new List<uint>();
                for (int count = 16; count <= 12 + StoneAmount * 4; count += 4)
                {
                    items.Add(ReadUInt(count));
                }

                return items;
            }
        }
    }
}