﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2102 - MsgSynMemberList.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common.Enums;

#endregion

namespace FtwCore.Networking.Packets
{
    public sealed class MsgSynMemberList : PacketStructure
    {
        public MsgSynMemberList()
            : base(PacketType.MsgSynMemberList, 28, 20)
        {
        }

        public MsgSynMemberList(byte[] packet)
            : base(packet)
        {
        }

        public uint Subtype
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public uint StartIndex
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint Amount
        {
            get => ReadUInt(12);
            set
            {
                Resize((int) (28 + value * 60));
                WriteHeader(Length - 8, PacketType.MsgSynMemberList);
                WriteUInt(value, 12);
            }
        }

        public void Append(string name, uint dwLookface, uint nobility, ushort level, SyndicateRank rank,
            uint positionExpire, uint totalDonation,
            bool isOnline, ushort profession, ulong offlineTime)
        {
            Amount += 1;
            uint dwShowNobility = nobility;
            //if (dwLookface % 10 == 3 || dwLookface % 10 == 4)
            //    dwShowNobility += 1;
            //else
            //    dwShowNobility += 2;
            var offset = (int) (16 + (Amount - 1) * 60);
            WriteString(name, 16, offset);
            //WriteUInt(0, offset + 16); // lookface? mess the nobility if filled
            WriteUInt(dwShowNobility, offset + 16);
            WriteUInt(dwLookface % 10000 / 1000, offset + 20);
            WriteUInt(level, offset + 24);
            WriteUShort((ushort) rank, offset + 28);
            WriteUInt(positionExpire, offset + 32);
            WriteUInt(totalDonation, offset + 36);
            WriteBoolean(isOnline, offset + 40);
            WriteUInt(profession, offset + 48);
            WriteULong(offlineTime, offset + 52);
        }
    }
}