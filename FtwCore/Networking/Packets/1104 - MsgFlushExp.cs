﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1104 - MsgFlushExp.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgFlushExp : PacketStructure
    {
        public MsgFlushExp()
            : base(PacketType.MsgFlushExp, 20, 12)
        {
        }

        public MsgFlushExp(byte[] packet)
            : base(packet)
        {
        }

        public uint Experience
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public ushort Identity
        {
            get => ReadUShort(8);
            set => WriteUShort(value, 8);
        }

        public ushort ExperienceType
        {
            get => ReadUShort(10);
            set => WriteUShort(value, 10);
        }

        public const ushort MSGFLUSHEXP_WEAPONSKILL = 0,
            MSGFLUSHEXP_MAGIC = 1,
            MSGFLUSHEXP_SKILL = 2;
    }
}