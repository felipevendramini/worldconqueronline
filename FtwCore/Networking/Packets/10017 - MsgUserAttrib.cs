﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 10017 - MsgUserAttrib.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    /// <summary>
    /// Packet Type: 10017
    /// This packet updates the client interface.
    /// </summary>
    public sealed class MsgUserAttrib : PacketStructure
    {
        /// <summary>
        /// Create the basic packet with the minimum length.
        /// </summary>
        public MsgUserAttrib()
            : base(44)
        {
            WriteHeader(Length - 8, PacketType.MsgUserAttrib);
        }

        public uint Identity
        {
            get { return ReadUInt(8); }
            set { WriteUInt(value, 8); }
        }

        /// <summary>
        /// The number of updates that are being sent on this packet.
        /// </summary>
        public uint UpdateCount
        {
            get { return ReadUInt(12); }
            set
            {
                Resize((int) (36 + (value * 24) + 8));
                WriteHeader(Length - 8, PacketType.MsgUserAttrib);
                WriteUInt(value, 12);
            }
        }

        /// <summary>
        /// Append the request to the update packet.
        /// </summary>
        /// <param name="type">The action that will be updated on the client screen.</param>
        /// <param name="value">The new value.</param>
        public void Append(ClientUpdateType type, byte value)
        {
            var offset = (ushort) (16+(UpdateCount * 20));
            UpdateCount += 1;
            WriteUInt((byte) type, offset);
            WriteULong(value, offset + 4);
        }

        /// <summary>
        /// Append the request to the update packet.
        /// </summary>
        /// <param name="type">The action that will be updated on the client screen.</param>
        /// <param name="value">The new value.</param>
        public void Append(ClientUpdateType type, ushort value)
        {
            var offset = (ushort) (16+(UpdateCount * 20));
            UpdateCount += 1;
            WriteUInt((byte) type, offset);
            WriteULong(value, offset + 4);
        }

        /// <summary>
        /// Append the request to the update packet.
        /// </summary>
        /// <param name="type">The action that will be updated on the client screen.</param>
        /// <param name="value">The new value.</param>
        public void Append(ClientUpdateType type, uint value)
        {
            var offset = (ushort) (16+(UpdateCount * 20));
            UpdateCount += 1;
            WriteUInt((byte) type, offset);
            WriteULong(value, offset + 4);
        }

        /// <summary>
        /// Append the request to the update packet.
        /// </summary>
        /// <param name="type">The action that will be updated on the client screen.</param>
        /// <param name="value1">The new value.</param>
        /// <param name="value2"></param>
        public void Append(ClientUpdateType type, uint value1, uint value2)
        {
            var offset = (ushort) (16+(UpdateCount * 20));
            UpdateCount += 1;
            WriteUInt((byte) type, offset);
            WriteUInt(value1, offset + 4);
            WriteUInt(value2, offset + 8);
        }

        /// <summary>
        /// Append the request to the update packet.
        /// </summary>
        /// <param name="type">The action that will be updated on the client screen.</param>
        /// <param name="value1">The new value.</param>
        /// <param name="value2"></param>
        /// <param name="value3"></param>
        /// <param name="value4"></param>
        public void Append(ClientUpdateType type, uint value1, uint value2, uint value3, uint value4)
        {
            var offset = (ushort) (16+(UpdateCount * 20));
            UpdateCount += 1;
            WriteUInt((byte) type, offset);
            WriteUInt(value1, offset + 4);
            WriteUInt(value2, offset + 8);
            WriteUInt(value3, offset + 12);
            WriteUInt(value4, offset + 16);
        }

        /// <summary>
        /// Append the request to the update packet.
        /// </summary>
        /// <param name="type">The action that will be updated on the client screen.</param>
        /// <param name="value">The new value.</param>
        public void Append(ClientUpdateType type, ulong value)
        {
            var offset = (ushort) (16+(UpdateCount * 20));
            UpdateCount += 1;
            WriteUInt((byte) type, offset);
            WriteULong(value, offset + 4);
        }

        public void Append(ClientUpdateType type, ulong value1, ulong value2)
        {
            var offset = (ushort)(16+(UpdateCount * 20));
            UpdateCount += 1;
            WriteUInt((byte)type, offset);
            WriteULong(value1, offset + 4);
            WriteULong(value2, offset + 12);
        }

        /// <summary>
        /// Append the request to the update packet. Used for flags.
        /// </summary>
        public void Append(ClientUpdateType type, ulong value1, ulong value2, ulong value3)
        {
            var offset = (ushort) (16+(UpdateCount * 20));
            UpdateCount += 1;
            WriteUInt((byte) type, offset);
            WriteULong(value1, offset + 4);
            WriteULong(value2, offset + 12);
            WriteULong(value3, offset + 20);
        }

        /// <summary>
        /// Clear the packet.
        /// </summary>
        public void Clear()
        {
            ClearPacket(44, PacketType.MsgUserAttrib);
        }
    }

    /// <summary>
    /// The attributes to be updated on the client.
    /// </summary>
    public enum ClientUpdateType : byte
    {
        Hitpoints = 0,
        MaxHitpoints = 1,
        Mana = 2,
        MaxMana = 3,
        Money = 4,
        Experience = 5,
        PkPoints = 6,
        Class = 7,
        Stamina = 8,
        WhMoney = 9,
        Atributes = 10,
        Mesh = 11,
        Level = 12,
        Spirit = 13,
        Vitality = 14,
        Strength = 15,
        Agility = 16,
        HeavensBlessing = 17,
        DoubleExpTimer = 18,
        CursedTimer = 20,
        Reborn = 22,
        StatusFlag = 25,
        HairStyle = 26,
        XpCircle = 27,
        LuckyTimeTimer = 28,
        ConquerPoints = 29,
        OnlineTraining = 31,
        ExtraBattlePower = 36,
        Unknown1 = 37,
        Merchant = 38,
        VipLevel = 39,
        QuizPoints = 40,
        EnlightPoints = 41,
        HonorPoints = 42,
        Unknown0 = 43,
        GuildBattlepower = 44,
        BoundConquerPoints = 45,
        AzureShield = 49,
        SoulShackleTimer = 54
    }
}