﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2051 - MsgPigeonQuery.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;

#endregion

namespace FtwCore.Networking.Packets
{
    public sealed class MsgPigeonQuery : PacketStructure
    {
        public MsgPigeonQuery(byte[] msg)
            : base(msg)
        {
        }

        public MsgPigeonQuery()
            : base(PacketType.MsgPigeonQuery, 20, 12)
        {
        }

        public uint Param
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public ushort Total
        {
            get => ReadUShort(8);
            set => WriteUShort(value, 8);
        }

        public ushort Amount
        {
            get => ReadUShort(10);
            set
            {
                Resize(12 + value * 112 + 8);
                WriteHeader(Length - 8, PacketType.MsgPigeonQuery);
                WriteUShort(value, 10);
            }
        }

        public void AddBroadcast(uint dwBcId, uint dwPos, uint dwPlayer, string szName,
            uint dwSpentCps, string szMessage)
        {
            if (Amount >= 8)
            {
                Console.WriteLine("System tried to exceed the maximum broadcast queue packet size.");
                return;
            }

            int offset = 12 + Amount * 112;
            Amount += 1;
            WriteUInt(dwBcId, offset);
            WriteUInt(dwPos, offset + 4);
            WriteUInt(dwPlayer, offset + 8);
            WriteString(szName, 16, offset + 12);
            WriteUInt(dwSpentCps, offset + 28);
            WriteString(szMessage, 80, offset + 32);
        }
    }
}