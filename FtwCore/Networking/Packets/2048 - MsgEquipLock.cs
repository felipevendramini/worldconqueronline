﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2048 - MsgEquipLock.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public enum LockMode : byte
    {
        RequestLock = 0,
        RequestUnlock = 1,
        UnlockDate = 2,
        UnlockedItem = 3
    }

    public sealed class MsgEquipLock : PacketStructure
    {
        public MsgEquipLock()
            : base(PacketType.MsgEquipLock, 24, 16)
        {
        }

        public MsgEquipLock(byte[] packet)
            : base(packet)
        {
        }

        public uint Identity
        {
            get { return ReadUInt(4); }
            set { WriteUInt(value, 4); }
        }

        public LockMode Mode
        {
            get { return (LockMode) ReadByte(8); }
            set { WriteByte((byte) value, 8); }
        }

        public byte Unknown
        {
            get { return ReadByte(9); }
            set { WriteByte(value, 9); }
        }

        public uint Param
        {
            get { return ReadUInt(12); }
            set { WriteUInt(value, 12); }
        }
    }
}