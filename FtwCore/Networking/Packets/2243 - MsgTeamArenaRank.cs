﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2243 - MsgTeamArenaRank.cs
// Last Edit: 2020/01/17 16:47
// Created: 2020/01/17 16:43
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgTeamArenaRank : PacketStructure
    {
        public MsgTeamArenaRank(byte[] buffer)
            : base(buffer)
        {
        }

        public MsgTeamArenaRank()
            : base(PacketType.MsgTeamArenaRank, 24, 16)
        {
        }

        public uint PageNumber
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public uint Count
        {
            get => ReadUInt(8);
            set
            {
                Resize((int) (24 + 43 * value));
                WriteHeader(Length - 8, PacketType.MsgTeamArenaRank);
                WriteUInt(value, 8);
            }
        }

        public uint Type
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public void Add(int rank, uint dwPoints, uint dwProfession, uint dwLevel, byte bGender, string szName)
        {
            int offset = (int) (16 + Count * 43);
            Count += 1;
        }
    }
}