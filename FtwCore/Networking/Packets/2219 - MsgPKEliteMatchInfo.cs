﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2219 - MsgPKEliteMatchInfo.cs
// Last Edit: 2020/01/16 22:06
// Created: 2020/01/16 22:05
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgPKEliteMatchInfo : PacketStructure
    {
        private int m_nStartOffset = 24;

        public MsgPKEliteMatchInfo()
            : base(PacketType.MsgPkEliteMatchInfo, 132, 124)
        {
        }

        public MsgPKEliteMatchInfo(byte[] pBuffer)
            : base(pBuffer)
        {
        }

        public ElitePkMatchType Type
        {
            get => (ElitePkMatchType) ReadUShort(4);
            set => WriteUShort((ushort) value, 4);
        }

        public ushort Page
        {
            get => ReadUShort(6);
            set => WriteUShort(value, 6);
        }

        public ushort UnknownUs8
        {
            get => ReadUShort(8);
            set => WriteUShort(value, 8);
        }

        public ushort MatchCount
        {
            get => ReadUShort(10);
            set => WriteUShort(value, 10);
        }

        public ushort UnknownUs12
        {
            get => ReadUShort(12);
            set => WriteUShort(value, 12);
        }

        public ushort Group
        {
            get => ReadUShort(14);
            set => WriteUShort(value, 14);
        }

        public ElitePkGuiType InterfaceType
        {
            get => (ElitePkGuiType) ReadUShort(16);
            set => WriteUShort((ushort) value, 16);
        }

        public ushort TimeLeft
        {
            get => ReadUShort(18);
            set => WriteUShort(value, 18);
        }

        public ushort TotalWatchers
        {
            get => ReadUShort(20);
            set => WriteUShort(value, 20);
        }

        public bool OnGoing
        {
            get => ReadBoolean(20);
            set => WriteBoolean(value, 20);
        }

        public void AppendMatch(uint idMatch, ushort idxMatch, ElitePkRoleStatusFlag flag, UserMatchStatus[] pRoles)
        {
            // 100 length???
            int nOffset = 24 + MatchCount * 100;
            MatchCount += 1;
            Resize(40 + MatchCount * 100);
            WriteHeader(Length - 8, PacketType.MsgPkEliteMatchInfo);
            WriteUInt(idMatch, nOffset);
            WriteUInt((uint) pRoles.Length, nOffset + 4);
            WriteUShort(idxMatch, nOffset + 6);
            WriteUShort((ushort) flag, nOffset + 8);
            for (int i = 0; i < pRoles.Length; i++)
            {
                WriteUInt(pRoles[i].Identity, nOffset + 10 + i * 30);
                WriteUInt(pRoles[i].Lookface, nOffset + 14 + i * 30);
                WriteString(pRoles[i].Name, 16, nOffset + 18 + i * 30);
                WriteUInt((uint) pRoles[i].Flag, nOffset + 34 + i * 30);
                WriteBoolean(pRoles[i].Advance, nOffset + 38 + i * 30);
            }
        }
    }

    public class UserMatchStatus
    {
        public uint Identity;
        public string Name;
        public uint Lookface;
        public uint Wage;
        public uint Cheer;
        public uint Points;
        public ElitePkRoleStatusFlag Flag;
        public bool Advance;

        public void Reset()
        {
            Wage = 0;
            Cheer = 0;
            Points = 0;
            Advance = false;
        }
    }

    public enum UserTournamentStatus
    {
        NONE = 0,
        FIGHTING = 1,
        LOST = 2,
        QUALIFIED = 3,
        WAITING = 4,
        BYE = 5,
        INACTIVE = 7,
        WON_MATCH = 8
    }

    public enum ElitePkGuiType : ushort
    {
        GUI_TOP8_RANKING = 0,
        GUI_KNOCKOUT = 3,
        GUI_TOP8_QUALIFIER = 4,
        GUI_TOP4_QUALIFIER = 5,
        GUI_TOP2_QUALIFIER = 6,
        GUI_TOP3 = 7,
        GUI_TOP1 = 8,
        GUI_RECONSTRUCT_TOP = 9
    }

    public enum ElitePkMatchType : ushort
    {
        INITIAL_LIST = 0,
        STATIC_UPDATE = 1,
        GUI_EDIT = 2,
        UPDATE_LIST = 3,
        REQUEST_INFORMATION = 4,
        STOP_WAGERS = 5,
        EPK_STATE = 6
    }

    public enum ElitePkRoleStatusFlag : ushort
    {
        EPKTFLAG_NONE = 0,
        EPKTFLAG_FIGHTING = 1,
        EPKTFLAG_LOST = 2,
        EPKTFLAG_QUALIFIED = 3,
        EPKTFLAG_WAITING = 4,
        EPKTFLAG_BYE = 5,
        EPKTFLAG_INACTIVE = 7,
        EPKTFLAG_WON_MATCH = 8
    }
}