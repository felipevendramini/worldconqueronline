﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1006 - MsgUserInfo.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common.Enums;

#endregion

namespace FtwCore.Networking.Packets
{
    /// <summary>
    /// Packet Type: 1006. This packet is sent to the server after the player's character has been loaded from the 
    /// database. It is used to initialize the client with character data, updated by update packets then after. It
    /// is also sent to the map server to initialize the player's location and character information. This packet 
    /// sets the identity for the client and map server.
    /// </summary>
    public sealed class MsgUserInfo : PacketStructure
    {
        // private int _NAME_START_OFFSET = 112;
        // private int _NAME_START_OFFSET = 116; //5729
        private int _NAME_START_OFFSET = 122; // 5808

        /// <summary>
        /// Packet Type: 1006. This packet is sent to the server after the player's character has been loaded from the 
        /// database. It is used to initialize the client with character data, updated by update packets then after. It
        /// is also sent to the map server to initialize the player's location and character information. This packet 
        /// sets the identity for the client and map server.
        /// </summary>
        public MsgUserInfo(string szName, string szSecondName, string szMate)
            : base(150 + szName.Length + szSecondName.Length + szMate.Length)
        {
            // Fill the packet structure using the database object:
            WriteHeader(Length - 8, PacketType.MsgUserInfo);
            NameDisplayed = true;
            StringCount = 3;
            Name = szName;
            SecondName = "None";
            Mate = szMate;
            WriteByte(1, 109);
        }

        /// <summary>
        /// Packet Type: 1006. This packet is sent to the server after the player's character has been loaded from the 
        /// database. It is used to initialize the client with character data, updated by update packets then after. It
        /// is also sent to the map server to initialize the player's location and character information. This packet 
        /// sets the identity for the client and map server.
        /// </summary>
        /// <param name="array">The received packet from the message server.</param>
        public MsgUserInfo(byte[] array) : base(array)
        {
        }

        /// <summary>
        /// The character UID.
        /// </summary>
        public uint Identity
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        /// <summary>
        /// The Mesh of the character. (Lookface * 10000) + BodyType
        /// </summary>
        public uint Mesh
        {
            get => ReadUInt(14);
            set => WriteUInt(value, 14);
        }

        /// <summary>
        /// The hair style of the character.
        /// </summary>
        public ushort Hairstyle
        {
            get => ReadUShort(18);
            set => WriteUShort(value, 18);
        }

        /// <summary>
        /// The silver amount.
        /// </summary>
        public uint Silver
        {
            get => ReadUInt(20);
            set => WriteUInt(value, 20);
        }

        /// <summary>
        /// The e-money (CPs) amount that the character has.
        /// </summary>
        public uint ConquerPoints
        {
            get => ReadUInt(24);
            set => WriteUInt(value, 24);
        }

        /// <summary>
        /// The amount of experience the player has towards leveling up.
        /// </summary>
        public ulong Experience
        {
            get => ReadULong(28);
            set => WriteULong(value, 28);
        }

        /// <summary>
        /// The strength attribute points.
        /// </summary>
        public ushort Strength
        {
            get => ReadUShort(56);
            set => WriteUShort(value, 56);
        }

        /// <summary>
        /// The character's agility to dodge attacks and attack faster.
        /// </summary>
        public ushort Agility
        {
            get => ReadUShort(58);
            set => WriteUShort(value, 58);
        }

        /// <summary>
        /// The character resistance to damage (increases health limit).
        /// </summary>
        public ushort Vitality
        {
            get => ReadUShort(60);
            set => WriteUShort(value, 60);
        }

        /// <summary>
        /// The character's spirit energy (increases mana limit).
        /// </summary>
        public ushort Spirit
        {
            get => ReadUShort(62);
            set => WriteUShort(value, 62);
        }

        /// <summary>
        /// The unspent attribute points used towards increasing attributes.
        /// </summary>
        public ushort Attributes
        {
            get => ReadUShort(64);
            set => WriteUShort(value, 64);
        }

        /// <summary>
        /// The character's current health level.
        /// </summary>
        public ushort Health
        {
            get => ReadUShort(66);
            set => WriteUShort(value, 66);
        }

        /// <summary>
        /// The character's current mana level.
        /// </summary>
        public ushort Mana
        {
            get => ReadUShort(68);
            set => WriteUShort(value, 68);
        }

        public ushort PkPoints
        {
            get => ReadUShort(70);
            set => WriteUShort(value, 70);
        }

        public byte Level
        {
            get => ReadByte(74);
            set => WriteByte(value, 74);
        }

        /// <summary>
        /// The actual profession
        /// </summary>
        public byte Profession
        {
            get => ReadByte(75);
            set => WriteByte(value, 75);
        }

        /// <summary>
        /// The first life profession.
        /// </summary>
        public byte AncestorProfession
        {
            get => ReadByte(76);
            set => WriteByte(value, 76);
        }

        /// <summary>
        /// The second life profession.
        /// </summary>
        public byte PreviousProfession
        {
            get => ReadByte(77);
            set => WriteByte(value, 77);
        }

        /// <summary>
        /// The amount of reborns.
        /// </summary>
        public byte Metempsychosis
        {
            get => ReadByte(79);
            set => WriteByte(value, 79);
        }


        /// <summary>
        /// If the names will be displayed on login or not.
        /// </summary>
        public bool NameDisplayed
        {
            get => ReadBoolean(80);
            set => WriteBoolean(value, 80);
        }

        /// <summary>
        /// The amount of Quiz Points that the user has.
        /// </summary>
        public uint QuizPoints
        {
            get => ReadUInt(81);
            set => WriteUInt(value, 81);
        }

        /// <summary>
        /// The amount of Enlighten points. 100 equals to 1 point.
        /// </summary>
        public ushort Enlighten
        {
            get => ReadUShort(89);
            set => WriteUShort(value, 89);
        }

        /// <summary>
        /// 1 point equals to 20 minutes. Not sure yet if it's Uint or byte.
        /// </summary>
        public byte EnlightenExp
        {
            get => ReadByte(91);
            set => WriteByte(value, 91);
        }

        public byte VipLevel
        {
            get => ReadByte(97);
            set => WriteByte(value, 97);
        }

        /// <summary>
        /// The title that the player is holding. Example: Elite PK
        /// </summary>
        public byte PlayerTitle
        {
            get => ReadByte(101);
            set => WriteByte(value, 101);
        }

        /// <summary>
        /// The amount of bound CPs the user have.
        /// </summary>
        public uint BoundEmoney
        {
            get => ReadUInt(103);
            set => WriteUInt(value, 103);
        }

        public byte Subclass
        {
            get => ReadByte(107);
            set => WriteByte(value, 107);
        }

        public uint RidePoints
        {
            get => ReadUInt(116);
            set => WriteUInt(value, 116);
        }

        public PlayerCountry CountryFlag
        {
            get => (PlayerCountry) ReadUShort(120);
            set => WriteUShort((ushort) value, 120);
        }
        //private int _NAME_START_OFFSET = 122;

        /// <summary>
        /// The amount of strings that will be sent. Actually 3, Name, SecondName (Empty) and the Spouse name.
        /// </summary>
        public byte StringCount
        {
            get => ReadByte(_NAME_START_OFFSET);
            set => WriteByte(value, _NAME_START_OFFSET);
        }

        /// <summary>
        /// The character name.
        /// </summary>
        public string Name
        {
            get => ReadString(ReadByte(_NAME_START_OFFSET + 1), _NAME_START_OFFSET + 2);
            set => WriteStringWithLength(value, _NAME_START_OFFSET + 1);
        } // 111 - The character's name and the length of the character's name.

        /// <summary>
        /// This function has been removed around 525+, So just leave this empty.
        /// </summary>
        public string SecondName
        {
            get
            {
                int length = Name.Length;
                return ReadString(ReadByte(_NAME_START_OFFSET + 2 + length), _NAME_START_OFFSET + 3 + length);
            }
            set => WriteStringWithLength(value, _NAME_START_OFFSET + 2 + Name.Length);
        }

        /// <summary>
        /// The character spouse name.
        /// </summary>
        public string Mate
        {
            get
            {
                int length = Name.Length + SecondName.Length;
                return ReadString(ReadByte(_NAME_START_OFFSET + 3 + length), _NAME_START_OFFSET + 4 + length);
            }
            set => WriteStringWithLength(value, _NAME_START_OFFSET + 3 + Name.Length + SecondName.Length);
        }
    }
}