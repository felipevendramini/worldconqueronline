﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1023 - MsgTeam.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgTeam : PacketStructure
    {
        public MsgTeam()
            : base(PacketType.MsgTeam, 20, 12)
        {
        }

        public MsgTeam(byte[] packet)
            : base(packet)
        {
        }

        public uint Target
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public TeamActionType Type
        {
            get => (TeamActionType) ReadUInt(4);
            set => WriteUInt((uint) value, 4);
        }
    }

    public enum TeamActionType
    {
        CREATE = 0x00, // 0
        REQUEST_JOIN = 0x01, // 1
        LEAVE_TEAM = 0x02, // 2
        ACCEPT_INVITE = 0x03, // 3
        REQUEST_INVITE = 0x04, // 4
        ACCEPT_JOIN = 0x05, // 5
        DISMISS = 0x06, // 6
        KICK = 0x07, // 7
        JOIN_DISABLE = 0x08, // 8
        JOIN_ENABLE = 9,
        MONEY_ENABLE = 10,
        MONEY_DISABLE = 11,
        ITEM_ENABLE = 12,
        ITEM_DISABLE = 13,
        LEADER = 15, // 15
    }
}