﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1009 - MsgItem.cs
// Last Edit: 2019/12/07 23:17
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common.Enums;

#endregion

namespace FtwCore.Networking.Packets
{
    /// <summary>
    ///     Packet Type: 1009
    ///     This class handles the item usage packet.
    /// </summary>
    /// <summary>
    ///     Packet Type: 1009
    ///     This class handles the item usage packet.
    /// </summary>
    public sealed class MsgItem : PacketStructure
    {
        /// <summary>
        ///     This will deserialize the input packet into an array of bytes.
        /// </summary>
        /// <param name="arrayBytes">The 1009 packet to be deserialized.</param>
        public MsgItem(byte[] arrayBytes)
            : base(arrayBytes)
        {
        }

        /// <summary>
        ///     This will create a new instance of the packet.
        /// </summary>
        public MsgItem()
            : base(88 + 8)
        {
            WriteHeader(Length - 8, PacketType.MsgItem);
        }

        /// <summary>
        ///     The target of the action. May be the Shop ID that is being used.
        /// </summary>
        public uint Identity
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        /// <summary>
        ///     The item ID.
        /// </summary>
        public uint Param1
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        /// <summary>
        ///     The action that is being called.
        /// </summary>
        public ItemAction Action
        {
            get => (ItemAction) ReadUInt(16);
            set => WriteUInt((uint) value, 16);
        }

        /// <summary>
        ///     The time when you called the action usually.
        /// </summary>
        public uint Timestamp
        {
            get => ReadUInt(20);
            set => WriteUInt(value, 20);
        }

        /// <summary>
        ///     The amount of items you choose to buy on shops.
        /// </summary>
        public uint Param2
        {
            get => ReadUInt(24);
            set => WriteUInt(value, 24);
        }

        public uint Param3
        {
            get => ReadUInt(28);
            set => WriteUInt(value, 28);
        }

        public uint Param4
        {
            get => ReadUInt(32);
            set => WriteUInt(value, 32);
        }

        public uint Headgear
        {
            get => ReadUInt(36);
            set => WriteUInt(value, 36);
        }

        public uint Necklace
        {
            get => ReadUInt(40);
            set => WriteUInt(value, 40);
        }

        public uint Armor
        {
            get => ReadUInt(44);
            set => WriteUInt(value, 44);
        }

        public uint RightHand
        {
            get => ReadUInt(48);
            set => WriteUInt(value, 48);
        }

        public uint LeftHand
        {
            get => ReadUInt(52);
            set => WriteUInt(value, 52);
        }

        public uint Ring
        {
            get => ReadUInt(56);
            set => WriteUInt(value, 56);
        }

        public uint Talisman
        {
            get => ReadUInt(60);
            set => WriteUInt(value, 60);
        }

        public uint Boots
        {
            get => ReadUInt(64);
            set => WriteUInt(value, 64);
        }

        public uint Garment
        {
            get => ReadUInt(68);
            set => WriteUInt(value, 68);
        }

        public uint RightAccessory
        {
            get => ReadUInt(72);
            set => WriteUInt(value, 72);
        }

        public uint LeftAccessory
        {
            get => ReadUInt(76);
            set => WriteUInt(value, 76);
        }

        public uint MountArmor
        {
            get => ReadUInt(80);
            set => WriteUInt(value, 80);
        }

        public uint Crop
        {
            get => ReadUInt(84);
            set => WriteUInt(value, 84);
        }

        public void Append(ItemPosition pos, uint id)
        {
            switch (pos)
            {
                case ItemPosition.Headwear:
                    Headgear = id;
                    break;
                case ItemPosition.Necklace:
                    Necklace = id;
                    break;
                case ItemPosition.Ring:
                    Ring = id;
                    break;
                case ItemPosition.RightHand:
                    RightHand = id;
                    break;
                case ItemPosition.LeftHand:
                    LeftHand = id;
                    break;
                case ItemPosition.Armor:
                    Armor = id;
                    break;
                case ItemPosition.Boots:
                    Boots = id;
                    break;
                case ItemPosition.SteedArmor:
                    MountArmor = id;
                    break;
                case ItemPosition.Crop:
                    Crop = id;
                    break;
                case ItemPosition.Gourd:
                    Talisman = id;
                    break;
                case ItemPosition.RightHandAccessory:
                    RightAccessory = id;
                    break;
                case ItemPosition.LeftHandAccessory:
                    LeftAccessory = id;
                    break;
                case ItemPosition.Garment:
                    Garment = id;
                    break;
            }
        }
    }

    /// <summary>
    ///     The item actions.
    /// </summary>
    public enum ItemAction : uint
    {
        None = 0,
        Buy = 1,
        Sell = 2,
        Remove = 3,
        Use = 4,
        Equip = 5,
        Unequip = 6,
        QueryMoneySaved = 9,
        SaveMoney = 10,
        DrawMoney = 11,
        DropMoney = 12,
        SpendMoney = 13,
        Repair = 14,
        RepairAll = 15,
        Ident = 16,
        Durability = 17,
        DropEquipment = 18,
        Improve = 19,
        Uplev = 20,
        BoothQuery = 21,
        BoothAdd = 22,
        BoothDelete = 23,
        BoothBuy = 24,
        SynchroAmount = 25,
        Fireworks = 26,
        Ping = 27,
        Enchant = 28,
        BoothAddCp = 29,
        RedeemEquipment = 32,
        PkItemRedeem = 33,
        PkItemClose = 34,
        TalismanSocketProgress = 35,
        TalismanSocketProgressCps = 36,
        Drop = 37,
        GemCompose = 39,
        TortoiseCompose = 40,
        ActivateAccessory = 41,
        SocketEquipment = 43,
        AlternativeEquipment = 45,
        DisplayGears = 46,
        MergeItems = 48,
        SplitItems = 49,
        RequestItemTooltip = 52,
        DegradeEquipment = 54,
        ForgingBuy = 55
    }
}