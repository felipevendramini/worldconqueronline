﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1136 - MsgAchievement.cs
// Last Edit: 2020/01/14 16:32
// Created: 2020/01/14 16:25
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;

#endregion

namespace FtwCore.Networking.Packets
{
    public sealed class MsgAchievement : PacketStructure
    {
        public MsgAchievement()
            : base(PacketType.MsgAchievement, 24, 16)
        {
        }

        public MsgAchievement(byte[] buffer)
            : base(buffer)
        {
        }

        public AchievementRequest Action
        {
            get => (AchievementRequest) ReadUInt(4);
            set => WriteUInt((uint) value, 4);
        }

        public uint Identity
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public int Flag
        {
            get => ReadInt(12);
            set => WriteInt(value, 12);
        }

        public int Count
        {
            get => ReadInt(12);
            set
            {
                Resize(24 + 4 * value);
                WriteHeader(Length - 8, PacketType.MsgAchievement);
                WriteInt(value, 12);
            }
        }

        public void Append(int flag)
        {
            if (Count >= 250)
                return;

            int offset = 16 + Count * 4;
            Count += 1;
            WriteInt(flag, offset);
        }

        public List<int> GetFlags()
        {
            List<int> result = new List<int>();
            if (Count <= 0)
                return result;
            for (int i = 0; i < Count; i++)
            {
                result.Add(ReadInt(16 + i * 4));
            }

            return result;
        }
    }

    public enum AchievementRequest : uint
    {
        BigHash = 0,
        CheckView = 1,
        SendQueue = 2
    }
}