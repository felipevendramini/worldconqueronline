﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2078 - MsgUserIpInfo.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgUserIpInfo : PacketStructure
    {
        public MsgUserIpInfo()
            : base(272)
        {
            WriteHeader(Length - 8, PacketType.MsgUserIpInfo);
            WriteUInt(0x4e591dba, 4);
        }

        public uint LoginTime
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public LastLoginTypes Type
        {
            get => (LastLoginTypes) ReadByte(8);
            set => WriteByte((byte) value, 8);
        }

        public bool DifferentAddress
        {
            get => ReadBoolean(9);
            set => WriteBoolean(value, 9);
        }
    }

    public enum LastLoginTypes : byte
    {
        LastLogin,
        DifferentCity,
        DifferentPlace
    }
}