﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1059 - MsgEncryptCode.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    /// <summary>
    /// Packet Type: 1059. This packet is sent to initialize the password cipher in the client. The server sends
    /// the player's seed for Rivest Cipher 5 so the client can encrypt the player's inputted password. This packet
    /// is only used in patches 5174 and above.
    /// </summary>
    public sealed class MsgEncryptCode : PacketStructure
    {
        /// <summary>
        /// Packet Type: 1059. This packet is sent to initialize the password cipher in the client. The server sends
        /// the player's seed for Rivest Cipher 5 so the client can encrypt the player's inputted password. This packet
        /// is only used in patches 5174 and above.
        /// </summary>
        /// <param name="seed">The seed to be sent to the client and used in Rivest Cipher 5.</param>
        public MsgEncryptCode(int seed)
            : base(8)
        {
            WriteHeader(8, PacketType.MsgEncryptCode);
            WriteInt(seed, 4);
        }

        /// <summary> Offset 4 - The random seed being sent to the client to initialize password ciphers. </summary>
        public int Seed
        {
            get { return ReadInt(4); }
            set { WriteInt(value, 4); }
        }
    }
}