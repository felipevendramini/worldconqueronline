﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 28 - MsgLoginUserInfo.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/10 17:12
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgLoginUserInfo : PacketStructure
    {
        public MsgLoginUserInfo()
            : base(PacketType.MsgLoginUserInfo, 62)
        {
            
        }

        public MsgLoginUserInfo(byte[] buffer)
            : base(buffer)
        {
            
        }

        public LoginUserInfoMode Mode
        {
            get => (LoginUserInfoMode) ReadUShort(4);
            set => WriteUShort((ushort) value, 4);
        }

        public uint UserIdentity
        {
            get => ReadUInt(6);
            set => WriteUInt(value, 6);
        }

        public string UserName
        {
            get => ReadString(16, 10);
            set => WriteString(value, 16, 10);
        }

        public uint AccountIdentity
        {
            get => ReadUInt(26);
            set => WriteUInt(value, 26);
        }

        public string IpAddress
        {
            get => ReadString(16, 30);
            set => WriteString(value, 16, 30);
        }

        public string MacAddress
        {
            get => ReadString(16, 46);
            set => WriteString(value, 16, 46);
        }
    }

    public enum LoginUserInfoMode : ushort
    {
        Connect,
        Update,
        Disconnect
    }
}