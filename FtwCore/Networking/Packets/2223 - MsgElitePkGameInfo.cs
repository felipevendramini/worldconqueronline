﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2223 - MsgElitePkGameInfo.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/17 14:07
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgElitePKGameRankInfo : PacketStructure
    {
        public MsgElitePKGameRankInfo()
            : base(PacketType.MsgElitePkGameRankInfo, 56, 48)
        {
        }

        public MsgElitePKGameRankInfo(byte[] pBuffer)
            : base(pBuffer)
        {
        }

        public uint Type
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public uint Group
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint GroupStatus
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public uint Count
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public uint Identity
        {
            get => ReadUInt(20);
            set => WriteUInt(value, 20);
        }

        public void Append(int nRank, string szName, uint dwLookface, uint idRole)
        {
            int offset = (int) (32 + Count * 36);
            Count += 1;
            Resize((int) (56 + Count * 36));
            WriteHeader(Length - 8, PacketType.MsgElitePkGameRankInfo);
            WriteInt(nRank, offset);
            WriteString(szName, 16, offset + 4);
            WriteUInt(dwLookface, offset + 20);
            WriteUInt(idRole, offset + 24);
        }
    }
}