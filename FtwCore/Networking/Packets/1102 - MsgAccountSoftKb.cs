﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1102 - MsgAccountSoftKb.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public enum WarehouseMode : byte
    {
        WH_VIEW = 0,
        WH_ADDITEM,
        WH_REMITEM
    }

    public sealed class MsgAccountSoftKb : PacketStructure
    {
        /// <summary>
        /// This method will create the basic packet with no items attached. Use append method
        /// to add items to it.
        /// </summary>
        public MsgAccountSoftKb()
            : base(PacketType.MsgAccountSoftKb, 104, 96)
        {
        }

        public MsgAccountSoftKb(byte[] packet)
            : base(packet)
        {
        }

        /// <summary>
        /// The warehouse identity.
        /// </summary>
        public uint Identity
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public WarehouseMode Action
        {
            get => (WarehouseMode) ReadByte(8);
            set => WriteByte((byte) value, 8);
        }

        public byte Type
        {
            get => ReadByte(9);
            set => WriteByte(value, 9);
        }

        public byte MaximumAmount
        {
            get => ReadByte(12);
            set => WriteByte(value, 12);
        }

        public uint Identifier
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public byte ItemsCount
        {
            get => ReadByte(20);
            set
            {
                Resize(32 + value * 52);
                WriteHeader(Length - 8, PacketType.MsgAccountSoftKb);
                WriteByte(value, 20);
            }
        }

        public uint ItemIdentity
        {
            get => ReadUInt(24);
            set => WriteUInt(value, 24);
        }

        public uint Itemtype
        {
            get => ReadUInt(28);
            set => WriteUInt(value, 28);
        }

        public byte SocketOne
        {
            get => ReadByte(33);
            set => WriteByte(value, 33);
        }

        public byte SocketTwo
        {
            get => ReadByte(34);
            set => WriteByte(value, 34);
        }

        public byte Plus
        {
            get => ReadByte(41);
            set => WriteByte(value, 41);
        }

        public byte Bless
        {
            get => ReadByte(42);
            set => WriteByte(value, 42);
        }

        public bool Bound
        {
            get => ReadBoolean(43);
            set => WriteBoolean(value, 43);
        }

        public ushort Enchant
        {
            get => ReadUShort(44);
            set => WriteUShort(value, 44);
        }

        public ushort Effect
        {
            get => ReadUShort(36);
            set => WriteUShort(value, 36);
        }

        public bool Locked
        {
            get => ReadBoolean(50);
            set => WriteBoolean(value, 50);
        }

        public bool Suspicious
        {
            get => ReadBoolean(48);
            set => WriteBoolean(value, 48);
        }

        public byte Color
        {
            get => ReadByte(51);
            set => WriteByte(value, 51);
        }

        public uint SocketProgress
        {
            get => ReadUInt(52);
            set => WriteUInt(value, 52);
        }

        public uint AddLevelExp
        {
            get => ReadUInt(56);
            set => WriteUInt(value, 56);
        }

        public uint RemainingTime
        {
            get => ReadUInt(64);
            set => WriteUInt(value, 64);
        }

        public ushort StackAmount
        {
            get => ReadUShort(72);
            set => WriteUShort(value, 72);
        }
    }
}