﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - Packet Header.cs
// Last Edit: 2020/01/17 18:32
// Created: 2019/12/11 22:57
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    /// <summary>
    ///     This structure encapsulates a packet's header information. It contains the length of the packet in offset
    ///     zero as an unsigned short, and the identity of the packet in offset 2 as an unsigned short.
    /// </summary>
    public struct PacketHeader
    {
        public ushort Length;
        public PacketType Identity;
    }

    /// <summary>
    ///     This enumeration type defines the types of packets that can be sent and received by the client / server. It
    ///     also defines the types of packets sent between the message and map servers (which is normally just the
    ///     original packet type plus base 10000 instead of base 1000).
    /// </summary>
    public enum PacketType : ushort
    {
        MsgLoginAuthRequest = 11,
        MsgLoginAuthConfirm = 12,
        MsgLoginCompleteAuthentication = 13,
        MsgLoginRequestOnlineNumber = 20,
        MSG_LOGIN_REQUEST_USER_ONLINE = 21,
        MSG_LOGIN_REQUEST_KICKOUT = 22,
        MSG_LOGIN_REQUEST_DISCONNECTION = 23,
        MsgLoginRequestServerInfo = 24,

        // LOGIN_REQUEST_SERVER_STATE = 25,
        MsgLoginRequestUserSignin = 26,
        MsgLoginUserMacAddr = 27,
        MsgLoginUserInfo = 28,
        MSG_LOGIN_RECEIVE_INFORMATION = 50,

        // client and game server
        // Between 1000-29999
        MsgRegister = 1001,
        MsgTalk = 1004,
        MsgUserInfo = 1006,
        MsgItemInformation = 1008,
        MsgItem = 1009,
        MsgName = 1015,
        MsgWeather = 1016,
        MsgFriend = 1019,
        MsgInteract = 1022,
        MsgTeam = 1023,
        MsgAllot = 1024,
        MsgWeaponSkill = 1025,
        MsgTeamMember = 1026,
        MsgGemEmbed = 1027,
        MsgData = 1033,
        MSG_DETAIN_ITEM_INFO = 1034,
        MsgPing = 1037,
        MsgSolidify = 1038,
        MsgPlayerAttribInfo = 1040,
        MsgMailOperation = 1045,
        MsgMailList = 1046,
        MsgMailNotify = 1047,
        MsgMailContent = 1048,
        MsgConnect = 1052,
        MsgConnectEx = 1055,
        MsgTrade = 1056,
        MsgSynpOffer = 1058,
        MsgEncryptCode = 1059,
        MsgDutyMinContri = 1061,
        MsgSelfSynMemAwardRank = 1063,
        MsgMeteSpecial = 1066,
        MsgHangUp = 1070,
        MsgPcNum = 1100,
        MsgMapItem = 1101,
        MsgAccountSoftKb = 1102,
        MsgMagicInfo = 1103,
        MsgFlushExp = 1104,
        MsgMagicEffect = 1105,
        MsgSyndicateAttributeInfo = 1106,
        MsgSyndicate = 1107,
        MsgItemInfoEx = 1108,
        MsgNpcInfoEx = 1109,
        MsgMapInfo = 1110,
        MsgInviteTrans = 1126,
        MSG_VIP_USER_HANDLE = 1128,
        MsgVipFunctionValidNotify = 1129,
        MsgTitle = 1130,
        MsgTaskStatus = 1134,
        MSG_TASK_DETAIL_INFO = 1135,
        MsgAchievement = 1136,
        MsgFlower = 1150,
        MsgRank = 1151,
        MSG_LOGIN_CHALLENGE_S = 1213,
        MSG_LOGIN_PROOF_C = 1214,
        MsgFamily = 1312,
        MsgFamilyOccupy = 1313,
        MsgLottery = 1314,
        MSG_OPERATING_ACT = 1315,
        MSG_OPERATING_ACT_INFO = 1316,
        MSG_AUCTION = 1320,
        MSG_AUCTION_ITEM = 1321,
        MSG_AUCTION_QUERY = 1322,
        MSG_PROMOTION_ACT = 1323,
        MSG_PROMOTION_INFO = 1324,
        MsgAccountSrp6 = 1542,
        MsgNpcInfo = 2030,
        MsgNpc = 2031,
        MsgTaskDialog = 2032,
        MsgFriendInfo = 2033,
        MsgDataArray = 2036,
        MsgAuraGroup = 2045,
        MsgTradeBuddy = 2046,
        MsgTradeBuddyInfo = 2047,
        MsgEquipLock = 2048,
        MsgPigeon = 2050,
        MsgPigeonQuery = 2051,
        MsgPeerage = 2064,
        MSG_GUIDE = 2065,
        MSG_GUIDE_INFO = 2066,
        MSG_GUIDE_CONTRIBUTE = 2067,
        MsgQuiz = 2068,
        MsgRelation = 2071,
        MsgQuench = 2076,
        MsgItemStatus = 2077,
        MsgUserIpInfo = 2078,
        MsgServerInfo = 2079,
        MsgChangeName = 2080,
        MsgDeadMark = 2081,
        MsgFactionRankInfo = 2101,
        MsgSynMemberList = 2102,
        MsgSuperFlag = 2110,
        MsgTotemPoleInfo = 2201,
        MsgWeaponsInfo = 2202,
        MsgTotemPole = 2203,
        MsgQualifyingInteractive = 2205,
        MsgQualifyingFightersList = 2206,
        MsgQualifyingRank = 2207,
        MsgQualifyingSeasonRankList = 2208,
        MsgQualifyingDetailInfo = 2209,
        MsgArenicScore = 2210,
        MsgArenicWitness = 2211,
        MSG_ELITE_PK_ARENIC = 2218,
        MsgPkEliteMatchInfo = 2219,
        MsgPkStatistic = 2220,
        MSG_PK_ENABLE = 2221,
        MSG_ELITE_PK_SCORE = 2222,
        MsgElitePkGameRankInfo = 2223,
        MsgWarFlag = 2224,
        MsgSynRecuitAdvertising = 2225,
        MsgSynRecruitAdvertisingList = 2226,
        MsgSynRecruitAdvertisingOpt = 2227,
        MsgTeamArenaInteractive = 2241,
        MsgTeamArenaFightingTeamList = 2242,
        MsgTeamArenaRank = 2243,
        MsgTeamArenaYTop10List = 2244,
        MsgTeamArenaHeroData = 2245,
        MsgTeamArenaScore = 2246,
        MsgTeamArenaFightingMemberInfo = 2247,
        Msg2ndPsw = 2261,
        MsgSubPro = 2320,
        MsgAura = 2410,
        MsgNationatlity = 2430,
        MsgTrainingVitality = 2533,
        MsgTrainingVitalityInfo = 2534,
        MsgTrainingVitalityScore = 2535,
        MsgOwnKongfuBase = 2700,
        MsgOwnKongfuImproveSummaryInfo = 2701,
        MsgOwnKongfuImproveFeedback = 2702,
        MsgOwnKongRank = 2703,
        MsgOwnKongfuPkSetting = 2704,
        MsgWalk = 10005,
        MsgAction = 10010,
        MsgPlayer = 10014,
        MsgUserAttrib = 10017
    }
}