﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2061 - MsgDutyMinContri.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common.Enums;

#endregion

namespace FtwCore.Networking.Packets
{
    /// <summary>
    /// This packet is used to show the minimum donation a user needs to reach to claim a higher position on the guild.
    /// </summary>
    public sealed class MsgDutyMinContri : PacketStructure
    {
        public MsgDutyMinContri()
            : base(PacketType.MsgDutyMinContri, 24, 16)
        {
        }

        public MsgDutyMinContri(byte[] pBuffer)
            : base(pBuffer)
        {
        }

        public ushort Count
        {
            get => ReadUShort(6);
            set => WriteUShort(value, 6);
        }

        public void Append(SyndicateRank pos, uint dwDonation)
        {
            int offset = 8 + Count * 8;
            Count += 1;
            Resize(24 + Count * 8);
            WriteHeader(Length - 8, PacketType.MsgDutyMinContri);
            WriteUInt((uint) pos, offset);
            WriteUInt(dwDonation, offset + 4);
        }
    }
}