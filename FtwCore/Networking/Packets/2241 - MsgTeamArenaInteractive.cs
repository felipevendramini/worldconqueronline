﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2241 - MsgTeamArenaInteractive.cs
// Last Edit: 2020/01/17 16:23
// Created: 2020/01/17 16:02
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgTeamArenaInteractive : PacketStructure
    {
        public MsgTeamArenaInteractive(byte[] buffer)
            : base(buffer)
        {
        }

        public MsgTeamArenaInteractive()
            : base(PacketType.MsgTeamArenaInteractive, 60, 52)
        {
        }

        public ArenaType Type
        {
            get => (ArenaType) ReadUInt(4);
            set => WriteUInt((uint) value, 4);
        }

        public DialogButton Mode
        {
            get => (DialogButton) ReadUInt(8);
            set => WriteUInt((uint) value, 8);
        }

        public uint Identity
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public string Name
        {
            get => ReadString(16, 16);
            set => WriteString(value, 16, 16);
        }

        public uint TodayRanking
        {
            get => ReadUInt(32);
            set => WriteUInt(value, 32);
        }

        public uint Profession
        {
            get => ReadUInt(36);
            set => WriteUInt(value, 36);
        }

        public uint Unknown
        {
            get => ReadUInt(40);
            set => WriteUInt(value, 40);
        }

        public uint ArenaPoints
        {
            get => ReadUInt(44);
            set => WriteUInt(value, 44);
        }

        public uint Level
        {
            get => ReadUInt(48);
            set => WriteUInt(value, 48);
        }

        public enum DialogButton : uint
        {
            Lose = 3,
            Win = 1,
            DoGiveUp = 2,
            Accept = 1,
            MatchOff = 3,
            MatchOn = 5,
            SignUp = 0
        }
    }
}