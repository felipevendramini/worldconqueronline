﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2081 - MsgDeadMark.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgDeadMark : PacketStructure
    {
        public MsgDeadMark(bool remove, uint idTarget)
            : base(PacketType.MsgDeadMark, 20, 12)
        {
            Remove = remove;
            Identity = idTarget;
        }

        public bool Remove
        {
            get => ReadBoolean(4);
            set => WriteBoolean(value, 4);
        }

        public uint Identity
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }
    }
}