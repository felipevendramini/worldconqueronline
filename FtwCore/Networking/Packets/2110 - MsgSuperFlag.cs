﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2110 - MsgSuperFlag.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgSuperFlag : PacketStructure
    {
        public MsgSuperFlag()
            : base(PacketType.MsgSuperFlag, 40, 32)
        {
        }

        public MsgSuperFlag(byte[] pBuffer)
            : base(pBuffer)
        {
        }

        public uint Action
        {
            get { return ReadUInt(4); }
            set { WriteUInt(value, 4); }
        }

        public uint ItemIdentity
        {
            get { return ReadUInt(8); }
            set { WriteUInt(value, 8); }
        }

        public uint CarryIdentity
        {
            get { return ReadUInt(12); }
            set { WriteUInt(value, 12); }
        }

        public uint Durability
        {
            get { return ReadUInt(24); }
            set { WriteUInt(value, 24); }
        }

        public uint LocationCount
        {
            get { return ReadUInt(28); }
            set { WriteUInt(value, 28); }
        }

        public string Name
        {
            get { return ReadString(32, 48); }
            set { WriteString(value, 32, 48); }
        }

        public void AddLocation(uint idLoc, uint idMap, ushort mapX, ushort mapY, string szName)
        {
            int offset = (int) (32 + (LocationCount * 48));
            LocationCount += 1;
            Resize((int) (40 + LocationCount * 48));
            WriteHeader(Length - 8, PacketType.MsgSuperFlag);
            WriteUInt(idLoc, offset);
            WriteUInt(idMap, offset + 4);
            WriteUInt(mapX, offset + 8);
            WriteUInt(mapY, offset + 12);
            WriteString(szName, 32, offset + 16);
        }
    }
}