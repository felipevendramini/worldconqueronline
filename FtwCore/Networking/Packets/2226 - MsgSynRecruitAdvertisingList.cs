﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2226 - MsgSynRecruitAdvertisingList.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgSynRecruitAdvertisingList : PacketStructure
    {
        public MsgSynRecruitAdvertisingList()
            : base(PacketType.MsgSynRecruitAdvertisingList, 40, 32)
        {
        }

        public MsgSynRecruitAdvertisingList(byte[] pBuffer)
            : base(pBuffer)
        {
        }

        /// <summary>
        /// The starting index.
        /// </summary>
        public uint StartIndex
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        /// <summary>
        /// The amount of displayed records.
        /// </summary>
        public uint Count
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        /// <summary>
        /// The maximum amount of records.
        /// </summary>
        public uint MaxCount
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        /// <summary>
        /// Set 1 when sending the first 2 records.
        /// </summary>
        public uint FirstMatch
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public void Append(uint idSyn, string szDesc, string szSyn, string szLeader, uint dwLevel, uint dwMembers,
            ulong ulFunds, bool canJoin)
        {
            int offset = (int) (24 + Count * 344);
            Count += 1;

            Resize((int) (40 + Count * 344));
            WriteHeader(Length - 8, PacketType.MsgSynRecruitAdvertisingList);

            WriteUInt(idSyn, offset);
            WriteString(szDesc, 255, offset + 4);
            WriteString(szSyn, 36, offset + 259);
            WriteString(szLeader, 17, offset + 295);
            WriteUInt(dwLevel, offset + 312);
            WriteUInt(dwMembers, offset + 316);
            WriteULong(ulFunds, offset + 320);
            WriteUShort((ushort) (canJoin ? 1 : 0), offset + 328);
            WriteUInt(0, offset + 330);
        }
    }
}