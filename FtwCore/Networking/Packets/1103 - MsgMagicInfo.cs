﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1103 - MsgMagicInfo.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common;

#endregion

namespace FtwCore.Networking.Packets
{
    public sealed class MsgMagicInfo : PacketStructure
    {
        public MsgMagicInfo()
            : base(PacketType.MsgMagicInfo, 28, 20)
        {
        }

        public MsgMagicInfo(uint experience, ushort level, ushort type)
            : base(PacketType.MsgMagicInfo, 36, 28)
        {
            WriteInt(Time.Now.GetHashCode(), 4);
            Experience = experience;
            Level = level;
            Type = type;
        }

        // hate when i type pubic instead of public...
        public MsgMagicInfo(byte[] packet)
            : base(packet)
        {
            
        }

        public uint Experience
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public ushort Type
        {
            get => ReadUShort(12);
            set => WriteUShort(value, 12);
        }

        public ushort Level
        {
            get => ReadUShort(14);
            set => WriteUShort(value, 14);
        }

        public SkillAction Action
        {
            get => (SkillAction) ReadUShort(16);
            set => WriteUShort((ushort) value, 16);
        }
    }

    public enum SkillAction : ushort
    {
        /// <summary>
        /// adds and existing skill, Requires Id, Exp, Level, Index and Flags 
        /// </summary>
        AddExsisting = 0,
        /// <summary>
        /// adds a new skill, Requires Id, Exp, Level only.
        /// </summary>
        AddNew = 1,
        /// <summary>
        /// Sent to Server only.
        /// </summary>
        Unknown2 = 2,
        /// <summary>
        /// Set selected name index
        /// </summary>
        SetSelectedName = 3,
        /// <summary>
        /// Set flags and index
        /// </summary>
        SetFlags = 4,
        /// <summary>
        /// Sent to Server only.
        /// </summary>
        RemoveName = 5,
        /// <summary>
        /// replaces the flags and index
        /// </summary>
        ReplaceFlags = 6,
    }
}