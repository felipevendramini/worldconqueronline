﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2261 - Msg2ndPsw.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class Msg2ndPsw : PacketStructure
    {
        public Msg2ndPsw(byte[] receivedPacket)
            : base(receivedPacket)
        {
        }

        public Msg2ndPsw()
            : base(PacketType.Msg2ndPsw, 24, 16)
        {
        }

        public PasswordRequestType Request
        {
            get => (PasswordRequestType) ReadUInt(4);
            set => WriteUInt((uint) value, 4);
        }

        public uint Password
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint NewPassword
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }
    }

    public enum PasswordRequestType
    {
        Unknown,
        VerifyPassword,
        ForgotPassword,
        SetNewPassword,
        SendInformation,
        CorrectPassword,
        WrongPassword
    }
}