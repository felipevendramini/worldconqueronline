﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1129 - MsgVipFunctionValidNotify.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public class MsgVipFunctionValidNotify : PacketStructure
    {
        public MsgVipFunctionValidNotify(uint functions = 57255)
            : base(PacketType.MsgVipFunctionValidNotify, 16, 8)
        {
            VipFunctions = functions;
        }

        public MsgVipFunctionValidNotify(byte[] packet)
            : base(packet)
        {
        }

        /// <summary>
        /// Send 57255 for all.
        /// </summary>
        public uint VipFunctions
        {
            get { return ReadUInt(4); }
            set { WriteUInt(value, 4); }
        }
    }
}