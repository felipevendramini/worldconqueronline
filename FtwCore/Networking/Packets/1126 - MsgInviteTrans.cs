﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1126 - MsgInviteTrans.cs
// Last Edit: 2020/01/14 00:15
// Created: 2020/01/14 00:12
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    /// <summary>
    ///     This packet is used for invitations for events in the client with timers.
    /// </summary>
    public sealed class MsgInviteTrans : PacketStructure
    {
        public MsgInviteTrans()
            : base(PacketType.MsgInviteTrans, 32, 24)
        {
        }

        public MsgInviteTrans(byte[] buffer)
            : base(buffer)
        {
        }

        public uint Message
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public InviteTransMode Mode
        {
            get => (InviteTransMode) ReadUInt(12);
            set => WriteUInt((uint) value, 12);
        }

        public uint Seconds
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }
    }

    public enum InviteTransMode
    {
        Append = 6
    }
}