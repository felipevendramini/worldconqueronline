﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2047 - MsgTradeBuddyInfo.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common.Enums;

#endregion

namespace FtwCore.Networking.Packets
{
    public sealed class MsgTradeBuddyInfo : PacketStructure
    {
        public MsgTradeBuddyInfo()
            : base(PacketType.MsgTradeBuddyInfo, 42, 34)
        {
        }

        public uint Identity
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public uint Lookface
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public byte Level
        {
            get => ReadByte(12);
            set => WriteByte(value, 12);
        }

        public ProfessionType Profession
        {
            get => (ProfessionType) ReadByte(13);
            set => WriteByte((byte) value, 13);
        }

        public ushort PkPoints
        {
            get => ReadUShort(14);
            set => WriteUShort(value, 14);
        }

        public uint SyndicateIdentity
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public SyndicateRank SyndicateRank
        {
            get => (SyndicateRank) ReadUInt(20);
            set => WriteUInt((uint) value, 20);
        }

        public ushort Unknown
        {
            get => ReadUShort(24);
            set => WriteUShort(value, 24);
        }

        public string Name
        {
            get => ReadString(16, 26);
            set => WriteString(value, 16, 26);
        }
    }
}