﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1045 - MsgMailOperation.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgMailOperation : PacketStructure
    {
        public MsgMailOperation()
            : base(PacketType.MsgMailOperation, 20, 12)
        {
        }

        public MsgMailOperation(byte[] pBuffer)
            : base(pBuffer)
        {
        }

        public Type Mode
        {
            get => (Type) ReadUInt(4);
            set => WriteUInt((uint) value, 4);
        }

        public uint MailIdentity
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public enum Type : byte
        {
            NONE,
            OPEN,
            DELETE,
            REMOVE_MONEY,
            REMOVE_E_MONEY,
            REMOVE_ITEM,
            REMOVE_ATTACHMENT
        }
    }
}