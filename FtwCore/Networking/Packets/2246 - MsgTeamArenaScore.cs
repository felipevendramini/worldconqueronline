﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2246 - MsgTeamArenaScore.cs
// Last Edit: 2020/01/17 17:27
// Created: 2020/01/17 17:19
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgTeamArenaScore : PacketStructure
    {
        public MsgTeamArenaScore()
            : base(PacketType.MsgTeamArenaScore, 72, 64)
        {
        }

        public MsgTeamArenaScore(byte[] buffer)
            : base(buffer)
        {
        }

        public uint Identity1
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public uint Rank1
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public string Name1
        {
            get => ReadString(16, 12);
            set => WriteString(value, 16, 12);
        }

        public uint Damage1
        {
            get => ReadUInt(28);
            set => WriteUInt(value, 18);
        }

        public uint Identity2
        {
            get => ReadUInt(32);
            set => WriteUInt(value, 32);
        }

        public uint Rank2
        {
            get => ReadUInt(36);
            set => WriteUInt(value, 36);
        }

        public string Name2
        {
            get => ReadString(16, 40);
            set => WriteString(value, 16, 40);
        }

        public uint Damage2
        {
            get => ReadUInt(56);
            set => WriteUInt(value, 56);
        }

        public uint Unknown
        {
            get => ReadUInt(60);
            set => WriteUInt(value, 4);
        }
    }
}