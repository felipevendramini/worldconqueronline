﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 24 - MsgServerInformation.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgServerInformation : PacketStructure
    {
        public MsgServerInformation(byte[] pBuffer)
            : base(pBuffer)
        {
        }

        public MsgServerInformation(ushort usMaxOnline, ushort usOnline, string szServerName, int nPort)
            : base(PacketType.MsgLoginRequestServerInfo, 36, 36)
        {
            MaxOnlinePlayers = usMaxOnline;
            OnlinePlayers = usOnline;
            ServerName = szServerName;
            GamePort = nPort;
        }

        public ushort MaxOnlinePlayers
        {
            get => ReadUShort(4);
            set => WriteUShort(value, 4);
        }

        public ushort OnlinePlayers
        {
            get => ReadUShort(6);
            set => WriteUShort(value, 6);
        }

        public string ServerName
        {
            get => ReadString(9, 10);
            set => WriteStringWithLength(value, 9);
        }

        public int GamePort
        {
            get => ReadInt(26);
            set => WriteInt(value, 26);
        }
    }
}