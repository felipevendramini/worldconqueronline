﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1040 - MsgPlayerAttribInfo.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgPlayerAttribInfo : PacketStructure
    {
        public MsgPlayerAttribInfo()
            : base(PacketType.MsgPlayerAttribInfo, 144, 136)
        {
        }

        public MsgPlayerAttribInfo(byte[] packet)
            : base(packet)
        {
        }

        /// <summary>
        /// The user identity.
        /// </summary>
        public uint Identity
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint Life
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public uint Mana
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public uint MaxAttack
        {
            get => ReadUInt(20);
            set => WriteUInt(value, 20);
        }

        public uint MinAttack
        {
            get => ReadUInt(24);
            set => WriteUInt(value, 24);
        }

        public uint PhysicalDefense
        {
            get => ReadUInt(28);
            set => WriteUInt(value, 28);
        }

        public uint MagicalAttack
        {
            get => ReadUInt(32);
            set => WriteUInt(value, 32);
        }

        public uint MagicDefense
        {
            get => ReadUInt(36);
            set => WriteUInt(value, 36);
        }

        public uint Dodge
        {
            get => ReadUInt(40);
            set => WriteUInt(value, 40);
        }

        public uint Agility
        {
            get => ReadUInt(44);
            set => WriteUInt(value, 44);
        }

        public uint Accuracy
        {
            get => ReadUInt(48);
            set => WriteUInt(value, 48);
        }

        public uint DragonGemBonus
        {
            get => ReadUInt(52);
            set => WriteUInt(value, 52);
        }

        public uint PhoenixGemBonus
        {
            get => ReadUInt(56);
            set => WriteUInt(value, 56);
        }

        public uint MagicDefenseBonus
        {
            get => ReadUInt(60);
            set => WriteUInt(value, 60);
        }

        public uint TortoiseGemBonus
        {
            get => ReadUInt(64);
            set => WriteUInt(value, 64);
        }

        public uint Bless
        {
            get => ReadUInt(68);
            set => WriteUInt(value, 68);
        }

        public uint CriticalStrike
        {
            get => ReadUInt(72);
            set => WriteUInt(value, 72);
        }

        public uint SkillCriticalStrike
        {
            get => ReadUInt(76);
            set => WriteUInt(value, 76);
        }

        public uint Immunity
        {
            get => ReadUInt(80);
            set => WriteUInt(value, 80);
        }

        public uint Penetration
        {
            get => ReadUInt(84);
            set => WriteUInt(value, 84);
        }

        public uint Block
        {
            get => ReadUInt(88);
            set => WriteUInt(value, 88);
        }

        public uint Breakthrough
        {
            get => ReadUInt(92);
            set => WriteUInt(value, 92);
        }

        public uint Counteraction
        {
            get => ReadUInt(96);
            set => WriteUInt(value, 96);
        }

        public uint Detoxication
        {
            get => ReadUInt(100);
            set => WriteUInt(value, 100);
        }

        public uint FinalPhysicalDamage
        {
            get => ReadUInt(104);
            set => WriteUInt(value, 104);
        }

        public uint FinalMagicDamage
        {
            get => ReadUInt(108);
            set => WriteUInt(value, 108);
        }

        public uint FinalDefense
        {
            get => ReadUInt(112);
            set => WriteUInt(value, 112);
        }

        public uint FinalMagicDefense
        {
            get => ReadUInt(116);
            set => WriteUInt(value, 116);
        }

        public uint MetalDefense
        {
            get => ReadUInt(120);
            set => WriteUInt(value, 120);
        }

        public uint WoodDefense
        {
            get => ReadUInt(124);
            set => WriteUInt(value, 124);
        }

        public uint WaterDefense
        {
            get => ReadUInt(128);
            set => WriteUInt(value, 128);
        }

        public uint FireDefense
        {
            get => ReadUInt(132);
            set => WriteUInt(value, 132);
        }

        public uint EarthDefense
        {
            get => ReadUInt(136);
            set => WriteUInt(value, 136);
        }
    }
}