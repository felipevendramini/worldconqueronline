﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 13 - MsgLoginSvAuthentication.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common.Enums;

#endregion

namespace FtwCore.Networking.Packets
{
    /// <summary>
    /// This packet is sent by the LoginServer to tell the MsgServer after a successfull connection or not.
    /// </summary>
    public sealed class LoginSvResponsePacket : PacketStructure
    {
        /// <summary>
        /// This constructor build the ready to send packet.
        /// </summary>
        /// <param name="pMsg">The reply that will be sent to the server.</param>
        public LoginSvResponsePacket(LoginServerResponse pMsg)
            : base(PacketType.MsgLoginCompleteAuthentication, 24, 24)
        {
            Response = pMsg;
        }

        /// <summary>
        /// Gets a built packet and deserialize it for data reading.
        /// </summary>
        /// <param name="pMsg">The buffer that will be read.</param>
        public LoginSvResponsePacket(byte[] pMsg)
            : base(pMsg)
        {
        }

        /// <summary>
        /// The response to the message server, telling if it's connected or not.
        /// </summary>
        public LoginServerResponse Response
        {
            get => (LoginServerResponse) ReadByte(8);
            set => WriteByte((byte) value, 8);
        }
    }
}