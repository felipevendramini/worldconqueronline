﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 12 - MsgLoginSvAuthentication.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common;

#endregion

namespace FtwCore.Networking.Packets
{
    /// <summary>
    /// This packet is sent by the MsgServer after receiving the Welcome message confirming the connection. After that, this packet
    /// will send the server information to the Login Server. The Username and Password are required to authenticate the servers
    /// and are filled in the Configuration files also. If the server doesn't meet the requirements, then the LoginServer will
    /// reply with the packet 13.
    /// </summary>
    public sealed class MsgLoginSvAuthentication : PacketStructure
    {
        /// <summary>
        /// This constructor builds the packet ready to be sent.
        /// </summary>
        /// <param name="szName">The username that will authenticate the Msg Server with the Login Server.</param>
        /// <param name="szPass">The password that will authenticate the Msg Server with the Login Server.</param>
        /// <param name="szServerName">The server name, so the LoginServer will be enabled to redirect the player to the right game server.</param>
        /// <param name="usMaxOnline">The max amount of players enabled on this server.</param>
        public MsgLoginSvAuthentication(string szName, string szPass, string szServerName, ushort usMaxOnline)
            : base(PacketType.MsgLoginAuthConfirm, 512, 512)
        {
            for (int i = 4; i < Length; i++)
                WriteByte((byte) ThreadSafeRandom.RandGet(1, 254), i);

            Username = szName.Substring(0,
                szName.Length > _SERVER_NAME_MAX_LENGTH ? _USERNAME_MAX_LENGTH - 1 : szName.Length);
            Password = szPass.Substring(0,
                szPass.Length > _PASSWORD_MAX_LENGTH ? _PASSWORD_MAX_LENGTH - 1 : szPass.Length);
            ServerName = szServerName.Substring(0,
                szServerName.Length > _PASSWORD_MAX_LENGTH ? _PASSWORD_MAX_LENGTH - 1 : szServerName.Length);
            MaxOnlinePlayers = usMaxOnline;
        }

        /// <summary>
        /// Gets a built packet and deserialize it for data reading.
        /// </summary>
        /// <param name="pMsg">The buffer that will be read.</param>
        public MsgLoginSvAuthentication(byte[] pMsg)
            : base(pMsg)
        {
        }

        public string ServerName
        {
            get => ReadString(ReadByte(16), 17);
            set => WriteStringWithLength(value, 16);
        }

        public string Username
        {
            get => ReadString(ReadByte(112), 113);
            set => WriteStringWithLength(value, 112);
        }

        public string Password
        {
            get => ReadString(ReadByte(225), 226);
            set => WriteStringWithLength(value, 225);
        }

        public ushort MaxOnlinePlayers
        {
            get => ReadUShort(500);
            set => WriteUShort(value < _SERVER_MAX_ONLINE_PLAYERS ? value : _SERVER_MAX_ONLINE_PLAYERS, 500);
        }

        private const int _USERNAME_MAX_LENGTH = 16;
        private const int _PASSWORD_MAX_LENGTH = 16;
        private const int _SERVER_NAME_MAX_LENGTH = 16;
        private const ushort _SERVER_MAX_ONLINE_PLAYERS = 5000;
    }
}