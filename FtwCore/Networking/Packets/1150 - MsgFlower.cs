﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1150 - MsgFlower.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgFlower : PacketStructure
    {
        public MsgFlower()
            : base(PacketType.MsgFlower, 64, 56)
        {
        }

        public MsgFlower(byte[] packet)
            : base(packet)
        {
        }

        // I guess it's the mode... I will check out when really building this
        public uint Mode
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public uint Identity
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint ItemIdentity
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public uint Flower
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public string Sender
        {
            get => ReadString(16, 16);
            set => WriteString(value, 16, 16);
        }

        public uint RedRoses
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public uint Amount
        {
            get => ReadUInt(20);
            set => WriteUInt(value, 20);
        }

        public uint RedRosesToday
        {
            get => ReadUInt(20);
            set => WriteUInt(value, 20);
        }

        public FlowerType FlowerType
        {
            get => (FlowerType) ReadUInt(24);
            set => WriteUInt((uint) value, 24);
        }

        public uint WhiteRoses
        {
            get => ReadUInt(24);
            set => WriteUInt(value, 24);
        }

        public uint WhiteRosesToday
        {
            get => ReadUInt(28);
            set => WriteUInt(value, 28);
        }

        public string Receptor
        {
            get => ReadString(16, 32);
            set => WriteString(value, 16, 32);
        }

        public uint Orchids
        {
            get => ReadUInt(32);
            set => WriteUInt(value, 32);
        }

        public uint OrchidsToday
        {
            get => ReadUInt(36);
            set => WriteUInt(value, 36);
        }

        public uint Tulips
        {
            get => ReadUInt(40);
            set => WriteUInt(value, 40);
        }

        public uint TulipsToday
        {
            get => ReadUInt(44);
            set => WriteUInt(value, 44);
        }

        public uint SendAmount
        {
            get => ReadUInt(48);
            set => WriteUInt(value, 48);
        }

        public FlowerType SendFlowerType
        {
            get => (FlowerType) ReadUInt(52);
            set => WriteUInt((uint) value, 52);
        }

        public uint Unknown56
        {
            get => ReadUInt(56);
            set => WriteUInt(value, 56);
        }
    }

    public enum FlowerType
    {
        RED_ROSE,
        WHITE_ROSE,
        ORCHID,
        TULIP
    }
}