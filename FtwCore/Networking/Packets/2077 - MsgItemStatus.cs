﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2077 - MsgItemStatus.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public enum PurificationType : byte
    {
        Refinery = 0,
        AddRefinery = 2,
        PermanentRefinery = 3,
        RefineryExpired = 4,
        DragonSoul = 5,
        AddDragonSoul = 6,
        ArtifactExpired = 7,
        PermanentArtifact = 8,
        StabilizationEffectPurification = 9
    }

    public sealed class MsgItemStatus : PacketStructure
    {
        public MsgItemStatus()
            : base(PacketType.MsgItemStatus, 16, 8)
        {
        }

        public MsgItemStatus(byte[] packet)
            : base(packet)
        {
        }

        public uint Type
        {
            get { return ReadUInt(4); }
            set { WriteUInt(value, 4); }
        }

        public uint Count
        {
            get { return ReadUInt(4); }
            set
            {
                Resize((int) (8 + (value * 28) + 8));
                WriteHeader(Length - 8, PacketType.MsgItemStatus);
                WriteUInt(value, 4);
            }
        }

        public uint Identity
        {
            get { return ReadUInt(8); }
            set { WriteUInt(value, 8); }
        }

        public PurificationType Mode
        {
            get { return (PurificationType) ReadByte(12); }
            set { WriteByte((byte) value, 12); }
        }

        /// <summary>
        /// The itemtype of the refinery or dragon soul.
        /// </summary>
        public uint PurificationIdentity
        {
            get { return ReadUInt(16); }
            set { WriteUInt(value, 16); }
        }

        public uint Level
        {
            get { return ReadUInt(20); }
            set { WriteUInt(value, 20); }
        }

        public uint Percent
        {
            get { return ReadUInt(24); }
            set { WriteUInt(value, 24); }
        }

        public uint Time
        {
            get { return ReadUInt(28); }
            set { WriteUInt(value, 28); }
        }

        /// <summary>
        /// This method will append the dragon soul data into the packet to be sent to the client.
        /// </summary>
        /// <param name="target">The item unique identification.</param>
        /// <param name="type">The Dragon Soul type is 6.</param>
        /// <param name="purifyIdentity">The Dragon Soul itemtype.</param>
        /// <param name="purifyLevel">The Dragon Soul Phase.</param>
        /// <param name="purifyDuration">The remaining seconds of the dragon soul time.</param>
        /// <returns>If the packet has been succesfuly written.</returns>
        public bool Append(uint target, PurificationType type, uint purifyIdentity, uint purifyLevel,
            uint purifyDuration)
        {
            Count += 1;
            var offset = (ushort) (8 + (Count - 1) * 28);
            WriteUInt(target, offset);
            WriteUInt((uint) type, offset + 4);
            WriteUInt(purifyIdentity, offset + 8);
            WriteUInt(purifyLevel, offset + 12);
            WriteUInt(purifyDuration, offset + 20);
            WriteUInt(0, offset + 24);
            return true;
        }

        /// <summary>
        /// This method will append the refinery data into the packet to be sent to the client.
        /// </summary>
        /// <param name="target">The item unique identification.</param>
        /// <param name="type">The refinery type is 2.</param>
        /// <param name="purifyIdentity">The Refinery type id.</param>
        /// <param name="purifyLevel">The Refinery level.</param>
        /// <param name="purifyPercent">The percent amount that the refinery addicts.</param>
        /// <param name="purifyDuration">The remaining seconds of the refinery time.</param>
        /// <returns>If the packet has been succesfuly written.</returns>
        public bool Append(uint target, PurificationType type, uint purifyIdentity, uint purifyLevel,
            uint purifyPercent, uint purifyDuration)
        {
            Count += 1;
            var offset = (ushort) (8 + (Count - 1) * 28);
            WriteUInt(target, offset);
            WriteUInt((uint) type, offset + 4);
            WriteUInt(purifyIdentity, offset + 8);
            WriteUInt(purifyLevel, offset + 12);
            WriteUInt(purifyPercent, offset + 16);
            WriteUInt(purifyDuration, offset + 20);
            WriteUInt(0, offset + 24);
            return true;
        }
    }
}