﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2210 - MsgArenicScore.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/17 14:08
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgArenicScore : PacketStructure
    {
        public MsgArenicScore()
            : base(PacketType.MsgArenicScore, 64, 56)
        {
        }

        public uint EntityIdentity1
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public string Name1
        {
            get => ReadString(16, 8);
            set => WriteString(value, 16, 8);
        }

        public uint Damage1
        {
            get => ReadUInt(24);
            set => WriteUInt(value, 24);
        }

        public uint EntityIdentity2
        {
            get => ReadUInt(28);
            set => WriteUInt(value, 28);
        }

        public string Name2
        {
            get => ReadString(16, 32);
            set => WriteString(value, 16, 32);
        }

        public uint Damage2
        {
            get => ReadUInt(48);
            set => WriteUInt(value, 48);
        }
    }
}