﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1314 - MsgLottery.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common.Enums;

#endregion

namespace FtwCore.Networking.Packets
{
    public sealed class MsgLottery : PacketStructure
    {
        public MsgLottery(byte[] buffer)
            : base(buffer)
        {
        }

        public MsgLottery()
            : base(PacketType.MsgLottery, 32, 24)
        {
            
        }

        public LotteryRequest Request
        {
            get => (LotteryRequest) ReadUShort(4);
            set => WriteUShort((ushort) value, 4);
        }

        public SocketGem SocketOne
        {
            get => (SocketGem) ReadByte(7);
            set => WriteByte((byte) value, 7);
        }

        public SocketGem SocketTwo
        {
            get => (SocketGem) ReadByte(8);
            set => WriteByte((byte) value, 8);
        }

        public byte Addition
        {
            get => ReadByte(9);
            set => WriteByte(value, 9);
        }

        public ItemColor Color
        {
            get => (ItemColor) ReadByte(10);
            set => WriteByte((byte) value, 10);
        }

        public byte UsedChances
        {
            get => ReadByte(11);
            set => WriteByte(value, 11);
        }

        public byte Chances
        {
            get => ReadByte(12);
            set => WriteByte(value, 12);
        }

        public uint Itemtype
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }
    }

    public enum LotteryRequest : ushort
    {
        Accept = 0,
        AddJade = 1,
        Continue = 2,
        Show = 259
    }
}