﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2202 - MsgWeaponsInfo.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgWeaponsInfo : PacketStructure
    {
        public MsgWeaponsInfo()
            : base(PacketType.MsgWeaponsInfo, 92, 84)
        {
        }

        public MsgWeaponsInfo(byte[] packet)
            : base(packet)
        {
        }

        public uint Type
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public uint BeginAt
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint EndAt
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }

        public uint ArsenalType
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public uint TotalInscribed
        {
            get => ReadUInt(20);
            set => WriteUInt(value, 20);
        }

        public uint SharedBattlePower
        {
            get => ReadUInt(24);
            set => WriteUInt(value, 24);
        }

        public uint Enchantment
        {
            get => ReadUInt(28);
            set => WriteUInt(value, 28);
        }

        public uint EnchantmentExpire
        {
            get => ReadUInt(32);
            set => WriteUInt(value, 32);
        }

        public uint Donation
        {
            get => ReadUInt(36);
            set => WriteUInt(value, 36);
        }

        public uint Count
        {
            get => ReadUInt(40);
            set
            {
                Resize((int) (44 + 40 * value + 8));
                WriteHeader(Length - 8, PacketType.MsgWeaponsInfo);
                WriteUInt(value, 40);
            }
        }

        public void AppendItem(uint itemid, uint position, string name, uint itemtype, byte quality,
            byte plus, byte socketone, byte sockettwo, uint battlepower, uint donation)
        {
            Count += 1;
            int offset = (int) (44 + 40 * (Count - 1));
            WriteUInt(itemid, offset);
            WriteUInt(position + 1, offset + 4);
            WriteString(name, 16, offset + 8);
            WriteUInt(itemtype, offset + 24);
            WriteByte(quality, offset + 28);
            WriteByte(plus, offset + 29);
            WriteByte(socketone, offset + 30);
            WriteByte(sockettwo, offset + 31);
            WriteUInt(battlepower, offset + 32);
            WriteUInt(donation, offset + 36);
        }
    }
}