﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 1048 - MsgMailContent.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgMailContent : PacketStructure
    {
        public MsgMailContent(string szMessage)
            : base(PacketType.MsgMailContent, szMessage.Length + 20, szMessage.Length + 12)
        {
            Content = szMessage;
        }

        public MsgMailContent(byte[] pBuffer)
            : base(pBuffer)
        {
        }

        public uint MailIdentity
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public string Content
        {
            get => ReadString(Length - 12, 8);
            set
            {
                Resize(value.Length + 20);
                WriteHeader(Length - 8, PacketType.MsgMailContent);
                WriteString(value, value.Length, 8);
            }
        }
    }
}