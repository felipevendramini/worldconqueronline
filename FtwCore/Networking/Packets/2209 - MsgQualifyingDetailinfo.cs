﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2209 - MsgQualifyingDetailinfo.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/17 14:09
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgQualifyingDetailInfo : PacketStructure
    {
        public MsgQualifyingDetailInfo()
            : base(PacketType.MsgQualifyingDetailInfo, 60, 52)
        {
        }

        public MsgQualifyingDetailInfo(byte[] pBuffer)
            : base(pBuffer)
        {
        }

        public uint Ranking
        {
            get => ReadUInt(4);
            set => WriteUInt(value, 4);
        }

        public uint Unknown
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public ArenaWaitStatus Status
        {
            get => (ArenaWaitStatus) ReadUInt(12);
            set => WriteUInt((uint) value, 12);
        }

        public uint TotalWins
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        }

        public uint TotalLose
        {
            get => ReadUInt(20);
            set => WriteUInt(value, 20);
        }

        public uint TodayWins
        {
            get => ReadUInt(24);
            set => WriteUInt(value, 24);
        }

        public uint TodayLose
        {
            get => ReadUInt(28);
            set => WriteUInt(value, 28);
        }

        public uint TotalHonor
        {
            get => ReadUInt(32);
            set => WriteUInt(value, 32);
        }

        public uint CurrentHonor
        {
            get => ReadUInt(36);
            set => WriteUInt(value, 36);
        }

        public uint ArenaPoints
        {
            get => ReadUInt(40);
            set => WriteUInt(value, 40);
        }
    }

    public enum ArenaWaitStatus : uint
    {
        NOT_SIGNED_UP = 0,
        WAITING_FOR_OPPONENT = 1,
        WAITING_INACTIVE = 2
    }
}