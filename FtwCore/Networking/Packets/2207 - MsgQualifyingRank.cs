﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2207 - MsgQualifyingRank.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/17 14:10
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgQualifyingRank : PacketStructure
    {
        public MsgQualifyingRank()
            : base(PacketType.MsgQualifyingRank, 56, 48)
        {
        }

        public MsgQualifyingRank(byte[] pBuffer)
            : base(pBuffer)
        {
        }

        public ArenaRankType RankType
        {
            get => (ArenaRankType) ReadUShort(4);
            set => WriteUShort((ushort) value, 4);
        }

        public ushort PageNumber
        {
            get => ReadUShort(6);
            set => WriteUShort(value, 6);
        }

        public uint Count
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public uint Showing
        {
            get => ReadUInt(12);
            set
            {
                Resize((int) (56 + 36 * value));
                WriteHeader(Length - 8, PacketType.MsgQualifyingRank);
                WriteUInt(value, 12);
            }
        }

        public void AddPlayer(ushort usRank, string szName, ushort usType, uint dwPoints,
            uint dwProf, uint dwLevel, uint dwUnknown)
        {
            int offset = (int) (16 + Showing * 36);
            Showing += 1;

            WriteUShort(usRank, offset);
            WriteString(szName, 16, offset + 2);
            WriteUShort(usType, offset + 18);
            WriteUInt(dwPoints, offset + 20);
            WriteUInt(dwProf, offset + 24);
            WriteUInt(dwLevel, offset + 28);
        }
    }

    public enum ArenaRankType : ushort
    {
        QUALIFIER_RANK,
        HONOR_HISTORY
    }
}