﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2224 - MsgWarFlag.cs
// Last Edit: 2020/01/13 23:29
// Created: 2020/01/13 23:29
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgWarFlag : PacketStructure
    {
        private readonly int m_pStringStart = 20;

        public MsgWarFlag()
            : base(PacketType.MsgWarFlag, 64, 56)
        {
        }

        public MsgWarFlag(byte[] pBuffer)
            : base(pBuffer)
        {
        }

        /// <summary>
        ///     Gotta figure out
        /// </summary>
        public WarFlagType Type
        {
            get => (WarFlagType) ReadUInt(4);
            set => WriteUInt((uint) value, 4);
        }

        public bool IsWar
        {
            get => ReadBoolean(8);
            set => WriteBoolean(value, 8);
        }

        public uint Identity
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public int Count
        {
            get => ReadInt(28);
            set
            {
                Resize(62 + 24 * value);
                WriteHeader(Length - 8, PacketType.MsgWarFlag);
                WriteInt(value, 28);
            }
        }

        public bool Active
        {
            get => ReadBoolean(28);
            set => WriteBoolean(value, 28);
        }

        public ushort MapX
        {
            get => ReadUShort(36);
            set => WriteUShort(value, 36);
        }

        public ushort MapY
        {
            get => ReadUShort(38);
            set => WriteUShort(value, 38);
        }

        public void Append(uint param1, uint param2, string szName)
        {
            int offset = 36 + Count * 24;
            Count += 1;
            WriteUInt(param1, offset + 20);
            WriteUInt(param2, offset);
            WriteString(szName, 16, offset + 4);
        }
    }

    public enum WarFlagType
    {
        Initialize = 0,
        WarFlagBaseRank = 1,
        WarFlagTop4 = 2,
        WarBaseDominate = 5,
        GrabFlagEffect = 6,
        DeliverFlagEffect = 7,
        GenerateTimer = 8,
        SyndicateRewardTab = 11,
        Location2XBonus = 12,
    }
}