﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2032 - MsgTaskDialog.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgTaskDialog : PacketStructure
    {
        /// <summary>
        /// The types of NPC Dialog.
        /// </summary>
        public const byte
            CLIENT_REQUEST = 0,
            DIALOG = 1,
            OPTION = 2,
            INPUT = 3,
            AVATAR = 4,
            LAY_NPC = 5,
            MESSAGE_BOX = 6,
            FINISH = 100,
            ANSWER = 101,
            TEXT_INPUT = 102,
            UPDATE_WINDOW = 112;

        /// <summary>
        /// This method will create a empty reply packet with default length.
        /// </summary>
        public MsgTaskDialog()
            : base(PacketType.MsgTaskDialog, 28, 20)
        {
        }


        public MsgTaskDialog(byte interactType, string text)
            : base(PacketType.MsgTaskDialog, 33 + text.Length, 25 + text.Length)
        {
            InteractType = interactType;
            OptionId = 255;
            DontDisplay = true;
            Text = text;
        }

        /// <summary>
        /// This method will deserialize a packet.
        /// </summary>
        /// <param name="packet">The Npc Reply packet to be deserialized.</param>
        public MsgTaskDialog(byte[] packet)
            : base(packet)
        {
        }

        public uint TaskId
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        public ushort InputMaxLength
        {
            get => ReadUShort(12);
            set => WriteUShort(value, 12);
        }

        public byte OptionId
        {
            get => ReadByte(14);
            set => WriteByte(value, 14);
        }

        public byte InteractType
        {
            get => ReadByte(15);
            set => WriteByte(value, 15);
        }

        public bool DontDisplay
        {
            get => ReadBoolean(16);
            set => WriteBoolean(value, 16);
        }

        public string Text
        {
            get => ReadString(ReadByte(17), 18);
            set
            {
                Resize(33 + value.Length);
                WriteStringWithLength(value, 17);
            }
        }
    }
}