﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 10010 - MsgAction.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common;
using FtwCore.Common.Enums;

#endregion

namespace FtwCore.Networking.Packets
{
    /// <summary>
    /// Packet Type: 1010. This packet encapsulates a general action to be performed by the server or by the 
    /// client. The action is performed by an actor of the screen or sends a general action to an observer.
    /// It can also be used to control actions in the client interface.
    /// </summary>
    public sealed class MsgAction : PacketStructure
    {
        /// <summary>
        /// Packet Type: 1010. This packet encapsulates a general action to be performed by the server or by the 
        /// client. The action is performed by an actor of the screen or sends a general action to an observer.
        /// It can also be used to control actions in the client interface.
        /// </summary>
        /// <param name="packet">The received packet from the client.</param>
        public MsgAction(byte[] packet)
            : base(packet)
        {
        }

        /// <summary>
        /// Packet Type: 1010. This packet encapsulates a general action to be performed by the server or by the 
        /// client. The action is performed by an actor of the screen or sends a general action to an observer.
        /// It can also be used to control actions in the client interface.
        /// </summary>
        /// <param name="identity">The identity of the actor.</param>
        /// <param name="left">The left data being sent through the packet.</param>
        /// <param name="right">The right data being sent through the packet.</param>
        /// <param name="action">The type of action being performed.</param>
        public MsgAction(uint identity, ushort left, ushort right, GeneralActionType action)
            : base(PacketType.MsgAction, 50, 42)
        {
            TimeStamp = Time.Now;
            Identity = identity;
            LeftData = left;
            RightData = right;
            Action = action;
        }

        /// <summary>
        /// Packet Type: 1010. This packet encapsulates a general action to be performed by the server or by the 
        /// client. The action is performed by an actor of the screen or sends a general action to an observer.
        /// It can also be used to control actions in the client interface.
        /// </summary>
        /// <param name="identity">The identity of the actor.</param>=
        /// <param name="data">The data being sent through the action.</param>
        /// <param name="x">The x-coordinate of the character.</param>
        /// <param name="y">The y-coordinate of the character.</param>
        /// <param name="action">The type of action being performed.</param>
        public MsgAction(uint identity, uint data, ushort x, ushort y, GeneralActionType action)
            : base(PacketType.MsgAction, 50, 42)
        {
            TimeStamp = Time.Now;
            Identity = identity;
            Data = data;
            X = x;
            Y = y;
            Action = action;
        }

        // Packet Structure Properties:
        public uint Identity
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        } // 4  - The identity of the actor.

        public uint Data
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        } // 8  - The data being sent through the action.

        public ushort LeftData
        {
            get => ReadUShort(12);
            set => WriteUShort(value, 12);
        } // 8  - Left data inside the data parameter.

        public ushort RightData
        {
            get => ReadUShort(14);
            set => WriteUShort(value, 14);
        } // 10 - Right data inside the data parameter.

        public uint Details
        {
            get => ReadUInt(16);
            set => WriteUInt(value, 16);
        } // 12 - The details for the action, used in defining sub-actions.

        public Time TimeStamp
        {
            get => ReadUInt(20);
            set => WriteUInt(value, 20);
        } // 16 - The time stamp from the client for processing.

        public GeneralActionType Action
        {
            get => (GeneralActionType) ReadUInt(24);
            set => WriteUInt((uint) value, 24);
        } // 20 - The type of action being performed by the actor.

        public FacingDirection Direction
        {
            get => (FacingDirection) ReadUShort(26);
            set => WriteUShort((ushort) value, 26);
        } // 22 - The direction the player is facing in.

        public ushort X
        {
            get => ReadUShort(28);
            set => WriteUShort(value, 28);
        } // 24 - The x-coordinate of the character.

        public ushort Y
        {
            get => ReadUShort(30);
            set => WriteUShort(value, 30);
        } // 26 - The y-coordinate of the character.

        public uint Map
        {
            get => ReadUInt(32);
            set => WriteUInt(value, 32);
        } // 28 - The map of the action.

        public uint Hue
        {
            get => ReadUInt(36);
            set => WriteUInt(value, 36);
        } // 32 - The RGB code for the color of the action.

        public bool MountedFlag
        {
            get => ReadBoolean(40);
            set => WriteBoolean(value, 40);
        }

        public bool ResponseFlag
        {
            get => ReadBoolean(41);
            set => WriteBoolean(value, 41);
        }
    }

    /// <summary> This enumeration type defines the types of general actions that can be performed. </summary>
    public enum GeneralActionType : ushort
    {
        SetLocation = 74,
        Hotkeys = 75,
        ConfirmFriends = 76,
        ConfirmProficiencies = 77,
        ConfirmSpells = 78,
        ChangeDirection = 79,
        ChangeAction = 81,
        UsePortal = 85,
        ChangeMap = 86,
        Leveled = 92,
        XpClear = 93,
        Revive = 94,
        DelRole = 95,
        ChangePkMode = 96,
        ConfirmGuild = 97,
        Mine = 99,
        TeamMemberPos = 101,
        RequestEntitySpawn = 102,
        AbortMagic = 103,
        MapArgb = 104,
        MapStatus = 105,
        QueryTeamMember = 106,
        NewCoordinates = 108,
        DropMagic = 109,
        DropSkill = 110,
        CreateBooth = 111,
        SuspendBooth = 112,
        ResumeBooth = 113,
        GetSurroundings = 114,
        OpenCustom = 116,
        ObserveEquipment = 117,
        AbortTransform = 118,
        EndFly = 120,
        GetMoney = 121,
        ViewEnemyInfo = 123,
        OpenWindow = 126,
        GuardJump = 130,
        LeaveMap = 132,

        /// <summary>
        /// Data1 = EntityId,
        /// Data3Low = PositionX,
        /// Data3High = PositionY
        /// </summary>
        SpawnEffect = 134,
        RemoveEntity = 135,
        Jump = 137,
        TeleportReply = 138,
        DieQuestion = 145,
        EndTeleport = 146,
        ViewFriendInfo = 148,
        ChangeFace = 151,
        ViewPartnerInfo = 152,
        DetainItem = 153,
        ItemsDetained = 155,
        NinjaStep = 156,
        HideInterface = 158,
        OpenUpgrade = 160,
        Away = 161,
        PathFinding = 162,
        DragonBallDropped = 165,
        ChangeLook = 178,
        TableState = 233,
        TablePot = 234,
        TablePlayerCount = 235,
        CompleteLogin = 251,
        UpgradeMagicSkill = 252,
        UpgradeWeaponSkill = 253,
        ObserveFriendEquipment = 310,
        ActionRequestUserInfo = 408
    }
}