﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - FtwCore - 2076 - MsgQuench.cs
// Last Edit: 2019/11/24 19:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

namespace FtwCore.Networking.Packets
{
    public sealed class MsgQuench : PacketStructure
    {
        public MsgQuench()
            : base(PacketType.MsgQuench, 24, 16)
        {
        }

        public MsgQuench(byte[] packet)
            : base(packet)
        {
        }

        public PurificationMode Mode
        {
            get => (PurificationMode) ReadByte(4);
            set => WriteByte((byte) value, 4);
        }

        /// <summary>
        /// The item identity (cq_item id) of the item.
        /// </summary>
        public uint ItemIdentity
        {
            get => ReadUInt(8);
            set => WriteUInt(value, 8);
        }

        /// <summary>
        /// The identity of the artifact that will be stabilized.
        /// </summary>
        public uint TargetIdentity
        {
            get => ReadUInt(12);
            set => WriteUInt(value, 12);
        }
    }

    public enum PurificationMode : byte
    {
        ItemArtifact = 1,
        Purify = 0,
        Stabilize = 2
    }
}