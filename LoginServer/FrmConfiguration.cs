﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5729 (auto hunt feature)
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - LoginServer - FrmConfiguration.cs
// Last Edit: 2019/04/22 18:34
// Created: 2019/04/22 17:02
// ////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace LoginServer
{
    public partial class FrmConfiguration : Form
    {
        private List<GameServerConfig> m_currentServers = new List<GameServerConfig>();
        private List<GameServerConfig> m_removedServers = new List<GameServerConfig>();

        public FrmConfiguration()
        {
            InitializeComponent();
            if (File.Exists("AccountServer.xml"))
            {
                XDocument reader = XDocument.Load("AccountServer.xml");

                txtMySqlHost.Text = reader.XPathSelectElement("/AccountServerConfiguration/MySQL/Hostname")?.Value;
                txtMySqlUser.Text = reader.XPathSelectElement("/AccountServerConfiguration/MySQL/Username")?.Value;
                txtMySqlPassword.Text = reader.XPathSelectElement("/AccountServerConfiguration/MySQL/Password")?.Value;
                txtMySqlGameDatabase.Text =
                    reader.XPathSelectElement("/AccountServerConfiguration/MySQL/Database")?.Value;
                decimal value = 3306;
                decimal.TryParse((reader.XPathSelectElement("/AccountServerConfiguration/MySQL/Port")?.Value ?? "0"),
                    out value);
                numPort.Value = value;

                foreach (var server in reader.XPathSelectElements("/AccountServerConfiguration/Server"))
                {
                    GameServerConfig add = new GameServerConfig
                    {
                        Name = server.Attribute("id")?.Value ?? "Invalid",
                        Username = server.Element("Username")?.Value ?? "test",
                        Password = server.Element("Password")?.Value ?? "test",
                        IpAddress = server.Element("IpAddress")?.Value ?? "127.0.0.1",
                        Port = int.Parse(server.Element("Port")?.Value ?? "9865")
                    };

                    if (!AddServer(add))
                    {
                        m_removedServers.Add(add);
                        continue;
                    }
                }

                btnCancel.Enabled = true;
            }
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            if (!File.Exists("AccountServer.xml"))
            {
                XmlTextWriter writer =
                    new XmlTextWriter($"{Environment.CurrentDirectory}\\AccountServer.xml", Encoding.UTF8)
                    {
                        Formatting = Formatting.Indented
                    };
                writer.WriteStartDocument();

                writer.WriteStartElement("AccountServerConfiguration");

                writer.WriteStartElement("MySQL");
                writer.WriteElementString("Hostname", txtMySqlHost.Text);
                writer.WriteElementString("Username", txtMySqlUser.Text);
                writer.WriteElementString("Password", txtMySqlPassword.Text);
                writer.WriteElementString("Database", txtMySqlGameDatabase.Text);
                writer.WriteElementString("Port", "3306");
                writer.WriteEndElement(); // MySQL

                writer.WriteStartElement("TransferKey");
                writer.WriteElementString("Key", "EypKhLvYJ3zdLCTyz9Ak8RAgM78tY5F32b7CUXDuLDJDFBH8H67BWy9QThmaN5Vb");
                writer.WriteElementString("Salt", "MyqVgBf3ytALHWLXbJxSUX4uFEu3Xmz2UAY9sTTm8AScB7Kk2uwqDSnuNJske4By");
                writer.WriteEndElement(); // TransferKey

                writer.WriteElementString("LoginName", "test");
                writer.WriteElementString("Password", "test");

                writer.WriteEndElement(); // AccountServerConfiguration

                writer.WriteEndDocument();
                writer.Close();
            }
            else // we update it :)
            {
                XmlDocument xml = new XmlDocument();
                xml.Load($"{Environment.CurrentDirectory}\\AccountServer.xml");

                ChangeValue(xml, "/AccountServerConfiguration/MySQL/Hostname", txtMySqlHost.Text);
                ChangeValue(xml, "/AccountServerConfiguration/MySQL/Username", txtMySqlUser.Text);
                ChangeValue(xml, "/AccountServerConfiguration/MySQL/Password", txtMySqlPassword.Text);
                ChangeValue(xml, "/AccountServerConfiguration/MySQL/Database", txtMySqlGameDatabase.Text);
                ChangeValue(xml, "/AccountServerConfiguration/MySQL/Port", $"{numPort.Value:0}");

                foreach (var removed in m_removedServers)
                {
                    DeleteNode(xml, $"/AccountServerConfiguration/Server[@id='{removed.Name}']");
                }

                foreach (var saved in m_currentServers)
                {
                    if (!CheckNodeExists(xml, $"/AccountServerConfiguration/Server[@id='{saved.Name}']"))
                    {
                        XmlNode node = xml.SelectSingleNode($"/AccountServerConfiguration");
                        XElement element = new XElement("Server", new XAttribute("id", saved.Name),
                            new XElement("Name", saved.Name),
                            new XElement("Username", saved.Username),
                            new XElement("Password", saved.Password),
                            new XElement("IpAddress", saved.IpAddress),
                            new XElement("Port", saved.Port)
                        );
                        node.AppendChild((XmlElement) xml.ReadNode(element.CreateReader()));
                    }
                    else
                    {
                    }
                }

                xml.Save($"{Environment.CurrentDirectory}\\AccountServer.xml");
            }

            Close();
        }

        private bool AddServer(GameServerConfig server)
        {
            if (ServerExists(server.Name))
            {
                MessageBox.Show(this, $@"Duplicate for server {server.Name}");
                return false;
            }

            m_currentServers.Add(server);
            lbxServers.Items.Add($"{server.Name}({server.IpAddress}:{server.Port})");
            return true;
        }

        private bool ServerExists(string name)
        {
            foreach (var server in m_currentServers)
            {
                if (string.Equals(server.Name, name, StringComparison.CurrentCultureIgnoreCase))
                    return true;
            }

            return false;
        }

        private void DeleteNode(XmlDocument xml, string xpath)
        {
            DeleteNode(xml, xml.SelectSingleNode(xpath));
        }

        private void DeleteNode(XmlDocument xml, XmlNode node)
        {
            if (node == null)
                return;

            XmlNode parent = node.ParentNode;
            parent?.RemoveChild(node);
        }

        private void ChangeValue(XmlDocument xml, string xpath, string newValue)
        {
            try
            {
                xml.SelectSingleNode(xpath).InnerText = newValue;
            }
            catch
            {
            }
        }

        private bool CheckNodeExists(XmlDocument xml, string xpath)
        {
            return xml.SelectSingleNode(xpath) != null;
        }

        private void btnAddServer_Click(object sender, EventArgs e)
        {
            if (txtServerName.Text.Length < 4 || txtServerName.Text.Length > 15)
            {
                MessageBox.Show(this, "Invalid server name.");
                return;
            }

            if (ServerExists(txtServerName.Text))
            {
                MessageBox.Show(this, "Server already registered.");
                return;
            }

            int port = 0;
            if (string.IsNullOrEmpty(txtServerUsername.Text)
                || string.IsNullOrEmpty(txtServerPassword.Text)
                || string.IsNullOrEmpty(txtServerPassword.Text)
                || !int.TryParse(txtServerPort.Text, out port)
            )
            {
                MessageBox.Show(this, "Please verify the information. There may be incosistences.");
                return;
            }

            GameServerConfig server = new GameServerConfig
            {
                Name = txtServerName.Text,
                Username = txtServerUsername.Text,
                Password = txtServerPassword.Text,
                IpAddress = txtServerIpAddress.Text,
                Port = port
            };
            if (!AddServer(server))
            {
                MessageBox.Show(this, @"Could not add server. Unknown error.");
                return;
            }

            txtServerName.Text = string.Empty;
            txtServerUsername.Text = string.Empty;
            txtServerPassword.Text = string.Empty;
            txtServerIpAddress.Text = string.Empty;
            txtServerPort.Text = string.Empty;
        }

        private void btnDeleteServer_Click(object sender, EventArgs e)
        {
            if (lbxServers.SelectedIndex < 0 || lbxServers.SelectedIndex >= m_currentServers.Count)
                return;

            GameServerConfig remove = m_currentServers[lbxServers.SelectedIndex];
            m_currentServers.RemoveAt(lbxServers.SelectedIndex);
            m_removedServers.Add(remove);
            lbxServers.Items.RemoveAt(lbxServers.SelectedIndex);
        }
    }
}