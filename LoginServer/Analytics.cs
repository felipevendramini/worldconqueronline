﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - LoginServer - Analytics.cs
// Last Edit: 2019/11/24 19:07
// Created: 2019/10/14 14:47
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Threading;
using FtwCore.Common;
using LoginServer.Structures;

#endregion

namespace LoginServer
{
    public class Analytics
    {
#if DEBUG
        public const int ANALYTIC_INTERVAL = 86400;
#else
        public const int ANALYTIC_INTERVAL = 300;
#endif

        public const string ANALYTIC_FILENAME = "MsgLoginAnalytics";

        public static LoginStatistic Statistic;

        private int m_lastTick;
        private readonly DateTime m_dtServerStartTime;

        private LogWriter m_pLog;

        public Analytics(string szFilename = null)
        {
            if (szFilename == null)
                szFilename = Environment.CurrentDirectory + @"\";

            m_pLog = new LogWriter(szFilename);
            m_dtServerStartTime = DateTime.Now;

            m_lastTick = UnixTimestamp.Now();
        }

        public DateTime StartTime => m_dtServerStartTime;

        public void IncrementSentBytes(long bytes)
        {
            Interlocked.Add(ref Statistic.SentBytes, bytes);
        }

        public void IncrementRecvBytes(long bytes)
        {
            Interlocked.Add(ref Statistic.RecvBytes, bytes);
        }

        public void IncrementSentPackets(long packets = 1)
        {
            Interlocked.Add(ref Statistic.SentPackets, packets);
        }

        public void IncrementRecvPackets(long packets = 1)
        {
            Interlocked.Add(ref Statistic.RecvPackets, packets);
        }

        private void SaveLog(string msg, LogType type = LogType.MESSAGE)
        {
            m_pLog.SaveLog(msg, ANALYTIC_FILENAME, type);
        }

        public void OnTimer(int nTick)
        {
            if (nTick < m_lastTick + ANALYTIC_INTERVAL)
                return; // invalid time

            SaveLog("");
            SaveLog("========================================================================");
            SaveLog("Conquer Online Server - Account Server");
            SaveLog($"Server Startup Time: {DateTime.Now.ToShortDateString()}");
            var interval = DateTime.Now - m_dtServerStartTime;
            SaveLog($"Server takes: {interval.Days} days, {interval.Hours} hours, {interval.Minutes} minutes, {interval.Seconds} secs");
            SaveLog("========================================================================");
            SaveLog($"SentPackets:{Statistic.SentPackets:N0}\tSendBytes:{Statistic.SentBytes:N0}");
            SaveLog($"RecvPackets:{Statistic.RecvPackets:N0}\tRecvBytes:{Statistic.RecvBytes:N0}");
            SaveLog("========================================================================");

            Statistic = new LoginStatistic();

            if (nTick > m_lastTick + ANALYTIC_INTERVAL + 10)
                m_pLog.SaveLog("Analytics overtime", ANALYTIC_FILENAME, LogType.WARNING);

            m_lastTick = UnixTimestamp.Now();
        }
    }
}