﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - LoginServer - ServerKernel.cs
// Last Edit: 2019/11/24 19:07
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Concurrent;
using FtwCore.Common;
using FtwCore.Database;
using LoginServer.Networking.GameServerSocket;
using LoginServer.Networking.LoginServer;
using LoginServer.Structures;

#endregion

namespace LoginServer
{
    public static class ServerKernel
    {
        public static MySqlConfig MyDatabase;

        /// <summary>
        /// The message sent by this server to the MsgServer after a successfull connection
        /// to confirm if the server is compatible and enabled to login.
        /// </summary>
        public static string HelloSendString = "fzYPi0xsRGiBmj6X";

        /// <summary>
        /// The message that the MsgServer should send after a successfull connection to 
        /// confirm if the server is compatible and enabled to login.
        /// </summary>
        public static string HelloExpectedMsg = "rDOLjXHL3bFkyMVk";

        public static string Username = "test";
        public static string Password = "test";

        public static Analytics Analytics = new Analytics();

        /// <summary>
        /// This dictionary holds the Server Sockets that are already connected to this
        /// Login Server;
        /// </summary>
        public static ConcurrentDictionary<string, GameServer> OnlineServers = new ConcurrentDictionary<string, GameServer>();

        /// <summary>
        /// This dictionary holds information about banned IP Addresses.
        /// </summary>
        public static ConcurrentDictionary<string, BannedAddress> BannedAddresses = new ConcurrentDictionary<string, BannedAddress>();

        /// <summary>
        /// This dictionary holds information about the addresses that tried to login to this server.
        /// </summary>
        public static ConcurrentDictionary<string, LoginAttemptRecord> LoginAttemptRecords = new ConcurrentDictionary<string, LoginAttemptRecord>();

        /// <summary>
        /// List with the online players across all servers. An account cannot be connected in two servers at the same time.
        /// </summary>
        public static ConcurrentDictionary<uint, Character> OnlinePlayers = new ConcurrentDictionary<uint, Character>();

        // libraries
        /// <summary>
        /// This dictionary holds informations about the game servers enabled to login to 
        /// this MsgServer.
        /// </summary>
        public static ConcurrentDictionary<string, EnabledGameServer> EnabledServer = new ConcurrentDictionary<string, EnabledGameServer>();

        // log
        public static LogWriter LogSystem = new LogWriter(Environment.CurrentDirectory + @"\");
        public const string LOGIN_SERVER_LOG_FILE = "Account Server";

        // server config
        public static int MaxLoginAttempt = 5;
        public static int MinLoginTime = 10;
        public static int BanTime = 10; // in minutes
        public static int WrongPasswordLock = 5;
        public static int MinOnlinePlayer = 10;

        // constant configuration
        public static string LoginTransferKey = "EypKhLvYJ3zdLCTyz9Ak8RAgM78tY5F32b7CUXDuLDJDFBH8H67BWy9QThmaN5Vb";
        public static string LoginTransferSalt = "MyqVgBf3ytALHWLXbJxSUX4uFEu3Xmz2UAY9sTTm8AScB7Kk2uwqDSnuNJske4By";

        public static LoginSocket LoginSocket = new LoginSocket();
        public static MsgServerSocket GameSocket = new MsgServerSocket();

        public static DateTime StartTime = DateTime.MaxValue;
    }
}