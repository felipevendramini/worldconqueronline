﻿namespace LoginServer
{
    partial class FrmMain
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbServerInfo = new System.Windows.Forms.GroupBox();
            this.lblOnlineUsr = new System.Windows.Forms.Label();
            this.lblOnlineSv = new System.Windows.Forms.Label();
            this.lblRecvPackets = new System.Windows.Forms.Label();
            this.lblSentPackets = new System.Windows.Forms.Label();
            this.lblOnlineTime = new System.Windows.Forms.Label();
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.lbxPlayersPerServer = new System.Windows.Forms.ListBox();
            this.gbServerInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbServerInfo
            // 
            this.gbServerInfo.Controls.Add(this.lblOnlineUsr);
            this.gbServerInfo.Controls.Add(this.lblOnlineSv);
            this.gbServerInfo.Controls.Add(this.lblRecvPackets);
            this.gbServerInfo.Controls.Add(this.lblSentPackets);
            this.gbServerInfo.Controls.Add(this.lblOnlineTime);
            this.gbServerInfo.Location = new System.Drawing.Point(12, 12);
            this.gbServerInfo.Name = "gbServerInfo";
            this.gbServerInfo.Size = new System.Drawing.Size(315, 186);
            this.gbServerInfo.TabIndex = 0;
            this.gbServerInfo.TabStop = false;
            this.gbServerInfo.Text = "Server Information";
            // 
            // lblOnlineUsr
            // 
            this.lblOnlineUsr.AutoSize = true;
            this.lblOnlineUsr.Location = new System.Drawing.Point(8, 80);
            this.lblOnlineUsr.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.lblOnlineUsr.Name = "lblOnlineUsr";
            this.lblOnlineUsr.Size = new System.Drawing.Size(67, 13);
            this.lblOnlineUsr.TabIndex = 2;
            this.lblOnlineUsr.Text = "Online Users";
            // 
            // lblOnlineSv
            // 
            this.lblOnlineSv.AutoSize = true;
            this.lblOnlineSv.Location = new System.Drawing.Point(7, 64);
            this.lblOnlineSv.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.lblOnlineSv.Name = "lblOnlineSv";
            this.lblOnlineSv.Size = new System.Drawing.Size(76, 13);
            this.lblOnlineSv.TabIndex = 2;
            this.lblOnlineSv.Text = "Online Servers";
            // 
            // lblRecvPackets
            // 
            this.lblRecvPackets.AutoSize = true;
            this.lblRecvPackets.Location = new System.Drawing.Point(7, 48);
            this.lblRecvPackets.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.lblRecvPackets.Name = "lblRecvPackets";
            this.lblRecvPackets.Size = new System.Drawing.Size(72, 13);
            this.lblRecvPackets.TabIndex = 1;
            this.lblRecvPackets.Text = "PacketsRecv";
            // 
            // lblSentPackets
            // 
            this.lblSentPackets.AutoSize = true;
            this.lblSentPackets.Location = new System.Drawing.Point(7, 32);
            this.lblSentPackets.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.lblSentPackets.Name = "lblSentPackets";
            this.lblSentPackets.Size = new System.Drawing.Size(68, 13);
            this.lblSentPackets.TabIndex = 1;
            this.lblSentPackets.Text = "PacketsSent";
            // 
            // lblOnlineTime
            // 
            this.lblOnlineTime.AutoSize = true;
            this.lblOnlineTime.Location = new System.Drawing.Point(6, 16);
            this.lblOnlineTime.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.lblOnlineTime.Name = "lblOnlineTime";
            this.lblOnlineTime.Size = new System.Drawing.Size(60, 13);
            this.lblOnlineTime.TabIndex = 1;
            this.lblOnlineTime.Text = "OnlineTime";
            // 
            // txtOutput
            // 
            this.txtOutput.Location = new System.Drawing.Point(12, 204);
            this.txtOutput.Multiline = true;
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ReadOnly = true;
            this.txtOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtOutput.Size = new System.Drawing.Size(632, 234);
            this.txtOutput.TabIndex = 1;
            // 
            // lbxPlayersPerServer
            // 
            this.lbxPlayersPerServer.FormattingEnabled = true;
            this.lbxPlayersPerServer.Location = new System.Drawing.Point(333, 12);
            this.lbxPlayersPerServer.Name = "lbxPlayersPerServer";
            this.lbxPlayersPerServer.Size = new System.Drawing.Size(311, 186);
            this.lbxPlayersPerServer.TabIndex = 2;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(656, 450);
            this.Controls.Add(this.lbxPlayersPerServer);
            this.Controls.Add(this.txtOutput);
            this.Controls.Add(this.gbServerInfo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmMain";
            this.Text = "Login Server - Inactive";
            this.Activated += new System.EventHandler(this.FrmMain_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.gbServerInfo.ResumeLayout(false);
            this.gbServerInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbServerInfo;
        private System.Windows.Forms.TextBox txtOutput;
        public System.Windows.Forms.Label lblRecvPackets;
        public System.Windows.Forms.Label lblSentPackets;
        public System.Windows.Forms.Label lblOnlineTime;
        public System.Windows.Forms.Label lblOnlineUsr;
        public System.Windows.Forms.Label lblOnlineSv;
        private System.Windows.Forms.ListBox lbxPlayersPerServer;
    }
}

