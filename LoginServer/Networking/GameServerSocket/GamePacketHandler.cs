﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - LoginServer - GamePacketHandler.cs
// Last Edit: 2019/11/24 19:06
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using LoginServer.Structures;

#endregion

namespace LoginServer.Networking.GameServerSocket
{
    public class GamePacketHandler
    {
        #region 11 - Hello World!

        [PacketHandlerType(PacketType.MsgLoginAuthRequest)]
        public void AuthRequest(GameServer pServer, byte[] pBuffer)
        {
            var pMsg = new MsgLoginSvAuthRequest(pBuffer);
            if (pMsg.Message != ServerKernel.HelloExpectedMsg)
            {
                pServer.Disconnect();
                return;
            }

            pServer.ConnectionState = InterServerState.MeetingOk;
            pServer.Send(new MsgLoginSvAuthRequest(ServerKernel.HelloSendString));
            Program.MainForm.WriteLog($"Waiting for server [{pServer.IpAddress}] to accept connection.",
                LogType.WARNING);
        }

        #endregion

        #region 12 - Authentication

        [PacketHandlerType(PacketType.MsgLoginAuthConfirm)]
        public void AuthenticationRequest(GameServer pServer, byte[] pBuffer)
        {
            var pMsg = new MsgLoginSvAuthentication(pBuffer);
            if (pMsg.Username != ServerKernel.Username || pMsg.Password != ServerKernel.Password)
            {
                pServer.Send(new LoginSvResponsePacket(LoginServerResponse.LoginDeniedLogin));
                pServer.Disconnect();
                return;
            }

            pServer.ConnectionState = InterServerState.Connected;
            pServer.Send(new LoginSvResponsePacket(LoginServerResponse.LoginSuccessful));
        }

        #endregion

        #region 20 - Server Online Player

        [PacketHandlerType(PacketType.MsgLoginRequestOnlineNumber)]
        public void ProcessMsgOnlineNum(GameServer pServer, byte[] pBuffer)
        {
            if (pServer.ConnectionState < InterServerState.Connected)
                return;
            var pMsg = new MsgLoginSvPlayerAmount(pBuffer);
            switch (pMsg.Type)
            {
                case LoginPlayerAmountRequest.ReplyOnlineAmount:
                {
                    pServer.OnlinePlayers = pMsg.Amount;
                    break;
                }

                case LoginPlayerAmountRequest.ReplyOnlineMaxamount:
                {
                    pServer.MaxPermitedPlayers = pMsg.Amount;
                    break;
                }
            }
        }

        #endregion

        #region 24 - Game Server Information

        [PacketHandlerType(PacketType.MsgLoginRequestServerInfo)]
        public void ServerInformation(GameServer pServer, byte[] pBuffer)
        {
            if (pServer.ConnectionState < InterServerState.Connected)
            {
                pServer.Disconnect();
                Program.MainForm.WriteLog($"Disconnected[{pServer.IpAddress}]: Unauthorized.", LogType.WARNING);
                return;
            }

            var pMsg = new MsgServerInformation(pBuffer);
            pServer.ServerName = pMsg.ServerName;
            pServer.MaxPermitedPlayers = pMsg.MaxOnlinePlayers;
            pServer.OnlinePlayers = pMsg.OnlinePlayers;
            pServer.GamePort = pMsg.GamePort;

            if (!ServerKernel.OnlineServers.TryAdd(pServer.ServerName, pServer))
            {
                pServer.Disconnect();
                return;
            }

            Program.MainForm.WriteLog(
                $"Server [{pServer.ServerName}] has connected with the following information>\r\n\tPort: {pServer.GamePort}, Online: {pServer.OnlinePlayers}, MaxOnline: {pServer.MaxPermitedPlayers}");
        }

        #endregion

        #region 28 - MsgLoginUserInfo

        [PacketHandlerType(PacketType.MsgLoginUserInfo)]
        public void ProcessLoginUserInfo(GameServer server, byte[] buffer)
        {
            if (server == null)
                return;

            MsgLoginUserInfo msg = new MsgLoginUserInfo(buffer);
            switch (msg.Mode)
            {
                case LoginUserInfoMode.Connect:
                {
                    // remove any remaining user if not removed as usual
                    if (!ServerKernel.OnlinePlayers.TryGetValue(msg.AccountIdentity, out Character user))
                    {
                        ServerKernel.OnlinePlayers.TryAdd(msg.AccountIdentity, user = new Character
                        {
                            AccountIdentity = msg.AccountIdentity,
                            GameServer = server,
                            LoginRcd = new LoginRcdEntity
                            {
                                Geolocation = "!",
                                IpAddress = msg.IpAddress,
                                LoginTime = UnixTimestamp.Now(),
                                MacAddress = msg.MacAddress,
                                OnlineSecond = 0,
                                ResourceSource = "2",
                                UserIdentity = msg.UserIdentity
                            },
                            Name = msg.UserName,
                            OnlineEntity = new OnlinePlayersEntity
                            {
                                AccountIdentity = msg.AccountIdentity,
                                ServerName = server.ServerName,
                                LoginTime = DateTime.Now,
                                CharacterIdentity = msg.UserIdentity,
                                CharacterName = msg.UserName
                            },
                            Identity = msg.UserIdentity
                        });

                        new LoginRcdRepository().Save(user.LoginRcd);
                        new OnlinePlayersRepository().Save(user.OnlineEntity);
                    }
                    else
                    {
                        user.OnlineEntity = new OnlinePlayersEntity
                        {
                            AccountIdentity = msg.AccountIdentity,
                            ServerName = server.ServerName,
                            LoginTime = DateTime.Now,
                            CharacterIdentity = msg.UserIdentity,
                            CharacterName = msg.UserName
                        };
                        new OnlinePlayersRepository().Save(user.OnlineEntity);
                        user.GameServer = server;
                        user.Name = msg.UserName;
                        user.Identity = msg.UserIdentity;
                    }
                    break;
                }

                case LoginUserInfoMode.Disconnect:
                {
                    if (ServerKernel.OnlinePlayers.TryRemove(msg.AccountIdentity, out Character user))
                    {
                        user.SetLogout();
                    }
                    break;
                }
            }
        }

        #endregion

        /// <summary>
        /// This function reports a missing packet handler to the console. It writes the length and type of the
        /// packet, then a packet dump to the console.
        /// </summary>
        /// <param name="packet">The packet buffer being reported.</param>
        public static void Report(byte[] packet)
        {
            ushort length = BitConverter.ToUInt16(packet, 0);
            ushort identity = BitConverter.ToUInt16(packet, 2);

            // Print the packet and the packet header:
            ServerKernel.LogSystem.SaveLog($"Missing Packet Handler: {identity} (Length: {length})", "missing_packet",
                LogType.DEBUG);
            string aPacket = "";

            for (int index = 0; index < length; index++)
                aPacket += $"{packet[index]:X2} ";

            ServerKernel.LogSystem.SaveLog(aPacket, "missing_packet", LogType.DEBUG);
        }
    }
}