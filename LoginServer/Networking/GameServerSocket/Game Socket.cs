﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - LoginServer - Game Socket.cs
// Last Edit: 2019/11/24 19:06
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Net.Sockets;
using FtwCore.Common;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using FtwCore.Networking.Sockets;

#endregion

namespace LoginServer.Networking.GameServerSocket
{
    public sealed class MsgServerSocket : AsynchronousServerSocket
    {
        private readonly PacketProcessor<PacketHandlerType, PacketType, Action<GameServer, byte[]>> m_packetProcessor;

        public MsgServerSocket()
            : base("", AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
        {
            OnClientConnect = Connect;
            OnClientReceive = Receive;
            OnClientDisconnect = Disconnect;

            m_packetProcessor =
                new PacketProcessor<PacketHandlerType, PacketType, Action<GameServer, byte[]>>(new GamePacketHandler());
        }

        public void Connect(AsynchronousState pState)
        {
            var pClient = new GameServer(this, pState.Socket, null);
            pState.Client = pClient;
        }

        public void Receive(AsynchronousState pState)
        {
            if (pState.Client is GameServer pServer && pServer.Packet != null)
            {
                var type = (PacketType) BitConverter.ToUInt16(pServer.Packet, 2);
                Action<GameServer, byte[]> action = m_packetProcessor[type];

                ServerKernel.Analytics.IncrementRecvPackets();
                ServerKernel.Analytics.IncrementRecvBytes(pServer.Packet.Length);

                // Process the client's packet:
                if (action != null) action(pServer, pServer.Packet);
                else GamePacketHandler.Report(pServer.Packet);
            }
        }

        public void Disconnect(object pObj)
        {
            var pServer = pObj as GameServer;
            if (pServer?.ServerName == null) return;

            ServerKernel.OnlineServers.TryRemove(pServer.ServerName, out _);
            new OnlinePlayersRepository().DeleteFromServer(pServer.ServerName);
            Program.MainForm.WriteLog($"Server [{pServer.ServerName}] has disconnected.", LogType.WARNING);
        }
    }
}