﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - LoginServer - GameServer.cs
// Last Edit: 2019/11/24 19:06
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Net.Sockets;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Interfaces;
using FtwCore.Networking.Sockets;

#endregion

namespace LoginServer.Networking.GameServerSocket
{
    /// <summary>
    /// This class encapsulates the GameServer client. This contains informations about the server
    /// and is responsible to the communication between Login Server and MsgServer.
    /// </summary>
    public sealed class GameServer : Passport
    {
        private readonly AsynchronousServerSocket m_serverSocket;

        /// <summary>
        /// The server name might be unique, you can't signin 2 servers with the same name.
        /// </summary>
        public string ServerName;

        /// <summary>
        /// The amount of players that are actually online on the game server.
        /// </summary>
        public int OnlinePlayers;

        /// <summary>
        /// The max amount of online players on the game.
        /// </summary>
        public int MaxPermitedPlayers = 5;

        public int GamePort = 5816;

        public InterServerState ConnectionState = InterServerState.WaitingHello;

        /// <summary>
        /// Starts a new instance of GameServer.
        /// </summary>
        /// <param name="server"></param>
        /// <param name="socket"></param>
        /// <param name="cipher"></param>
        public GameServer(AsynchronousServerSocket server, Socket socket, ICipher cipher)
            : base(server, socket, cipher)
        {
            m_serverSocket = server;
        }

        /// <summary>
        /// Sends the buffer to the Game Server client.
        /// </summary>
        /// <param name="pMsg">The message buffer.</param>
        public new void Send(byte[] pMsg)
        {
            try
            {
                ServerKernel.Analytics.IncrementSentPackets();
                ServerKernel.Analytics.IncrementSentBytes(pMsg.Length);
                base.Send(pMsg);
            }
            catch (Exception ex)
            {
                ServerKernel.LogSystem.SaveLog("Could not send message to MsgServer\r\n" + ex,
                    LogType.ERROR);
            }
        }

        /// <summary>
        /// Checks if the server is online or not.
        /// </summary>
        /// <returns></returns>
        public bool IsOnline()
        {
            bool part1 = m_serverSocket.Poll(1000, SelectMode.SelectRead);
            bool part2 = m_serverSocket.Available == 0;
            return !part1 || !part2;
        }
    }
}