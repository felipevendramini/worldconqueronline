﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - LoginServer - LoginSocket.cs
// Last Edit: 2019/11/24 19:07
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Net.Sockets;
using FtwCore.Common;
using FtwCore.Networking.Packets;
using FtwCore.Networking.Sockets;
using FtwCore.Security;

#endregion

namespace LoginServer.Networking.LoginServer
{
    /// <summary>
    /// This class encapsulates the account server. It inherits functionality from the asynchronous server socket 
    /// class, allowing the server to be created and instantiated as a server socket system. It also contains the
    /// socket events used in processing clients, packets, and other socket events.
    /// </summary>
    public sealed class LoginSocket : AsynchronousServerSocket
    {
        PacketProcessor<PacketHandlerType, PacketType, Action<User, byte[]>> m_pProcessor;

        /// <summary>
        /// This class encapsulates the account server. It inherits functionality from the asynchronous server socket 
        /// class, allowing the server to be created and instantiated as a server socket system. It also contains the
        /// socket events used in processing clients, packets, and other socket events.
        /// </summary>
        public LoginSocket()
            : base("Login Server", AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
        {
            OnClientConnect = Connect;
            OnClientReceive = Receive;

            m_pProcessor =
                new PacketProcessor<PacketHandlerType, PacketType, Action<User, byte[]>>(new LoginPacketHandler());
        }

        /// <summary>
        /// This method is invoked when the client has been approved of connecting to the server. The client should
        /// be constructed in this method, and cipher algorithms should be initialized. If any packets need to be
        /// sent in the connection state, they should be sent here.
        /// </summary>
        /// <param name="state">Represents the status of an asynchronous operation.</param>
        public void Connect(AsynchronousState state)
        {
            var client = new User(this, state.Socket, new NetDragonAuthenticationCipher());
            state.Client = client;
            client.Send(new MsgEncryptCode(client.IpAddress.GetHashCode()));
        }

        /// <summary>
        /// This method is invoked when the client has data ready to be processed by the server. The server will
        /// switch between the packet type to find an appropriate function for processing the packet. If the
        /// packet was not found, the packet will be outputted to the console as an error.
        /// </summary>
        /// <param name="state">Represents the status of an asynchronous operation.</param>
        public void Receive(AsynchronousState state)
        {
            // Retrieve client information from the asynchronous state:
            var pClient = state.Client as User;

            if (pClient != null && pClient.Packet != null) // check if it's alright
            {
                var type = (PacketType) BitConverter.ToUInt16(pClient.Packet, 2);
                Action<User, byte[]> action = m_pProcessor[type];

                ServerKernel.Analytics.IncrementRecvPackets();
                ServerKernel.Analytics.IncrementRecvBytes(pClient.Packet.Length);

                // Process the client's packet:
                if (action != null) action(pClient, pClient.Packet);
                else LoginPacketHandler.Report(pClient.Packet);
            }
        }

        /// <summary>
        /// This method is invoked when the client is disconnecting from the server. It disconnects the client
        /// from server and disposes of game structures.
        /// </summary>
        /// <param name="state">Represents the status of an asynchronous operation.</param>
        public void Disconnect(object state)
        {
            try
            {
                var pClient = state as User;
                if (pClient == null)
                {
                    ServerKernel.LogSystem.SaveLog("Disconnection: pClient is null", "LoginServer", LogType.ERROR);
                    return; // hope it doesn't happen :)
                }

                ServerKernel.LogSystem.SaveLog($"User [{pClient.Account.Username}] has been disconnected");
            }
            catch (Exception ex)
            {
                ServerKernel.LogSystem.SaveLog(ex.ToString(), "LoginServer", LogType.EXCEPTION);
            }
        }
    }
}