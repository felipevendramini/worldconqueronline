﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - LoginServer - Login Packet Handler.cs
// Last Edit: 2019/11/24 19:06
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Linq;
using FtwCore.Common;
using FtwCore.Common.Enums;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Networking.Packets;
using FtwCore.Security;
using LoginServer.Networking.GameServerSocket;
using LoginServer.Structures;

#endregion

namespace LoginServer.Networking.LoginServer
{
    public class LoginPacketHandler
    {
        #region 1052 - Authentication Complete

        [PacketHandlerType(PacketType.MsgConnect)]
        public void HandleAuthenticationComplete(User pClient, byte[] pBuffer)
        {
        }

        #endregion

        #region 1100 - Mac Address

        [PacketHandlerType(PacketType.MsgPcNum)]
        public void ProcessMacAddress(User pClient, byte[] pBuffer)
        {
            var pMsg = new MsgMacAddr(pBuffer);
            Character awaiting = new Character
            {
                AccountIdentity = pClient.Account.Identity,
                OnlineEntity = new OnlinePlayersEntity
                {
                    AccountIdentity = pClient.Account.Identity,
                    LoginTime = DateTime.Now
                }
            };

            new LoginRcdRepository().Save(awaiting.LoginRcd = new LoginRcdEntity
            {
                UserIdentity = pClient.Account.Identity,
                IpAddress = pClient.IpAddress,
                MacAddress = pMsg.MacAddress,
                LoginTime = UnixTimestamp.Now(),
                ResourceSource = "2",
                Geolocation = "NOT_IMPLEMENTED",
                OnlineSecond = 0
            });

            ServerKernel.OnlinePlayers.TryAdd(awaiting.AccountIdentity, awaiting);

            string lastIpAddr = "";
            AccountEntity
                pUser = new AccountRepository().SearchByIdentity(pClient.Account.Identity); // fetch user information
            if (pUser != null)
            {
                lastIpAddr = pUser.IpAddress;
                pUser.MacAddress = pMsg.MacAddress;
                new AccountRepository().Save(pUser);
            }

            //GameServer server = ServerKernel.OnlineServers.Values.FirstOrDefault(x => x.ServerName == pClient.ServerName);
            //server?.Send(new MsgUsrLogin(pClient.Account.Identity, awaiting.LoginRcd.Identity)
            //{
            //    IpAddress = pClient.IpAddress,
            //    LastIpAddress = lastIpAddr,
            //    Authority = pClient.Account.Type,
            //    VipLevel = pClient.Account.Vip,
            //    RequestTime = (uint) UnixTimestamp.Now(),
            //    MacAddress = pMsg.MacAddress
            //});
        }

        #endregion

        #region 1124 - MsgAccountSrp6

        [PacketHandlerType(PacketType.MsgAccountSrp6)]
        public void ProcessAccountSrp6(User pClient, byte[] pBuffer)
        {
            MsgAccountSRP6 msg = new MsgAccountSRP6(pBuffer);

            // let's check if user is spamming login requests
            LoginAttemptRecord pLogin = null;
            if (!ServerKernel.LoginAttemptRecords.TryGetValue(pClient.IpAddress, out pLogin))
            {
                pLogin = new LoginAttemptRecord(pClient.IpAddress);
                ServerKernel.LoginAttemptRecords.TryAdd(pLogin.IpAddress, pLogin);
            }

            if (!pLogin.Enabled) // user spamming login?
            {
                pClient.Send(new MsgConnectEx(RejectionType.MaximumLoginAttempts));
                Program.MainForm.WriteLog(
                    $"User [{msg.Username}] has passport denied due to exceeding login limit on IP [{pClient.IpAddress}].");
                pClient.Disconnect();
                return;
            }

            AccountEntity account = new AccountRepository().SearchByName(msg.Username);
            if (account == null)
            {
                pClient.Send(new MsgConnectEx(RejectionType.InvalidAccount));
                Program.MainForm.WriteLog($"[{pClient.IpAddress}] tried to access invalid username {msg.Username}");
                pClient.Disconnect();
                return;
            }

            pClient.Account = account;

            var password = string.Empty;
            foreach (var c in msg.Password)
            {
                switch (c)
                {
                    case '-':
                        password += '0';
                        break;
                    case '#':
                        password += '1';
                        break;
                    case '(':
                        password += '2';
                        break;
                    case '"':
                        password += '3';
                        break;
                    case '%':
                        password += '4';
                        break;
                    case '\f':
                        password += '5';
                        break;
                    case '\'':
                        password += '6';
                        break;
                    case '$':
                        password += '7';
                        break;
                    case '&':
                        password += '8';
                        break;
                    case '!':
                        password += '9';
                        break;
                    default:
                        password += c;
                        break;
                }
            }

            if (account.Password != WhirlpoolHash.Hash(password))
            {
                pClient.Send(new MsgConnectEx(RejectionType.InvalidPassword));
                Program.MainForm.WriteLog(
                    $"[{pClient.IpAddress}] {msg.Username} tried to connect with invalid password.");
                pClient.Disconnect();
                return;
            }

            if (account.Lock != 0)
            {
                switch (account.Lock)
                {
                    case 1:
                        pClient.Send(new MsgConnectEx(RejectionType.AccountBanned));
                        break;
                    case 2:
                        pClient.Send(new MsgConnectEx(RejectionType.AccountLocked));
                        break;
                    case 3:
                        pClient.Send(new MsgConnectEx(RejectionType.AccountNotActivated));
                        break;
                    case 4:
                        if (UnixTimestamp.Now() > account.LockExpire)
                        {
                            account.Lock = 0;
                            account.LockExpire = 0;
                            new AccountRepository().Save(account);
                            pClient.Send(new MsgConnectEx(RejectionType.PleaseTryAgainLater));
                            Program.MainForm.WriteLog(
                                $"[{pClient.IpAddress}] {account.Username} has been removed from its ban state.");
                        }
                        else
                            pClient.Send(new MsgConnectEx(RejectionType.AccountBanned));

                        break;
                }

                Program.MainForm.WriteLog($"[{pClient.IpAddress}] {account.Username} is banned");
                pClient.Disconnect();
                return;
            }

#if DEBUG
            if (account.Type < 6)
            {
                pClient.Send(new MsgConnectEx(RejectionType.NonCooperatorAccount));
                Program.MainForm.WriteLog(
                    $"[{pClient.IpAddress}] has tried to login on Server {msg.ServerName} during alpha tests with non cooperator account.");
                pClient.Disconnect();
                return;
            }
#endif

            GameServer pServer = null;
            if (ServerKernel.OnlineServers.Count > 1 && !ServerKernel.OnlineServers.TryGetValue(msg.Server, out pServer)
                || (pServer = ServerKernel.OnlineServers.Values.FirstOrDefault()) == null)
                // server is not online
            {
                pClient.Send(new MsgConnectEx(RejectionType.ServerMaintenance));
                Program.MainForm.WriteLog(
                    $"[{pClient.IpAddress}] {account.Username} has tried to login on invalid server {msg.ServerName}");
                pClient.Disconnect();
                return;
            }

            uint dwHash = (uint) ThreadSafeRandom.RandGet(1000, int.MaxValue);

            var pTransferCipher = new TransferCipher(ServerKernel.LoginTransferKey,
                ServerKernel.LoginTransferSalt, pClient.IpAddress);
            var pCrypto = pTransferCipher.Encrypt(new[] {account.Identity, dwHash});

            string szAddress = "135.12.15.139"; // random ip just to connect
            if (!pServer.IpAddress.StartsWith("127") && pServer.IpAddress != "localhost")
                szAddress = pServer.IpAddress;

            pServer.Send(new MsgUsrLogin(account.Identity, dwHash)
            {
                IpAddress = pClient.IpAddress,
                LastIpAddress = account.IpAddress,
                Authority = account.Type,
                VipLevel = account.Vip,
                RequestTime = (uint) UnixTimestamp.Now()
            });

            pClient.ServerName = pServer.ServerName;
            pClient.Send(new MsgConnectEx(pCrypto[0], pCrypto[1], szAddress, 5816));

            if (account.FirstLogin == 0)
                account.FirstLogin = (uint) UnixTimestamp.Now();

            account.LastLogin = UnixTimestamp.Now();
            account.IpAddress = pClient.IpAddress;
            new AccountRepository().Save(account);

            Program.MainForm.WriteLog(
                $"[{pClient.IpAddress}] {account.Username} has successfully logged in on server {msg.ServerName}");
        }

        #endregion

        /// <summary>
        /// This function reports a missing packet handler to the console. It writes the length and type of the
        /// packet, then a packet dump to the console.
        /// </summary>
        /// <param name="packet">The packet buffer being reported.</param>
        public static void Report(byte[] packet)
        {
            ushort length = BitConverter.ToUInt16(packet, 0);
            ushort identity = BitConverter.ToUInt16(packet, 2);

            // Print the packet and the packet header:
            Program.MainForm.WriteLog($"Missing Packet Handler: {identity} (Length: {length})", LogType.DEBUG);
            string aPacket = "";

            for (int index = 0; index < length; index++)
                aPacket += $"{packet[index]:X2} ";

            ServerKernel.LogSystem.SaveLog(aPacket, "missing_packet", LogType.DEBUG);
        }
    }
}