﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - LoginServer - Character.cs
// Last Edit: 2019/11/24 19:07
// Created: 2019/10/10 17:38
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using LoginServer.Networking.GameServerSocket;

#endregion

namespace LoginServer.Structures
{
    public class Character
    {
        public GameServer GameServer { get; set; }
        public uint AccountIdentity { get; set; }
        public uint Identity { get; set; }
        public string Name { get; set; }
        public LoginRcdEntity LoginRcd { get; set; }
        public OnlinePlayersEntity OnlineEntity { get; set; }

        /// <summary>
        /// True if the server is disconnecting, so it wont affect the online time.
        /// </summary>
        public void SetLogout(bool forced = false)
        {
            if (!forced)
            {
                if (LoginRcd != null)
                {
                    LoginRcd.OnlineSecond = UnixTimestamp.Now() - LoginRcd.LoginTime;
                    new LoginRcdRepository().Save(LoginRcd);
                }
            }

            if (OnlineEntity != null)
            {
                new OnlinePlayersRepository().Delete(OnlineEntity);
            }
        }
    }
}