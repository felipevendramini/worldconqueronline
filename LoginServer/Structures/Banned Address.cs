﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - LoginServer - Banned Address.cs
// Last Edit: 2019/11/24 19:07
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Collections.Generic;
using FtwCore.Common;

#endregion

namespace LoginServer.Structures
{
    public sealed class BannedAddress
    {
        private readonly string m_address;
        private readonly List<string> m_usernames;
        private uint m_banTime;

        private readonly TimeOut m_tBanTime;

        public BannedAddress(string address, List<string> usernames, uint banTime, int banSeconds)
        {
            m_address = address;
            m_usernames = usernames;
            m_banTime = banTime;

            m_tBanTime = new TimeOut(banSeconds);
            m_tBanTime.Update();
        }

        public string Address
        {
            get { return m_address; }
        }

        public List<string> Usernames
        {
            get { return m_usernames; }
        }

        public uint BanTime
        {
            get { return m_banTime; }
            set { m_banTime = value; }
        }

        public void SetBan(int nTime)
        {
            m_tBanTime.SetInterval(nTime);
            m_tBanTime.Update();
        }

        public void UnsetBan()
        {
            m_tBanTime.Clear();
        }

        public bool Banned
        {
            get { return !m_tBanTime.IsTimeOut(); }
        }
    }
}