﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - LoginServer - Login Attempt Record.cs
// Last Edit: 2019/11/24 19:07
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Common;

#endregion

namespace LoginServer.Structures
{
    public sealed class LoginAttemptRecord
    {
        private TimeOut m_pTimeOut = new TimeOut(10);
        private TimeOut m_pUnlock = new TimeOut(60);

        private string m_szAddress;
        private int m_bTries;

        public LoginAttemptRecord(string ipAddress)
        {
            m_szAddress = ipAddress;
            m_pTimeOut.Update();
        }

        /// <summary>
        /// The IP Address that tried to login on the server.
        /// </summary>
        public string IpAddress
        {
            get { return m_szAddress; }
        }

        /// <summary>
        /// User may login 5 times every 10 seconds, no mather if it has successfully logged in or not.
        /// If it exceeds the 5 tries, user will be locked for 1 minute.
        /// </summary>
        public bool Enabled
        {
            get
            {
                if (!m_pTimeOut.ToNextTime())
                    m_bTries++;

                if (m_bTries > 5)
                {
                    if (!m_pUnlock.IsTimeOut())
                        m_pUnlock.Update();
                    else
                    {
                        m_bTries = 0;
                        return true;
                    }

                    return false;
                }

                return true;
            }
        }
    }
}