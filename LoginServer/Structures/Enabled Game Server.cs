﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - LoginServer - Enabled Game Server.cs
// Last Edit: 2019/11/24 19:07
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Runtime.InteropServices;

#endregion

namespace LoginServer.Structures
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode, Pack = 4, Size = 52)]
    public struct EnabledGameServer
    {
        public EnabledGameServer(string szName, int nPort, string szUser, string szPass)
        {
            Name = szName;
            Port = nPort;
            Username = szUser;
            Password = szPass;
        }

        public readonly int Port;
        public readonly string Name;
        public readonly string Username;
        public readonly string Password;
    }
}