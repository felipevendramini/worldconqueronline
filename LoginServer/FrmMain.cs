﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - LoginServer - FrmMain.cs
// Last Edit: 2019/11/24 19:07
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Xml.XPath;
using FtwCore;
using FtwCore.Common;
using FtwCore.Database;
using LoginServer.Networking.GameServerSocket;
using LoginServer.Networking.LoginServer;
using LoginServer.Structures;

#endregion

namespace LoginServer
{
    public partial class FrmMain : Form
    {
        private bool m_bHasOppenedOnce;
        private ServerState m_serverState;
        private BackgroundWorker m_bgWorker;

        public FrmMain()
        {
            InitializeComponent();
        }

        private void btnConfigure_Click(object sender, EventArgs e)
        {
            FrmConfiguration form = new FrmConfiguration();
            form.ShowDialog(this);
        }

        public void AppendText(TextBox control, string text)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<TextBox, string>(AppendText), control, text);
                return;
            }

            control.AppendText(text);
        }

        public void EditLabel(Label control, string text)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<Label, string>(EditLabel), control, text);
                return;
            }

            control.Text = text;
        }

        private void UpdateOnlineList()
        {
            if (InvokeRequired)
            {
                Invoke(new Action<ListBox>(InvokeUpdateOnlineList), lbxPlayersPerServer);
                return;
            }

            InvokeUpdateOnlineList(lbxPlayersPerServer);
        }

        private void InvokeUpdateOnlineList(ListBox control)
        {
            control.BeginUpdate();
            control.Items.Clear();
            try
            {
                foreach (var server in ServerKernel.OnlineServers.Values)
                {
                    var pPlayers = ServerKernel.OnlinePlayers.Values
                        .Where(x => x?.GameServer?.ServerName == server.ServerName).ToArray();
                    control.Items.Add($"{server.ServerName} [Online Players: {pPlayers.Length}]");
                    foreach (var player in pPlayers.Where(x => !string.IsNullOrEmpty(x.Name)))
                    {
                        control.Items.Add($"\t[{player.Identity}]{player.Name}");
                    }
                }
            }
            catch
            {
            }

            control.EndUpdate();
        }

        public void PureLog(string text, bool bOutput = true)
        {
            string msg = ServerKernel.LogSystem.SavePureLog(text, LogWriter.STR_SYSLOG_ACCOUNTSERVER);
            if (bOutput)
                AppendText(txtOutput, $"\r\n{msg}");
        }

        public void UpdateTitle(string text)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<string>(UpdateTitle), text);
            }

            Text = text;
        }

        public void WriteLog(string text, LogType type = LogType.MESSAGE, bool bOutput = true)
        {
            string msg = ServerKernel.LogSystem.SaveLog(text, LogWriter.STR_SYSLOG_ACCOUNTSERVER, type);
            if (bOutput)
                AppendText(txtOutput, $"\r\n{msg}");
        }

        private void FrmMain_Activated(object sender, EventArgs e)
        {
            if (!m_bHasOppenedOnce)
            {
                #region Server information logging

                PureLog("Project WConquer: Conquer Online Private Server Emulator");
                PureLog("Developed by Felipe Vieira (FTW! Masters)");
                PureLog("April 21th, 2019 - All Rights Reserved");

                WriteLog(Environment.CurrentDirectory, LogType.DEBUG);
                WriteLog($"Computer Name: {Environment.MachineName}", LogType.DEBUG);
                WriteLog($"User Name: {Environment.UserName}", LogType.DEBUG);
                WriteLog($"System Directory: {Environment.SystemDirectory}", LogType.DEBUG);
                WriteLog("Some environment variables:", LogType.DEBUG);
                WriteLog($"OS: {Environment.OSVersion.VersionString}", LogType.DEBUG);
                WriteLog($"NUMBER_OF_PROCESSORS: {Environment.ProcessorCount}", LogType.DEBUG);
                WriteLog($"PROCESSOR_ARCHITETURE: {(Environment.Is64BitProcess ? "x64" : "x86")}",
                    LogType.DEBUG);
                WriteLog($"WorkingSet: {Environment.WorkingSet}", LogType.DEBUG);

                #endregion

                #region Server Start

                WriteLog("Loading configuration file");

                XDocument file = XDocument.Load("AccountServer.xml");
                string host = file.XPathSelectElement("/AccountServerConfiguration/MySQL/Hostname")?.Value;
                string user = file.XPathSelectElement("/AccountServerConfiguration/MySQL/Username")?.Value;
                string pass = file.XPathSelectElement("/AccountServerConfiguration/MySQL/Password")?.Value;
                string database = file.XPathSelectElement("/AccountServerConfiguration/MySQL/Database")?.Value;
                string szPort = file.XPathSelectElement("/AccountServerConfiguration/MySQL/Port")?.Value;
                if (!ushort.TryParse(szPort, out ushort port))
                {
                    MessageBox.Show(this, @"Could not parse game mysql port. It's not an unsigned short value. "
                                          + @"Make sure your configuration file is correct and try again.");
                    return;
                }

                WriteLog("Connecting to account database...");
                ServerKernel.MyDatabase = new MySqlConfig(host, user, pass, database, port);

                SessionFactory.StartAccountConnection(
                    ServerKernel.MyDatabase.Host,
                    ServerKernel.MyDatabase.User,
                    ServerKernel.MyDatabase.Pass,
                    ServerKernel.MyDatabase.Database,
                    ServerKernel.MyDatabase.Port
                );
                WriteLog("Account database connected!");

                XDocument reader = XDocument.Load("AccountServer.xml");

                foreach (var server in reader.XPathSelectElements("/AccountServerConfiguration/Server"))
                {
                    EnabledGameServer gs = new EnabledGameServer(
                        server.Element("Name")?.Value ?? "Invalid",
                        int.Parse(server.Element("Port")?.Value ?? "3306"),
                        server.Element("Username")?.Value ?? "test",
                        server.Element("Password")?.Value ?? "test"
                    );
                    if (ServerKernel.EnabledServer.TryAdd(gs.Name, gs))
                        Program.MainForm.WriteLog($"Server {gs.Name} is permitted to login.");
                }

                WriteLog("Opening server connections");
                ServerKernel.GameSocket = new MsgServerSocket();
                ServerKernel.GameSocket.Bind("0.0.0.0", 9865);
                ServerKernel.GameSocket.Listen(5);
                WriteLog("Waiting for game server connections...");

                ServerKernel.LoginSocket = new LoginSocket();
                ServerKernel.LoginSocket.Bind("0.0.0.0", 9958);
                ServerKernel.LoginSocket.Listen(5);
                WriteLog("Waiting for client connections...");

                m_bgWorker = new BackgroundWorker();
                m_bgWorker.WorkerSupportsCancellation = true;
                m_bgWorker.DoWork += UIOnTimer_DoWork;
                m_bgWorker.RunWorkerCompleted += UIOnTimer_Complete;
                m_bgWorker.RunWorkerAsync();

                WriteLog("Server is ready for connections!");

                #endregion

                m_serverState = ServerState.Running;
                ServerKernel.StartTime = DateTime.Now;

                m_bHasOppenedOnce = true;
            }
        }

        private void UIOnTimer_Complete(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                m_bgWorker.Dispose();
                m_bgWorker = null;
            }
            finally
            {
                Close();
            }
        }

        private void UIOnTimer_DoWork(object sender, DoWorkEventArgs e)
        {
            TimeOut tTitle = new TimeOut(1);
            TimeOut tAnalytic = new TimeOut(300);
            TimeOutMS tCheckBan = new TimeOutMS(10000);

            var remove = new List<BannedAddress>();
            while (true)
            {
                if (tTitle.ToNextTime())
                {
                    DateTime now = DateTime.Now;
                    UpdateTitle(string.Format(Language.LoginServerWindowTitle, now.Hour, now.Minute, now.Second));
                    EditLabel(lblOnlineSv,
                        $"Online Servers: {ServerKernel.OnlineServers.Count}/{ServerKernel.EnabledServer.Count}");
                    EditLabel(lblOnlineUsr, $"Online Players: {ServerKernel.OnlinePlayers.Count}");
                    EditLabel(lblSentPackets,
                        $"Sent Packets: {Analytics.Statistic.SentPackets:N0}\t\tSent Bytes: {Calculations.FormatDataSize(Analytics.Statistic.SentBytes)}");
                    EditLabel(lblRecvPackets,
                        $"Recv Packets: {Analytics.Statistic.RecvPackets:N0}\t\tRecv Bytes: {Calculations.FormatDataSize(Analytics.Statistic.RecvBytes)}");
                    UpdateOnlineList();

                    if (ServerKernel.StartTime != DateTime.MaxValue)
                    {
                        var interval = DateTime.Now - ServerKernel.StartTime;
                        EditLabel(lblOnlineTime,
                            $"Uptime: {interval.Days} days, {interval.Hours:00} hours, {interval.Minutes:00} min and {interval.Seconds:00} secs");
                    }
                }

                if (ServerKernel.BannedAddresses.Count > 0 && tCheckBan.ToNextTime())
                    remove = ServerKernel.BannedAddresses.Values.Where(banned => !banned.Banned).ToList();

                if (remove.Count > 0)
                {
                    foreach (var get in remove)
                        ServerKernel.BannedAddresses.TryRemove(get.Address, out _);
                }

                if (m_bgWorker.CancellationPending)
                {
                    try
                    {
                        foreach (var server in ServerKernel.OnlineServers.Values)
                            server.Disconnect();

                        foreach (var player in ServerKernel.OnlinePlayers.Values)
                        {
                            player.SetLogout(true);
                        }
                    }
                    finally
                    {
                        e.Cancel = true;
                    }

                    return;
                }

                Thread.Sleep(1000);
            }
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            if (!File.Exists("AccountServer.xml"))
            {
                FrmConfiguration form = new FrmConfiguration();
                form.ShowDialog(this);
            }
        }

        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (m_bgWorker == null || m_bgWorker.CancellationPending)
                return;

            m_bgWorker.CancelAsync();
            e.Cancel = true;
        }
    }

    public enum ServerState
    {
        Idle,
        Running
    }
}