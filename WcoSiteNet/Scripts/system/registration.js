﻿var AccountMgr = function() {
    return {
        CreateNewAccount: function() {
            event.preventDefault();
            var url = $("#frmRegistration").attr("action");
            $.ajax({
                url: url,
                type: "POST",
                data: $("#frmRegistration").serialize()
            }).done(function(res) {
                if (!res.Success) {
                    $('input[name=Password]').value = "";
                    $('input[name=ConfirmPassword]').value = "";
                    $('#tempReturnMessage').html(
                        "<div class=\"alert alert-danger alert-bordered pd-y-20 wd-100p\" role=\"alert\">\r\n  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">" +
                        "<span aria-hidden=\"true\">&times;</span></button>" +
                        "<div class=\"d-flex align-items-center justify-content-start\">" +
                        "<i class=\"icon ion-ios-close alert-icon tx-52 tx-danger mg-r-20\"></i>" +
                        "<div><h5 class=\"mg-b-2 tx-danger\">Oops!</h5>" +
                        "<p class=\"mg-b-0 tx-gray\">" +
                        res.Error +
                        "</p>" +
                        "</div></div></div>");
                    $('#tempReturnMessage').css("visibility", "visible");
                    grecaptcha.reset();
                } else {
                    window.location.href = "/Panel";
                }
            }).fail(function() {

            });
        },
        ForgotPassword: function() {

        },
        CreateNewGameAccount: function() {
            event.preventDefault();
            var url = $("#frmRegisterGameAccount").attr("action");
            $.ajax({
                url: url,
                type: "POST",
                data: $("#frmRegisterGameAccount").serialize()
            }).done(function(res) {
                if (!res.Success) {
                    $('input[name=Password]').value = "";
                    $('input[name=ConfirmPassword]').value = "";
                    $('#tempReturnMessage').html(
                        "<div class=\"alert alert-danger alert-bordered pd-y-20 wd-100p\" role=\"alert\">\r\n  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">" +
                        "<span aria-hidden=\"true\">&times;</span></button>" +
                        "<div class=\"d-flex align-items-center justify-content-start\">" +
                        "<i class=\"icon ion-ios-close alert-icon tx-52 tx-danger mg-r-20\"></i>" +
                        "<div><h5 class=\"mg-b-2 tx-danger\">Oops!</h5>" +
                        "<p class=\"mg-b-0 tx-gray\">" +
                        res.Error +
                        "</p>" +
                        "</div></div></div>");
                    $('#tempReturnMessage').css("visibility", "visible");
                    grecaptcha.reset();
                } else {
                    window.location.reload();
                }
            }).fail(function() {

            });
        }
    };
};