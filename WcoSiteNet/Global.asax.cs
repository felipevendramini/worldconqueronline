﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5729 (auto hunt feature)
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - WcoSiteNet - Global.asax.cs
// Last Edit: 2019/10/16 17:02
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using FtwCore.Database;
using WcoSiteNet.Models.Dashboard;

#endregion

namespace WcoSiteNet
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            SessionFactory.StartAccountConnection(ConfigurationManager.AppSettings["accountMysql"]);
            SessionFactory.StartGameConnection(ConfigurationManager.AppSettings["gameMysql"]);
            SessionFactory.StartResourceConnection(ConfigurationManager.AppSettings["logMysql"]);
            SessionFactory.StartLogConnection(ConfigurationManager.AppSettings["resourceMysql"]);

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        /// <summary>
        /// Thriggered when the user session expires.
        /// </summary>
        protected void Session_OnEnd(object sender, EventArgs e)
        {
            try
            {
                ((GlobalAccount) Session["User"])?.SetLogout();
                Session.Clear();
                Session.Abandon();
            }
            catch
            {
            }
        }

        protected void Application_EndRequest()
        {
        }
    }
}