﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - WcoSiteNet - BaseController.cs
// Last Edit: 2019/11/24 19:07
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using WcoSiteNet.Models;
using WcoSiteNet.Models.Dashboard;

#endregion

namespace WcoSiteNet.Controllers
{
    public abstract class BaseController : Controller
    {
        public new GlobalAccount User;

        public bool IsConnected => base.User.Identity.GetUserId<int>() > 0;

        #region Override

        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            /**
             * Antes de mais nada, inicializaremos buscando definir o idioma de exibição do site.
             * O padrão é o pt-BR.
             */
            string lang = "pt-BR";
            /**
             * Se nossa sessão for nula, utilizaremos dos Cookies para armazenar o idioma. Senão,
             * o mesmo será armazenado dentro da sessão do usuário.
             */
            if (Session == null)
            {
                HttpCookie langCookie = Request.Cookies["culture"];
                /**
                 * Se o Cookie já existir, pegamos o valor e definimos para o carregamento da página.
                 * Senão, definiremos o idioma desejado pelo usuário (definido na criação da session
                 * ou na escolha pelo site).
                 */
                if (langCookie != null)
                {
                    lang = langCookie.Value;
                }
                else
                {
                    var userLanguage = Request.UserLanguages;
                    var userLang = userLanguage != null ? userLanguage[0] : "";
                    lang = userLang != "" ? userLang : LanguageManager.GetDefaultLanguage();
                }
            }
            else
            {
                lang = Session.Get<string>("Locale");
                /**
                 * Caso nossa sessão já exista, estamos verificando se existe um valor atribuido.
                 * Caso não haja, utilizaremos o idioma padrão que é o pt-BR. Senão, verificamos
                 * se o idioma selecionado realmente existe e o definimos.
                 */
                if (string.IsNullOrEmpty(lang))
                {
                    lang = LanguageManager.GetDefaultLanguage();
                }
                else
                {
                    if (!LanguageManager.IsLanguageAvailable(lang))
                        lang = LanguageManager.GetDefaultLanguage();
                }
            }

            /**
             * Finalmente, definimos nosso idioma para o uso correto com os arquivos de RESX.
             */
            new LanguageManager().SetLanguage(lang, Session);
            return base.BeginExecuteCore(callback, state);
        }

        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            /**
             * Realizaremos a autorização padrão do sistema.
             */
            base.OnAuthorization(filterContext);

            /**
             * Se o usuário ainda não foi definido e nosso Usuário do Identity está autênticado,
             * realizaremos a criação do usuário e adicionaremos à Sessão.
             */
            if (User == null && (base.User?.Identity?.IsAuthenticated ?? false))
            {
                /**
                 * Como os elementos são armazenados dentro de um único objeto na sessão, não podemos buscá-lo no banco de dados
                 * o tempo inteiro. Portanto, Se nosso usuário for nulo (acabou de fazer login), o carregaremos pelo seu construtor.
                 * Se ele já existe na sessão, apenas buscaremos o objeto novamente e atualizaremos sua entidade de banco de dados,
                 * para que possamos saber se está tudo nos conformes com sua licença e etc.
                 */
                if (Session.GetUser() == null)
                {
                    User = GetUser(base.User.Identity.GetUserId<int>(), Session);
                    User.SetLogin(HttpContext.Request.UserHostAddress);
                }
                else
                {
                    User = Session.GetUser();
                    // Use o Refresh apenas para atualizar a Entidade, pode ser removido se a verificação de integridade for feita apenas
                    // no primeiro login.
                    User.Refresh(Session);
                }

                if (User == null)
                {
                    filterContext.Result = RedirectToAction("DoLogout", "Account");
                    return;
                }

                Session.Add("User", User);

                filterContext.Controller.ViewData.Model = new BaseModel
                {
                    User = User
                };
            }
        }

        private GlobalAccount GetUser(int identity, HttpSessionStateBase session)
        {
            GlobalAccount account = new GlobalAccount(session);
            if (account.Init(identity))
                return account;
            return null;
        }

        #endregion

        public string GetBaseUrl()
        {
            //return Request.Url.GetLeftPart(UriPartial.Authority) + Url.Content("~");
            //return new Uri(Request.Url, Url.Content("~")).ToString();
            string appPath = string.Empty;

            if (HttpContext.Request?.Url != null)
            {
                //Formatting the fully qualified website url/name
                appPath = string.Format("{0}://{1}{2}{3}",
                    HttpContext.Request.Url.Scheme,
                    HttpContext.Request.Url.Host,
                    HttpContext.Request.Url.Port == 80 ? string.Empty : ":" + HttpContext.Request.Url.Port,
                    HttpContext.Request.ApplicationPath);
            }

            if (!appPath.EndsWith("/"))
            {
                appPath += "/";
            }

            return appPath;
        }
    }
}