﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - WcoSiteNet - HomeController.cs
// Last Edit: 2019/11/24 19:07
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Web.Mvc;
using FtwCore.Database.Repositories;
using WcoSiteNet.Models;

#endregion

namespace WcoSiteNet.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("Index", "Dashboard");
        }

        [AllowAnonymous]
        public ActionResult TermsOfUse()
        {
            return View("PunishmentTable");
        }

        [AllowAnonymous]
        public ActionResult TermsOfPrivacy()
        {
            return View("PrivacyTerms");
        }

        [HttpGet]
        [AllowAnonymous]
        public JsonResult GuildWarWinner()
        {
            dynamic query = new ActionRepository(MysqlTargetConnection.Account)
                .ExecutePureQuery("SELECT syn.id, IFNULL(syn.`name`, 'StrNone') synname, IFNULL(syn.`leader_name`, 'StrNone') synleader, IFNULL(syn.`leader_id`, 0) synid FROM cq_dynanpc npc " +
                                    "LEFT JOIN cq_syndicate syn ON syn.id = npc.ownerid " +
                                    "WHERE npc.id = 820");
            if (query == null)
            {
                return Json(new
                {
                    synid = 0,
                    synname = LanguageManager.GetString("StrNone"),
                    synleaderid = 0,
                    synleadername = LanguageManager.GetString("StrNone")
                }, JsonRequestBehavior.AllowGet);
            }
            return Json(new
            {
                synid = (int) int.Parse(query[0].ToString()),
                synname = (string) query[1].ToString(),
                synleaderid = (int) int.Parse(query[3].ToString()),
                synleadername = (string) query[2].ToString()
            }, JsonRequestBehavior.AllowGet);
        }
    }
}