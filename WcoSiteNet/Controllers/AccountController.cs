﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - WcoSiteNet - AccountController.cs
// Last Edit: 2019/11/24 19:07
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#define NO_INDEX

#region References

using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using FtwCore.Database.Entities;
using FtwCore.Database.Repositories;
using FtwCore.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using reCAPTCHA.MVC;
using WcoSiteNet.Models;
using WcoSiteNet.Models.Account;
using WcoSiteNet.Models.Dashboard;
using WcoSiteNet.Models.Identity;

#endregion

namespace WcoSiteNet.Controllers
{
    public class AccountController : BaseController
    {
        // GET: Account
        public ActionResult Index()
        {
            if (IsConnected)
                return RedirectToAction("Index", "Dashboard");
            return RedirectToAction("Login");
        }

        public ActionResult Login()
        {
            if (IsConnected)
                return RedirectToAction("Index", "Dashboard");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<ActionResult> DoLogin(LoginFormModel model)
        {
            if (IsConnected)
                return RedirectToAction("Index", "Dashboard");

            if (!ModelState.IsValid)
            {
                TempData["Error"] = LanguageManager.GetString("StrErrorRegisterInvalidModel");
                return RedirectToAction("Login");
            }

            var result = await SignInManager.PasswordSignInAsync(model.Username, model.Password, false, false);
            if (result == SignInStatus.Success)
            {
                AccountWebEntity user = UserManager.FindByEmail(model.Username);
                if (user.FirstLogin == null)
                    user.FirstLogin = DateTime.Now;
                if (user.LastLogin == null)
                    user.LastLogin = DateTime.Now;
                new AccountWebRepository().Save(user);
                return RedirectToAction("Index", "Dashboard");
            }

            TempData["Error"] = LanguageManager.GetString("StrInvalidCredentials");
            return RedirectToAction("Login", "Account");
        }

        public ActionResult Register()
        {
            ViewData.Model = new RegisterPageModel();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        [CaptchaValidator]
        public JsonResult DoRegister(RegisterFormModel model, bool captchaValid)
        {
            JsonResultModel result = new JsonResultModel();

            if (!captchaValid)
            {
                result.Error = LanguageManager.GetString("StrErrorRegisterInvalidCaptcha");
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (!ModelState.IsValid)
            {
                result.Error = LanguageManager.GetString("StrErrorRegisterInvalidModel");
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (model.Gender < 0 || model.Gender > 4)
            {
                result.Error = LanguageManager.GetString("StrErrorRegisterInvalidGender");
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (string.IsNullOrEmpty(model.Username))
            {
                result.Error = LanguageManager.GetString("StrErrorRegisterEmptyRealName");
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            AccountWebRepository repository = new AccountWebRepository();
            var user = repository.SearchByName(model.Username);
            if (user != null)
            {
                result.Error = LanguageManager.GetString("StrErrorRegisterUsernameAlreadyInUse");
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (model.Password.Length < 6 || model.Password.Length > 32)
            {
                result.Error = LanguageManager.GetString("StrErrorRegisterInvalidPasswordLength");
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (model.Password != model.ConfirmPassword)
            {
                result.Error = LanguageManager.GetString("StrErrorRegisterPasswordNoMatch");
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (model.SecurityQuestion <= 0)
            {
                result.Error = LanguageManager.GetString("StrErrorRegisterInvalidSecurityQuestion");
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            // todo verify if question exists.
            if (string.IsNullOrEmpty(model.SecurityAnswer))
            {
                result.Error = LanguageManager.GetString("StrErrorRegisterInvalidSecurityAnswer");
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            if (model.SecurityCode < 99999)
            {
                result.Error = LanguageManager.GetString("StrErrorRegisterInvalidSecurityCode");
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            user = new AccountWebEntity
            {
                UserName = model.Username,
                PasswordHash = WhirlpoolHash.HardHash(model.Password),
                Language = "pt_BR",
                Gender = (byte) model.Gender,
                Type = (byte) AccountType.User,
                VipLevel = 0,
                VipPoints = 0,
                Country = "BR",
                SecurityCode = (ulong) model.SecurityCode,
                Flag = 0,
                RealName = model.RealName,
                Age = model.Birthday,
                Address = "",
                AddressNum = 0,
                Hash = WhirlpoolHash.Hash(new Random().Next().ToString()),
                Locale = "pt_BR",
                NetBarIp = Request.UserHostAddress,
                Phone = "",
                SecurityAnswer = model.SecurityAnswer,
                SecurityQuestion = model.SecurityQuestion.ToString(),
                VipCoins = 0
            };

            result.Success = repository.Save(user);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult DoLogout()
        {
            User?.SetLogout();
            SignInManager.SignOut();
            Session.Clear();
            Session.Abandon();
#if NO_INDEX
            return RedirectToAction("Index", "Account");
#else
            return RedirectToAction("Index", "Home");
#endif
        }

        public SignInManager SignInManager => HttpContext.GetOwinContext().Get<SignInManager>();

        public UserManager UserManager => HttpContext.GetOwinContext().GetUserManager<UserManager>();
    }
}