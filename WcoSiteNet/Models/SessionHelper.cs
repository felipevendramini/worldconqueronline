﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - WcoSiteNet - SessionHelper.cs
// Last Edit: 2019/11/24 19:07
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.Web;
using WcoSiteNet.Models.Dashboard;

#endregion

namespace WcoSiteNet.Models
{
    public static class SessionHelper
    {
        public static void SetUser(this HttpSessionStateBase session, GlobalAccount account)
        {
            session.Add("User", account);
        }

        public static GlobalAccount GetUser(this HttpSessionStateBase session)
        {
            return session.Get<GlobalAccount>("User");
        }

        public static TV Get<TV>(this HttpSessionStateBase session, string key)
        {
            return session[key] is TV ? (TV) session[key] : default;
        }

        public static bool Get<TV>(this HttpSessionStateBase session, string key, out TV value)
        {
            if (session[key] != null && session[key] is TV)
            {
                value = (TV) session[key];
                return true;
            }

            value = default;
            return false;
        }
    }
}