﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - WcoSiteNet - GameAccountRegisterModel.cs
// Last Edit: 2019/11/24 19:07
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System.ComponentModel.DataAnnotations;
using FtwCore;

#endregion

namespace WcoSiteNet.Models.Dashboard
{
    public class GameAccountRegisterModel
    {
        [Required(ErrorMessageResourceType = typeof(LanguageResource), ErrorMessageResourceName = "StrErrorEmptyUsername")]
        [MinLength(4, ErrorMessageResourceType = typeof(LanguageResource), ErrorMessageResourceName = "StrErrorGameUsernameLength")]
        [MaxLength(16, ErrorMessageResourceType = typeof(LanguageResource), ErrorMessageResourceName = "StrErrorGameUsernameLength")]
        public string Username { get; set; }

        [Required(ErrorMessageResourceType = typeof(LanguageResource), ErrorMessageResourceName = "StrErrorEmptyPassword")]
        [MinLength(4, ErrorMessageResourceType = typeof(LanguageResource), ErrorMessageResourceName = "StrErrorGamePasswordLength")]
        [MaxLength(16, ErrorMessageResourceType = typeof(LanguageResource), ErrorMessageResourceName = "StrErrorGamePasswordLength")]
        public string Password { get; set; }

        [Required(ErrorMessageResourceType = typeof(LanguageResource), ErrorMessageResourceName = "StrErrorEmptyPassword")]
        [MinLength(4, ErrorMessageResourceType = typeof(LanguageResource), ErrorMessageResourceName = "StrErrorGamePasswordLength")]
        [MaxLength(16, ErrorMessageResourceType = typeof(LanguageResource), ErrorMessageResourceName = "StrErrorGamePasswordLength")]
        public string ConfirmPassword { get; set; }
    }
}