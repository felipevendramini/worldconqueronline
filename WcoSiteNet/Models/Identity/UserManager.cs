﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - WcoSiteNet - UserManager.cs
// Last Edit: 2019/11/24 19:07
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using FtwCore.Database.Entities;
using FtwCore.Security;
using Microsoft.AspNet.Identity;

#endregion

namespace WcoSiteNet.Models.Identity
{
    public class UserManager : UserManager<AccountWebEntity, int>
    {
        public UserManager(IUserStore<AccountWebEntity, int> store)
            : base(store)
        {
            UserValidator = new UserValidator<AccountWebEntity, int>(this);
            PasswordValidator = new PasswordValidator() {RequiredLength = 6};
            PasswordHasher = new NewPasswordHasher();
        }
    }

    public class NewPasswordHasher : IPasswordHasher
    {
        public string HashPassword(string password)
        {
            return WhirlpoolHash.HardHash(password);
        }

        public PasswordVerificationResult VerifyHashedPassword(string hashedPassword, string providedPassword)
        {
            // Whirlpool hash has 128 characters (or 64 bytes of data), the password has two characters more which indicates
            // the password version. 00 is the actual password version.
            if (hashedPassword.Length < 130 || !hashedPassword.EndsWith("00") || HashPassword(providedPassword) != hashedPassword)
                return PasswordVerificationResult.Failed;
            return PasswordVerificationResult.Success;
        }
    }
}