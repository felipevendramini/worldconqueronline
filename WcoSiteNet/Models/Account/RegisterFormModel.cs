﻿// ////////////////////////////////////////////////////////////////////////////////////
// World Conquer Online Version 3 - Phoenix Project Based
// This project has been created by Felipe Vieira Vendramini
// Source Infrastructure based on Phoenix Source, written by Gareth Jensen
// This source is targeted to client 5808
// 
// If this source has ever been released, please keep the credits and don't
// claim it as yours. This source has features from many sources, has been
// optmized for a better resource use and is meant to have great scalability
// and maintability.
// 
// File information
// File Created by: Felipe Vieira Vendramini
// Computer User: FELIPEVIEIRAVENDRAMI - FELIPE VIEIRA VENDRAMINI
// WorldConquerOnline - WcoSiteNet - RegisterFormModel.cs
// Last Edit: 2019/11/24 19:07
// Created: 2019/10/04 19:06
// ////////////////////////////////////////////////////////////////////////////////////

#region References

using System;
using System.ComponentModel.DataAnnotations;

#endregion

namespace WcoSiteNet.Models.Account
{
    public class RegisterFormModel
    {
        [Required]
        [EmailAddress]
        [MaxLength(64)]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [MaxLength(32)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [MaxLength(32)]
        public string ConfirmPassword { get; set; }

        [Required]
        [Range(0, long.MaxValue)]
        public long SecurityCode { get; set; }

        [Required]
        [Range(0,4)]
        public int Gender { get; set; }

        [Required]
        public int SecurityQuestion { get; set; }

        [Required]
        [MaxLength(16)]
        public string SecurityAnswer { get; set; }

        [Required]
        [MaxLength(32)]
        public string RealName { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Birthday { get; set; }
    }
}