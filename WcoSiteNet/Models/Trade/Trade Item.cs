﻿using FtwCore.Database.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WcoSiteNet.Models.Trade
{
    public class TradeItem
    {
        public TradeItem(string xml)
        {
            try
            {
                Item = JsonConvert.DeserializeObject<ItemEntity>(xml);
            }
            catch
            {
                Item = null;
            }
        }

        public ItemEntity Item { get; private set; }
    }
}